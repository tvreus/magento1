<?php
class MultiSafepay_Msp_Model_Resource_Tokenization extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('msp/tokenization','id');
    }
}