<?php

/**
 *
 * @category MultiSafepay
 * @package  MultiSafepay_Msp
 */
/** @var $this MultiSafepay_Msp_Model_Setup */

$installer = $this;


$installer->startSetup();

$table = $installer->getConnection()->newTable($installer->getTable('multisafepay_tokenization'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
        'unsigned' => true,
        'nullable' => false,
        'primary' => true,
        'identity' => true,
    ), 'ID')
    ->addColumn('customer_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable' => false,
    ), 'Customer ID')
    ->addColumn('order_id', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable' => false,
    ), 'Order Id')    
    ->addColumn('recurring_id', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable' => true,
        'default' => null,
    ), 'Token - Recurring ID')
    ->addColumn('recurring_hash', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable' => false,
    ), 'Recurring hash')
    ->addColumn('cc_type', Varien_Db_Ddl_Table::TYPE_VARCHAR, 64, array(
        'nullable' => false,
        'default' => '0',
    ), 'Payment method')
    ->addColumn('cc_last4', Varien_Db_Ddl_Table::TYPE_VARCHAR, 4, array(
        'nullable' => true,
        'default' => null,
    ), 'Last 4')
    ->addColumn('cc_expiry_date', Varien_Db_Ddl_Table::TYPE_VARCHAR, 4, array(
        'nullable' => true,
        'default' => null,
    ), 'Expiry Date')
    ->addColumn('cc_name', Varien_Db_Ddl_Table::TYPE_VARCHAR, 64, array(
        'nullable' => true,
        'default' => null,
    ), 'Custom name')
    ->setComment('MultiSafepay Tokenization Table');


$installer->getConnection()->createTable($table);




$installer->endSetup();




