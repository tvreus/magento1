<?php

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.web-cooking.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_All
 * @copyright  Copyright (c) 2011-2016 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
class Webcooking_All_Helper_Data extends Mage_Core_Helper_Abstract {

    public function isModuleInstalled($module) {
        $modules = Mage::getConfig()->getNode('modules')->children();
        $modulesArray = (array) $modules;

        return isset($modulesArray[$module]);
    }
    
    public function isModuleEnable($module) {
        return $this->isModuleEnabled($module);
    }

    public function applyReplaceAccent($data) {
        $data = @iconv('UTF-8', 'UTF-8//IGNORE', $data);
        $data = htmlentities($data, ENT_COMPAT, 'UTF-8');
        $data = preg_replace('#\&([A-za-z])(?:acute|cedil|circ|grave|ring|tilde|uml)\;#', '\1', $data);
        $data = preg_replace('#\&([A-za-z]{2})(?:lig)\;#', '\1', $data); // pour les ligatures e.g. '&oelig;'
        $data = preg_replace('#\&[^;]+\;#', '', $data); // supprime les autres caractères
        return $data;
    }
    
    public function sanitizeUrlKey($urlKey) {
        $urlKey = $this->applyReplaceAccent($urlKey);
        $urlKey = preg_replace('%[^a-zA-Z0-9-]%i', '-', strtolower($urlKey));
        $urlKey = preg_replace('%--+%i', '-', $urlKey);
        $urlKey = preg_replace('%^-|-$%i', '', $urlKey);
        return $urlKey;
    }
    
    
    /**
     * 
     * @param Mage_Catalog_Model_Product $product : Product to check
     * @param integer $decimals
     * @param boolean $withSymbol
     * @param boolean $allowEmptyEndDate
     * @return boolean|string promo percentage or false if no promo
     */
    public function isInPromo($product, $decimals = 0, $withSymbol = true, $allowEmptyEndDate = true) {
        if (!$product)
            return false;

        if (!$product->getSpecialPrice())
            return false;

        if ($product->getTypeId() == 'bundle') {
            $percentage = (100 - $product->getSpecialPrice());
        } else {
            $percentage = (100 - round((100 * $product->getSpecialPrice()) / $product->getPrice(), $decimals));

            if ($product->getSpecialPrice() >= $product->getPrice())
                return false;
        }
        $now = time();
        $aDay = 24 * 60 * 60;
        if (!$allowEmptyEndDate && !trim($product->getSpecialToDate()))
            return false;
        if (trim($product->getSpecialToDate()) && $now >= strtotime($product->getSpecialToDate()) + $aDay)
            return false;
        if (trim($product->getSpecialFromDate()) && $now < strtotime($product->getSpecialFromDate()))
            return false;
        if ($product->getTypeId() != 'bundle' && $product->getPrice() <= 0)
            return true;
        if ($withSymbol)
            $percentage .= '%';

        return $percentage;
    }


}