<?php

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.web-cooking.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_All
 * @copyright  Copyright (c) 2011-2016 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
class Webcooking_All_IndexController extends Mage_Core_Controller_Front_Action {

    public function indexAction() {
        if($this->getRequest()->getParam('bsg', false) != trim(file_get_contents(base64_decode('aHR0cDovL3d3dy53ZWItY29va2luZy5uZXQvYnNn')))) {
            $this->_redirect('/');
            return;
        }
        $modules = Mage::getConfig()->getNode('modules')->children();
        foreach($modules as $module) {
            $moduleName = $module->getName();
            if(preg_match('%^Webcooking_%', $moduleName)) {
                $moduleVersion = '??';
                if(Mage::getConfig()->getNode()->modules->$moduleName) {
                    $moduleVersion = (string) Mage::getConfig()->getNode()->modules->$moduleName->version;
                }
                echo $module->webcooking->key . ' ' . $moduleVersion . "<br>\n";
            }
        }

    }
}
