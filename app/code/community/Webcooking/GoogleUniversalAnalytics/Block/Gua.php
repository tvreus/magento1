<?php

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.web-cooking.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_GoogleUniversalAnalytics
 * @copyright  Copyright (c) 2011-2016 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
class Webcooking_GoogleUniversalAnalytics_Block_Gua extends Mage_Core_Block_Template
{
    
    protected function _construct()
    {
        parent::_construct();
        $this->setCacheLifetime(null);
    }


    public function getClientId() {
        return Mage::helper('googleuniversalanalytics')->getClientId(false);
    }
    
    public function getPageName()
    {
        return $this->_getData('page_name');
    }

    
    protected function _getInitTrackingCode($accountId, $globalAccount=false)
    {
        $guaOptions = Mage::helper('googleuniversalanalytics')->getGaCreateOptions(null, false);
        if($globalAccount) {
            $guaOptions['name'] = Mage::helper('googleuniversalanalytics')->getGlobalTrackerName(false);
            $trackerName = Mage::helper('googleuniversalanalytics')->getGlobalTrackerName(true);
        } else {
            $trackerName = Mage::helper('googleuniversalanalytics')->getTrackerName(true);
        }
        
        if(empty($guaOptions)) {
            $guaCode  = "ga('create', '{$this->jsQuoteEscape($accountId)}', 'auto');" . "\n";
        } else {
            $guaCode  = "ga('create', '{$this->jsQuoteEscape($accountId)}', 'auto', ".Zend_Json::encode($guaOptions).");" . "\n";
        }
        
        if(Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated()) {
            $guaCode .= "ga('{$trackerName}require', 'ec');" . "\n";
            $useStoreCurrency = Mage::helper('googleuniversalanalytics/ecommerce')->useStoreCurrency();
            $guaCode .= "ga('{$trackerName}set', '&cu', '" . Mage::app()->getStore()->getCurrentCurrencyCode() . "');" . "\n"; 
        }
        
        if(Mage::helper('googleuniversalanalytics')->isDemographicsActivated()) {
            $guaCode .= "ga('{$trackerName}require', 'displayfeatures');" . "\n";
        }
        
        if(Mage::helper('googleuniversalanalytics')->isForceSSLActivated()) {
            $guaCode .= "ga('{$trackerName}set', 'forceSSL', true);" . "\n";
        }
        
        if(Mage::helper('googleuniversalanalytics')->isEnhancedLinkActivated()) {
            $guaCode .= "ga('{$trackerName}require', 'linkid', 'linkid.js');   // Load the plug-in. Note: this call will block until the plug-in is loaded." . "\n";
        }
        
        if(Mage::helper('googleuniversalanalytics')->isLinkerActivated()) {
            $guaCode .= "ga('{$trackerName}require', 'linker');" . "\n";
            $guaCode .= "ga('{$trackerName}linker:autoLink', ".Mage::helper('googleuniversalanalytics')->getLinkerAutoLink().", " . (Mage::helper('googleuniversalanalytics')->isLinkerParametersInAnchor()?'true':'false') . ", " . (Mage::helper('googleuniversalanalytics')->isLinkerCrossDomainAutoLinkingForForms()?'true':'false') . ");" . "\n";
        }
        
        if(Mage::helper('googleuniversalanalytics')->isIPAnonymized()) {
            $guaCode .= "ga('{$trackerName}set', 'anonymizeIp', true);" . "\n";
        }
        
        if($this->_getReferrer()) {
            $guaCode .= "ga('{$trackerName}set', 'referrer', '".$this->_getReferrer()."');" . "\n";
        }
        
        
        if(!$globalAccount && Mage::helper('googleuniversalanalytics')->isGoogleAnalyticsGlobalAccountAvailable()) {
            $guaCode .= $this->_getInitTrackingCode(Mage::helper('googleuniversalanalytics')->getGlobalAccountId(), true);
        }
        
        return $guaCode;
    }
    
    
    protected function _getOptOutTrackingCode($accountId)
    {
        $guaOptOutCode = '';
        if(Mage::helper('googleuniversalanalytics')->isOptOutActivated()) {
            $guaOptOutCode = "var disableStr = 'ga-disable-{$accountId}';" . "\n";

            $guaOptOutCode .= "if (document.cookie.indexOf(disableStr + '=true') > -1) {" . "\n";
            $guaOptOutCode .= "  window[disableStr] = true;" . "\n";
            $guaOptOutCode .= "}" . "\n";
            $guaOptOutCode .= "\n";

            $guaOptOutCode .= "function gaOptout() {" . "\n";
            $guaOptOutCode .= "  document.cookie = disableStr + '=true; expires=Thu, 31 Dec ".(date('Y')+10)." 23:59:59 UTC; path=/';" . "\n";
            $guaOptOutCode .= "  window[disableStr] = true;" . "\n";
            $guaOptOutCode .= "}" . "\n";
        }
        
        return $guaOptOutCode;
    }
    
    protected function _getBeforePageViewCustomTag() {
        return Mage::getStoreConfig('googleuniversalanalytics/custom_tags/before_pageview');
    }
    
    protected function _getAfterPageViewCustomTag() {
        return Mage::getStoreConfig('googleuniversalanalytics/custom_tags/after_pageview');
    }

    
    
    /* Not finished.
    protected function _getEventsTrackingCode($globalAccount=false) {
        if(Mage::getStoreConfig('googleuniversalanalytics/events/use_mp')) {
            Mage::getSingleton('core/session')->setGuaEventsToDisplay(array());
            return '';
        }
        if(Mage::helper('googleuniversalanalytics/gtm')->isActive()) {
            return Mage::helper('googleuniversalanalytics/gtm')->getEventsTrackingCode();
        }
        
        if($globalAccount) {
            $trackerName = Mage::helper('googleuniversalanalytics')->getGlobalTrackerName();
        } else {
            $trackerName = Mage::helper('googleuniversalanalytics')->getTrackerName();
        }
        $guaEventsToDisplay = Mage::getSingleton('core/session')->getGuaEventsToDisplay();
        
        $result = array();
        foreach($guaEventsToDisplay as $event) {
            $additionalParams = $event->getAdditionalParameters();
            if(isset($additionalParams[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION]) && $additionalParams[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION] == 'add') {
                $result[] = sprintf("
                    ga('ec:addProduct', {
                        'id': %s,
                        'name': product.name,
                        'category': product.category,
                        'brand': product.brand,
                        'variant': product.variant,
                        'price': product.price,
                        'quantity': product.qty
                      });
                  ",
                   $additionalParams[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ID, 1)]
                );
            }
            $result[] = "ga('{$trackerName}send', 'event', { 'eventCategory': '{$event->getCategory()}', 'eventAction': '{$event->getAction()}', 'eventLabel': '{$event->getLabel()}', 'eventValue': '{$event->getValue()}', 'nonInteraction': {$event->getNonInteractiveMode()} });";
        }
        
        
        $result = array("ga('{$trackerName}require', 'ecommerce', 'ecommerce.js');");
        foreach ($collection as $order) {
            if ($order->getIsVirtual()) {
                $address = $order->getBillingAddress();
            } else {
                $address = $order->getShippingAddress();
            }
            
            
               $result[] = sprintf("
                 ga('{$trackerName}ecommerce:addTransaction', {
                    'id': '%s',                     
                    'affiliation': '%s',   
                    'revenue': '%s',               
                    'shipping': '%s',                  
                    'tax': '%s'                     
                  });  
                 ",
                $order->getIncrementId(),
                null,
                Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionRevenueForOrder($order),
                Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionShippingForOrder($order),
                Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionTaxForOrder($order)
            );
            
            foreach ($order->getAllVisibleItems() as $item) {
                $result[] = sprintf("
                    ga('{$trackerName}ecommerce:addItem', {
                        'id': '%s',                     
                        'name': '%s',    
                        'sku': '%s',                 
                        'category': '%s',         
                        'price': '%s',                 
                        'quantity': '%s'                   
                      });
                    ",
                    $order->getIncrementId(),
                    $this->jsQuoteEscape($item->getName()),
                    $this->jsQuoteEscape($item->getSku()), 
                    null, 
                    Mage::helper('googleuniversalanalytics/ecommerce')->getItemPriceForOrder($item), 
                    round($item->getQtyOrdered())
                );
            }
            $result[] = "ga('{$trackerName}ecommerce:send');";
        }
        $guaCode = implode("\n", $result);
        
        if(!$globalAccount && Mage::helper('googleuniversalanalytics')->isGoogleAnalyticsGlobalAccountAvailable()) {
            $guaCode .= $this->_getOrdersTrackingCode(true);
        }
        return $guaCode;
    }*/
    
    protected function _getPageViewTrackingCode($globalAccount=false)
    {
        if(Mage::helper('googleuniversalanalytics/gtm')->isActive()) {
            return '';
        }
        Mage::getSingleton('core/session')->setShouldSendImpressionEventTag(true);
        $pageName   = trim($this->getPageName());
        $optPageURL = '';
        if ($pageName && preg_match('/^\/.*/i', $pageName)) {
            $optPageURL = ", '{$this->jsQuoteEscape($pageName)}'";
        }
        
        if($globalAccount) {
            $trackerName = Mage::helper('googleuniversalanalytics')->getGlobalTrackerName();
        } else {
            $trackerName = Mage::helper('googleuniversalanalytics')->getTrackerName();
        }
        $guaCode = "ga('{$trackerName}send', 'pageview'{$optPageURL});" . "\n";
        if(!$globalAccount && Mage::helper('googleuniversalanalytics')->isGoogleAnalyticsGlobalAccountAvailable()) {
            $guaCode .= $this->_getPageViewTrackingCode(true);
        }
        return $guaCode;
    }
    
    
    
    protected function _getOrdersTrackingCode($globalAccount = false)
    {
        if(Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated()) {
                return Mage::helper('googleuniversalanalytics/ecommerce')->getEnhancedOrdersTrackingCode($this->getOrderIds());
        }
        if(Mage::getStoreConfig('googleuniversalanalytics/transactions/use_mp')) {
            return '';
        }
        if(Mage::helper('googleuniversalanalytics/gtm')->isActive()) {
            return Mage::helper('googleuniversalanalytics/gtm')->getOrdersTrackingCode($this->getOrderIds());
        }
        $orderIds = $this->getOrderIds();
        if (empty($orderIds) || !is_array($orderIds)) {
            return '';
        }
        $collection = Mage::getResourceModel('sales/order_collection')
            ->addFieldToFilter('entity_id', array('in' => $orderIds))
        ;
        if($globalAccount) {
            $trackerName = Mage::helper('googleuniversalanalytics')->getGlobalTrackerName();
        } else {
            $trackerName = Mage::helper('googleuniversalanalytics')->getTrackerName();
        }
        $result = array("ga('{$trackerName}require', 'ecommerce', 'ecommerce.js');");
        foreach ($collection as $order) {
            if ($order->getIsVirtual()) {
                $address = $order->getBillingAddress();
            } else {
                $address = $order->getShippingAddress();
            }
            
            
               $result[] = sprintf("
                 ga('{$trackerName}ecommerce:addTransaction', {
                    'id': '%s',                     
                    'affiliation': '%s',   
                    'revenue': '%s',               
                    'shipping': '%s',                  
                    'tax': '%s'                     
                  });  
                 ",
                $order->getIncrementId(),
                null,
                Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionRevenueForOrder($order),
                Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionShippingForOrder($order),
                Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionTaxForOrder($order)
            );
            
            foreach ($order->getAllVisibleItems() as $item) {
                $result[] = sprintf("
                    ga('{$trackerName}ecommerce:addItem', {
                        'id': '%s',                     
                        'name': '%s',    
                        'sku': '%s',                 
                        'category': '%s',         
                        'price': '%s',                 
                        'quantity': '%s'                   
                      });
                    ",
                    $order->getIncrementId(),
                    $this->jsQuoteEscape($item->getName()),
                    $this->jsQuoteEscape($item->getSku()), 
                    null, 
                    Mage::helper('googleuniversalanalytics/ecommerce')->getItemPriceForOrder($item), 
                    round($item->getQtyOrdered())
                );
            }
            $result[] = "ga('{$trackerName}ecommerce:send');";
        }
        $guaCode = implode("\n", $result);
        
        if(!$globalAccount && Mage::helper('googleuniversalanalytics')->isGoogleAnalyticsGlobalAccountAvailable()) {
            $guaCode .= $this->_getOrdersTrackingCode(true);
        }
        return $guaCode;
    }
    
    
    public function _getProductDetailsViewCode() {
        $product = Mage::registry('current_product');
        return Mage::helper('googleuniversalanalytics/ecommerce')->getAddProductDetailsTag($product);
    }
    
    
    public function _getTimeOnPageEventCode($globalAccount=false) {
        if(Mage::helper('googleuniversalanalytics/gtm')->isActive()) {
            return Mage::helper('googleuniversalanalytics/gtm')->getTimeOnPageEventCode();
        }
        $guaCode = '';
        if($globalAccount) {
            $trackerName = Mage::helper('googleuniversalanalytics')->getGlobalTrackerName();
        } else {
            $trackerName = Mage::helper('googleuniversalanalytics')->getTrackerName();
        }
        
        $timeOnPageDelays =  Mage::helper('googleuniversalanalytics')->getTimeOnPageDelays();
      
        foreach($timeOnPageDelays as $delay) {
            $delayMs = $delay * 1000;
            $guaCode .= "setTimeout(function(){   ga('{$trackerName}send', 'event', { 'eventCategory': 'timeOnPage', 'eventAction': '{$delay} seconds', 'nonInteraction': 1});   }, {$delayMs});    " . "\n";
        }
        
        if(!$globalAccount && Mage::helper('googleuniversalanalytics')->isGoogleAnalyticsGlobalAccountAvailable()) {
            $guaCode .= $this->_getTimeOnPageEventCode(true);
        }
        return $guaCode;
    }
    
    public function _getCustomDimensionsCode($globalAccount=false) {
        if(Mage::helper('googleuniversalanalytics/gtm')->isActive()) {
            return Mage::helper('googleuniversalanalytics/gtm')->getCustomDimensionsCode();
        }
        $guaCode = '';
        $customDimensions = Mage::helper('googleuniversalanalytics')->getCustomDimensions();
        if($globalAccount) {
            $trackerName = Mage::helper('googleuniversalanalytics')->getGlobalTrackerName();
        } else {
            $trackerName = Mage::helper('googleuniversalanalytics')->getTrackerName();
        }
        foreach($customDimensions as $customDimensionIndex => $customDimensionValue) {
            $customDimensionValue = str_replace("'", "\'", $customDimensionValue);
            $guaCode .= "ga('{$trackerName}set','dimension{$customDimensionIndex}','" . $customDimensionValue . "');" . "\n";
        }
        if(!$globalAccount && Mage::helper('googleuniversalanalytics')->isGoogleAnalyticsGlobalAccountAvailable()) {
            $guaCode .= $this->_getCustomDimensionsCode(true);
        }
        return $guaCode;
    }

    protected function _toHtml()
    {
        if (!Mage::helper('googleuniversalanalytics')->isGoogleAnalyticsAvailable()) {
            return '';
        }
        
        /*if($this->getParentBlock()) {
            $parentBlockName = $this->getParentBlock()->getNameInLayout();
            if(Mage::helper('googleuniversalanalytics/gtm')->isActive() && $parentBlockName == 'head') {
                return '';
            }
            if(!Mage::helper('googleuniversalanalytics/gtm')->isActive() && $parentBlockName == 'after_body_start') {
                return '';
            }
        }*/

        return parent::_toHtml();
    }
    
    
    protected function _getReferrer() {
        if(!Mage::getStoreConfigFlag('googleuniversalanalytics/options/override_referrer')) {
            return false;
        }
        $coreSession = Mage::getSingleton('core/session');
        if(!$coreSession->getGuaHttpReferer()) {
            $coreSession->setGuaHttpReferer(Mage::helper('core/http')->getHttpReferer());
        }
        return $coreSession->getGuaHttpReferer();
    }
    
    
}
