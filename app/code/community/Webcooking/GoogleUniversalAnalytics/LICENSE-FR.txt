Vincent Enjalbert - Web Cooking
www.web-cooking.net

CE CONTRAT DE LICENCE (CONTRAT CI-APRES) EST UN CONTRAT ENTRE VOUS (LE
LICENCIÉ) ET Web Cooking (VINCENT ENJALBERT EI). LE CONTRAT S'APPLIQUE A TOUS
LES PRODUITS/LOGICIELS/SCRIPTS/SERVICES QUE VOUS ACHETEZ CHEZ WEB COOKING.

1. En achetant le Logiciel, vous reconnaissez avoir lu le présent contrat, 
   accepter le contenu de celui-ci et ses termes, et accepter d'utiliser 
   le Logiciel conformément à celui-ci.

2. Le Contrat entre légalement en vigueur à partir de la livraison du 
   Logiciel, laquelle ne pourra intervenir qu’après le paiement. Cette 
   livraison est réalisée par téléchargement en dehors de tout support 
   matériel. Le licencié demande expressément à ce que cette livraison 
   soit effectuée avant l’expiration du délai de rétractation comme le 
   stipule l’article L121-21 du code de la consommation et le licencié 
   déclare renoncer expressément à son droit de rétractation.

3. Web Cooking est le titulaire de l’ensemble des droits de propriété 
   intellectuelle relatif au Logiciel. Il ne peut être utilisé en totalité
   ou en partie en contravention avec les dispositions qui suivent.
   Toute activité qui enfreint les termes de ce Contrat constitue une violation
   des droits d'auteur et sera poursuivie conformément à la loi en vigueur. Nous nous
   réservons le droit de révoquer la licence de n'importe quel utilisateur qui
   détient une licence invalide.

4. Cette licence donne au licencié le droit d'utiliser une seule copie du Logiciel sur
   une seule instance Magento. Une licence distincte doit être achetée pour chaque 
   nouvelle installation du Logiciel. Le licencié peut néanmoins installer le logiciel
   sur des instances Magento ne générant aucun chiffre d’affaire et qui ne sont pas 
   accessibles depuis des réseau publics tel qu’internet, sans avoir à acheter une 
   nouvelle licence.
   Toute distribution du Logiciel sans consentement de la part de Web Cooking, 
   incluant une distribution non commerciale, sera considérée comme une violation 
   de la présente licence et entraînera des poursuites de la part de Web Cooking.

5. Le licencié ne peut utiliser ni le code dans son ensemble, ni une de ses parties,
   dans un autre logiciel, produit ou site internet, 

6. Le licencié ne peut pas donner, vendre, distribuer, accorder une sous-licence,
   louer ou prêter toute partie du Logiciel ou de la Documentation.
   Le licencié ne peut pas mettre à disposition le Logiciel sur un serveur qui est
   accessible via un réseau public, notamment Internet, à des fins de distribution.

7. Le licencié est tenu de conserver les informations des droits d'auteur intactes, ce
   qui inclut le rappel de cette licence dans les fichiers sources.

8.  Ce logiciel est conçu pour l'édition vierge ( = sans autre extension installée 
    que celles fournies de base par Magento) COMMUNITY de Magento. WebCooking ne 
    garantit pas le fonctionnement correct de cette extension sur une autre édition 
    de Magento exceptée l'édition COMMUNITY de Magento. 
    Web Cooking ne fournit pas de support d'extension en cas d'utilisation incorrecte 
    de l'édition.

9. Web Cooking ne pourrait être tenu pour responsable de tout dommage lié à l’utilisation 
   du logiciel par le licencié pour quelle que cause que ce soit.

10. Web Cooking ne pourrait être tenu pour responsable de toute utilisation par le licencié 
    du logiciel qui serait contraire aux lois en vigueur.
 
11. Le licencié peut résilier à tout moment ce Contrat en supprimant toutes les
    copies du Logiciel. La résiliation de ce Contrat n’engage pas WebCooking au
    remboursement du Logiciel.

12. Sans prejudice de tous dommages et intérêts auquel Web Cooking pourrait réclamer, 
    Web Cooking se réserve le droit de mettre fin à la présente licence à
    tout moment, à sa seule discrétion, si le licencié ne respecte pas ses conditions.
    La résiliation se fera par écrit. A réception de cette notification de résiliation 
    le licencié devra supprimer toutes les copies du logiciel à sa disposition.
    Web Cooking se reserve le droit de vérifier, aux frais du licencié, que toutes les 
    copies ont bien été supprimées.

13. Si le licencié continue néanmoins à utiliser le logiciel après que Web Cooking 
    lui ait fait parvenir une notification de résiliation de la présente licence,
    le licencié sera tenu au remboursement de tous les coûts (y compris les honoraires
    d’avocat) exposés par Web Cooking pour faire valoir ses droits légitimes.

14. La présente licence est soumise au droit français, et seuls les tribunaux du ressort
    de Paris sont compétents pour connaitre de tout litige relatif à la présente licence.
