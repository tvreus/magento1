<?php

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.web-cooking.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_GoogleUniversalAnalytics
 * @copyright  Copyright (c) 2011-2016 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
class Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol {
    
    
    //General
    const PROTOCOL_URL = "https://ssl.google-analytics.com/collect";//"http://www.google-analytics.com/collect";
    const PROTOCOL_VERSION = "v";
    const PROTOCOL_VERSION_VALUE = "1";
    const TRACKING_ID = "tid";
    const ANONYMIZE_IP = "aip";
    const DATA_SOURCE = "ds";
    const QUEUE_TIME = "qt";
    const CACHE_BUSTER = "z";
    
    
    //User
    const CLIENT_ID = "cid";
    const USER_ID = "uid";
    
    //Session
    const SESSION_CONTROL = "sc";
    const IP_OVERRIDE = "uip";
    const USER_AGENT_OVERRIDE = "ua";
    const GEOGRAPHICAL_OVERRIDE = "geoid";
    
    //Traffic Sources
    const DOCUMENT_REFERRER = "dr";
    const CAMPAIGN_NAME = "cn";
    const CAMPAIGN_SOURCE = "cs";
    const CAMPAIGN_MEDIUM = "cm";
    const CAMPAIGN_KEYWORD = "ck";
    const CAMPAIGN_CONTENT = "cc";
    const CAMPAIGN_ID = "ci";
    const GOOGLE_ADWORDS_ID = "gclid";
    const GOOGLE_DISPLAY_ADS_ID = "dclid";
    
    //System info
    const SCREEN_RESOLUTION = "sr";
    const VIEWPORT_SIZE = "vp";
    const DOCUMENT_ENCODING = "de";
    const SCREEN_COLORS = "sd";
    const USER_LANGUAGE = "ul";
    const JAVA_ENABLED = "je";
    const FLASH_VERSION = "fl";
    
    //Hit
    const HIT_TYPE = "t";
    const NON_INTERACTION_HIT = "ni";
    
    //Content Information
    const DOCUMENT_LOCATION_URL = "dl";
    const DOCUMENT_HOSTNAME = "dh";
    const DOCUMENT_PATH = "dp";
    const DOCUMENT_TITLE = "dt";
    const SCREEN_NAME = "cd";
    const LINK_ID = "linkid";
    
    //Event Tracking
    const EVENT_CATEGORY = "ec";
    const EVENT_ACTION = "ea";
    const EVENT_LABEL = "el";
    const EVENT_VALUE = "ev";
    
    //E-Commerce
    const TRANSACTION_ID = "ti";
    const TRANSACTION_AFFILIATION = "ta";
    const TRANSACTION_REVENUE = "tr";
    const TRANSACTION_SHIPPING = "ts";
    const TRANSACTION_TAX = "tt";
    const ITEM_NAME = "in";
    const ITEM_PRICE = "ip";
    const ITEM_QUANTITY = "iq";
    const ITEM_SKU = "ic";
    const ITEM_VARIATION = "iv";
    const CURRENCY = "cu";
    
    
    //Enhanced E-Commerce
    const PRODUCT_ID = "pr%did";
    const PRODUCT_NAME = "pr%dnm";
    const PRODUCT_BRAND = "pr%dbr";
    const PRODUCT_CATEGORY = "pr%dca";
    const PRODUCT_VARIANT = "pr%dva";
    const PRODUCT_PRICE = "pr%dpr";
    const PRODUCT_QUANTITY = "pr%dqt";
    const PRODUCT_COUPON = "pr%dcc";
    const PRODUCT_POSITION = "pr%dps";
    const PRODUCT_CUSTOM_DIMENSION = "pr%dcd%d";
    const PRODUCT_CUSTOM_METRIC = "pr%dcm%d";
    const PRODUCT_ACTION = "pa";
    const PRODUCT_ACTION_LIST = "pal";
    const TRANSACTION_COUPON = "tcc";
    const CHECKOUT_STEP = "cos";
    const CHECKOUT_STEP_OPTION = "col";
    
    
    
    
    //Application
    const APPLICATION_NAME = "an";
    const APPLICATION_VERSION = "av";
    
    
    
    
    //Custom dimensions / metrics
    const CUSTOM_DIMENSION = "cd";/*must be suffixed by a number*/
    const CUSTOM_METRIC = "cm";/*must be suffixed by a number*/
    
    
    
    
    
    
    
    
    
    protected function _getDefaultParams($storeId=null) {
        $params = array();
        $params[self::PROTOCOL_VERSION] = self::PROTOCOL_VERSION_VALUE;
        $params[self::DATA_SOURCE] = 'web';
        if(Mage::helper('googleuniversalanalytics')->isIPAnonymized($storeId)) {
            $params[self::ANONYMIZE_IP] = '1';
        }
        if(!isset($params[self::IP_OVERRIDE]) && Mage::helper('core/http')->getRemoteAddr()) {
            $params[self::IP_OVERRIDE] = Mage::helper('core/http')->getRemoteAddr();
        }
        $params[self::TRACKING_ID] = Mage::helper('googleuniversalanalytics')->getAccountId($storeId);
        if(!isset($params[self::USER_AGENT_OVERRIDE]) && Mage::getSingleton('core/session')->getGuaUserAgent()) {
            $params[self::USER_AGENT_OVERRIDE] = Mage::getSingleton('core/session')->getGuaUserAgent();
        }
        if(!isset($params[self::USER_ID]) && Mage::getSingleton('customer/session')->isLoggedIn()) {
            $params[self::USER_ID] = Mage::getSingleton('customer/session')->getCustomer()->getId();
        }
        if(!Mage::getStoreConfigFlag('googleuniversalanalytics/options/enable_user_id', $storeId)) {
            unset($params[self::USER_ID]);
        }
        if(!$storeId && !Mage::app()->getStore()->isAdmin()) {
            $storeId = Mage::app()->getStore()->getId();
        }
        if($storeId) {
            $params[self::DOCUMENT_HOSTNAME] = Mage::helper('googleuniversalanalytics')->getDocumentHost(false, $storeId);
        } 
        
        return $params;
    }
    
    protected function _parseParams($params, $storeId=null) {
        $parsedParams = $this->_getDefaultParams($storeId);
        $clientIdFound = false;
        $hitTypeFound = false;
        foreach($params as $key=>$val) {
            $parsedParams[$key] = $val;
            if($key == self::CLIENT_ID) {
                $clientIdFound = true;
            }
            if($key == self::HIT_TYPE) {
                $hitTypeFound = true;
            }
        }
        if(!$clientIdFound || !$hitTypeFound) return false;
        return $parsedParams;
    }
    
    public function canExecRequest($storeId = null) {
        return Mage::helper('googleuniversalanalytics')->isGoogleAnalyticsAvailable($storeId);
    }
    
    public function canUseGlobalAccount() {
        return Mage::helper('googleuniversalanalytics')->isGoogleAnalyticsGlobalAccountAvailable();
    }
    
    public function execRequest($params, $storeId = null, $globalAccount = false) { 
        if(!$this->canExecRequest($storeId)) {
            return false;
        }
        $params = $this->_addGlobalDimensionsAndMetrics($params);
        $params = $this->_parseParams($params, $storeId);
        if(!$params) {
            Mage::helper('googleuniversalanalytics')->log('Empty params :');
            Mage::helper('googleuniversalanalytics')->log(debug_backtrace());
            return false;
        }
        if($globalAccount) {
            $params[self::TRACKING_ID] = Mage::helper('googleuniversalanalytics')->getGlobalAccountId($storeId);
        }
        $httpClient = new Varien_Http_Client();
        try {
            $response = $httpClient->setUri(self::PROTOCOL_URL)
                            ->setParameterPost($params)
                            ->setConfig(array('timeout' => 5))
                            ->request('POST');
            Mage::helper('googleuniversalanalytics')->log('GUA Request :');
            Mage::helper('googleuniversalanalytics')->log($params);
            //Mage::helper('googleuniversalanalytics')->log($response);
        } catch(Exception $e) {
            Mage::helper('googleuniversalanalytics')->log($e->getMessage());
            return false;
        }
        if(!$globalAccount && $this->canUseGlobalAccount()) {
            $this->execRequest($params, $storeId, true);
        }
        return true;
        
    }
    
    protected function _addGlobalDimensionsAndMetrics($params) {
        $customDimensions = Mage::helper('googleuniversalanalytics')->getCustomDimensions();
        foreach($customDimensions as $customDimensionIndex => $customDimensionValue) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CUSTOM_DIMENSION . $customDimensionIndex] = $customDimensionValue;
        }
        return $params;
    }
    
    
    public function sendEvent($eventCategory, $eventAction, $eventLabel, $eventValue, $nonInteractiveMode = false, $guaClientId = false, $storeId = null, $additionalParameters=array()) {
        $clientId = $guaClientId ? $guaClientId : Mage::helper('googleuniversalanalytics')->getClientId();
        if (!$clientId) {
            return;
        }
        $params = array(
            Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CLIENT_ID => $clientId,
            Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::HIT_TYPE => 'event',
            Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::EVENT_CATEGORY => $eventCategory,
            Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::EVENT_ACTION => $eventAction,
            Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::EVENT_LABEL => $eventLabel,
        );
        
        
        
        if($eventValue !== false) {
            $eventValue = round($eventValue);
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::EVENT_VALUE] = $eventValue;
        }
        
        if ($nonInteractiveMode) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::NON_INTERACTION_HIT] = 1;
        }

        $params = array_merge($params, $additionalParameters);
        
        
        if (!isset($params[self::DOCUMENT_PATH]) && $beforeUrl = Mage::app()->getRequest()->getParam(Mage_Core_Controller_Front_Action::PARAM_NAME_URL_ENCODED)) {
            $beforePage = str_replace(Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB), '', Mage::helper('core')->urlDecode($beforeUrl));
            if(!preg_match('%^/%',$beforePage)) {
                $beforePage = '/' . $beforePage;
            }   
            $params[self::DOCUMENT_PATH] = Mage::helper('googleuniversalanalytics')->getDocumentPath($beforePage, $storeId);
            
        }
        
        if (!isset($params[self::DOCUMENT_LOCATION_URL])) {
            $beforeUrl = Mage::app()->getRequest()->getParam(Mage_Core_Controller_Front_Action::PARAM_NAME_URL_ENCODED);
            if($beforeUrl) {
                $beforePage = str_replace(Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB), '', Mage::helper('core')->urlDecode($beforeUrl));
                if(!preg_match('%^/%',$beforePage)) {
                    $beforePage = '/' . $beforePage;
                }   
                $params[self::DOCUMENT_LOCATION_URL] = preg_replace('%//+%', '/', Mage::app()->getStore($storeId)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK) . Mage::helper('googleuniversalanalytics')->getDocumentPath($beforePage, $storeId));
            } else {
                 $params[self::DOCUMENT_LOCATION_URL] = Mage::app()->getStore($storeId)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK);
            }
            
        }
        
        
        return $this->execRequest($params, $storeId);

    }
    
    public function sendPageView($page, $nonInteractiveMode = false, $guaClientId = false, $storeId = null) {
        $clientId = $guaClientId ? $guaClientId : Mage::helper('googleuniversalanalytics')->getClientId();
        if (!$clientId) {
            return;
        }
           
        $params = array(
            Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CLIENT_ID => $clientId,
            Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::HIT_TYPE => 'pageview',
            Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::DOCUMENT_PATH => Mage::helper('googleuniversalanalytics')->getDocumentPath($page, $storeId),
        );
        if ($nonInteractiveMode) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::NON_INTERACTION_HIT] = 1;
        }

        return $this->execRequest($params, $storeId);
    }
    
    public function getTransactionParams($storeId, $id, $affiliation, $revenue, $shipping, $tax, $currency, $coupon='') {
        $params = array();
        if (Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated($storeId)) {
            $params = array(
                Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::TRANSACTION_ID => $id,
                Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::TRANSACTION_AFFILIATION => $affiliation,
                Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::TRANSACTION_REVENUE => $revenue,
                Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::TRANSACTION_SHIPPING => $shipping,
                Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::TRANSACTION_TAX => $tax,
                Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION => 'purchase',
                Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CURRENCY => $currency,
            );
            if($coupon) {
                $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::TRANSACTION_COUPON] = $coupon;
            }
            if(Mage::getSingleton('core/session')->getLastListName()) {
                $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION_LIST] = Mage::getSingleton('core/session')->getLastListName();
            }
        } else {
            $params = array(
                Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::TRANSACTION_ID => $id,
                Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::TRANSACTION_AFFILIATION => $affiliation,
                Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::TRANSACTION_REVENUE => $revenue,
                Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::TRANSACTION_SHIPPING => $shipping,
                Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::TRANSACTION_TAX => $tax,
                Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CURRENCY => $currency,
            );
        }
        return $params;
    }
    
    
    public function sendTransaction($id, $affiliation, $revenue, $shipping, $tax, $currency, $guaClientId=false, $nonInteractiveMode = false, $storeId = null, $additionalParams = array()) {
        $clientId = $guaClientId ? $guaClientId : Mage::helper('googleuniversalanalytics')->getClientId();
        if (!$clientId) {
            return;
        }
        if (!Mage::getStoreConfig('googleuniversalanalytics/transactions/use_mp', $storeId)) {
            return;
        }
        $params = array(
            Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CLIENT_ID => $clientId,
            Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::HIT_TYPE => 'transaction',
        );
        
        $params = array_merge($params, $this->getTransactionParams($storeId, $id, $affiliation, $revenue, $shipping, $tax, $currency));
        
        
        if($nonInteractiveMode) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::NON_INTERACTION_HIT] = 1;
        }
            
        $params = array_merge($params, $additionalParams);
        
        return $this->execRequest($params, $storeId);
    
    }
    
    
    public function getItemParams($storeId, $itemIncrement, $itemName, $itemPrice, $itemQty, $itemSku, $itemVariation, $itemBrand, $itemCategory) {
        $itemName = $itemName;
        $itemQty = round($itemQty);
        $params = array();
        if (Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated($storeId)) {
            $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ID, $itemIncrement)] = $itemSku;
            $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_NAME, $itemIncrement)] = Mage::helper('googleuniversalanalytics')->formatData($itemName);
            $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_CATEGORY, $itemIncrement)] = Mage::helper('googleuniversalanalytics')->formatData($itemCategory);
            $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_BRAND, $itemIncrement)] = Mage::helper('googleuniversalanalytics')->formatData($itemBrand);
            $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_VARIANT, $itemIncrement)] = $itemVariation;
            $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_PRICE, $itemIncrement)] = $itemPrice;
            $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_QUANTITY, $itemIncrement)] = $itemQty;
        } else {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::ITEM_NAME] = $itemName;
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::ITEM_PRICE] = $itemPrice;
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::ITEM_QUANTITY] = $itemQty;
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::ITEM_SKU] = $itemSku;
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::ITEM_VARIATION] = $itemVariation;
        }
        
        return $params;
    }
    
    public function sendItem($id, $itemIncrement, $itemName, $itemPrice, $itemQty, $itemSku, $itemVariation, $itemBrand, $itemCategory, $currency, $guaClientId=false, $nonInteractiveMode = false, $storeId = null, $additionalParams = array()) {
        $clientId = $guaClientId ? $guaClientId : Mage::helper('googleuniversalanalytics')->getClientId($storeId);
        if (!$clientId) {
            return;
        }
        if (!Mage::getStoreConfig('googleuniversalanalytics/transactions/use_mp', $storeId)) {
            return;
        }
        
        $params = array(
            Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CLIENT_ID => $clientId,
            Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::HIT_TYPE => 'item',
            Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::TRANSACTION_ID => $id,
            Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CURRENCY => $currency,
        );
        
        $params = array_merge($params, $this->getItemParams($storeId, $itemIncrement, $itemName, $itemPrice, $itemQty, $itemSku, $itemVariation, $itemBrand, $itemCategory));
        
        
        
        if($nonInteractiveMode) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::NON_INTERACTION_HIT] = 1;
        }
            
        $params = array_merge($params, $additionalParams);
        
        return $this->execRequest($params, $storeId);
    
    }
    
    
    
    
    
}
