<?php

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.enjalbert.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_GoogleUniversalAnalytics
 * @copyright  Copyright (c) 2011-2016 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
$installer = $this;

$installer->startSetup();

foreach (array(
    'googleuniversalanalytics/mp_dimensions/product_sku'    => 'googleuniversalanalytics/dimensions/product_sku',
    'googleuniversalanalytics/mp_dimensions/product_name'    => 'googleuniversalanalytics/dimensions/product_name',
    'googleuniversalanalytics/mp_dimensions/customer_goup_id'    => 'googleuniversalanalytics/dimensions/customer_goup_id',
    'googleuniversalanalytics/mp_dimensions/customer_email'    => 'googleuniversalanalytics/dimensions/customer_email',
    'googleuniversalanalytics/mp_dimensions/customer_id'    => 'googleuniversalanalytics/dimensions/customer_id',
    'googleuniversalanalytics/mp_transactions/ecommerce'    => 'googleuniversalanalytics/transactions/use_mp',
    'googleuniversalanalytics/mp_transactions/on_invoice'    => 'googleuniversalanalytics/transactions/on_invoice',
    'googleuniversalanalytics/mp_events/customer_register'    => 'googleuniversalanalytics/events/customer_register',
    'googleuniversalanalytics/mp_events/customer_login'    => 'googleuniversalanalytics/events/customer_login',
    'googleuniversalanalytics/mp_events/customer_logout'    => 'googleuniversalanalytics/events/customer_logout',
    'googleuniversalanalytics/mp_events/checkout_add_product'    => 'googleuniversalanalytics/events/checkout_add_product',
    'googleuniversalanalytics/mp_events/checkout_remove_product'    => 'googleuniversalanalytics/events/checkout_remove_product',
    'googleuniversalanalytics/mp_events/checkout_add_coupon'    => 'googleuniversalanalytics/events/checkout_add_coupon',
    'googleuniversalanalytics/mp_events/wishlist_share'    => 'googleuniversalanalytics/events/wishlist_share',
    'googleuniversalanalytics/mp_events/wishlist_add_product'    => 'googleuniversalanalytics/events/wishlist_add_product',
    'googleuniversalanalytics/mp_events/newsletter_subscribe'    => 'googleuniversalanalytics/events/newsletter_subscribe',
    'googleuniversalanalytics/mp_events/newsletter_unsubscribe'    => 'googleuniversalanalytics/events/newsletter_unsubscribe',
    'googleuniversalanalytics/mp_transactions/transaction_event'    => 'googleuniversalanalytics/transactions/transaction_event',
    'googleuniversalanalytics/mp_dimensions/product_id'    => 'googleuniversalanalytics/dimensions/product_id',
    'googleuniversalanalytics/mp_dimensions/page_type'    => 'googleuniversalanalytics/dimensions/page_type',
    'googleuniversalanalytics/mp_dimensions/total_value'    => 'googleuniversalanalytics/dimensions/total_value',
    'googleuniversalanalytics/mp_transactions/order_status_changed'    => 'googleuniversalanalytics/transactions/order_status_changed',
    'googleuniversalanalytics/mp_transactions/transaction_revenue_inc_tax'    => 'googleuniversalanalytics/transactions/transaction_revenue_inc_tax',
    'googleuniversalanalytics/mp_transactions/transaction_revenue_inc_shipping'    => 'googleuniversalanalytics/transactions/transaction_revenue_inc_shipping',
    'googleuniversalanalytics/mp_transactions/transaction_shipping_inc_tax'    => 'googleuniversalanalytics/transactions/transaction_shipping_inc_tax',
    'googleuniversalanalytics/mp_transactions/transaction_tax_inc_shipping_tax'    => 'googleuniversalanalytics/transactions/transaction_tax_inc_shipping_tax',
    'googleuniversalanalytics/mp_transactions/transaction_item_price_inc_tax'    => 'googleuniversalanalytics/transactions/transaction_item_price_inc_tax',
    'googleuniversalanalytics/mp_transactions/use_store_currency'    => 'googleuniversalanalytics/transactions/use_store_currency',
    'googleuniversalanalytics/mp_transactions/send_children_items'    => 'googleuniversalanalytics/transactions/send_children_items',
    'googleuniversalanalytics/mp_dimensions/product_cost'    => 'googleuniversalanalytics/dimensions/product_cost',
    'googleuniversalanalytics/mp_dimensions/product_profit'    => 'googleuniversalanalytics/dimensions/product_profit',
    'googleuniversalanalytics/mp_dimensions/customer_gender'    => 'googleuniversalanalytics/dimensions/customer_gender',
    'googleuniversalanalytics/mp_dimensions/customer_name'    => 'googleuniversalanalytics/dimensions/customer_name',
    'googleuniversalanalytics/mp_dimensions/env_page'    => 'googleuniversalanalytics/dimensions/env_page',
    'googleuniversalanalytics/mp_dimensions/customer_logged_in'    => 'googleuniversalanalytics/dimensions/customer_logged_in',
    'googleuniversalanalytics/mp_dimensions/customer_postcode'    => 'googleuniversalanalytics/dimensions/customer_postcode',
    'googleuniversalanalytics/mp_dimensions/customer_orders_count'    => 'googleuniversalanalytics/dimensions/customer_orders_count',
    'googleuniversalanalytics/mp_dimensions/product_price'    => 'googleuniversalanalytics/dimensions/product_price',
    'googleuniversalanalytics/mp_events/order_status'    => 'googleuniversalanalytics/events/order_status',
    'googleuniversalanalytics/mp_dimensions/quote_id'    => 'googleuniversalanalytics/dimensions/quote_id',
    'googleuniversalanalytics/mp_dimensions/quote_grand_total'    => 'googleuniversalanalytics/dimensions/quote_grand_total',
    'googleuniversalanalytics/mp_dimensions/quote_items_count'    => 'googleuniversalanalytics/dimensions/quote_items_count',
    'googleuniversalanalytics/mp_dimensions/quote_items_qty'    => 'googleuniversalanalytics/dimensions/quote_items_qty',
    'googleuniversalanalytics/mp_dimensions/session_id'    => 'googleuniversalanalytics/dimensions/session_id',
    'googleuniversalanalytics/mp_dimensions/timestamp'    => 'googleuniversalanalytics/dimensions/timestamp',
    'googleuniversalanalytics/mp_dimensions/customer_last_order_date'    => 'googleuniversalanalytics/dimensions/customer_last_order_date',
    'googleuniversalanalytics/mp_dimensions/customer_spent_amount'    => 'googleuniversalanalytics/dimensions/customer_spent_amount',
    'googleuniversalanalytics/mp_events/compare_add_product'    => 'googleuniversalanalytics/events/compare_add_product',
    'googleuniversalanalytics/mp_events/compare_remove_product'    => 'googleuniversalanalytics/events/compare_remove_product',
    'googleuniversalanalytics/mp_dimensions/aw_points_used_points'    => 'googleuniversalanalytics/dimensions/aw_points_used_points',
    'googleuniversalanalytics/mp_events/time_on_page'    => 'googleuniversalanalytics/events/time_on_page',
    'googleuniversalanalytics/mp_dimensions/unirgy_dropship_vendor'    => 'googleuniversalanalytics/dimensions/unirgy_dropship_vendor',
    'googleuniversalanalytics/mp_dimensions/customer_group_id'    => 'googleuniversalanalytics/dimensions/customer_group_id',
    'googleuniversalanalytics/mp_dimensions/env_demo'    => 'googleuniversalanalytics/dimensions/env_demo',
    'googleuniversalanalytics/mp_dimensions/page_name'    => 'googleuniversalanalytics/dimensions/page_name',
    'googleuniversalanalytics/mp_dimensions/page_id'    => 'googleuniversalanalytics/dimensions/page_id',
    'googleuniversalanalytics/mp_dimensions/env_client_id'    => 'googleuniversalanalytics/dimensions/env_client_id',
    'googleuniversalanalytics/mp_events/checkout_estimate'    => 'googleuniversalanalytics/events/checkout_estimate',
    'googleuniversalanalytics/mp_dimensions/g_session_id'    => 'googleuniversalanalytics/dimensions/g_session_id',
    ) as $was => $become) {
    $installer->run(sprintf("UPDATE `%s` SET `path` = '%s' WHERE `path` = '%s'",
        $this->getTable('core/config_data'), $become, $was
    ));
}

$installer->endSetup(); 
 