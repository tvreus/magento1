<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Ogrid
 */


if ($columns = Mage::getStoreConfig('amogrid/general/columns')) {
    $columns = Mage::helper('amogrid')->unserialize($columns);
    Mage::getConfig()->saveConfig('amogrid/general/columns', json_encode($columns));
}
