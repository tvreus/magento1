<?php
/**
* @author Amasty Team
* @copyright Copyright (c) 2010-2012 Amasty (http://www.amasty.com)
* @package Amasty_Social
*/
class Amasty_Social_Model_Coupon 
{
    
    public function getCouponCode()
    {
        $couponData = array();
        $couponData['name']      = Mage::helper('amsocial')->__('Facebook and Twitter Promo Coupon')  . ' (' . date('Y-m-d') . ')';
        $couponData['is_active'] = 1;
        // all websites here:
        $couponData['website_ids'] =  array_keys(Mage::app()->getWebsites(true));
	$couponData['stop_rules_processing'] =  Mage::getStoreConfig('amsocial/coupon/stop_rules_processing');
        
        $couponData['coupon_type'] = 2;  // for magento 1.4.1.1
        $couponData['coupon_code'] = strtoupper(substr(uniqid(), 0, Mage::getStoreConfig('amsocial/coupon/lenght'))); 
        $maxUses = intval(Mage::getStoreConfig('amsocial/coupon/coupon_uses'));

        $couponData['uses_per_coupon']   = $maxUses;
        $couponData['uses_per_customer'] = $maxUses;
        $couponData['from_date'] = ''; //current date

        $days = intval(Mage::getStoreConfig('amsocial/coupon/coupon_days'));
        $date = date('Y-m-d', time() + $days * 24 * 3600);
        $couponData['to_date'] = $date;
        
        $couponData['simple_action']   = Mage::getStoreConfig('amsocial/coupon/coupon_type');
        $couponData['discount_amount'] = Mage::getStoreConfig('amsocial/coupon/coupon_amount');
        $couponData['conditions'] = array(
            1 => array(
                'type'       => 'salesrule/rule_condition_combine',
                'aggregator' => 'all',
                'value'      => 1,
                'new_child'  =>'', 
            )
        );
        
        $couponData['actions'] = array(
            1 => array(
                'type'       => 'salesrule/rule_condition_product_combine',
                'aggregator' => 'all',
                'value'      => 1,
                'new_child'  =>'', 
            )
        );

        //compatibility with aitoc's individual promo extension
        $couponData['customer_individ_ids'] = array();
        
        //create for all customer groups
        $couponData['customer_group_ids'] = array();
        $customerGroups = Mage::getResourceModel('customer/group_collection')
            ->load();

        $found = false;
        foreach ($customerGroups as $group) {
            if (0 == $group->getId()) {
                $found = true;
            }
            $couponData['customer_group_ids'][] = $group->getId();
        }
        if (!$found) {
            $couponData['customer_group_ids'][] = 0;
        }
        
        try { 
            Mage::getModel('salesrule/rule')
                ->loadPost($couponData)
                ->save();      
        } 
        catch (Exception $e){
            $couponData['coupon_code'] = '';   
        }
        
        if (!$couponData['coupon_code'])
        {
            return ;
        }
        return $couponData['coupon_code'];
    }
    

    
    public function removeOldCoupons()    //config.xml cron
    {
        $days = intVal(Mage::getStoreConfig('amsocial/coupon/remove_days'));
        if ($days <= 0)
            return;
            
        $rules = Mage::getResourceModel('salesrule/rule_collection')
            ->addFieldToFilter('name', array('like' => Mage::helper('amsocial')->__('Facebook and Twitter Promo Coupon') . '%'))
            ->addFieldToFilter('from_date', array('lt' => date('Y-m-d', strtotime("-$days days"))))
            ;
         
        $errors = '';       
        foreach ($rules as $rule){
            try {
                $rule->delete();
            } 
            catch (Exception $e) {
                $errors .= "\r\nError when deleting rule #" . $rule->getId() . ' : ' . $e->getMessage();    
            }
        }
        
        if ($errors)
        {
            throw new Exception($errors);
        }
    }

}