<?php

class Prisync_Pricer_Model_Observer
{
    public $isNewProduct = false;
    public $isEditable = true;
    
    public function beforeSaveProduct($observer) 
    {
        $productModel = $observer->getEvent()->getProduct();
        
        // Control product is created or updated
        if ($productModel->isObjectNew()) {
            $this->isNewProduct = true;
        }
    }
    
    public function saveProductListener($observer) 
    {
        $autoSync = Mage::getStoreConfig('pricer_options/settings/auto_sync');

        if ($this->isNewProduct && !$autoSync) return true;

        if ($this->isEditable) {
            $editProductResponse = false;
            $addProductResponse = false;

            $productId = $observer->getEvent()->getProduct()->getId();

            // Keep store ids which product is available on it
            $storeIdsOfProduct = $observer->getEvent()->getProduct()->getStoreIds();

            // Selected store from user to import products
            $storeId = Mage::getStoreConfig('pricer_options/settings/store_id');

            // Just edit product if it is available for selected store
            if ($storeId && in_array($storeId, $storeIdsOfProduct)) {
                $productModel = Mage::getModel('catalog/product')->setStoreId($storeId)->load($productId);

                $packageProductResponse = Mage::helper('prisync_pricer/util')->packageProductInfo($productModel);

                if ($this->isNewProduct) {
                    $addProductResponse =
                        Mage::helper('prisync_pricer/util')->addProduct($packageProductResponse['product']);
                } else {
                    $editProductResponse =
                        Mage::helper('prisync_pricer/util')->editProduct($packageProductResponse['product']);

                    if (!$editProductResponse && $autoSync) { // If we cannot edit product then try to add
                        $addProductResponse =
                            Mage::helper('prisync_pricer/util')->addProduct($packageProductResponse['product']);
                    }
                }
            } else {
                Mage::helper('prisync_pricer/util')->customLog(
                    array(
                        'class' => 'OBSERVER',
                        'function' => 'saveProductListener',
                        'level' => 'info',
                        'message' => 'Store ID not exist or saved product not available for store id: '.$storeId
                    )
                );
            }
            
            // If add/edit product is success, then add my url to product
            if ($editProductResponse || $addProductResponse) {
                Mage::helper('prisync_pricer/util')->addUrlToProduct($packageProductResponse['url']);
            }
        }
    }
    
    public function deleteProductListener($observer) 
    {
        $productModel = $observer->getEvent()->getProduct();
        
        $externalRef = $productModel->getId();
        
        Mage::helper('prisync_pricer/util')->deleteProduct($externalRef);
    }

    public function setSuggestedPrices() 
    {
        $setPriceAuto = Mage::getStoreConfig('pricer_options/settings/set_price_auto');
        if ($setPriceAuto === '1') {
            Mage::helper('prisync_pricer/util')->setSuggestedPrices();
        } else {
            Mage::helper('prisync_pricer/util')->customLog(
                array(
                    'class' => 'OBSERVER',
                    'function' => 'setSuggestedPrices',
                    'level' => 'info',
                    'message' => 'Cron job does not run due to set_price_auto setting.'
                )
            );
        }
    }

    // This is the heartbeat to track cron healt
    public function heartbeat()
    {
        return true;
    }

    // This is the cron handler for  extension heartbeat
    public function extensionHeartbeat()
    {
        Mage::helper('prisync_pricer/util')->extensionHeartbeat();
    }

    // This is the cron handler for extension debug
    public function extensionDebug()
    {
        Mage::helper('prisync_pricer/util')->extensionDebug();
    }
}