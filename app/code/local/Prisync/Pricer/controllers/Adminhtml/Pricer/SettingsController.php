<?php

class Prisync_Pricer_Adminhtml_Pricer_SettingsController extends Mage_Adminhtml_Controller_Action
{
    protected function _isAllowed() 
    {
        return Mage::getSingleton('admin/session')->isAllowed('pricer/pricer_settings');
    }

    public function indexAction()
    {
        $this->loadLayout();
        
        // Set menu item as active
        $this->_setActiveMenu('pricer');
        
        // Set window title
        $this->_title('Prisync - Settings');
        
        // Pass app email and app api key to template
        if (Mage::getStoreConfig('pricer_options/settings/email') 
            && Mage::getStoreConfig('pricer_options/settings/apikey')) {
            $this->getLayout()->getBlock('pricerSettings')->setAppEmail(
                Mage::getStoreConfig('pricer_options/settings/email')
            );

            $this->getLayout()->getBlock('pricerSettings')->setAppApiKey(
                Mage::getStoreConfig('pricer_options/settings/apikey')
            );

            // Send heartbeat to Prisync
            Mage::helper('prisync_pricer/util')->extensionHeartbeat();

            // Control debug flag on Prisync
            Mage::helper('prisync_pricer/util')->extensionDebug();
        }

        // Save default store id when extension installed
        if (!Mage::getStoreConfig('pricer_options/settings/store_id')) {
            Mage::getModel('core/config')->saveConfig(
                'pricer_options/settings/store_id',
                Mage::helper('prisync_pricer/util')->getDefaultStoreId()
            );
        }

        // Pass saved store id to template
        $this->getLayout()->getBlock('pricerSettings')->setSavedStoreId(
            Mage::getStoreConfig('pricer_options/settings/store_id')
        );

        // Pass store options to template
        $this->getLayout()->getBlock('pricerSettings')->setStoreOptions(
            Mage::helper('prisync_pricer/util')->getAllStoresAsOptions()
        );

        // Save set auto price setting when extension installed
        if (!Mage::getStoreConfig('pricer_options/settings/set_price_auto')) {
            Mage::getModel('core/config')->saveConfig('pricer_options/settings/set_price_auto', 0);
        }

        // Pass set auto price setting to template
        $this->getLayout()->getBlock('pricerSettings')->setPriceAuto(
            Mage::getStoreConfig('pricer_options/settings/set_price_auto')
        );
        
        // Save price attr setting when extension installed
        if (!Mage::getStoreConfig('pricer_options/settings/price_attr')) {
            Mage::getModel('core/config')->saveConfig('pricer_options/settings/price_attr', 'price');
        }

        // Pass price attr setting to template
        $this->getLayout()->getBlock('pricerSettings')->setPriceAttr(
            Mage::getStoreConfig('pricer_options/settings/price_attr')
        );

        // Save auto-sync setting when extension installed
        if (!Mage::getStoreConfig('pricer_options/settings/auto_sync')) {
            Mage::getModel('core/config')->saveConfig('pricer_options/settings/auto_sync', 0);
        }

        // Pass auto-sync setting to template
        $this->getLayout()->getBlock('pricerSettings')->setAutoSync(
            Mage::getStoreConfig('pricer_options/settings/auto_sync')
        );

        $checkCronJob = Mage::helper('prisync_pricer/util')->checkCronJob();
        if (!$checkCronJob) {
            Mage::getSingleton('adminhtml/session')->addError(
                'Please check your cron configuration. 
                See detailed info 
                <a href="http://bit.ly/install-cron-on-magento" 
                target="_blank">here</a>'
            );
            $this->_initLayoutMessages('adminhtml/session'); // Show added notice immediately
        }
        
        if (Mage::getStoreConfig('pricer_options/settings/sync_status')) {
            Mage::getSingleton('adminhtml/session')->addSuccess(
                "Prisync successfully set your products' prices."
            );
            $this->_initLayoutMessages('adminhtml/session'); // Show added notice immediately
            
            Mage::getModel('core/config')->saveConfig('pricer_options/settings/sync_status', false);
        }
        
        $this->renderLayout();
    }
    
    public function saveCredentialAction() 
    {
        $response = $error = array();
        try {
            $appEmail = $this->getRequest()->getPost('app_email');
            $appKey = $this->getRequest()->getPost('app_api_key');
            
            if ($appEmail === '') {
                $error['app_email'] = 'blank';
            } else if (!Zend_Validate::is($appEmail, 'EmailAddress')) {
                $error['app_email'] = 'not_validated';
            }
            
            if ($appKey === '') {
                $error['app_api_key'] = 'blank';
            } else if (!Zend_Validate::is($appKey, 'Alnum')) {
                $error['app_api_key'] = 'not_validated';
            } else if (!Zend_Validate::is($appKey, 'StringLength', array(32, 32))) {
                $error['app_api_key'] = 'length_issue';
            }
            
            if (count($error) == 0) {
                //If all fields filled correctly, validate credential
                $validateCredentialResponse = 
                                Mage::helper('prisync_pricer/util')->validateCredential($appEmail, $appKey);
                if ($validateCredentialResponse['status'] === 0) {
                    $error = $validateCredentialResponse['error'];
                }
            }
            
            if (count($error) == 0) {
                Mage::getModel('core/config')->saveConfig('pricer_options/settings/email', $appEmail);
                Mage::getModel('core/config')->saveConfig('pricer_options/settings/apikey', $appKey);
                
                // Clean config cache to use saved email and apikey
                $type = "config";
                Mage::app()->getCacheInstance()->cleanType($type);
                Mage::dispatchEvent('adminhtml_cache_refresh_type', array('type' => $type));

                $response['status'] = 1;
                $response['message'] = 'Email and API Key successfully saved.';
                $response['redirect_uri'] = $this->getUrl('adminhtml/pricer_index/index');

                Mage::helper('prisync_pricer/util')->customLog(
                    array(
                        'class' => 'SETTINGS_CONTROLLER',
                        'function' => 'saveCredential',
                        'level' => 'info',
                        'message' => 'Detail: '.$response['message']
                    )
                );
            } else {
                $response['status'] = 0;
                $response['error'] = $error;

                Mage::helper('prisync_pricer/util')->customLog(
                    array(
                        'class' => 'SETTINGS_CONTROLLER',
                        'function' => 'saveCredential',
                        'level' => 'error',
                        'message' => 'Error occurred while saving credentials! - Detail: '.$response['error']
                    )
                );
            }
        } catch (Exception $e) {
            $response['status'] = 0;
            $response['error'] = 'An error is occurred, please try again.';

            Mage::helper('prisync_pricer/util')->customLog(
                array(
                    'class' => 'SETTINGS_CONTROLLER',
                    'function' => 'saveCredential',
                    'level' => 'error',
                    'message' => 'Error occurred while saving credentials! - Detail: '.$e->getMessage()
                )
            );
        }
        
        Mage::helper('prisync_pricer/util')->returnAjax($response);
    }

    public function saveSettingsAction() 
    {
        $response = $error = array();
        try {
            $setPriceAuto = $this->getRequest()->getPost('set_price_auto');
            $storeId = $this->getRequest()->getPost('set_store_id');
            $priceAttr = $this->getRequest()->getPost('set_price_attr');
            $autoSync = $this->getRequest()->getPost('set_auto_sync');

            if ($setPriceAuto === '') {
                $error['set_price_auto'] = 'blank';
            }

            if ($storeId === '') {
                $error['set_store_id'] = 'blank';
            }

            if ($priceAttr === '') {
                $error['set_price_attr'] = 'blank';
            }

            if ($autoSync === '') {
                $error['set_auto_sync'] = 'blank';
            }

            if (count($error) == 0) {
                Mage::getModel('core/config')->saveConfig('pricer_options/settings/set_price_auto', $setPriceAuto);
                Mage::getModel('core/config')->saveConfig('pricer_options/settings/store_id', $storeId);
                Mage::getModel('core/config')->saveConfig('pricer_options/settings/price_attr', $priceAttr);
                Mage::getModel('core/config')->saveConfig('pricer_options/settings/auto_sync', $autoSync);

                // Clean config cache to use saved settings
                $type = "config";
                Mage::app()->getCacheInstance()->cleanType($type);
                Mage::dispatchEvent('adminhtml_cache_refresh_type', array('type' => $type));

                $response['status'] = 1;
                $response['message'] = 'Settings are successfully saved.';
                $response['redirect_uri'] = $this->getUrl('adminhtml/pricer_index/index');

                Mage::helper('prisync_pricer/util')->customLog(
                    array(
                        'class' => 'SETTINGS_CONTROLLER',
                        'function' => 'saveSettings',
                        'level' => 'info',
                        'message' => 'Detail: '.$response['message']
                    )
                );
            } else {
                $response['status'] = 0;
                $response['error'] = $error;

                Mage::helper('prisync_pricer/util')->customLog(
                    array(
                        'class' => 'SETTINGS_CONTROLLER',
                        'function' => 'saveSettings',
                        'level' => 'error',
                        'message' => 'Error occurred while saving settings! - Detail: '.$response['error']
                    )
                );
            }
        } catch (Exception $e) {
            $response['status'] = 0;
            $response['error'] = 'An error is occurred, please try again.';

            Mage::helper('prisync_pricer/util')->customLog(
                array(
                    'class' => 'SETTINGS_CONTROLLER',
                    'function' => 'saveSettings',
                    'level' => 'error',
                    'message' => 'Error occurred while saving settings! - Detail: '.$e->getMessage()
                )
            );
        }

        Mage::helper('prisync_pricer/util')->returnAjax($response);
    }
}
