<?php
class Sisow_Model_Observer_Sendebill
{
	public function sendEbill(Varien_Event_Observer $observer)
	{
		$order = $observer->getEvent()->getOrder();
		
		if( $order->getPayment()->getMethodInstance()->getCode() == 'sisow_ebill')
		{
			$arg = array();
			$base = Mage::getModel('sisow/base');
			
			$base->payment = 'ebill';
			
			$arg['billing_firstname'] = $order->getBillingAddress()->getFirstname();
			$arg['billing_lastname'] = $order->getBillingAddress()->getLastname();
			$arg['billing_mail'] = $order->getBillingAddress()->getEmail();
			$arg['testmode'] = (Mage::getStoreConfig('payment/sisow_ebill/testmode')) ? 'true' : 'false';
			$arg['days'] = Mage::getStoreConfig('payment/sisow_ebill/days');
			
			$base->amount = round($order->getGrandTotal(), 2);
			$base->purchaseId = $order->getCustomerId() . $order->getRealOrderId();
			$base->entranceCode = $order->getRealOrderId();
			$base->description = $order->getRealOrderId();
			
			$base->notifyUrl = Mage::getUrl('sisow/checkout/notify', array('_secure' => true));
			$base->returnUrl = Mage::getUrl('sisow/checkout/return', array('_secure' => true));
			
			if( ($ex = $base->TransactionRequest($arg)) < 0)
			{
				Mage::getSingleton('adminhtml/session')->addError( 'Sisow error: ' . $ex . ', ' . $base->errorCode );
				return $this;
			}	
			
			$payment = $order->getPayment();
			$comm = 'Sisow Ebill created.<br />';
			$comm .= 'Transaction ID: ' . $base->trxId . '<br/>';
			$st = Mage_Sales_Model_Order::STATE_PENDING_PAYMENT;
			$payment->setAdditionalInformation('trxId', $base->trxId)
				->setAdditionalInformation('documentId', $base->documentId)
				->setAdditionalInformation('linkPdf', $base->GetLink(''))
				->save();
			$order->setState($st, $st, $comm);
			$order->save();
							
			$order->setState(Mage_Sales_Model_Order::STATE_PENDING_PAYMENT);
			$order->getPayment()->setAdditionalInformation('trxId', $base->trxId)->save();
				
			$transaction = Mage::getModel('sales/order_payment')
						->setMethod('sisow_'.$method)
						->setTransactionId($base->trxId)
						->setIsTransactionClosed(false);
						
			$order->setPayment($transaction);
			$transaction->addTransaction(Mage_Sales_Model_Order_Payment_Transaction::TYPE_AUTH);	
			$order->save();	
			
			Mage::getSingleton('adminhtml/session')->addSuccess( 'The Ebill has been created and send to the customer.' );
		}
		
		return $this;
	}
}
?>