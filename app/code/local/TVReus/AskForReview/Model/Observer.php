<?php
class TVReus_AskForReview_Model_Observer
{
    public function saveCustomData($event)
    {
        $quote = $event->getSession()->getQuote();
        $quote->setData('ask_for_review', $event->getRequestModel()->getPost('ask_for_review'));

        return $this;
    }
}