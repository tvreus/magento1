<?php
$installer = $this;
$installer->startSetup();

$installer->addAttribute("order", "ask_for_review", array("type"=>"boolean"));
$installer->addAttribute("quote", "ask_for_review", array("type"=>"boolean"));
$installer->endSetup();