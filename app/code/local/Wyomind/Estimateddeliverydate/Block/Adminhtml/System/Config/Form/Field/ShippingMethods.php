<?php

class Wyomind_Estimateddeliverydate_Block_Adminhtml_System_Config_Form_Field_ShippingMethods extends Mage_Adminhtml_Block_System_Config_Form_Field
{

    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $component = $element->getHtmlId();
        $html = "";

        $html .= "<input class=' input-text'  type='hidden' id='" . $element->getHtmlId() . "' name='" . $element->getName() . "' value='" . $element->getEscapedValue() . "' '" . $element->serialize($element->getHtmlAttributes()) . "/>";

        $html .= '<div class="grid" style="width:850px">
                    <div class="hor-scroll">
                        <table cellspacing="0" id="' . $component . '_table" class="data">
                            <thead>
                                <tr class="headings">
                                    <th class="a-center" width="250">Shipping method code</th>
                                    <th class="a-center" width="250">Leadtime</th>
                                    <th class="a-center" width="250">Message</th>
                                    <th class="a-center" width="100"><span class="nobr"></span></th>
                                </tr>
                            </thead>
                            <tbody id="' . $component . '_body">
                                ';
        $data = json_decode($element->getValue());
        if (!is_array($data))
            $data = array();
        foreach ($data as $row) {
            $html .= '<tr title="#" class="pointer ' . $component . '_row">'
                    . '<td class="a-center"><input onchange="' . $component . '_update()" class="input-text" style="width:95%" value="' . $row->code . '"/></td>'
                    . '<td class="a-center"><input onchange="' . $component . '_update()" class="input-text" style="width:95%" value="' . $row->leadtime . '"/></td>'
                    . '<td class="a-center"><textarea onchange="' . $component . '_update()" class="input-text" style="width:95%;height:60px">' . $row->message . '</textarea></td>'
                    . '<td>'
                    . '<button class="scalable delete" type="button" onclick="' . $component . '_delete(this);">
                        <span>
                            <span>
                                <span>Delete</span>
                            </span>
                        </span>
                       </button>'
                    . '</td>'
                    . '</tr>';
        }

        $shippingMethodList = "<option>Select a shipping method...</option>";

        $shippingMethods = array();
        $activeCarriers = Mage::getSingleton('shipping/config')->getActiveCarriers();

        foreach ($activeCarriers as $carrierCode => $carrier) {
            $methodOptions = array();
            $methods = $carrier->getAllowedMethods();
            if ($methods) {
                foreach ($methods as $methodCode => $method) {
                    $code = $carrierCode . '_' . $methodCode;
                    $methodOptions[$code] = array('value' => $code, 'label' => $method);
                }

                if (!$carrierTitle = Mage::getStoreConfig('carriers/' . $carrierCode . '/title')) {
                    $carrierTitle = $carrierCode;
                }

                $shippingMethods[] = array('value' => $methodOptions, 'label' => $carrierTitle);
            }
        }



        foreach ($shippingMethods as $key => $shippingMethod) {

            $shippingMethodList .= '<optgroup label="' . $shippingMethod['label'] . '">';
            foreach ($shippingMethod['value'] as $allowedMethod) {

                $shippingMethodList .= '<option value="'.$allowedMethod['value'].'">' . $allowedMethod['label'] . '</option>';
            }
            $shippingMethodList .='</optgroup>';
        }


        $html .= '<tr id="' . $component . '_add"">
                    
                    <td colspan="4" align="right">
                    <select id="'. $component . '_method">
                    ' . $shippingMethodList . '
                    </select>
                    <button class="scalable add" type="button" onclick="' . $component . '_add()" style="">
                    <span>
                        <span>
                            <span>Add a new method</span>
                        </span>
                    </span>
                   </button>
                   </td>
                   </tr>
                </tbody>
            </table>
            </div>
            </div>';

        $html .= '<script type="text/javascript" language="javascript">';
        $html .= '
            var values = ' . ($element->getValue() != "" ? $element->getValue() : "[]") . ';
             
               function ' . $component . '_delete(button) {
                    button.up("tr").remove();
                   ' . $component . '_update();
                }
                function ' . $component . '_add() {
                
                   tr=Builder.node("TR",{"class":"' . $component . '_row"},[
                        Builder.node("TD",{"align":"center"},[
                            Builder.node("INPUT",{
                               "type":"text",
                               "class":"input-text",
                               "style":"width:95%",
                               "value":$('. $component . '_method).value,
                               "onchange":"' . $component . '_update()"
                           }),
                        ]),
                        Builder.node("TD",{"align":"center"},[
                            Builder.node("INPUT",{
                               "type":"text",
                               "class":"input-text",
                              "style":"width:95%",
                               "onchange":"' . $component . '_update()"
                           }),
                        ]),
                        Builder.node("TD",{"align":"center"},[
                            Builder.node("textarea",{
                              "class":"input-text",
                               "style":"width:95%;height:60px",
                               "onchange":"' . $component . '_update()"
                           }),
                        ]),
                        Builder.node("TD",[
                            Builder.node("BUTTON",{
                               "class":"scalable delete",
                               "type":"button",
                               "onclick":"' . $component . '_delete(this);"    
                             },[
                                Builder.node("SPAN",[
                                    Builder.node("SPAN",[
                                         Builder.node("SPAN",[
                                            " Delete "
                                         ])
                                    ])
                                ])
                           ]), 
                            
                        ]),
                        
                    ])
                   
                    $('. $component . '_method).value=null;
                  
                    $("' . $component . '_add").insert({before:tr}); 

                }
               function  ' . $component . '_update() {
                    var data = new Array();
                    $$(".' . $component . '_row").each(function(row) {
                        code=row.select("INPUT")[0].value;
                        leadtime=row.select("INPUT")[1].value;
                        message=row.select("TEXTAREA")[0].value;
                        data.push({code:code,leadtime:leadtime,message:message});
                    });
                                      
                    console.log(data);
                    $("' . $element->getHtmlId() . '").value = Object.toJSON(data);
                }
            
            ';
        $html .= '</script>';

        return $html;
    }

}
