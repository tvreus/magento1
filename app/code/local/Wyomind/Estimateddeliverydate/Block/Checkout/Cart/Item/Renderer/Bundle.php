<?php

/* *
 * Copyright © 2016 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

class Wyomind_Estimateddeliverydate_Block_Checkout_Cart_Item_Renderer_Bundle extends Mage_Bundle_Block_Checkout_Cart_Item_Renderer
{

    /**
     * Return cart item error messages
     *
     * @return array
     */
    public function getMessages()
    {
        $messages = array();
        $quoteItem = $this->getItem();

        // Add basic messages occuring during this page load
        $baseMessages = $quoteItem->getMessage(false);
        if ($baseMessages) {
            foreach ($baseMessages as $message) {
                $messages[] = array(
                    'text' => $message,
                    'type' => $quoteItem->getHasError() ? 'error' : 'notice'
                );
            }
        }
        

        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $sfqi = $resource->getTableName('sales_flat_quote_item');
        $query = 'SELECT product_id,qty FROM ' . $sfqi . " WHERE parent_item_id = " . $quoteItem->getId();
        $results = $readConnection->fetchAll($query);
        $storeId = Mage::app()->getStore()->getId();
        $leadtimes = array();
        $stockStatus = true;
        foreach ($results as $item) {
            $product = Mage::getModel("catalog/product")->load($item['product_id']);
            if ($product) {
                $stockStatus &= Mage::helper("estimateddeliverydate")->getStockStatus($product, $storeId, $item['qty']);
                $leadtimes[] = Mage::helper("estimateddeliverydate")->getLeadTimes($product, $storeId, 0, 0, $item['qty']);
            }
        }
        
        $range = Mage::helper("estimateddeliverydate")->getLeadTimeRange($leadtimes);

        $text = strip_tags(Mage::helper("estimateddeliverydate")->renderGlobalMessage($range, $storeId,'item_cart_bundle',false,$stockStatus?"orders":"backorders"));

        if ($text) {
            $messages[] = array(
                'text' => $text,
                'type' => "success"
            );
        }
        
        
        
        return $messages;
    }

}
