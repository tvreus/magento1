<?php

/*
 * Copyright © 2016 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */
if (Mage::helper('core')->isModuleEnabled('Best4Mage_ConfigurableProductsSimplePrices')) {
    /**
     * Fix potential conflict with Best4Mage_ConfigurableProductsSimplePrices
     * Class Wyomind_Estimateddeliverydate_Block_Checkout_Cart_Item_Renderer_Configurable_Abstract
     */
    class Wyomind_Estimateddeliverydate_Block_Checkout_Cart_Item_Renderer_Configurable_Abstract
        extends Best4Mage_ConfigurableProductsSimplePrices_Block_Cart_Item_Configurable
    {
    }
} else {
    /**
     * If Best4Mage_ConfigurableProductsSimplePrices doesn't exist then use the classname
     * Class Wyomind_Estimateddeliverydate_Block_Checkout_Cart_Item_Renderer_Configurable_Abstract
     */
    class Wyomind_Estimateddeliverydate_Block_Checkout_Cart_Item_Renderer_Configurable_Abstract
        extends Mage_Checkout_Block_Cart_Item_Renderer_Configurable
    {
    }
}

/**
 * Class Wyomind_Estimateddeliverydate_Block_Checkout_Cart_Item_Renderer_Configurable
 */
class Wyomind_Estimateddeliverydate_Block_Checkout_Cart_Item_Renderer_Configurable
    extends Wyomind_Estimateddeliverydate_Block_Checkout_Cart_Item_Renderer_Configurable_Abstract
{

    /**
     * @return array
     * @throws Mage_Core_Model_Store_Exception
     */
    public function getMessages()
    {

        $messages = array();
        $quoteItem = $this->getItem();

        // Add basic messages occuring during this page load
        $baseMessages = $quoteItem->getMessage(false);
        if ($baseMessages) {
            foreach ($baseMessages as $message) {
                $messages[] = array(
                    'text' => $message,
                    'type' => $quoteItem->getHasError() ? 'error' : 'notice'
                );
            }
        }

        // Add messages saved previously in checkout session
        $checkoutSession = $this->getCheckoutSession();
        if ($checkoutSession) {
            /* @var $collection Mage_Core_Model_Message_Collection */
            $collection = $checkoutSession->getQuoteItemMessages($quoteItem->getId(), true);
            if ($collection) {
                $additionalMessages = $collection->getItems();
                foreach ($additionalMessages as $message) {
                    /* @var $message Mage_Core_Model_Message_Abstract */
                    $messages[] = array(
                        'text' => $message->getCode(),
                        'type' => ($message->getType() == Mage_Core_Model_Message::ERROR) ? 'error' : 'notice'
                    );
                }
            }
        }


        $storeId = Mage::app()->getStore()->getId();
        $options = Mage::helper("catalog/product_configuration")->getCustomOptions($quoteItem);
        $product = $this->getChildProduct();
        if (!$product) {
            $text = Mage::helper("estimateddeliverydate")->__("Child item not found");
        } else {
            $product = Mage::getModel("catalog/product")->load($product->getId());
            $additional = Mage::helper("estimateddeliverydate")->getCustomOptions($options, $storeId);
            $text = strip_tags(Mage::helper("estimateddeliverydate")->renderMessage($product, $storeId, "item_cart", $additional[0], $additional[1], $quoteItem->getQty()));
        }
        if ($text) {
            $messages[] = array(
                'text' => $text,
                'type' => "success"
            );
        }

        return $messages;
    }

}
