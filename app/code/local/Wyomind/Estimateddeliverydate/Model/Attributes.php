<?php

class Wyomind_Estimateddeliverydate_Model_Attributes extends Mage_Core_Model_Abstract
{

    protected function _construct()
    {


        $this->_init('estimateddeliverydate/attributes');
    }

    public function getLeadTimes($attribute_id, $option_id)
    {
        $collection = $this->getCollection();
        return $collection
                        ->addFieldToFilter('value_id', array('eq' => $option_id))
                        ->addFieldToFilter('attribute_id', array('eq' => $attribute_id))
                        ->getFirstItem()->getValue();
    }

}
