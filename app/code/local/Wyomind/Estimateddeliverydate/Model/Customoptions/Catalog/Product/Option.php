<?php

class Wyomind_Estimateddeliverydate_Model_Customoptions_Catalog_Product_Option extends MageWorx_CustomOptions_Model_Catalog_Product_Option
{

    protected function _afterSave()
    {
        if (!Mage::helper('mageworx_customoptions')->isEnabled() || (Mage::app()->getRequest()->getControllerName() != 'catalog_product' && Mage::app()->getRequest()->getControllerName() != 'adminhtml_catalog_product')) {
            return parent::_afterSave();
        }

        $optionId = $this->getData('option_id');
        $defaultArray = $this->getData('default') ? $this->getData('default') : array();
        $tablePrefix = (string) Mage::getConfig()->getTablePrefix();
        $connection = Mage::getSingleton('core/resource')->getConnection('core_write');

        $helper = Mage::helper('mageworx_customoptions');

        $storeId = $this->getProduct()->getStoreId();
        if (is_array($this->getData('values'))) {
            $values = array();
            foreach ($this->getData('values') as $key => $value) {
                if (isset($value['option_type_id'])) {

                    if (isset($value['dependent_ids']) && $value['dependent_ids'] != '') {
                        $dependentIds = array();
                        $dependentIdsTmp = explode(',', $value['dependent_ids']);
                        foreach ($dependentIdsTmp as $d_id) {
                            if ($this->decodeViewIGI($d_id) > 0)
                                $dependentIds[] = $this->decodeViewIGI($d_id);
                        }
                        $value['dependent_ids'] = implode(',', $dependentIds);
                    }

                    $value['sku'] = trim($value['sku']);

                    // prepare customoptions_qty
                    $customoptionsQty = NULL;
                    if (isset($value['customoptions_qty']) && (!$helper->isSkuQtyLinkingEnabled() || $helper->getProductIdBySku($value['sku']) == 0)) {
                        $customoptionsQty = $value['customoptions_qty'];
                        if (!is_numeric($customoptionsQty))
                            $customoptionsQty = NULL;
                    }

                    $extra = '';
                    if (isset($value['extra']) && (!$helper->isSkuQtyLinkingEnabled() || $helper->getProductIdBySku($value['sku']) == 0)) {
                        $extra = strtolower(trim($value['extra']));
                        if (substr($extra, 0, 1) != 'x' && substr($extra, 0, 1) != 'i' && substr($extra, 0, 1) != 'l')
                            $extra = '';
                        if (substr($extra, 0, 1) == 'i')
                            $extra = $this->decodeViewIGI($extra);
                    }

                    $customoptionsMinQty = NULL;
                    if (isset($value['customoptions_min_qty'])) {
                        $customoptionsMinQty = $value['customoptions_min_qty'];
                        if (!is_numeric($customoptionsMinQty))
                            $customoptionsMinQty = NULL;
                    }

                    $customoptionsMaxQty = NULL;
                    if (isset($value['customoptions_max_qty'])) {
                        $customoptionsMaxQty = $value['customoptions_max_qty'];
                        if (!is_numeric($customoptionsMaxQty))
                            $customoptionsMaxQty = NULL;
                    }

                    $optionValue = array(
                        'option_id' => $optionId,
                        'sku' => $value['sku'],
                        'sort_order' => $value['sort_order'],
                        'customoptions_qty' => $customoptionsQty,
                        'extra' => $extra,
                        'leadtime' => $value['leadtime'],
                        'customoptions_min_qty' => $customoptionsMinQty,
                        'customoptions_max_qty' => $customoptionsMaxQty,
                        'default' => array_search($key, $defaultArray) !== false ? 1 : 0,
                        'in_group_id' => $value['in_group_id']
                    );
                    if (isset($value['dependent_ids']))
                        $optionValue['dependent_ids'] = $value['dependent_ids'];
                    if (isset($value['weight']))
                        $optionValue['weight'] = $value['weight'];
                    if (isset($value['cost']))
                        $optionValue['cost'] = $value['cost'];
                    if (isset($value['upc']))
                        $optionValue['upc'] = $value['upc'];
                     $optionValue['leadtime'] = $value['leadtime'];

                    $optionTypePriceId = 0;

                    if ($helper->isSkuNameLinkingEnabled() && (!isset($value['scope']['title']) || $value['scope']['title'] != 1) && (!isset($value['title']) || $value['title'] == '') && $value['sku']) {
                        $value['title'] = $helper->getProductNameBySku($value['sku'], $storeId);
                    }

                    if (isset($value['option_type_id']) && $value['option_type_id'] > 0) {
                        $optionTypeId = $value['option_type_id'];
                        if ($value['is_delete'] == '1') {
                            $connection->delete($tablePrefix . 'catalog_product_option_type_value', 'option_type_id = ' . $optionTypeId);
                            $helper->deleteOptionFile(null, $optionId, $optionTypeId);
                        } else {
                            $connection->update($tablePrefix . 'catalog_product_option_type_value', $optionValue, 'option_type_id = ' . $optionTypeId);

                            // update or insert price
                            $select = $connection->select()->from($tablePrefix . 'catalog_product_option_type_price', array('option_type_price_id'))->where('option_type_id = ' . $optionTypeId . ' AND `store_id` = ' . $storeId);
                            $optionTypePriceId = $isUpdate = $connection->fetchOne($select);
                            if (isset($value['price']) && isset($value['price_type'])) {
                                $priceValue = array('price' => $value['price'], 'price_type' => $value['price_type']);
                                if ($isUpdate) {
                                    $connection->update($tablePrefix . 'catalog_product_option_type_price', $priceValue, 'option_type_id = ' . $optionTypeId . ' AND `store_id` = ' . $storeId);
                                } else {
                                    $priceValue['option_type_id'] = $optionTypeId;
                                    $priceValue['store_id'] = $storeId;
                                    $connection->insert($tablePrefix . 'catalog_product_option_type_price', $priceValue);
                                    $optionTypePriceId = $connection->lastInsertId($tablePrefix . 'catalog_product_option_type_price');
                                }
                            } elseif (isset($value['scope']['price']) && $value['scope']['price'] == 1 && $isUpdate && $storeId > 0) {
                                $connection->delete($tablePrefix . 'catalog_product_option_type_price', 'option_type_id = ' . $optionTypeId . ' AND `store_id` = ' . $storeId);
                                $optionTypePriceId = -1;
                            }

                            // update or insert title
                            if ($storeId > 0) {
                                $select = $connection->select()->from($tablePrefix . 'catalog_product_option_type_title', array('COUNT(*)'))->where('option_type_id = ' . $optionTypeId . ' AND `store_id` = ' . $storeId);
                                $isUpdate = $connection->fetchOne($select);
                            } else {
                                $isUpdate = 1;
                            }

                            if (isset($value['title'])) {
                                if ($isUpdate) {
                                    $connection->update($tablePrefix . 'catalog_product_option_type_title', array('title' => $value['title']), 'option_type_id = ' . $optionTypeId . ' AND `store_id` = ' . $storeId);
                                } else {
                                    $connection->insert($tablePrefix . 'catalog_product_option_type_title', array('option_type_id' => $optionTypeId, 'store_id' => $storeId, 'title' => $value['title']));
                                }
                            } elseif (isset($value['scope']['title']) && $value['scope']['title'] == 1 && $isUpdate && $storeId > 0) {
                                $connection->delete($tablePrefix . 'catalog_product_option_type_title', 'option_type_id = ' . $optionTypeId . ' AND `store_id` = ' . $storeId);
                            }

                            // update or insert description
                            if (isset($value['description']) || isset($value['scope']['description'])) {
                                $select = $connection->select()->from($tablePrefix . 'mageworx_custom_options_option_type_description', array('COUNT(*)'))->where('option_type_id = ' . $optionTypeId . ' AND `store_id` = ' . $storeId);
                                $isUpdate = $connection->fetchOne($select);
                            }
                            if (isset($value['description']) && $value['description'] != '') {
                                if ($isUpdate) {
                                    $connection->update($tablePrefix . 'mageworx_custom_options_option_type_description', array('description' => $value['description']), 'option_type_id = ' . $optionTypeId . ' AND `store_id` = ' . $storeId);
                                } else {
                                    $connection->insert($tablePrefix . 'mageworx_custom_options_option_type_description', array('option_type_id' => $optionTypeId, 'store_id' => $storeId, 'description' => $value['description']));
                                }
                            } elseif ((isset($value['scope']['description']) && $value['scope']['description'] == 1 && $isUpdate && $storeId > 0) || (isset($value['description']) && $value['description'] == '')) {
                                $connection->delete($tablePrefix . 'mageworx_custom_options_option_type_description', 'option_type_id = ' . $optionTypeId . ' AND `store_id` = ' . $storeId);
                            }
                        }
                    } else {
                        if ($value['is_delete'] == '1')
                            continue;
                        $connection->insert($tablePrefix . 'catalog_product_option_type_value', $optionValue);
                        $optionTypeId = $connection->lastInsertId($tablePrefix . 'catalog_product_option_type_value');
                        if (isset($value['price']) && isset($value['price_type'])) {
                            // save not default
                            //if ($storeId>0) $connection->insert($tablePrefix . 'catalog_product_option_type_price', array('option_type_id' =>$optionTypeId, 'store_id'=>$storeId, 'price' => $value['price'], 'price_type' => $value['price_type']));
                            // save default
                            $connection->insert($tablePrefix . 'catalog_product_option_type_price', array('option_type_id' => $optionTypeId, 'store_id' => 0, 'price' => $value['price'], 'price_type' => $value['price_type']));
                            $optionTypePriceId = $connection->lastInsertId($tablePrefix . 'catalog_product_option_type_price');
                        }
                        if (isset($value['title'])) {
                            // save default
                            $connection->insert($tablePrefix . 'catalog_product_option_type_title', array('option_type_id' => $optionTypeId, 'store_id' => 0, 'title' => $value['title']));
                        }

                        if (isset($value['description']) && $value['description'] != '') {
                            // save default
                            $connection->insert($tablePrefix . 'mageworx_custom_options_option_type_description', array('option_type_id' => $optionTypeId, 'store_id' => 0, 'description' => $value['description']));
                        }
                    }

                    if ($optionTypeId > 0 && $optionTypePriceId >= 0) {
                        $id = $this->getData('id');

                        $this->_uploadImage('file_' . $id . '_' . $key, $optionId, $optionTypeId, $value);

                        // check $optionTypePriceId
                        if ($optionTypePriceId == 0) {
                            $select = $connection->select()->from($tablePrefix . 'catalog_product_option_type_price', array('option_type_price_id'))->where('option_type_id = ' . $optionTypeId . ' AND `store_id` = ' . $storeId);
                            $optionTypePriceId = $isUpdate = $connection->fetchOne($select);
                        }
                        if ($optionTypePriceId) {

                            // save special prices
                            if (isset($value['specials']) && is_array($value['specials'])) {
                                $specials = array();
                                foreach ($value['specials'] as $special) {
                                    if ($special['is_delete'] == '1' || isset($specials[$special['customer_group_id']])) {
                                        if ($special['special_price_id'] > 0)
                                            $connection->delete($tablePrefix . 'mageworx_custom_options_option_type_special_price', 'option_type_special_price_id = ' . intval($special['special_price_id']));
                                        continue;
                                    }
                                    $specials[$special['customer_group_id']] = $special;
                                }
                                if (count($specials) > 0) {
                                    foreach ($specials as $special) {
                                        $zendDate = new Zend_Date();
                                        $dateFormat = Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
                                        if ($special['date_from'])
                                            $special['date_from'] = $zendDate->setDate($special['date_from'], $dateFormat)->toString(Varien_Date::DATE_INTERNAL_FORMAT);
                                        else
                                            $special['date_from'] = null;
                                        if ($special['date_to'])
                                            $special['date_to'] = $zendDate->setDate($special['date_to'], $dateFormat)->toString(Varien_Date::DATE_INTERNAL_FORMAT);
                                        else
                                            $special['date_to'] = null;

                                        $specialData = array('option_type_price_id' => $optionTypePriceId,
                                            'customer_group_id' => $special['customer_group_id'],
                                            'price' => floatval($special['price']),
                                            'price_type' => $special['price_type'],
                                            'comment' => trim($special['comment']),
                                            'date_from' => $special['date_from'],
                                            'date_to' => $special['date_to'],
                                        );
                                        if ($special['special_price_id'] > 0) {
                                            $connection->update($tablePrefix . 'mageworx_custom_options_option_type_special_price', $specialData, 'option_type_special_price_id = ' . intval($special['special_price_id']));
                                        } else {
                                            $connection->insert($tablePrefix . 'mageworx_custom_options_option_type_special_price', $specialData);
                                        }
                                    }
                                }
                            }

                            // save tier prices
                            if (isset($value['tiers']) && is_array($value['tiers'])) {
                                $tiers = array();
                                foreach ($value['tiers'] as $tier) {
                                    $uniqKey = $tier['qty'] . '+' . $tier['customer_group_id'];
                                    if ($tier['is_delete'] == '1' || isset($tiers[$uniqKey])) {
                                        if ($tier['tier_price_id'] > 0)
                                            $connection->delete($tablePrefix . 'mageworx_custom_options_option_type_tier_price', 'option_type_tier_price_id = ' . intval($tier['tier_price_id']));
                                        continue;
                                    }
                                    $tiers[$uniqKey] = $tier;
                                }
                                if (count($tiers) > 0) {
                                    foreach ($tiers as $tier) {
                                        $tierData = array('option_type_price_id' => $optionTypePriceId, 'customer_group_id' => $tier['customer_group_id'], 'qty' => intval($tier['qty']), 'price' => floatval($tier['price']), 'price_type' => $tier['price_type']);
                                        if ($tier['tier_price_id'] > 0) {
                                            $connection->update($tablePrefix . 'mageworx_custom_options_option_type_tier_price', $tierData, 'option_type_tier_price_id = ' . intval($tier['tier_price_id']));
                                        } else {
                                            $connection->insert($tablePrefix . 'mageworx_custom_options_option_type_tier_price', $tierData);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    unset($value['option_type_id']);
                }

                $values[$key] = $value;
            }
            $this->setData('values', $values);
        } elseif ($this->getGroupByType($this->getType()) == self::OPTION_GROUP_SELECT) {
            Mage::throwException(Mage::helper('catalog')->__('Select type options required values rows.'));
        }

        if (version_compare($helper->getMagetoVersion(), '1.4.0', '>='))
            $this->cleanModelCache();

        Mage::dispatchEvent('model_save_after', array('object' => $this));
        if (version_compare($helper->getMagetoVersion(), '1.4.0', '>='))
            Mage::dispatchEvent($this->_eventPrefix . '_save_after', $this->_getEventData());
        return $this;
    }

    public function saveOptionValue($optionId, $value, $prevValue, $newValue, $optionTypeId = 0,
            $storeId = 0)
    {
        $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
        $tablePrefix = (string) Mage::getConfig()->getTablePrefix();

        if ($storeId == 0) {
            $optionValue = array();
            if (isset($value['sku']))
                $optionValue['sku'] = $value['sku'];
            if (isset($value['sort_order']))
                $optionValue['sort_order'] = $value['sort_order'];
            if (isset($value['customoptions_qty'])) {
                $optionValue['customoptions_qty'] = ($value['customoptions_qty'] == '') ? NULL : $value['customoptions_qty'];
            }
            if (isset($value['customoptions_min_qty'])) {
                $optionValue['customoptions_min_qty'] = ($value['customoptions_min_qty'] == '') ? NULL : $value['customoptions_min_qty'];
            }
            if (isset($value['customoptions_max_qty'])) {
                $optionValue['customoptions_max_qty'] = ($value['customoptions_max_qty'] == '') ? NULL : $value['customoptions_max_qty'];
            }
            if (isset($value['extra']))
                $optionValue['extra'] = $value['extra'];
            if (isset($value['leadtime']))
                $optionValue['leadtime'] = $value['leadtime'];
            if (isset($value['default']))
                $optionValue['default'] = $value['default'];
            if (isset($value['in_group_id']))
                $optionValue['in_group_id'] = $value['in_group_id'];
            if (isset($value['dependent_ids']))
                $optionValue['dependent_ids'] = $value['dependent_ids'];
            if (isset($value['weight']))
                $optionValue['weight'] = $value['weight'];
            if (isset($value['cost']))
                $optionValue['cost'] = $value['cost'];
            if (isset($value['upc']))
                $optionValue['upc'] = $value['upc'];

            if (count($optionValue) > 0)
                $optionValue['option_id'] = $optionId;

            if ($optionTypeId) {
                $updateFlag = true;
                unset($optionValue['option_id']);
                if (count($optionValue) > 0)
                    $connection->update($tablePrefix . 'catalog_product_option_type_value', $optionValue, 'option_type_id = ' . $optionTypeId);
            } else {
                $updateFlag = false;
                $connection->insert($tablePrefix . 'catalog_product_option_type_value', $optionValue);
                $optionTypeId = $connection->lastInsertId($tablePrefix . 'catalog_product_option_type_value');
            }
        }

        $optionTypePrice = array();
        if (isset($value['price']))
            $optionTypePrice['price'] = $value['price'];
        if (isset($value['price_type']))
            $optionTypePrice['price_type'] = $value['price_type'];

        // for support old format:
        if (isset($value['special_price']) && $value['special_price'] != 0) {
            $value['specials'] = $newValue['specials'] = array(array(
                    'customer_group_id' => 32000,
                    'price' => $value['special_price'],
                    'price_type' => 'fixed',
                    'comment' => (isset($value['special_comment']) ? $value['special_comment'] : ''),
                    'date_from' => '',
                    'date_to' => ''
            ));
        }

        $optionTypePriceId = 0;

        if (count($optionTypePrice) > 0) {
            $optionTypePrice['option_type_id'] = $optionTypeId;
            $optionTypePrice['store_id'] = $storeId;
            if ($storeId > 0) {
                $select = $connection->select()->from($tablePrefix . 'catalog_product_option_type_price', array('option_type_price_id', 'price', 'price_type'))->where('option_type_id = ' . $optionTypeId . ' AND `store_id` = ' . $storeId);
                $updateFlag = $connection->fetchRow($select);
                if (isset($updateFlag['option_type_price_id']))
                    $optionTypePriceId = $updateFlag['option_type_price_id'];
            }

            if (!is_array($updateFlag) || (isset($value['price']) && $value['price'] != $updateFlag['price']) || (isset($value['price_type']) && $value['price_type'] != $updateFlag['price_type'])
            ) {
                if ($updateFlag) {
                    $connection->update($tablePrefix . 'catalog_product_option_type_price', $optionTypePrice, 'option_type_id = ' . $optionTypeId . ' AND `store_id` = ' . $storeId);
                } else {
                    $connection->insert($tablePrefix . 'catalog_product_option_type_price', $optionTypePrice);
                    $optionTypePriceId = $connection->lastInsertId($tablePrefix . 'catalog_product_option_type_price');
                }
            }
        } elseif ($storeId > 0) {
            $connection->delete($tablePrefix . 'catalog_product_option_type_price', 'option_type_id = ' . $optionTypeId . ' AND store_id = ' . $storeId);
            $optionTypePriceId = -1;
        }



        if ($optionTypePriceId >= 0) {
            if ($optionTypePriceId == 0) {
                $select = $connection->select()->from($tablePrefix . 'catalog_product_option_type_price', array('option_type_price_id'))->where('option_type_id = ' . $optionTypeId . ' AND `store_id` = ' . $storeId);
                $optionTypePriceId = $connection->fetchOne($select);
            }
            if ($optionTypePriceId > 0) {


                // save option value special_prices
                if ((isset($prevValue['specials']) && count($prevValue['specials']) > 0) || (isset($value['specials']) && count($value['specials']) > 0)) {
                    // remove missing value specials
                    if (isset($prevValue['specials'])) {
                        foreach ($prevValue['specials'] as $key => $specialValue) {
                            if (!isset($newValue['specials'][$key])) {
                                $connection->delete($tablePrefix . 'mageworx_custom_options_option_type_special_price', 'option_type_price_id = ' . $optionTypePriceId . ' AND customer_group_id = ' . $specialValue['customer_group_id']);
                            }
                        }
                    }
                    // save option value special
                    if (isset($value['specials']) && count($value['specials']) > 0) {
                        foreach ($value['specials'] as $key => $specialValue) {
                            if (isset($prevValue['specials'][$key]['customer_group_id']))
                                $prevCustomerGroupId = $prevValue['specials'][$key]['customer_group_id'];
                            else
                                $prevCustomerGroupId = null;
                            $this->saveOptionValueSpecial($optionTypePriceId, $prevCustomerGroupId, $newValue['specials'][$key]);
                        }
                    }
                }


                // save option value tier_prices
                if ((isset($prevValue['tiers']) && count($prevValue['tiers']) > 0) || (isset($value['tiers']) && count($value['tiers']) > 0)) {
                    // remove missing value tiers
                    if (isset($prevValue['tiers'])) {
                        foreach ($prevValue['tiers'] as $key => $tierValue) {
                            if (!isset($newValue['tiers'][$key])) {
                                $connection->delete($tablePrefix . 'mageworx_custom_options_option_type_tier_price', 'option_type_price_id = ' . $optionTypePriceId .
                                        (isset($tierValue['customer_group_id']) ? ' AND customer_group_id = ' . $tierValue['customer_group_id'] : '') .
                                        ' AND qty = ' . $tierValue['qty']);
                            }
                        }
                    }
                    // save option value tier
                    if (isset($value['tiers']) && count($value['tiers']) > 0) {
                        foreach ($value['tiers'] as $key => $tierValue) {
                            if (isset($prevValue['tiers'][$key]['customer_group_id']))
                                $prevCustomerGroupId = $prevValue['tiers'][$key]['customer_group_id'];
                            else
                                $prevCustomerGroupId = null;
                            if (isset($prevValue['tiers'][$key]['qty']))
                                $prevQty = $prevValue['tiers'][$key]['qty'];
                            else
                                $prevQty = 0;
                            $this->saveOptionValueTier($optionTypePriceId, $prevQty, $prevCustomerGroupId, $newValue['tiers'][$key]);
                        }
                    }
                }
            }
        }

        if (isset($value['title'])) {
            $optionTypeTitle = array(
                'option_type_id' => $optionTypeId,
                'store_id' => $storeId,
                'title' => $value['title']
            );

            if ($storeId > 0) {
                $select = $connection->select()->from($tablePrefix . 'catalog_product_option_type_title', array('title'))->where('option_type_id = ' . $optionTypeId . ' AND `store_id` = ' . $storeId);
                $updateFlag = $connection->fetchOne($select);
            }
            if ($value['title'] !== $updateFlag) {
                if ($updateFlag) {
                    $connection->update($tablePrefix . 'catalog_product_option_type_title', $optionTypeTitle, 'option_type_id = ' . $optionTypeId . ' AND `store_id` = ' . $storeId);
                } else {
                    $connection->insert($tablePrefix . 'catalog_product_option_type_title', $optionTypeTitle);
                }
            }
        } elseif ($storeId > 0) {
            $connection->delete($tablePrefix . 'catalog_product_option_type_title', 'option_type_id = ' . $optionTypeId . ' AND store_id = ' . $storeId);
        }


        if (isset($value['description']) && $value['description'] != '') {
            $optionTypeDescription = array(
                'option_type_id' => $optionTypeId,
                'store_id' => $storeId,
                'description' => $value['description']
            );


            $select = $connection->select()->from($tablePrefix . 'mageworx_custom_options_option_type_description', array('description'))->where('option_type_id = ' . $optionTypeId . ' AND `store_id` = ' . $storeId);
            $updateFlag = $connection->fetchOne($select);

            if ($value['description'] !== $updateFlag) {
                if ($updateFlag) {
                    $connection->update($tablePrefix . 'mageworx_custom_options_option_type_description', $optionTypeDescription, 'option_type_id = ' . $optionTypeId . ' AND `store_id` = ' . $storeId);
                } else {
                    $connection->insert($tablePrefix . 'mageworx_custom_options_option_type_description', $optionTypeDescription);
                }
            }
        } elseif ($storeId > 0 || isset($value['description']) && $value['description'] == '') {
            $connection->delete($tablePrefix . 'mageworx_custom_options_option_type_description', 'option_type_id = ' . $optionTypeId . ' AND store_id = ' . $storeId);
        }


        // option value images
        if ((isset($prevValue['images']) && count($prevValue['images']) > 0) || (isset($value['images']) && count($value['images']) > 0)) {
            if ($optionTypeId > 0) {
                // remove missing value image
                if (isset($prevValue['images'])) {
                    if (!isset($newValue['images']))
                        $newValue['images'] = array();
                    foreach ($prevValue['images'] as $imageFile) {
                        if (!in_array($imageFile, $newValue['images'])) {
                            $connection->delete($tablePrefix . 'mageworx_custom_options_option_type_image', $connection->quoteInto('option_type_id = ' . $optionTypeId . ' AND  image_file = ?', $imageFile));
                        }
                    }
                }
                // save option value image
                if (isset($value['images']) && count($value['images']) > 0) {
                    foreach ($value['images'] as $sort => $imageFile) {
                        if (isset($prevValue['images']) && in_array($imageFile, $prevValue['images']))
                            $isUpdate = true;
                        else
                            $isUpdate = 0;
                        $this->saveOptionValueImage($optionTypeId, $imageFile, $sort, $isUpdate);
                    }
                }
            }
        }

        return $optionTypeId;
    }

}
