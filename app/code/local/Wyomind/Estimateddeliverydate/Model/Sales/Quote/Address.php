<?php

if (Mage::helper('core')->isModuleEnabled('Payone_Core')) {

    class Wyomind_Estimateddeliverydate_Model_Sales_Quote_Address_Abstract extends Payone_Core_Model_Sales_Quote_Address
    {
        
    }

} else {

    class Wyomind_Estimateddeliverydate_Model_Sales_Quote_Address_Abstract extends Mage_Sales_Model_Quote_Address
    {
        
    }

}

class Wyomind_Estimateddeliverydate_Model_Sales_Quote_Address extends Wyomind_Estimateddeliverydate_Model_Sales_Quote_Address_Abstract
{

    public function getGroupedAllShippingRates()
    {
        $rates = array();
        $storeId = Mage::app()->getStore()->getId();
        $jsonShipping = Mage::helper("core")->jsonDecode(Mage::getStoreConfig("estimateddeliverydate/shipping/json", $storeId));


        $quoteId = Mage::getModel('checkout/cart')->getQuoteId();
        $quote = Mage::getModel('sales/quote')->load($quoteId);


        $items = $quote->getAllItems();
        if (count($items) == 0) {
            $quote = Mage::getSingleton('checkout/session')->getQuote();
            $items = $quote->getAllItems();
        }


        foreach ($this->getShippingRatesCollection() as $rate) {
            if (!$rate->isDeleted() && $rate->getCarrierInstance()) {
                if (!isset($rates[$rate->getCarrier()])) {
                    $rates[$rate->getCarrier()] = array();
                }

                $code = $rate->getCode();

                $message = null;

                foreach ($jsonShipping as $rule) {
                    if (preg_match("#" . $rule['code'] . "#", $code)) {
                        $leadtime = array();
                        foreach ($items as $item) {
                            $options = Mage::helper("catalog/product_configuration")->getCustomOptions($item);
                            $product = Mage::getModel("catalog/product")->loadByAttribute("sku", $item->getSku());
                            if ($product === false) {
                                $product = Mage::getModel("catalog/product")->load($item->getData('product_id'));
                            }
                            if ($product) {
                                $add = Mage::helper("estimateddeliverydate")->parseFromTo($rule["leadtime"]);

                                $additional = Mage::helper("estimateddeliverydate")->getCustomOptions($options, $storeId);
                                $leadtime[] = Mage::helper("estimateddeliverydate")->getLeadTimes($product, $storeId, $additional[0] + $add['from'], $additional[1] + $add['to'], $item->getQty(), false);

                            }
                        }
                        $range = Mage::helper("estimateddeliverydate")->getLeadTimeRange($leadtime);

                        $message = Mage::helper("estimateddeliverydate")->renderGlobalMessage($range, $storeId, "cart", $rule["message"]);
                        $rate->setMethodTitle(str_replace("{{title}}", $rate->getMethodTitle(), strip_tags($message)));
                        break;
                    } else {
                        
                    }
                }
                $rates[$rate->getCarrier()][] = $rate;
                $rates[$rate->getCarrier()][0]->carrier_sort_order = $rate->getCarrierInstance()->getSortOrder();
            }
        }
        uasort($rates, array($this, '_sortRates'));
        return $rates;
    }

}
