<?php

class Wyomind_Estimateddeliverydate_Model_System_Config_Source_Countdowntype
{

    public function toOptionArray()
    {
        return array(
            array('label' => 'Textual', "value" => '0'),
            array('label' => 'Graphic', 'value' => '1'),
        );
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return $this->toOptionArray();
    }

}
