<?php

$installer = $this;

$installer->startSetup();


$setup = new Mage_Sales_Model_Resource_Setup('core_setup');

$installer->getConnection()->addColumn(
        $installer->getTable('sales/quote_item'), 'estimated_delivery_date', 'TEXT'
);

$installer->getConnection()->addColumn(
        $installer->getTable('sales/quote_item'), 'stock_status', 'INT(1)'
);

$installer->getConnection()->addColumn(
        $installer->getTable('sales/order_item'), 'estimated_delivery_date', 'TEXT'
);

$installer->getConnection()->addColumn(
        $installer->getTable('sales/order_item'), 'stock_status', 'INT(1)'
);


$installer->endSetup();
