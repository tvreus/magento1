<?php

$installer = $this;

$installer->startSetup();

$installer->run("
update {$this->getTable('eav_attribute')} set attribute_code='orders_lt' where attribute_code='orders_leadtime';
update {$this->getTable('eav_attribute')} set attribute_code='orders_lst' where attribute_code='orders_cutoff';
update {$this->getTable('eav_attribute')} set attribute_code='orders_mfp' where attribute_code='orders_message';
update {$this->getTable('eav_attribute')} set attribute_code='orders_sd' where attribute_code='orders_shipping';
update {$this->getTable('eav_attribute')} set attribute_code='backorders_lt' where attribute_code='backorders_leadtime';
update {$this->getTable('eav_attribute')} set attribute_code='backorders_lst' where attribute_code='backorders_cutoff';
update {$this->getTable('eav_attribute')} set attribute_code='backorders_mfp' where attribute_code='backorders_message';
update {$this->getTable('eav_attribute')} set attribute_code='backorders_sd' where attribute_code='backorders_shipping';
update {$this->getTable('eav_attribute')} set attribute_code='use_base_orders_lt' where attribute_code='use_base_orders_leadtime';
update {$this->getTable('eav_attribute')} set attribute_code='use_base_orders_lst' where attribute_code='use_base_orders_cutoff';
update {$this->getTable('eav_attribute')} set attribute_code='use_base_orders_mfp' where attribute_code='use_base_orders_message';
update {$this->getTable('eav_attribute')} set attribute_code='use_base_orders_sd' where attribute_code='use_base_orders_shipping';
update {$this->getTable('eav_attribute')} set attribute_code='use_base_backorders_lt' where attribute_code='use_base_backorders_leadtime';
update {$this->getTable('eav_attribute')} set attribute_code='use_base_backorders_lst' where attribute_code='use_base_backorders_cutoff';
update {$this->getTable('eav_attribute')} set attribute_code='use_base_backorders_mfp' where attribute_code='use_base_backorders_message';
update {$this->getTable('eav_attribute')} set attribute_code='use_base_backorders_sd' where attribute_code='use_base_backorders_shipping';
update {$this->getTable('eav_attribute')} set attribute_code='use_config_orders_lt' where attribute_code='use_config_orders_leadtime';
update {$this->getTable('eav_attribute')} set attribute_code='use_config_orders_lst' where attribute_code='use_config_orders_cutoff';
update {$this->getTable('eav_attribute')} set attribute_code='use_config_orders_mfp' where attribute_code='use_config_orders_message';
update {$this->getTable('eav_attribute')} set attribute_code='use_config_orders_sd' where attribute_code='use_config_orders_shipping';
update {$this->getTable('eav_attribute')} set attribute_code='use_config_backorders_lt' where attribute_code='use_config_backorders_leadtime';
update {$this->getTable('eav_attribute')} set attribute_code='use_config_backorders_lst' where attribute_code='use_config_backorders_cutoff';
update {$this->getTable('eav_attribute')} set attribute_code='use_config_backorders_mfp' where attribute_code='use_config_backorders_message';
update {$this->getTable('eav_attribute')} set attribute_code='use_config_backorders_sd' where attribute_code='use_config_backorders_shipping';
");

$installer->endSetup();
