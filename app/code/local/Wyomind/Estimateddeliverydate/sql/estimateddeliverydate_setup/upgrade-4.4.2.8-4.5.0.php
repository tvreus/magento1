<?php

    $installer = Mage::getResourceModel('catalog/setup', 'catalog_setup');

    $installer->startSetup();

    $fields = array(
        "last_shipping_time_0",
        "last_shipping_time_1",
        "last_shipping_time_2",
        "last_shipping_time_3",
        "last_shipping_time_4",
        "last_shipping_time_5",
        "last_shipping_time_6",
    );

    $attr = array();

    $days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

    foreach (array("orders", "backorders") as $type) {
        foreach ($fields as $name) {

            $attributeCode = Mage::helper("estimateddeliverydate")->getAttributeShorten($name);

            $attributeName = ucWords(str_replace('_', ' ', $attributeCode));
            $attributeName = substr($attributeName, 0, -1) . " - " . $days[substr($attributeName, -1)];

            $attr[$type . '_' . $attributeCode] = array(
                'group' => null,
                'label' => $attributeName,
                'type' => 'varchar',
                'input' => 'hidden',
                'default' => '1',
                'class' => '',
                'backend' => '',
                'frontend' => '',
                'source' => '',
                'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
                'visible' => true,
                'required' => false,
                'user_defined' => false,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'visible_in_advanced_search' => false,
                'unique' => false
            );
            $attr['use_config_' . $type . '_' . $attributeCode] = array(
                'group' => null,
                'label' => "Use Config " . $attributeName,
                'type' => 'int',
                'input' => 'hidden',
                'default' => '1',
                'note' => '',
                'class' => '',
                'backend' => 'catalog/product_attribute_backend_boolean',
                'frontend' => '',
                'source' => '',
                'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
                'visible' => true,
                'required' => false,
                'user_defined' => false,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'visible_in_advanced_search' => false,
                'unique' => false
            );
            $attr['use_base_' . $type . '_' . $attributeCode] = array(
                'group' => null,
                'label' => "Use Default " . $attributeName,
                'type' => 'int',
                'input' => 'hidden',
                'default' => '1',
                'note' => '',
                'class' => '',
                'backend' => 'catalog/product_attribute_backend_boolean',
                'frontend' => '',
                'source' => '',
                'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
                'visible' => true,
                'required' => false,
                'user_defined' => false,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'visible_in_advanced_search' => false,
                'unique' => false
            );
        }
    }

    foreach ($attr as $code => $att) {
        $installer->addAttribute('catalog_product', $code, $att);
    }

    $installer->endSetup();
