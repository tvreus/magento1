<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage
 * @copyright  Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

if (version_compare(phpversion(), '5.3.0', '<')===true) {
    echo  '<div style="font:12px/1.35em arial, helvetica, sans-serif;">
<div style="margin:0 0 25px 0; border-bottom:1px solid #ccc;">
<h3 style="margin:0; font-size:1.7em; font-weight:normal; text-transform:none; text-align:left; color:#2f2f2f;">
Whoops, it looks like you have an invalid PHP version.</h3></div><p>Magento supports PHP 5.3.0 or newer.
<a href="http://www.magentocommerce.com/install" target="">Find out</a> how to install</a>
 Magento using PHP-CGI as a work-around.</p></div>';
    exit;
}

/**
 * Compilation includes configuration file
 */
define('MAGENTO_ROOT', getcwd() . '/..');

$compilerConfig = MAGENTO_ROOT . '/includes/config.php';
if (file_exists($compilerConfig)) {
    include $compilerConfig;
}

$mageFilename = MAGENTO_ROOT . '/app/Mage.php';
$maintenanceFile = 'maintenance.flag';

if (!file_exists($mageFilename)) {
    if (is_dir('downloader')) {
        header("Location: downloader");
    } else {
        echo $mageFilename." was not found";
    }
    exit;
}

if (file_exists($maintenanceFile)) {
    include_once dirname(__FILE__) . '/errors/503.php';
    exit;
}

require MAGENTO_ROOT . '/app/bootstrap.php';
require_once $mageFilename;

#Varien_Profiler::enable();

if (isset($_SERVER['MAGE_IS_DEVELOPER_MODE'])) {
    Mage::setIsDeveloperMode(true);
}

ini_set('display_errors', 1);

umask(0);

/* Store or website code */
$mageRunCode = isset($_SERVER['MAGE_RUN_CODE']) ? $_SERVER['MAGE_RUN_CODE'] : '';

/* Run store or run website */
$mageRunType = isset($_SERVER['MAGE_RUN_TYPE']) ? $_SERVER['MAGE_RUN_TYPE'] : 'store';

Mage::app();

header("Content-type: text/xml");

$collection = Mage::getModel('catalog/product')->getCollection();
$collection->addAttributeToSelect('*');
//add filters if needed.
$collection->addAttributeToFilter('beslist_feed', 1); //for example.

$padd = '    '; //4 spaces for identation
$eol = "\n"; //end of line
$xml = '<?xml version="1.0" encoding="ISO-8859-1"?>'.$eol;
$xml .= '<products>'.$eol;
foreach ($collection as $product) {
	$price = number_format(floatval($product->getFinalPrice()),2, ',', '');
    $xml .= $padd.'<product>'.$eol;
    $xml .= str_repeat($padd, 2).'<titel>'.$product->getName().'</titel>'.$eol;
    $xml .= str_repeat($padd, 2).'<prijs>'. $price .'</prijs>'.$eol;
    $xml .= str_repeat($padd, 2).'<producturl>'.str_replace('/beslist.php/', '/index.php/', $product->getProductUrl()).'</producturl>'.$eol;
    $xml .= str_repeat($padd, 2).'<afbeelding>'.str_replace('/beslist.php/', '/index.php/', Mage::getModel('catalog/product_media_config')->getMediaUrl( $product->getThumbnail() )).'</afbeelding>'.$eol;
    $xml .= str_repeat($padd, 2).'<omschrijving>'.substr(html_entity_decode(strip_tags($product->getDescription())),0,155).'</omschrijving>'.$eol;
    $xml .= str_repeat($padd, 2).'<levertijd>'.$product->getAttributeText('levertijd').'</levertijd>'.$eol;
    $xml .= str_repeat($padd, 2).'<sku>'.$product->getSku().'</sku>'.$eol;
    $xml .= str_repeat($padd, 2).'<verzendkosten>0,00</verzendkosten>'.$eol;

    if($merk = $product->getAttributeText('manufacturer')) {
    	$xml .= str_repeat($padd, 2).'<merk>'.$merk.'</merk>'.$eol;
    } else if($merk = $product->getAttributeText('acc_brand')) {
    	$xml .= str_repeat($padd, 2).'<merk>'.$merk.'</merk>'.$eol;
    } else if($merk = $product->getAttributeText('muurbeugel_brand')) {
    	$xml .= str_repeat($padd, 2).'<merk>'.$merk.'</merk>'.$eol;
    }

    if($ean = $product->getEan()) {
    	$xml .= str_repeat($padd, 2).'<ean>'.$ean.'</ean>'.$eol;
    }
    //add here all the attributes you need to export
    $xml .= $padd.'</product>'.$eol;
}
$xml .= '</products>'.$eol;
//Do something with $xml - save it in a file or echo it.

echo $xml;
