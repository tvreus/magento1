<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage
 * @copyright  Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

if (version_compare(phpversion(), '5.3.0', '<')===true) {
    echo  '<div style="font:12px/1.35em arial, helvetica, sans-serif;">
<div style="margin:0 0 25px 0; border-bottom:1px solid #ccc;">
<h3 style="margin:0; font-size:1.7em; font-weight:normal; text-transform:none; text-align:left; color:#2f2f2f;">
Whoops, it looks like you have an invalid PHP version.</h3></div><p>Magento supports PHP 5.3.0 or newer.
<a href="http://www.magentocommerce.com/install" target="">Find out</a> how to install</a>
 Magento using PHP-CGI as a work-around.</p></div>';
    exit;
}

/**
 * Compilation includes configuration file
 */
define('MAGENTO_ROOT', getcwd() . '/..');

$compilerConfig = MAGENTO_ROOT . '/includes/config.php';
if (file_exists($compilerConfig)) {
    include $compilerConfig;
}

$mageFilename = MAGENTO_ROOT . '/app/Mage.php';
$maintenanceFile = 'maintenance.flag';

if (!file_exists($mageFilename)) {
    if (is_dir('downloader')) {
        header("Location: downloader");
    } else {
        echo $mageFilename." was not found";
    }
    exit;
}

if (file_exists($maintenanceFile)) {
    include_once dirname(__FILE__) . '/errors/503.php';
    exit;
}

require MAGENTO_ROOT . '/app/bootstrap.php';
require_once $mageFilename;

#Varien_Profiler::enable();

if (isset($_SERVER['MAGE_IS_DEVELOPER_MODE'])) {
    Mage::setIsDeveloperMode(true);
}

ini_set('display_errors', 1);

umask(0);

/* Store or website code */
$mageRunCode = isset($_SERVER['MAGE_RUN_CODE']) ? $_SERVER['MAGE_RUN_CODE'] : '';

/* Run store or run website */
$mageRunType = isset($_SERVER['MAGE_RUN_TYPE']) ? $_SERVER['MAGE_RUN_TYPE'] : 'store';

Mage::app();

header("Content-type: text/xml");

$collection = Mage::getModel('catalog/product')->getCollection();
$collection->addAttributeToSelect('*');
//add filters if needed.
$collection->addAttributeToFilter('tweakers_feed', 1); //for example.

$padd = '    '; //4 spaces for identation
$eol = "\n"; //end of line
$xml = '<?xml version="1.0" encoding="UTF-8"?>'.$eol;
$xml .= '<Products>'.$eol;
foreach ($collection as $product) {
    $ean = $product->getEan();
    if( $ean == '' ) {
        continue;
    }
    $price = number_format(floatval($product->getFinalPrice()),2, ',', '');
    $xml .= $padd.'<Product>'.$eol;
    $xml .= str_repeat($padd, 2). '<Product_in_stock>' . (int)Mage::getModel('cataloginventory/stock_item')->loadByProduct($product)->getQty() . '</Product_in_stock>';

    if($merk = $product->getAttributeText('manufacturer')) {
        $xml .= str_repeat($padd, 2).'<Product_Brand>'.$merk.'</Product_Brand>'.$eol;
    } else if($merk = $product->getAttributeText('acc_brand')) {
        $xml .= str_repeat($padd, 2).'<Product_Brand>'.$merk.'</Product_Brand>'.$eol;
    } else if($merk = $product->getAttributeText('muurbeugel_brand')) {
        $xml .= str_repeat($padd, 2).'<Product_Brand>'.$merk.'</Product_Brand>'.$eol;
    }
    $xml .= str_repeat($padd, 2).'<SKU_Code>'.$product->getSku().'</SKU_Code>'.$eol;
    $xml .= str_repeat($padd, 2).'<Product_Name>'.$product->getName().'</Product_Name>'.$eol;
    $xml .= str_repeat($padd, 2).'<Product_Price>'. $price .'</Product_Price>'.$eol;

    $levertijd = $product->getAttributeText('levertijd');

    if( $levertijd == 'Voor 23:59 besteld, morgen in huis' ) {
        $xml .= str_repeat($padd, 2).'<Delivery_Period>Vandaag besteld, morgen in huis. Verstuurt ook in het weekend.</Delivery_Period>'.$eol;
    } else {
        $xml .= str_repeat($padd, 2).'<Delivery_Period>'.$levertijd.'</Delivery_Period>'.$eol;
    }
    $xml .= str_repeat($padd, 2).'<Product_Ean>'.$ean.'</Product_Ean>'.$eol;
    $xml .= str_repeat($padd, 2).'<Deeplink>'.str_replace('/tweakers.php/', '/index.php/', $product->getProductUrl()).'</Deeplink>'.$eol;
    $xml .= str_repeat($padd, 2).'<Delivery_Costs>0,00</Delivery_Costs>'.$eol;
    $xml .= str_repeat($padd, 2).'<Delivery_Costs_BE>0,00</Delivery_Costs_BE>'.$eol;
    $xml .= str_repeat($padd, 2).'<Visible>'. ($product->getTweakersFeed() == "1" ? 1 : 0) . '</Visible>' . $eol;


    //add here all the attributes you need to export
    $xml .= $padd.'</Product>'.$eol;
}
$xml .= '</Products>'.$eol;
//Do something with $xml - save it in a file or echo it.

echo $xml;
