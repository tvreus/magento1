<?php

/**
 * MageWorx
 * MageWorx Order Base Extension
 *
 * @category   MageWorx
 * @package    MageWorx_OrdersBase
 * @copyright  Copyright (c) 2017 MageWorx (http://www.mageworx.com/)
 */
class MageWorx_OrdersBase_Model_Logger extends Mage_Core_Model_Abstract
{
    /**
     * Add comment and save order
     *
     * @param $text
     * @param Mage_Sales_Model_Order $order
     * @param int $notify (0 - no one; 1 - only admin; 2 - notify all)
     * @return $this
     * @throws Exception
     */
    public function log($text, Mage_Sales_Model_Order $order, $notify)
    {
        $order->addStatusHistoryComment($text, $order->getStatus())
            ->setIsVisibleOnFront(1)
            ->setIsCustomerNotified($notify > 1);

        if ($notify) {
            $order->sendOrderUpdateEmail($notify > 1, $text);
        }
        $order->save();

        return $this;
    }
}