<?php
/**
 * @copyright   Copyright (c) 2011 http://magentosupport.net
 * @author		Vlad Vysochansky
 * @license     http://opensource.org/licenses/gpl-license.php  GNU General Public License (GPL)
 */
class MagentoSupport_CallBackRequest_IndexController extends Mage_Core_Controller_Front_Action
{
    const XML_PATH_CALLBACK_RECIPIENT  = 'callbackrequest/general/recipient_email';
    const XML_PATH_CALLBACK_SENDER     = 'callbackrequest/general/sender_email_identity';
    const XML_PATH_CALLBACK_TEMPLATE   = 'callbackrequest/general/email_template';
    const XML_PATH_ENABLED          = 'callbackrequest/general/enabled';

/*
    public function indexAction()
    {
        //Get current layout state
        $this->loadLayout();

        $block = $this->getLayout()->createBlock(
            'Mage_Core_Block_Template',
            'magentosupport.call_back_request',
            array(
                'template' => 'magentosupport/callbackrequest.phtml'
            )
        );

        $this->getLayout()->getBlock('content')->append($block);
        //$this->getLayout()->getBlock('right')->insert($block, 'catalog.compare.sidebar', true);

        $this->_initLayoutMessages('core/session');

        $this->renderLayout();
    }
*/
    public function sendAction()
    {

        $post = $this->getRequest()->getPost();
        if ( $post ) {
            $translate = Mage::getSingleton('core/translate');
            /* @var $translate Mage_Core_Model_Translate */
            $translate->setTranslateInline(false);
            try {
                $postObject = new Varien_Object();
                $postObject->setData($post);

                $error = false;

                if (!Zend_Validate::is(trim($post['name']) , 'NotEmpty')) {
                    $error = true;
                }

                if (!Zend_Validate::is(trim($post['comment']) , 'NotEmpty')) {
                    $error = true;
                }

                if (!Zend_Validate::is(trim($post['telephone']), 'NotEmpty')) {
                    $error = true;
                }


                if ($error) {
                    throw new Exception();
                }

                $mailTemplate = Mage::getModel('core/email_template');
                /* @var $mailTemplate Mage_Core_Model_Email_Template */
                $mailTemplate->setDesignConfig(array('area' => 'frontend'))
//                    ->setReplyTo($post['email'])
                    ->sendTransactional(
                        Mage::getStoreConfig(self::XML_PATH_CALLBACK_TEMPLATE),
                        Mage::getStoreConfig(self::XML_PATH_CALLBACK_SENDER),
                        Mage::getStoreConfig(self::XML_PATH_CALLBACK_RECIPIENT),
                        null,
                        array('data' => $postObject)
                    );

                if (!$mailTemplate->getSentSuccess()) {
                    throw new Exception();
                }

                $translate->setTranslateInline(true);

		        if($this->getRequest()->isXmlHttpRequest())
		        {
		            $this->loadLayout();
		            $this->getResponse()->setBody('<ul class="messages"><li class="success-msg"><ul><li>'.Mage::helper('callbackrequest')->__('Your inquiry was submitted and will be responded to as soon as possible. Thank you for contacting us.').'</li></ul></li></ul>');
		        }
             	else{
	                Mage::getSingleton('customer/session')->addSuccess();
	                $this->_redirect('*/*/');
                }

                return;
            } catch (Exception $e) {
                $translate->setTranslateInline(true);
		        if($this->getRequest()->isXmlHttpRequest())
		        {
		            $this->loadLayout();
		            $this->getResponse()->setBody('<ul class="messages"><li class="error-msg"><ul><li>'.Mage::helper('callbackrequest')->__('Unable to submit your request. Please, try again later').'</li></ul></li></ul>');
		        }
             	else{
	                Mage::getSingleton('customer/session')->addError(Mage::helper('callbackrequest')->__('Unable to submit your request. Please, try again later'));
	                $this->_redirect('*/*/');
                }
                return;
            }

        } else {
            $this->_redirect('*/*/');
        }
/*

        //Fetch submited params
        $params = $this->getRequest()->getParams();

        $mail = new Zend_Mail();
        $mail->setBodyText($params['comment']);
        $mail->setFrom($params['email'], $params['name']);
        $mail->addTo('somebody_else@example.com', 'Some Recipient');
        $mail->setSubject('Test ActiveCodeline_SimpleContact Module for Magento');
        try {
            $mail->send();
        }
        catch(Exception $ex) {
            Mage::getSingleton('core/session')->addError('Unable to send email. Sample of a custom notification error from ActiveCodeline_SimpleContact.');

        }

        //Redirect back to index action of (this) activecodeline-simplecontact controller
        $this->_redirect('activecodeline-simplecontact/');
*/
    }
}

?>