<?php 
/**
 * Magmodules.eu - http://www.magmodules.eu
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@magmodules.eu so we can send you a copy immediately.
 *
 * @category    Magmodules
 * @package     Magmodules_Webwinkelconnect
 * @author      Magmodules <info@magmodules.eu)
 * @copyright   Copyright (c) 2014 (http://www.magmodules.eu)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
 
class Magmodules_Webwinkelconnect_Model_Api extends Mage_Core_Model_Abstract {

	public function processFeed($storeid = 0, $type) {				
		if($feed = $this->getFeed($storeid, $type)) {
			$results		 	= Mage::getModel('webwinkelconnect/reviews')->processFeed($feed, $storeid, $type);			
			$results['stats'] 	= Mage::getModel('webwinkelconnect/stats')->processFeed($feed, $storeid);	
			return $results;
		} else {
			return false;
		}	
	}

	public function getFeed($storeid, $type = '') {
		$api_id			= trim(Mage::getStoreConfig('webwinkelconnect/general/api_id', $storeid));
		$api_key		= trim(Mage::getStoreConfig('webwinkelconnect/general/api_key', $storeid));

		if($type != 'stats') {		
			$api_url = 'https://www.webwinkelkeur.nl/apistatistics.php?id=' . $api_id . '&password=' . $api_key . '&showall=1';
		} else {
			$api_url = 'https://www.webwinkelkeur.nl/apistatistics.php?id=' . $api_id . '&password=' . $api_key;		
		}
			
		if($api_id && $api_key) {
			$xml = simplexml_load_file($api_url);	
			if($xml) {
				return $xml;
			} else {
				$error = file_get_contents($api_url);
				Mage::getSingleton('adminhtml/session')->addError(Mage::helper('webwinkelconnect')->__('%s, please check the online manual for suggestions.', $error));
				return false;
			}	
		} else {
			return false;
		}		
	}
	
	public function sendInvitation($order) {
		$start_time = microtime(true);		
		$crontype 	= 'orderupdate';
		$order_id 	= $order->getIncrementId(); 
		$api_id 	= trim(Mage::getStoreConfig('webwinkelconnect/general/api_id', $order->getStoreId()));
		$api_key 	= trim(Mage::getStoreConfig('webwinkelconnect/general/api_key', $order->getStoreId()));
		$delay		= trim(Mage::getStoreConfig('webwinkelconnect/invitation/delay', $order->getStoreId()));
		$email		= $order->getCustomerEmail();
		
		$http = new Varien_Http_Adapter_Curl();
		$http->setConfig(array('timeout' => 30, 'maxredirects' => 0));

		$url = 'https://www.webwinkelkeur.nl/api.php?id=' . $api_id . '&password=' . $api_key . '&email=' . $email . '&order=' . $order_id . '&delay=' . $delay;
		$http->write(Zend_Http_Client::POST, $url, '1.1', array());
		$result = $http->read();

		if($result) {
			$lines = explode("text/html", $result);
			if(isset($lines[1])) {
				$response_html = $lines[1];
			}	
		}
		if(!isset($response_html)) {
			$response_html = 'No response from https://www.webwinkelkeur.nl';
		}

		// Write to log
		$writelog = Mage::getModel('webwinkelconnect/log')->addToLog('invitation', $order->getStoreId(), '', $response_html, (microtime(true) - $start_time), $crontype, $url, $order->getId());
		return true;
    }

	public function getStoreIds() {
		$resource = Mage::getSingleton('core/resource');
		$read = $resource->getConnection('core_read');
		$query = "SELECT DISTINCT value, scope_id FROM " . $resource->getTableName('core/config_data') . " WHERE path LIKE 'webwinkelconnect/general/api_id'";
		$results = $read->fetchAll($query);
		$storeids = array(); 
		
		foreach($results as $result)  {
			if($result['value'] > 0) {
				$storeids[] = $result['scope_id'];
			}
		}						
		return $storeids; 		
	}
	    
}