<?php

/**
 *
 * @category MultiSafepay
 * @package  MultiSafepay_Msp
 */

class MultiSafepay_Msp_TokenizationController extends Mage_Core_Controller_Front_Action
{
    public function deletedataAction()
    {
        $hash = $this->getRequest()->getParam("hash");

        $mspHelper = Mage::helper('msp/data');
        $token = $mspHelper->getTokenizationByRecurringHash($hash);
        if(Mage::getSingleton('customer/session')->isLoggedIn()) {
            $customerId = Mage::getSingleton('customer/session')->getCustomer()->getId();
            if($token['customer_id'] === $customerId){
                $token->delete();
                return true;
            }
        }
        return false;
    }
}
