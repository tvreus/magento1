<?php

class MultiSafepay_Msp_Model_Resource_Tokenization_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct() {
        $this->_init('msp/tokenization');
    }
}