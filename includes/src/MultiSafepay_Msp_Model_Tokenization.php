<?php

/**
 *
 * @category MultiSafepay
 * @package  MultiSafepay_Msp
 */
require_once(Mage::getBaseDir('lib') . DS . 'multisafepay' . DS . 'MultiSafepay.combined.php');

class MultiSafepay_Msp_Model_Tokenization extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('msp/tokenization');
    }
}
