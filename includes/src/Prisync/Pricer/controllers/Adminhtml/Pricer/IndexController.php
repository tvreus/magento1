<?php

class Prisync_Pricer_Adminhtml_Pricer_IndexController extends Mage_Adminhtml_Controller_Action
{
    protected function _isAllowed() 
    {
        return Mage::getSingleton('admin/session')->isAllowed('pricer/pricer_import_products');
    }
    
    public function indexAction() 
    {
        // Validate email and api key and redirect to settings page and show error if necessary
        $util = Mage::helper('prisync_pricer/util');
        $validationResponse = $util->validateCredential();
        $isValid = $validationResponse['status'];
        $isInitialSetup = $validationResponse['initial_setup'];
        
        $this->loadLayout();
        
        // Set menu item as active
        $this->_setActiveMenu('pricer');
        
        // Set window title
        $this->_title('Prisync');
        
        // Use these info in template to determine show/hide product grid
        $this->getLayout()->getBlock('pricerIndex')->setIsInitialSetup($isInitialSetup);
        $this->getLayout()->getBlock('pricerIndex')->setIsValid($isValid);
        
        if ($isInitialSetup === 1) { // If user uses the app for the first time, show notice about email and api key
            $validationError = $validationResponse['error'];
            Mage::getSingleton('adminhtml/session')->addNotice($validationError);
            $this->_initLayoutMessages('adminhtml/session'); // Show added notice immediately
        } else {
            if ($isValid !== 1) { // If something goes wrong and cannot validate user, show error
                $validationError = $validationResponse['error'];
                Mage::getSingleton('adminhtml/session')->addError($validationError);
                $this->_initLayoutMessages('adminhtml/session'); // Show added error immediately
            } else {
                // Pass company info to template
                $this->getLayout()->getBlock('pricerIndex')->setCompanyPackageLimit(
                    Mage::getStoreConfig('pricer_options/settings/package_limit')
                );
                
                $this->getLayout()->getBlock('pricerIndex')->setCompanyStatus(
                    Mage::getStoreConfig('pricer_options/settings/company_status')
                );
                
                $this->getLayout()->getBlock('pricerIndex')->setRemainingTrialDays(
                    Mage::getStoreConfig('pricer_options/settings/remaining_trial_days')
                );

                $this->getLayout()->getBlock('pricerIndex')->setSelectedStoreInfo(
                    $util->getStoreInfo()
                );

                // Send heartbeat to Prisync
                $util->extensionHeartbeat();

                // Control debug flag on Prisync
                $util->extensionDebug();
            }
        }
        
        if (Mage::getStoreConfig('pricer_options/settings/sync_status')) {
            Mage::getSingleton('adminhtml/session')->addSuccess(
                "Prisync successfully set your products' prices."
            );
            $this->_initLayoutMessages('adminhtml/session'); // Show added notice immediately
            
            Mage::getModel('core/config')->saveConfig('pricer_options/settings/sync_status', false);
        }
        
        $this->renderLayout();
    }
    
    public function gridAction() 
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('prisync_pricer/adminhtml_product_list_grid')->toHtml()
        );
    }
    
    public function importAction() 
    {
        $productList = $response = array();
        $productIdList = $this->getRequest()->getPost('entity_id');
        
        if (!empty($productIdList)) {
            // Just import first `companyPackageLimit` (e.g. 100, 1000, 5000) of the selected products
            $productIdList = array_slice(
                $productIdList, 0, 
                Mage::getStoreConfig('pricer_options/settings/package_limit')
            );

            $defaultStoreId = Mage::app()->getDefaultStoreView()->getId();
            
            foreach ($productIdList as $productId) {
                if ($storeId = Mage::getStoreConfig('pricer_options/settings/store_id')) {
                    $productModel = Mage::getModel('catalog/product')->setStoreId($storeId)->load($productId);
                } else {
                    $productModel = Mage::getModel('catalog/product')->setStoreId($defaultStoreId)->load($productId);
                }

                // If product is grouped than do not import itself. Instead import associative products.
                // Get each associative product and load with store id if exists.
                if ($productModel->isGrouped()) {
                    foreach ($productModel->getTypeInstance()->getAssociatedProducts() as $product) {
                        $productId = $product->getId();

                        if ($storeId = Mage::getStoreConfig('pricer_options/settings/store_id')) {
                            $assocProductModel = 
                                Mage::getModel('catalog/product')->setStoreId($storeId)->load($productId);
                        } else {
                            $assocProductModel = 
                                Mage::getModel('catalog/product')->setStoreId($defaultStoreId)->load($productId);
                        }

                        $packageProductResponse = Mage::helper('prisync_pricer/util')
                                                    ->packageProductInfo($assocProductModel, true, $productModel);

                        array_push($productList, $packageProductResponse['product']);
                    }
                } else {
                    $packageProductResponse = 
                        Mage::helper('prisync_pricer/util')->packageProductInfo($productModel, true);

                    array_push($productList, $packageProductResponse['product']);
                }
            }
            
            $response = Mage::helper('prisync_pricer/util')->batchImportProducts($productList);
        } else {
            Mage::helper('prisync_pricer/util')->customLog(
                array(
                    'class' => 'INDEX_CONTROLLER',
                    'function' => 'importAction',
                    'level' => 'error',
                    'message' => 'Detail: There is not any product in import list!'
                )
            );
        }
        
        Mage::helper('prisync_pricer/util')->returnAjax($response);
    }
    
    public function getBatchImportProgressAction() 
    {
        $util = Mage::helper('prisync_pricer/util');
        
        return $util->getBatchImportProgress();
    }
}
