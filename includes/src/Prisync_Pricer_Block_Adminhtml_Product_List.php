<?php

class Prisync_Pricer_Block_Adminhtml_Product_List extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    // We declare the content of our items container
    public function __construct()
    {
        // This block is called as prisync_pricer/adminhtml_product_list
        
        // The blockGroup must match the first half of how we call the block
        $this->_blockGroup = 'prisync_pricer';
        
        // The controller must match the second half of how we call the block
        $this->_controller = 'adminhtml_product_list';
        
        $this->_headerText = Mage::helper('adminhtml')->__('Product List');
        
        parent::__construct();
        $this->_removeButton('add');
    }
    
    /**
     * Render grid
     *
     * @return string
     */
    public function getGridHtml()
    {
        return $this->getChildHtml('grid');
    }
}