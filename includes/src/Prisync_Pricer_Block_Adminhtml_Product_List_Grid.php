<?php

class Prisync_Pricer_Block_Adminhtml_Product_List_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    
    public function __construct()
    {
        parent::__construct();
        
        // Set a unique id for our grid
        $this->setId('product_list');
        
        // Default sort by column
        $this->setDefaultSort('entity_id');
        
        // Used for AJAX loading
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }
    
    public function getRowClickCallback()
    {
        return false;
    }
    
    protected function _prepareCollection()
    {
        $store = $this->_getStore();
        
        $collection = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('sku')
            ->addAttributeToSelect('name')
            ->addAttributeToSelect('attribute_set_id')
            ->addAttributeToSelect('type_id')
            ->addAttributeToFilter('visibility', array(2, 3, 4))
            ->addAttributeToFilter('status', 1);

        if ($store->getId()) {
            $adminStore = Mage_Core_Model_App::ADMIN_STORE_ID;
            $collection->addStoreFilter($store);
            $collection->joinAttribute(
                'name',
                'catalog_product/name',
                'entity_id',
                null,
                'inner',
                $adminStore
            );

            $collection->joinAttribute(
                'price',
                'catalog_product/price',
                'entity_id',
                null,
                'left',
                $store->getId()
            );
        } else {
            $collection->addAttributeToSelect('price');
        }
        
        $this->setCollection($collection);
        
        parent::_prepareCollection();
        $this->getCollection()->addWebsiteNamesToResult();
        return $this;
    }

    protected function _addColumnFilterToCollection($column)
    {
        if ($this->getCollection()) {
            if ($column->getId() == 'websites') {
                $this->getCollection()->joinField(
                    'websites',
                    'catalog/product_website',
                    'website_id',
                    'product_id=entity_id',
                    null,
                    'left'
                );
            }
        }

        return parent::_addColumnFilterToCollection($column);
    }
    
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('entity_id');
        
        $this->getMassactionBlock()->addItem(
            'import', array(
                'label'=> Mage::helper('prisync_pricer')->__('Import'),
                'url'  => $this->getUrl('*/*/import', array('isAjax' => 'true')),
                'confirm' => Mage::helper('prisync_pricer')->__(
                    "We are going to import the products you've selected with considering your package limits."
                )
            )
        );
        
        return $this;
    }
    
    // Set every column to be displayed on the grid
    protected function _prepareColumns()
    {
        $this->addColumn(
            'entity_id',
            array(
                'header'=> Mage::helper('catalog')->__('ID'),
                'width' => '50px',
                'type'  => 'number',
                'index' => 'entity_id',
            )
        );

        $this->addColumn(
            'name',
            array(
                'header'=> Mage::helper('catalog')->__('Name'),
                'index' => 'name',
            )
        );
        
        $sets = Mage::getResourceModel('eav/entity_attribute_set_collection')
            ->setEntityTypeFilter(Mage::getModel('catalog/product')->getResource()->getTypeId())
            ->load()
            ->toOptionHash();
        
        $this->addColumn(
            'set_name',
            array(
                'header'=> Mage::helper('catalog')->__('Attrib. Set Name'),
                'width' => '100px',
                'index' => 'attribute_set_id',
                'type'  => 'options',
                'options' => $sets,
            )
        );

        $this->addColumn(
            'type',
            array(
                'header'=> Mage::helper('catalog')->__('Type'),
                'width' => '60px',
                'index' => 'type_id',
                'type'  => 'options',
                'options' => Mage::getSingleton('catalog/product_type')->getOptionArray(),
            )
        );
        
        $manufacturer  = Mage::getResourceModel('catalog/product')->getAttribute('manufacturer');
        $manufacturerPreOptions = $manufacturer->getSource()->getAllOptions(false);
        
        $manufacturerOptions = array();
        foreach ($manufacturerPreOptions as $manufacturerOption) {
            if ($manufacturerOption['value']) {
                $manufacturerOptions[$manufacturerOption['value']] = $manufacturerOption['label'];
            }
        }
        
        $this->addColumn(
            'manufacturer', array(
            'header'  => $this->__($manufacturer->getFrontendLabel()),
            'width'   => '100px',
            'type'    => 'options',
            'index'   => $manufacturer->getAttributeCode(),
            'options' => $manufacturerOptions,
            )
        );
        
        $this->addColumn(
            'sku',
            array(
                'header'=> Mage::helper('catalog')->__('SKU'),
                'width' => '80px',
                'index' => 'sku',
            )
        );
        
        $store = $this->_getStore();
        $this->addColumn(
            'price',
            array(
                'header'=> Mage::helper('catalog')->__('Price'),
                'type'  => 'price',
                'currency_code' => $store->getBaseCurrency()->getCode(),
                'index' => 'price',
            )
        );

        if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn(
                'websites',
                array(
                    'header'=> Mage::helper('catalog')->__('Websites'),
                    'width' => '100px',
                    'sortable'  => false,
                    'index'     => 'websites',
                    'type'      => 'options',
                    'options'   => Mage::getModel('core/website')->getCollection()->toOptionHash(),
                )
            );
        }
        
        return parent::_prepareColumns();
    }
    
    // Used for AJAX loading
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }
    
    protected function _getStore()
    {
        $storeId = Mage::getStoreConfig('pricer_options/settings/store_id');
        return Mage::app()->getStore($storeId);
    }
}