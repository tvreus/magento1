<?php

class Prisync_Pricer_Helper_Util extends Mage_Core_Helper_Abstract
{
    public $apiUrl = 'https://prisync.com/api/v2/';
    const PRISYNC_URI = 'https://prisync.com/';
    const PRISYNC_HELPCENTER_URI = 'https://helpcenter.prisync.com/';

    const LOGGLY_HOST = 'logs-01.loggly.com';
    const LOGGLY_SECURE_PORT = 443;
    const LOGGLY_CONN_TIMEOUT = 30;
    const LOGGLY_TOKEN = "32aef48b-d7d2-494a-a65d-27799ed42fff";
    const LOGGLY_USERAGENT = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36";
    
    public function validateCredential($appEmail = null, $appKey = null) 
    {
        $appEmail = $appEmail == null ? Mage::getStoreConfig('pricer_options/settings/email') : $appEmail;
        $appKey = $appKey == null ? Mage::getStoreConfig('pricer_options/settings/apikey') : $appKey;
        
        $response = array();
        
        if ($appKey === null || $appEmail === null) {
            $response['error'] = 'You need to enter your Email and API Key to use app.';
            $response['status'] = 0;
            $response['initial_setup'] = 1;
        } else {
            $response['initial_setup'] = 0;
            
            $header = array();
            array_push($header, "Apikey: ".$appEmail);
            array_push($header, "Apitoken: ".$appKey);
            
            $result = $this->cURLRequest("account", 'GET', false, $header);
            
            if (isset($result['result']['errorCode'])) {
                $response['error'] = 'We cannot authorize you. Please check your Email and/or API Key';
                $response['status'] = 0;
            } else if ($result['result'] == null) {
                $response['error'] = 'You need to enter your Email and API Key to use app.';
                $response['status'] = 0;
            } else {
                $companyApiStatus = $result['result']['api_enabled'];
                $companyPackageType = $result['result']['package'];
                $companyPackageLimit = $result['result']['product_limit'];
                $companyStatus = $result['result']['company_status'];
                $companyLeftTrialDays = $result['result']['remaining_trial_days'];
                
                // Save necessary info into Magento config table
                Mage::getModel('core/config')->saveConfig(
                    'pricer_options/settings/api_status', $companyApiStatus
                );

                Mage::getModel('core/config')->saveConfig(
                    'pricer_options/settings/package_type', $companyPackageType
                );

                Mage::getModel('core/config')->saveConfig(
                    'pricer_options/settings/package_limit', $companyPackageLimit
                );

                Mage::getModel('core/config')->saveConfig(
                    'pricer_options/settings/company_status', $companyStatus
                );

                Mage::getModel('core/config')->saveConfig(
                    'pricer_options/settings/remaining_trial_days', $companyLeftTrialDays
                );

                if (!Mage::getStoreConfig('pricer_options/settings/set_price_auto')) {
                    Mage::getModel('core/config')->saveConfig(
                        'pricer_options/settings/set_price_auto', 0
                    );
                }

                // To use updated DB configs need to reinit config and store
                Mage::getConfig()->reinit();
                Mage::app()->reinitStores();
                
                // Control company status
                if ($companyStatus === 'extended' || $companyStatus === 'in_trial') {
                    if ($companyLeftTrialDays > 0) {
                        if ($companyApiStatus === true) {
                            $response['status'] = 1;
                        } else {
                            $response['error'] = 'Your API is disabled. Please contact us on 
                                                    <a href="'.self::PRISYNC_URI.'login" target="_blank">Prisync</a>.';
                            $response['status'] = 0;
                        }
                    } else {
                        $response['error'] = 'Your 14 days trial period is over. Please subscribe on 
                                                <a href="'.self::PRISYNC_URI.'login" target="_blank">Prisync</a>.';
                        $response['status'] = 0;
                    }
                } else if ($companyStatus === 'paid') {
                    if ($companyApiStatus === true) {
                        $response['status'] = 1;
                    } else {
                        $response['error'] = 'Your API is disabled. Please contact us on 
                                                <a href="'.self::PRISYNC_URI.'login" target="_blank">Prisync</a>.';
                        $response['status'] = 0;
                    }
                } else {
                    $response['error'] = 'Please contact us on 
                                            <a href="'.self::PRISYNC_URI.'login" target="_blank">Prisync</a>.';
                    $response['status'] = 0;
                }
            }
        }
        
        if (isset($response['error'])) {
            $this->customLog(
                array(
                    'class' => 'UTIL',
                    'function' => 'validateCredential',
                    'level' => 'error',
                    'message' => 'Error occurred while validating credentials! - Detail: '.$response['error']
                )
            );
        } else {
            $this->customLog(
                array(
                    'class' => 'UTIL',
                    'function' => 'validateCredential',
                    'level' => 'info',
                    'message' => 'Credential validation successfully completed!'
                )
            );
        }
        
        return $response;
    }
    
    public function setSuggestedPrices() 
    {
        $productList = $this->retrieveAllProducts();
        $productCount = count($productList);
        $currentRow = 1;
        $errors = array();
        
        if (!empty($productList)) {
            foreach ($productList as $product) {
                $getProductDetail = $this->getProduct($product['id']);

                $this->customLog(
                    array(
                        'class' => 'UTIL',
                        'function' => 'setSuggestedPrices',
                        'level' => 'info',
                        'message' => 'Getting '
                            .$currentRow.' / '.$productCount.' - Prisync Product ID: '.$product['id']
                    )
                );

                if ($getProductDetail != false) {
                    /**
                     * is_numeric() not accept 1,234.00 format as a numeric value. So, clean commas and test it.
                     */
                    $smartPriceOrig = $getProductDetail['result']['smart_price'];
                    $smartPriceTmp = str_replace(',','', $smartPriceOrig);

                    if (is_numeric($smartPriceTmp)) {
                        $smartPrice = $smartPriceOrig;

                        if (!empty($getProductDetail['result']['external_ref'])) {
                            try {
                                $magentoProduct = '';
                                $magentoProductId = $getProductDetail['result']['external_ref'];

                                $this->customLog(
                                    array(
                                        'class' => 'UTIL',
                                        'function' => 'setSuggestedPrices',
                                        'level' => 'info',
                                        'message' => 'Processing Prisync Product ID: '.$product['id']
                                            .' - Magento Product ID: '.$magentoProductId
                                    )
                                );

                                $storeId = -1; // Set a default store id to check setting existence

                                /**
                                 * Check Catalog Price Scope config of Magento
                                 *
                                 * @see http://docs.magento.com/m2/ce/user_guide/catalog/catalog-price-scope.html
                                */
                                if (Mage::getStoreConfig('catalog/price/scope') == 0) {
                                    $magentoProduct = Mage::getModel('catalog/product')
                                        ->load($magentoProductId);

                                    $storeId = Mage_Core_Model_App::ADMIN_STORE_ID;
                                } else {
                                    // Load selected store product to set price for right product
                                    if (!empty(Mage::getStoreConfig('pricer_options/settings/store_id'))) {
                                        $storeId = Mage::getStoreConfig('pricer_options/settings/store_id');

                                        $magentoProduct = Mage::getModel('catalog/product')
                                            ->setStoreId($storeId)->load($magentoProductId);
                                    }
                                }

                                if ($magentoProduct->getId() && $storeId != -1) {
                                    // Check price attr settings to determine which price to set
                                    if (!empty(Mage::getStoreConfig('pricer_options/settings/price_attr'))) {
                                        $priceAttr = Mage::getStoreConfig('pricer_options/settings/price_attr');

                                        if ($priceAttr == 'special_price') {
                                            $attribute = array('special_price' => $smartPrice);
                                        } else {
                                            $attribute = array('price' => $smartPrice);
                                        }
                                    } else {
                                        $attribute = array('price' => $smartPrice);
                                    }

                                    Mage::getSingleton('catalog/product_action')
                                        ->updateAttributes(array($magentoProduct->getId()), $attribute, $storeId);

                                    $this->customLog(
                                        array(
                                            'class' => 'UTIL',
                                            'function' => 'setSuggestedPrices',
                                            'level' => 'info',
                                            'message' => 
                                                'SmartPrice automatically set for product id: '.$magentoProductId
                                        )
                                    );
                                } else {
                                    $this->customLog(
                                        array(
                                            'class' => 'UTIL',
                                            'function' => 'setSuggestedPrices',
                                            'level' => 'error',
                                            'message' => 'Magento product cannot load for store id : '.$storeId
                                                            .' - product id:'.$magentoProductId
                                                            .' or store setting of Prisync is corrupted!'
                                        )
                                    );
                                }
                            } catch (Exception $e) {
                                $this->customLog(
                                    array(
                                        'class' => 'UTIL',
                                        'function' => 'setSuggestedPrices',
                                        'level' => 'error',
                                        'message' => 'Error occurred while setting prices automatically! - Detail: '
                                                            .$e->getMessage()
                                    )
                                );
                            }
                        } else {
                            $errors[$product['id']] = 'External Ref is null!';
                        }
                    } else {
                        $errors[$product['id']] = 'SmartPrice is not numerical!';
                    }
                }

                $currentRow += 1;
            }

            if (!empty($errors)) {
                $this->customLog(
                    array(
                        'class' => 'UTIL',
                        'function' => 'setSuggestedPrices',
                        'level' => 'error',
                        'message' => 'Error(s) occurred while setting prices automatically! - Detail: '
                            .Mage::helper('core')->jsonEncode($errors)
                    )
                );
            } else {
                $this->customLog(
                    array(
                        'class' => 'UTIL',
                        'function' => 'setSuggestedPrices',
                        'level' => 'info',
                        'message' => 'Product prices are set automatically!'
                    )
                );
            }

            $this->cleanAndRefreshCaches();
            
            Mage::getModel('core/config')->saveConfig('pricer_options/settings/sync_status', true);
        } else {
            $this->customLog(
                array(
                    'class' => 'UTIL',
                    'function' => 'setSuggestedPrices',
                    'level' => 'info',
                    'message' => 'Product list is empty. Need to import products to Prisync first!'
                )
            );
        }
    }
    
    public function retrieveAllProducts() 
    {
        $productList = array();
        
        $response = $this->cURLRequest('list/product', 'GET', false);
        
        if (empty($response['result']['error'])) {
            while (!empty($response['result']['results'])) {
                $productList = array_merge($productList, $response['result']['results']);
                if ($response['result']['nextURL'] != null) {
                    $startFrom = explode('startFrom', $response['result']['nextURL'])[1];
                    $response = $this->cURLRequest('list/product/startFrom'.$startFrom, 'GET', false);
                } else {
                    $this->customLog(
                        array(
                            'class' => 'UTIL',
                            'function' => 'retrieveAllProducts',
                            'level' => 'info',
                            'message' => 'Got all products from Prisync!'
                        )
                    );
                    break;
                }
            }
        } else {
            $this->customLog(
                array(
                    'class' => 'UTIL',
                    'function' => 'retrieveAllProducts',
                    'level' => 'error',
                    'message' => 'Error occurred while retrieving products from Prisync! - Detail: '
                                        .$response['result']['error']
                )
            );
        }
        
        return $productList;
    }
    
    public function checkCronJob() 
    {
        $errorMsg = '';
        $isCronJobAlive = $this->isCronJobAlive();
        if ($isCronJobAlive === false) {
            $errorMsg = '[UTIL][checkCronJob][ERROR] Magento cron job may not working properly! 
                            Please check your configuration.';
        } else {
            $timespanNow = strtotime('now');
            $timespanLastExecTime = strtotime($isCronJobAlive);
            $diff = ($timespanNow - $timespanLastExecTime) / 86400; // find diff in days
            $diff = floor($diff);
            
            if ($diff > 1) {
                $errorMsg = '[UTIL][checkCronJob][ERROR] Last price sync was more than 1 day ago. 
                                Please make sure Magento cron job is working.';
            } else {
                return true;
            }
        }
        
        if (!empty($errorMsg)) {
            $this->customLog(
                array(
                    'class' => 'UTIL',
                    'function' => 'checkCronJob',
                    'level' => 'error',
                    'message' => 'Error occurred while checking cron jobs! - Detail: '.$errorMsg
                )
            );
            return false;
        }
    }
    
    public function isCronJobAlive() 
    {
        $schedules = Mage::getModel('cron/schedule')->getCollection();
        $schedules->getSelect()->limit(1)->order('executed_at DESC');
        $schedules->addFieldToFilter(
            array('status'),
            array(
                array('eq' => Mage_Cron_Model_Schedule::STATUS_SUCCESS),
                array('eq' => Mage_Cron_Model_Schedule::STATUS_PENDING)
            )
        );

        $schedules->addFieldToFilter('job_code', 'pricer_heartbeat');

        $schedules->load();
        
        if (count($schedules) == 0) {
            return false;
        }

        $executedAt = $schedules->getFirstItem()->getExecutedAt();
        $value = Mage::getSingleton('core/date')->date(NULL, $executedAt);
        return $value;
    }

    public function batchImportProducts($productList) 
    {
        $batchImportProductsResponse = $this->cURLRequest('add/batch?priv=1&source=magento', 'POST', $productList);
        
        if (isset($batchImportProductsResponse['errors']) && !empty($batchImportProductsResponse['errors'])) {
            $this->customLog(
                array(
                    'class' => 'UTIL',
                    'function' => 'batchImportProducts',
                    'level' => 'error',
                    'message' => 'Error occurred while batch import products! - Detail: '
                                        .$batchImportProductsResponse['errors']
                )
            );
        }

        return $batchImportProductsResponse;
    }
    
    public function getProduct($productId) 
    {
        $result = false;
        
        // Retrieve product detail
        $getProductDetail = $this->cURLRequest(
            'get/product/id/'.$productId,
            'GET',
            false
        );
        
        if (empty($getProductDetail['result']['error'])) {
            $result = $getProductDetail;

            $this->customLog(
                array(
                    'class' => 'UTIL',
                    'function' => 'getProduct',
                    'level' => 'error',
                    'message' => 
                        'Retrieved Prisync Product ID: '.$productId
                            .' - Detail: '.Mage::helper('core')->jsonEncode($getProductDetail)
                )
            );
        } else {
            $this->customLog(
                array(
                    'class' => 'UTIL',
                    'function' => 'getProduct',
                    'level' => 'error',
                    'message' => 'Prisync Product ID: '.$productId.' - Detail: '.$getProductDetail['result']['error']
                )
            );
        }
        
        return $result;
    }
    
    public function addProduct($product) 
    {
        $result = false;
        
        $addProductResponse = $this->cURLRequest('add/product?priv=1&source=magento', 'POST', $product);
        
        if (isset($addProductResponse['result']['result']) && $addProductResponse['result']['result']) {
            $result = true;
        }
        
        $message = $result ? "successfully done! " : "failed! ";

        $this->customLog(
            array(
                'class' => 'UTIL',
                'function' => 'addProduct',
                'level' => 'info',
                'message' => 'Add product '.$message.'Product ID: '.$product['external_ref']
            )
        );
        return $result;
    }
    
    public function editProduct($product) 
    {
        $result = false;
        $tmpRef = $product['external_ref'];
        
        // Retrieve product id
        $getProductResponse = $this->cURLRequest(
            'get/product/id/'.$product['external_ref'].'?priv=1&source=magento',
            'GET',
            false
        );
        
        if (isset($getProductResponse['result']['id'])) {
            unset($product['external_ref']);
            $editProductResponse = $this->cURLRequest(
                'edit/product/id/'.$getProductResponse['result']['id'],
                'POST',
                $product
            );
            
            if (isset($editProductResponse['result']['result']) && $editProductResponse['result']['result']) {
                $result = true;

                $this->customLog(
                    array(
                        'class' => 'UTIL',
                        'function' => 'editProduct',
                        'level' => 'info',
                        'message' => 'Magento Product ID: '.$tmpRef.' - Detail: Product edited successfully!'
                    )
                );
            } else if (isset($editProductResponse['result']['error'])) {
                $this->customLog(
                    array(
                        'class' => 'UTIL',
                        'function' => 'editProduct',
                        'level' => 'error',
                        'message' => 'Magento Product ID: '.$tmpRef.' - Detail: '
                                        .$editProductResponse['result']['error']
                    )
                );
            }
        } else if (isset($getProductResponse['result']['error'])) {
            $this->customLog(
                array(
                    'class' => 'UTIL',
                    'function' => 'editProduct',
                    'level' => 'error',
                    'message' => 'Magento Product ID: '.$tmpRef.' - Detail: '.$getProductResponse['result']['error']
                )
            );
        }
        
        return $result;
    }
    
    public function deleteProduct($externalRef) 
    {
        // Retrieve product id
        $getProductResponse = $this->cURLRequest(
            'get/product/id/'.$externalRef.'?priv=1&source=magento',
            'GET',
            false
        );
        
        if (isset($getProductResponse['result']['id'])) {
            $deleteProductResponse = $this->cURLRequest(
                'delete/product/id/'.$getProductResponse['result']['id'],
                'POST',
                false
            );
            
            if (isset($deleteProductResponse['result']['error'])) {
                $this->customLog(
                    array(
                        'class' => 'UTIL',
                        'function' => 'deleteProduct',
                        'level' => 'error',
                        'message' => 'Magento Product ID: '.$externalRef.' - Detail: '
                                        .$deleteProductResponse['result']['error']
                    )
                );
            } else {
                $this->customLog(
                    array(
                        'class' => 'UTIL',
                        'function' => 'deleteProduct',
                        'level' => 'info',
                        'message' => 'Magento Product ID: '.$externalRef.' - Detail: Product successfully deleted'
                    )
                );
            }
        } else if (isset($getProductResponse['result']['error'])) {
            $this->customLog(
                array(
                    'class' => 'UTIL',
                    'function' => 'deleteProduct',
                    'level' => 'error',
                    'message' => 'Magento Product ID: '.$externalRef.' - Detail: '
                                    .$getProductResponse['result']['error']
                )
            );
        }
    }
    
    public function addUrlToProduct($url) 
    {
        $tmpRef = $url['external_ref'];
        
        // Retrieve product id
        $getProductResponse = $this->cURLRequest(
            'get/product/id/'.$url['external_ref'].'?priv=1&source=magento',
            'GET',
            false
        );
        
        if (isset($getProductResponse['result']['id'])) {
            unset($url['external_ref']);
            $url['product_id'] = $getProductResponse['result']['id'];
            
            $addUrlToProductResponse = $this->cURLRequest('add/url?priv=1&source=magento', 'POST', $url);
            
            if (isset($addUrlToProductResponse['result']['error'])) {
                $this->customLog(
                    array(
                        'class' => 'UTIL',
                        'function' => 'addUrlToProduct',
                        'level' => 'error',
                        'message' => 'Magento Product ID: '.$tmpRef
                                        .' - Detail: '.$addUrlToProductResponse['result']['error']
                    )
                );
            } else {
                $this->customLog(
                    array(
                        'class' => 'UTIL',
                        'function' => 'addUrlToProduct',
                        'level' => 'info',
                        'message' => 'Magento Product ID: '.$tmpRef.' - Detail: Url successfully added to product!'
                    )
                );
            }
        } else if (isset($getProductResponse['result']['error'])) {
            $this->customLog(
                array(
                    'class' => 'UTIL',
                    'function' => 'addUrlToProduct',
                    'level' => 'error',
                    'message' => 'Magento Product ID: '.$tmpRef.' - Detail: '.$getProductResponse['result']['error']
                )
            );
        }
    }
    
    public function getBatchImportProgress() 
    {
        $getBatchImportProgressResponse = $this->cURLRequest("progress/batchImport", 'GET', false);
        
        if (!empty($getBatchImportProgressResponse['result']['errors'])) {
            $this->customLog(
                array(
                    'class' => 'UTIL',
                    'function' => 'getBatchImportProgress',
                    'level' => 'error',
                    'message' => 'Error occurred while get batch import progress. Details: '
                                    .Mage::helper('core')
                                        ->jsonEncode($getBatchImportProgressResponse['result']['errors'])
                )
            );
        }

        $this->returnAjax($getBatchImportProgressResponse);
    }
    
    public function cURLRequest($endpoint, $requestType, $postData = false, $header = false)
    {
        $response = array();

        //Set header
        if ($header == false) {
            $header = array();
            array_push($header, "Apikey: ".Mage::getStoreConfig('pricer_options/settings/email'));
            array_push($header, "Apitoken: ".Mage::getStoreConfig('pricer_options/settings/apikey'));
        }

        //Set request url
        $requestUrl = $this->apiUrl.$endpoint;

        $curl = new Varien_Http_Adapter_Curl();

        //Set data if request type post
        if ($postData !== false) {
            $curl->write($requestType, $requestUrl, '1.0', $header, http_build_query($postData));
        } else {
            $curl->write($requestType, $requestUrl, '1.0', $header);
        }

        $curlResponse = $curl->read();

        if ($curlResponse != false) {
            $curlResponse = Zend_Http_Response::extractBody($curlResponse);
            $response['result'] = Mage::helper('core')->jsonDecode($curlResponse, true);
        }
        
        if ($curl->getError()) {
            $response['error'] = 'Error:' . $curl->getError();

            $this->customLog(
                array(
                    'class' => 'UTIL',
                    'function' => 'cURLRequest',
                    'level' => 'error',
                    'message' => ' Requested Url: '.$requestUrl.' - Details: '.$response['error']
                )
            );
        }

        $curl->close();
        
        return $response;
    }
    
    public function returnAjax($response) 
    {
        // Make sure the content type for this response is JSON
        Mage::app()->getFrontController()->getResponse()->clearHeaders()->setHeader(
            'Content-type',
            'application/json'
        );
        
        // Set the response body / contents to be the JSON data
        Mage::app()->getFrontController()->getResponse()->setBody(
            Mage::helper('core')->jsonEncode($response)
        );
    }
    
    public function packageProductInfo($productModel, $isBatch = false, $parentProduct = false)
    {
        try {
            $response = array();
            $externalRef = $productModel->getId();

            // Product package
            $product['external_ref'] = $externalRef;
            $product['name'] = $productModel->getData('name');
            $product['product_code'] = $productModel->getData('sku');

            // Set product brand
            if ($productModel->getAttributeText('manufacturer') != false) {
                $productBrand = $productModel->getAttributeText('manufacturer');
            } else {
                $productBrand = 'No Brand';
                $this->customLog(
                    array(
                        'class' => 'UTIL',
                        'function' => 'packageProductInfo',
                        'level' => 'info',
                        'message' => 'Product ID: '.$externalRef.' has not a specific brand name.'
                    )
                );
            }

            $product['brand'] = $productBrand;

            // Set product category
            if ($productModel->getCategoryIds()[0] !== null) {
                $productCategory = Mage::getModel('catalog/category')
                                    ->load($productModel->getCategoryIds()[0])->getName();
            } else {
                $productCategory = 'No Category';
                $this->customLog(
                    array(
                        'class' => 'UTIL',
                        'function' => 'packageProductInfo',
                        'level' => 'info',
                        'message' => 'Product ID: '.$externalRef.' has not a specific category name.'
                    )
                );
            }

            $product['category'] = $productCategory;

            // Set product cost
            if ($productModel->getData('cost') !== null && $productModel->getData('cost') !== '') {
                $product['cost'] = $productModel->getData('cost');
            } else {
                $this->customLog(
                    array(
                        'class' => 'UTIL',
                        'function' => 'packageProductInfo',
                        'level' => 'info',
                        'message' => 'Product ID: '.$externalRef.' has not a specific cost value.'
                    )
                );
            }

            $productPrice = $isBatch ? $productModel->getData('price') : '';

            if ($parentProduct) {
                $productModel = $parentProduct;
            }

            $urlPath = $productModel->getData('url_path');
            if ($urlPath === '' || $urlPath === null) {
                $productUrl = $productModel->getProductUrl();
            } else {
                $productUrl = $productModel->getUrlInStore();
            }

            // Add #externalRef to end of url if its an associative product
            $productUrl = $parentProduct ? rtrim($productUrl, '/').'#'.$externalRef : $productUrl;

            if ($isBatch) {
                $product['price'] = $productPrice;
                $product['url'] = $productUrl;
            } else {
                // Url package
                $url['url'] = $productUrl;
                $url['external_ref'] = $externalRef;

                $response['url'] = $url;
            }

            // Check product package to ensure name, brand and category keys are exist!
            $productArrayKeys = array_keys($product);
            $mustHaveKeys = array('name', 'brand', 'category');

            foreach ($mustHaveKeys as $key) {
                if (!in_array($key, $productArrayKeys)) {
                    $product[$key] = 'No '.$key;
                }
            }

            $response['product'] = $product;

            return $response;
        } catch (Exception $e) {
            $this->customLog(
                array(
                    'class' => 'UTIL',
                    'function' => 'packageProductInfo',
                    'level' => 'error',
                    'message' => 'Error occurred while creating product package! Details: '.$e->getMessage()
                )
            );
        }
    }

    public function getAllStoresAsOptions()
    {
        $options = array();
        $allStores = Mage::app()->getStores();

        foreach ($allStores as $storeId => $storeObj) {
            $storeWebsite = $storeObj->getWebsite()->getName();
            $storeName = $storeObj->getName();
            $storeId = $storeObj->getId();

            $options[$storeId] = $storeWebsite." - ".$storeName;
        }

        return $options;
    }

    public function getDefaultStoreId()
    {
        $defaultStoreId = null;

        try {
            $defaultStoreId = Mage::app()->getWebsite(true)->getDefaultGroup()->getDefaultStoreId();
        } catch (Exception $e) {
            $this->customLog(
                array(
                    'class' => 'UTIL',
                    'function' => 'getDefaultStoreId',
                    'level' => 'error',
                    'message' => 'Error occurred while getting default store id! Details: '.$e->getMessage()
                )
            );
        }

        return $defaultStoreId;
    }

    // This functions make a request to Prisync APIv2 /account endpoint to track extension uninstallation
    public function extensionHeartbeat()
    {
        $query = array();
        $query['priv'] = 1;
        $query['source'] = 'magento';
        $query['info']['api_status'] = Mage::getStoreConfig('pricer_options/settings/api_status');
        $query['info']['package_type'] = Mage::getStoreConfig('pricer_options/settings/package_type');
        $query['info']['package_limit'] = Mage::getStoreConfig('pricer_options/settings/package_limit');
        $query['info']['company_status'] = Mage::getStoreConfig('pricer_options/settings/company_status');
        $query['info']['remaining_trial_days'] = Mage::getStoreConfig('pricer_options/settings/remaining_trial_days');
        $query['info']['email'] = Mage::getStoreConfig('pricer_options/settings/email');
        $query['info']['apikey'] = Mage::getStoreConfig('pricer_options/settings/apikey');
        $query['info']['sync_status'] = Mage::getStoreConfig('pricer_options/settings/sync_status');
        $query['info']['store_id'] = Mage::getStoreConfig('pricer_options/settings/store_id');
        $query['info']['enable_debug'] = Mage::getStoreConfig('pricer_options/settings/enable_debug');
        $query['info']['auto_sync'] = Mage::getStoreConfig('pricer_options/settings/auto_sync');
        $query['info']['set_price_auto'] = Mage::getStoreConfig('pricer_options/settings/set_price_auto');
        $query['info']['price_attr'] = Mage::getStoreConfig('pricer_options/settings/price_attr');
        $query['info']['edition'] = Mage::getEdition();
        $query['info']['version'] = Mage::getVersion();
        $query['info']['prisync_app'] = Mage::getConfig()->getNode()->modules->Prisync_Pricer;
        $query['info']['cron'] = $this->checkCronJob() == true ? 'working' : 'not working';

        $this->cURLRequest('account?'.http_build_query($query), 'GET');
    }

    // This functions make a request to Prisync APIv2 /debug endpoint to get debug flag and save it as config
    public function extensionDebug()
    {
        $extensionDebugResponse = $this->cURLRequest('debug', 'GET');

        if (!empty($extensionDebugResponse['result']['error'])) {
            $this->customLog(
                array(
                    'class' => 'UTIL',
                    'function' => 'extensionDebug',
                    'level' => 'error',
                    'message' => 'Error occurred while getting extension debug flag! Details: '
                                        .$extensionDebugResponse['result']['error']
                )
            );
        } else if (isset($extensionDebugResponse['result']['debug'])) {
            $enableExtensionDebug = $extensionDebugResponse['result']['debug'] ? 1 : 0;

            Mage::getModel('core/config')->saveConfig(
                'pricer_options/settings/enable_debug',
                $enableExtensionDebug
            );
        }
    }

    /**
     * Return store-website info for given store id.
     *
     * @param $id mixed Store Id
     * @return string
     */
    public function getStoreInfo($id = false)
    {
        if (!$id) {
            $id =  Mage::getStoreConfig('pricer_options/settings/store_id');
        }

        $store = Mage::getModel('core/store')->load($id);

        $storeName = $store->getName();
        $storeWebsiteName = $store->getWebsite()->getName();

        return $storeName." - ".$storeWebsiteName;
    }

    /**
     * This function determines where to log (Magento log file or Loggly) and log givem message
     * @param $message array
     */
    public function customLog($message)
    {
        try {
            $companyEmail = Mage::getStoreConfig('pricer_options/settings/email');
            $companyEmail = str_replace('@', '_', $companyEmail);

            $enableDebug = Mage::getStoreConfig('pricer_options/settings/enable_debug');
            if ($enableDebug) {
                try {
                    $message = Mage::helper('core')->jsonEncode($message);

                    $socketAdapter = new Zend_Http_Client_Adapter_Socket();
                    $socketAdapter->setConfig(array('timeout'=>self::LOGGLY_CONN_TIMEOUT));
                    $socketAdapter->connect(self::LOGGLY_HOST, self::LOGGLY_SECURE_PORT, true);

                    $headers = array(
                        'Host' => self::LOGGLY_HOST,
                        'User-Agent' => self::LOGGLY_USERAGENT,
                        'Content-Type' => 'application/x-www-form-urlencoded',
                        'Content-Length' => strlen($message),
                        'Connection' => 'Close'
                    );

                    $method = 'POST';

                    $url = 'https://'.self::LOGGLY_HOST.'/inputs/'.self::LOGGLY_TOKEN."/tag/".$companyEmail."/";

                    $uri = Zend_Uri_Http::fromString($url);
                    $uri->setPort(self::LOGGLY_SECURE_PORT);

                    $socketAdapter->write($method, $uri, '1.1', $headers, $message);
                    $socketAdapter->close();
                } catch (Exception $e) {
                    Mage::log(
                        'Error occurred while trying to log to Loggly! Details: '.$e->getMessage(),
                        LOG_INFO,
                        'prisync-pricer.log'
                    );

                    Mage::log(
                        json_encode($message),
                        LOG_INFO,
                        'prisync-pricer.log'
                    );
                }
            } else {
                Mage::log(
                    json_encode($message),
                    LOG_INFO,
                    'prisync-pricer.log'
                );
            }
        } catch (Exception $e) {
            Mage::log(
                '[UTIL][customLog] Error occurred while logging! Error message: '.$e->getMessage(),
                LOG_INFO,
                'prisync-pricer.log'
            );
        }
    }

    /**
     * Clean and refresh cache after setting Suggested Prices
     */
    private function cleanAndRefreshCaches()
    {
        $types = array('config','layout','block_html','translate','collections','eav','config_api','config_api2');

        foreach ($types as $type) {
            $c = Mage::app()->getCacheInstance()->cleanType($type);
            Mage::dispatchEvent('adminhtml_cache_refresh_type', array('type' => $type));
        }

        // This event listening by LiteMag especially to flush its cache.
        Mage::dispatchEvent('controller_action_postdispatch_adminhtml_cache_flushAll');
    }
}
