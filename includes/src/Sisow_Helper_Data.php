<?php
class Sisow_Helper_Data extends Mage_Payment_Helper_Data
{
	public function GetNewMailConfig($method)
	{
		$config = Mage::getStoreConfig('payment/'.$method.'/newordermail');
		
		if($config == "general")
			return Mage::getStoreConfig('sisow_core/newordermail');
		else
			return $config;
	}
}
?>