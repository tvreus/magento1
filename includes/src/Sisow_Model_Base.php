<?php
class Sisow_Model_Base extends Mage_Core_Model_Abstract
{
	public static $issuers; // = null;
	public static $lastcheck; // = 0;

	private $response;

	// Merchant data
	private $merchantId;
	private $merchantKey;

	// Transaction data
	public $shopId;
	public $payment;	// empty=iDEAL; sofort=DIRECTebanking; mistercash=MisterCash; ...
	public $issuerId;	// mandatory; sisow bank code
	public $purchaseId;	// mandatory; max 16 alphanumeric
	public $entranceCode;	// max 40 strict alphanumeric (letters and numbers only)
	public $description;	// mandatory; max 32 alphanumeric
	public $amount;		// mandatory; min 0.45
	public $notifyUrl;
	public $returnUrl;	// mandatory
	public $cancelUrl;
	public $callbackUrl;

	// Status data
	public $status;
	public $timeStamp;
	public $consumerAccount;
	public $consumerIban;
	public $consumerBic;
	public $consumerName;
	public $consumerCity;
	public $active;

	// Result/check data
	public $trxId;
	public $issuerUrl;
	public $invoiceNo;
	public $documentId;
	
	// Klarna Factuur/Account
	public $pendingKlarna;
	public $monthly;
	public $pclass;
	public $intrestRate;
	public $invoiceFee;
	public $months;
	public $startFee;

	// Error data
	public $errorCode;
	public $errorMessage;

	// Status
	const statusSuccess = "Success";
	const statusCancelled = "Cancelled";
	const statusExpired = "Expired";
	const statusFailure = "Failure";
	const statusOpen = "Open";

    /**
     * Init resource model
     */
    protected function _construct()
    {
        //$this->_init('sisow/base');
		$this->merchantId = Mage::getStoreConfig('sisow_core/merchantid');
		$this->merchantKey = Mage::getStoreConfig('sisow_core/merchantkey');
    }

	/*public function __construct() { //$merchantid, $merchantkey) {
		//$this->merchantId = $merchantid;
		//$this->merchantKey = $merchantkey;
	}*/

	private function error() {
		$this->errorCode = $this->parse("errorcode");
		$this->errorMessage = urldecode($this->parse("errormessage"));
	}

	private function parse($search, $xml = false) {
		if ($xml === false) {
			$xml = $this->response;
		}
		
		if (($start = strpos($xml, "<" . $search . ">")) === false) {
			return false;
		}
		$start += strlen($search) + 2;
		if (($end = strpos($xml, "</" . $search . ">", $start)) === false) {
			return false;
		}
		return substr($xml, $start, $end - $start);
	}

	private function send($method, array $keyvalue = NULL, $return = 1) {
		$url = "https://www.sisow.nl/Sisow/iDeal/RestHandler.ashx/" . $method;
		$options = array(
			CURLOPT_POST => 1,
			CURLOPT_HEADER => 0,
			CURLOPT_URL => $url,
			CURLOPT_FRESH_CONNECT => 1,
			CURLOPT_RETURNTRANSFER => $return,
			CURLOPT_FORBID_REUSE => 1,
			CURLOPT_TIMEOUT => 120,
			CURLOPT_SSL_VERIFYPEER => 0,
			CURLOPT_POSTFIELDS => $keyvalue == NULL ? "" : http_build_query($keyvalue, '', '&'));
		$ch = curl_init();
		curl_setopt_array($ch, $options);
		$this->response = curl_exec($ch);
		curl_close($ch); 
		if (!$this->response) {
			return false;
		}
		return true;
	}

	private function getDirectory() {
		$diff = 24 * 60 * 60;
		if (self::$lastcheck)
			$diff = time() - self::$lastcheck;
		if ($diff < 24 * 60 * 60)
			return 0;
		//$this->Report('Magento DirectoryRequest ' . self::$lastcheck);
		if (!$this->send("DirectoryRequest"))
			return -1;
		$search = $this->parse("directory");
		if (!$search) {
			$this->error();
			return -2;
		}
		self::$issuers = array();
		$iss = explode("<issuer>", str_replace("</issuer>", "", $search));
		foreach ($iss as $k => $v) {
			$issuerid = $this->parse("issuerid", $v);
			$issuername = $this->parse("issuername", $v);
			if ($issuerid && $issuername) {
				self::$issuers[$issuerid] = $issuername;
			}
		}
		self::$lastcheck = time();
		//$this->Report('Magento lastcheck ' . self::$lastcheck);
		return 0;
	}

	// DirectoryRequest
	public function DirectoryRequest(&$output, $select = false, $test = false) {
		if ($test === true) {
			// kan ook via de gateway aangevraagd worden, maar is altijd hetzelfde
			if ($select === true) {
				$output = "<select id=\"sisowbank\" name=\"issuerid\">";
				$output .= "<option value=\"99\">Sisow Bank (test)</option>";
				$output .= "</select>";
			}
			else {
				$output = array("99" => "Sisow Bank (test)");
			}
			return 0;
		}
		$output = false;
		$ex = $this->getDirectory();
		if ($ex < 0) {
			return $ex;
		}
		if ($select === true) {
			$output = "<select id=\"sisowbank\" name=\"issuerid\">";
		}
		else {
			$output = array();
		}
		foreach (Sisow_Model_Base::$issuers as $k => $v) {
			if ($select === true) {
				$output .= "<option value=\"" . $k . "\">" . $v . "</option>";
			}
			else {
				$output[$k] = $v;
			}
		}
		if ($select === true) {
			$output .= "</select>";
		}
		return 0;
	}
	
	public function Report($message) {
		if ($message) {
			$arr['message'] = $message;
			$this->send('Report', $arr);
		}
	}

	// TransactionRequest
	public function TransactionRequest($keyvalue = NULL) {
		if(!isset($this->shopId))
			$this->shopId = "";
			
		$this->trxId = $this->issuerUrl = "";
		if (!$this->merchantId)
			return -1;
		if (!$this->merchantKey)
			return -2;
		if (!$this->purchaseId)
			return -3;
		if ($this->amount < 0.45)
			return -4;
		if (!$this->description)
			return -5;
		if (!$this->returnUrl)
			return -6;
		if (!$this->issuerId && !$this->payment)
			return -7;
		if (!$this->entranceCode)
			$this->entranceCode = $this->purchaseId;
		$pars = array();
		
		if(strlen($keyvalue['billing_countrycode']) == 2)
			$pars["locale"] = $this->setLocale($keyvalue['billing_countrycode']);
		else
			$pars["locale"] = $this->setLocale("");
		
		$pars["merchantid"] = $this->merchantId;
		$pars["payment"] = $this->payment;
		$pars["issuerid"] = $this->issuerId;
		$pars["shopid"] = $this->shopId;
		$pars["purchaseid"] = $this->purchaseId; 
		$pars["amount"] = round($this->amount * 100);
		$pars["description"] = $this->description;
		$pars["entrancecode"] = $this->entranceCode;
		$pars["returnurl"] = $this->returnUrl;
		$pars["cancelurl"] = $this->cancelUrl;
		$pars["callbackurl"] = $this->callbackUrl;
		$pars["notifyurl"] = $this->notifyUrl;
		$pars["sha1"] = sha1($this->purchaseId . $this->entranceCode . round($this->amount * 100) . $this->shopId . $this->merchantId . $this->merchantKey);
		if ($keyvalue) {
			foreach ($keyvalue as $k => $v) {
				$pars[$k] = $v;
			}
		}
		if (!$this->send("TransactionRequest", $pars))
			return -8;
		$this->trxId = $this->parse("trxid");
		$this->invoiceNo = $this->parse("invoiceno");
		$this->documentId = $this->parse("documentid");
		$this->sha = $this->parse("sha1");
		$url = $this->parse("issuerurl");
		
		/*
		if($this->sha != sha1($this->trxId . '' . $this->merchantId . $this->merchantKey) )
		{
			$this->errorMessage = 'Illegal request';
			return -9;
		}
		*/
		
		$this->issuerUrl = urldecode($url);
		$this->pendingKlarna = $this->parse("pendingklarna") == "true";
		if (!$this->issuerUrl) {
			$this->error();
			return -9;
		}
		return 0;
	}

	// StatusRequest
	public function StatusRequest($trxid = false) {
		if(!isset($this->shopId))
			$this->shopId = "";
			
		if ($trxid === false)
			$trxid = $this->trxId;
		if (!$this->merchantId)
			return -1;
		if (!$this->merchantKey)
			return -2;
		if (!$trxid)
			return -3;
		$this->trxId = $trxid;
		$pars = array();
		$pars["merchantid"] = $this->merchantId;
		$pars["shopid"] = $this->shopId;
		$pars["trxid"] = $this->trxId;
		$pars["sha1"] = sha1($this->trxId . $this->shopId . $this->merchantId . $this->merchantKey);
		if (!$this->send("StatusRequest", $pars))
			return -4;
		$this->status = $this->parse("status");
		if (!$this->status) {
			$this->error();
			return -5;
		}
		$this->timeStamp = $this->parse("timestamp");
		$this->amount = $this->parse("amount") / 100.0;
		$this->consumerAccount = $this->parse("consumeraccount");
		$this->consumerIban = $this->parse("consumeriban");
		$this->consumerBic = $this->parse("consumerbic");
		$this->consumerName = $this->parse("consumername");
		$this->consumerCity = $this->parse("consumercity");
		$this->purchaseId = $this->parse("purchaseid");
		$this->description = $this->parse("description");
		$this->entranceCode = $this->parse("entrancecode");
		return 0;
	}
	
	// checkMerchant
	public function checkMerchant() {
		if (!$this->merchantId)
			return -1;
		if (!$this->merchantKey)
			return -2;
		$pars = array();
		$pars["merchantid"] = $this->merchantId;
		$pars["sha1"] = sha1($this->merchantId . $this->merchantKey);
		
		if (!$this->send("CheckMerchantRequest", $pars))
			return -4;
		$this->timeStamp = $this->parse("timestamp");
		$this->active = $this->parse("active");
		return 0;
	}

	// FetchMonthlyRequest
	public function FetchMonthlyRequest($amt = false) {
		if (!$amt) $amt = round($this->amount * 100);
		else $amt = round($amt * 100);
		$pars = array();
		$pars["merchantid"] = $this->merchantId;
		$pars["amount"] = $amt;
		$pars["sha1"] = sha1($amt . $this->merchantId . $this->merchantKey);
		if (!$this->send("FetchMonthlyRequest", $pars))
			return -1;
		$this->monthly = $this->parse("monthly");
		$this->pclass = $this->parse("pclass");
		$this->intrestRate = $this->parse("intrestRate");
		$this->invoiceFee = $this->parse("invoiceFee");
		$this->months = $this->parse("months");
		$this->startFee = $this->parse("startFee");
		return $this->monthly;
	}

	// RefundRequest
	public function RefundRequest($trxid) {
		$pars = array();
		$pars["merchantid"] = $this->merchantId;
		$pars["amount"] = round($this->amount * 100.0);
		$pars["trxid"] = $trxid;
		$pars["sha1"] = sha1($trxid . $this->merchantId . $this->merchantKey);
		if (!$this->send("RefundRequest", $pars))
			return -1;
		$id = $this->parse("refundid");
		if (!$id) {
			$this->error();
			return -2;
		}
		return $id;
	}

	// InvoiceRequest
	public function InvoiceRequest($trxid, $keyvalue = NULL) {
		$pars = array();
		$pars["merchantid"] = $this->merchantId;
		$pars["trxid"] = $trxid;
		$pars["sha1"] = sha1($trxid . $this->merchantId . $this->merchantKey);
		if ($keyvalue) {
			foreach ($keyvalue as $k => $v) {
				$pars[$k] = $v;
			}
		}
		if (!$this->send("InvoiceRequest", $pars))
			return -1;
		$this->invoiceNo = $this->parse("invoiceno");
		if (!$this->invoiceNo) {
			$this->error();
			return -2;
		}
		$this->documentId = $this->parse("documentid");
		return 0; //$this->invoiceNo;
	}

	// CreditInvoiceRequest
	public function CreditInvoiceRequest($trxid) {
		$pars = array();
		$pars["merchantid"] = $this->merchantId;
		$pars["trxid"] = $trxid;
		$pars["sha1"] = sha1($trxid . $this->merchantId . $this->merchantKey);
		if (!$this->send("CreditInvoiceRequest", $pars))
			return -1;
		$this->invoiceNo = $this->parse("invoiceno");
		if (!$this->invoiceNo) {
			$this->error();
			return -2;
		}
		$this->documentId = $this->parse("documentid");
		return 0; //$this->invoiceNo;
	}

	// CancelReservationRequest
	public function CancelReservationRequest($trxid) {
		$pars = array();
		$pars["merchantid"] = $this->merchantId;
		$pars["trxid"] = $trxid;
		$pars["sha1"] = sha1($trxid . $this->merchantId . $this->merchantKey);
		if (!$this->send("CancelReservationRequest", $pars))
			return -1;
		return 0;
	}
	
	public function GetLink($msg = 'hier', $method = '') {
		if (!$method) {
			if (!$msg)
				$link = 'https://www.sisow.nl/Sisow/Opdrachtgever/download.aspx?merchantid=' .
					$this->merchantId . '&doc=' . $this->documentId . '&sha1=' .
					sha1($this->documentId . $this->merchantId . $this->merchantKey);
			else
				$link = '<a href="https://www.sisow.nl/Sisow/Opdrachtgever/download.aspx?merchantid=' .
					$this->merchantId . '&doc=' . $this->documentId . '&sha1=' .
					sha1($this->documentId . $this->merchantId . $this->merchantKey) . '">' . $msg . '</a>';
			return $link;
		}
		if ($method == 'make') {
		}
	}
	
	public function getMerchantId()
	{
		return $this->merchantId;
	}
	
	public function getMerchantKey()
	{
		return $this->merchantKey;
	}
	
	public function setLocale($countryIso)
	{
		$supported = array("US");
		
		switch($this->payment)
		{
			case "paypalec":
				$supported = array('AU','AT','BE','BR','CA','CH','CN','DE','ES','GB','FR','IT','NL','PL','PT','RU','US');
				break;
			case "mistercash":
				$supported = array('NL', 'BE', 'DE', 'IT', 'ES', 'PT', 'BR', 'SE', 'FR');
				break;
			case "creditcard":
				$supported = array('NL', 'BE', 'DE', 'IT', 'ES', 'PT', 'BR', 'SE', 'FR');
				break;
			case "maestro":
				$supported = array('NL', 'BE', 'DE', 'IT', 'ES', 'PT', 'BR', 'SE', 'FR');
				break;
			case "mastercard":
				$supported = array('NL', 'BE', 'DE', 'IT', 'ES', 'PT', 'BR', 'SE', 'FR');
				break;
			case "visa":
				$supported = array('NL', 'BE', 'DE', 'IT', 'ES', 'PT', 'BR', 'SE', 'FR');
				break;
			default:
				return "NL";
				break;
		}
		
		$lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
		$lang = strtoupper($lang);
		
		$lang = (!isset($lang) || $lang == "") ? $countryIso : $lang;
		
		if($lang == "")
			return "US";
		if(in_array($lang, $supported))
			return $lang;
		else
			return 'US';
	}
}
