<?php

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.web-cooking.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_All
 * @copyright  Copyright (c) 2011-2016 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
class Webcooking_All_Adminhtml_Webcooking_Select2Controller extends Mage_Adminhtml_Controller_Action {

    protected function _construct() {
        $this->setUsedModuleName('Webcooking_All');
    }
    
    protected function _isAllowed() {
        return Mage::getSingleton('admin/session')->isLoggedIn();
    }

    public function indexAction() {
        if (!$this->getRequest()->isAjax()) {
            $this->_forward('noRoute');
            return;
        }
        $search = $this->getRequest()->getParam('search');
        $attributeCode = $this->getRequest()->getParam('attribute_code');
        $entityType = $this->getRequest()->getParam('entity_type');
        $attribute = Mage::helper('wcooall/attribute')->getAttribute($attributeCode, $entityType);
        
        
        $options = array();
        $store  = Mage::app()->getStore();
        $read = Mage::getSingleton('core/resource')->getConnection('core_read');
        if($attribute->getSourceModel()) {
            $sourceModel = Mage::getModel($attribute->getSourceModel());
            $sourceOptions = method_exists($sourceModel, 'getAllOptionsForSelect2') ? $sourceModel->getAllOptionsForSelect2() : $sourceModel->getAllOptions();
            $i = 0;
            foreach($sourceOptions as $option) {
                if(preg_match('%'.preg_quote($search).'%i', $option['label'])) {
                    $options[$i]['id'] = $option['value'];
                    $options[$i]['text'] = $option['label'];
                    $i++;
                }
            }
        } else {
            $select = $read->select()->from(array('a' => Mage::getSingleton('core/resource')->getTableName('eav/attribute_option_value')))
                ->joinLeft(
                    array('b' => Mage::getSingleton('core/resource')->getTableName('eav/attribute_option')),
                    'a.option_id = b.option_id',
                    array()
                )
                ->where('a.store_id = ?', $store->getId())
                ->where('b.attribute_id = ?', $attribute->getId())
                ->where('a.value LIKE ?', '%' . $search . '%');
            
            $data = $read->fetchAll($select);
            
            $i = 0;
            foreach ($data as $opt) {
                $options[$i]['id'] = $opt['option_id'];
                $options[$i]['text'] = $opt['value'];
                $i ++;
            }
        }
        
        
        
        
        
        $this->getResponse()->setBody(Mage::helper('core')->
            jsonEncode($options));
    }
    
}
