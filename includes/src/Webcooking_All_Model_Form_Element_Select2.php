<?php

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.web-cooking.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_All
 * @copyright  Copyright (c) 2011-2016 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
class Webcooking_All_Model_Form_Element_Select2 extends Varien_Data_Form_Element_Select
{
      public function getElementHtml()
    {
        $html = '
       <p>
            <input type="hidden"
                id = "' . $this->getHtmlId() . '"
                name = "' . $this->getName() . '"
                value = "' . $this->getEscapedValue() .  '"
                data-placeholder="' . Mage::helper('wcooall')->__('-- Please select --') . '" style="width:98%;"/>
        </p>';
        $defaultOptionText = $this->getDefaultOptionText();
        $defaultOptionText = str_replace("'", "\'", $defaultOptionText);
        $searchText = Mage::helper('wcooall')->__('Search');
        $js = <<<EOF
<script type="text/javascript">
jQuery.noConflict()(document).ready(function() {
        jQuery.noConflict()('#{$this->getHtmlId()}').select2({
            minimumInputLength: 3,
            placeholder: '{$searchText}',
            ajax: {
                url: '{$this->getLink()}',
                dataType: 'json',
                data: function(term, page) {
                    return {$this->getJsonData()};
                },
                results: function (data, page) {
                    return { results: data };
                }
            },
            initSelection : function (element, callback) {
                var data = {id: '{$this->getValue()}', text: '{$defaultOptionText}'};
                callback(data);
            }
        });
    });
</script>
EOF;
                
        
        $html .= $this->getAfterElementHtml();
        $html .= $js;
        return $html;
    }
    
    public function getLink() {
        if(!$this->hasData('link')) {
            $this->setData('link', Mage::getModel('adminhtml/url')->getUrl('adminhtml/webcooking_select2'));
        }
        return $this->getData('link');
    }
    
    public function getDefaultOptionText() {
        if(!$this->hasData('default_option_text')) {
            if(Mage::registry('current_product')) {
                $this->setData('default_option_text', Mage::registry('current_product')->getAttributeText($this->getId())?Mage::registry('current_product')->getAttributeText($this->getId()):Mage::registry('current_product')->getData($this->getId()));
            }
            if(Mage::registry('current_category')) {
                $this->setData('default_option_text', Mage::registry('current_category')->getAttributeText($this->getId())?Mage::registry('current_category')->getAttributeText($this->getId()):Mage::registry('current_category')->getData($this->getId()));
            }
        }
        return $this->getData('default_option_text');
    }
    
    public function getJsonData() {
        if(!$this->hasData('json_data')) {
            $entityType = Mage::registry('current_product')?'catalog_product':'';
            if(!$entityType && Mage::registry('current_category')) {
                $entityType = 'catalog_category';
            }
            $this->setData('json_data', "{
                        search: term,
                        attribute_code: '{$this->getId()}',
                        entity_type: '{$entityType}'
                    }");
        }
        return $this->getData('json_data');
        
    }
}
