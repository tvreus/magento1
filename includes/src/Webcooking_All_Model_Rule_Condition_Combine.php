<?php

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.enjalbert.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_All
 * @copyright  Copyright (c) 2011-2016 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
class Webcooking_All_Model_Rule_Condition_Combine extends Mage_Rule_Model_Condition_Combine {

   public function __construct()
    {
        parent::__construct();
        $this->setType($this->getRuleConditionCombineClass());
    }
    
    public function loadAggregatorOptions()
    {
        $this->setAggregatorOption(
            array(
                'all' => Mage::helper('rule')->__('ALL'),
                'any' => Mage::helper('rule')->__('ANY'),
            )
        );

        return $this;
    }
    
     public function loadValueOptions()
    {
        $this->setValueOption(
            array(
                1 => Mage::helper('rule')->__('TRUE'),
                0 => Mage::helper('rule')->__('FALSE'),
            )
        );

        return $this;
    }
    
    public function getRuleConditionCombineClass() {
        return 'wcooall/rule_condition_combine';
    }
    public function getRuleConditionProductClass() {
        return 'wcooall/rule_condition_product';
    }
    
    public function getNewChildSelectOptions()
    {
     
        $productCondition = Mage::getModel($this->getRuleConditionProductClass());
        $productAttributes = $productCondition->loadAttributeOptions()->getAttributeOption();
        $pAttributes = array();
        foreach ($productAttributes as $code=>$label) {
            $pAttributes[] = array('value'=>$this->getRuleConditionProductClass().'|'.$code, 'label'=>$label);
        }
        
        $conditions = parent::getNewChildSelectOptions();
        $conditions = array_merge_recursive($conditions, array(
            array('value'=>$this->getRuleConditionCombineClass(), 'label'=>Mage::helper('wcooall')->__('Conditions combination')),
            array('label'=>Mage::helper('catalog')->__('Product Attribute'), 'value'=>$pAttributes),
        ));

        $additional = new Varien_Object();
        Mage::dispatchEvent('webcooking_rule_condition_combine', array('additional' => $additional));
        if ($additionalConditions = $additional->getConditions()) {
            $conditions = array_merge_recursive($conditions, $additionalConditions);
        }

        return $conditions;
    }
    
    
    public function applyConditionToProductCollection($productCollection, $defaultCondition="1 = 0")
    {
       
        $condition = $this->getConditionForProductCollection($productCollection);
        if(!$condition) {
            $condition = $defaultCondition;
        }
        
        $productCollection->getSelect()->where($condition);
        $productCollection->getSelect()->group('e.entity_id');
                        
        //echo $productCollection->getSelect().'';
        //die();
        return $productCollection;
    }
    
     public function getConditionForProductCollection($productCollection)
    {
        $conditions = array();
        $aggregator = $this->getAggregator() == 'all' ? ' AND ' : ' OR ';
        $operator   = $this->getValue() ? '' : 'NOT';

        foreach ($this->getConditions() as $condition) {
            $subCondition = $condition->getConditionForProductCollection($productCollection);
            if ($subCondition) {
                $conditions[] = sprintf('%s %s', $operator, $subCondition);
            }
        }

        if ($conditions) {
            return  new Zend_Db_Expr(sprintf('(%s)', join($aggregator, $conditions)));
            
        } 

        return false;
    }
    
}
