<?php

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.web-cooking.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_GoogleUniversalAnalytics
 * @copyright  Copyright (c) 2011-2016 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
class Webcooking_GoogleUniversalAnalytics_Block_Adminhtml_Sales_Order_View_Tab_Gua
    extends Mage_Adminhtml_Block_Sales_Order_Abstract
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('webcooking/googleuniversalanalytics/sales/order/view/tab/gua.phtml');
    }
    
    
    public function getOrder()
    {
        return Mage::registry('current_order');
    }
    
    public function getTabTitle()
    {
        return $this->getTabLabel();
    }
    
    public function getTabLabel()
    {
        return Mage::helper('googleuniversalanalytics')->__('Google Analytics');
    }
    
     public function canShowTab()
    {
        return !$this->getOrder()->getFromShoppingflux();//compatibility with ShoppingFeed extension 
    }

    
    public function isHidden()
    {
        return false;
    }
    
    
    
    public function getAnalyticsData() {
        if(!$this->getApiHelper()->getProfile()) {
            return array();
        }
        $dimensions = array(
            'ga:source'=>'Source',
            'ga:referralPath' => 'Referral Path',
            'ga:campaign' => 'Campaign',
            'ga:medium' => 'Medium',
            'ga:sourceMedium' => 'Source Medium',
            'ga:keyword' => 'Keyword',
            'ga:sessionsToTransaction'=>'Sessions To Transactions',
            'ga:daysToTransaction'=>'Days To Transactions',
            'ga:operatingSystem' => 'OS',
            'ga:operatingSystemVersion' => 'OS Version',
            'ga:browser' => 'Browser',
            'ga:browserVersion' => 'Browser Version',
          //  'ga:productListName' => 'Product List Name'
        );
        
        try {
            $projectId = $this->getApiHelper()->getProfile($this->getOrder()->getStoreId())->getId();
            $data = false;
            $dimensionsChunks = array_chunk(array_keys($dimensions), 7);
            foreach($dimensionsChunks as $dimensionsForRequest) {
                $dimensionsForRequest = implode(',', $dimensionsForRequest);
                $apiResponse =  $this->getApiHelper()->getService()->data_ga->get(
                    'ga:' . $projectId, 
                    substr($this->getOrder()->getCreatedAt(), 0, 10), 
                    substr($this->getOrder()->getUpdatedAt(), 0, 10), 
                    'ga:hits', 
                    array(
                        'dimensions' => $dimensionsForRequest,
                        'filters'=>'ga:transactionId=='.$this->getOrder()->getIncrementId()
                    )
                );
                if(!$data) {
                     $data = $apiResponse;
                } else {
                    $data['rows'] = array_merge($data['rows'], $apiResponse['rows']);
                }
            }
            /*echo "<pre>";
            //var_dump($data['rows']);
            var_dump($data);
            echo "</pre>";*/
            if(count($data['rows'])>0) {
                $dataFormatted = array();
                $i=0;
                foreach($dimensions as $dimensionCode => $dimensionLabel) {
                    $dataFormatted[$dimensionLabel] = $data['rows'][0][$i++];
                } 
                return $dataFormatted;
            }
        } catch (Exception $e) {
            //var_dump($e->getMessage());
            Mage::helper('googleapi')->logException($e, 'gua');
        }
        return array();
    }
    
    public function getApiHelper() {
        return Mage::helper('googleuniversalanalytics/api');
    }
    
     public function getTabClass()
    {
        return 'ajax only';
    }
    
     /**
     * Get Class
     *
     * @return string
     */
    public function getClass()
    {
        return $this->getTabClass();
    }

    /**
     * Get Tab Url
     *
     * @return string
     */
    public function getTabUrl()
    {
        return $this->getUrl('adminhtml/gua_order/guatab', array('_current' => true));
    }


}
