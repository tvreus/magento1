<?php

class Wyomind_Estimateddeliverydate_CustomOptionsController extends Mage_Core_Controller_Front_Action
{

    public function updateAction()
    {
        $data = (Mage::app()->getRequest()->getParams());
        $from = 0;
        $storeId = Mage::app()->getStore()->getId();
        $method = Mage::getStoreConfig("estimateddeliverydate/custom_options/calculation", $storeId);

        foreach (Mage::helper("core")->jsonDecode($data["leadtime_from"]) as $f) {
            if ($method)
                $from+=$f;
            else
            if ($f > $from)
                $from = $f;
        }
        $to = 0;
        foreach (Mage::helper("core")->jsonDecode($data["leadtime_to"]) as $t) {
            if ($method)
                $to+=$t;
            else
            if ($t > $to)
                $to = $t;
        }


        $_product = Mage::helper("estimateddeliverydate/data")->getProduct($data["id"], $storeId);
        $html = Mage::helper("estimateddeliverydate/data")->renderMessage($_product, $storeId, "product", $from, $to);

        $this->getResponse()->setHeader('Content-type', 'text/html');
        $this->getResponse()->setBody($html);
    }

}
