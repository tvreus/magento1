<?php

/*
 * Copyright © 2016 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

class Wyomind_Estimateddeliverydate_Block_Adminhtml_Catalog_Product_Edit_Tab_Attributes extends Mage_Adminhtml_Block_Template
        implements Mage_Adminhtml_Block_Widget_Tab_Interface
{

    public function _construct()
    {
        parent::_construct();
        $this->setTemplate('estimateddeliverydate/catalog/product/tab/estimated-delivery-date.phtml');
    }

    public function getTabLabel()
    {
        return $this->__('Estimated Delivery Date');
    }

    public function getTabTitle()
    {
        return $this->__('Estimated Delivery Date');
    }

    public function canShowTab()
    {
        return Mage::getSingleton('admin/session')->isAllowed('catalog/products/estimateddeliverydate/attributes');
    }

    public function isHidden()
    {
        return !Mage::getSingleton('admin/session')->isAllowed('catalog/products/estimateddeliverydate/attributes');
    }

}
