<?php

/**
 * MageWorx
 * CustomOptions Extension
 * 
 * @category   MageWorx
 * @package    MageWorx_CustomOptions
 * @copyright  Copyright (c) 2015 MageWorx (http://www.mageworx.com/)
 */
class Wyomind_Estimateddeliverydate_Block_Adminhtml_Catalog_Product_Edit_Tab_Customoptions_Options_Type_Select
        extends MageWorx_CustomOptions_Block_Adminhtml_Catalog_Product_Edit_Tab_Options_Type_Select
{

    public function __construct()
    {
        parent::__construct();
        if (!Mage::helper('mageworx_customoptions')->isEnabled())
            return $this;
        $this->setTemplate('estimateddeliverydate/customoptions/catalog-product-edit-options-type-select.phtml');
    }

}
