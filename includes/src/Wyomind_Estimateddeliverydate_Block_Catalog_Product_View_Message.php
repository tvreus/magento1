<?php

/*
 * Copyright © 2016 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

class Wyomind_Estimateddeliverydate_Block_Catalog_Product_View_Message extends Mage_Catalog_Block_Product_View_Options
{

    function getChildrenJson()
    {

        $storeId = Mage::app()->getStore()->getId();
        $_product = $this->getProduct();
        $attributes = array();
        $_attributes = $_product->getTypeInstance(true)->getConfigurableAttributes($_product);
        foreach ($_attributes as $_attribute) {
            $attributes[] = Mage::getModel('eav/config')->getAttribute('catalog_product', $_attribute->getAttributeId());
        }


        $AssociatedProduct = $_product->getTypeInstance()->getUsedProducts();
        $children = array();
        $i = 0;
        foreach ($AssociatedProduct as $child) {
            $child = Mage::getModel('catalog/product')->load($child->getId());
            foreach ($attributes as $attr) {
                $children[$i]["attribute" . $attr->getAttributeId()] = $child->getData($attr->getAttributeCode());
            }
            $children[$i]['message'] = Mage::helper("estimateddeliverydate/data")->renderMessage($child, $storeId);
            $children[$i]['id'] = $child->getId();

            $i++;
        };
        return Mage::helper("core")->jsonEncode($children);
    }

    function getMessage()
    {
        $storeId = Mage::app()->getStore()->getId();
        $_product = $this->getProduct();

        return Mage::helper("estimateddeliverydate/data")->renderMessage($_product, $storeId);
    }

    public function getOptionsLeadtimes()
    {

        $config = array();
        foreach ($this->getOptions() as $option) {
            foreach ($option->getValues() as $value) {
                $config[$option->getId()][$value->getId()] = $value->getLeadtime();
            }
        }
        return Mage::helper('core')->jsonEncode($config);
    }

}
