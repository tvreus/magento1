<?php

class Wyomind_Estimateddeliverydate_Block_Checkout_Cart_Message extends Mage_Checkout_Block_Cart
{

    function _toHtml()
    {
        $storeId = Mage::app()->getStore()->getId();
        $leadtime = array();
        $items = $this->getItems();
        if (!count($items)) {
            return;
        }
        foreach ($items as $item) {
            $options = $this->getCustomOptions($item);

            $product = Mage::getModel("catalog/product")->load($item->getProductId());


            if ($product) {
                if ($product->getTypeId() == "bundle") {
                    $resource = Mage::getSingleton('core/resource');
                    $readConnection = $resource->getConnection('core_read');
                    $sfqi = $resource->getTableName('sales_flat_quote_item');
                    $query = 'SELECT product_id,qty FROM ' . $sfqi . " WHERE parent_item_id = " . $item->getId();
                    $results = $readConnection->fetchAll($query);
                    foreach ($results as $item) {
                        $subProduct = Mage::getModel("catalog/product")->load($item['product_id']);
                        if ($subProduct) {
                            $leadtime[] = Mage::helper("estimateddeliverydate")->getLeadTimes($subProduct, $storeId, 0, 0, $item['qty']);
                        }
                    }
                } else if ($product->getTypeId() == "configurable") {
                    $product = $item->getOptionByCode('simple_product')->getProduct();
                    if ($product != null) {
                        $product = Mage::getModel("catalog/product")->load($product->getId());
                        $additional = Mage::helper("estimateddeliverydate")->getCustomOptions($options, $storeId);
                        $leadtime[] = Mage::helper("estimateddeliverydate")->getLeadTimes($product, $storeId, $additional[0], $additional[1], $item->getQty());
                    }
                } else {
                    $additional = Mage::helper("estimateddeliverydate")->getCustomOptions($options, $storeId);
                    $leadtime[] = Mage::helper("estimateddeliverydate")->getLeadTimes($product, $storeId, $additional[0], $additional[1], $item->getQty());
                }
            }
        }


        $range = Mage::helper("estimateddeliverydate")->getLeadTimeRange($leadtime);

        return Mage::helper("estimateddeliverydate")->renderGlobalMessage($range, $storeId);
    }

    public function getCustomOptions($item) {
        $product = $item->getProduct();
        $options = array();
        $optionIds = $item->getOptionByCode('option_ids');
        if ($optionIds) {
            $options = array();

            foreach (explode(',', $optionIds->getValue()) as $optionId) {
                $option = $product->getOptionById($optionId);
                if ($option) {
                    $itemOption = $item->getOptionByCode('option_' . $option->getId());

                    $options[] = array(
                        'value_id' => $itemOption->getValue()
                    );
                }
            }
        }

        $addOptions = $item->getOptionByCode('additional_options');
        if ($addOptions) {
            $options = array_merge($options, unserialize($addOptions->getValue()));
        }

        return $options;
    }

}
