<?php

/*
 * Copyright © 2016 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */

Class Wyomind_Estimateddeliverydate_Block_Sales_Order_view_Message extends Mage_Core_Block_Messages
{

    function _toHtml()
    {
        if (Mage::getStoreConfig("estimateddeliverydate/global_message/use_message_for_account")) {
            $order = Mage::getModel("sales/order")->load(Mage::app()->getRequest()->getParam("order_id"));
            return "<div class='estimated-delivery-date'>" . $order->getEstimatedDeliveryDate() . "</div>";
        }
    }

}
