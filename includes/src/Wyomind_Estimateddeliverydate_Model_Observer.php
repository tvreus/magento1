<?php

class Wyomind_Estimateddeliverydate_Model_Observer
{

    /**
     * Flag to stop observer executing more than once
     *
     * @var static bool
     */
    static protected $_singletonFlag = false;

    public function saveConfig($observer)
    {
        $attributes = explode(",", Mage::getStoreConfig("estimateddeliverydate/attributes/list"));
        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('core_read');
        $write = $resource->getConnection('core_write');
        $tableEal = Mage::getSingleton("core/resource")->getTableName("eav_attribute_leadtime");
        $sql = array();
        $value_ids = array();
        foreach ($attributes as $attribute) {


            $attribute = Mage::getModel('catalog/resource_eav_attribute')->load($attribute);
            $values = $attribute->getSource()->getAllOptions();
            foreach ($values as $value) {

                if ((string) $value["value"] != '') {
                    $value_ids[] = $value['value'];
                    $sql[] = "SELECT @value:=IF(COUNT(value)<1,0,value) FROM $tableEal WHERE attribute_id=" . $attribute['attribute_id'] . " AND value_id=" . $value['value'] . ";";
                    $sql[] = "REPLACE INTO $tableEal (attribute_id,value_id,value) VALUES (" . $attribute['attribute_id'] . "," . $value['value'] . ",@value);";
                }
            }
        }
        $sql[] = "SELECT NULL;";
        $sql[] = "DELETE FROM $tableEal WHERE attribute_id NOT IN ('" . implode("','", $attributes) . "')";
        $sql[] = "SELECT NULL;";
        $sql[] = "DELETE FROM $tableEal WHERE value_id NOT IN ('" . implode("','", $value_ids) . "')";
        $commit = true;


        foreach ($sql as $k => $s) {

            try {
                if ($k % 2 == 0) {
                    $read->fetchAll($s);
                } else {
                    $write->exec($s);
                }
            } catch (Mage_Core_Exception $e) {
                $commit = false;
            } catch (Exception $e) {

                $commit = false;
            }
        }
        if (!$commit) {
            Mage::getSingleton("core/session")->addError(Mage::helper("estimateddeliverydate")->__("Error while processing. Rollback happened."));
            $write->rollback();
            return false;
        } else {
            $write->commit();
            Mage::getSingleton("core/session")->addSuccess(Mage::helper("estimateddeliverydate")->__("Leadtime/Attribute updated. Go to <a href='" . Mage::helper("adminhtml")->getUrl("adminhtml/manageleadtimes/index/") . "'>Catalog > Attributes > Manage Leadtime/Attribute</a>."));
        }


        $shippingJson = Mage::getStoreConfig("estimateddeliverydate/shipping/json");

        if (!$this->isJson($shippingJson)) {
            Mage::getSingleton("core/session")->addError(Mage::helper("estimateddeliverydate")->__("Error : invalid json string in shipping methods leadtimes."));
        }
    }

    public function isJson($string)
    {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }

    public function saveProductTabData(Varien_Event_Observer $observer)
    {

        if (!self::$_singletonFlag) {
            self::$_singletonFlag = true;
            Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
            $product = $observer->getEvent()->getProduct();

            try {

                $fields = Mage::helper("estimateddeliverydate")->getConfig();
                $data = $this->_getRequest()->getPost();


                foreach (array("orders", "backorders") as $type) {
                    foreach ($fields as $name => $field) {
                        $storeId = Mage::app()->getRequest()->getParam('store');
                        $attributeCode = Mage::helper("estimateddeliverydate")->getAttributeShorten($name);
                        $value = (isset($data['product'][$type . "_" . $attributeCode])) ? $data['product'][$type . "_" . $attributeCode] : null;
                        $useConfigValue = (isset($data['product']["use_config_" . $type . "_" . $attributeCode])) ? $data['product']["use_config_" . $type . "_" . $attributeCode] : null;
                        $useBaseValue = (isset($data['product']["use_base_" . $type . "_" . $attributeCode])) ? $data['product']["use_base_" . $type . "_" . $attributeCode] : null;


                        if ($storeId != Mage_Core_Model_App::ADMIN_STORE_ID) {
                            $product->setStoreId($storeId);
                        }

                        if ($useConfigValue != 1 && $useConfigValue != null) {
                            $useConfigValue = 0;
                        }

                        if ($useBaseValue != 1 && $useBaseValue != null && ($storeId != Mage_Core_Model_App::ADMIN_STORE_ID)) {
                            $useBaseValue = 0;
                        }

                        if ($value != null) {
                            $product->setData($type . "_" . $name, $value);
                        }

                        if ($useConfigValue != null) {

                            $product->setData("use_config_" . $type . "_" . $attributeCode, $useConfigValue);
                        }


                        if ($storeId != Mage_Core_Model_App::ADMIN_STORE_ID && $useBaseValue !== null) {

                            $product->setData("use_base_" . $type . "_" . $attributeCode, $useBaseValue);
                        }
                    }
                }
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
    }

    /**
     * Retrieve the product model
     *
     * @return Mage_Catalog_Model_Product $product
     */
    public function getProduct()
    {
        return Mage::registry('product');
    }

    /**
     * Shortcut to getRequest
     *
     */
    protected function _getRequest()
    {
        return Mage::app()->getRequest();
    }

    public function quoteUpdate($observer)
    {
        $quote = $observer->getEvent()->getQuote();
        $storeId = Mage::app()->getStore()->getId();
        $leadtime = array();

        foreach ($quote->getAllItems() as $item) {
            $product = Mage::getModel("catalog/product")->load($item->getProductId());

            if ($product->getTypeId() == "configurable") {
                $product = $item->getOptionByCode('simple_product')->getProduct();
                if ($product != null) {
                    $product = Mage::getModel("catalog/product")->load($product->getId());
                    $leadtime[] = Mage::helper("estimateddeliverydate")->getLeadTimes($product, $storeId, 0, 0, $item->getQty());
                    $item->setEstimatedDeliveryDate(strip_tags(Mage::helper("estimateddeliverydate")->renderMessage($product, $storeId, "item_email", 0, 0, $item->getQty())));
                    $item->setStockStatus(Mage::helper("estimateddeliverydate")->getStockStatus($product, $storeId, $item->getQty()));
                }
            } else {
                $options = $this->getCustomOptions($item);
                $additional = Mage::helper("estimateddeliverydate")->getCustomOptions($options, $storeId);
                $leadtime[] = Mage::helper("estimateddeliverydate")->getLeadTimes($product, $storeId, $additional[0], $additional[1], $item->getQty());
                $item->setEstimatedDeliveryDate(strip_tags(Mage::helper("estimateddeliverydate")->renderMessage($product, $storeId, "item_email", $additional[0], $additional[1], $item->getQty())));
                $item->setStockStatus(Mage::helper("estimateddeliverydate")->getStockStatus($product, $storeId, $item->getQty()));
            }
        }

        $range = Mage::helper("estimateddeliverydate")->getLeadTimeRange($leadtime);
        $msg = Mage::helper("estimateddeliverydate")->renderGlobalMessage($range, $storeId, "quote") . "</div>";

        $quote->setEstimatedDeliveryDate(strip_tags($msg));
    }

    public function getCustomOptions($item) {
        $product = $item->getProduct();
        // $options = array();
        $optionIds = $item->getOptionByCode('option_ids');
        $options = array();
        if ($optionIds) {

            foreach (explode(',', $optionIds->getValue()) as $optionId) {
                $option = $product->getOptionById($optionId);
                if ($option) {
                    $itemOption = $item->getOptionByCode('option_' . $option->getId());

                    $options[] = array(
                        'value_id' => $itemOption->getValue()
                    );
                }
            }
        }

        $addOptions = $item->getOptionByCode('additional_options');
        if ($addOptions) {
            $options = array_merge($options, unserialize($addOptions->getValue()));
        }

        return $options;
    }

    public function orderUpdate($observer)
    {
        $order = $observer->getEvent()->getOrder();
        $order->addStatusHistoryComment($order->getEstimatedDeliveryDate(), false);
        $order->save();
    }

    public function orderItemUpdate($observer)
    {
        $observer->getOrderItem()->setData("estimated_delivery_date", $observer->getItem()->getData('estimated_delivery_date'));
        $observer->getOrderItem()->setData("stock_status", $observer->getItem()->getData('stock_status'));
        return $observer;
    }

}
