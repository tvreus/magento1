<?php

class Wyomind_Estimateddeliverydate_Model_System_Config_Source_Customoptions
{

    public function toOptionArray()
    {
        return array(
            array("value" => 0, "label" => __('sum of all custom option leadtime')),
            array("value" => 1, "label" => __('highest custom option leadtime')),
        );
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return $this->toOptionArray();
    }

}
