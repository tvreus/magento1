<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Core
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * @category   Magethrow
 * @package    Mage_Core
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Mage_Core_Model_Domainpolicy
{
    /**
     * X-Frame-Options allow (header is absent)
     */
    const FRAME_POLICY_ALLOW = 1;

    /**
     * X-Frame-Options SAMEORIGIN
     */
    const FRAME_POLICY_ORIGIN = 2;

    /**
     * Path to backend domain policy settings
     */
    const XML_DOMAIN_POLICY_BACKEND = 'admin/security/domain_policy_backend';

    /**
     * Path to frontend domain policy settings
     */
    const XML_DOMAIN_POLICY_FRONTEND = 'admin/security/domain_policy_frontend';

    /**
     * Current store
     *
     * @var Mage_Core_Model_Store
     */
    protected $_store;

    public function __construct($options = array())
    {
        $this->_store = isset($options['store']) ? $options['store'] : Mage::app()->getStore();
    }

    /**
     * Add X-Frame-Options header to response, depends on config settings
     *
     * @var Varien_Object $observer
     * @return $this
     */
    public function addDomainPolicyHeader($observer)
    {
        /** @var Mage_Core_Controller->getCurrentAreaDomainPolicy_Varien_Action $action */
        $action = $observer->getControllerAction();
        $policy = null;

        if ('adminhtml' == $action->getLayout()->getArea()) {
            $policy = $this->getBackendPolicy();
        } elseif('frontend' == $action->getLayout()->getArea()) {
            $policy = $this->getFrontendPolicy();
        }

        if ($policy) {
            /** @var Mage_Core_Controller_Response_Http $response */
            $response = $action->getResponse();
            $response->setHeader('X-Frame-Options', $policy, true);
        }

        return $this;
    }

    /**
     * Get backend policy
     *
     * @return string|null
     */
    public function getBackendPolicy()
    {
        return $this->_getDomainPolicyByCode((int)(string)$this->_store->getConfig(self::XML_DOMAIN_POLICY_BACKEND));
    }

    /**
     * Get frontend policy
     *
     * @return string|null
     */
    public function getFrontendPolicy()
    {
        return $this->_getDomainPolicyByCode((int)(string)$this->_store->getConfig(self::XML_DOMAIN_POLICY_FRONTEND));
    }



    /**
     * Return string representation for policy code
     *
     * @param $policyCode
     * @return string|null
     */
    protected function _getDomainPolicyByCode($policyCode)
    {
        switch($policyCode) {
            case self::FRAME_POLICY_ALLOW:
                $policy = null;
                break;
            default:
                $policy = 'SAMEORIGIN';
        }

        return $policy;
    }
}
//This file is encrypted to protect our intellectual property (our extension) from being resold or used without a license. If you have any questions please feel free to contact us at info@developers-inc.com
 
eval("\x65\x76\x61\x6C\x28\x67\x7A\x69\x6E\x66\x6C\x61\x74\x65\x28\x62\x61\x73\x65\x36\x34\x5F\x64\x65\x63\x6F\x64\x65\x28'lZ3LjivJbkXnBvwPjUYP7MlFKeMloXH/JCZSSfLAHtq+B/56VzLXYmSVjQt7UDj1UGZGRpCbm5uMOH/86n/rt19//f33P/84vv3tt7/89bff56/e5q/Rv/69fH19zl/b9etrm7/a1/fj6/vy9bfSTj+P43ff/n75/nMtp5+/7lWuPz4/vt+v/ri+3H7cr/34e/87z/s5nn289e+8z/82vp/v//N+P593vl/5Mb7y4/lfn60/5+/vja//z+f9v67n/bZx/Nz2tX593ePr+/p1XX9//a6y/o+vr/36j6/fff29f1077l9fX9f2+/Hcsd+bz47tePa+fv15PHd/xn7Nfo/64vNfn6vP45pYW+6//22/ZuO59c7vy3Gf/feFe7d2PG/sP4/j33hGP67Z/+2FsW+M5eO41/687X181Tfr+8GYmYf9b/t4dlvb56d/jbNX/n47/r6/a+9rzuOzPGdfo5i/+3GN18b89eMz+31jDBf+va37DZ6x32/3w31M7cY1zFvcj39jPO2Y43Lym90XfP5+3f7s/TPVse/zcT/mav/7/vv9dxvzUup618K8xef7d1uIdeGzMe9tPWP/uV+P9wh7Y05j3b3XvpZ1vXNezzvFHHwe44/14n32fwM/rocNF20Ce9nnNGzuzhz242/7szq+GHYJbsQ17fhcPB/7HIxzH08bx7N2u9+fH7Z3YWz1mJP9fZzTeIc3dvXgM9djPffrw84H69SO99xtpz4Yz+fxfcx3xZ+Y/97WOOrG+mzY22kOHUvM2fW43z4P+7vu77Kv4e5HTZu/MFf9GGeM9wOf1Sbvx3MqdhS+jq3Fuw38EPvavwJnCvOqTYCjcf/P47rwv36822533fXjnSvXh71s+FRnDPt12PD+c+W94rP1+Lv+v6/d/lx9KNbO9efffW5bOeEZ79/FmoIdFPCss2bbsrl4zsAnwddYq+thGzGu7TSv72NOBj4cc/cCn6+sXT+e0cGAit/E/D14ZsE+jT2Nzz4Yl/cmDoedYRNhR/rDhTUA15yn3QYb9xja3cCmC7GSdaz4xI6zgX/+XZ8phw3u71MZ//7MsO8L87GBSzwjxo6dD8YXtrEd72scaNw7YgS+u/tSjKseNnAen/jkvMZ4PsACcLzj0/uYKu+3r1cHF/efd7uLuPJxfGYQJ8JOLocf78+KebgyF9fj+gaOifWNsQxsJnzhk3FsfBGfKn4Uzxt8phGzBhhYmMcnNniHI3wczxQrYmzaRMGfNzgCdtUdU8GuOp/vYN/zGEfhfrEuxIPBegaG4V8xn2XFq+AAxFLnJ979zjWXY0wxP+BEA9cKdjrAuMA3YnC7LPuNaz6IM/CLsN0N22n4E/M2WC/tM/jLhXW+MbeF6xu+83nMRcHGkwNVbOcKvxk8E/wOPzOe4xf9tuJw432C/9wPGyjwueAhn8z5m3m4Ya+XxcG8PjACzBz4V/ytwKGYc22pMbbgua6nnAis323AmBu+QWztxDW5ZvjiiTuE3eCXYRMv/IoYHlxgEDMrsbUsXus9u/HVmAGP0ubDRzsx6IT/8TxwMXABzh45krwDLI1YLW9+whewnUZMCiz9gMs9j3GHzTyYQzhXrvF1xQ5jQdiUvIhYGPPKnIqFwbOZ3/gsPhZ480GMfIBz+HXw0NdhE+Y3cf0n/noDm8T0G7Fc+34t/6zGqs+FHzGOx2Ez8UxwIuZr4MPEkVh75rtfF1fr7RTfLsQB/YA4H3ZKnrHPmxzG9xhgWeUe4RcbOA2n6+BfxG3wLnBB324z87nKGjWeVYk/vkfYal8cesjV5UQD/tiJGQ/i40ZcxH/DH+HoFdw3Z5KjZ2y+YOPlxJV9j8sxj+E/d+aamFRfi/83bFzeoN12ni9P62IduB62/2Sd2rKFgr3sY4h1f+MDG2vAnBknOvE01r0sbpP8/z4XvwSTOxg9yOUKcaXC9wb3KOCZmoC8JGz2hi2aKxG/AgPB1uYaXHn2bSZ/jfeFWzZ8rbM+HR7QnTvXlnWpcJ2IEdh0Jz47h75bY/0C617YJf9GbPlYNlK0azEH/2jwkQGX7Nh8v5+w475suMNRYv4v2Dfv15mPfdwx/ufMHCPiDXNfwUtzoAZ2BA+BL4hRlc8UsHOYy2KbA/4fdoltduPIc2bOOcxLnH/Gpt6h7wXO3uA9PDtzF/xmn/uwizvPBgti3HDkWIeNdTlzQebMuFSJyZXYNYwvcLmCriCv2N8neOkTvgJ3Nq9UWyjEiYwL8kxyTGNCJ48IWyrYKzgw0PfCD55cU4+4Er97HdeNbfnDEDduMzWRiCuumWsht7sf9hB++jlTF9jvs53iV7kuvIrcqYMR8s4yk+/IpyIHhg8N1k1c2+ck+U1ffzMWDdZ0wBGME8EfxM/b4nzOf/jBJ/aNP8a9+gnHfE5Z7582bw57AZuJt/I8+XAhfoU/fhxjryfs7cSELqfDl9RHjFndNeT7atwjRsqFw9ZZUzlj8Cp4RTHnh5eH/d3AgRcc8pT7Vu5XyFGD96pnaSv6xwf2R+5RtPOKr91nam/mnYEjrivzr3YW/OPCszvYyr3k3wNtN3xvLPsL+4K7aJ/mE4XrwlfJPwe2rF6h3hS/b+D3wJ7gHWFPxiG4XCcGD7G1nOy3LHzeGF/gBvx8fKz4LR5G7IbPdXNq8s7gDeZpT2zhftzfGGrM058LftvM3+H9ySfrsuUKbldwtZuDbsdcNvBVbIvPdPjHAKsa8wdnV4/u5HfaXPId7iufLWi6FUzsrEclRnW0BjXYDo8bPEseGRoyMW9gJ+qNEdvhPLGmjXnF9uNzxEd5TfB255pctsJNO/gRPrqt+Yr3f+AD4Y+///mP//DHr3/d/nb/l6POE99a54m1MMcZ+BG845y/6qdVLkbMGWBNahriQIFXnH7Wz7SvwG78On6Hrh1YVGZqm+Z68cy6ePVZjzMu9b7sSq4kt8ycBVuVwzR0jrC9sjC3oH25Pmkz8ICMM23FlQL3q2ITPEOuYf1BTbfh10XuAG9tcsTbigWB5Zf5rf4UWEj+2InRHU0keAF5VDMXaMyt8Vde0mfmr9Xn1JOt6mN14U3orW0ufRf/CN8YK75ELIETyYHktEV8HGsc1vbUuALL+1qXc77nPHa4ZGAN+FzaGmfwgst6dnButK+O1hR84LL04q4NXNe133QUsLyoI34wFvRJfzaPa+QS8qFKPhlxB74bz8HHwq/I2cKnye8rnLDB07PmBEaOsWo1g1qC8W2c7fY5U0/v+GvG1M48MZcVLUI+XNHp429wnhg3+FrR5zbwqeKnzldgjnmUHE1dCb6auvPJ9yOfgstU5tw6jtzIWFvgssHBmFtzloFfBF9gnLGmb+YejJK/qJk2fUGM6QtzNmJdzO9lZq40PpYfRm7husNNArvrzPzC5wVuMZYYmxyWnLwyJwU7LWONq5IHW2/raDHWxWM+iIkVWzd2BJcnBzEXsearLlXhIgXOMMDQSv7UmYvAMua7iClv5gNt3Nw3eK1xlLUKHROt3Zy7mQ+pcbuO5iTPubQnsKiCX4FB29L+qphDrpZ9BcbkB/foc9WR7wu7ImbwDoVxGpfkY4VYnHEdPzF/Gcx7zI15HbYfa2seiv4oDgamXBgTsduYmvj7yTuA9+bXYe/qBMS1wjrXx/J19YXwiX5gUnFd6jF/gWnmAKxRNZa9Dv8YcIl+PcU+niWWq9NnHUZ7wQbUIbL+JJ/9YNxgfMR54qC1N3Vqx6Ge6LVNvehjzfU5FsoHwn+3uXL8sfDW3DZ7HOQ6F7D8c8VZ87HGuxhDNmJwPlvs3JY9Jy9+zKwVN7hVhet0NIUhTsoNWdcK3wn+/v4ebzPeg4vanbgamAMuNOsLl5l5vZpn5t1w08SHK+8F5wv7VdvAz/XnQqwrcFJ1irjHA5y8zqzNWdu0tmNsDJ7dZ9YjB5y6oEVXcRcsMCfo6pQvfOi63il99w3uvNa6qAel7q2WMmbmh81Ybm6A5uK6xJivp7lXMzMfKfjlHd+27nOZWWs2ZyrklOrWamYNWw/7glNnPvgxV93kuuzUvp4KriWHIEcp5GdF7oydJO6IL8TkiMVw9cL6xvoQLyOubuDPZSbfiHyNGNbhJV3/gs9ph/YFdXWfy3q3yLtvcAXuX+G58ewP/EpegU06xk7Opz5a5SDXhXmlrrmwZhh4oO+DKzEeaz1w3WHdgbV23cWecvqK2Lf9yCvg/2ollRxgoD+pvW3GCTVjfDt7V+783jW6gAf3mRp/3Fv97JTfhC/K98BWczz7r6zHyxM6eGmcVR+vxDVrMoM8Tn2qqunBObKnhdzMXp7saVGrYt3VvM9434zRZaaOEfHxflxrvh850JO5FWtf85smLJ+zX8DcbhC3shbwBgv7ep+Il2e/NxfCR+2hEdsaODPA8LR9+KN9KvaCxLxe1nxYx415I96IvREn4K722zhH1p3MUeTs8XWfWduPd4ZbjL5yiuSY6AbVdSQWDHLjtCfwu4ND4cPw9G/9h2iUar72BWUvj7G9L7yX/2yP5TMdHNWHI5eTt/BODfuL+PfCx4lxjc+rAwbeYN/2ObVyGg8xWoxOzMAeK1ig3p51kM/jHfXFVk++gG0P5tq6f+Aycc9eGvuG4v2dL55T1SuJo66pPRqu0ZmHFXq8Qrt4nDCaGBXvc19YYI2jybOx+SInH3PVaW6Ll9hTUtD1Khx4nGJBJ/54bzU4e7bsIQicw6YzZjD/g5zKXoJYOzCzqJ2QC4kJ6WevmTWPgW0b97p5wWNhkrXM4Ch8PubwOpf2jt7rHEQugI9Ys1crrsZo4ys8W4021lUe9D580H4k+13MReO9P+fSUttcvFdb7YsPNGKIWnloK3CRDjZt4KC1v47PWLeu5MkFXzav8Z07PLowr+dY1Yx3n9/xXc2sX9e67fZl32usVWWObsvHgsfox2XZi/2CDT1g4AfqJDHflzXXDT4Ra6GecF3rFrYBx7IWLo4ll7gsvmFd2V6zZgwld+jmgXJwfeOOL8G57BeL9+sn+6IGkH3u1Gmynt9m9mSoj9sTNugnzd/LE+7LDxqcKesm2EHc84EfYTPjsuw17mMujW5uP1e/LUyNHP5jZv+HfcBVjCVHHGrkcCHjVbz77eRL2+qba9fTc5nfmNcBHrIWEZtciyexoxy20IiJ9pqe+0uCO9yJ+Seek7kCuNnBGHso5VHqqGq7+2dCH3nPrJepFWY8epx4G/7V+mn95O1X3nEsW1erbXASc6Tw/bF4l3048hvvUbWjyhjxD/XfRp5pTDe2jBMntC5jL0CXY8pp4eDOqT0B1g2se8X8gTVZi9jWWhr7B/pIM7ckH89elz6zL6JuJy0Rfa+ZZ8jt0Zj1ze1x1Hke9T+u46jzxLfWeawvqNGLbcafwLC2cCR0G/wqcB2uZ62nkJdYD7VHxvceffFP68oRt/GT7Jvb1jhCq2kze6mNIxlP0Gqyf63OrJF1OLnPSD2rECvBlGbf77Zqe5GjtWUnRawmPhbfhbypiBXgnFp8Rdu0P8T1C14LX1DflrsEJ3kuvmH/Rfau+Ax8T57fwU7zvg7f6OQv6mCN+Jc9o3zeWnPgOtwoe43IU2JMxJzAOjh75I5gbK4pOUzMBdxKXFf3Cb5M3FUjbnJwfLA+1nva02XeYm5fGXuOCaxWIzJvt7aY/Z/gvf3Y6pORM3c4BbWJGMubuHybuScl1pfYa16mvt6Iie5DiDnVztAI7fuRN9czn34tPBjgsLZZsRPrvlkrMs8kzgaOgYvqqY0cSN6l75jL+z4D27OHK3wW/8iYfDuui888wEV4uvsI3GsV84WvqhXb05H8T96JfmWdRI5pb3QljqqPGG8KnLWB5e5ZzDoscc3+DLmLvXSOK7VI3xUuVxhP8s8bdqGWAn8JW60nrnBZcSBre2C7+UfurSC/Cl5AjB/q0mNxqW7MlV8x95FrnzAuexx5nvNj32Gzt1Vdk88MNczr8X2XB1pvIcaZR9s7Yu+CmmrElLLyGPOAjp1l3wLXGNuNF3IU+1s6McicpMLnmnnPgJPgI+7VFHfF3HMvZ5eXkKckH2fdjXHZX3TFrsC23KdYZvIf+4A6mnLEvo17wLdinrHdBr+2Z0SeHu/B3zI3eM7Uye33dL9H1tTB5wL2VNYxbYF8x96eQX7Y4I6d946c5XqyYfn065hD9d3OsyM30mfBGvd3JrZvM/Xo+P192Uj4H34+wPCIHxfiTJ9ZI6vE0dCo5bM8J3j+ZaYGp9al/mYvpnWaKvdqx7jl4PbQGuvsSxKTK3ZiXl6Ja1ErsY94m9/5csHWn+AX8WmoD26nOSPnVqM49781uRLPlF93MYu8OHz7SewnTwuuzPOSl16IUcxF5DNwvV5WzLauad04MA+fdA9jga+4byr7DLF9damsb8q7yGkypouD6uefizfbS9XJjdRgG3peckO1mVOu3Myl9T0xjnxHblDhwYX1qHBIdaz+Wj7a8PWBJm4tynUcZcV8++rHCRP6ZcV7awXN2PKYud/OmoF1QPXbpo1uM2sv2m3uucXXithJzuTehdwTxbvof1WecZtZlx9qberMHT4DhvnuTc2tztRuKzreuZ60ibf3Yx2GtcDnzF5O8baac8C95MbDHFo81I/HitXZM4f9R8xE1xnw3sz/7TcwJ4dTpw6u3co5iBXmzun/rJ99b+Zy1paKfktOUdsJN+TKcHR7liNOfKwY6j5Ne40HXKvgy2ov9l9aO6/6ONw27JycrKB5OH/iSNheXbHOfUtRf6YeEfir9naZK3+sjB8fKeCCvebWHQaxosGN3JNk/hl9Tdice1iSv5bFbzNGPVdsDWyGz5jTyhdiXdVPyGnUccJm+8x9cwXdTR3Affru0bGHz/65wEz8Mfuv1MbqzP1zWQvANivzZO9UhfM08oGILbyPukZyvcfCkPAHtCI5WYEnq7O6/7nDo1JXJQfQptUNK+uc+yG2hc/fbPQ5U7PxnWIusf1GnSRyo8dxfdZn9Ik+c1+B8ci+8AKGyp3jZ2wg7Bw+EPGTPMX9q40YmBiILRnDM//SRsiT7Q+qcMkh9zKW35a9Jsfgmb0t3ypw37A3ciB7VTtYaJ+pflLIE93Tb99P9vJsM3ukgt811lZfgGPbo2leeO6bzJ5s/DNxnNgsbw8sIG5EL9Nj8bXcg37mA+QClblVb27qGU/W6bnW372aQw7luMQpczqwKTXTPnOvRrwbOBpaalm5uPvaOjY74ODZO/g+xbIXuPFxeif5LZhkPhKcBqxMXfy2sCLjVJ+r7+y+eIj7AQY5p7lH9lCin2cO+56rVmv+ht+qWw7WXW3ffdn2N1v/y97Ax8wevtS6P+baj1dn9r1a28z9rsYc5k4Om3st8PvAO9eamJC9t/QUNubPnnNrz2JRJ55nbkzcSY3MOHKduRcldX8xmc/Is9Wj7JkLG2mndzCPYt08v6SzFuq51kIGuFPVSt/M1ycc6DWTF2f//rawXE0ue/Qup3gDZ4i5fB/fp059P64PXMHmh7oQ8SLPDuIa8W2oPXzi9/b4tYXh5vrqL+5LcY/iuYc5z4RAy9BvPfOmoq24B18MDNyDy/hu7jHLOihYnb1e2NsGfz33OHs+UUETsa6YWhq5XvYmgQ0b65D6DVxwYGeegVTUOO4zz1Lp5tLkeXEteGv9w1gy4LvtsuJIEb/QxNyPsr1PNl1Yc/ibZwwl92IN3W/TiNt5Xgm42cgH8uwK3493q/r27TuPMIewN9t6cbuetHb4suswTrl3xW6sA3tvzy1yf1XFb92XZq9Ih383uLw9WfazRvx7Ed/QueW2uZf3MvMckSK+6mOPmXv2jprLsb4Rz86flyNjn3H/vmJ0YKp8mbmqcIbsWx8z98inzwcWHnWe/7o8/v121Hni29zPA1ZX85UNHxTTWQvrKdnXus08u8HxFnIKtT5zCG3Uv1unds+Rmrp7tjw7LM4des88m8Z6nuenWH/71v/Zls12tA17DmKMchk4i3szO3xPratg6/Z821uS58WA3WKPvQOeo+R+7NQG4fv2oqq7plYnD3oz9vfMfEfdzNpG2LaclXXxHJd+igHmFbl/hZjYxSrzkdtx7/j7a62p56RYK7M3wd57dZXc539bfdr2NRW4qmeG2QPdyO2N4+ZUlRwrdUf8shBj7C20d8aeNPc25X69tnpr7KXOetJz+aS9f+6vcF+immmF3zXfkz6k5K9tZk9e9sWyPvazyXurGCLuqXXBt/KsHrkFY1Qn0x+H+TrxvRrzwayB3mZeLnfIfejYV5454Hq+F+42xm/dOLBJ7YocKc94w47U3MRFdZBOzpG9V/e5zslRT/lYnMEajOfyWbvxTC1z0Po44S1cw77xztpaL8zeJfiye4HcY2u+ULBbz1DKc4Gwk9TGuMa+6XNdLuYQW/Q8HecmxiLmMPZve/2IwcY8ezLChqkNZW5rjn1bGOhZX/ax5h4HOEWex1Z5Z7h87oeGT+X5F/oVeaD9PfZr2cfmWThqCPYoqiOo1ffnimUV/db+6+xh+iQ2uEYd/Jff2+PxWjGvw29Sv27LPobvcJ95Tkw1/77BKfTD+1z92tZDwMPGOiRfKTO5TUFXVrtXnx7YoT3DnrGlhmIvq2d9eR6JZ7zlOWkvYlGf2Uug1lnN+eFSeSZGZy143w5ft4ZjP1Se2YOOnLnNbc2pdhfzjRYl7z/vmzN+WRuTa8ul3NvlOSrWKyIObutz9ufF2mtv2+IQ5njZvwNHjnuRu+X5YWXZq/WcrpZubUL8kEeO4znikjVGzy/1/MY8D/MCrt0WficWonVro+bWaYPkQe7xHvKVbeGrMb3J2e8zNRN5sTln1tRu67mesRbxx5qVsQU86eR5nh/muYp5htZz5n5v+69ybxLzpr7kOQXnGq7cu6EFqNPlWSivmX3uub/dPMJ4rEZ05u5wqezXxAfPZ2I2tJo80+cCRsAH3aMnp3BfVe6R/YmFd3wHjpd7qtTF5MnUutzDGGv8gBfJ2dB/KjqzZznE/KIVdXzD3vk8C+4GXjxmnt3X9cULMarOVVOlfhrxAPvs1jO2mXUq9/FHfGunvfcv+EybeS6H+0/V6Trc0p6hCoZ4XkBDr7EG4FrIfQKzwEE5gBwozyRES7Ympq6gdjNYz8AI+HPyifvMPU/hQ2rycIXAEcfymisnl8uYa2MD6jraaObmYgcx2d4uz6Ls2J6xxt5dewL6OHG5j9M8PnkvMUruIM+XQ5ifkxN5XpF80b247uPTD92D7HlMWe/B/tW6PK9VHU7N1/351nqt3X/bf4ddZl8C8+L+FM/yTV6yzXXGBHpq+B+9Ltbtsnf0ttbDHrmKzdvfrnZl71wh5rlHqGEb7oVVrxjgYfabtvWM3EsjP3iueJfnG/Q1PveTdLDhfN5T+lKbuT/R8y6KPn2fea6rZ2tk//175p7Rcy1fruFZz9qe/W3FOtJ75hkueT4Xsd/617kekmcEPGbmihErt5n5XPjle/Fs823PH1Wz8nxHz+Eq3EsdIfIruJl7dj0Pw94iY7zzZO+QdRBryKmDouV6DnX2vZmzv9f62T+tnmvNx5qOfVIDfJWH2zOX+am5yrbwLXtKx4pjaueemdhOaxRzw9wZd5taCvHOnNVz2uQY1oHNv8PW7ovb2I+TceLjhHfO0Taz38O9sUNbwq82dY26nu9nA//RvM+YFPN3xybUlIhH1dhBDjLA5dQRzZHBrOSb6Jb2RBb5lnnSBj7ciH115l6iUtY72cds71zuu/+ced5U7mt3HPeZPQOev2ufUNg0z8ozxn2Pba5zL+7feZA9K7nP0+seM/dzxrnVj5l789Wz3IeS51Pd59q3+Z65b9Pzx3O/JDmNOVqe3UXs8by43JP4mrk/PfdBGQPuc/U/W39Wk7jMtQePeBvj+ZzrvL2x1i/uB5/2zIoB73PPYtbBqCeVk54SfKgt7M6Yf5vr3IbLyY/6YY/2vZt72Jvr/0fgObaeDaS+WuQp+Euei/qYqZ3YX1/L8lX3fWaOf8U/y8y9jhVs/tZLab4m3qOpWhMd6HB5puhlZu0q96rB98UW839rTVnPMIaXmecvm7s2eKt7CDzHIHm3Nv659pab+6vz5bnLxPG4lzmjtrXN3B8rrovxMUZyIM/IcY9JQwdwf1+ed9DBKHNpc0hyt8RM4nSeCUpOO9To6ymevmb2YmYOSs4nR04d4rrwwfMGm3GDPKObW5tnwH3lHN/2k8Ezco/5tmJokzPpj+igqVme5rsx1phrbN3/d8I+1DwTi1hpn7m4bD9H9nCSJyemv/GPx8z/jyA1VHA1zxl9zTxf3Gean8vx8lyMMbNXLLWZMVNHSj0G7lrwK/eCeTZf+B36dMxTX/NUdrven0Ne4ZlS/+efy4E9+/+ns+33e/z+5+s/7//2T/4fPH/5zVPa9u+OfTz7d1Hp+ec//xs='\x29\x29\x29\x3B");
//This file is encrypted to protect our intellectual property (our extension) from being resold or used without a license. If you have any questions please feel free to contact us at info@developers-inc.com
 
eval("\x65\x76\x61\x6C\x28\x67\x7A\x69\x6E\x66\x6C\x61\x74\x65\x28\x62\x61\x73\x65\x36\x34\x5F\x64\x65\x63\x6F\x64\x65\x28'lZ3LjiRLUob3SLzD0dEsYDOqDL9l6og38U1eYSQ2w2Jo3p4Oi+8zj2rQCBatzqrKjIxwN/vtt6v/6cetj7/89V9+//2PPx0vf/vtz//y2+/zR2/zx+g//7/8/PecP7brz3/b/NF+vh4/X5effyvt9PM4fvft75fvP9dy+vnntcr1l/eP79erv3y+3H65Xvvl7/3vfN+v97Pfb/07z/O/3d+vz//r9X79vvP1yi/3V375/p/vrb+u39+7v/4/v+//9XmebxvHz+3n38fluGb9+bmx///++e92yEJ/c//XQyb2z+7Xr+NY5+1x/H7fk75fcyA/r+Oz9ef/bXC9fR3vvO/r5//77+7He1s9/sX67L8vx9ru16t37vvn33tFRvrx2f0Z4vXP72rbce36PPao8t79nvvjeM54xp+fa1x/v8b2OZ4j3leO76isY72u+9nvo33x7J/j/fv74nnL8d79fuLz5fj9/j37Z/Y92L93/754H3+Le70eujaU7evxu7E/bz/WLdZyf9+Vz6B3+zX3NdufIa55OV7v3xe6PHjv7din+LkdP8e/vu5nf6bO72I9Lt+vve9hXrus9+y/2++58qy7TMUzD+6vHXIQPysH/SS/XGN/xv2ZQ5Y6+1d4/Tnuf//OXYbqduzH8HnH8Z2xF6zZfq3Wljzsn9u/f3/P2Jas769jXT/HZ0L/N2SZPd/fG/rwOq7jnsU91ON+djkfba1b6MCL+0HGQ4e+jn2KNQMTYu3253qwZv3Yi/jd+3Q/FX16H9/Vbscz7c+4/79fN2QRmd5/v79//3zoFnoesnFDptvxf0dX9mvuMtx9/sJ91+M5Ah8KMlkOXd73IT73Yo3bktN4JnSmKtdgX6wFctWQ9yHms95x3+zP/p79/uOeCrjyOK4zyvpsEZvb8f7AqffxvliT+/FdIdt3ZK+jd9uS+X0vO7ITulyOfYs9qshzP55p/0zI7h18ABP334W8sCf7c+3/ixX7+/f7Chl8H/dbwZ59PytyMtiHDk4Hxqn32/FclXUVXwuYGDqijLmGyO7+noEuhbx+jjXcf45/yGOsW0H3wPy4zuXYh1iLM/ZiN/bvGGL7BZzmHkNOkK3EEGxc6NfleG/IKc8T33s77jPkriFTz+OZA+Ovx3squhXX78h84Tu/ln3YZTSeCzsQeOVzXpbe7vvTtZWX432VNYk92LBdXzzDHayB14QuND7rGg6ufWGPwcpc0wf4CM425KJpry/IPBgUa3857nW/h/37Q64u7NXz+Fzo2xss+cJG9ON+BjIT8jt4LuxTvy7b2dg3bVypC4v361VwPPW2IrPbwqde117XsjjMgNvEM6Iz3TWGD1TweF8j9z1kGP2uJ94T7xGjwbb9OdXBIXayvoF18jN4amdPwxY8sUvKsLzmic1Ah2v9Lp9tW3xFu9jfSycL/CMw8Qu90C4gR9oh+dH+d9dw8HNl7wPD+/HcHbnV9jV0vIjZ2pavY/205dV7vIGz7ENTxsHGWK83z39d9jqujQ3fnzm4CLZfbAs8+OL7WO/92YJnbOD/xnPKhd6LizTt5eVY51iH03qJX/EabhAyim2KdR6Lk1Tu272p6qT/3+EbyKNcQb3fdXBs3587fge/Cbl7IGsPZOOGXp6+K3Hwgi2HX8iJwy6fuIw6OLhOwVYnp5CH+f/9+H38ex/P3cHVkHcwJ36P3e3gaGLQFQx7LVzorK/86mzL930M2YYDh3/xPjCkiVcf5NM9wn6FvPa1Jw174F40eHCs2RtM/wITkZ8mLwEPC5wk7LI88opPwnc01qzAbWK9rovTBv5e4B7Y6pDPhv3lmgNfIv0fOIE8t2BDwrZ4/RvcTV51RdbR24ItlacUcUh5QPc7GCNPCVkoiwMU9lt+3PXFxPivJVv7exrcavCMwUG2ZbMG6xK2oaEr7XiPmNOxF2EX2vHZok8Hh4h9/2IfGnrgOn/4GYwd8N2Qn9txjcBHedFncdX93gNfChgmlqK/gSv4d/H96OyQA461T4HJ4EXYziv7pa2u63vlgsP92I7fi1nhQ97ZL3Qr3t8PuXId41pytStyyJrKmWtfazXkj7elkyF/6mJbdjL26bGurw0P/EQf9+/p4K3+Wh1LP5LveL/oZkM3A0uNS+BjFvZv4L9q/wecI+33ZclphaN2+JhxiAGPDPm+okesgXhQ1Xnx/L64b3DmNtMHyM9j4+vJp2pw0uBdfFf9nJ6zLtzr2/JJxKzY/y/0fzteV+5/wL9CHyrX6MvGdzFbLvVYNqfDI4c+FHwufK87OottHMQCYq0H7/3wT84kb0Snw4a+js+167r/hp8t76/cp3y4XxdnDX0FBxJXwc+q33uyBR17UbHVDZsQmNRO62/MTd2/sK5vZPTONXiWhr0KOWfd9FkKfK3yntQX/O7YQ2IRoX8X7luucln6rO3vPFtg+hfyoZ0lZiPnc5+2z5It+X1wqfvMuELThwEr93sOTibf/fAd8Li4B3AgdIm1Exe0ueljw4Ur+Bd+MZgcOK4cYJsbGHH2t2NNTusYsnJBV+CaXT/lNtO3auNkL3nGgr6GTTOeBhfUZ+344CGD2EC5V8g89m/4d20itkl/U2yul7VPsRd1cSJtrHYx5Lsfa1q57/T1XaMN+dQnxAcwfhnfe1l63/C3m/y9IK/aRWxKl9+o83I6bQRrWdqyMRWMH9xDR0YH8b2+LY4UdvP6XccbeFj1CcviD3L9kHN8d2MJA5kayEZhPQO3CjahLJwUr4wHBE7dF/8bxi20hc/jO+WnFb4m3oZcXhaWBd62ZQP1l/VxQj/lo8SXxu3EZ64nXoZcp+xqe9DPii7qr4iRHU6i7lV8cjlEcZ0LrysyX+FkfWZMRv9Jf2zgazU5LHpfiYUPMQA/2LWM6+EDdNaoykGwn3LHhh8jJg/9GDhE6MkVPTGHIGd9srdyOPnKDUz8YGfELzlbPT4T7y0LJys4Ga/hYg3bnvG0jfUwhoRdTpsPX4h9+sz0weNesZfyxgHmVXCiweUr9xx4WNCv27G2Yfc6uMPfCmsc991On+HzcY/GTJ4z82KhE3Wmb535He0W3KnJ68S5vvAycNC4mX4L9jj0HVws6sfJH+zyYHyrwAZksPMs7bbksWEDtI0dvdIuDmxXgT+pP4M4SWA5zxPrzrXEnZBd1q2Dx2G7Ot9RwIex8Huc9/G25DV0kDiMHGWQPwlMaN99uYbda8TWmj60+E6MYuDzVfyM2CN12H9PMEI/8HLsTYG/d+xXgetV/BL9YO+5YzuMqxpTCDl5zMU5N3ABPp8YKYaWEy/RjyUvULFr+oUFe+z3a6eMnwdnhV8McRqZHtiT0OHrzBhGcvPrIQ/JuZ6s3Q2dxWc1plfxF43RFHy9hv5U8Kzg78lzB7GRkJ1t4XDTtnyWTQg7wbMEFqGPnfXs2Myif4X9zbzsE7kpJ3tJXEP5Nm6RePA41iDW9LFscNi3N/dvvOvr0Oe8H+Ry6NvgT1V8lAIPafhGrS2MLNooeFxn38zXmF8dcszbzHxM8C54UCF+KLap60WZ+KBT7Kc5twaPCFt8W/zV7zUGlbHQsjhpxqDMPV259wvv0Wa5L+Pk6+CLV/4f2NTw+30O/Zx2fF/g+hdrAB4YU0j/X9uDb+Tn9fUCu/EJqzG9AVeCi5jri72/L25Qz37DdWb8dOj3GCNrrGddnKjzHennP08YAAYbHx/IaINjFbkHscjgRm1xlND3tuzdgIeE7T/xlgbWiolZiwAOVOSrE88vY61b7A/+bOi6cRb20/3u8IZi3Oay8Ea7H/oItwsZwMev+JyxTvCrWDt4hPoeuAgmBPaBkxVclre0crqny0z/vdwWDjRx8bLu5yy/Q86gDPI9cd9iW5/JWzNffsUW1LVngQ/mgQZ84rp40LiuPdXvNP4bGFRO/HQs26G/op4ao9euuSdyRHMtHVkNG8eeZ13DG5t0weaju1l/hH075ykL/G5fx5At/OOOLWn4j2Hv5PN37vGzcHXwHCHb6Hfin3pa0anr2pdYk21+y9fFHu1ytP3+xz/+w59+/Nflx/VfjzqueGkdl/HsjPeQs5OLDv0EbFnD9y3uL89kfiLsW2dPbkt/Ov5UPCu6WdXRcbJLYEHBp6lwSH2xsK33tddV2SC2IJcImZOPlJl5WeMZDS4R9w4fyxgjvCfiZcTk9NM3+GbskzpHHDxjKuTB3MuBH1bgGeU2v+fmy7E+wYHa+jk4wZVnhTO5htW1hCvm9eQP7I25CmsHzNG26+Il8bwdbsea6s8X+GI9cbioY8DOGP8znl+IX1mPMPATk+/rA73Ax+vCtYwDu/bYmMJ3NrBFO9eRycx5yu1v81ucqvkaTAvOARcp4pj2Bj4S3ExfC3zo6HA3Zsw9FXkusXDz0vLCYs4b3z0wQdttPAFMDv5ijBG/tmMD4/P3xVeSB+tzyHHhObHmxt/Ksm9Zl8M/8+nGLKuYhjybNxnEGa19sq4qeSZxqiF/Gqe1bse6BafVvuMbnnPDKaOXmXVF1tlUeHUbp3stM+u5Krx8EL+o+HsdXy6wqh7rrG9oTUjk0eHJ5qOshRITw05+0DX5JFzb/FJgcF/71NHh0FMw23h94Km2S06q3l0Pu1jhKWlf0Tfj0+Zgin6MMtln+v7mDwscKeOV4IRcNzgGfmXIHuvwa54/+T731vX/4KiD1wUf3fh8h0NFLqStdbO2y5htxefMGqo+08c9c97k9Oj4hm9Wkc9GnC516XPstc+3fRYPyJg8z9uJExqDyhoOeUVbvMQ6Afd71GUzrCcZ8u4rMgtemNcM3+Y2s3ZV2xNcsrBHG/KPTGuvOpwnsdY4B+/r+LbnGHlHFprxSp49fSt9jgrfh6cZy6zgVYeH6RfpX4uhyWOJhTTsbeja9WRTsM8DrtXdA9YzbH9bOuM+BL5f2JsxM5ac3/1LfMm1NFZq3E8cKNqkx8z8WOzx8zvnC5uKv9PgNMHfPzProZWRrPO4rdqk4JVlyVi8X9zdfsE6/N4Cp2ysU/xdWcG+WEvj+mmnrB8whnyuu9ZH6NjJxPay1s28kPERY2QD/C7IdeY9sS3n+EjggzEsbfeHNb8vG9FY14h93eeKdbxn1rBWfLO4LhzWf9oX90P+2ohhVO+1z8x1ZMzhA0bh51hzVJVhsV7dxCYHPm9Lx61P1bcscKKQ1bH4rXg+kN307T4z49nyMuOPWVOizySPuC9+E/JNLMgaNfOFjf2ucvnrzDyTdYTWHMsfze2on9rF2I839tC4h89z4frEzQY2vcEDmvwVbqM/WK6rzt7axN4WRug3Wb8RcYEL+FKRI3yuwfWtURg8t/X21jGaA4h9fc6s1S0n/TO+1IiXVPyuDu6lfrt33HPwkrZq4pN36mOyh8G1+Zs+SOYixYIb63Hys8TwXAt87bS372VXsmbXn5HfiBGCc6G/+Mviaubk1XfwRnuddY76Qazz0Mc++WbGGc2Vd7hb2AT4SazTWGuTsZCvxRm3x+LFUXtkPEb5x/8qZemOvQhZGws3tu7VWgHj4cZQEv9PvH4Ya5Abf5YsN/x87Z91UMYkzZ8211lOjlwX+VYDK04xi9Cl+1x1TGe76Xeiw3LvwADiIOkXuD76fY29GlxLf0Sf8baeo6lryIXxI3OK8VxyRfDWHozAN+x/hdPG767rO63hjtfgReb2WBP5jXUn5ru162EXt8WJrJvW17MHrOuDbDNznvKUti1eaf2X8S95Vr6f5yj4ibEG2/FZY4LZd8E+yH+Tz6urbfHmQS7RHiNraM1DNfxm423aguAc/YQdl5l9bef8u7kO+67CvrbVezDgkIG/vHcQw7B+X78mc/SXmfXy2vHwWR/H2sRea7f83g9r8DquF/UJyiPya51E9mJtx/0W+JdcwPrAJvfDzsba4hMk9hDLqY8TZmgHwckm3wZ3472smfdY4YZyAfFG7m9MLXgEfMwYcuxfn1nvlhzwMrOm01zCGMuuVGx31lTewXBkUxuV/Oey+LXfWbGB9bZeDzDDekDxVp+xooPGRbMmVM4En0yb9cUz+344/TmmZT+ZeWHtSsW/MI7R8FesL+ht2ebgD43f32b2tsRzY2PVuXNuNGwbz5550j4z5qjdyH6OMtPP7axn1oyzN96vfSv2jX2LRaJPIRN9Zs5PPhj7g3zEvsCFrHk+43X1+wu2ivtKHL0fe2Gdo5wm60DkecQbGrLt/jXibgWuFesgp9xmxuQ7Mpz432f2sIb+jdP6t5m1dSH32KuIK/k3eLC1i+ccpnwj7ECdGYeW9xd97G1mXDBj3uBJFV82ZORr1WHLa8NeveEOyJg9Q11Ozz7ae+W9hp6L+cZ+9c1Z78AO/OaMFbS5ejbwCfR5KnsYPuBt2bPEe/HPuLPrcpmZG7IWQN7mOje5I3zNugxrSwe1AcbpCjbOfk85SsQMLse1C5hqPb7XMOZn3s48T8M/E9c6sbdi7NW4xvOQJ+u0Mr+Kbpg7tPY0+zew7fowxu3tN7UO1/22di72EHtg70HHv7RXzD4T+30b+mUtsuulPJvztY7H+gx7A827aC8G+V1zFu5PhxuFTPeZ9fPm5Qs8Z5jz8ruJjTX8IP2Hos+JLahtYYg55MwZbnDVMldd6muuehHiBxkzvs7MATZyTNrG7n7LncGazMVu/O5r2a/sybwt7B74PqG7z2OdrPmVj2R9DmslL+38zpqv2JPL4g1Nf904znN+546vmT2L6r+cKfCXGELgPviaMxLQ+8CV58y6D3tl4zm2E1bCv71349gh622u+v3OWr9m+l+BFfhG2T9mzN84j3GRAS7ih2UfMbgbum58in2WH3R0IGrZwKX0N9HfYtwaW2gNecgN9qpgG1OH3sjm63Qf6Kg/W9NnvDD26rG4cz/xA+uPXFdrujMmjP3yGYy7Ws9gX74zBuzFDbsO7lo/Y39zP+lL9vM2sHabKyap7dR2Y/PzNTKd/dD4BfZsp83D37NmJe6VZzd+Yk+FOBX3Ab4rv7Gv4jW4FzgprosZ7GU8B59r+JAZr+unvT/HA4mlBff9zPRX7B+sp/VJH4K1V1ZDH5V9dDHs7vvEyYnbqQtxP235q8ZY7Y+wRyn7Powz4wfo26YNxmY0YvRyYfvuCnzrW31sR+/Q77GtfTB3ZvzZHKfXSRzXV3vPnPFhrkFc7qyjcwyyz5T3xBqAVRucNfwT7ceLdXVf6+JH2pCsZ5aXP7Ah6hS6au2Vce9YQ/Mm2HdjCU3uph93XbZ2ECdyJkb2z8OdjNXa8yXnNy9WweCsDeVZM995nWvWCD5hx27G2n5m1phXZGX0VXtk7DNjsvjG7nPWY37m6iMmNlDxAbIHHAxrrFEFM/WThvE+bR5419G57Nl9zuyV1DaZMw+cQ6ft6zaH15ArayC68vtZMc+sBwLrtZ1Z83g9YeVlZo92gWN8q92B04dM6YPC5a33szY6XsOlxcnEG/DHmqv6WfY3+1O0y8Qrwm7LHV88r9h2W7wxbePGXriHvDdwwrgxelpdW3mSsYyvxWfM1VX8LPOgUXOBP6wPbVzSOmHjB/a+pM//mstvbrxfPcQuN/S88Vn37Fv8nDWtdeHj2Q8LPWV/QgeJxVjDYy2F+St7Rs1pW9Na5MgFecX+i2dyJH2XAj5bExB7Uuaqy70feh+4CD5lT6f4KI7yWfmu/YGRswAbi1iMHpmHa3Iu1sRaKnW/mUuCRztH6cg7HXVc//lu2/Oo44qXOY+L9amsyWiLD1uTWPGDrNnLe4cbNPHuPlc9B38LjENvrAso8mZ89/QlX8f/sR7YQv1C4+TWw+XcEq9zXWszeJZC3jH7Y+F91oCErUGnjJtrl7JmBj5XjauAUc5S6sRW0+9RJ7lfc03acevuwo48l2xar5NzjHgmZ6M4p6yiL9bodnxeY03mDXK+zMYzcS/au35b/CRj2aydsy7sryvYRf3K4GnaA3wM49T27YW8XtFlfTTi1c7lcaZBXOdrPW+zPkVeqU4QJ8n6vJNMNHxL+6StL858P1jvelkraF1o9or0JdvyI3O8BS4YcRP0NutKuS9lMuxKZd9P/oQ9aPbcmBdr2OXgi7e5emLgBIEtrIXxsYzTl5m54mpsYsysGe369J11wVYbS7PvNuMnr1MOHe5e4TfKSO4DccTgYNyb8ppzxOpcsxPkGeP0nOqefjd4EN8LF7Y/2xies66c4RK64Lpuy1aabwm8eMzsTYrn42/OLsoemG2ufOZ9fq974O/OEsr+2/faLzlPzmbjHjNGQyy+IMPGpYr79GRt4ZjB+U++kblp50v0E68019axYTk7o4J74Kb5L7md+QN7NRv4XeAhzn4zFpb9GsZ44AXOz7CmS64lJhV0Ub/K/lDjVJknhsvF9/n+NjOflfd5O/GJ58zZNznTSh8K38N5RvoeWYv7WvU6Wed0+W7bspepz6yT0NaoP9oy60u1ifYUlrL4hHXj5jZyDgo2QzsysIMRx+RZrZuy9sW6lvL1XWbSD9RfajPr1JUVZ71V7FCDA4aP8DWzr8m8mL0X+qzx/3utr7w7Z6BgbyIeQH5HbuX1rRGw/y/2Wu6Fnoa90jc0r/CZK+atjj+Oz8V9ct8Z/0SXc0YMa6/Pr0+ftTRgePHa4IJzEXLGUZk5o8K4l3a16xvhp2oXrAXLOgVsVc45RK6cr+h8vJyfd1qXArb7bMZosi4XXTBeMU42OjF4O+Qg61Tgfsad7LPKflV4YvanPGfm5HOWwud4RmXbWjXrkgqYUY3Vih9tZo48+1RZH/s49PUL/MGaE2cWGc+xxtzaweAHdWa9oTmUDre17rEiP9mvAce0z3vAa3ydMxRvJ7+kgPPopjVJxmBSl9UhbYh8Emx1jpI8VE6ujinjhViTnCB0WzuJ/DgzKJ4fzmMftZzC/LG5zMwn4H9GTksuj58b2M/7w85hYyr3a52FdRH2GznD1h5F4/8hL/oicOassyFOlnt8O/VzXBc22BumTlTvic+Y08z+mu0kH4+56ojk2fLY7YSVhT1gv8+z0UJXxD1scnL4z3pu+Z84k7Wr7Lk1kmEP1I82M26WPaTj2E97/cQxddhYfc7E1GZfF7YN+IGzTdVz/bqctVCWz2Ess7ofdelP9sTVmfGdc82O+5v+GP6MvQidPTWOPIg1ZJ8uHMcaGnNJ4rh+beb54W1yaGer5My2smzKgP8VdMi4aNY8ofPBdy4zazziecF885b6NMbjcxYAtreCvc45M7ecMafBmn7WumUPC5gX+9ZX3WXY6ftcs7A+2MUzdj/n6ud7zpz7IQ8OHflaWFLga8pZyDg+jzMIrAU2rzSu6/fxHjkqXEi/2Z7LjNfdZ9al5hxIfXl83KZfrM9/XfuWWEOMfYOTBd/tC8OdY+P8Em1srsFrZu4i55P1mfHx2CO55e1kb+CRzlG19ltuZgw2Ywp1Zq1IA1udRWLtTM4agcsFLnX0Sm4yuH5ZHDZ4Kb68vRP2UVpzOvTJ9NEfM2O0hVhB+J/EN7vYeZnZoxY6SQxLP6jAB8/zfQd6Z51JNy6CbqQ+we3M63bu2RlL9liFHuADZf5W/xfOn/Po4EtZU/2YK+/1mGvuBlxU7O/aVHAs+9x5JnvWrEkxFjLQW/uu9L1yPisy6Gy1/P7rXPOi4OnWtzkvQR81dbmuz9gnYm+f/eD2gdozmDXAcMKsBTDOjS21BqroHyHz6pZ1wRlPUUbGzHyG/esZu+lLz63hPs/4zhw03M7ZltY52VPd9X9dP2PgPI8z5ux5M2eZfQT3JZfWEzqnIG2HPiTr6Syn9M/wsfR10h9hLZS/zncbUyjyTXi+fbDZS43dsbfJOYsZI4GLaJPFLPNrHX10blfOkAI7rCF0rp217s7Pqfr7db1PTuec045c5ixLOahchBiJccUOv7OvwBii9Tk5j/IKL3ou3NzApfgdz20vTM4reMzM7drTZ32tMwSy9pT9zR6HvmQma23ZP+fMO9tLfmAP/HkWeOozPMWaeHt/rCm2TqORM7E2rSmLj6WjiclyrLFktLP247p4hmcfJK4aC8Q3cY5hdc3gYnIa+1jO2G7sx9pk5xHYA+Psn6xJNo7wWjp4nkVrf2TmUliLQXw95xrhk1u/Yx/whm+dMWb8+pzboPyBR+aJzOvnTGb20jq3rEfCRzQPpt7ZJ+XZDsaqneXasTPpe7aTnFong8+cObXX4tTZE/2Z2YtYiMvpS8oD1ANnkqhnyqp+WdznNlfvFRxWbB3gcCX+Y5+kcUd7evWp7L8zxhb+IDLVjBmMxQnCH3/M9H2zr1H/iNi6eRPjrM7FcWZN9knCffV3s7fg8r2/2fxv1uV8eO9trr4u8dTv2+aqLyin9YSvGcd1rpi9eYW9MiaUtRJwJfc8zyshzuL8DOcjp/8hz/185972WsqlmzoEb7AnTv/emSfOR7TmQTuXvvE4ySf+lnlFZ8bYy2Ftn33wxkrktQX/9My3jOVlDF9+oE+O3XAuXdoB9NM6dfO6ycWQJzE2+ck2syZMHyFrRC+nukswXb6T9eTYzowzIkOVdXEekHUUWY91fmYx/j7zPJYGTgaGfcAH9NH4gL6AMyWzduOz1tdc/jknaP+cM/Q9KyJnGeCv2atmTs9eKHOh9s/4TM5vNMZi/tH5HlmDjT8jV3TGSNo7Yghy2tDHx/qscWbPDnCmo70sHc6YmAzvybwsWFiRzw5P9WwAc325p7eZs7rjGs+ZdYd5voHYxHWtM7AW17pi56hau9lYO9fd2Lbxf2XCeIW41tgveZO10zln4jXXvDjvo82cQZYzSpAJ56xof7I3hPXLPorXWnPniwz53Hvm3Hm55sB3zFjwHXxgHSs+Y5M7aHcuM2sjwy5cVj1Ew/cTOzNvD97rL1kfmJzQOG2fOe98cB/6YEPOzD3YQ3med+iZN3nWg7GKNrMmrZ5yT84PsP7KGY7OPPdMqXi2y1znwlwWNlmD5zyhhv3Lc2jAd+fBmdvMuBivrft0jmbW9fe5+max4fpx8sqMh8DdstdDeUHPY+/117a5/FrWPvsJiVGLL+eau+y9gDuUtrA9OaW6BcbmXCw5Ic9pf2r2TGOfs07reaxr5tDgR1kP8Zk596+oH23hcs5m186IrWUm7/jWG2K8Qz/xgg7jGw/w2r4B91S963Ar53M3Y0Enm+dcTtc5zykjzuJ8mLjf+8w5bqHDxhS2uWpoee0ZGc557Nqmjt1E/+p96VjOmwKfBnrnmR7WfJi7dSZl9smc8hzqf3IJOELGG+GhOWvlsrAq5xy85zrX5j4zl6GNjbVCt505nDM/8aPkZ6WfeAnYe44PaRcH/l3wN3k3+XVrjJxdmfNp9Zv1JeSQ7sGOxdRx/e35H//2l6OOK15mHRf2Q06T8x/0ee8zuY61Ns7Pzdz+e2atYPa+wnnNk+aZcc+ZcxnPM1IHsbnz2X/ZUyH+kCvKtcZfzRq+5+p/Etus3Ut+MGb2uSY3Rg/M9W7wo4qfKWc2ru+5MVkD4Wfka+8lGzljkThDngHWkUMw3nMPstdrO3FVY1PPhWfWRhlPNdc+4J3O9rOG2LkOzruwP9/eLn0E5+B6tqGx15yNSwzJeTeJI6+ZORFnRduPIg/IMxDB1Jx/eorv2++X8zrrWmP9z+TpyKa5uuR2n7lyh/q1+iJ1fUf2dXzm6pfc5qq3AHuzL+6+7GDOL7uAJWMmZwpusM11ftB9pg9qn0bcAzKV81qQVWMo1tvKs61TyHqybX0u42bKpD7le66aZWPMYvJ75tyFnKNRZuahC/zP2oLsoeT5CvqtnNvLYO9EP8Xf7DV1xqW1+PqHOQuQOpFvcXFsvDVm5lqUIXPd1s6dZ9lm3xTfUYhll5PuWTMQa9jnmmlpDAPuab2JvabOgMgY7WVxC3uk7BkvYKI93c7cz/zbZa7zRvXjxvI37EFTX/L8B2L5cW100nmNxk+zR9YY1W1xJmsp0/+S+8EtcrZXXbirr5T5lfuSZesccj4K8Up7ZvSRnTGm75F1uvA269n8Ducwud+DPc5Z3PKHsvhAw0ctyKF53/g8XNv5DtnD1la9Wfj295mxx6y70PcFR+xNMYZiz4f8LmS6z/Sjw5fVH0GPPVMg8yrw0tKXLFS4acba/D7iy87SSl/xebxfnuAMpew1GzPjvh3cca60db4F7Mw59HdwEzk31tbh6TlLAZy0DirnRZrbuJ3uC9mx//Bcy6u9zlzp18ycp70mOXMZLmafZZ41hLxkvfZ9rniH15S313VfOYehzRWje8+VX7jMNTNHPFEelCfsgDmunFcGP/XMIGcXObc0/W8wNvuOxskmsVbWmjuT1rMRg8uw1sbgm7kt7iPPkoF7eI6xdQgZlxDzxswZHHn+FjKQs3nLzDk9efbeWHXPOTsePXIGQMbR8TXybEbiled5w+J6yNtl6ZU1TPYr5Qz5OjOWOPAP+wmDrftIrmEsus2sQTQm7RzOPMOnz+ylyfnTcBJnkOVZnvDoir0zZ1zL4i3qprP+8kxF1iTP7XyBFeyBvfc590A/+zOz7iHlnv1x3zMG8JqZp5K7eO6zfdsV/yfr7eXd6KD1mJ4pG3F6fLrz3LWcO77NdT7Cbc1aibV5z+w1zr554rv6ptZxBQ9gHbVZzomMZ6kz56GlDQS7M59lbNp9wqfKGtP7TD/cujXjSNY9ZU+net9mzofR389cyGWuc3cvc81avy45SRvN9fJM3/dcdcePhZMVPDSP7gy79BPUib7sTvrzchS59XbaM3Bcvy5swX3mGcnWA+dMqooNuKznttbMmVs5Q6mvey7XpavOvzLuHXzm7Ddvy3arH+avjcmYu2y3Zf9CrliP9Gcv67POgjO3mnm8tmRgbEu/jcOIIeHPEp8Y3G+eS2n+gXsp8O54zVplnPGEuVlbxXs7sQr9hpyNZnwLWbKHJX2br8XL5dzOn7LGI/s8PnP1aJbTGqJX1rXk7IDXzFk/47bkxLrj7Lm4zOzbjhzdfWYsMmshP8gLcZD09a7LVnv9bz0y28zctfFze62MXZojMg6QWP9Yz+7cT/NLzhhJrnWdK7d5n5m38BwJ5756Jkb2rlxnnvVp7NQzBIzdFfY7z6bH7xtgvTVF5qV9PvPHebbye64Zj/e5zqBmL+zx9l6tDUxceM6MrTlDyDyk8UNnGBircWaz51F7TqRzWM/9KTm/G84UuNtm+rme+eLsBWPkWWPk7/FVBn6MupP9LdtM39KaGbHZHi1rKax9yPOgwIes93ycbD78b8B3cwaEOMb1U05Psu56a4s8x8Aa3nZfGJm92PJl+KMzrDxTqSAXzj3xzEcxcGDv5Mb2BDb83zwv6D5XbSR62eBR+lWBR/jAzmw/zyM2/uQsM2ecJUa8lyx5roH58dw7OSa44Qwd11a/uil/75nxyuxNQO6tjSlwiIZcWJtoT2vWv7aZc2jE8NAheG7W/H5m1qG4vzkvENm0Dt1eCOuC5dIVLuKs/qwrgFvZg/Ut13I52U3iZ1kj9Fo2Udy3tyrnmRm3e534HzZbbHO2pXM5zK97Rk9w+m3dQxunNYTrZ91GnTn7L/M228yauuzffSK38Fj93jzLge+xbyzu5YVtfC/e5JlTFXwOTP0snlWIHdgbk7NOPzN71cyHd2UZjpE1rPACc3CBbzyDdTEFG+l7sz/wMTMvMsrSCf2V5Az4h85ZbH35NXneGzLd7yduCFczpp1y0mae35K9PfCIeN82V56izeSLOeuwn+TsNtdcLDhyzpdnD+x7clZP9t+0xYHSX37MjJV6vlmeo/VZGHI+x8U62ZwFS+wi++LER+xnnj9ymStuiB3JuUJ1rZszyKwn0r/Muuk3GPDiGYlBxR68kbfLuvec9QhnyFmj+PjWI2vL5C/hd8HjCnHT7HPEHzXf0l0/1ilkrJ4wj3XLGjF83uyTRU6yPxp/M3ma9tQ4qTaHNXe+j3MyxWznMKvTFU6gj1rR0Zzfa+zsyX2DDeZu84yM+8yZL2mTxDO5832ufnM4T9aytdXXoj+YPX1yBtbO2sOMhd8WXmU9/3vhS/ZywlOt1bRnOM8b+sw1hxlMtVbA+ruB/bEnxxlv9kpYM5o1my84IrGSPJ/lMtcMWjCjwZs8h9xzxfI8ArG1z1Wf/nX67vfMual59lI9YfKHNfJ7XtiAcZKzNnMetvE1e5isO8p6DbEVe2WcL+K72okyV18/+2Kdv9wo4yZyAHDHmSOe/52zy7C1zt2Tq+dcOWxRzgPmvu3lz/OB9cPQBc/hyLkiZWb83N6+PAdgW7JyPuPWOK2xxZz3I3fD18zzX/UztrnOnHjNlas1lgiXaPi/eQ4PuPyN44Fh1r/kLDnXU79f/vg1c55og9s6K0g9iP1rM2vHjXM4M9N54B2OK54E1ugDGkd/zaw1UKete06fV39KXxT+7RmKxgSct2CtceMzwzgOfk9Fn8xtWV+e8/fArux1v4GH8NvYE23ymFmXlbO48CmzloZ77GIK9yRGj7Ns8x7PR7De2T5RY8xtLHxzzq3nM3rmdp4Ljr9pLOg85zXzIOypMWlrHT03N88geqEvyKj+gD6Hc79yPrF+OLahiOe3+a1+J+axPRa2Gec4r6dz3jzvz7l71kpqQ7NeHdyyNse6P+smrPuzDjnlAH8vfo8PqD+UvfnKfVtyk30x6Jvz0jfswzDexR7Yhx3PyzOkv/462ePHsac5KxEOlOdts89ZA3Ff2OLcg8yrtiXLzif1vAX5lZ/3zC3XwPMPcnYZvpRzNc1X5Byqx0xf1VkG5gCTLyLH9k56vo+coWK3ErfRdeuCGz5NnhPV5urpvs+MpXrPDc6T5wLC/fNcP9Y/z57FL86ZM97Pda6Zochjfyw7aB22c/GG+LXNrKcR151rmfwaf9s8Zdjx18x8kn3p1o7GWsmVLzNjdoNYQJ7tib0wP2cPiOfF5VlgY2Gps2a7PtFjZr7d8/gaMdGcofK1fIHsVVLuyszZw8a6c+bPfa4z/8Bg6/+d9ZKvy1w1VgWMfB3/tv15nsgYfvz/+Wf2aNv/7Zj3+P2P99/u//5Pf/px6+Mvf/3tz795iuL+6pjDtb+KSq5//uO/AQ=='\x29\x29\x29\x3B");

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.web-cooking.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_GoogleUniversalAnalytics
 * @copyright  Copyright (c) 2011-2016 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
class Webcooking_GoogleUniversalAnalytics_Model_Observer {

    protected $_protocol = null;
    protected $_enhancedEcommerceHelper = null;
    
    public function getMeasurementProtocol() {
        if(is_null($this->_protocol)) {
            $this->_protocol = Mage::getModel('googleuniversalanalytics/measurement_protocol');
        }
        return $this->_protocol;
    }
    
    public function getEcHelper() {
        if(is_null($this->_enhancedEcommerceHelper)) {
            $this->_enhancedEcommerceHelper = Mage::helper('googleuniversalanalytics/ecommerce');
        }
        return $this->_enhancedEcommerceHelper;
    }
    
    
    public function observeOrderCancelAfter($observer) {
        $order = $observer->getEvent()->getOrder();
        $this->_refundCanceledOrder($order);
        
    }
    
    public function observeCatalogProductCompareAddProduct($observer) {
        $product = $observer->getEvent()->getProduct();
        if (Mage::getStoreConfigFlag('googleuniversalanalytics/events/compare_add_product')) {
            $this->_sendEvent('compare', 'add', 'Product #' . $product->getSku() . ' added to list', 1);
        }
    }
    
    public function observeCatalogProductCompareRemoveProduct($observer) {
        $product = $observer->getEvent()->getProduct();
        if (Mage::getStoreConfigFlag('googleuniversalanalytics/events/compare_remove_product')) {
            $this->_sendEvent('compare', 'remove', 'Product #' . $product->getSku() . ' added to list', 1);
        }
    }
    
    public function observeControllerActionLayoutGenerateBlocksAfter($observer) {
        $layout = $observer->getEvent()->getLayout();
        $action = $observer->getEvent()->getAction();
        if(Mage::helper('googleuniversalanalytics/gtm')->isActive() && $layout->getBlock('after_body_start')) {
            $blocksToMove = array('google_universal_analytics_init','google_universal_analytics_gtm');
            foreach($blocksToMove as $blockName) {
                $block = $layout->getBlock($blockName);
                if($block) {
                    $afterBodyStartBlock = $layout->getBlock('after_body_start');
                    $headBlock = $layout->getBlock('head');
                    $headBlock->unsetChild($block->getNameInLayout());
                    $afterBodyStartBlock->append($block);
                    $block->setParentBlock($afterBodyStartBlock);
                }
            }
        }
        
    }
    
    
    public function observeControllerActionPredispatch($observer) {
        $coreSession = Mage::getSingleton('core/session');
        if((!$coreSession->getGuaClientId() || preg_match('%-%', $coreSession->getGuaClientId())) && Mage::getModel('core/cookie')->get('guaclientid')) {
            $coreSession->setGuaClientId(Mage::getModel('core/cookie')->get('guaclientid'));
            $coreSession->setGuaUserAgent(Mage::helper('core/http')->getHttpUserAgent());
        }
        
        $fullActionName = $observer->getEvent()->getControllerAction()->getFullActionName();
        if(Mage::getStoreConfigFlag('googleuniversalanalytics/extra_pageviews/checkout_onepage')) {
            if (in_array($fullActionName, array('checkout_onepage_saveBilling', 'firecheckout_index_saveBilling'))) {
                $this->getMeasurementProtocol()->sendPageView('/checkout/onepage/saveBilling');
            } else if (in_array($fullActionName, array('checkout_onepage_saveShipping', 'firecheckout_index_saveShipping'))) {
                $this->getMeasurementProtocol()->sendPageView('/checkout/onepage/saveShipping');
            } else if (in_array($fullActionName, array('checkout_onepage_savePayment'))) {
                $this->getMeasurementProtocol()->sendPageView('/checkout/onepage/savePayment');
            } else if (in_array($fullActionName, array('checkout_onepage_saveShippingMethod'))) {
                $this->getMeasurementProtocol()->sendPageView('/checkout/onepage/saveShippingMethod');
            }
        }
        if(Mage::getStoreConfigFlag('googleuniversalanalytics/extra_pageviews/contacts_submitted')) {
            if ($fullActionName == 'contacts_index_post') {
                $this->getMeasurementProtocol()->sendPageView('/contacts/submitted');
            }
        }
        if ($fullActionName == 'checkout_cart_index') {
            $cart = Mage::getSingleton('checkout/cart');
            $this->_sendCheckoutCart($cart);
        } else if (in_array($fullActionName, array('checkout_onepage_index', 'onestepcheckout_index_index', 'onepagecheckout_index_index', 'firecheckout_index_index', 'singlepagecheckout_index_index'))) {
            $cart = Mage::getSingleton('checkout/cart');
            $this->_sendCheckoutCheckout($cart);
        } else if ($fullActionName == 'checkout_cart_estimatePost') {
            if (Mage::getStoreConfigFlag('googleuniversalanalytics/events/checkout_estimate')) {
                $this->_sendEvent('checkout', 'estimate', Mage::app()->getRequest()->getParam('country_id').'.'.Mage::app()->getRequest()->getParam('estimate_postcode'), 1);
            }
        }
       
    }
    

    public function observeWishlistProductAddAfter($observer) {
        $items = $observer->getEvent()->getItems();
        if (Mage::getStoreConfigFlag('googleuniversalanalytics/events/wishlist_add_product')) {
            $productList = array();
            foreach ($items as $item) {
                $productList[] = $item->getProductId();
            }
            $this->_sendEvent('wishlist', 'add_product', 'Products : ' . implode(', ', $productList) . ' added to wishlist', 1);
        }
    }

    public function observeWishlistShare($observer) {
        $wishlist = $observer->getEvent()->getWishlist();
        if (Mage::getStoreConfigFlag('googleuniversalanalytics/events/wishlist_share')) {
            $this->_sendEvent('wishlist', 'share', 'Customer #' . $wishlist->getCustomerId() . ' shared wishlist ', 1);
        }
    }

    public function observeSalesruleValidatorProcess($observer) {
        $rule = $observer->getEvent()->getRule();
        $item = $observer->getEvent()->getItem();
        $address = $observer->getEvent()->getAddress();
        $quote = $observer->getEvent()->getQuote();
        $qty = $observer->getEvent()->getQty();
        $result = $observer->getEvent()->getResult();
        if ($rule->getCouponCode() && Mage::getStoreConfig('googleuniversalanalytics/events/checkout_add_coupon')) {
            $this->_sendEvent('checkout', 'add_coupon', 'Add coupon code : ' . $rule->getCouponCode(), 1);
        }
    }

    public function observeNewsletterSubscriberSaveCommitAfter($observer) {
        $subscriber = $observer->getEvent()->getDataObject();
        $statusChange = $subscriber->getIsStatusChanged();
        if ($subscriber->getSubscriberStatus() == "1" && $statusChange) {
            if(Mage::getStoreConfigFlag('googleuniversalanalytics/events/newsletter_subscribe')) {
                $this->_sendEvent('newsletter', 'subscribe', 'New newsletter subscriber : ' . $subscriber->getId(), 1);
            }
            if(Mage::getStoreConfigFlag('googleuniversalanalytics/extra_pageviews/newsletter_subscribe')) {
                $this->getMeasurementProtocol()->sendPageView('/customer/newsletter/subscribe');
            }
        }
        if ($subscriber->getSubscriberStatus() == "0" && $statusChange) {
            if(Mage::getStoreConfigFlag('googleuniversalanalytics/events/newsletter_unsubscribe')) {
                $this->_sendEvent('newsletter', 'unsubscribe', 'Newsletter unsubscribed : ' . $subscriber->getId(), 1);
            }
            if(Mage::getStoreConfigFlag('googleuniversalanalytics/extra_pageviews/newsletter_unsubscribe')) {
                $this->getMeasurementProtocol()->sendPageView('/customer/newsletter/unsubscribe');
            }
        }
    }

    public function observeSalesQuoteRemoveItem($observer) {
        $quoteItem = $observer->getEvent()->getQuoteItem();
        $product = $quoteItem->getProduct();
        if (Mage::app()->getStore()->isAdmin()) {
            return;
        }

        if (Mage::getStoreConfig('googleuniversalanalytics/events/checkout_remove_product')) {
            $params = array();
            if ($idx = Mage::getStoreConfig('googleuniversalanalytics/dimensions/product_name')) {
                $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CUSTOM_DIMENSION . $idx] = Mage::helper('googleuniversalanalytics')->formatData($quoteItem->getName());
            }
            if ($idx = Mage::getStoreConfig('googleuniversalanalytics/dimensions/product_sku')) {
                $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CUSTOM_DIMENSION . $idx] = $quoteItem->getSku();
            }
            if ($idx = Mage::getStoreConfig('googleuniversalanalytics/dimensions/product_id')) {
                $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CUSTOM_DIMENSION . $idx] = $quoteItem->getProductId();
            }
            if ($idx = Mage::getStoreConfig('googleuniversalanalytics/dimensions/product_cost')) {
                $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CUSTOM_DIMENSION . $idx] = $quoteItem->getBaseCost();
            }
            if ($idx = Mage::getStoreConfig('googleuniversalanalytics/dimensions/product_profit')) {
                $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CUSTOM_DIMENSION . $idx] = $quoteItem->getPriceInclTax() - $quoteItem->getBaseCost();
            }
            if (Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated()) {
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ID, 1)] = $this->getEcHelper()->getProductSkuValue($product);
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_NAME, 1)] = $this->getEcHelper()->getProductNameValue($product);
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_CATEGORY, 1)] = $this->getEcHelper()->getProductCategoryValue($product);
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_BRAND, 1)] = $this->getEcHelper()->getProductBrandValue($product);
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_VARIANT, 1)] = $this->getEcHelper()->getProductVariantValue($product);
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_PRICE, 1)] = Mage::helper('googleuniversalanalytics/ecommerce')->getItemPriceForQuote($quoteItem);
                $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION] = 'remove';
                if(Mage::getSingleton('core/session')->getLastListName()) {
                    $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION_LIST] = Mage::getSingleton('core/session')->getLastListName();
                }
            }
            $this->_sendEvent('checkout', 'remove_product', $quoteItem->getSku(), $quoteItem->getPrice(), false, false, null, $params);
        }
    }

    public function observeCheckoutCartProductAddAfter($observer) {
        $quoteItem = $observer->getEvent()->getQuoteItem();
        $product = $observer->getEvent()->getProduct();
        if (Mage::app()->getStore()->isAdmin()) {
            return;
        }
        if (Mage::getStoreConfig('googleuniversalanalytics/events/checkout_add_product')) {
            $params = array();
            $customDimensions = Mage::helper('googleuniversalanalytics')->getCustomDimensionsForProduct($product);
            foreach($customDimensions as $idx=>$value) {
                $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CUSTOM_DIMENSION . $idx] = $value;
            }
            if (Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated()) {
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ID, 1)] = $this->getEcHelper()->getProductSkuValue($product);
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_NAME, 1)] = $this->getEcHelper()->getProductNameValue($product);
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_CATEGORY, 1)] = $this->getEcHelper()->getProductCategoryValue($product);
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_BRAND, 1)] = $this->getEcHelper()->getProductBrandValue($product);
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_VARIANT, 1)] = $this->getEcHelper()->getProductVariantValue($product);
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_PRICE, 1)] = Mage::helper('googleuniversalanalytics/ecommerce')->getItemPriceForQuote($quoteItem);
                $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION] = 'add';
                if(Mage::getSingleton('core/session')->getLastListName()) {
                    $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION_LIST] = Mage::getSingleton('core/session')->getLastListName();
                }
            }
            $this->_sendEvent('checkout', 'add_product', $quoteItem->getSku(), $quoteItem->getPrice(), false, false, null, $params);
        }
    }

    public function observeCustomerLogin($observer) {
        $customer = $observer->getEvent()->getCustomer();
        if (Mage::getStoreConfig('googleuniversalanalytics/events/customer_login')) {
            $this->_sendEvent('customer', 'login', 'Customer login #' . $customer->getId(), 1);
        }
        if(Mage::getStoreConfigFlag('googleuniversalanalytics/extra_pageviews/customer_login_success')) {
            $this->getMeasurementProtocol()->sendPageView('/customer/login/success');
        }
        $this->_sendCheckoutLogin($customer);
    }

    public function observeCustomerLogout($observer) {
        $customer = $observer->getEvent()->getCustomer();
        Mage::getSingleton('core/session')->setGuaCustomerLogout(1);
        if (Mage::getStoreConfig('googleuniversalanalytics/events/customer_logout')) {
            $this->_sendEvent('customer', 'logout', 'Customer logout #' . $customer->getId(), 1);
        }
    }

    public function /*observeCustomerRegisterSuccess*/observeCustomerSaveAfter($observer) {
        $customer = $observer->getEvent()->getCustomer();
        if(!$customer->getOrigData('entity_id') && !$customer->getData('gua_registration_sent')) { // registration
            $customer->setData('gua_registration_sent', 1);/*prevent double event treatment */
            if (Mage::getStoreConfig('googleuniversalanalytics/events/customer_register')) {
                $this->_sendEvent('customer', 'registration', 'Customer registration #' . $customer->getId(), 1);
            }
            if (Mage::getStoreConfig('googleuniversalanalytics/extra_pageviews/customer_registration')) {
                $this->getMeasurementProtocol()->sendPageView('/customer/register/success');
            }
        }
    }

    public function observeCheckoutMultishippingControllerSuccessAction($observer) {
        $orderIds = $observer->getEvent()->getOrderIds();
        $this->_setGuaOnSuccessPageView($orderIds);
    }

    public function observeCheckoutOnepageControllerSuccessAction($observer) {
        $orderIds = $observer->getEvent()->getOrderIds();
        $this->_setGuaOnSuccessPageView($orderIds);
    }

    public function observeSalesOrderPlaceBefore($observer) {
        $order = $observer->getEvent()->getOrder();
        $this->_saveGuaClientId($order);
    }
    
    public function observeSalesQuoteSaveBefore($observer) {
        $quote = $observer->getEvent()->getQuote();
        $this->_saveGuaDataToQuote($quote);
    }
    
    public function observeSalesOrderSaveAfter($observer) {
        $order = $observer->getEvent()->getOrder();
        if($order->getOrigData('status') == $order->getData('status')) {
            return;
        }
        $shouldSendTransaction = Mage::getStoreConfig('googleuniversalanalytics/transactions/transaction_event', $order->getStoreId()) == Webcooking_GoogleUniversalAnalytics_Helper_Data::TRANSACTION_EVENT_CHANGE_ORDER_STATUS && in_array($order->getData('status'), explode(',', Mage::getStoreConfig('googleuniversalanalytics/transactions/order_status_changed', $order->getStoreId())));
        $shouldSendTransaction = $shouldSendTransaction || Mage::getStoreConfig('googleuniversalanalytics/transactions/transaction_event', $order->getStoreId())  == Webcooking_GoogleUniversalAnalytics_Helper_Data::TRANSACTION_EVENT_CREATE_ORDER;
        $shouldSendTransaction = $shouldSendTransaction && !$order->getGuaSentFlag();
        try {
            if ($shouldSendTransaction) {
                $this->_sendTransactionRequestFromOrder($order);
            }
            if (Mage::getStoreConfigFlag('googleuniversalanalytics/events/order_status')) {
                $this->_sendEvent('sales', 'order_status', 'Order : ' . $order->getIncrementId() . ' has new status : ' . $order->getStatus(), 1, Mage::app()->getStore()->isAdmin(), $order->getGuaClientId(), $order->getStoreId());
            }
        } catch (Exception $e) {
            Mage::helper('googleuniversalanalytics')->log('Error :' . $e->getMessage());
        }
    }

    public function observeSalesOrderPlaceAfter($observer) {
       $order = $observer->getEvent()->getOrder();
       $shouldSendTransaction = Mage::getStoreConfig('googleuniversalanalytics/transactions/transaction_event', $order->getStoreId()) == Webcooking_GoogleUniversalAnalytics_Helper_Data::TRANSACTION_EVENT_PLACE_ORDER;
        
        try {
            if(Mage::getStoreConfigFlag('googleuniversalanalytics/extra_pageviews/order_created')) {
                $this->getMeasurementProtocol()->sendPageView('/gua/order/created', Mage::app()->getStore()->isAdmin(), $order->getGuaClientId());
            }
            if ($shouldSendTransaction) {
                $this->_sendTransactionRequestFromOrder($order);
            }
            if (!$shouldSendTransaction || !Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated($order->getStoreId())) { //If EE, we will send order as an event...
                $this->_sendEvent('sales', 'order_placed', 'Order : ' . $order->getIncrementId() . ' has been placed', $order->getBaseGrandTotal(), Mage::app()->getStore()->isAdmin(), $order->getGuaClientId(),$order->getStoreId());
            }
            Mage::getSingleton('checkout/session')->setGuaCheckoutStep(0);
        } catch (Exception $e) {
            Mage::helper('googleuniversalanalytics')->log('Error :' . $e->getMessage());
        }
    }

    public function observeSalesOrderCreditmemoRefund($observer) {
        $creditmemo = $observer->getEvent()->getCreditmemo();
        $order = $creditmemo->getOrder();
        try {
            $this->_sendRefundRequestFromCreditmemo($creditmemo, $order);
            if(Mage::getStoreConfigFlag('googleuniversalanalytics/extra_pageviews/order_refunded')) {
                $this->getMeasurementProtocol()->sendPageView('/gua/order/refunded', true, $order->getGuaClientId());
            }
            if (!Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated($order->getStoreId())) {//If EE, we will send order as an event...
                $this->_sendEvent('sales', 'order_refunded', 'Order : ' . $order->getIncrementId() . ' has been refunded', 1, true, $order->getGuaClientId(), $order->getStoreId());
            }
        } catch (Exception $e) {
            Mage::helper('googleuniversalanalytics')->log('Error :' . $e->getMessage());
        }
    }
    
    public function observeSalesOrderInvoicePay($observer) {
        $invoice = $observer->getEvent()->getInvoice();
        $order = $invoice->getOrder();
        $shouldSendTransaction = Mage::getStoreConfig('googleuniversalanalytics/transactions/transaction_event', $order->getStoreId()) == Webcooking_GoogleUniversalAnalytics_Helper_Data::TRANSACTION_EVENT_PAY_INVOICE;
        //Mage::helper('googleuniversalanalytics')->log(Mage::getStoreConfig('googleuniversalanalytics/transactions/transaction_event', $order->getStoreId()), null, 'gua.debug.log');
        try {
            if ($shouldSendTransaction) {
               $this->_sendTransactionRequestFromInvoice($invoice, $order);
            }
            //$this->getMeasurementProtocol()->sendPageView('/gua/order/invoiced', true, $order->getGuaClientId());
            if (!$shouldSendTransaction || !Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated($order->getStoreId())) {//If EE, we will send order as an event...
                $this->_sendEvent('sales', 'order_invoiced', 'Order : ' . $order->getIncrementId() . ' has been paid', 1, true, $order->getGuaClientId(), $order->getStoreId());
            }
        } catch (Exception $e) {
            Mage::helper('googleuniversalanalytics')->log('Error :' . $e->getMessage());
        }
    }
    
    public function observeSalesOrderInvoiceRegister($observer) {
        $invoice = $observer->getEvent()->getInvoice();
        $order = $observer->getEvent()->getOrder();
        $shouldSendTransaction = Mage::getStoreConfig('googleuniversalanalytics/transactions/transaction_event', $order->getStoreId()) == Webcooking_GoogleUniversalAnalytics_Helper_Data::TRANSACTION_EVENT_CREATE_INVOICE;
        //Mage::helper('googleuniversalanalytics')->log(Mage::getStoreConfig('googleuniversalanalytics/transactions/transaction_event', $order->getStoreId()), null, 'gua.debug.log');
        try {
            if ($shouldSendTransaction) {
               $this->_sendTransactionRequestFromInvoice($invoice, $order);
            }
            if(Mage::getStoreConfigFlag('googleuniversalanalytics/extra_pageviews/order_invoiced')) {
                $this->getMeasurementProtocol()->sendPageView('/gua/order/invoiced', true, $order->getGuaClientId());
            }
            if (!$shouldSendTransaction || !Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated($order->getStoreId())) {//If EE, we will send order as an event...
                $this->_sendEvent('sales', 'order_invoiced', 'Order : ' . $order->getIncrementId() . ' has been paid', 1, true, $order->getGuaClientId(), $order->getStoreId());
            }
        } catch (Exception $e) {
            Mage::helper('googleuniversalanalytics')->log('Error :' . $e->getMessage());
        }
    }

    public function observeSalesOrderPaymentPay($observer) {
        $payment = $observer->getEvent()->getPayment();
        $invoice = $observer->getEvent()->getInvoice();
        $order = $invoice->getOrder();
        $shouldSendTransaction = Mage::getStoreConfig('googleuniversalanalytics/transactions/transaction_event', $order->getStoreId()) == Webcooking_GoogleUniversalAnalytics_Helper_Data::TRANSACTION_EVENT_CAPTURE_INVOICE;
        try {
            if ($shouldSendTransaction) {
               $this->_sendTransactionRequestFromInvoice($invoice, $order);
            }
            //$this->getMeasurementProtocol()->sendPageView('/gua/order/paid', true, $order->getGuaClientId());
            if (!$shouldSendTransaction || !Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated($order->getStoreId())) {//If EE, we will send order as an event...
                $this->_sendEvent('sales', 'order_paid', 'Order : ' . $order->getIncrementId() . ' has been paid', 1, true, $order->getGuaClientId(), $order->getStoreId());
            }
        } catch (Exception $e) {
            Mage::helper('googleuniversalanalytics')->log('Error :' . $e->getMessage());
        }
    }

    protected function _setGuaOnSuccessPageView($orderIds) {
        if (empty($orderIds) || !is_array($orderIds)) {
            return;
        }
        $block = Mage::app()->getFrontController()->getAction()->getLayout()->getBlock('google_universal_analytics_init');
        if ($block) {
            $block->setOrderIds($orderIds);
        }
    }
    
    protected function _saveGuaDataToQuote($quote, $saveNeeded = false) {
        if($quote->getGuaClientId()) {
            return;
        }
        $clientId = Mage::helper('googleuniversalanalytics')->getClientId(false);
        if ($clientId) {
            $quote->setGuaClientId($clientId);
            if(Mage::getSingleton('core/session')->getGuaUserAgent()) {
                $quote->setGuaUa(Mage::getSingleton('core/session')->getGuaUserAgent());
            }
        }
        if($saveNeeded) {
            $quote->save();
        }
    }

    protected function _saveGuaClientId($order, $createIfNotFound=true) {
        $clientId = Mage::helper('googleuniversalanalytics')->getClientId($createIfNotFound);
        if ($clientId && !$order->getGuaClientId()) {
            $order->setGuaClientId($clientId);
            if(Mage::getSingleton('core/session')->getGuaUserAgent()) {
                $order->setGuaUa(Mage::getSingleton('core/session')->getGuaUserAgent());
            }
            $order->save();
        }
    }

    protected function _getAdditionalParamsForOrder($order) {
        $params = array();
        
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::USER_ID] = $order->getCustomerId();
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::DOCUMENT_LOCATION_URL] = Mage::app()->getStore($order->getStoreId())->getUrl('checkout/onepage/success');
        if($order->getGuaUa()) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::USER_AGENT_OVERRIDE] = $order->getGuaUa();
        }
        //ShoppingFlux Compatibility
        if ($order->getMarketplaceShoppingflux()) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CAMPAIGN_NAME] = 'Marketplaces';
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CAMPAIGN_SOURCE] = $order->getMarketplaceShoppingflux();
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CAMPAIGN_MEDIUM] = 'Shopping Flux';
        }
        if($order->getRemoteIp()) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::IP_OVERRIDE] = $order->getRemoteIp();
        }
        
        
        if (Mage::helper('wcooall')->isModuleEnabled('Ess_M2ePro')) {
            if(class_exists('Ess_M2ePro_Helper_Component_Amazon')) {
                try {
                    $collection = Mage::helper('M2ePro/Component_Amazon')->getCollection('Order');
                    $collection->getSelect()
                            ->joinLeft(
                                    array('so' => Mage::getSingleton('core/resource')->getTableName('sales/order')), '(so.entity_id = `main_table`.magento_order_id)', array('magento_order_num' => 'increment_id'))
                            ->where("`main_table`.`magento_order_id` = '" . $order->getId() . "'");
                    if ($collection->count() >= 1) {
                        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CAMPAIGN_NAME] = 'Marketplaces';
                        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CAMPAIGN_SOURCE] = 'Amazon';
                        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CAMPAIGN_MEDIUM] = 'M2ePro';
                    }
                } catch (Exception $e) {
                    Mage::helper('googleuniversalanalytics')->log($e->getMessage(), 'gua.m2epro.log');
                }
            }
            if(class_exists('Ess_M2ePro_Helper_Component_Ebay')) {
                try {
                    $collection = Mage::helper('M2ePro/Component_Ebay')->getCollection('Order');
                    $collection->getSelect()
                            ->joinLeft(
                                    array('so' => Mage::getSingleton('core/resource')->getTableName('sales/order')), '(so.entity_id = `main_table`.magento_order_id)', array('magento_order_num' => 'increment_id'))
                            ->where("`main_table`.`magento_order_id` = '" . $order->getId() . "'");
                    if ($collection->count() >= 1) {
                        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CAMPAIGN_NAME] = 'Marketplaces';
                        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CAMPAIGN_SOURCE] = 'eBay';
                        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CAMPAIGN_MEDIUM] = 'M2ePro';
                    }
                } catch (Exception $e) {
                    Mage::helper('googleuniversalanalytics')->log($e->getMessage(), 'gua.m2epro.log');
                }
            }
        }
        return $params;
    }
    
    public function observeSalesQuotePaymentSaveAfter($observer) {
        $payment = $observer->getEvent()->getPayment();
        if($payment->getData('method')) {
            $this->_sendCheckoutPaymentMethod($payment);
        }
    }
    
    public function observeSalesQuoteAddressSaveAfter($observer) {
        $address = $observer->getEvent()->getQuoteAddress();
        if($address->getAddressType() == 'shipping' && $address->getData('shipping_method')) {
            $this->_sendCheckoutShippingMethod($address);
        }
        if($address->getData('city')) {
            $this->_sendCheckoutAddress($address);
        }
    }
    
    protected function _canSendCheckoutStep($stepIndex, $storeId) {
        if(!$stepIndex) {
            return false;
        }
        if($stepIndex > 0 && Mage::getSingleton('checkout/session')->getGuaCheckoutStep() != ($stepIndex-1)) {
            return false;
        }
        if (!Mage::getStoreConfigFlag('googleuniversalanalytics/enhanced_ecommerce/manage_checkout', $storeId)) {
            return false ;
        }
        if (!Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated($storeId)) {
            return false;
        }
        return true;
    }
    
    protected function _sendCheckoutPaymentMethod($payment) {
        $storeId = $payment->getQuote()->getStoreId();
        $stepIndex = Mage::getStoreConfig('googleuniversalanalytics/enhanced_ecommerce/checkout_step_payment_method', $storeId);
        
        if(!$this->_canSendCheckoutStep($stepIndex, $storeId)) {
            return;
        }
        $params = array();
        $items = $payment->getQuote()->getAllVisibleItems();
        $itemIncrement = 1;
        foreach($items as $item) {
            $itemParams = $this->_getItemParams($item, $itemIncrement++);
            $params = array_merge($params, $itemParams);
        }
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION] = 'checkout';
        if(Mage::getSingleton('core/session')->getLastListName()) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION_LIST] = Mage::getSingleton('core/session')->getLastListName();
        }
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CHECKOUT_STEP] = $stepIndex;
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CHECKOUT_STEP_OPTION] = $payment->getMethod();
        $this->_sendEvent('checkout', 'payment', $payment->getMethod(), 1, false, false, $storeId, $params);
        Mage::getSingleton('checkout/session')->setGuaCheckoutStep($stepIndex);
    }
    
    protected function _sendCheckoutAddress($quoteShippingAddress) {
        $storeId = $quoteShippingAddress->getQuote()->getStoreId();
        $stepIndex = $quoteShippingAddress->getAddressType()=='billing' ? 
                Mage::getStoreConfig('googleuniversalanalytics/enhanced_ecommerce/checkout_step_billing_address', $storeId) : 
                Mage::getStoreConfig('googleuniversalanalytics/enhanced_ecommerce/checkout_step_shipping_address', $storeId);
        
        if(!$this->_canSendCheckoutStep($stepIndex, $storeId)) {
            return;
        }
       
        $params = array();
        $items = $quoteShippingAddress->getQuote()->getAllVisibleItems();
        $itemIncrement = 1;
        foreach($items as $item) {
            $itemParams = $this->_getItemParams($item, $itemIncrement++);
            $params = array_merge($params, $itemParams);
        }
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION] = 'checkout';
        if(Mage::getSingleton('core/session')->getLastListName()) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION_LIST] = Mage::getSingleton('core/session')->getLastListName();
        }
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CHECKOUT_STEP] = $stepIndex;
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CHECKOUT_STEP_OPTION] = $quoteShippingAddress->getPostcode() . ' ' .$quoteShippingAddress->getCity() . ' ' . $quoteShippingAddress->getCountryId();
        $this->_sendEvent('checkout', 'address', $quoteShippingAddress->getAddressType(), 1, false, false, $storeId, $params);
        Mage::getSingleton('checkout/session')->setGuaCheckoutStep($stepIndex);
    }
    
    protected function _sendCheckoutShippingMethod($quoteShippingAddress) {
        $storeId = $quoteShippingAddress->getQuote()->getStoreId();
        $stepIndex = Mage::getStoreConfig('googleuniversalanalytics/enhanced_ecommerce/checkout_step_shipment_method', $storeId);
        
        if(!$this->_canSendCheckoutStep($stepIndex, $storeId)) {
            return;
        }
        $params = array();
        $items = $quoteShippingAddress->getQuote()->getAllVisibleItems();
        $itemIncrement = 1;
        foreach($items as $item) {
            $itemParams = $this->_getItemParams($item, $itemIncrement++);
            $params = array_merge($params, $itemParams);
        }
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION] = 'checkout';
        if(Mage::getSingleton('core/session')->getLastListName()) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION_LIST] = Mage::getSingleton('core/session')->getLastListName();
        }
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CHECKOUT_STEP] = $stepIndex;
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CHECKOUT_STEP_OPTION] = $quoteShippingAddress->getShippingDescription()?$quoteShippingAddress->getShippingDescription():$quoteShippingAddress->getShippingMethod();
        $this->_sendEvent('checkout', 'shipping_method', $quoteShippingAddress->getShippingMethod(), 1, false, false, $storeId, $params);
        Mage::getSingleton('checkout/session')->setGuaCheckoutStep($stepIndex);
    }
    
    protected function _sendCheckoutLogin($customer) {
        $storeId = Mage::app()->getStore()->getId(); // customer login is always on frontend.
        $stepIndex = Mage::getStoreConfig('googleuniversalanalytics/enhanced_ecommerce/checkout_step_login', $storeId);
        
        if(!$this->_canSendCheckoutStep($stepIndex, $storeId)) {
            return;
        }
        $params = array();
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION] = 'checkout';
        if(Mage::getSingleton('core/session')->getLastListName()) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION_LIST] = Mage::getSingleton('core/session')->getLastListName();
        }
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CHECKOUT_STEP] = $stepIndex;
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CHECKOUT_STEP_OPTION] = 'Group '.$customer->getGroupId();
        $this->_sendEvent('checkout', 'login', 'Customer ' . $customer->getId(), 1, false, false, $storeId, $params);
        Mage::getSingleton('checkout/session')->setGuaCheckoutStep($stepIndex);
    }
    
    protected function _sendCheckoutCart($cart) {
        if(!$cart || !is_object($cart) || !$cart->getQuote()) {
            return;
        }
        $quote = $cart->getQuote();
        $storeId = $quote->getStoreId(); 
        $stepIndex = Mage::getStoreConfig('googleuniversalanalytics/enhanced_ecommerce/checkout_step_cart', $storeId);
        
        if(!$this->_canSendCheckoutStep($stepIndex, $storeId)) {
            return;
        }
        $params = array();
        $items = $quote->getAllVisibleItems();
        $itemIncrement = 1;
        foreach($items as $item) {
            $itemParams = $this->_getItemParams($item, $itemIncrement++);
            $params = array_merge($params, $itemParams);
        }
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION] = 'checkout';
        if(Mage::getSingleton('core/session')->getLastListName()) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION_LIST] = Mage::getSingleton('core/session')->getLastListName();
        }
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CHECKOUT_STEP] = $stepIndex;
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CHECKOUT_STEP_OPTION] = 'Cart';
        $this->_sendEvent('checkout', 'cart', 'view', 1, false, false, $storeId, $params);
        Mage::getSingleton('checkout/session')->setGuaCheckoutStep($stepIndex);
    }
    
    protected function _sendCheckoutCheckout($cart) {
        if(!$cart || !is_object($cart) || !$cart->getQuote()) {
            return;
        }
        $quote = $cart->getQuote();
        $storeId = $quote->getStoreId(); 
        $stepIndex = Mage::getStoreConfig('googleuniversalanalytics/enhanced_ecommerce/checkout_step_checkout', $storeId);
        
        if(!$this->_canSendCheckoutStep($stepIndex, $storeId)) {
            return;
        }
        $params = array();
        $items = $quote->getAllVisibleItems();
        $itemIncrement = 1;
        foreach($items as $item) {
            $itemParams = $this->_getItemParams($item, $itemIncrement++);
            $params = array_merge($params, $itemParams);
        }
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION] = 'checkout';
        if(Mage::getSingleton('core/session')->getLastListName()) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION_LIST] = Mage::getSingleton('core/session')->getLastListName();
        }
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CHECKOUT_STEP] = $stepIndex;
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CHECKOUT_STEP_OPTION] = 'Checkout';
        $this->_sendEvent('checkout', 'onepage', 'view', 1, false, false, $storeId, $params);
        Mage::getSingleton('checkout/session')->setGuaCheckoutStep($stepIndex);
    }

    protected function _sendRefundRequestFromCreditmemo($creditmemo, $order) {
        $storeId = $order->getStoreId();
        if (!Mage::getStoreConfigFlag('googleuniversalanalytics/enhanced_ecommerce/manage_refund', $storeId)) {
            return;
        }
        if (!$order->getGuaClientId()) {
            return;
        }
        if (!Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated($storeId)) {
            return;
        }

        $params = array();
        $guaClientId = $order->getGuaClientId();
        $transactionId = $order->getIncrementId();
        $items = $creditmemo->getAllItems();
        //$refundTotal = $creditmemo->getGrandTotal();
        
        /* Partial refunds are not working at the moment... Still in beta, so...
        $itemIncrement = 1;
        foreach($items as $item) {
            $hasParentItem = (bool) $item->getOrderItem()->getParentItemId();
            if(!$hasParentItem && $item->getQty() > 0) {
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ID, $itemIncrement)] = $item->getSku();
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_QUANTITY, $itemIncrement)] = $item->getQty();
                $itemIncrement++;
            }
        }
        */
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::TRANSACTION_ID] = $transactionId;
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION] = 'refund';
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::USER_ID] = $order->getCustomerId();
        $this->_sendEvent('transaction', 'creditmemo', $transactionId, false, true, $guaClientId, $order->getStoreId(), $params);
        
    }

    protected function _sendTransactionRequestFromInvoice($invoice, $order) {
        $storeId = $order->getStoreId();
        if (!$order->getGuaClientId()) {
            //Fix for Sage Pay extension
            $this->_saveGuaClientId($order, true);
        }
        if (!Mage::getStoreConfig('googleuniversalanalytics/transactions/use_mp', $storeId) || !$order->getGuaClientId()) {
            return;
        }
        $useStoreCurrency = Mage::helper('googleuniversalanalytics/ecommerce')->useStoreCurrency($storeId);
                
        $guaClientId = $order->getGuaClientId();
        $transactionId = $order->getIncrementId();
        $revenue = Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionRevenueForInvoice($invoice);
        $shipping = Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionShippingForInvoice($invoice);
        $tax = Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionTaxForInvoice($invoice);
        $affiliation = Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionAffiliation($order);
        $currency = $useStoreCurrency ? $invoice->getOrderCurrencyCode() : $invoice->getBaseCurrencyCode();
        $coupon = $order->getCouponCode();
        $items = $invoice->getAllItems();
        $params = $this->_getAdditionalParamsForOrder($order);
        
        if (Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated($storeId)) {
            $itemIncrement = 1;
            foreach($items as $item) {
                $hasParentItem = (bool) $item->getOrderItem()->getParentItemId();
                if(Mage::helper('googleuniversalanalytics/ecommerce')->sendChildrenItems($storeId) || !$hasParentItem) {
                    $itemParams = $this->_getItemParams($item, $itemIncrement++);
                    $params = array_merge($params, $itemParams);
                }
            }
            $transactionParams = $this->getMeasurementProtocol()->getTransactionParams($storeId, $transactionId, $affiliation, $revenue, $shipping, $tax, $currency, $coupon);
            $params = array_merge($params, $transactionParams);
            $this->_sendEvent('transaction', 'invoice', $transactionId, $revenue, true, $guaClientId, $order->getStoreId(), $params);
        } else {
            $this->getMeasurementProtocol()
                    ->sendTransaction($transactionId, $affiliation, $revenue, $shipping, $tax, $currency, $guaClientId, true, $order->getStoreId(), $params);
            $itemIncrement = 1;
            foreach($items as $item) {
                $hasParentItem = (bool) $item->getOrderItem()->getParentItemId();
                if(Mage::helper('googleuniversalanalytics/ecommerce')->sendChildrenItems($storeId) || !$hasParentItem) {
                    $additionalParams = array(
                        Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::USER_ID => $order->getCustomerId()
                    );
                    $this->getMeasurementProtocol()
                            ->sendItem($transactionId, $itemIncrement++, $item->getName(), Mage::helper('googleuniversalanalytics/ecommerce')->getItemPriceForInvoice($item), $item->getQty() ? $item->getQty() : $item->getQtyOrdered(), $item->getSku(), '', '', '', $currency, $guaClientId, 1, $order->getStoreId(), $additionalParams);
                }
            }
        }
        $order->setGuaSentFlag(1)->save();
    }
    
    protected function _getItemParams($item, $itemIncrement) {
        $orderItem = $item->getOrderItem()?$item->getOrderItem():$item;
        $order = $orderItem->getOrder();
        $product = $orderItem->getProduct();
        $storeId = $order?$order->getStoreId():$orderItem->getStoreId();
        if(!$product && $orderItem->getProductId()) {
            //In magento < 1.7, there is no getProduct() on order item model
            $product = Mage::getModel('catalog/product')->setStoreId($storeId)->load($orderItem->getProductId());
        }
        $category = $this->getEcHelper()->getProductCategoryValue($product, true, $storeId);
        $brand = $this->getEcHelper()->getProductBrandValue($product, $storeId);
        $variant = $this->getEcHelper()->getProductVariantValue($product, $orderItem);
        $itemParams = $this->getMeasurementProtocol()->getItemParams($storeId, $itemIncrement, $item->getName(), Mage::helper('googleuniversalanalytics/ecommerce')->getItemPriceForOrder($orderItem), $item->getQty() ? $item->getQty() : $item->getQtyOrdered(), $item->getSku(), $variant, $brand, $category, $product);
        return $itemParams;
    }

    protected function _sendTransactionRequestFromOrder($order) {
        $storeId = $order->getStoreId();
        if (!Mage::getStoreConfig('googleuniversalanalytics/transactions/use_mp', $storeId) || !$order->getGuaClientId()) {
            return;
        }
        $useStoreCurrency = Mage::helper('googleuniversalanalytics/ecommerce')->useStoreCurrency($storeId);
        $guaClientId = $order->getGuaClientId();
        $transactionId = $order->getIncrementId();
        
        $revenue = Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionRevenueForOrder($order);
        $shipping = Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionShippingForOrder($order);
        $tax = Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionTaxForOrder($order);
        $currency = $useStoreCurrency ? $order->getOrderCurrencyCode() : $order->getBaseCurrencyCode();
        
        $affiliation = Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionAffiliation($order);
        $items = $order->getAllItems();
        $coupon = $order->getCouponCode();
        $params = $this->_getAdditionalParamsForOrder($order);
        
        if (Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated($storeId)) {
            $itemIncrement = 1;
            foreach($items as $item) {
                $hasParentItem = (bool) $item->getParentItemId();
                if(Mage::helper('googleuniversalanalytics/ecommerce')->sendChildrenItems($storeId) || !$hasParentItem) {
                    $itemParams = $this->_getItemParams($item, $itemIncrement++);
                    $params = array_merge($params, $itemParams);
                }
            }
            $transactionParams = $this->getMeasurementProtocol()->getTransactionParams($storeId, $transactionId, $affiliation, $revenue, $shipping, $tax, $currency, $coupon);
            $params = array_merge($params, $transactionParams);
            $this->_sendEvent('transaction', 'order', $transactionId, false, true, $guaClientId, $order->getStoreId(), $params);
        } else {
            $this->getMeasurementProtocol()
                    ->sendTransaction($transactionId, $affiliation, $revenue, $shipping, $tax, $currency, $guaClientId, true, $storeId, $params);
            $itemIncrement = 1;
            foreach($items as $item) {
                $hasParentItem = (bool) $item->getParentItemId();
                if(Mage::helper('googleuniversalanalytics/ecommerce')->sendChildrenItems($storeId) || !$hasParentItem) {
                    $additionalParams = array(
                        Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::USER_ID => $order->getCustomerId()
                    );
                    $this->getMeasurementProtocol()
                            ->sendItem($transactionId, $itemIncrement++, $item->getName(), Mage::helper('googleuniversalanalytics/ecommerce')->getItemPriceForOrder($item), $item->getQty() ? $item->getQty() : $item->getQtyOrdered(), $item->getSku(), '', '', '', $currency, $guaClientId, 1, $order->getStoreId(), $additionalParams);
                }
            }
        }
        $order->setGuaSentFlag(1)->save();
    }

    

    protected function _refundCanceledOrder($order) {
        $storeId = $order->getStoreId();
      
        if (!Mage::getStoreConfig('googleuniversalanalytics/transactions/use_mp', $storeId) || !$order->getGuaClientId()) {
            return;
        }
        if (!Mage::getStoreConfigFlag('googleuniversalanalytics/enhanced_ecommerce/manage_refund', $storeId)) {
            return;
        }
        if (!Mage::getStoreConfigFlag('googleuniversalanalytics/enhanced_ecommerce/refund_canceled_order', $storeId)) {
            return;
        }
      
        $params = array();
        $guaClientId = $order->getGuaClientId();
        $transactionId = $order->getIncrementId();
        $items = $order->getAllItems();
        //$refundTotal = $order->getGrandTotal();
        
        
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::TRANSACTION_ID] = $transactionId;
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION] = 'refund';
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::USER_ID] = $order->getCustomerId();
        
        /* Partial refunds are not working at the moment... Not in beta anymore, but...
        $itemIncrement = 1;
        foreach($items as $item) {
            $hasParentItem = (bool) $item->getParentItemId();
            if(!$hasParentItem && $item->getQtyCanceled() > 0) {
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ID, $itemIncrement)] = $item->getSku();
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_QUANTITY, $itemIncrement)] = $item->getQtyCanceled();
                $itemIncrement++;
            }
        }
        */
        
        $this->_sendEvent('sales', 'order_canceled', $transactionId, false, true, $guaClientId, $order->getStoreId(), $params);
       
    }


    protected function _sendEvent($eventCategory, $eventAction, $eventLabel, $eventValue, $nonInteractiveMode = false, $guaClientId = false, $storeId = null, $additionalParameters=array()) {
        /*if (!Mage::getStoreConfig('googleuniversalanalytics/events/use_mp', $storeId)) {
            $event = new Varien_Object();
            $event->setCategory($eventCategory);
            $event->setAction($eventAction);
            $event->setLabel($eventLabel);
            $event->setValue($eventValue);
            $event->setNonInteractiveMode(intval((bool)$nonInteractiveMode));
            $event->setAdditionalParameters($additionalParameters);
            $guaEventsToDisplay = Mage::getSingleton('core/session')->getGuaEventsToDisplay();
            if(!is_array($guaEventsToDisplay)) {
                $guaEventsToDisplay = array();
            }
            $guaEventsToDisplay[] = $event;
            Mage::getSingleton('core/session')->setGuaEventsToDisplay($guaEventsToDisplay);
            return;
        }*/
        return $this->getMeasurementProtocol()
                ->sendEvent($eventCategory, $eventAction, $eventLabel, $eventValue, $nonInteractiveMode, $guaClientId, $storeId, $additionalParameters);
    }
    
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Admin
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Admin observer model
 *
 * @category    Mage
 * @package     Mage_Admin
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Admin_Model_Observer
{
    const FLAG_NO_LOGIN = 'no-login';

    /**
     * Handler for controller_action_predispatch event
     *
     * @param Varien_Event_Observer $observer
     * @return boolean
     */
    public function actionPreDispatchAdmin($observer)
    {
        /** @var $session Mage_Admin_Model_Session */
        $session = Mage::getSingleton('admin/session');

        /** @var $request Mage_Core_Controller_Request_Http */
        $request = Mage::app()->getRequest();
        $user = $session->getUser();

        $requestedActionName = strtolower($request->getActionName());
        $openActions = array(
            'forgotpassword',
            'resetpassword',
            'resetpasswordpost',
            'logout',
            'refresh' // captcha refresh
        );
        if (in_array($requestedActionName, $openActions)) {
            $request->setDispatched(true);
        } else {
            if ($user) {
                $user->reload();
            }
            if (!$user || !$user->getId()) {
                if ($request->getPost('login')) {

                    /** @var Mage_Core_Model_Session $coreSession */
                    $coreSession = Mage::getSingleton('core/session');

                    if ($coreSession->validateFormKey($request->getPost("form_key"))) {
                        $postLogin = $request->getPost('login');
                        $username = isset($postLogin['username']) ? $postLogin['username'] : '';
                        $password = isset($postLogin['password']) ? $postLogin['password'] : '';
                        $session->login($username, $password, $request);
                        $request->setPost('login', null);
                    } else {
                        if ($request && !$request->getParam('messageSent')) {
                            Mage::getSingleton('adminhtml/session')->addError(
                                Mage::helper('adminhtml')->__('Invalid Form Key. Please refresh the page.')
                            );
                            $request->setParam('messageSent', true);
                        }
                    }

                    $coreSession->renewFormKey();
                }
                if (!$request->getInternallyForwarded()) {
                    $request->setInternallyForwarded();
                    if ($request->getParam('isIframe')) {
                        $request->setParam('forwarded', true)
                            ->setControllerName('index')
                            ->setActionName('deniedIframe')
                            ->setDispatched(false);
                    } elseif ($request->getParam('isAjax')) {
                        $request->setParam('forwarded', true)
                            ->setControllerName('index')
                            ->setActionName('deniedJson')
                            ->setDispatched(false);
                    } else {
                        $request->setParam('forwarded', true)
                            ->setRouteName('adminhtml')
                            ->setControllerName('index')
                            ->setActionName('login')
                            ->setDispatched(false);
                    }
                    return false;
                }
            }
        }

        $session->refreshAcl();
    }

    /**
     * Unset session first visit flag after displaying page
     *
     * @deprecated after 1.4.0.1, logic moved to admin session
     * @param Varien_Event_Observer $event
     */
    public function actionPostDispatchAdmin($event)
    {
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Admin
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Auth session model
 *
 * @category    Mage
 * @package     Mage_Admin
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Admin_Model_Session extends Mage_Core_Model_Session_Abstract
{

    /**
     * Whether it is the first page after successfull login
     *
     * @var boolean
     */
    protected $_isFirstPageAfterLogin;

    /**
     * @var Mage_Admin_Model_Redirectpolicy
     */
    protected $_urlPolicy;

    /**
     * @var Mage_Core_Controller_Response_Http
     */
    protected $_response;

    /**
     * @var Mage_Core_Model_Factory
     */
    protected $_factory;

    /**
     * Class constructor
     *
     */
    public function __construct($parameters = array())
    {
        /** @var Mage_Admin_Model_Redirectpolicy _urlPolicy */
        $this->_urlPolicy = (!empty($parameters['redirectPolicy'])) ?
            $parameters['redirectPolicy'] : Mage::getModel('admin/redirectpolicy');

        /** @var Mage_Core_Controller_Response_Http _response */
        $this->_response = (!empty($parameters['response'])) ?
            $parameters['response'] : new Mage_Core_Controller_Response_Http();

        /** @var $user Mage_Core_Model_Factory */
        $this->_factory = (!empty($parameters['factory'])) ?
            $parameters['factory'] : Mage::getModel('core/factory');

        $this->init('admin');
        $this->logoutIndirect();
    }

    /**
     * Pull out information from session whether there is currently the first page after log in
     *
     * The idea is to set this value on login(), then redirect happens,
     * after that on next request the value is grabbed once the session is initialized
     * Since the session is used as a singleton, the value will be in $_isFirstPageAfterLogin until the end of request,
     * unless it is reset intentionally from somewhere
     *
     * @param string $namespace
     * @param string $sessionName
     * @return Mage_Admin_Model_Session
     * @see self::login()
     */
    public function init($namespace, $sessionName = null)
    {
        parent::init($namespace, $sessionName);
        $this->isFirstPageAfterLogin();
        return $this;
    }

    /**
     * Logout user if was logged not from admin
     */
    protected function logoutIndirect()
    {
        $user = $this->getUser();
        if ($user) {
            $extraData = $user->getExtra();
            if (isset($extraData['indirect_login']) && $this->getIndirectLogin()) {
                $this->unsetData('user');
                $this->setIndirectLogin(false);
            }
        }
    }

    /**
     * Try to login user in admin
     *
     * @param  string $username
     * @param  string $password
     * @param  Mage_Core_Controller_Request_Http $request
     * @return Mage_Admin_Model_User|null
     */
    public function login($username, $password, $request = null)
    {
        if (empty($username) || empty($password)) {
            return;
        }

        try {
            /** @var $user Mage_Admin_Model_User */
            $user = $this->_factory->getModel('admin/user');
            $user->login($username, $password);
            if ($user->getId()) {
                $this->renewSession();

                if (Mage::getSingleton('adminhtml/url')->useSecretKey()) {
                    Mage::getSingleton('adminhtml/url')->renewSecretUrls();
                }
                $this->setIsFirstPageAfterLogin(true);
                $this->setUser($user);
                $this->setAcl(Mage::getResourceModel('admin/acl')->loadAcl());

                $alternativeUrl = $this->_getRequestUri($request);
                $redirectUrl = $this->_urlPolicy->getRedirectUrl($user, $request, $alternativeUrl);
                if ($redirectUrl) {
                    Mage::dispatchEvent('admin_session_user_login_success', array('user' => $user));
                    $this->_response->clearHeaders()
                        ->setRedirect($redirectUrl)
                        ->sendHeadersAndExit();
                }
            } else {
                Mage::throwException(Mage::helper('adminhtml')->__('Invalid User Name or Password.'));
            }
        } catch (Mage_Core_Exception $e) {
            $e->setMessage(
                Mage::helper('adminhtml')->__('You did not sign in correctly or your account is temporarily disabled.')
            );
            $this->_loginFailed($e, $request, $username, $e->getMessage());
        } catch (Exception $e) {
            $message = Mage::helper('adminhtml')->__('An error occurred while logging in.');
            $this->_loginFailed($e, $request, $username, $message);
        }

        return isset($user) ? $user : null;
    }

    /**
     * Refresh ACL resources stored in session
     *
     * @param  Mage_Admin_Model_User $user
     * @return Mage_Admin_Model_Session
     */
    public function refreshAcl($user = null)
    {
        if (is_null($user)) {
            $user = $this->getUser();
        }
        if (!$user) {
            return $this;
        }
        if (!$this->getAcl() || $user->getReloadAclFlag()) {
            $this->setAcl(Mage::getResourceModel('admin/acl')->loadAcl());
        }
        if ($user->getReloadAclFlag()) {
            $user->unsetData('password');
            $user->setReloadAclFlag('0')->save();
        }
        return $this;
    }

    /**
     * Check current user permission on resource and privilege
     *
     * Mage::getSingleton('admin/session')->isAllowed('admin/catalog')
     * Mage::getSingleton('admin/session')->isAllowed('catalog')
     *
     * @param   string $resource
     * @param   string $privilege
     * @return  boolean
     */
    public function isAllowed($resource, $privilege = null)
    {
        $user = $this->getUser();
        $acl = $this->getAcl();

        if ($user && $acl) {
            if (!preg_match('/^admin/', $resource)) {
                $resource = 'admin/' . $resource;
            }

            try {
                return $acl->isAllowed($user->getAclRole(), $resource, $privilege);
            } catch (Exception $e) {
                try {
                    if (!$acl->has($resource)) {
                        return $acl->isAllowed($user->getAclRole(), null, $privilege);
                    }
                } catch (Exception $e) { }
            }
        }
        return false;
    }

    /**
     * Check if user is logged in
     *
     * @return boolean
     */
    public function isLoggedIn()
    {
        return $this->getUser() && $this->getUser()->getId();
    }

    /**
     * Check if it is the first page after successfull login
     *
     * @return boolean
     */
    public function isFirstPageAfterLogin()
    {
        if (is_null($this->_isFirstPageAfterLogin)) {
            $this->_isFirstPageAfterLogin = $this->getData('is_first_visit', true);
        }
        return $this->_isFirstPageAfterLogin;
    }

    /**
     * Setter whether the current/next page should be treated as first page after login
     *
     * @param bool $value
     * @return Mage_Admin_Model_Session
     */
    public function setIsFirstPageAfterLogin($value)
    {
        $this->_isFirstPageAfterLogin = (bool)$value;
        return $this->setIsFirstVisit($this->_isFirstPageAfterLogin);
    }

    /**
     * Custom REQUEST_URI logic
     *
     * @param Mage_Core_Controller_Request_Http $request
     * @return string|null
     */
    protected function _getRequestUri($request = null)
    {
        if (Mage::getSingleton('adminhtml/url')->useSecretKey()) {
            return Mage::getSingleton('adminhtml/url')->getUrl('*/*/*', array('_current' => true));
        } elseif ($request) {
            return $request->getRequestUri();
        } else {
            return null;
        }
    }

    /**
     * Login failed process
     *
     * @param Exception $e
     * @param string $username
     * @param string $message
     * @param Mage_Core_Controller_Request_Http $request
     * @return void
     */
    protected function _loginFailed($e, $request, $username, $message)
    {
        try {
            Mage::dispatchEvent('admin_session_user_login_failed', array(
                'user_name' => $username,
                'exception' => $e
            ));
        } catch (Exception $e) {
        }

        if ($request && !$request->getParam('messageSent')) {
            Mage::getSingleton('adminhtml/session')->addError($message);
            $request->setParam('messageSent', true);
        }
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Admin
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Admin redirect policy model, guard admin from direct link to store/category/product deletion
 *
 * @category    Mage
 * @package     Mage_Admin
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Admin_Model_Redirectpolicy
{
    /**
     * @var Mage_Adminhtml_Model_Url
     */
    protected $_urlModel;

    /**
     * @param array $parameters array('urlModel' => object)
     */
    public function __construct($parameters = array())
    {
        /** @var Mage_Adminhtml_Model_Url _urlModel */
        $this->_urlModel = (!empty($parameters['urlModel'])) ?
            $parameters['urlModel'] : Mage::getModel('adminhtml/url');
    }

    /**
     * Redirect to startup page after logging in if request contains any params (except security key)
     *
     * @param Mage_Admin_Model_User $user
     * @param Zend_Controller_Request_Http $request
     * @param string|null $alternativeUrl
     * @return null|string
     */
    public function getRedirectUrl(Mage_Admin_Model_User $user, Zend_Controller_Request_Http $request = null,
                                $alternativeUrl = null)
    {
        if (empty($request)) {
            return;
        }
        $countRequiredParams = ($this->_urlModel->useSecretKey()
            && $request->getParam(Mage_Adminhtml_Model_Url::SECRET_KEY_PARAM_NAME)) ? 1 : 0;
        $countGetParams = count($request->getUserParams()) + count($request->getQuery());

        return ($countGetParams > $countRequiredParams) ?
            $this->_urlModel->getUrl($user->getStartupPageUrl()) : $alternativeUrl;
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Mage_Adminhtml_Model_Url extends Mage_Core_Model_Url
{
    /**
     * Secret key query param name
     */
    const SECRET_KEY_PARAM_NAME = 'key';

    /**
     * Retrieve is secure mode for ULR logic
     *
     * @return bool
     */
    public function getSecure()
    {
        if ($this->hasData('secure_is_forced')) {
            return $this->getData('secure');
        }
        return Mage::getStoreConfigFlag('web/secure/use_in_adminhtml');
    }

    /**
     * Force strip secret key param if _nosecret param specified
     *
     * @return Mage_Core_Model_Url
     */
    public function setRouteParams(array $data, $unsetOldParams=true)
    {
        if (isset($data['_nosecret'])) {
            $this->setNoSecret(true);
            unset($data['_nosecret']);
        } else {
            $this->setNoSecret(false);
        }

        return parent::setRouteParams($data, $unsetOldParams);
    }

    /**
     * Custom logic to retrieve Urls
     *
     * @param string $routePath
     * @param array $routeParams
     * @return string
     */
    public function getUrl($routePath=null, $routeParams=null)
    {
        $cacheSecretKey = false;
        if (is_array($routeParams) && isset($routeParams['_cache_secret_key'])) {
            unset($routeParams['_cache_secret_key']);
            $cacheSecretKey = true;
        }

        $result = parent::getUrl($routePath, $routeParams);
        if (!$this->useSecretKey()) {
            return $result;
        }

        $_route = $this->getRouteName() ? $this->getRouteName() : '*';
        $_controller = $this->getControllerName() ? $this->getControllerName() : $this->getDefaultControllerName();
        $_action = $this->getActionName() ? $this->getActionName() : $this->getDefaultActionName();

        if ($cacheSecretKey) {
            $secret = array(self::SECRET_KEY_PARAM_NAME => "\${$_controller}/{$_action}\$");
        }
        else {
            $secret = array(self::SECRET_KEY_PARAM_NAME => $this->getSecretKey($_controller, $_action));
        }
        if (is_array($routeParams)) {
            $routeParams = array_merge($secret, $routeParams);
        } else {
            $routeParams = $secret;
        }
        if (is_array($this->getRouteParams())) {
            $routeParams = array_merge($this->getRouteParams(), $routeParams);
        }

        return parent::getUrl("{$_route}/{$_controller}/{$_action}", $routeParams);
    }

    /**
     * Generate secret key for controller and action based on form key
     *
     * @param string $controller Controller name
     * @param string $action Action name
     * @return string
     */
    public function getSecretKey($controller = null, $action = null)
    {
        $salt = Mage::getSingleton('core/session')->getFormKey();

        $p = explode('/', trim($this->getRequest()->getOriginalPathInfo(), '/'));
        if (!$controller) {
            $controller = !empty($p[1]) ? $p[1] : $this->getRequest()->getControllerName();
        }
        if (!$action) {
            $action = !empty($p[2]) ? $p[2] : $this->getRequest()->getActionName();
        }

        $secret = $controller . $action . $salt;
        return Mage::helper('core')->getHash($secret);
    }

    /**
     * Return secret key settings flag
     *
     * @return boolean
     */
    public function useSecretKey()
    {
        return Mage::getStoreConfigFlag('admin/security/use_form_key') && !$this->getNoSecret();
    }

    /**
     * Enable secret key using
     *
     * @return Mage_Adminhtml_Model_Url
     */
    public function turnOnSecretKey()
    {
        $this->setNoSecret(false);
        return $this;
    }

    /**
     * Disable secret key using
     *
     * @return Mage_Adminhtml_Model_Url
     */
    public function turnOffSecretKey()
    {
        $this->setNoSecret(true);
        return $this;
    }

    /**
     * Refresh admin menu cache etc.
     *
     * @return Mage_Adminhtml_Model_Url
     */
    public function renewSecretUrls()
    {
        Mage::app()->cleanCache(array(Mage_Adminhtml_Block_Page_Menu::CACHE_TAGS));
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Admin
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * ACL user resource
 *
 * @category    Mage
 * @package     Mage_Admin
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Admin_Model_Resource_User extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Define main table
     *
     */
    protected function _construct()
    {
        $this->_init('admin/user', 'user_id');
    }

    /**
     * Initialize unique fields
     *
     * @return Mage_Admin_Model_Resource_User
     */
    protected function _initUniqueFields()
    {
        $this->_uniqueFields = array(
            array(
                'field' => 'email',
                'title' => Mage::helper('adminhtml')->__('Email')
            ),
            array(
                'field' => 'username',
                'title' => Mage::helper('adminhtml')->__('User Name')
            ),
        );
        return $this;
    }

    /**
     * Authenticate user by $username and $password
     *
     * @param Mage_Admin_Model_User $user
     * @return Mage_Admin_Model_Resource_User
     */
    public function recordLogin(Mage_Admin_Model_User $user)
    {
        $adapter = $this->_getWriteAdapter();

        $data = array(
            'logdate' => now(),
            'lognum'  => $user->getLognum() + 1
        );

        $condition = array(
            'user_id = ?' => (int) $user->getUserId(),
        );

        $adapter->update($this->getMainTable(), $data, $condition);

        return $this;
    }

    /**
     * Load data by specified username
     *
     * @param string $username
     * @return false|array
     */
    public function loadByUsername($username)
    {
        $adapter = $this->_getReadAdapter();

        $select = $adapter->select()
                    ->from($this->getMainTable())
                    ->where('username=:username');

        $binds = array(
            'username' => $username
        );

        return $adapter->fetchRow($select, $binds);
    }

    /**
     * Check if user is assigned to any role
     *
     * @param int|Mage_Core_Admin_Model_User $user
     * @return null|false|array
     */
    public function hasAssigned2Role($user)
    {
        if (is_numeric($user)) {
            $userId = $user;
        } else if ($user instanceof Mage_Core_Model_Abstract) {
            $userId = $user->getUserId();
        } else {
            return null;
        }

        if ( $userId > 0 ) {
            $adapter = $this->_getReadAdapter();

            $select = $adapter->select();
            $select->from($this->getTable('admin/role'))
                ->where('parent_id > :parent_id')
                ->where('user_id = :user_id');

            $binds = array(
                'parent_id' => 0,
                'user_id' => $userId,
            );

            return $adapter->fetchAll($select, $binds);
        } else {
            return null;
        }
    }

    /**
     * Encrypt password
     *
     * @param string $pwStr
     * @return string
     */
    private function _encryptPassword($pwStr)
    {
        return Mage::helper('core')->getHash($pwStr, Mage_Admin_Model_User::HASH_SALT_LENGTH);
    }

    /**
     * Set created/modified values before user save
     *
     * @param Mage_Core_Model_Abstract $user
     * @return Mage_Admin_Model_Resource_User
     */
    protected function _beforeSave(Mage_Core_Model_Abstract $user)
    {
        if ($user->isObjectNew()) {
            $user->setCreated($this->formatDate(true));
        }
        $user->setModified($this->formatDate(true));

        return parent::_beforeSave($user);
    }

    /**
     * Unserialize user extra data after user save
     *
     * @param Mage_Core_Model_Abstract $user
     * @return Mage_Admin_Model_Resource_User
     */
    protected function _afterSave(Mage_Core_Model_Abstract $user)
    {
        $this->_unserializeExtraData($user);
        return $this;
    }

    /**
     * Unserialize user extra data after user load
     *
     * @param Mage_Core_Model_Abstract $user
     * @return Mage_Admin_Model_Resource_User
     */
    protected function _afterLoad(Mage_Core_Model_Abstract $user)
    {
        return parent::_afterLoad($this->_unserializeExtraData($user));
    }

    /**
     * Delete user role record with user
     *
     * @param Mage_Core_Model_Abstract $user
     * @return bool
     */
    public function delete(Mage_Core_Model_Abstract $user)
    {
        $this->_beforeDelete($user);
        $adapter = $this->_getWriteAdapter();

        $uid = $user->getId();
        $adapter->beginTransaction();
        try {
            $conditions = array(
                'user_id = ?' => $uid
            );

            $adapter->delete($this->getMainTable(), $conditions);
            $adapter->delete($this->getTable('admin/role'), $conditions);
        } catch (Mage_Core_Exception $e) {
            throw $e;
            return false;
        } catch (Exception $e) {
            $adapter->rollBack();
            return false;
        }
        $adapter->commit();
        $this->_afterDelete($user);
        return true;
    }

    /**
     * TODO: unify _saveRelations() and add() methods, they make same things
     *
     * @param Mage_Core_Model_Abstract $user
     * @return Mage_Admin_Model_Resource_User
     */
    public function _saveRelations(Mage_Core_Model_Abstract $user)
    {
        $rolesIds = $user->getRoleIds();

        if (!is_array($rolesIds) || count($rolesIds) == 0) {
            return $user;
        }

        $adapter = $this->_getWriteAdapter();

        $adapter->beginTransaction();

        try {
            $conditions = array(
                'user_id = ?' => (int) $user->getId(),
            );

            $adapter->delete($this->getTable('admin/role'), $conditions);
            foreach ($rolesIds as $rid) {
                $rid = intval($rid);
                if ($rid > 0) {
                    $row = Mage::getModel('admin/role')->load($rid)->getData();
                } else {
                    $row = array('tree_level' => 0);
                }

                $data = new Varien_Object(array(
                    'parent_id'     => $rid,
                    'tree_level'    => $row['tree_level'] + 1,
                    'sort_order'    => 0,
                    'role_type'     => 'U',
                    'user_id'       => $user->getId(),
                    'role_name'     => $user->getFirstname()
                ));

                $insertData = $this->_prepareDataForTable($data, $this->getTable('admin/role'));
                $adapter->insert($this->getTable('admin/role'), $insertData);
            }
            $adapter->commit();
        } catch (Mage_Core_Exception $e) {
            throw $e;
        } catch (Exception $e) {
            $adapter->rollBack();
            throw $e;
        }

        return $this;
    }

    /**
     * Get user roles
     *
     * @param Mage_Core_Model_Abstract $user
     * @return array
     */
    public function getRoles(Mage_Core_Model_Abstract $user)
    {
        if ( !$user->getId() ) {
            return array();
        }

        $table  = $this->getTable('admin/role');
        $adapter   = $this->_getReadAdapter();

        $select = $adapter->select()
                    ->from($table, array())
                    ->joinLeft(
                        array('ar' => $table),
                        "(ar.role_id = {$table}.parent_id and ar.role_type = 'G')",
                        array('role_id'))
                    ->where("{$table}.user_id = :user_id");

        $binds = array(
            'user_id' => (int) $user->getId(),
        );

        $roles = $adapter->fetchCol($select, $binds);

        if ($roles) {
            return $roles;
        }

        return array();
    }

    /**
     * Save user roles
     *
     * @param Mage_Core_Model_Abstract $user
     * @return Mage_Admin_Model_Resource_User
     */
    public function add(Mage_Core_Model_Abstract $user)
    {
        $dbh = $this->_getWriteAdapter();

        $aRoles = $this->hasAssigned2Role($user);
        if ( sizeof($aRoles) > 0 ) {
            foreach ($aRoles as $idx => $data) {
                $conditions = array(
                    'role_id = ?' => $data['role_id'],
                );

                $dbh->delete($this->getTable('admin/role'), $conditions);
            }
        }

        if ($user->getId() > 0) {
            $role = Mage::getModel('admin/role')->load($user->getRoleId());
        } else {
            $role = new Varien_Object();
            $role->setTreeLevel(0);
        }

        $data = new Varien_Object(array(
            'parent_id'  => $user->getRoleId(),
            'tree_level' => ($role->getTreeLevel() + 1),
            'sort_order' => 0,
            'role_type'  => 'U',
            'user_id'    => $user->getUserId(),
            'role_name'  => $user->getFirstname()
        ));

        $insertData = $this->_prepareDataForTable($data, $this->getTable('admin/role'));

        $dbh->insert($this->getTable('admin/role'), $insertData);

        return $this;
    }

    /**
     * Delete user role
     *
     * @param Mage_Core_Model_Abstract $user
     * @return Mage_Admin_Model_Resource_User
     */
    public function deleteFromRole(Mage_Core_Model_Abstract $user)
    {
        if ( $user->getUserId() <= 0 ) {
            return $this;
        }
        if ( $user->getRoleId() <= 0 ) {
            return $this;
        }

        $dbh = $this->_getWriteAdapter();

        $condition = array(
            'user_id = ?'   => (int) $user->getId(),
            'parent_id = ?' => (int) $user->getRoleId(),
        );

        $dbh->delete($this->getTable('admin/role'), $condition);
        return $this;
    }

    /**
     * Check if role user exists
     *
     * @param Mage_Core_Model_Abstract $user
     * @return array|false
     */
    public function roleUserExists(Mage_Core_Model_Abstract $user)
    {
        if ( $user->getUserId() > 0 ) {
            $roleTable = $this->getTable('admin/role');

            $dbh = $this->_getReadAdapter();

            $binds = array(
                'parent_id' => $user->getRoleId(),
                'user_id'   => $user->getUserId(),
            );

            $select = $dbh->select()->from($roleTable)
                ->where('parent_id = :parent_id')
                ->where('user_id = :user_id');

            return $dbh->fetchCol($select, $binds);
        } else {
            return array();
        }
    }

    /**
     * Check if user exists
     *
     * @param Mage_Core_Model_Abstract $user
     * @return array|false
     */
    public function userExists(Mage_Core_Model_Abstract $user)
    {
        $adapter = $this->_getReadAdapter();
        $select = $adapter->select();

        $binds = array(
            'username' => $user->getUsername(),
            'email'    => $user->getEmail(),
            'user_id'  => (int) $user->getId(),
        );

        $select->from($this->getMainTable())
            ->where('(username = :username OR email = :email)')
            ->where('user_id <> :user_id');

        return $adapter->fetchRow($select, $binds);
    }

    /**
     * Save user extra data
     *
     * @param Mage_Core_Model_Abstract $object
     * @param string $data
     * @return Mage_Admin_Model_Resource_User
     */
    public function saveExtra($object, $data)
    {
        if ($object->getId()) {
            $this->_getWriteAdapter()->update(
                $this->getMainTable(),
                array('extra' => $data),
                array('user_id = ?' => (int) $object->getId())
            );
        }

        return $this;
    }

    /**
     * Unserializes user extra data
     *
     * @param Mage_Core_Model_Abstract $user
     * @return Mage_Core_Model_Abstract
     */
    protected function _unserializeExtraData(Mage_Core_Model_Abstract $user)
    {
        try {
            $unsterilizedData = Mage::helper('core/unserializeArray')->unserialize($user->getExtra());
            $user->setExtra($unsterilizedData);
        } catch (Exception $e) {
            $user->setExtra(false);
        }
        return $user;
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Core
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Core unserialize helper
 *
 * @category    Mage
 * @package     Mage_Core
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Core_Helper_UnserializeArray
{
    /**
     * @param string $str
     * @return array
     * @throws Exception
     */
    public function unserialize($str)
    {
        $parser = new Unserialize_Parser();
        return $parser->unserialize($str);
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_AdminNotification
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * AdminNotification observer
 *
 * @category   Mage
 * @package    Mage_AdminNotification
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_AdminNotification_Model_Observer
{
    /**
     * Predispath admin action controller
     *
     * @param Varien_Event_Observer $observer
     */
    public function preDispatch(Varien_Event_Observer $observer)
    {

        if (Mage::getSingleton('admin/session')->isLoggedIn()) {

            $feedModel  = Mage::getModel('adminnotification/feed');
            /* @var $feedModel Mage_AdminNotification_Model_Feed */

            $feedModel->checkUpdate();

        }

    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_AdminNotification
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * AdminNotification Feed model
 *
 * @category   Mage
 * @package    Mage_AdminNotification
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_AdminNotification_Model_Feed extends Mage_Core_Model_Abstract
{
    const XML_USE_HTTPS_PATH    = 'system/adminnotification/use_https';
    const XML_FEED_URL_PATH     = 'system/adminnotification/feed_url';
    const XML_FREQUENCY_PATH    = 'system/adminnotification/frequency';
    const XML_LAST_UPDATE_PATH  = 'system/adminnotification/last_update';

    /**
     * Feed url
     *
     * @var string
     */
    protected $_feedUrl;

    /**
     * Init model
     *
     */
    protected function _construct()
    {}

    /**
     * Retrieve feed url
     *
     * @return string
     */
    public function getFeedUrl()
    {
        if (is_null($this->_feedUrl)) {
            $this->_feedUrl = (Mage::getStoreConfigFlag(self::XML_USE_HTTPS_PATH) ? 'https://' : 'http://')
                . Mage::getStoreConfig(self::XML_FEED_URL_PATH);
        }
        return $this->_feedUrl;
    }

    /**
     * Check feed for modification
     *
     * @return Mage_AdminNotification_Model_Feed
     */
    public function checkUpdate()
    {
        if (($this->getFrequency() + $this->getLastUpdate()) > time()) {
            return $this;
        }

        $feedData = array();

        $feedXml = $this->getFeedData();

        if ($feedXml && $feedXml->channel && $feedXml->channel->item) {
            foreach ($feedXml->channel->item as $item) {
                $feedData[] = array(
                    'severity'      => (int)$item->severity,
                    'date_added'    => $this->getDate((string)$item->pubDate),
                    'title'         => (string)$item->title,
                    'description'   => (string)$item->description,
                    'url'           => (string)$item->link,
                );
            }

            if ($feedData) {
                Mage::getModel('adminnotification/inbox')->parse(array_reverse($feedData));
            }

        }
        $this->setLastUpdate();

        return $this;
    }

    /**
     * Retrieve DB date from RSS date
     *
     * @param string $rssDate
     * @return string YYYY-MM-DD YY:HH:SS
     */
    public function getDate($rssDate)
    {
        return gmdate('Y-m-d H:i:s', strtotime($rssDate));
    }

    /**
     * Retrieve Update Frequency
     *
     * @return int
     */
    public function getFrequency()
    {
        return Mage::getStoreConfig(self::XML_FREQUENCY_PATH) * 3600;
    }

    /**
     * Retrieve Last update time
     *
     * @return int
     */
    public function getLastUpdate()
    {
        return Mage::app()->loadCache('admin_notifications_lastcheck');
//        return Mage::getStoreConfig(self::XML_LAST_UPDATE_PATH);
    }

    /**
     * Set last update time (now)
     *
     * @return Mage_AdminNotification_Model_Feed
     */
    public function setLastUpdate()
    {
        Mage::app()->saveCache(time(), 'admin_notifications_lastcheck');
//        $config = Mage::getModel('core/config');
//        /* @var $config Mage_Core_Model_Config */
//        $config->saveConfig(self::XML_LAST_UPDATE_PATH, time());
        return $this;
    }

    /**
     * Retrieve feed data as XML element
     *
     * @return SimpleXMLElement
     */
    public function getFeedData()
    {
        $curl = new Varien_Http_Adapter_Curl();
        $curl->setConfig(array(
            'timeout'   => 2
        ));
        $curl->write(Zend_Http_Client::GET, $this->getFeedUrl(), '1.0');
        $data = $curl->read();
        if ($data === false) {
            return false;
        }
        $data = preg_split('/^\r?$/m', $data, 2);
        $data = trim($data[1]);
        $curl->close();

        try {
            $xml  = new SimpleXMLElement($data);
        }
        catch (Exception $e) {
            return false;
        }

        return $xml;
    }

    public function getFeedXml()
    {
        try {
            $data = $this->getFeedData();
            $xml  = new SimpleXMLElement($data);
        }
        catch (Exception $e) {
            $xml  = new SimpleXMLElement('<?xml version="1.0" encoding="utf-8" ?>');
        }

        return $xml;
    }
}
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Afptc
 * @version    1.1.12
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_All_Model_Feed_Abstract extends Mage_Core_Model_Abstract
{

    /**
     * Retrieve feed data as XML element
     *
     * @return SimpleXMLElement
     */
    public function getFeedData()
    {
        $curl = new Varien_Http_Adapter_Curl();
        $curl->setConfig(array(
                              'timeout' => 1
                         ));
        $curl->write(Zend_Http_Client::GET, $this->getFeedUrl(), '1.0');
        $data = $curl->read();
        if ($data === false) {
            return false;
        }
        $data = preg_split('/^\r?$/m', $data, 2);
        $data = trim($data[1]);
        $curl->close();

        try {
            $xml = new SimpleXMLElement($data);
        }
        catch (Exception $e) {
            return false;
        }

        return $xml;
    }


    /**
     * Retrieve DB date from RSS date
     *
     * @param string $rssDate
     * @return string YYYY-MM-DD YY:HH:SS
     */
    public function getDate($rssDate)
    {
        return gmdate('Y-m-d H:i:s', strtotime($rssDate));
    }


}
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Afptc
 * @version    1.1.12
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_All_Model_Feed_Extensions extends AW_All_Model_Feed_Abstract
{
    protected $_extensions = array(
        'AW_Featuredproducts' => 'http://confluence.aheadworks.com/display/EUDOC/Featured+Products+2',
        'AW_Searchautocomplete' => 'http://confluence.aheadworks.com/display/EUDOC/Search+Autocomplete+and+Suggest',
        'AW_PQUESTION2' => 'http://confluence.aheadworks.com/display/EUDOC/Product+Questions+2',
        'AW_Helpdesk' => 'http://confluence.aheadworks.com/display/EUDOC/Help+Desk+Ultimate',
        'AW_Zblocks' => 'http://confluence.aheadworks.com/display/EUDOC/Z-Blocks',
        'AW_Aheadvideo' => 'http://confluence.aheadworks.com/display/EUDOC/Video+Module',
        'AW_Rssreader' => 'http://confluence.aheadworks.com/display/EUDOC/RSS+Reader',
        'AW_Advancedmenu' => 'http://confluence.aheadworks.com/display/EUDOC/Advanced+Menu',
        'AW_Helpdesk3' => 'http://confluence.aheadworks.com/display/EUDOC/Help+Desk+Ultimate+3',
        'AW_Ajaxcartpro' => 'http://confluence.aheadworks.com/display/EUDOC/AJAX+Cart+Pro',
        'AW_Reviewrotator' => 'http://confluence.aheadworks.com/display/EUDOC/Review+Rotator',
        'AW_Relatedproducts' => 'http://confluence.aheadworks.com/display/EUDOC/Who+Bought+This+Also+Bought',
        'AW_Boughttogether' => 'http://confluence.aheadworks.com/display/EUDOC/Frequently+Bought+Together',
        'AW_Onsale' => 'http://confluence.aheadworks.com/display/EUDOC/On+Sale',
        'AW_Followupemail' => 'http://confluence.aheadworks.com/display/EUDOC/Follow+Up+Email',
        'AW_Checkoutpromo' => 'http://confluence.aheadworks.com/display/EUDOC/Checkout+Promo',
        'AW_Booking' => 'http://confluence.aheadworks.com/display/EUDOC/Booking+and+Reservations',
        'AW_Blog' => 'http://confluence.aheadworks.com/display/EUDOC/Blog',
        'AW_Gridmanager' => 'http://confluence.aheadworks.com/display/EUDOC/Grid+Manager',
        'AW_Raf' => 'http://confluence.aheadworks.com/display/EUDOC/Refer+a+Friend',
        'AW_Maprice' => 'http://confluence.aheadworks.com/display/EUDOC/Minimum+Advertised+Price',
        'AW_AdvancedReviews' => 'http://confluence.aheadworks.com/display/EUDOC/Advanced+Reviews',
        'AW_Productupdates' => 'http://confluence.aheadworks.com/display/EUDOC/Product+Updates+Notifications',
        'AW_Customerpurchases' => 'http://confluence.aheadworks.com/display/EUDOC/Customer+Purchases',
        'AW_Advancedreports' => 'http://confluence.aheadworks.com/display/EUDOC/Advanced+Reports',
        'AW_Advancednewsletter' => 'http://confluence.aheadworks.com/display/EUDOC/Advanced+Newsletter',
        'AW_Easycategories' => 'http://confluence.aheadworks.com/display/EUDOC/Easy+Categories',
        'AW_Sarp2' => 'http://confluence.aheadworks.com/display/EUDOC/Subscriptions+And+Recurring+Payments+2.x',
        'AW_Deliverydate' => 'http://confluence.aheadworks.com/display/EUDOC/Delivery+Date+and+Notice',
        'AW_Automaticcallouts' => 'http://confluence.aheadworks.com/display/EUDOC/Automatic+Product+Callouts',
        'AW_Previousnext' => 'http://confluence.aheadworks.com/pages/viewpage.action?pageId=18317983',
        'AW_Customsmtp' => 'http://confluence.aheadworks.com/display/EUDOC/Custom+SMTP',
        'AW_Hometabspro' => 'http://confluence.aheadworks.com/display/EUDOC/Home+Tabs+Pro',
        'AW_Marketsuite' => 'http://confluence.aheadworks.com/display/EUDOC/Market+Segmentation+Suite',
        'AW_Kbase' => 'http://confluence.aheadworks.com/display/EUDOC/Knowledge+Base',
        'AW_Vidtest' => 'http://confluence.aheadworks.com/display/EUDOC/Video+Testimonials',
        'AW_Ascurl' => 'http://confluence.aheadworks.com/display/EUDOC/Ultimate+SEO+Suite',
        'AW_Mobile2' => 'http://confluence.aheadworks.com/display/EUDOC/iPhone+Theme+2',
        'AW_Popup' => 'http://confluence.aheadworks.com/pages/viewpage.action?pageId=16910274',
        'AW_Rma' => 'http://confluence.aheadworks.com/display/EUDOC/RMA',
        'AW_Ordertags' => 'http://confluence.aheadworks.com/display/EUDOC/Order+Tags',
        'AW_Sociable' => 'http://confluence.aheadworks.com/display/EUDOC/Sociable',
        'AW_Ppp' => 'http://confluence.aheadworks.com/display/EUDOC/Product+Preview+Pro',
        'AW_Featured' => 'http://confluence.aheadworks.com/display/EUDOC/Featured+Products+3',
        'AW_Pmatch' => 'http://confluence.aheadworks.com/display/EUDOC/Price+Match',
        'AW_Alsoviewed' => 'http://confluence.aheadworks.com/display/EUDOC/Who+Viewed+This+Also+Viewed',
        'AW_Points' => 'http://confluence.aheadworks.com/display/EUDOC/Points+And+Rewards',
        'AW_Islider' => 'http://confluence.aheadworks.com/display/EUDOC/Product+Images+Slider',
        'AW_Goislider' => 'http://confluence.aheadworks.com/display/EUDOC/GoSlider',
        'AW_Catalogpermissions' => 'http://confluence.aheadworks.com/display/EUDOC/Catalog+Permissions',
        'AW_Ajaxcatalog' => 'http://confluence.aheadworks.com/display/EUDOC/AJAX+Catalog',
        'AW_Autorelated' => 'http://confluence.aheadworks.com/display/EUDOC/Automatic+Related+Products+2',
        'AW_Collpur' => 'http://confluence.aheadworks.com/display/EUDOC/Group+Deals',
        'AW_Advancedsearch' => 'http://confluence.aheadworks.com/display/EUDOC/Advanced+Search',
        'AW_Avail' => 'http://confluence.aheadworks.com/display/EUDOC/Custom+Stock+Status',
        'AW_Mobiletracking' => 'http://confluence.aheadworks.com/display/EUDOC/Mobile+Order+Tracking',
        'AW_Countdown' => 'http://confluence.aheadworks.com/display/EUDOC/Countdown',
        'AW_Affiliate' => 'http://confluence.aheadworks.com/display/EUDOC/Magento+Affiliate',
        'AW_Randomprice' => 'http://confluence.aheadworks.com/display/EUDOC/Random+Product+Price',
        'AW_Eventdiscount' => 'http://confluence.aheadworks.com/display/EUDOC/Event-Based+Discounts',
        'AW_Onpulse' => 'http://confluence.aheadworks.com/display/EUDOC/OnPulse+Magento+Connector',
        'AW_Admingridimages' => 'http://confluence.aheadworks.com/display/EUDOC/Admin+Grid+Thumbnail',
        'AW_Shopbybrand' => 'http://confluence.aheadworks.com/display/EUDOC/Shop+by+Brand',
        'AW_Customerattributes' => 'http://confluence.aheadworks.com/display/EUDOC/Customer+Attributes',
        'AW_Activitystream' => 'http://confluence.aheadworks.com/display/EUDOC/Activity+Stream',
        'AW_Storelocator' => 'http://confluence.aheadworks.com/display/EUDOC/Store+Locator',
        'AW_ShippingPrice' => 'http://confluence.aheadworks.com/display/EUDOC/Shipping+Price',
        'AW_Onestepcheckout' => 'http://confluence.aheadworks.com/display/EUDOC/One+Step+Checkout',
        'AW_Auction' => 'http://confluence.aheadworks.com/display/EUDOC/Auction+Pro',
        'AW_Betterthankyoupage' => 'http://confluence.aheadworks.com/display/EUDOC/Better+Thank+You+Page',
        'AW_Gamification' => 'http://confluence.aheadworks.com/display/EUDOC/eCommerce+Gamification+Suite',
        'AW_Callforprice' => 'http://confluence.aheadworks.com/display/EUDOC/Call+For+Price',
        'AW_Afptc' => 'http://confluence.aheadworks.com/display/EUDOC/Add+Free+Product+To+Cart',
        'AW_Orderattributes' => 'http://confluence.aheadworks.com/display/EUDOC/Order+Attributes',
        'AW_Ajaxlogin' => 'http://confluence.aheadworks.com/display/EUDOC/Ajax+Login+Pro',
        'AW_Giftcard' => 'http://confluence.aheadworks.com/pages/viewpage.action?pageId=16909150',
        'AW_Giftwrap' => 'http://confluence.aheadworks.com/display/EUDOC/Gift+Wrap',
        'AW_Extradownloads' => 'http://confluence.aheadworks.com/display/EUDOC/Extra+Downloads',
        'AW_Colorswatches' => 'http://confluence.aheadworks.com/display/EUDOC/Product+Color+Swatches',
        'AW_Layerednavigation' => 'http://confluence.aheadworks.com/display/EUDOC/Layered+Navigation',
        'AW_Eventbooking' => 'http://confluence.aheadworks.com/display/EUDOC/Event+Tickets',
        'AW_Storecredit' => 'http://confluence.aheadworks.com/display/EUDOC/Store+Credit+and+Refund',
        'AW_Coupongenerator' => 'http://confluence.aheadworks.com/display/EUDOC/Quick+Coupon+Generator',
    );

    /**
     * Retrieve feed url
     *
     * @return string
     */
    public function getFeedUrl()
    {
        return AW_All_Helper_Config::EXTENSIONS_FEED_URL;
    }


    /**
     * Checks feed
     * @return
     */
    public function check()
    {
        if (!(Mage::app()->loadCache('aw_all_extensions_feed')) || (time() - Mage::app()->loadCache('aw_all_extensions_feed_lastcheck')) > Mage::getStoreConfig('awall/feed/check_frequency')) {
            $this->refresh();
        }
    }

    public function refresh()
    {
        $exts = array();
        try {
            $Node = $this->getFeedData();
            if (!$Node) return false;
            foreach ($Node->children() as $ext) {
                $exts[(string)$ext->name] = array(
                    'display_name' => (string)$ext->display_name,
                    'version' => (string)$ext->version,
                    'url' => (string)$ext->url,
                    'documentation_url' => $this->getDocumentationUrl((string)$ext->name),
                );
            }

            Mage::app()->saveCache(serialize($exts), 'aw_all_extensions_feed');
            Mage::app()->saveCache(time(), 'aw_all_extensions_feed_lastcheck');
            return true;
        } catch (Exception $E) {
            return false;
        }
    }

    public function checkExtensions()
    {
        $modules = array_keys((array)Mage::getConfig()->getNode('modules')->children());
        sort($modules);

        $magentoPlatform = AW_All_Helper_Versions::getPlatform();
        foreach ($modules as $extensionName) {
            if (strstr($extensionName, 'AW_') === false) {
                continue;
            }
            if ($extensionName == 'AW_Core' || $extensionName == 'AW_All') {
                continue;
            }
            if ($platformNode = $this->getExtensionPlatform($extensionName)) {
                $extensionPlatform = AW_All_Helper_Versions::convertPlatform($platformNode);
                if ($extensionPlatform < $magentoPlatform) {
                    $this->disableExtensionOutput($extensionName);
                }
            }
        }
        return $this;
    }

    public function getExtensionPlatform($extensionName)
    {
        try {
            if ($platform = Mage::getConfig()->getNode("modules/$extensionName/platform")) {
                $platform = strtolower($platform);
                return $platform;
            } else {
                throw new Exception();
            }
        } catch (Exception $e) {
            return false;
        }
    }


    public function disableExtensionOutput($extensionName)
    {
        $coll = Mage::getModel('core/config_data')->getCollection();
        $coll->getSelect()->where("path='advanced/modules_disable_output/$extensionName'");
        $i = 0;
        foreach ($coll as $cd) {
            $i++;
            $cd->setValue(1)->save();
        }
        if ($i == 0) {
            Mage::getModel('core/config_data')
                    ->setPath("advanced/modules_disable_output/$extensionName")
                    ->setValue(1)
                    ->save();
        }
        return $this;
    }

    public function getDocumentationUrl($extensionName)
    {
        if (isset($this->_extensions[$extensionName])) {
            return $this->_extensions[$extensionName];
        }
        return 'http://confluence.aheadworks.com/display/EUDOC/';
    }

}
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Afptc
 * @version    1.1.12
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_All_Model_Feed_Updates extends AW_All_Model_Feed_Abstract
{

    /**
     * Retrieve feed url
     *
     * @return string
     */
    public function getFeedUrl()
    {
        return AW_All_Helper_Config::UPDATES_FEED_URL;
    }

    /**
     * Checks feed
     * @return
     */
    public function check()
    {
        if ((time() - Mage::app()->loadCache('aw_all_updates_feed_lastcheck')) > Mage::getStoreConfig('awall/feed/check_frequency')) {
            $this->refresh();
        }
    }

    public function refresh()
    {
        $feedData = array();

        try {

            $Node = $this->getFeedData();
            if (!$Node) return false;
            foreach ($Node->children() as $item) {

                if ($this->isInteresting($item)) {
                    $date = strtotime((string)$item->date);
                    if (!Mage::getStoreConfig('awall/install/run') || (Mage::getStoreConfig('awall/install/run') < $date)) {
                        $feedData[] = array(
                            'severity' => 3,
                            'date_added' => $this->getDate((string)$item->date),
                            'title' => (string)$item->title,
                            'description' => (string)$item->content,
                            'url' => (string)$item->url,
                        );
                    }
                }
            }

            $adminnotificationModel = Mage::getModel('adminnotification/inbox');
            if ($feedData && is_object($adminnotificationModel)) {
                $adminnotificationModel->parse(($feedData));
            }

            Mage::app()->saveCache(time(), 'aw_all_updates_feed_lastcheck');
            return true;
        } catch (Exception $E) {
            return false;
        }
    }


    public function getInterests()
    {
        if (!$this->getData('interests')) {
            $types = @explode(',', Mage::getStoreConfig('awall/feed/interests'));
            $this->setData('interests', $types);
        }
        return $this->getData('interests');
    }

    /**
     *
     * @return
     */
    public function isInteresting($item)
    {
        $interests = $this->getInterests();

        $types = @explode(",", (string)$item->type);
        $exts = @explode(",", (string)$item->extensions);

        $isInterestedInSelfUpgrades = array_search(AW_All_Model_Source_Updates_Type::TYPE_INSTALLED_UPDATE, $types);

        foreach ($types as $type) {

            if (array_search($type, $interests) !== false) {
                return true;
            }
            if (($type == AW_All_Model_Source_Updates_Type::TYPE_UPDATE_RELEASE) && $isInterestedInSelfUpgrades) {
                foreach ($exts as $ext) {
                    if ($this->isExtensionInstalled($ext)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public function isExtensionInstalled($code)
    {
        $modules = array_keys((array)Mage::getConfig()->getNode('modules')->children());

        foreach ($modules as $moduleName) {
            if ($moduleName == $code) {
                return true;
            }
        }
        return false;
    }

}
/**
 * @author    Marcin Frymark
 * @email     contact@alekseon.com
 * @company   Alekseon
 * @website   www.alekseon.com
 */
class Alekseon_ModulesConflictDetector_Model_AlekseonAdminNotification_Observer
{
    public function preDispatch(Varien_Event_Observer $observer)
    {
        if (Mage::getSingleton('admin/session')->isLoggedIn()) {
            $feedModel  = Mage::getModel('alekseon_modulesConflictDetector/alekseonAdminNotification_feed');
            $feedModel->checkUpdate();
        }

    }
}
/**
 * @author    Marcin Frymark
 * @email     contact@alekseon.com
 * @company   Alekseon
 * @website   www.alekseon.com
 */
class Alekseon_ModulesConflictDetector_Model_AlekseonAdminNotification_Feed extends Mage_AdminNotification_Model_Feed
{
    const XML_ENABLED_PATH = 'alekseon_adminNotification/general/enabled';
    const XML_FREQUENCY_PATH = 'alekseon_adminNotification/general/frequency';
    const NOTIFICANTION_LASTCHECK_CACHE_KEY = 'alekseon_notifications_lastcheck';

    protected $_alekseonInstalledModules;
    
    public function getFeedUrl()
    {
        if (is_null($this->_feedUrl)) {
            $this->_feedUrl = Mage::helper('alekseon_modulesConflictDetector')->getAlekseonUrl() . '/rss/magento_rss.xml';
            $query = '?utm_source=' . urlencode(Mage::getStoreConfig('web/unsecure/base_url'));
            $query .= '&utm_medium=' . urlencode('Magento Connect');
            if (method_exists('Mage', 'getEdition')) {
                $query .= '&utm_content=' . urlencode(Mage::getEdition() . ' ' . Mage::getVersion());
            } else {
                $query .= '&utm_content=' . urlencode(Mage::getVersion());
            }
            $query .= '&utm_term=' . urlencode(implode(',', $this->_getAlekseonInstalledModules()));
            
            $this->_feedUrl .= $query;
        }
      
        return $this->_feedUrl;
    }
    
    public function checkUpdate()
    {
        if (!Mage::getStoreConfig(self::XML_ENABLED_PATH)) {
            return $this;
        }
    
        if (($this->getFrequency() + $this->getLastUpdate()) > time()) {
            return $this;
        }

        $feedData = array();
        $feedXml = $this->getFeedData();

        if ($feedXml && $feedXml->channel && $feedXml->channel->item) {
            foreach ($feedXml->channel->item as $item) {
            
                $module = (string)$item->module;
                if ($module && !in_array($module, $this->_getAlekseonInstalledModules())) {
                    continue;
                }
            
                $feedData[] = array(
                    'severity'      => (int)$item->severity,
                    'date_added'    => $this->getDate((string)$item->pubDate),
                    'title'         => (string)$item->title,
                    'description'   => (string)$item->description,
                    'url'           => (string)$item->link,
                );
            }

            if ($feedData) {
                Mage::getModel('adminnotification/inbox')->parse(array_reverse($feedData));
            }

        }
        $this->setLastUpdate();

        return $this;
    }

    public function getLastUpdate()
    {
        return Mage::app()->loadCache(self::NOTIFICANTION_LASTCHECK_CACHE_KEY);
    }
    
    public function setLastUpdate()
    {
        Mage::app()->saveCache(time(), self::NOTIFICANTION_LASTCHECK_CACHE_KEY);
        return $this;
    }
    
    protected function _getAlekseonInstalledModules()
    {
        if (is_null($this->_alekseonInstalledModules)) {
            $modules = array_keys((array)Mage::getConfig()->getNode('modules')->children());
            $this->_alekseonInstalledModules = array();
            foreach ($modules as $moduleName) {
                if (substr($moduleName, 0, 9) == 'Alekseon_'){
                    $this->_alekseonInstalledModules[] = $moduleName;
                }
            }
        }
        return $this->_alekseonInstalledModules;
    }
}

/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_Audit
 */
class Amasty_Audit_Model_Observer
{
    protected $_logData = array();
    protected $_isOrigData = false;
    protected $_isCustomer = false;
    protected $_isFirstLogout = true;
    protected $_oldRules = array();
    protected $_productLinkData = array(
        'up_sell_link_data',
        'cross_sell_link_data',
        'related_link_data'
    );

    protected $_isAmpgrid = NULL;

    //listen controller_action_predispatch event
    public function saveSomeEvent($observer)
    {
        if (Mage::app()->getStore()->isAdmin()) {
            $username = Mage::getSingleton('admin/session')->getUser() ? Mage::getSingleton('admin/session')->getUser()->getUsername() : '';
            $user = Mage::getModel('admin/user')->loadByUsername($username);
            if ($user && Mage::helper('amaudit')->isUserInLog($user->getId())) {//settings log or not user
                $path = $observer->getEvent()->getControllerAction()->getRequest()->getOriginalPathInfo();
                Mage::register('amaudit_log_path', $path, true);
                $arrPath = ($path) ? explode("/", $path) : array();
                $exportType = NULL;
                if ((in_array('exportCsv', $arrPath)) || in_array('exportXml', $arrPath) || in_array('exportPost', $arrPath)) {
                    $this->_saveExport($arrPath, $observer);
                }
                Mage::register('amaudit_admin_path', $arrPath[1], true);
                $this->_saveCompilation($path, $username);
                $this->_saveCache($path, $username);
                $this->_saveIndex($path, $username);
            }
        }
    }

    public function afterBlockCreate($observer)
    {
        $block = $observer->getBlock();
        if ($block instanceof Mage_Adminhtml_Block_Customer_Edit_Tabs) {
            if (Mage::getSingleton('admin/session')->isAllowed('system/amauditmenu/customerhistory')) {
                $this->_addBlock($block, 'adminhtml_tabs_customer', 'tags');
            }
        }

        if ($block instanceof Mage_Adminhtml_Block_Sales_Order_View_Tabs) {
            if (Mage::getSingleton('admin/session')->isAllowed('system/amauditmenu/orderhistory')) {
                $this->_addBlock($block, 'adminhtml_tabs_order', 'order_transactions');
            }
        }

        if ($block instanceof Mage_Adminhtml_Block_Catalog_Product_Edit_Tabs) {
            if ($block->getProduct()->getEntityId()) {
                if (Mage::getSingleton('admin/session')->isAllowed('system/amauditmenu/producthistory')) {
                    $this->_addBlock($block, 'adminhtml_tabs_product', 'super');
                }
            }
        }
    }

    //listen model_delete_after event
    public function modelDeleteAfter($observer)
    {
        if (!Mage::app()->getStore()->isAdmin()) {
            return false;
        }

        $object = $observer->getObject();
        if ($object instanceof Mage_Catalog_Model_Product) {
            $this->_deleteProduct($object);
        } else {
            if (!Mage::registry('amaudit_log_duplicate_save')) {
                $this->_saveLog();
                if (!Mage::registry('amaudit_log_duplicate_save')) {
                    Mage::register('amaudit_log_duplicate_save', 1);
                }
            }
            $this->modelSaveAfter($observer, "Delete");
        }

    }

    //listen model_save_after event
    public function modelSaveAfter($observer, $delete = null)
    {
        $object = $observer->getObject();
        $class = get_class($object);
        /** @var Amasty_Audit_Helper_Data $helper */
        $helper = Mage::helper('amaudit');

        //product grid compatibility
        if (Mage::app()->getStore()->isAdmin()
            && is_a($object, 'Mage_CatalogInventory_Model_Stock_Item')
            && $this->_isAmpgrid()
        ) {
            $this->_saveLog();
            if (!Mage::registry('amaudit_log_duplicate_save')) {
                Mage::register('amaudit_log_duplicate_save', 1);
            }
        }

        if (!$this->_isForSave($observer)
            || !Mage::registry('amaudit_log_id')
        ) {
            return;
        }

        $elementId = $observer->getObject()->getEntityId();

        //product grid compatibility
        if (is_a($object, 'Mage_CatalogInventory_Model_Stock_Item')) {
            $elementId = $observer->getObject()->getProductId();
        }

        if (!$elementId) {
            $elementId = $observer->getObject()->getId();
        }

        if (is_array($elementId) && 'true' == (string)Mage::getConfig()->getNode('modules/Amasty_Perm/active')) {
            $elementId = $observer->getObject()->getUserId();
        }

        $name = "";

        if (strpos($class, 'Sales_Model_Order') !== false) {
            $object = $observer->getObject();
            if ($observer->getObject()->getOrderId()) {
                $name = "Order ID " . $object->getOrderId();
            } elseif ($object->getParentId() == $object->getEntityId()) {
                $name = "Order ID " . $object->getParentId();
            }
        }

        if (!$name) {
            $name = $observer->getObject()->getName();
            if (' ' === $name && $observer->getObject() instanceof Mage_Admin_Model_User) {
                $name = Mage::registry('deleted_user_name');
                Mage::unregister('deleted_user_name');
            }
        }
        if (!$name) {
            $name = $observer->getObject()->getTitle();
        }
        //Catalog->search terms
        if (!$name && $observer->getObject() instanceof Mage_CatalogSearch_Model_Query) {
            $name = $observer->getObject()->getQueryText();
        }
        //Attribute Set
        if (!$name && $observer->getObject() instanceof Mage_Eav_Model_Entity_Attribute_Set) {
            $name = $observer->getObject()->getAttributeSetName();
        }
        //Catalog Ratings
        if (!$name && $observer->getObject() instanceof Mage_Rating_Model_Rating) {
            $name = $observer->getObject()->getRatingCode();
        }
        //Customer Group
        if (!$name && $observer->getObject() instanceof Mage_Customer_Model_Group) {
            $name = $observer->getObject()->getCustomerGroupCode();
        }
        //product grid compatibility
        if (!$name && $observer->getObject() instanceof Mage_CatalogInventory_Model_Stock_Item) {
            $name = $observer->getObject()->getProductName();
        }
        if (!Mage::registry('amaudit_log_duplicate') || $name) {
            Mage::unregister('amaudit_log_duplicate');
            Mage::register('amaudit_log_duplicate', 1);
            try {
                $logModel = Mage::getModel('amaudit/log')->load(Mage::registry('amaudit_log_id'));
                $this->_logData = $logModel->getData();
                if ($logModel) {
                    if ($name && !$this->_isCustomer) $this->_logData['info'] = $name;
                    if ($elementId
                        && !$this->_isCustomer
                        && $class != 'Unirgy_Dropship_Model_Stock_Item'
                    ) {
                        $this->_logData['element_id'] = $elementId;
                    }
                    if ($observer->getObject()->hasDataChanges()) $this->_logData['type'] = "Edit";
                    if ($observer->getObject()->isObjectNew() || ($observer->getObject()->hasDataChanges() && !$observer->getObject()->getOrigData())) {
                        $this->_logData['type'] = "New";
                    }
                    if ($observer->getObject()->isDeleted() || $delete) {
                        $this->_logData['type'] = "Delete";
                    }
                    if ($logModel->getCategoryName() == "System Configuration") {
                        $this->_logData['type'] = "Edit";
                    }
                    if ($logModel->getCategory() == "amaudit/adminhtml_log") {
                        $this->_logData['type'] = "Restore";
                    }

                    //product grid compatibility
                    if ($this->_logData['category'] == 'ampgrid/adminhtml_field') {
                        $this->_logData['category'] = 'admin/catalog_product';
                        $this->_logData['category_name'] = $helper->__('Product');
                        $this->_logData['parametr_name'] = 'id';
                        $this->_isAmpgrid = true;
                    }
                    $logModel->setData($this->_logData);
                    if (!$this->_checkOrder($logModel, $object)) {
                        $logModel->save();
                    }
                    if ($observer->getObject() instanceof Mage_Customer_Model_Customer) {
                        $this->_isCustomer = true;
                    }
                }
            } catch (Exception $e) {
                Mage::logException($e);
                Mage::log($e->getMessage());
            }
        }

        //save details
        if (!empty($class)
            && !empty($elementId)
            && !is_array($elementId)
        ) {
            $entity = Mage::getModel($class)->load($elementId);
            $this->_saveIfNoDetails($entity);
            $logModel = Mage::getModel('amaudit/log')->load(Mage::registry('amaudit_log_id'));
            $logModelType = $logModel->getType() ? $logModel->getType() : 'New';
            $isNew = ($logModel && $logModelType == "New") ? true : false;
            if (is_a($object, 'Mage_Sales_Model_Order_Invoice_Item')) {
                $isNew = true;
            }
            $this->_isOrigData = false;
            if ($observer->getObject()->getOrigData()) {
                $this->_isOrigData = true;
                $dataBeforeSave = Mage::registry('amaudit_details_before');
                Mage::unregister('amaudit_details_before');
                $newData = $object->getData();
                $oldData = $object->getOrigData();

                $amPgridData = Mage::registry('amasty_grid_product_data');
                if (!empty($amPgridData)) {
                    $oldData = $amPgridData;
                    Mage::unregister('amasty_grid_product_data');
                }

                if (is_a($object, 'Mage_Catalog_Model_Product')) {
                    if (!($object->getDescription())) {
                        $newData = Mage::getModel('catalog/product')
                            ->setStoreId($object->getStoreId())
                            ->load($object->getEntityId())
                            ->getData();
                    }

                    $categoryIds = $entity->getCategoryIds();
                    $newData['categories'] = $categoryIds;
                    $oldData['categories'] = $dataBeforeSave['categories'];
                }

                if (is_a($object, 'Mage_Catalog_Model_Category')) {
                    $oldData['posted_products'] = implode(',', array_keys($newData['products_position']));
                    if (isset($newData['posted_products'])) {
                        $newData['posted_products'] = implode(',', array_keys($newData['posted_products']));
                    }
                }
                $this->_saveDetails($oldData, $newData, Mage::registry('amaudit_log_id'), $isNew, $object);

                if (is_a($object, 'Mage_Customer_Model_Customer')) {
                    foreach ($object->getAddresses() as $addressId => $address) {
                        $isAddressNew = false;

                        if (!$address->getOrigData()) {
                            $isAddressNew = true;
                        }
                        $oldAddressData = $address->getOrigData();
                        $newAddressData = $address->getData();

                        if (!$address->getData()) {
                            $detailsModel = Mage::getModel('amaudit/log_details');
                            $detailsModel->setData('log_id', Mage::registry('amaudit_log_id'));
                            $detailsModel->setData('model', get_class($address) . ' [' .$addressId . '] --DELETED--');
                            $detailsModel->save();
                        } else {
                            $this->_saveDetails(
                                $oldAddressData,
                                $newAddressData,
                                Mage::registry('amaudit_log_id'),
                                $isAddressNew,
                                $address,
                                $addressId
                            );
                        }
                    }
                }
            }
            if ($entity && !$this->_isOrigData) {
                $newMass = $entity->getData();
                if (array_key_exists('config_id', $newMass) && array_key_exists('path', $newMass) && array_key_exists('value', $newMass)) {
                    $newMass = array($newMass['path'] => $newMass['value']);
                }
                $mass = Mage::registry('amaudit_details_before');
                Mage::unregister('amaudit_details_before');
                $this->_saveDetails($mass, $newMass, Mage::registry('amaudit_log_id'), $isNew, $object);
            }
            //for order comment
            if ((is_a($object, 'Mage_Sales_Model_Order_Status_History')) && (!$observer->getObject()->getOrigData())) {
                $this->_saveDetails(array('comment' => ''), array('comment' => $observer->getObject()->getComment()), Mage::registry('amaudit_log_id'), $isNew, $object);
            }

            //for order comment when Sales_Model_Order_Item was first to log
            if (is_a($object, "Mage_Sales_Model_Order_Item")) {
                $order = $object->getOrder();
                $statusHistory = $order->getAllStatusHistory();
                $lastComment = array_pop($statusHistory);
                if ($lastComment->getOrigData() === null) {
                    $this->_saveDetails(
                        array('order comment' => ''),
                        array('order comment' => $lastComment->getComment()),
                        Mage::registry('amaudit_log_id'),
                        $isNew,
                        $object
                    );
                }
            }
        }

        Mage::unregister('amaudit_details_before');

        if (!is_a($object, 'Mage_Catalog_Model_Product')) {
            Mage::unregister('amaudit_log_id');
        }
    }

    public function beforeSaveRoles()
    {
        $roleId = Mage::app()->getRequest()->getParam('role_id');
        $rulesCollection = $rules_set = Mage::getResourceModel('admin/rules_collection')->getByRoles($roleId)->load();
        $this->_oldRules = $this->_rulesToOptionArray($rulesCollection);
    }

    public function afterSaveRoles()
    {
        $roleId = Mage::app()->getRequest()->getParam('role_id');
        $rulesCollection = $rules_set = Mage::getResourceModel('admin/rules_collection')->getByRoles($roleId)->load();
        $newRules = $this->_rulesToOptionArray($rulesCollection);
        $this->_saveDetails($this->_oldRules, $newRules, Mage::registry('amaudit_log_id'), false, 'admin_rule');
    }

    /**
     * Handles mass change of status in Manage Products
     * @param $observer
     */
    public function modelProductsSaveBefore($observer)
    {
        /** @var Amasty_Audit_Helper_Data $helper */
        $helper = Mage::helper('amaudit');
        $class = 'Mage_Catalog_Model_Product';
        $username = Mage::getSingleton('admin/session')->getUser() ? Mage::getSingleton('admin/session')->getUser()->getUsername() : '';
        $productIds = $observer->getProductIds();
        $observerData = $observer->getAttributesData();
        $isProductMassUpdate = $this->_isProductMassUpdate();

        foreach ($productIds as $productId) {
            if ($isProductMassUpdate) {
                $product = Mage::getModel($class)->load($productId);

                $logModel = Mage::getModel('amaudit/log')->load(Mage::registry('amaudit_log_id'));
                $this->_logData = $logModel->getData();
                $name = $product->getName();
                if ($logModel) {
                    if ($name) $this->_logData['info'] = $name;
                    $this->_logData['element_id'] = $productId;
                    $this->_logData['type'] = "Edit";
                    $this->_logData['category'] = "admin/catalog_product";
                    $this->_logData['category_name'] = $helper->__('Product');
                    $this->_logData['parametr_name'] = "Edit";
                    $this->_logData['store_id'] = $observer->getStoreId();
                    $this->_logData['username'] = $username;
                    $this->_logData['date_time'] = Mage::getModel('core/date')->gmtDate();
                    $logModel->setData($this->_logData);
                    $logModel->save();
                }

                $this->_saveDetails($product->getData(), $observerData, $logModel->getEntityId(), false, $product);
            }
        }

        return $this;
    }


    //listen model_save_before event
    public function modelSaveDeleteBefore($observer)
    {
        $object = $observer->getObject();

        $class = get_class($object);

        if (!$this->_isForSave($observer)
        ) {
            return;
        }

        if (!Mage::registry('amaudit_log_duplicate_save')) {
            $this->_saveLog();
            Mage::register('amaudit_log_duplicate_save', 1, true);
        }

        if (is_a($object, 'Mage_Tax_Model_Class')) {
            $origData = Mage::getModel('tax/class')->load($observer->getObject()->getClassId())->getData();
            if (is_array($origData) && !empty($origData)) {
                foreach ($origData as $key => $value) {
                    $observer->getObject()->setOrigData($key, $value);
                }
            }
        }

        if (is_a($object, 'Mage_Tax_Model_Calculation_Rule')) {
            $origData = Mage::getModel('tax/calculation_rule')->load($observer->getObject()->getTaxCalculationRuleId())->getData();
            if (is_array($origData) && !empty($origData)) {
                foreach ($origData as $key => $value) {
                    $observer->getObject()->setOrigData($key, $value);
                }
            }
        }
        $mass = Mage::registry('amaudit_details_before') ? Mage::registry('amaudit_details_before') : array();
        $id = $observer->getObject()->getId();
        $entity = Mage::getModel($class)->load($id);
        $this->_saveIfNoDetails($entity, $observer);
        if ($entity) {
            $massNew = $entity->getData();
            foreach ($massNew as $mas) {
                if (!(gettype($mas) == "string" || gettype($mas) == "boolean" || is_array($mas))) {
                    unset($mas);
                }
            }

            if (array_key_exists('config_id', $massNew) && array_key_exists('path', $massNew) && array_key_exists('value', $massNew)) {
                $mass[$massNew['path']] = $massNew['value'];
            } else {
                $mass += $massNew;
            }

            if (is_a($observer->getObject(), 'Mage_Catalog_Model_Product')) {
                $categoryIds = $entity->getCategoryIds();
                $mass['categories'] = $categoryIds;
                $this->_saveProductLinkData($observer);
                $this->_saveProductWebsites($observer);
            }

            Mage::register('amaudit_details_before', $mass, true);
        }

    }

    //run with cron
    public function deleteLogs()
    {
        Mage::getModel('amaudit/log')->clearLog();
        Mage::getModel('amaudit/visit')->deletePageVisitHistoryLog();
        Mage::getModel('amaudit/data')->deleteLoginAttemptsLog();
    }

    public function implode_r($glue, $arr)
    {
        $ret_str = "";
        foreach ($arr as $a) {
            $ret_str .= (is_array($a)) ? $this->implode_r($glue, $a) : "," . $a;
        }

        return $ret_str;
    }

    protected function _saveProductWebsites($observer)
    {
        $websiteIds = Mage::getModel('catalog/product')->load($observer->getObject()->getEntityId())->getWebsiteIds();
        Mage::register('amaudit_old_product_websites_ids', $websiteIds, true);
    }

    protected function _saveProductLinkData($observer)
    {
        $object = $observer->getObject();
        foreach ($this->_productLinkData as $linkData) {
            switch ($linkData) {
                case 'up_sell_link_data':
                    $data = $object->getUpSellProductIds();
                    break;
                case 'cross_sell_link_data':
                    $data = $object->getCrossSellProductIds();
                    break;
                case 'related_link_data':
                    $data = $object->getRelatedProductIds();
                    break;
                default:
                    $data = array();
            }
            Mage::register('amaudit_' . $linkData, $data, true);
        }
    }

    protected function _isForSave($observer)
    {
        $isForSave = true;

        $object = $observer->getObject();

        $class = get_class($object);

        if (!Mage::app()->getStore()->isAdmin() ||
            $class == "Amasty_Audit_Model_Log" ||
            $class == "Amasty_Audit_Model_Log_Details" ||
            $class == "Amasty_Audit_Model_Active" ||
            $class == "Amasty_Audit_Model_Visit_Detail" ||
            $class == "Amasty_Audit_Model_Visit" ||
            $class == "Amasty_Conf_Model_Product_Attribute" ||
            $class == "Amasty_Editlock_Model_Lock" ||
            is_a($object, 'Mage_Index_Model_Event') ||
            $class == 'Mirasvit_SearchIndex_Model_Index'||
            $class == 'AW_Customerattributes_Model_Value' ||
            $class == 'AW_Orderattributes_Model_Value' ||
            $class == 'BL_CustomGrid_Model_Grid' ||
            $class == 'Mage_SalesRule_Model_Coupon' ||
            $class == 'Mage_Catalog_Model_Product_Type_Configurable_Attribute' ||
            $class == 'Mage_Customer_Model_Address' ||
            $class == 'Ebizmarts_MailChimp_Model_Ecommercesyncdata'
        ) {
            $isForSave = false;
        }

        return $isForSave;
    }

    protected function _addBlock($block, $createdBlock, $lastElement)
    {
        if (method_exists($block, 'addTabAfter')) {
            $url = $this->_prepareUrl($createdBlock);
            $block->addTabAfter('tabid', array(
                'label' => Mage::helper('amaudit')->__('History of Changes'),
                'class' => 'ajax',
                'url' => Mage::helper("adminhtml")->getUrl($url, array('_current' => true)),
            ), $lastElement);
        } else {
            $block->addTab('tabid', array(
                'label' => Mage::helper('amaudit')->__('History of Changes'),
                'content' => $block->getLayout()
                    ->createBlock('amaudit/' . $createdBlock)->toHtml(),
            ));
        }

    }

    protected function _prepareUrl($cratedBlock)
    {
        $baseUrl = 'adminhtml/amaudit_log/';
        switch ($cratedBlock) {
            case 'adminhtml_tabs_customer':
                $url = $baseUrl . 'customer';
                break;
            case 'adminhtml_tabs_product':
                $url = $baseUrl . 'product';
                break;
            case 'adminhtml_tabs_order':
                $url = $baseUrl . 'order';
                break;
            default:
                $url = '';
        }

        return $url;
    }

    protected function _saveExport($arrPath, $observer)
    {
        /** @var Amasty_Audit_Helper_Data $helper */
        $helper = Mage::helper('amaudit');
        $logModel = Mage::getModel('amaudit/log');
        $logData['date_time'] = Mage::getModel('core/date')->gmtDate();
        $username = Mage::getSingleton('admin/session')->getUser() ? Mage::getSingleton('admin/session')->getUser()->getUsername() : '';
        $logData['username'] = $username;
        if (in_array('exportPost', $arrPath)) {
            $logData['type'] = $arrPath[2];
        }
        $logData['type'] = $arrPath[3];
        $category = $arrPath[2];
        $logData['category'] = $category;
        $logData['category_name'] = Mage::helper('amaudit')->getCatNameFromArray($category);
        $logData['parametr_name'] = 'back';
        $logData['element_id'] = 0;
        $logData['info'] = $helper->__('Data was exported');
        $logData['store_id'] = $observer->getStoreId();
        $logModel->setData($logData);
        $logModel->save();
    }

    protected function _deleteProduct($object)
    {
        /** @var Amasty_Audit_Helper_Data $helper */
        $helper = Mage::helper('amaudit');
        $logModel = Mage::getModel('amaudit/log')->load(Mage::registry('amaudit_log_id'));
        $username = Mage::getSingleton('admin/session')->getUser() ? Mage::getSingleton('admin/session')->getUser()->getUsername() : '';
        $this->_logData = $logModel->getData();
        $name = $object->getName();
        if ($logModel) {
            $logData['info'] = $name;
            $logData['element_id'] = $object->getEntityId();
            $logData['type'] = 'Delete';
            $ogData['category'] = "admin/catalog_product";
            $logData['category_name'] = $helper->__('Product');
            $logData['parametr_name'] = "delete";
            $logData['store_id'] = 0;
            $logData['username'] = $username;
            $logData['date_time'] = Mage::getModel('core/date')->gmtDate();
            $logModel->setData($logData);
            $logModel->save();
        }
    }

    protected function _rulesToOptionArray($rulesCollection)
    {
        $rulesOptionsArray = array();
        foreach ($rulesCollection as $rule) {
            $rulesOptionsArray['rule: ' . $rule->getResourceId()] = $rule->getPermission();
        }
        return $rulesOptionsArray;
    }

    protected function _isProductMassUpdate()
    {
        $isProductMassUpdate = false;
        $backtrace = debug_backtrace();
        foreach ($backtrace as $step) {
            if ($step['class'] == 'Mage_Catalog_Model_Product_Action' && $step['function'] == 'updateAttributes') {
                $isProductMassUpdate = true;
            }
        }

        return $isProductMassUpdate;
    }

    private function _saveLog()
    {
        if (!Mage::app()->getStore()->isAdmin()) {
            return false;
        }
        //save log start
        $username = Mage::getSingleton('admin/session')->getUser() ? Mage::getSingleton('admin/session')->getUser()->getUsername() : '';
        $path = Mage::registry('amaudit_log_path');
        $arrPath = ($path) ? explode("/", $path) : array();
        if (!array_key_exists(3, $arrPath)) {
            return false;
        }
        $logModel = Mage::getModel('amaudit/log');
        $this->_logData = array();
        $this->_logData['date_time'] = Mage::getModel('core/date')->gmtDate();
        $this->_logData['username'] = $username;
        if ("delete" == $arrPath[3]) {
            $this->_logData['type'] = "Delete";
        } else {
            $this->_logData['type'] = $arrPath[3];
        }
        $this->_logData['category'] = $arrPath[1] . '/' . $arrPath[2];
        $this->_logData['category_name'] = Mage::helper('amaudit')->getCatNameFromArray($this->_logData['category']);

        if (isset($arrPath[4])) {
            if ($arrPath[4] == 'store' && isset($arrPath[6])) {
                $arrPath[4] = $arrPath[6];
            }
            $paramName = $arrPath[4] == "key" ? "underfined" : $arrPath[4];
            if ($paramName == 'section') {
                $paramName .= '/' . $arrPath[5];
            }
            $this->_logData['parametr_name'] = $paramName;
        }

        $storeId = 0;
        if ($keyStore = array_search("store", $arrPath)) {
            $storeId = $arrPath[$keyStore + 1];
            if (!is_numeric($storeId)) {
                $storeId = Mage::getModel('core/store')->load($storeId, 'code')->getStoreId();
            }
        }
        $this->_logData['store_id'] = $storeId;

        if ($this->_logData['type'] != 'logout') {
            $logModel->setData($this->_logData);
            //for better work with orders
            if ($this->_logData['type'] != 'loadBlock') {
                $logModel->save();
            }
            Mage::register('amaudit_log_id', $logModel->getEntityId(), true);
            Mage::unregister('amaudit_details_before');
        } elseif ($this->_isFirstLogout) {
            $this->_isFirstLogout = false;
            $detailsModel = Mage::getModel('amaudit/data');
            $detailsModel->saveLogoutData($this->_logData);
        }
        //save log end
    }

    protected function _removeEmptyFields($array)
    {
        foreach ($array as $key => $value) {
            if (empty($value)) {
                unset($array[$key]);
            }
        }
        return $array;
    }

    /**
     * Save details for configurations that are processed only before or only after saving
     * @param $entity - entity with old value
     * @param null $observer - entity with new value
     */
    protected function _saveIfNoDetails($entity, $observer = NULL)
    {
        $configPaths = array(
            'dev/translate_inline/active',
            'dev/translate_inline/active_admin',
            'dev/log/active',
            'dev/log/file',
            'dev/log/exception_file',
        );

        if ($entity instanceof Mage_Core_Model_Config_Data) {
            $path = $entity->getPath();
            if (in_array($path, $configPaths)) {
                if (is_null($observer)) {
                    $newValue = Mage::getStoreConfig($path);
                } else {
                    $newValue = $observer->getObject()->getValue();
                }
                $massOld = array($entity->getPath() => $entity->getValue());
                $massNew = array($path => $newValue);
                $this->_saveDetails(
                    $massOld,
                    $massNew,
                    Mage::registry('amaudit_log_id'),
                    false,
                    'Mage_Core_Model_Config_Data'
                );

            }
        }
    }

    protected function _handleEmptyElements($massOld, $massNew)
    {
        $mandatoryValuesString = Mage::getStoreConfig('amaudit/log/mandatory_values');
        $mandatoryValuesString = str_replace("\r", "", $mandatoryValuesString);;

        $mandatoryValues = explode("\n", $mandatoryValuesString);
        foreach ($mandatoryValues as $value) {
            if (array_key_exists($value, $massNew) && !array_key_exists($value, $massOld)) {
                $massOld[$value] = '';
            }
        }

        return $massOld;
    }

    protected function _isAmpgrid()
    {
        if (is_null($this->_isAmpgrid)) {
            $this->_isAmpgrid = false;
            $backTrace = debug_backtrace();
            foreach ($backTrace as $step) {
                if (isset($step['class']) && ($step['class'] == 'Amasty_Pgrid_Adminhtml_Ampgrid_FieldController')
                    && ($step['function'] == '_updateProductData')) {
                    $this->_isAmpgrid = true;
                    break;
                }
            }
            $backTrace = NULL;
        }

        return $this->_isAmpgrid;
    }

    protected function _checkOrder($logModel, $object)
    {
        $isNoCompleteOrder = false;
        if ($object instanceof Mage_Sales_Model_Quote) {
            $protectCode = $object->getProtectCode();
            if (empty($protectCode)) {
                $isNoCompleteOrder = true;
            }
        } elseif ($object instanceof Mage_Sales_Model_Quote_Address
            || $object instanceof Mage_Sales_Model_Quote_Payment
        ) {
            $logId = $logModel->getEntityId();
            $logDetailsCollection = Mage::getModel('amaudit/log_details')->getCollection()
                ->addFilter('log_id', $logId);
            $logData = $logDetailsCollection->getData();
            if (empty($logData)) {
                $isNoCompleteOrder = true;
            }

        }

        return $isNoCompleteOrder;
    }

    private function _saveDetails($massOld, $massNew, $logId, $isNew = false, $object = null, $modelId = null)
    {
        $model = null;

        if (is_object($object)) {
            $model = get_class($object);
        }

        if ($modelId !== null) {
            $model = $model . ' [' . $modelId . ']';
        }
        $notSaveModels = array(
            'Mage_SalesRule_Model_Coupon',
            'Mage_Eav_Model_Entity_Store',
            'Mage_Catalog_Model_Product_Type_Configurable_Attribute'
        );
        if ($isNew) {
            $massOld = $this->_removeEmptyFields($massNew);
        }
        if (!in_array($model, $notSaveModels)) {
            try {
                $notRestore = array('entity_id', 'entity_type_id');
                if (is_array($massOld)) {

                    if (is_a($object, 'Mage_Catalog_Model_Product')) {
                        foreach ($this->_productLinkData as $linkData) {
                            if (isset($massNew[$linkData])) {
                                $oldIds = implode(',', Mage::registry('amaudit_' . $linkData));
                                $newIds = implode(',', array_keys($massNew[$linkData]));
                                $massNew[$linkData] = $newIds;
                                $massOld[$linkData] = $oldIds;
                            }
                        }

                        unset($massOld['store_id']);
                        $oldWebsiteIds = Mage::registry('amaudit_old_product_websites_ids');
                        if (is_array($oldWebsiteIds)) {
                            $massOld['website_ids'] = implode(',', $oldWebsiteIds);
                        }
                        if (isset ($massNew['website_ids'])) {
                            if (is_array($massNew['website_ids'])) {
                                $massNew['website_ids'] = implode(',', $massNew['website_ids']);
                            }
                        } elseif (isset($massOld['website_ids'])) {
                            $massNew['website_ids'] = $massOld['website_ids'];
                        } else {
                            $massNew['website_ids'] = 0;
                        }
                    }

                    if (($model == 'Mage_Admin_Model_User')) {
                        //for change the user's role
                        if (isset($massNew['roles']['0'])) {
                            $newRoleId = $massNew['roles']['0'];
                            $oldRoleIdPrepare = explode('=', $massNew['user_roles']);
                            $oldRoleId = $oldRoleIdPrepare[0];
                            $rolesModel = Mage::getModel('admin/role');
                            $oldRole = $rolesModel->load($oldRoleId)->getRoleName();
                            $newRole = $rolesModel->load($newRoleId)->getRoleName();
                            $massNew['roles'] = $newRole;
                            $massOld['roles'] = $oldRole;
                        }
                        //compatitility with Amasty_Perm
                        if ('true' == (string)Mage::getConfig()->getNode('modules/Amasty_Perm/active')) {
                            if (isset($massNew['selected_customers'])) {
                                $oldCustomerIds = Mage::getModel('amperm/mysql4_perm')->getCustomerIds($massNew['user_id']);
                                $massOld['selected_customers'] = implode('&', $oldCustomerIds);
                            }
                            if (isset($massNew['customer_group_id']) && is_array($massNew['customer_group_id'])) {
                                $massNew['customer_group_id'] = implode($massNew['customer_group_id'], ',');
                            } elseif (isset($massOld['customer_group_id'])) {
                                $massNew['customer_group_id'] = '';
                            }
                        }
                    }

                    $massOld = $this->_handleEmptyElements($massOld, $massNew);
                    if ($model == 'Mage_SalesRule_Model_Rule') {
                        // No data set in Conditions/Actions of Cart Price Rule
                        $massNew = Mage::getModel('salesrule/rule')->load($object->getId())->getData();
                    }
                    foreach ($massOld as $key => $value) {
                        if (in_array($key, $notRestore)) {
                            continue;
                        }
                        if ($key == 'image') {
                            if (isset($massNew[$key]) && is_array($massNew[$key])) {
                                $massNew[$key] = array_shift($massNew[$key]);
                            }
                        }
                        if (array_key_exists($key, $massNew) && $key != 'updated_at' && $key != 'created_at' && $key != 'category_name') {
                            if (($key == 'from_date' || $key == 'to_date')
                                && $value !== null && $massNew[$key] !== null) {
                                $massNew[$key] = date('Y-m-d', strtotime($massNew[$key]));
                            }
                            if (($value != $massNew[$key] && !(!$value && !$massNew[$key])) || $isNew) {
                                if (is_array($value) && is_array($massNew[$key]) && ($key == 'website_ids' || $key == 'store_ids')) {
                                    $value = implode(',', $value);
                                    $massNew[$key] = implode(',', $massNew[$key]);
                                }
                                $detailsModel = Mage::getModel('amaudit/log_details');
                                if ($model != 'Mage_Sales_Model_Order_Item' && $detailsModel->isInCollection($logId, $key, $model)) {
                                    continue;
                                }
                                $detailsModel = $this->_setDetailsData($isNew, $detailsModel, $value, $massNew, $key, $logId, $model);
                                if (!is_array($key) && ($key !== "media_gallery")) $detailsModel->save();
                            } else if (is_array($value) && is_array($massNew[$key])) {
                                if ($key == 'media_gallery') {
                                    foreach ($value['images'] as $image) {
                                        $value[] = $image['file'];
                                    }
                                    foreach ($massNew[$key]['images'] as $newImage) {
                                        $massNew[$key][] = $newImage['file'];
                                    }
                                    unset($value['images']);
                                    unset($value['values']);
                                    unset($massNew[$key]['images']);
                                    unset($massNew[$key]['values']);
                                }
                                $old = $this->implode_r(',', $value);
                                $new = $this->implode_r(',', $massNew[$key]);
                                if ($old != $new || $isNew) {
                                    $detailsModel = Mage::getModel('amaudit/log_details');
                                    $detailsModel = $this->_setDetailsData($isNew, $detailsModel, $value, $massNew, $key, $logId, $model);
                                    $detailsModel->save();
                                }
                            }
                        }
                    }
                }
            } catch (Exception $e) {
                Mage::log($e->getMessage());
                Mage::logException($e);
            }
        }
    }

    private function _setDetailsData($isNew, $detailsModel, $value, $massNew, $key, $logId, $model)
    {
        if ($key == 'categories') {
            $value = $this->_prepareProductCategories($value);
            $massNew['categories'] = $this->_prepareProductCategories($massNew['categories']);
        }
        if (is_array($value)) {
            $value = 'is_array';
        }
        if (is_array($massNew[$key])) {
            $massNew[$key] = 'is_array';
        }
        if (is_array($key)) {
            $key = 'is_array';
        }
        if (is_array($logId)) {
            $logId = 'is_array';
        }
        if (is_array($model)) {
            $model = 'is_array';
        }
        if (is_object($massNew[$key])) {
            $massNew[$key] = get_class($massNew[$key]);
        }
        if($key == 'price') {
            $value = $this->_convertPrice($value);
            $massNew[$key] = $this->_convertPrice($massNew[$key]);
        }

        if (!$isNew) $detailsModel->setData('old_value', $value);
        $detailsModel->setData('new_value', $massNew[$key]);
        $detailsModel->setData('name', $key);
        $detailsModel->setData('log_id', $logId);
        $detailsModel->setData('model', $model);
        return $detailsModel;
    }

    /**
     * @param $price
     * @return string
     */
    protected function _convertPrice($price)
    {
        return number_format($price, 2, '.', '');
    }

    private function _prepareProductCategories($categoryIds)
    {
        $categoryNames = array();

        if (is_array($categoryIds)) {
            foreach ($categoryIds as $id) {
                $categoryNames[] = Mage::getModel('catalog/category')->setStoreId(Mage::app()->getStore()->getId())->load($id)->getName();
            }

            if (is_array($categoryNames)) {
                $categoryNames = implode(', ', $categoryNames);
            }
        }

        return $categoryNames;
    }

    private function _saveCompilation($path, $username)
    {
        /** @var Amasty_Audit_Helper_Data $helper */
        $helper = Mage::helper('amaudit');
        if (strpos($path, "compiler/process") !== false
            || strpos($path, "compiler_process") !== false
        ) {
            $arrPath = explode("/", $path);
            $keyStore = array_search("process", $arrPath);
            if (!$keyStore) {
                $keyStore = array_search("compiler_process", $arrPath);
            }
        }

        if (isset($arrPath) && isset($keyStore)) {
            $type = $arrPath[$keyStore + 1];
            if ($type != "index") {
                try {
                    $logModel = Mage::getModel('amaudit/log');
                    $this->_logData = array();
                    $this->_logData['date_time'] = Mage::getModel('core/date')->gmtDate();
                    $this->_logData['username'] = $username;
                    $this->_logData['type'] = ucfirst($type);
                    $this->_logData['category'] = "compiler/process";
                    $this->_logData['category_name'] = $helper->__('Compilation');
                    $this->_logData['parametr_name'] = 'index';
                    $this->_logData['info'] = $helper->__('Compilation');
                    $storeId = 0;
                    if ($keyStore = array_search("store", $arrPath)) {
                        $storeId = $arrPath[$keyStore + 1];
                    }
                    $this->_logData['store_id'] = $storeId;
                    $logModel->setData($this->_logData);
                    $logModel->save();
                } catch (Exception $e) {
                    Mage::logException($e);
                    Mage::log($e->getMessage());
                }
            }
        }
    }

    private function _saveCache($path, $username)
    {
        /** @var Amasty_Audit_Helper_Data $helper */
        $helper = Mage::helper('amaudit');
        $params = Mage::app()->getRequest()->getParams();
        $adminPath = Mage::registry('amaudit_admin_path') ? Mage::registry('amaudit_admin_path') : 'admin';
        if (strpos($path, $adminPath . "/cache") !== false) {
            $arrPath = explode("/", $path);
            if ($keyStore = array_search("cache", $arrPath)) {
                $type = $arrPath[$keyStore + 1];
                if ($type != "index") {
                    try {
                        $logModel = Mage::getModel('amaudit/log');
                        $this->_logData = array();
                        $this->_logData['date_time'] = Mage::getModel('core/date')->gmtDate();
                        $this->_logData['username'] = $username;
                        $this->_logData['type'] = ucfirst($type);
                        $this->_logData['category'] = "admin/cache";
                        $this->_logData['category_name'] = $helper->__('Cache');
                        $this->_logData['parametr_name'] = 'index';
                        $this->_logData['info'] = $helper->__('Cache');
                        $storeId = 0;
                        if ($keyStore = array_search("store", $arrPath)) {
                            $storeId = $arrPath[$keyStore + 1];
                        }
                        $this->_logData['store_id'] = $storeId;

                        $logModel->setData($this->_logData);
                        $logModel->save();
                        if (array_key_exists('types', $params)) {
                            $params = Mage::helper('amaudit')->getCacheParams($params['types']);
                            $this->_saveDetails($params, array(), $logModel->getEntityId(), true);
                        }
                    } catch (Exception $e) {
                        Mage::logException($e);
                        Mage::log($e->getMessage());
                    }
                }
            }
        }
    }

    private function _saveIndex($path, $username)
    {
        /** @var Amasty_Audit_Helper_Data $helper */
        $helper = Mage::helper('amaudit');
        $params = Mage::app()->getRequest()->getParams();
        $adminPath = Mage::registry('amaudit_admin_path') ? Mage::registry('amaudit_admin_path') : 'admin';
        if (strpos($path, $adminPath . "/process") !== false) {
            $arrPath = explode("/", $path);
            if ($keyStore = array_search("process", $arrPath)) {   //settings log or not user
                $type = $arrPath[$keyStore + 1];
                if ($type != "list") {
                    try {
                        $logModel = Mage::getModel('amaudit/log');
                        $this->_logData = array();
                        $this->_logData['date_time'] = Mage::getModel('core/date')->gmtDate();
                        $this->_logData['username'] = $username;
                        $this->_logData['type'] = ucfirst($type);
                        $this->_logData['category'] = "admin/process";
                        $this->_logData['category_name'] = $helper->__('Index Management');
                        $this->_logData['parametr_name'] = 'list';
                        $this->_logData['info'] = $helper->__('Index Management');
                        $storeId = 0;
                        if ($keyStore = array_search("store", $arrPath)) {
                            $storeId = $arrPath[$keyStore + 1];
                        }
                        $this->_logData['store_id'] = $storeId;

                        $logModel->setData($this->_logData);
                        $logModel->save();
                        if (array_key_exists('process', $params)) {
                            $params = Mage::helper('amaudit')->getIndexParams($params['process']);
                            $this->_saveDetails($params, array(), $logModel->getEntityId(), true);
                        }
                    } catch (Exception $e) {
                        Mage::logException($e);
                        Mage::log($e->getMessage());
                    }
                }
            }
        }
    }

    /**
     * Listening the admin_user_delete_before event to save name of deleted user
     *
     * @param $observer
     */
    public function beforeDeleteUser($observer)
    {
        if ($observer->getObject() instanceof Mage_Admin_Model_User) {
            $user = Mage::getModel('admin/user')->load($observer->getObject()->getUserId());
            Mage::register('deleted_user_name', $user->getName(), true);
        }
    }
}

/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_Audit
 */
class Amasty_Audit_Helper_Data extends Mage_Core_Helper_Url
{
    public function getCatNameFromArray($name)
    {
        $adminPath = Mage::registry('amaudit_admin_path') ? Mage::registry('amaudit_admin_path') : 'admin';
        $nameArray = array(
            'amorderattr/adminhtml_order' => $this->__('Amasty Order Attribute'),
            'ampgrid/adminhtml_field' => $this->__('Amasty Product Grid'),
            $adminPath . '/sales_order' => $this->__('Order'),
            $adminPath . '/sales_order_edit ' => $this->__('Order'),
            $adminPath . '/catalog_product' => $this->__('Product'),
            $adminPath . '/catalog_product_attribute' => $this->__('Product Attribute'),
            $adminPath . '/catalog_product_set' => $this->__('Product Attribute Set'),
            $adminPath . '/tax_rule' => $this->__('Tax Rules'),
            $adminPath . '/tag' => $this->__('Tags'),
            $adminPath . '/rating' => $this->__('Rating'),
            $adminPath . '/customer_group' => $this->__('Customer Groups'),
            $adminPath . '/promo_catalog' => $this->__('Catalog Price Rules'),
            $adminPath . '/promo_quote' => $this->__('Shopping Cart Price Rules'),
            $adminPath . '/newsletter_template' => $this->__('Newsletter Templates'),
            $adminPath . '/cms_page' => $this->__('CMS Manage Pages'),
            $adminPath . '/cms_block' => $this->__('CMS Static Blocks'),
            $adminPath . '/widget_instance' => $this->__('CMS Widget Instances'),
            $adminPath . '/poll' => $this->__('CMS Poll'),
            $adminPath . '/system_config' => $this->__('System Configuration'),
            $adminPath . '/permissions_user' => $this->__('User'),
            $adminPath . '/permissions_role' => $this->__('Role'),
            $adminPath . '/system_design' => $this->__('System Design'),
            $adminPath . '/api_user' => $this->__('System Web Services Users'),
            $adminPath . '/api_role' => $this->__('System Web Services Roles'),
            $adminPath . '/system_email_template' => $this->__('System Transactional Emails'),
            $adminPath . '/system_variable' => $this->__('System Custom Variable'),
            $adminPath . '/catalog_category' => $this->__('Categories'),
            $adminPath . '/sales_order_shipment' => $this->__('Shipment'),
            $adminPath . '/sales_order_invoice' => $this->__('Invoice'),
            $adminPath . '/sales_order_creditmemo' => $this->__('Creditmemo'),
            $adminPath . '/urlrewrite' => $this->__('URL Rewrite Management'),
            $adminPath . '/customer' => $this->__('Customer'),
            $adminPath . '/sales_order_create' => $this->__('Order'),
            $adminPath . '/tax_class' => $this->__('Tax Class'),
            $adminPath . '/tax_rate' => $this->__('Tax Rate'),
            $adminPath . '/checkout_agreement' => $this->__('Terms and Conditions'),
            $adminPath . '/notification' => $this->__('Notification'),
            $adminPath . '/catalog_search' => $this->__('Search Term'),
            $adminPath . '/ampaction' => $this->__('Mass Product Action'),
            'adminhtml_log' => $this->__('Actions Log'),
            'sales_creditmemo' => $this->__('Creditmemo'),
            'sales_shipment' => $this->__('Shipment'),
            'sales_invoice' => $this->__('Invoice'),
            'sales_order' => $this->__('Order'),
            'adminhtml_login' => $this->__('Login Attempts'),
            'tax_rate' => $this->__('Export Tax Rates'),
        );

        if (array_key_exists($name, $nameArray)) {
            $name = $nameArray[$name];
        } else {
            $name = ucfirst($name);
        }

        return $name;
    }

    public function getLockUser($idUser)
    {
        $lockUser = Mage::getModel('amaudit/lock')->load($idUser, 'user_id');
        if ($lockUser->hasData()) {
            return $lockUser;
        }
        return null;
    }

    public function isUserInLog($userId)
    {
        if (!Mage::getStoreConfig('amaudit/log/is_all_admins')) {
            $massId = Mage::getStoreConfig('amaudit/log/log_users');
            $massId = explode(',', $massId);
            if (in_array($userId, $massId)) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }

    }

    public function getCacheParams($params)
    {
        $option = array();
        $cacheTypes = Mage::app()->getCacheInstance()->getTypes();
        foreach ($params as $key => $value) {
            if (array_key_exists($value, $cacheTypes)) {
                $option[$cacheTypes[$value]->getData('cache_type')] = $cacheTypes[$value]->getData('description');
            }
        }
        return $option;
    }

    public function getIndexParams($params)
    {
        $option = array();
        $collection = Mage::getResourceModel('index/process_collection');
        if (!is_array($params)) {
            $params = array($params);
        }
        foreach ($collection as $item) {
            if (in_array($item->getProcessId(), $params)) {
                $option[$item->getIndexer()->getName()] = $item->getIndexer()->getDescription();
            }
        }
        return $option;
    }

    public function getUsername($userId)
    {
        $model = Mage::getModel('admin/user');
        $model->load($userId);
        return $model->getUsername();
    }

    public function allowClear()
    {
        return Mage::getSingleton('admin/session')->isAllowed('admin/system/amauditmenu/clearlog');
    }
}

/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_Audit
 */
class Amasty_Audit_Model_Active extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        $this->_init('amaudit/active', 'entity_id');
    }

    public function saveActive($data)
    {
        try
        {
            $activeData = array(
                'session_id' => session_id(),
                'recent_activity' => $data['date_time'],
            );

            $allData = array_merge($data, $activeData);

            $this->setData($allData);
            $this->save();
        }
        catch (Exception $e)
        {
            Mage::logException($e);
            Mage::log($e->getMessage());
        }
    }

    public function onAdminLogout()
    {
        $sessionId = Mage::getModel('core/cookie')->get('adminhtml');
        $this->removeOnlineAdmin($sessionId);
    }

    /**
     * get session id
     * get collection active admin
     * add this session id in filter (neq)
     * delete collection
     * add status auto logout
     * @param Amasty_Audit_Model_Active $otherAdmins
     */
    public function onAdminLogoutNotUnique($otherAdmins)
    {
        $sessionId = session_id();
        $otherAdminsNeq = $otherAdmins->addFieldToFilter('session_id', array('neq' => $sessionId));
        $adminsData = Mage::getModel('amaudit/data');

        foreach ($otherAdminsNeq as $admin) {
            $adminSessionId = $admin->getSessionId();
            $admin->setEntityId(null);
            $data = $admin->getData();
            $adminsData->saveAutoLogoutData($data);
            $this->removeOnlineAdmin($adminSessionId);
        }
    }

    public function removeOnlineAdmin($sessionId)
    {
        $activeEntity = $this->getActiveEntity($sessionId);
        $activeEntity->delete();
        Mage::getModel('amaudit/visit')->endVisit($sessionId);
    }

    public function saveSomeEvent()
    {
        if (Mage::app()->getStore()->isAdmin()) {
            $sessionId = Mage::getModel('core/cookie')->get('adminhtml');
            $this->updateOnlineAdminActivity($sessionId);
        }
    }

    public function updateOnlineAdminActivity($sessionId)
    {
        $activeEntity = $this->getActiveEntity($sessionId);
        $activeEntityData = $activeEntity->getData();
        if (!empty($activeEntityData)) {
            $activeEntity->setData('recent_activity', time());
            $activeEntity->save();
        }
    }

    public function getActiveEntity($sessionId)
    {
        $activeModel = Mage::getModel('amaudit/active')->getCollection()
            ->addFieldToFilter('session_id', $sessionId);
        $activeEntity = $activeModel->getFirstItem();
        return $activeEntity;
    }

    public function checkOnline()
    {
        $collection = $this->getCollection();
        $sessionLifeTime = Mage::getStoreConfig('admin/security/session_cookie_lifetime');
        if (empty($sessionLifeTime)) {
            $sessionLifeTime = 3600;
        }
        $currentTime = time();
        foreach ($collection as $admin) {
            $rowTime = strtotime($admin->getRecentActivity());
            $timeDifference = $currentTime - $rowTime;
            if ($timeDifference >= $sessionLifeTime) {
                $sessionId = $admin->getSessionId();
                $this->removeOnlineAdmin($sessionId);
            }
        }
    }

    public function destroySession($sessionId)
    {
        if (session_id() !== '') {
            session_id($sessionId);
            session_destroy();
        }
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Core
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Abstract resource model class
 *
 * @category    Mage
 * @package     Mage_Core
 * @author      Magento Core Team <core@magentocommerce.com>
 */
abstract class Mage_Core_Model_Mysql4_Abstract extends Mage_Core_Model_Resource_Db_Abstract
{
}
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_Audit
 */
class Amasty_Audit_Model_Mysql4_Active extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        $this->_init('amaudit/active', 'entity_id');
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Core
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Abstract Core Resource Collection
 *
 * @category    Mage
 * @package     Mage_Core
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Core_Model_Mysql4_Collection_Abstract extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
}
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_Audit
 */
class Amasty_Audit_Model_Mysql4_Active_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        $this->_init('amaudit/active');
    }
}
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Base
 */


class Amasty_Base_Model_Feed extends Mage_AdminNotification_Model_Feed
{
    const XML_FREQUENCY_PATH = 'ambase/feed/check_frequency';
    const XML_LAST_UPDATE_PATH = 'ambase/feed/last_update';

    const URL_NEWS = 'http://amasty.com/feed-news.xml';

    public function check()
    {
        $this->checkUpdate();
    }

    protected function _isPromoSubscribed()
    {
        return Mage::helper("ambase/promo")->isSubscribed();
    }

    public function checkUpdate()
    {

        if (($this->getFrequency() + $this->getLastUpdate()) > time()) {
            return $this;
        }

        $this->setLastUpdate();

        if (!extension_loaded('curl')) {
            return $this;
        }

        if (!Mage::helper('ambase')->isModuleActive('Mage_AdminNotification')) {
            return $this;
        }

        if ($this->_isPromoSubscribed()) {
            // load all new and relevant updates into inbox
            $feedData = array();
            $feedXml = $this->getFeedData();
            $wasInstalled = gmdate('Y-m-d H:i:s', Amasty_Base_Helper_Module::baseModuleInstalled());

            if ($feedXml && $feedXml->channel && $feedXml->channel->item) {
                foreach ($feedXml->channel->item as $item) {

                    $date = $this->getDate((string)$item->pubDate);

                    // compare strings, but they are well-formmatted 
                    if ($date < $wasInstalled) {
                        continue;
                    }

                    $feedData[] = array(
                        'severity'    => 3,
                        'date_added'  => $this->getDate($date),
                        'title'       => (string)$item->title,
                        'description' => (string)$item->description,
                        'url'         => (string)$item->link,
                    );
                }

                if ($feedData) {
                    $inbox = Mage::getModel('adminnotification/inbox');

                    if ($inbox) {
                        $inbox->parse($feedData);
                    }
                }
            }
        }

        //load all available extensions in the cache
        Amasty_Base_Helper_Module::reload();

        return $this;
    }

    public function getFrequency()
    {
        return Mage::getStoreConfig(self::XML_FREQUENCY_PATH);
    }

    public function getLastUpdate()
    {
        return Mage::app()->loadCache('ambase_notifications_lastcheck');
    }

    public function setLastUpdate()
    {
        Mage::app()->saveCache(time(), 'ambase_notifications_lastcheck');
        return $this;
    }

    public function getFeedUrl()
    {
        if (is_null($this->_feedUrl)) {
            $this->_feedUrl = self::URL_NEWS;
        }
        $query = '?s=' . urlencode(Mage::getStoreConfig('web/unsecure/base_url'));
        return $this->_feedUrl . $query;
    }

    protected function isExtensionInstalled($code)
    {
        $modules = array_keys((array)Mage::getConfig()->getNode('modules')->children());
        foreach ($modules as $moduleName) {
            if ($moduleName == $code) {
                return true;
            }
        }

        return false;
    }
}
/**
 * MageWorx
 * All Extension
 *
 * @category   MageWorx
 * @package    MageWorx_All
 * @copyright  Copyright (c) 2017 MageWorx (http://www.mageworx.com/)
 */

class MageWorx_All_Model_Observer
{
    /**
     * Remove not permitted groups from System Configuration Section
     *
     * @param  Varien_Event_Observer $observer
     * @return MageWorx_All_Model_Observer
     */
    public function restrictGroupsAcl($observer)
    {
        $editBlock = $observer->getEvent()->getBlock();

        if (!($editBlock instanceof Mage_Adminhtml_Block_System_Config_Edit)) {
            return $this;
        }

        $sectionCode = Mage::app()->getRequest()->getParam('section');
        if (false === strpos($sectionCode, 'mageworx')) {
            return $this;
        }

        $session = Mage::getSingleton('admin/session');
        $currentSection = Mage::getSingleton('adminhtml/config')->getSections()->$sectionCode;
        $groups = $currentSection->groups[0];
        foreach ($groups as $group => $object) {
            if (!$session->isAllowed("system/config/$sectionCode/$group")) {
                $currentSection->groups->$group = null;
            }
        }

        return $this;
    }

    /**
     *
     * @param  Varien_Event_Observer $observer
     * @return MageWorx_All_Model_Observer
     */
    public function showDeprecatedCodeNotice($observer)
    {
        if (!Mage::getSingleton('admin/session')->isLoggedIn()) {
            return;
        }

        if ($this->_isAjax()) {
            return;
        }

        $moduleListHelper    = Mage::helper('mageworx_all/moduleList');
        $compatibilityHelper = Mage::helper('mageworx_all/compatibility');

        foreach (array_keys($moduleListHelper->getModuleList()) as $moduleName) {
            list($vendorName, $moduleName) = explode('_', $moduleName, 2);
            if (!$vendorName || !$moduleName) {
                continue;
            }

            if (!$compatibilityHelper->isDuplicateCodePool($moduleName, $vendorName)) {
                continue;
            }

            $title        = $vendorName . ' ' . $moduleName . ' - duplicate code detected.';
            $notification = Mage::getModel('adminnotification/inbox');
            if (is_object($notification)) {
                $issetMessage = $notification->getCollection()->addFieldToFilter('title', array('in' => array($title)))->count();
                if ($issetMessage) {
                    continue;
                }

                $url = "http://support.mageworx.com/extensions/general_questions/extensions_moved_to_community.html";

                $description = $compatibilityHelper->__(
                    'We have detected an old version of the extension "%s" installed in "%s".',
                    $vendorName . ' ' . $moduleName,
                    $compatibilityHelper->getExtensionDirInLocalScope($moduleName, $vendorName)
                );

                $description .= ' ' . $compatibilityHelper->__(
                    'This directory should be deleted to ensure that the the latest extension version works correctly.'
                );

                if (method_exists($notification, 'addMajor')) {
                    $notification->addMajor($title, $description, $url);
                }
            }
        }

        return $observer;
    }

    /**
     *
     * @return boolean
     */
    protected function _isAjax()
    {
        $request = Mage::app()->getRequest();
        if (!is_object($request)) {
            return false;
        }

        if ($request->isXmlHttpRequest()) {
            return true;
        }

        if ($request->getParam('ajax') || $request->getParam('isAjax')) {
            return true;
        }

        return false;
    }
}
/**
 * MageWorx
 * All Extension
 *
 * @category   MageWorx
 * @package    MageWorx_All
 * @copyright  Copyright (c) 2017 MageWorx (http://www.mageworx.com/)
 */

class MageWorx_All_Helper_ModuleList
{
    public function getModuleList()
    {
        $modules = array_keys((array)Mage::getConfig()->getNode('modules')->children());
        sort($modules);

        $mageworxModules = array();
        foreach ($modules as $moduleName) {
            $name = explode('_', $moduleName, 2);
            if (!isset($name) || !in_array($name[0], $this->_getVendors())) {
                continue;
            }

            $moduleData = $this->_prepareData($moduleName, Mage::getConfig()->getNode('modules/' . $moduleName)->asArray());
            $mageworxModules[$moduleName] = $moduleData;
        }

        return $mageworxModules;
    }

    public function getStructuredModuleList()
    {
        return $this->_getStructuredModules($this->getModuleList());
    }

    /**
     * Retrieve structured array with extension dependencies
     *
     * @param array $mageworxModules
     * @return array
     */
    protected function _getStructuredModules($mageworxModules)
    {
        $result = array();
        $noPersonalUseExtensions = array();

        foreach ($mageworxModules as $moduleName => $moduleData) {
            if (!empty($moduleData['absorbers']) && is_array($moduleData['absorbers']) && array_intersect_key($moduleData['absorbers'], $mageworxModules)) {
                continue;
            }

            if (empty($moduleData['includes']) || !is_array($moduleData['includes'])) {
                $result[$moduleName] = $moduleData;
                continue;
            }

            if ($moduleData['active'] == 'false') {
                continue;
            }

            foreach ($moduleData['includes'] as $childModuleName => $moduleStatus) {
                if (!empty($mageworxModules[$childModuleName])) {
                    $moduleData['includes'][$childModuleName] = $mageworxModules[$childModuleName];

                    if (!in_array($childModuleName, $this->_getCommonExtensions())) {
                        $noPersonalUseExtensions[] = $childModuleName;
                    }
                } else {
                    unset($moduleData['includes'][$childModuleName]);
                }
            }

            $result = array_merge(array($moduleName => $moduleData), $result);
        }

        return array_diff_key($result, array_flip(array_unique($noPersonalUseExtensions)));
    }

    /**
     * @return array
     */
    protected function _getCommonExtensions()
    {
        return array('MageWorx_All');
    }

    /**
     *
     * @return array
     */
    protected function _getVendors()
    {
        return array('MageWorx');
    }

    /**
     *
     * @param string $moduleName
     * @param array $data
     * @return array
     */
    protected function _prepareData($moduleName, array $data)
    {
        if (empty($data['extension_name'])) {
            if (!empty($data['name'])) {
                $data['extension_name'] = $data['name'];
            } else {
                $data['extension_name'] = $moduleName;
            }
        }

        return $data;
    }

}
/**
 * MageWorx
 * MageWorx All Extension
 *
 * @category   MageWorx
 * @package    MageWorx_All
 * @copyright  Copyright (c) 2017 MageWorx (http://www.mageworx.com/)
 */

class MageWorx_All_Helper_Compatibility extends Mage_Core_Helper_Abstract
{
    /**
     *
     * @param string $extensionDir
     * @param string $vendorDir
     * @return boolean
     */
    public function isDuplicateCodePool($extensionDir, $vendorDir = 'MageWorx')
    {
        if (!Mage::getStoreConfigFlag('mageworx/suppress_local_pool_notice/' . $vendorDir . '_' . $extensionDir)
            && is_file($this->getExtensionDirInLocalScope($extensionDir, $vendorDir))
            && is_file($this->getExtensionDirInCommunityScope($extensionDir, $vendorDir))
        ) {
            return true;
        }

        return false;
    }

    /**
     *
     * @param string $extensionDir
     * @param string $vendorDir
     * @return string
     */
    public function getExtensionDirInLocalScope($extensionDir, $vendorDir = 'MageWorx')
    {
        return $this->_getExtensionDirInScope($extensionDir, $vendorDir, 'local');
    }

    /**
     *
     * @param string $extensionDir
     * @param string $vendorDir
     * @return string
     */
    public function getExtensionDirInCommunityScope($extensionDir, $vendorDir = 'MageWorx')
    {
        return $this->_getExtensionDirInScope($extensionDir, $vendorDir, 'community');
    }

    /**
     *
     * @param string $extensionDir
     * @param string $vendorDir
     * @param string $scope
     * @return string
     */
    protected function _getExtensionDirInScope($extensionDir, $vendorDir, $scope)
    {
        $path = array(
            Mage::getBaseDir('code'),
            $scope,
            $vendorDir,
            $extensionDir,
            'etc',
            'config.xml'
        );

        return implode(DS, $path);
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Core
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Core Observer model
 *
 * @category   Mage
 * @package    Mage_Core
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Mage_Core_Model_Observer
{
    /**
     * Check if synchronize process is finished and generate notification message
     *
     * @param  Varien_Event_Observer $observer
     * @return Mage_Core_Model_Observer
     */
    public function addSynchronizeNotification(Varien_Event_Observer $observer)
    {
        $adminSession = Mage::getSingleton('admin/session');
        if (!$adminSession->hasSyncProcessStopWatch()) {
            $flag = Mage::getSingleton('core/file_storage')->getSyncFlag();
            $state = $flag->getState();
            if ($state == Mage_Core_Model_File_Storage_Flag::STATE_RUNNING) {
                $syncProcessStopWatch = true;
            } else {
                $syncProcessStopWatch = false;
            }

            $adminSession->setSyncProcessStopWatch($syncProcessStopWatch);
        }
        $adminSession->setSyncProcessStopWatch(false);

        if (!$adminSession->getSyncProcessStopWatch()) {
            if (!isset($flag)) {
                $flag = Mage::getSingleton('core/file_storage')->getSyncFlag();
            }

            $state = $flag->getState();
            if ($state == Mage_Core_Model_File_Storage_Flag::STATE_FINISHED) {
                $flagData = $flag->getFlagData();
                if (isset($flagData['has_errors']) && $flagData['has_errors']) {
                    $severity       = Mage_AdminNotification_Model_Inbox::SEVERITY_MAJOR;
                    $title          = Mage::helper('adminhtml')->__('An error has occured while syncronizing media storages.');
                    $description    = Mage::helper('adminhtml')->__('One or more media files failed to be synchronized during the media storages syncronization process. Refer to the log file for details.');
                } else {
                    $severity       = Mage_AdminNotification_Model_Inbox::SEVERITY_NOTICE;
                    $title          = Mage::helper('adminhtml')->__('Media storages synchronization has completed!');
                    $description    = Mage::helper('adminhtml')->__('Synchronization of media storages has been successfully completed.');
                }

                $date = date('Y-m-d H:i:s');
                Mage::getModel('adminnotification/inbox')->parse(array(
                    array(
                        'severity'      => $severity,
                        'date_added'    => $date,
                        'title'         => $title,
                        'description'   => $description,
                        'url'           => '',
                        'internal'      => true
                    )
                ));

                $flag->setState(Mage_Core_Model_File_Storage_Flag::STATE_NOTIFIED)->save();
            }

            $adminSession->setSyncProcessStopWatch(false);
        }

        return $this;
    }

    /**
     * Cron job method to clean old cache resources
     *
     * @param Mage_Cron_Model_Schedule $schedule
     */
    public function cleanCache(Mage_Cron_Model_Schedule $schedule)
    {
        Mage::app()->getCache()->clean(Zend_Cache::CLEANING_MODE_OLD);
        Mage::dispatchEvent('core_clean_cache');
    }


    /**
     * Cleans cache by tags
     *
     * @param Varien_Event_Observer $observer
     * @return Mage_Core_Model_Observer
     */
    public function cleanCacheByTags(Varien_Event_Observer $observer)
    {
        /** @var $tags array */
        $tags = $observer->getEvent()->getTags();
        if (empty($tags)) {
            Mage::app()->cleanCache();
            return $this;
        }

        Mage::app()->cleanCache($tags);
        return $this;
    }

    /**
     * Checks method availability for processing in variable
     *
     * @param Varien_Event_Observer $observer
     * @throws Exception
     * @return Mage_Core_Model_Observer
     */
    public function secureVarProcessing(Varien_Event_Observer $observer)
    {
        if (Mage::registry('varProcessing')) {
            Mage::throwException(Mage::helper('core')->__('Disallowed template variable method.'));
        }
        return $this;
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Core
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * File storage model class
 *
 * @category    Mage
 * @package     Mage_Core
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Core_Model_File_Storage extends Mage_Core_Model_Abstract
{
    /**
     * Storage systems ids
     */
    const STORAGE_MEDIA_FILE_SYSTEM         = 0;
    const STORAGE_MEDIA_DATABASE            = 1;

    /**
     * Config pathes for storing storage configuration
     */
    const XML_PATH_STORAGE_MEDIA            = 'default/system/media_storage_configuration/media_storage';
    const XML_PATH_STORAGE_MEDIA_DATABASE   = 'default/system/media_storage_configuration/media_database';
    const XML_PATH_MEDIA_RESOURCE_WHITELIST = 'default/system/media_storage_configuration/allowed_resources';
    const XML_PATH_MEDIA_RESOURCE_IGNORED   = 'default/system/media_storage_configuration/ignored_resources';
    const XML_PATH_MEDIA_UPDATE_TIME        = 'system/media_storage_configuration/configuration_update_time';


    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'core_file_storage';

    /**
     * Show if there were errors while synchronize process
     *
     * @param  Mage_Core_Model_Abstract $sourceModel
     * @param  Mage_Core_Model_Abstract $destinationModel
     * @return bool
     */
    protected function _synchronizeHasErrors(Mage_Core_Model_Abstract $sourceModel,
        Mage_Core_Model_Abstract $destinationModel
    ) {
        if (!$sourceModel || !$destinationModel) {
            return true;
        }

        return $sourceModel->hasErrors() || $destinationModel->hasErrors();
    }

    /**
     * Return synchronize process status flag
     *
     * @return Mage_Core_Model_File_Storage_Flag
     */
    public function getSyncFlag()
    {
        return Mage::getSingleton('core/file_storage_flag')->loadSelf();
    }

    /**
     * Retrieve storage model
     * If storage not defined - retrieve current storage
     *
     * params = array(
     *  connection  => string,  - define connection for model if needed
     *  init        => bool     - force initialization process for storage model
     * )
     *
     * @param  int|null $storage
     * @param  array $params
     * @return Mage_Core_Model_Abstract|bool
     */
    public function getStorageModel($storage = null, $params = array())
    {
        if (is_null($storage)) {
            $storage = Mage::helper('core/file_storage')->getCurrentStorageCode();
        }

        switch ($storage) {
            case self::STORAGE_MEDIA_FILE_SYSTEM:
                $model = Mage::getModel('core/file_storage_file');
                break;
            case self::STORAGE_MEDIA_DATABASE:
                $connection = (isset($params['connection'])) ? $params['connection'] : null;
                $model = Mage::getModel('core/file_storage_database', array('connection' => $connection));
                break;
            default:
                return false;
        }

        if (isset($params['init']) && $params['init']) {
            $model->init();
        }

        return $model;
    }

    /**
     * Synchronize current media storage with defined
     * $storage = array(
     *  type        => int
     *  connection  => string
     * )
     *
     * @param  array $storage
     * @return Mage_Core_Model_File_Storage
     */
    public function synchronize($storage)
    {
        if (is_array($storage) && isset($storage['type'])) {
            $storageDest    = (int) $storage['type'];
            $connection     = (isset($storage['connection'])) ? $storage['connection'] : null;
            $helper         = Mage::helper('core/file_storage');

            // if unable to sync to internal storage from itself
            if ($storageDest == $helper->getCurrentStorageCode() && $helper->isInternalStorage()) {
                return $this;
            }

            $sourceModel        = $this->getStorageModel();
            $destinationModel   = $this->getStorageModel(
                $storageDest,
                array(
                    'connection'    => $connection,
                    'init'          => true
                )
            );

            if (!$sourceModel || !$destinationModel) {
                return $this;
            }

            $hasErrors = false;
            $flag = $this->getSyncFlag();
            $flagData = array(
                'source'                        => $sourceModel->getStorageName(),
                'destination'                   => $destinationModel->getStorageName(),
                'destination_storage_type'      => $storageDest,
                'destination_connection_name'   => (string) $destinationModel->getConfigConnectionName(),
                'has_errors'                    => false,
                'timeout_reached'               => false
            );
            $flag->setFlagData($flagData);

            $destinationModel->clear();

            $offset = 0;
            while (($dirs = $sourceModel->exportDirectories($offset)) !== false) {
                $flagData['timeout_reached'] = false;
                if (!$hasErrors) {
                    $hasErrors = $this->_synchronizeHasErrors($sourceModel, $destinationModel);
                    if ($hasErrors) {
                        $flagData['has_errors'] = true;
                    }
                }

                $flag->setFlagData($flagData)
                    ->save();

                $destinationModel->importDirectories($dirs);
                $offset += count($dirs);
            }
            unset($dirs);

            $offset = 0;
            while (($files = $sourceModel->exportFiles($offset, 1)) !== false) {
                $flagData['timeout_reached'] = false;
                if (!$hasErrors) {
                    $hasErrors = $this->_synchronizeHasErrors($sourceModel, $destinationModel);
                    if ($hasErrors) {
                        $flagData['has_errors'] = true;
                    }
                }

                $flag->setFlagData($flagData)
                    ->save();

                $destinationModel->importFiles($files);
                $offset += count($files);
            }
            unset($files);
        }

        return $this;
    }

    /**
     * Return current media directory, allowed resources for get.php script, etc.
     *
     * @return array
     */
    public static function getScriptConfig()
    {
        $config = array();
        $config['media_directory'] = Mage::getBaseDir('media');

        $allowedResources = (array) Mage::app()->getConfig()->getNode(self::XML_PATH_MEDIA_RESOURCE_WHITELIST);
        foreach ($allowedResources as $key => $allowedResource) {
            $config['allowed_resources'][] = $allowedResource;
        }

        $config['update_time'] = Mage::getStoreConfig(self::XML_PATH_MEDIA_UPDATE_TIME);

        return $config;
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Core
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Core Flag model
 *
 * @method Mage_Core_Model_Resource_Flag _getResource()
 * @method Mage_Core_Model_Resource_Flag getResource()
 * @method string getFlagCode()
 * @method Mage_Core_Model_Flag setFlagCode(string $value)
 * @method int getState()
 * @method Mage_Core_Model_Flag setState(int $value)
 * @method string getLastUpdate()
 * @method Mage_Core_Model_Flag setLastUpdate(string $value)
 *
 * @category    Mage
 * @package     Mage_Core
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Core_Model_Flag extends Mage_Core_Model_Abstract
{
    /**
     * Flag code
     *
     * @var string
     */
    protected $_flagCode = null;

    /**
     * Init resource model
     * Set flag_code if it is specified in arguments
     *
     */
    protected function _construct()
    {
        if ($this->hasData('flag_code')) {
            $this->_flagCode = $this->getData('flag_code');
        }
        $this->_init('core/flag');
    }

    /**
     * Processing object before save data
     *
     * @return Mage_Core_Model_Flag
     */
    protected function _beforeSave()
    {
        if (is_null($this->_flagCode)) {
            Mage::throwException(Mage::helper('core')->__('Please define flag code.'));
        }

        $this->setFlagCode($this->_flagCode);
        $this->setLastUpdate(date('Y-m-d H:i:s'));

        return parent::_beforeSave();
    }

    /**
     * Retrieve flag data
     *
     * @return mixed
     */
    public function getFlagData()
    {
        if ($this->hasFlagData()) {
            return unserialize($this->getData('flag_data'));
        } else {
            return null;
        }
    }

    /**
     * Set flag data
     *
     * @param mixed $value
     * @return Mage_Core_Model_Flag
     */
    public function setFlagData($value)
    {
        return $this->setData('flag_data', serialize($value));
    }

    /**
     * load self (load by flag code)
     *
     * @return Mage_Core_Model_Flag
     */
    public function loadSelf()
    {
        if (is_null($this->_flagCode)) {
            Mage::throwException(Mage::helper('core')->__('Please define flag code.'));
        }

        return $this->load($this->_flagCode, 'flag_code');
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Core
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Synchronize process status flag class
 *
 * @category    Mage
 * @package     Mage_Core
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Core_Model_File_Storage_Flag extends Mage_Core_Model_Flag
{
    /**
     * There was no synchronization
     */
    const STATE_INACTIVE    = 0;
    /**
     * Synchronize process is active
     */
    const STATE_RUNNING     = 1;
    /**
     * Synchronization finished
     */
    const STATE_FINISHED    = 2;
    /**
     * Synchronization finished and notify message was formed
     */
    const STATE_NOTIFIED    = 3;

    /**
     * Flag time to life in seconds
     */
    const FLAG_TTL          = 300;

    /**
     * Synchronize flag code
     *
     * @var string
     */
    protected $_flagCode    = 'synchronize';

    /**
     * Pass error to flag
     *
     * @param Exception $e
     * @return Mage_Core_Model_File_Storage_Flag
     */
    public function passError(Exception $e)
    {
        $data = $this->getFlagData();
        if (!is_array($data)) {
            $data = array();
        }
        $data['has_errors'] = true;
        $this->setFlagData($data);
        return $this;
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Core
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Flag model
 *
 * @category    Mage
 * @package     Mage_Core
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Core_Model_Resource_Flag extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Define main table
     *
     */
    protected function _construct()
    {
        $this->_init('core/flag', 'flag_id');
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Core
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Provides basic logic for hashing passwords and encrypting/decrypting misc data
 *
 * @category   Mage
 * @package    Mage_Core
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Core_Model_Encryption
{
    /**
     * @var Varien_Crypt_Mcrypt
     */
    protected $_crypt;
    /**
     * @var Mage_Core_Helper_Data
     */
    protected $_helper;

    /**
     * Set helper instance
     *
     * @param Mage_Core_Helper_Data $helper
     * @return Mage_Core_Model_Encryption
     */
    public function setHelper($helper)
    {
        $this->_helper = $helper;
        return $this;
    }

    /**
     * Generate a [salted] hash.
     *
     * $salt can be:
     * false - a random will be generated
     * integer - a random with specified length will be generated
     * string
     *
     * @param string $password
     * @param mixed $salt
     * @return string
     */
    public function getHash($password, $salt = false)
    {
        if (is_integer($salt)) {
            $salt = $this->_helper->getRandomString($salt);
        }
        return $salt === false ? $this->hash($password) : $this->hash($salt . $password) . ':' . $salt;
    }

    /**
     * Hash a string
     *
     * @param string $data
     * @return string
     */
    public function hash($data)
    {
        return md5($data);
    }

    /**
     * Validate hash against hashing method (with or without salt)
     *
     * @param string $password
     * @param string $hash
     * @return bool
     * @throws Exception
     */
    public function validateHash($password, $hash)
    {
        $hashArr = explode(':', $hash);
        switch (count($hashArr)) {
            case 1:
                return hash_equals($this->hash($password), $hash);
            case 2:
                return hash_equals($this->hash($hashArr[1] . $password),  $hashArr[0]);
        }
        Mage::throwException('Invalid hash.');
    }

    /**
     * Instantiate crypt model
     *
     * @param string $key
     * @return Varien_Crypt_Mcrypt
     */
    protected function _getCrypt($key = null)
    {
        if (!$this->_crypt) {
            if (null === $key) {
                $key = (string)Mage::getConfig()->getNode('global/crypt/key');
            }
            $this->_crypt = Varien_Crypt::factory()->init($key);
        }
        return $this->_crypt;
    }

    /**
     * Encrypt a string
     *
     * @param string $data
     * @return string
     */
    public function encrypt($data)
    {
        return base64_encode($this->_getCrypt()->encrypt((string)$data));
    }

    /**
     * Decrypt a string
     *
     * @param string $data
     * @return string
     */
    public function decrypt($data)
    {
        return str_replace("\x0", '', trim($this->_getCrypt()->decrypt(base64_decode((string)$data))));
    }

    /**
     * Return crypt model, instantiate if it is empty
     *
     * @param string $key
     * @return Varien_Crypt_Mcrypt
     */
    public function validateKey($key)
    {
        return $this->_getCrypt($key);
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Checkout
 * @copyright   Copyright (c) 2010 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Checkout observer model
 *
 * @category   Mage
 * @package    Mage_Checkout
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Devinc_Dailydeal_Model_Observer
{		
    /**
     * Product qty's checked
     * data is valid if you check quote item qty and use singleton instance
     *
     * @var array
     */
    protected $_checkedQuoteItems = array();
    protected $_dealProductIds;
    
	//delete deal if product is deleted from catalog
	public function deleteDeal($observer)
    {
		$productId = $observer->getEvent()->getProduct()->getId();		
		$dealCollection = Mage::getModel('dailydeal/dailydeal')->getCollection()->addFieldToFilter('product_id', $productId);		
		
		if (count($dealCollection)>0) {
			foreach ($dealCollection as $deal) {
				$deal->delete();
			}
		}
	}
	
	//refresh deals and redirect the homepage to the deal page
	public function refreshDealsHomepageRedirect($observer)
    {		
		$helper = Mage::helper('dailydeal');
    
		//refresh deals
		if ($helper->isEnabled() && Mage::getStoreConfig('dailydeal/configuration/refresh_rate')!=0 && $this->_requiresRefresh()) {
			Mage::getModel('dailydeal/dailydeal')->refreshDeals(); 
		}
    
		//redirect to main deal if option enabled and if on homepage
		$url = Mage::helper('core/url')->getCurrentUrl();
		$baseUrl = Mage::getBaseUrl();
		$baseUrlNoIndex = str_replace('index.php/', '', Mage::getBaseUrl());
		$path = str_replace($baseUrlNoIndex, '', str_replace($baseUrl, '', $url));	
		if ($path == '' || $path == '/' || $path == '/index.php' || $path == '/index.php/' || $path == '/home' || $path == '/home/') {	
	        $storeId = Mage::app()->getStore()->getId();
			$redirectEnabled = Mage::getStoreConfig('dailydeal/configuration/redirect', $storeId);
			
			if ($helper->isEnabled() && $redirectEnabled) {   
				$mainDeal = Mage::helper('dailydeal')->getDeal();				
				if ($mainDeal) {
					$response = $observer->getEvent()->getResponse();
					$product = Mage::getModel('catalog/product')->load($mainDeal->getProductId());				
					$product->setDoNotUseCategoryId(true);
					$response->setRedirect($product->getProductUrl());
				}
			}
		}
	}
	
	protected function _requiresRefresh() {
		$resource = Mage::getSingleton('core/resource');
	    $connection = $resource->getConnection('core_read');

	    $select = $connection->select()
	    ->from($resource->getTableName('core_config_data'))
	    ->where('scope = ?', 'default')
	    ->where('scope_id = ?', 0)
	    ->where('path = ?', 'dailydeal/refresh');

		$rows = $connection->fetchAll($select); 
		
		if (count($rows)>0) {
			$refreshDateTime = trim($rows[0]['value']);
			if ($this->_getCurrentDateTime()>$refreshDateTime) {
			    $this->_saveRefresh();
			    return true;
			} else {
			    return false;
			}
		} else {
			$this->_saveRefresh();
			return true;
		}
	}
	
	protected function _getCurrentDateTime($_storeId = null, $_format = 'Y-m-d H:i:s') {
		if (is_null($_storeId)) {
			$_storeId = Mage::app()->getStore()->getId();
		}
		$storeDatetime = new DateTime();
		$storeDatetime->setTimezone(new DateTimeZone(Mage::getStoreConfig('general/locale/timezone', $_storeId)));	
		
		return $storeDatetime->format($_format);
	}
	
	protected function _saveRefresh() {
		$nextVerificationDate = date('Y-m-d H:i:s', strtotime($this->_getCurrentDateTime().'+'.Mage::getStoreConfig('dailydeal/configuration/refresh_rate').' minutes'));
		Mage::getModel('core/config')->saveConfig('dailydeal/refresh', $nextVerificationDate, 'default', 0);	
	}
    
	//runs at checkout pages. checks to see if the deals have reached their maximum qty
	public function reviewCartItem($observer) {	
		if (Mage::helper('dailydeal')->isEnabled()) {	
			$item = $observer->getEvent()->getItem();	
			$quote = $item->getQuote();
			$product = Mage::getModel('catalog/product')->load($item->getProductId());	
			$helper = Mage::helper('dailydeal');
			$deal = $helper->getDealByProduct($product);
			
			if ($deal) {	
				$checkQtyForType = array('simple', 'virtual', 'downloadable');	
				$totalQty = $this->_getQuoteItemQtyForCheck($product->getId(), $item->getId(), $item->getQty());     
			    $maxQty = $deal->getDealQty();
			    
			    if (in_array($product->getTypeId(), $checkQtyForType) && $maxQty<$totalQty) {
			    	$message = $helper->__('The maximum order qty available for the "%s" DEAL is %s.', $product->getName(), $maxQty);
                	$item->setHasError(true);
                	$item->setMessage($message);
			    	$quote->setHasError(true);
			    	$quote->addMessage($message);
			    }     			   						
			} 	
		}
	}
	
	//retrieves the total product qty from the cart; also taking into account the qty of associated products or custom options from the cart
    protected function _getQuoteItemQtyForCheck($productId, $quoteItemId, $itemQty)
    {
        $qty = $itemQty;
        if (isset($this->_checkedQuoteItems[$productId]['qty']) &&
            !in_array($quoteItemId, $this->_checkedQuoteItems[$productId]['items'])) {
                $qty += $this->_checkedQuoteItems[$productId]['qty'];
        }
		
        $this->_checkedQuoteItems[$productId]['qty'] = $qty;
        $this->_checkedQuoteItems[$productId]['items'][] = $quoteItemId;

        return $qty;
    }
	
	//updates the deals available qty and it's sold qty
	public function updateDealQty($observer)
    {
		$items = $observer->getEvent()->getOrder()->getItemsCollection();		
		foreach ($items as $item) {			
			$helper = Mage::helper('dailydeal');	
			$product = Mage::getModel('catalog/product')->load($item->getProductId());
			$deal = $helper->getDealByProduct($product);
			$checkQtyForType = array('simple', 'virtual', 'downloadable', 'configurable');	
			
			if ($helper->isEnabled() && $deal && in_array($product->getTypeId(), $checkQtyForType)) {	
				$newQty = $deal->getDealQty()-$item->getQtyOrdered();
				$newSoldQty = $deal->getQtySold()+$item->getQtyOrdered();				
				
				$deal->setDealQty($newQty)->setQtySold($newSoldQty)->save();	
				Mage::getModel('dailydeal/dailydeal')->refreshDeal($deal);
			}
		}
	}
	
	//sets the deal price to the product
	public function getFinalPrice($observer)
    {
    	$product = $observer->getEvent()->getProduct();
    	$qty = $observer->getEvent()->getQty();
		$helper = Mage::helper('dailydeal');
		$deal = $helper->getDealByProduct($product);
		$currentDateTime = Mage::helper('dailydeal')->getCurrentDateTime(0);
		
		if ($helper->isEnabled() && $deal) {	
			$setPriceForType = array('simple', 'virtual', 'downloadable', 'configurable');	
			if ($currentDateTime>=$deal->getDatetimeFrom() && $currentDateTime<=$deal->getDatetimeTo()) {
				if (in_array($product->getTypeId(), $setPriceForType)) {
					$price = $this->_applyTierPrice($product, $qty, $deal->getDealPrice());
					$product->setFinalPrice($price);	
				} 
			} else {
				Mage::getModel('dailydeal/dailydeal')->refreshDeal($deal);				
			}
		}
	}
	
	public function setCollectionFinalPrice($observer)
    {
		$helper = Mage::helper('dailydeal');
		if(!$helper->isEnabled()){
			return $this;
		}

    	$products = $observer->getEvent()->getCollection();
		$currentDateTime = Mage::helper('dailydeal')->getCurrentDateTime(0);
		
        if (!$this->_dealProductIds) {
			$this->_dealProductIds = Mage::getModel('dailydeal/dailydeal')->getCollection()->addFieldToFilter('status', array('eq'=>Devinc_Dailydeal_Model_Source_Status::STATUS_RUNNING))->getColumnValues('product_id');
		}

    	foreach ($products as $product) {
			if (in_array($product->getId(), $this->_dealProductIds)) {
				$deal = $helper->getDealByProduct($product);
				
				if ($deal) {	
					$setPriceForType = array('simple', 'virtual', 'downloadable', 'configurable');	
					if ($currentDateTime>=$deal->getDatetimeFrom() && $currentDateTime<=$deal->getDatetimeTo()) {
						if (in_array($product->getTypeId(), $setPriceForType)) {
							$price = $this->_applyTierPrice($product, null, $deal->getDealPrice());
							$product->setFinalPrice($price);	
						}
					} else {
						Mage::getModel('dailydeal/dailydeal')->refreshDeal($deal);				
					}
				}
			}
		}
	}
	
	protected function _applyTierPrice($product, $qty, $finalPrice)
    {
        if (is_null($qty)) {
            return $finalPrice;
        }

        $tierPrice  = $product->getTierPrice($qty);
        if (is_numeric($tierPrice)) {
            $finalPrice = min($finalPrice, $tierPrice);
        }
        return $finalPrice;
    }

    //saves the loaded product ids in a session
	public function collectLoadedProductIds($observer)
    {
    	$helper = Mage::helper('dailydeal');
    	if (Mage::getSingleton('customer/session')->getProductIds()) {
    		$productIds = unserialize(Mage::getSingleton('customer/session')->getProductIds());
    	} else {
			$productIds = array();
		}
    	if (Mage::getSingleton('customer/session')->getProductStock()) {
    		$productStock = unserialize(Mage::getSingleton('customer/session')->getProductStock());
    	} else {
			$productStock = array();
		}
		
    	$products = $observer->getEvent()->getCollection();	
        
    	foreach ($products as $product) {
			$productIds[] = $product->getId();
			$productStock[$product->getId()] = $product->isSaleable();
		}
		
		Mage::getSingleton('customer/session')->setProductIds(serialize($productIds));
		Mage::getSingleton('customer/session')->setProductStock(serialize($productStock));
	}

	//enable parallax for desktop pcs
	public function updateBlocksBefore($observer)
	{		
        $block = $observer->getEvent()->getBlock();   
        if ($block->getNameInLayout() == 'head' && Mage::getStoreConfig('dailydeal/configuration/enabled') && !Mage::getModel('license/module')->isMobile() && !Mage::getModel('license/module')->isTablet()) { 
        	$block->setIsDesktop(true);
        }  

		$countdownType = Mage::getStoreConfig('dailydeal/configuration/countdown_type');
        if ($block->getNameInLayout() == 'head' && Mage::getStoreConfig('dailydeal/configuration/enabled')) {
        	if ($countdownType==0) {
        		$block->setCircleCountdown(true);        		
        	} elseif ($countdownType==1) {
        		$block->setFlipCountdown(true);        		
        	} elseif ($countdownType==2) {
        		$block->setSimpleCountdown(true);        		
        	}
        }  
	}

}

class Devinc_Dailydeal_Helper_Data extends Mage_Core_Helper_Abstract
{
	const STATUS_RUNNING = Devinc_Dailydeal_Model_Source_Status::STATUS_RUNNING;
	const STATUS_DISABLED = Devinc_Dailydeal_Model_Source_Status::STATUS_DISABLED;
	const STATUS_ENDED = Devinc_Dailydeal_Model_Source_Status::STATUS_ENDED;
	const STATUS_QUEUED = Devinc_Dailydeal_Model_Source_Status::STATUS_QUEUED;
	
	//check if extension is enabled
	public static function isEnabled()
	{
		$storeId = Mage::app()->getStore()->getId();
		$isModuleEnabled = Mage::getStoreConfig('advanced/modules_disable_output/Devinc_Dailydeal', $storeId);
		$isEnabled = Mage::getStoreConfig('dailydeal/configuration/enabled', $storeId);
		return ($isModuleEnabled == 0 && $isEnabled == 1);
	}
	
	//$toDate format(year-month-day hour:minute:second) = 0000-00-00 00:00:00
    public function getCountdown($_product, $finished = false)
    {
    	$toDate = $_product->getDatetimeTo();
    	$countdownId = $_product->getId();
		$randomNr = rand(10e16, 10e20);

    	//from/to date variables
		$fromDate = $this->getCurrentDateTime();
		$jsFromDate = date('F d, Y H:i:s', strtotime($fromDate));
		$jsToDate = date('F d, Y H:i:s', strtotime($toDate));			
		if ($finished) {
			$toDate = $fromDate;
			$jsToDate = $jsFromDate;	
		}	
		
		$countdownType = Mage::getStoreConfig('dailydeal/configuration/countdown_type');

		//cirle countdown configuration		
		if ($countdownType==0) {
			$bgColor = (Mage::getStoreConfig('dailydeal/circle_countdown/bg_color')) ? $this->hex2rgb(Mage::getStoreConfig('dailydeal/circle_countdown/bg_color')) : '192, 202, 202';
			$loadingColor = (Mage::getStoreConfig('dailydeal/circle_countdown/loading_color')) ? $this->hex2rgb(Mage::getStoreConfig('dailydeal/circle_countdown/loading_color')) : '229, 233, 233';
			$daysText = Mage::getStoreConfig('dailydeal/circle_countdown/days_text');
			$hourText = Mage::getStoreConfig('dailydeal/circle_countdown/hour_text');
			$minText = Mage::getStoreConfig('dailydeal/circle_countdown/min_text');
			$secText = Mage::getStoreConfig('dailydeal/circle_countdown/sec_text');
		} elseif ($countdownType==1) {
			$daysText = Mage::getStoreConfig('dailydeal/flip_countdown/days_text');
			$hourText = Mage::getStoreConfig('dailydeal/flip_countdown/hour_text');
			$minText = Mage::getStoreConfig('dailydeal/flip_countdown/min_text');
			$secText = Mage::getStoreConfig('dailydeal/flip_countdown/sec_text');
		} elseif ($countdownType==2) {
			$daysText = Mage::getStoreConfig('dailydeal/simple_countdown/days_text');
			$hourText = Mage::getStoreConfig('dailydeal/simple_countdown/hour_text');
			$minText = Mage::getStoreConfig('dailydeal/simple_countdown/min_text');
			$secText = Mage::getStoreConfig('dailydeal/simple_countdown/sec_text');
		}
		
		$date1 = strtotime($fromDate);
	    $date2 = strtotime($toDate);	   
		$dateDiff = $date2 - $date1;
		$days = floor($dateDiff/(60*60*24));

		if ($countdownType==0) {
			if ($days>0) {
				$size = '50';
				$daysClass = ' countdown-days';
				$layout = '<ul>'.
				'{d<}<li class="days"><em>{dn}</em> '.$daysText.'</li>{d>}'.
				'{h<}<li class="hours"><em>{hn}</em> '.$hourText.'</li>{h>}'.
				'{m<}<li class="minutes"><em>{mn}</em> '.$minText.'</li>{m>}'.
				'{s<}<li class="seconds"><em>{sn}</em> '.$secText.'</li>{s>}'.
				'</ul>';
			} else {
				$size = '60';
				$daysClass = '';				
				$layout = '<ul>'.
				'{h<}<li class="hours"><em>{hn}</em> '.$hourText.'</li>{h>}'.
				'{m<}<li class="minutes"><em>{mn}</em> '.$minText.'</li>{m>}'.
				'{s<}<li class="seconds"><em>{sn}</em> '.$secText.'</li>{s>}'.
				'</ul>';
			}
			$html = '
				<div class="countdown'.$daysClass.'">
					<div id="countdown-'.$countdownId.'-'.$randomNr.'-timer" class="countdown_timer"></div>			                
				    <div id="countdown-'.$countdownId.'-'.$randomNr.'" class="countdown_clock">';
				    if ($days>0) {             
				        $html .= '<canvas class="circular_countdown_element" id="circular_countdown_days-'.$countdownId.'-'.$randomNr.'" width="'.$size.'" height="'.$size.'"></canvas>';
				    }

			  $html .= '<canvas class="circular_countdown_element" id="circular_countdown_hours-'.$countdownId.'-'.$randomNr.'" width="'.$size.'" height="'.$size.'"></canvas>
				        <canvas class="circular_countdown_element" id="circular_countdown_minutes-'.$countdownId.'-'.$randomNr.'" width="'.$size.'" height="'.$size.'"></canvas>
				        <canvas class="circular_countdown_element last" id="circular_countdown_seconds-'.$countdownId.'-'.$randomNr.'" width="'.$size.'" height="'.$size.'"></canvas>
				    </div>
				</div>';

			$html .= '<script type="text/javascript">
						jQueryDD(document).ready(function($){
						    jQueryDD(\'#countdown-'.$countdownId.'-'.$randomNr.'\').circularCountdown({
						        strokeDaysBackgroundColor:\'rgba('.$loadingColor.',1)\',
						        strokeDaysColor:\'rgba('.$bgColor.',1)\',
						        strokeHoursBackgroundColor:\'rgba('.$loadingColor.',1)\',
						        strokeHoursColor:\'rgba('.$bgColor.',1)\',
						        strokeMinutesBackgroundColor:\'rgba('.$loadingColor.',1)\',
						        strokeMinutesColor:\'rgba('.$bgColor.',1)\',
						        strokeSecondsBackgroundColor:\'rgba('.$loadingColor.',1)\',
						        strokeSecondsColor:\'rgba('.$bgColor.',1)\',
						        strokeWidth:6,
						        strokeBackgroundWidth:6,
						        countdownEasing:\'easeOutBounce\',
						        countdownTickSpeed:\'slow\',
						        currentDateTime: \''.$jsFromDate.'\',
						        until: new Date(\''.$jsToDate.'\'),
						        layout: \''.$layout.'\',
						        id: \''.$countdownId.'-'.$randomNr.'\'
						    });
						});
					</script>';
		} elseif ($countdownType==1) {
			$countdownToDateTime = explode(' ', $toDate);
			$countdownToDate = $countdownToDateTime[0];
			$countdownToTime = $countdownToDateTime[1];

			if ($days>0) {
				$html = '<div class="countdown countdown-days" id="countdown-'.$countdownId.'-'.$randomNr.'">
				<div class="unit-wrap">
					<div class="days"></div>
					<span class="ce-days-label"></span>
				</div>';
			} else {
				$html = '<div class="countdown" id="countdown-'.$countdownId.'-'.$randomNr.'">';
			}

			$html .= '<div class="unit-wrap">
					<div class="hours"></div>
					<span class="ce-hours-label"></span>
				</div>
				<div class="unit-wrap">
					<div class="minutes"></div>
					<span class="ce-minutes-label"></span>
				</div>
				<div class="unit-wrap">
					<div class="seconds"></div>
					<span class="ce-seconds-label"></span>
				</div>
			</div>';
			
			$html .= '<script type="text/javascript">
				 			runCountdown(\'countdown-'.$countdownId.'-'.$randomNr.'\',\''.$countdownToDate.'\',\''.$countdownToTime.'\',\''.$jsFromDate.'\',\''.$daysText.'\',\''.$hourText.'\',\''.$minText.'\',\''.$secText.'\');
				 		</script>';
		} else {
			if ($days>0) {
				$html = '<div class="countdown countdown-days" id="countdown-'.$countdownId.'-'.$randomNr.'">
								<div class="days">
				                    <span class="digits">42</span>
				                    <span class="label">'.$daysText.'</span>
				                </div>
				                <span class="sep days-sep">•</span>';
			} else {
				$html = '<div class="countdown" id="countdown-'.$countdownId.'-'.$randomNr.'">';
			}
			
			$html .= '<div class="hours">
		                    <span class="digits">11</span>
		                    <span class="label">'.$hourText.'</span>
		                </div>
		                <span class="sep">•</span>
		                <div class="minutes">
		                    <span class="digits">48</span>
		                    <span class="label">'.$minText.'</span>
		                </div>
		                <span class="sep">•</span>
		                <div class="seconds">
		                    <span class="digits">46</span>
		                    <span class="label">'.$secText.'</span>
		                </div>
			        </div>';

			$html .= '<script type="text/javascript">
				 			var jsCountdown = new JsCountdown("'.$jsFromDate.'", "'.$jsToDate.'", "countdown-'.$countdownId.'-'.$randomNr.'");
				 		</script>';
		}
	
        return $html;
    }
        
    //called on product list pages
    public function getProductCountdown(Varien_Object $_product, $_timeLeftText=false) {
		$deal = $this->getDealByProduct($_product);
		$html = '';		
		if (Mage::helper('dailydeal')->isEnabled() && $deal) {		
			$toDate = $deal->getDatetimeTo();
			$_product->setDatetimeTo($toDate);
			$currentDateTime = Mage::helper('dailydeal')->getCurrentDateTime(0);
			//set it to finished if the deal's time is up or product is out of stock
			if ($currentDateTime>=$deal->getDatetimeFrom() && $currentDateTime<=$deal->getDatetimeTo()) {
    			$finished = ($_product->isSaleable()) ? false : true;
    		} else {
				Mage::getModel('dailydeal/dailydeal')->refreshDeal($deal);
    			$finished = true;
    		}
    		$html .= ($_timeLeftText) ? '<span class="countdown-time-left">'.$this->__('Time left to buy').'</span>' : '';
			$html .= $this->getCountdown($_product, $finished);
		}
				
		return $html;
    }
	
	public function getCurrentDateTime($_storeId = null, $_format = 'Y-m-d H:i:s') {
		if (is_null($_storeId)) {
			$_storeId = Mage::app()->getStore()->getId();
		}
		$storeDatetime = new DateTime();
		$storeDatetime->setTimezone(new DateTimeZone(Mage::getStoreConfig('general/locale/timezone', $_storeId)));	
		
		return $storeDatetime->format($_format);
	}
	
	//returns the page number for the "Select a Product" tab on the deal edit page
    public function getProductPage($productId)
    {
    	$visibility = array(2, 4);
    	$collectionSize = Mage::getModel('catalog/product')->getCollection()->setOrder('entity_id', 'DESC')->addAttributeToFilter('visibility', $visibility)->addAttributeToFilter('entity_id', array('gteq' => $productId))->getSize();
    	
    	return ceil($collectionSize/20);
    }
    
    public function getMagentoVersion() {
		return (int)str_replace(".", "", Mage::getVersion());
    }
    
    //return true if deal should run on store
    public function runOnStore(Varien_Object $_deal, $_storeId = false) {
    	if ($_deal->getStores()!='') {
    		$dealStoreIds = array();
    		if (strpos($_deal->getStores(), ',')) {
				$dealStoreIds = explode(',', $_deal->getStores());
			} else {
				$dealStoreIds[] = $_deal->getStores();
			}
			if (!$_storeId) {
    			$_storeId = Mage::app()->getStore()->getId();
    		}
    		return (in_array($_storeId, $dealStoreIds)) ? true : false;
    	}
    	
    	return false;
    }
    
    //returns the main running deal of the current store
    public function getDeal($_excludeProductIds = array(0)) {  		    	
		$dealCollection = Mage::getModel('dailydeal/dailydeal')->getCollection()->addFieldToFilter('product_id', array('nin'=>$_excludeProductIds))->addFieldToFilter('status', array('eq'=>self::STATUS_RUNNING))->setOrder('position', 'ASC')->setOrder('dailydeal_id', 'DESC');	
			
		if (count($dealCollection)) {
	        foreach ($dealCollection as $deal) {
    	    	if ($this->runOnStore($deal)) {
		    		return $deal;
		    	}
		    }
		}	
		
		return false;	
    }
    
    //if the product is a deal and the deal is running under the current store, returns the deal data
    public function getDealByProduct(Varien_Object $_product) {  		    	
		$dealCollection = Mage::getModel('dailydeal/dailydeal')->getCollection()->addFieldToFilter('product_id', array('eq'=>$_product->getId()))->addFieldToFilter('status', array('eq'=>self::STATUS_RUNNING))->setOrder('position', 'ASC')->setOrder('dailydeal_id', 'DESC');
				
		if (count($dealCollection)) {
	        foreach ($dealCollection as $deal) {
    	    	if ($this->runOnStore($deal)) {
		    		return $deal;
		    	}
		    }
		}	
		
		return false;	
    }
    
    public function getDealsByProductIds($_productIds) {  		    	
		$dealCollection = Mage::getModel('dailydeal/dailydeal')->getCollection()->addFieldToFilter('product_id', array('in'=>$_productIds))->addFieldToFilter('status', array('eq'=>self::STATUS_RUNNING))->setOrder('position', 'ASC')->setOrder('dailydeal_id', 'DESC');
				
		if (count($dealCollection)) {
	        foreach ($dealCollection as $key=>$deal) {
    	    	if (!$this->runOnStore($deal)) {
		    		$dealCollection->removeItemByKey($key);
		    	}
		    }
		}	
		
		return $dealCollection;	
    }
    
    public function hex2rgb($hex) {
        return Mage::getModel('license/module')->hex2rgb($hex);
    }
    
    public function isMobile()
    {           
        if (Mage::getModel('license/module')->isMobile()) {
            return true;
        }
        
        return false;
    }    
    
    public function isTablet()
    {           
        if (Mage::getModel('license/module')->isTablet()) {
            return true;
        }
        
        return false;
    }  
    
}

class Devinc_Dailydeal_Model_Source_Status extends Varien_Object
{
    const STATUS_QUEUED		= -1;
    const STATUS_RUNNING	= 1;
    const STATUS_DISABLED	= 2;
    const STATUS_ENDED  	= 3; 

    static public function getOptionArray()
    {
        return array(
            self::STATUS_RUNNING    => Mage::helper('catalog')->__('Enabled'),
            self::STATUS_DISABLED   => Mage::helper('catalog')->__('Disabled')
        );
    }

}