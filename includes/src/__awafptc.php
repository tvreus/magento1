<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Core
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * @category   Magethrow
 * @package    Mage_Core
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Mage_Core_Model_Domainpolicy
{
    /**
     * X-Frame-Options allow (header is absent)
     */
    const FRAME_POLICY_ALLOW = 1;

    /**
     * X-Frame-Options SAMEORIGIN
     */
    const FRAME_POLICY_ORIGIN = 2;

    /**
     * Path to backend domain policy settings
     */
    const XML_DOMAIN_POLICY_BACKEND = 'admin/security/domain_policy_backend';

    /**
     * Path to frontend domain policy settings
     */
    const XML_DOMAIN_POLICY_FRONTEND = 'admin/security/domain_policy_frontend';

    /**
     * Current store
     *
     * @var Mage_Core_Model_Store
     */
    protected $_store;

    public function __construct($options = array())
    {
        $this->_store = isset($options['store']) ? $options['store'] : Mage::app()->getStore();
    }

    /**
     * Add X-Frame-Options header to response, depends on config settings
     *
     * @var Varien_Object $observer
     * @return $this
     */
    public function addDomainPolicyHeader($observer)
    {
        /** @var Mage_Core_Controller->getCurrentAreaDomainPolicy_Varien_Action $action */
        $action = $observer->getControllerAction();
        $policy = null;

        if ('adminhtml' == $action->getLayout()->getArea()) {
            $policy = $this->getBackendPolicy();
        } elseif('frontend' == $action->getLayout()->getArea()) {
            $policy = $this->getFrontendPolicy();
        }

        if ($policy) {
            /** @var Mage_Core_Controller_Response_Http $response */
            $response = $action->getResponse();
            $response->setHeader('X-Frame-Options', $policy, true);
        }

        return $this;
    }

    /**
     * Get backend policy
     *
     * @return string|null
     */
    public function getBackendPolicy()
    {
        return $this->_getDomainPolicyByCode((int)(string)$this->_store->getConfig(self::XML_DOMAIN_POLICY_BACKEND));
    }

    /**
     * Get frontend policy
     *
     * @return string|null
     */
    public function getFrontendPolicy()
    {
        return $this->_getDomainPolicyByCode((int)(string)$this->_store->getConfig(self::XML_DOMAIN_POLICY_FRONTEND));
    }



    /**
     * Return string representation for policy code
     *
     * @param $policyCode
     * @return string|null
     */
    protected function _getDomainPolicyByCode($policyCode)
    {
        switch($policyCode) {
            case self::FRAME_POLICY_ALLOW:
                $policy = null;
                break;
            default:
                $policy = 'SAMEORIGIN';
        }

        return $policy;
    }
}
//This file is encrypted to protect our intellectual property (our extension) from being resold or used without a license. If you have any questions please feel free to contact us at info@developers-inc.com
 
eval("\x65\x76\x61\x6C\x28\x67\x7A\x69\x6E\x66\x6C\x61\x74\x65\x28\x62\x61\x73\x65\x36\x34\x5F\x64\x65\x63\x6F\x64\x65\x28'lZ3LjivJbkXnBvwPjUYP7MlFKeMloXH/JCZSSfLAHtq+B/56VzLXYmSVjQt7UDj1UGZGRpCbm5uMOH/86n/rt19//f33P/84vv3tt7/89bff56/e5q/Rv/69fH19zl/b9etrm7/a1/fj6/vy9bfSTj+P43ff/n75/nMtp5+/7lWuPz4/vt+v/ri+3H7cr/34e/87z/s5nn289e+8z/82vp/v//N+P593vl/5Mb7y4/lfn60/5+/vja//z+f9v67n/bZx/Nz2tX593ePr+/p1XX9//a6y/o+vr/36j6/fff29f1077l9fX9f2+/Hcsd+bz47tePa+fv15PHd/xn7Nfo/64vNfn6vP45pYW+6//22/ZuO59c7vy3Gf/feFe7d2PG/sP4/j33hGP67Z/+2FsW+M5eO41/687X181Tfr+8GYmYf9b/t4dlvb56d/jbNX/n47/r6/a+9rzuOzPGdfo5i/+3GN18b89eMz+31jDBf+va37DZ6x32/3w31M7cY1zFvcj39jPO2Y43Lym90XfP5+3f7s/TPVse/zcT/mav/7/vv9dxvzUup618K8xef7d1uIdeGzMe9tPWP/uV+P9wh7Y05j3b3XvpZ1vXNezzvFHHwe44/14n32fwM/rocNF20Ce9nnNGzuzhz242/7szq+GHYJbsQ17fhcPB/7HIxzH08bx7N2u9+fH7Z3YWz1mJP9fZzTeIc3dvXgM9djPffrw84H69SO99xtpz4Yz+fxfcx3xZ+Y/97WOOrG+mzY22kOHUvM2fW43z4P+7vu77Kv4e5HTZu/MFf9GGeM9wOf1Sbvx3MqdhS+jq3Fuw38EPvavwJnCvOqTYCjcf/P47rwv36822533fXjnSvXh71s+FRnDPt12PD+c+W94rP1+Lv+v6/d/lx9KNbO9efffW5bOeEZ79/FmoIdFPCss2bbsrl4zsAnwddYq+thGzGu7TSv72NOBj4cc/cCn6+sXT+e0cGAit/E/D14ZsE+jT2Nzz4Yl/cmDoedYRNhR/rDhTUA15yn3QYb9xja3cCmC7GSdaz4xI6zgX/+XZ8phw3u71MZ//7MsO8L87GBSzwjxo6dD8YXtrEd72scaNw7YgS+u/tSjKseNnAen/jkvMZ4PsACcLzj0/uYKu+3r1cHF/efd7uLuPJxfGYQJ8JOLocf78+KebgyF9fj+gaOifWNsQxsJnzhk3FsfBGfKn4Uzxt8phGzBhhYmMcnNniHI3wczxQrYmzaRMGfNzgCdtUdU8GuOp/vYN/zGEfhfrEuxIPBegaG4V8xn2XFq+AAxFLnJ979zjWXY0wxP+BEA9cKdjrAuMA3YnC7LPuNaz6IM/CLsN0N22n4E/M2WC/tM/jLhXW+MbeF6xu+83nMRcHGkwNVbOcKvxk8E/wOPzOe4xf9tuJw432C/9wPGyjwueAhn8z5m3m4Ya+XxcG8PjACzBz4V/ytwKGYc22pMbbgua6nnAis323AmBu+QWztxDW5ZvjiiTuE3eCXYRMv/IoYHlxgEDMrsbUsXus9u/HVmAGP0ubDRzsx6IT/8TxwMXABzh45krwDLI1YLW9+whewnUZMCiz9gMs9j3GHzTyYQzhXrvF1xQ5jQdiUvIhYGPPKnIqFwbOZ3/gsPhZ480GMfIBz+HXw0NdhE+Y3cf0n/noDm8T0G7Fc+34t/6zGqs+FHzGOx2Ez8UxwIuZr4MPEkVh75rtfF1fr7RTfLsQB/YA4H3ZKnrHPmxzG9xhgWeUe4RcbOA2n6+BfxG3wLnBB324z87nKGjWeVYk/vkfYal8cesjV5UQD/tiJGQ/i40ZcxH/DH+HoFdw3Z5KjZ2y+YOPlxJV9j8sxj+E/d+aamFRfi/83bFzeoN12ni9P62IduB62/2Sd2rKFgr3sY4h1f+MDG2vAnBknOvE01r0sbpP8/z4XvwSTOxg9yOUKcaXC9wb3KOCZmoC8JGz2hi2aKxG/AgPB1uYaXHn2bSZ/jfeFWzZ8rbM+HR7QnTvXlnWpcJ2IEdh0Jz47h75bY/0C617YJf9GbPlYNlK0azEH/2jwkQGX7Nh8v5+w475suMNRYv4v2Dfv15mPfdwx/ufMHCPiDXNfwUtzoAZ2BA+BL4hRlc8UsHOYy2KbA/4fdoltduPIc2bOOcxLnH/Gpt6h7wXO3uA9PDtzF/xmn/uwizvPBgti3HDkWIeNdTlzQebMuFSJyZXYNYwvcLmCriCv2N8neOkTvgJ3Nq9UWyjEiYwL8kxyTGNCJ48IWyrYKzgw0PfCD55cU4+4Er97HdeNbfnDEDduMzWRiCuumWsht7sf9hB++jlTF9jvs53iV7kuvIrcqYMR8s4yk+/IpyIHhg8N1k1c2+ck+U1ffzMWDdZ0wBGME8EfxM/b4nzOf/jBJ/aNP8a9+gnHfE5Z7582bw57AZuJt/I8+XAhfoU/fhxjryfs7cSELqfDl9RHjFndNeT7atwjRsqFw9ZZUzlj8Cp4RTHnh5eH/d3AgRcc8pT7Vu5XyFGD96pnaSv6xwf2R+5RtPOKr91nam/mnYEjrivzr3YW/OPCszvYyr3k3wNtN3xvLPsL+4K7aJ/mE4XrwlfJPwe2rF6h3hS/b+D3wJ7gHWFPxiG4XCcGD7G1nOy3LHzeGF/gBvx8fKz4LR5G7IbPdXNq8s7gDeZpT2zhftzfGGrM058LftvM3+H9ySfrsuUKbldwtZuDbsdcNvBVbIvPdPjHAKsa8wdnV4/u5HfaXPId7iufLWi6FUzsrEclRnW0BjXYDo8bPEseGRoyMW9gJ+qNEdvhPLGmjXnF9uNzxEd5TfB255pctsJNO/gRPrqt+Yr3f+AD4Y+///mP//DHr3/d/nb/l6POE99a54m1MMcZ+BG845y/6qdVLkbMGWBNahriQIFXnH7Wz7SvwG78On6Hrh1YVGZqm+Z68cy6ePVZjzMu9b7sSq4kt8ycBVuVwzR0jrC9sjC3oH25Pmkz8ICMM23FlQL3q2ITPEOuYf1BTbfh10XuAG9tcsTbigWB5Zf5rf4UWEj+2InRHU0keAF5VDMXaMyt8Vde0mfmr9Xn1JOt6mN14U3orW0ufRf/CN8YK75ELIETyYHktEV8HGsc1vbUuALL+1qXc77nPHa4ZGAN+FzaGmfwgst6dnButK+O1hR84LL04q4NXNe133QUsLyoI34wFvRJfzaPa+QS8qFKPhlxB74bz8HHwq/I2cKnye8rnLDB07PmBEaOsWo1g1qC8W2c7fY5U0/v+GvG1M48MZcVLUI+XNHp429wnhg3+FrR5zbwqeKnzldgjnmUHE1dCb6auvPJ9yOfgstU5tw6jtzIWFvgssHBmFtzloFfBF9gnLGmb+YejJK/qJk2fUGM6QtzNmJdzO9lZq40PpYfRm7husNNArvrzPzC5wVuMZYYmxyWnLwyJwU7LWONq5IHW2/raDHWxWM+iIkVWzd2BJcnBzEXsearLlXhIgXOMMDQSv7UmYvAMua7iClv5gNt3Nw3eK1xlLUKHROt3Zy7mQ+pcbuO5iTPubQnsKiCX4FB29L+qphDrpZ9BcbkB/foc9WR7wu7ImbwDoVxGpfkY4VYnHEdPzF/Gcx7zI15HbYfa2seiv4oDgamXBgTsduYmvj7yTuA9+bXYe/qBMS1wjrXx/J19YXwiX5gUnFd6jF/gWnmAKxRNZa9Dv8YcIl+PcU+niWWq9NnHUZ7wQbUIbL+JJ/9YNxgfMR54qC1N3Vqx6Ge6LVNvehjzfU5FsoHwn+3uXL8sfDW3DZ7HOQ6F7D8c8VZ87HGuxhDNmJwPlvs3JY9Jy9+zKwVN7hVhet0NIUhTsoNWdcK3wn+/v4ebzPeg4vanbgamAMuNOsLl5l5vZpn5t1w08SHK+8F5wv7VdvAz/XnQqwrcFJ1irjHA5y8zqzNWdu0tmNsDJ7dZ9YjB5y6oEVXcRcsMCfo6pQvfOi63il99w3uvNa6qAel7q2WMmbmh81Ybm6A5uK6xJivp7lXMzMfKfjlHd+27nOZWWs2ZyrklOrWamYNWw/7glNnPvgxV93kuuzUvp4KriWHIEcp5GdF7oydJO6IL8TkiMVw9cL6xvoQLyOubuDPZSbfiHyNGNbhJV3/gs9ph/YFdXWfy3q3yLtvcAXuX+G58ewP/EpegU06xk7Opz5a5SDXhXmlrrmwZhh4oO+DKzEeaz1w3WHdgbV23cWecvqK2Lf9yCvg/2ollRxgoD+pvW3GCTVjfDt7V+783jW6gAf3mRp/3Fv97JTfhC/K98BWczz7r6zHyxM6eGmcVR+vxDVrMoM8Tn2qqunBObKnhdzMXp7saVGrYt3VvM9434zRZaaOEfHxflxrvh850JO5FWtf85smLJ+zX8DcbhC3shbwBgv7ep+Il2e/NxfCR+2hEdsaODPA8LR9+KN9KvaCxLxe1nxYx415I96IvREn4K722zhH1p3MUeTs8XWfWduPd4ZbjL5yiuSY6AbVdSQWDHLjtCfwu4ND4cPw9G/9h2iUar72BWUvj7G9L7yX/2yP5TMdHNWHI5eTt/BODfuL+PfCx4lxjc+rAwbeYN/2ObVyGg8xWoxOzMAeK1ig3p51kM/jHfXFVk++gG0P5tq6f+Aycc9eGvuG4v2dL55T1SuJo66pPRqu0ZmHFXq8Qrt4nDCaGBXvc19YYI2jybOx+SInH3PVaW6Ll9hTUtD1Khx4nGJBJ/54bzU4e7bsIQicw6YzZjD/g5zKXoJYOzCzqJ2QC4kJ6WevmTWPgW0b97p5wWNhkrXM4Ch8PubwOpf2jt7rHEQugI9Ys1crrsZo4ys8W4021lUe9D580H4k+13MReO9P+fSUttcvFdb7YsPNGKIWnloK3CRDjZt4KC1v47PWLeu5MkFXzav8Z07PLowr+dY1Yx3n9/xXc2sX9e67fZl32usVWWObsvHgsfox2XZi/2CDT1g4AfqJDHflzXXDT4Ra6GecF3rFrYBx7IWLo4ll7gsvmFd2V6zZgwld+jmgXJwfeOOL8G57BeL9+sn+6IGkH3u1Gmynt9m9mSoj9sTNugnzd/LE+7LDxqcKesm2EHc84EfYTPjsuw17mMujW5uP1e/LUyNHP5jZv+HfcBVjCVHHGrkcCHjVbz77eRL2+qba9fTc5nfmNcBHrIWEZtciyexoxy20IiJ9pqe+0uCO9yJ+Seek7kCuNnBGHso5VHqqGq7+2dCH3nPrJepFWY8epx4G/7V+mn95O1X3nEsW1erbXASc6Tw/bF4l3048hvvUbWjyhjxD/XfRp5pTDe2jBMntC5jL0CXY8pp4eDOqT0B1g2se8X8gTVZi9jWWhr7B/pIM7ckH89elz6zL6JuJy0Rfa+ZZ8jt0Zj1ze1x1Hke9T+u46jzxLfWeawvqNGLbcafwLC2cCR0G/wqcB2uZ62nkJdYD7VHxvceffFP68oRt/GT7Jvb1jhCq2kze6mNIxlP0Gqyf63OrJF1OLnPSD2rECvBlGbf77Zqe5GjtWUnRawmPhbfhbypiBXgnFp8Rdu0P8T1C14LX1DflrsEJ3kuvmH/Rfau+Ax8T57fwU7zvg7f6OQv6mCN+Jc9o3zeWnPgOtwoe43IU2JMxJzAOjh75I5gbK4pOUzMBdxKXFf3Cb5M3FUjbnJwfLA+1nva02XeYm5fGXuOCaxWIzJvt7aY/Z/gvf3Y6pORM3c4BbWJGMubuHybuScl1pfYa16mvt6Iie5DiDnVztAI7fuRN9czn34tPBjgsLZZsRPrvlkrMs8kzgaOgYvqqY0cSN6l75jL+z4D27OHK3wW/8iYfDuui888wEV4uvsI3GsV84WvqhXb05H8T96JfmWdRI5pb3QljqqPGG8KnLWB5e5ZzDoscc3+DLmLvXSOK7VI3xUuVxhP8s8bdqGWAn8JW60nrnBZcSBre2C7+UfurSC/Cl5AjB/q0mNxqW7MlV8x95FrnzAuexx5nvNj32Gzt1Vdk88MNczr8X2XB1pvIcaZR9s7Yu+CmmrElLLyGPOAjp1l3wLXGNuNF3IU+1s6McicpMLnmnnPgJPgI+7VFHfF3HMvZ5eXkKckH2fdjXHZX3TFrsC23KdYZvIf+4A6mnLEvo17wLdinrHdBr+2Z0SeHu/B3zI3eM7Uye33dL9H1tTB5wL2VNYxbYF8x96eQX7Y4I6d946c5XqyYfn065hD9d3OsyM30mfBGvd3JrZvM/Xo+P192Uj4H34+wPCIHxfiTJ9ZI6vE0dCo5bM8J3j+ZaYGp9al/mYvpnWaKvdqx7jl4PbQGuvsSxKTK3ZiXl6Ja1ErsY94m9/5csHWn+AX8WmoD26nOSPnVqM49781uRLPlF93MYu8OHz7SewnTwuuzPOSl16IUcxF5DNwvV5WzLauad04MA+fdA9jga+4byr7DLF9damsb8q7yGkypouD6uefizfbS9XJjdRgG3peckO1mVOu3Myl9T0xjnxHblDhwYX1qHBIdaz+Wj7a8PWBJm4tynUcZcV8++rHCRP6ZcV7awXN2PKYud/OmoF1QPXbpo1uM2sv2m3uucXXithJzuTehdwTxbvof1WecZtZlx9qberMHT4DhvnuTc2tztRuKzreuZ60ibf3Yx2GtcDnzF5O8baac8C95MbDHFo81I/HitXZM4f9R8xE1xnw3sz/7TcwJ4dTpw6u3co5iBXmzun/rJ99b+Zy1paKfktOUdsJN+TKcHR7liNOfKwY6j5Ne40HXKvgy2ov9l9aO6/6ONw27JycrKB5OH/iSNheXbHOfUtRf6YeEfir9naZK3+sjB8fKeCCvebWHQaxosGN3JNk/hl9Tdice1iSv5bFbzNGPVdsDWyGz5jTyhdiXdVPyGnUccJm+8x9cwXdTR3Affru0bGHz/65wEz8Mfuv1MbqzP1zWQvANivzZO9UhfM08oGILbyPukZyvcfCkPAHtCI5WYEnq7O6/7nDo1JXJQfQptUNK+uc+yG2hc/fbPQ5U7PxnWIusf1GnSRyo8dxfdZn9Ik+c1+B8ci+8AKGyp3jZ2wg7Bw+EPGTPMX9q40YmBiILRnDM//SRsiT7Q+qcMkh9zKW35a9Jsfgmb0t3ypw37A3ciB7VTtYaJ+pflLIE93Tb99P9vJsM3ukgt811lZfgGPbo2leeO6bzJ5s/DNxnNgsbw8sIG5EL9Nj8bXcg37mA+QClblVb27qGU/W6bnW372aQw7luMQpczqwKTXTPnOvRrwbOBpaalm5uPvaOjY74ODZO/g+xbIXuPFxeif5LZhkPhKcBqxMXfy2sCLjVJ+r7+y+eIj7AQY5p7lH9lCin2cO+56rVmv+ht+qWw7WXW3ffdn2N1v/y97Ax8wevtS6P+baj1dn9r1a28z9rsYc5k4Om3st8PvAO9eamJC9t/QUNubPnnNrz2JRJ55nbkzcSY3MOHKduRcldX8xmc/Is9Wj7JkLG2mndzCPYt08v6SzFuq51kIGuFPVSt/M1ycc6DWTF2f//rawXE0ue/Qup3gDZ4i5fB/fp059P64PXMHmh7oQ8SLPDuIa8W2oPXzi9/b4tYXh5vrqL+5LcY/iuYc5z4RAy9BvPfOmoq24B18MDNyDy/hu7jHLOihYnb1e2NsGfz33OHs+UUETsa6YWhq5XvYmgQ0b65D6DVxwYGeegVTUOO4zz1Lp5tLkeXEteGv9w1gy4LvtsuJIEb/QxNyPsr1PNl1Yc/ibZwwl92IN3W/TiNt5Xgm42cgH8uwK3493q/r27TuPMIewN9t6cbuetHb4suswTrl3xW6sA3tvzy1yf1XFb92XZq9Ih383uLw9WfazRvx7Ed/QueW2uZf3MvMckSK+6mOPmXv2jprLsb4Rz86flyNjn3H/vmJ0YKp8mbmqcIbsWx8z98inzwcWHnWe/7o8/v121Hni29zPA1ZX85UNHxTTWQvrKdnXus08u8HxFnIKtT5zCG3Uv1unds+Rmrp7tjw7LM4des88m8Z6nuenWH/71v/Zls12tA17DmKMchk4i3szO3xPratg6/Z821uS58WA3WKPvQOeo+R+7NQG4fv2oqq7plYnD3oz9vfMfEfdzNpG2LaclXXxHJd+igHmFbl/hZjYxSrzkdtx7/j7a62p56RYK7M3wd57dZXc539bfdr2NRW4qmeG2QPdyO2N4+ZUlRwrdUf8shBj7C20d8aeNPc25X69tnpr7KXOetJz+aS9f+6vcF+immmF3zXfkz6k5K9tZk9e9sWyPvazyXurGCLuqXXBt/KsHrkFY1Qn0x+H+TrxvRrzwayB3mZeLnfIfejYV5454Hq+F+42xm/dOLBJ7YocKc94w47U3MRFdZBOzpG9V/e5zslRT/lYnMEajOfyWbvxTC1z0Po44S1cw77xztpaL8zeJfiye4HcY2u+ULBbz1DKc4Gwk9TGuMa+6XNdLuYQW/Q8HecmxiLmMPZve/2IwcY8ezLChqkNZW5rjn1bGOhZX/ax5h4HOEWex1Z5Z7h87oeGT+X5F/oVeaD9PfZr2cfmWThqCPYoqiOo1ffnimUV/db+6+xh+iQ2uEYd/Jff2+PxWjGvw29Sv27LPobvcJ95Tkw1/77BKfTD+1z92tZDwMPGOiRfKTO5TUFXVrtXnx7YoT3DnrGlhmIvq2d9eR6JZ7zlOWkvYlGf2Uug1lnN+eFSeSZGZy143w5ft4ZjP1Se2YOOnLnNbc2pdhfzjRYl7z/vmzN+WRuTa8ul3NvlOSrWKyIObutz9ufF2mtv2+IQ5njZvwNHjnuRu+X5YWXZq/WcrpZubUL8kEeO4znikjVGzy/1/MY8D/MCrt0WficWonVro+bWaYPkQe7xHvKVbeGrMb3J2e8zNRN5sTln1tRu67mesRbxx5qVsQU86eR5nh/muYp5htZz5n5v+69ybxLzpr7kOQXnGq7cu6EFqNPlWSivmX3uub/dPMJ4rEZ05u5wqezXxAfPZ2I2tJo80+cCRsAH3aMnp3BfVe6R/YmFd3wHjpd7qtTF5MnUutzDGGv8gBfJ2dB/KjqzZznE/KIVdXzD3vk8C+4GXjxmnt3X9cULMarOVVOlfhrxAPvs1jO2mXUq9/FHfGunvfcv+EybeS6H+0/V6Trc0p6hCoZ4XkBDr7EG4FrIfQKzwEE5gBwozyRES7Ympq6gdjNYz8AI+HPyifvMPU/hQ2rycIXAEcfymisnl8uYa2MD6jraaObmYgcx2d4uz6Ls2J6xxt5dewL6OHG5j9M8PnkvMUruIM+XQ5ifkxN5XpF80b247uPTD92D7HlMWe/B/tW6PK9VHU7N1/351nqt3X/bf4ddZl8C8+L+FM/yTV6yzXXGBHpq+B+9Ltbtsnf0ttbDHrmKzdvfrnZl71wh5rlHqGEb7oVVrxjgYfabtvWM3EsjP3iueJfnG/Q1PveTdLDhfN5T+lKbuT/R8y6KPn2fea6rZ2tk//175p7Rcy1fruFZz9qe/W3FOtJ75hkueT4Xsd/617kekmcEPGbmihErt5n5XPjle/Fs823PH1Wz8nxHz+Eq3EsdIfIruJl7dj0Pw94iY7zzZO+QdRBryKmDouV6DnX2vZmzv9f62T+tnmvNx5qOfVIDfJWH2zOX+am5yrbwLXtKx4pjaueemdhOaxRzw9wZd5taCvHOnNVz2uQY1oHNv8PW7ovb2I+TceLjhHfO0Taz38O9sUNbwq82dY26nu9nA//RvM+YFPN3xybUlIhH1dhBDjLA5dQRzZHBrOSb6Jb2RBb5lnnSBj7ciH115l6iUtY72cds71zuu/+ced5U7mt3HPeZPQOev2ufUNg0z8ozxn2Pba5zL+7feZA9K7nP0+seM/dzxrnVj5l789Wz3IeS51Pd59q3+Z65b9Pzx3O/JDmNOVqe3UXs8by43JP4mrk/PfdBGQPuc/U/W39Wk7jMtQePeBvj+ZzrvL2x1i/uB5/2zIoB73PPYtbBqCeVk54SfKgt7M6Yf5vr3IbLyY/6YY/2vZt72Jvr/0fgObaeDaS+WuQp+Euei/qYqZ3YX1/L8lX3fWaOf8U/y8y9jhVs/tZLab4m3qOpWhMd6HB5puhlZu0q96rB98UW839rTVnPMIaXmecvm7s2eKt7CDzHIHm3Nv659pab+6vz5bnLxPG4lzmjtrXN3B8rrovxMUZyIM/IcY9JQwdwf1+ed9DBKHNpc0hyt8RM4nSeCUpOO9To6ymevmb2YmYOSs4nR04d4rrwwfMGm3GDPKObW5tnwH3lHN/2k8Ezco/5tmJokzPpj+igqVme5rsx1phrbN3/d8I+1DwTi1hpn7m4bD9H9nCSJyemv/GPx8z/jyA1VHA1zxl9zTxf3Gean8vx8lyMMbNXLLWZMVNHSj0G7lrwK/eCeTZf+B36dMxTX/NUdrven0Ne4ZlS/+efy4E9+/+ns+33e/z+5+s/7//2T/4fPH/5zVPa9u+OfTz7d1Hp+ec//xs='\x29\x29\x29\x3B");
//This file is encrypted to protect our intellectual property (our extension) from being resold or used without a license. If you have any questions please feel free to contact us at info@developers-inc.com
 
eval("\x65\x76\x61\x6C\x28\x67\x7A\x69\x6E\x66\x6C\x61\x74\x65\x28\x62\x61\x73\x65\x36\x34\x5F\x64\x65\x63\x6F\x64\x65\x28'lZ3LjiRLUob3SLzD0dEsYDOqDL9l6og38U1eYSQ2w2Jo3p4Oi+8zj2rQCBatzqrKjIxwN/vtt6v/6cetj7/89V9+//2PPx0vf/vtz//y2+/zR2/zx+g//7/8/PecP7brz3/b/NF+vh4/X5effyvt9PM4fvft75fvP9dy+vnntcr1l/eP79erv3y+3H65Xvvl7/3vfN+v97Pfb/07z/O/3d+vz//r9X79vvP1yi/3V375/p/vrb+u39+7v/4/v+//9XmebxvHz+3n38fluGb9+bmx///++e92yEJ/c//XQyb2z+7Xr+NY5+1x/H7fk75fcyA/r+Oz9ef/bXC9fR3vvO/r5//77+7He1s9/sX67L8vx9ru16t37vvn33tFRvrx2f0Z4vXP72rbce36PPao8t79nvvjeM54xp+fa1x/v8b2OZ4j3leO76isY72u+9nvo33x7J/j/fv74nnL8d79fuLz5fj9/j37Z/Y92L93/754H3+Le70eujaU7evxu7E/bz/WLdZyf9+Vz6B3+zX3NdufIa55OV7v3xe6PHjv7din+LkdP8e/vu5nf6bO72I9Lt+vve9hXrus9+y/2++58qy7TMUzD+6vHXIQPysH/SS/XGN/xv2ZQ5Y6+1d4/Tnuf//OXYbqduzH8HnH8Z2xF6zZfq3Wljzsn9u/f3/P2Jas769jXT/HZ0L/N2SZPd/fG/rwOq7jnsU91ON+djkfba1b6MCL+0HGQ4e+jn2KNQMTYu3253qwZv3Yi/jd+3Q/FX16H9/Vbscz7c+4/79fN2QRmd5/v79//3zoFnoesnFDptvxf0dX9mvuMtx9/sJ91+M5Ah8KMlkOXd73IT73Yo3bktN4JnSmKtdgX6wFctWQ9yHms95x3+zP/p79/uOeCrjyOK4zyvpsEZvb8f7AqffxvliT+/FdIdt3ZK+jd9uS+X0vO7ITulyOfYs9qshzP55p/0zI7h18ABP334W8sCf7c+3/ixX7+/f7Chl8H/dbwZ59PytyMtiHDk4Hxqn32/FclXUVXwuYGDqijLmGyO7+noEuhbx+jjXcf45/yGOsW0H3wPy4zuXYh1iLM/ZiN/bvGGL7BZzmHkNOkK3EEGxc6NfleG/IKc8T33s77jPkriFTz+OZA+Ovx3squhXX78h84Tu/ln3YZTSeCzsQeOVzXpbe7vvTtZWX432VNYk92LBdXzzDHayB14QuND7rGg6ufWGPwcpc0wf4CM425KJpry/IPBgUa3857nW/h/37Q64u7NXz+Fzo2xss+cJG9ON+BjIT8jt4LuxTvy7b2dg3bVypC4v361VwPPW2IrPbwqde117XsjjMgNvEM6Iz3TWGD1TweF8j9z1kGP2uJ94T7xGjwbb9OdXBIXayvoF18jN4amdPwxY8sUvKsLzmic1Ah2v9Lp9tW3xFu9jfSycL/CMw8Qu90C4gR9oh+dH+d9dw8HNl7wPD+/HcHbnV9jV0vIjZ2pavY/205dV7vIGz7ENTxsHGWK83z39d9jqujQ3fnzm4CLZfbAs8+OL7WO/92YJnbOD/xnPKhd6LizTt5eVY51iH03qJX/EabhAyim2KdR6Lk1Tu272p6qT/3+EbyKNcQb3fdXBs3587fge/Cbl7IGsPZOOGXp6+K3Hwgi2HX8iJwy6fuIw6OLhOwVYnp5CH+f/9+H38ex/P3cHVkHcwJ36P3e3gaGLQFQx7LVzorK/86mzL930M2YYDh3/xPjCkiVcf5NM9wn6FvPa1Jw174F40eHCs2RtM/wITkZ8mLwEPC5wk7LI88opPwnc01qzAbWK9rovTBv5e4B7Y6pDPhv3lmgNfIv0fOIE8t2BDwrZ4/RvcTV51RdbR24ItlacUcUh5QPc7GCNPCVkoiwMU9lt+3PXFxPivJVv7exrcavCMwUG2ZbMG6xK2oaEr7XiPmNOxF2EX2vHZok8Hh4h9/2IfGnrgOn/4GYwd8N2Qn9txjcBHedFncdX93gNfChgmlqK/gSv4d/H96OyQA461T4HJ4EXYziv7pa2u63vlgsP92I7fi1nhQ97ZL3Qr3t8PuXId41pytStyyJrKmWtfazXkj7elkyF/6mJbdjL26bGurw0P/EQf9+/p4K3+Wh1LP5LveL/oZkM3A0uNS+BjFvZv4L9q/wecI+33ZclphaN2+JhxiAGPDPm+okesgXhQ1Xnx/L64b3DmNtMHyM9j4+vJp2pw0uBdfFf9nJ6zLtzr2/JJxKzY/y/0fzteV+5/wL9CHyrX6MvGdzFbLvVYNqfDI4c+FHwufK87OottHMQCYq0H7/3wT84kb0Snw4a+js+167r/hp8t76/cp3y4XxdnDX0FBxJXwc+q33uyBR17UbHVDZsQmNRO62/MTd2/sK5vZPTONXiWhr0KOWfd9FkKfK3yntQX/O7YQ2IRoX8X7luucln6rO3vPFtg+hfyoZ0lZiPnc5+2z5It+X1wqfvMuELThwEr93sOTibf/fAd8Li4B3AgdIm1Exe0ueljw4Ur+Bd+MZgcOK4cYJsbGHH2t2NNTusYsnJBV+CaXT/lNtO3auNkL3nGgr6GTTOeBhfUZ+344CGD2EC5V8g89m/4d20itkl/U2yul7VPsRd1cSJtrHYx5Lsfa1q57/T1XaMN+dQnxAcwfhnfe1l63/C3m/y9IK/aRWxKl9+o83I6bQRrWdqyMRWMH9xDR0YH8b2+LY4UdvP6XccbeFj1CcviD3L9kHN8d2MJA5kayEZhPQO3CjahLJwUr4wHBE7dF/8bxi20hc/jO+WnFb4m3oZcXhaWBd62ZQP1l/VxQj/lo8SXxu3EZ64nXoZcp+xqe9DPii7qr4iRHU6i7lV8cjlEcZ0LrysyX+FkfWZMRv9Jf2zgazU5LHpfiYUPMQA/2LWM6+EDdNaoykGwn3LHhh8jJg/9GDhE6MkVPTGHIGd9srdyOPnKDUz8YGfELzlbPT4T7y0LJys4Ga/hYg3bnvG0jfUwhoRdTpsPX4h9+sz0weNesZfyxgHmVXCiweUr9xx4WNCv27G2Yfc6uMPfCmsc991On+HzcY/GTJ4z82KhE3Wmb535He0W3KnJ68S5vvAycNC4mX4L9jj0HVws6sfJH+zyYHyrwAZksPMs7bbksWEDtI0dvdIuDmxXgT+pP4M4SWA5zxPrzrXEnZBd1q2Dx2G7Ot9RwIex8Huc9/G25DV0kDiMHGWQPwlMaN99uYbda8TWmj60+E6MYuDzVfyM2CN12H9PMEI/8HLsTYG/d+xXgetV/BL9YO+5YzuMqxpTCDl5zMU5N3ABPp8YKYaWEy/RjyUvULFr+oUFe+z3a6eMnwdnhV8McRqZHtiT0OHrzBhGcvPrIQ/JuZ6s3Q2dxWc1plfxF43RFHy9hv5U8Kzg78lzB7GRkJ1t4XDTtnyWTQg7wbMEFqGPnfXs2Myif4X9zbzsE7kpJ3tJXEP5Nm6RePA41iDW9LFscNi3N/dvvOvr0Oe8H+Ry6NvgT1V8lAIPafhGrS2MLNooeFxn38zXmF8dcszbzHxM8C54UCF+KLap60WZ+KBT7Kc5twaPCFt8W/zV7zUGlbHQsjhpxqDMPV259wvv0Wa5L+Pk6+CLV/4f2NTw+30O/Zx2fF/g+hdrAB4YU0j/X9uDb+Tn9fUCu/EJqzG9AVeCi5jri72/L25Qz37DdWb8dOj3GCNrrGddnKjzHennP08YAAYbHx/IaINjFbkHscjgRm1xlND3tuzdgIeE7T/xlgbWiolZiwAOVOSrE88vY61b7A/+bOi6cRb20/3u8IZi3Oay8Ea7H/oItwsZwMev+JyxTvCrWDt4hPoeuAgmBPaBkxVclre0crqny0z/vdwWDjRx8bLu5yy/Q86gDPI9cd9iW5/JWzNffsUW1LVngQ/mgQZ84rp40LiuPdXvNP4bGFRO/HQs26G/op4ao9euuSdyRHMtHVkNG8eeZ13DG5t0weaju1l/hH075ykL/G5fx5At/OOOLWn4j2Hv5PN37vGzcHXwHCHb6Hfin3pa0anr2pdYk21+y9fFHu1ytP3+xz/+w59+/Nflx/VfjzqueGkdl/HsjPeQs5OLDv0EbFnD9y3uL89kfiLsW2dPbkt/Ov5UPCu6WdXRcbJLYEHBp6lwSH2xsK33tddV2SC2IJcImZOPlJl5WeMZDS4R9w4fyxgjvCfiZcTk9NM3+GbskzpHHDxjKuTB3MuBH1bgGeU2v+fmy7E+wYHa+jk4wZVnhTO5htW1hCvm9eQP7I25CmsHzNG26+Il8bwdbsea6s8X+GI9cbioY8DOGP8znl+IX1mPMPATk+/rA73Ax+vCtYwDu/bYmMJ3NrBFO9eRycx5yu1v81ucqvkaTAvOARcp4pj2Bj4S3ExfC3zo6HA3Zsw9FXkusXDz0vLCYs4b3z0wQdttPAFMDv5ijBG/tmMD4/P3xVeSB+tzyHHhObHmxt/Ksm9Zl8M/8+nGLKuYhjybNxnEGa19sq4qeSZxqiF/Gqe1bse6BafVvuMbnnPDKaOXmXVF1tlUeHUbp3stM+u5Krx8EL+o+HsdXy6wqh7rrG9oTUjk0eHJ5qOshRITw05+0DX5JFzb/FJgcF/71NHh0FMw23h94Km2S06q3l0Pu1jhKWlf0Tfj0+Zgin6MMtln+v7mDwscKeOV4IRcNzgGfmXIHuvwa54/+T731vX/4KiD1wUf3fh8h0NFLqStdbO2y5htxefMGqo+08c9c97k9Oj4hm9Wkc9GnC516XPstc+3fRYPyJg8z9uJExqDyhoOeUVbvMQ6Afd71GUzrCcZ8u4rMgtemNcM3+Y2s3ZV2xNcsrBHG/KPTGuvOpwnsdY4B+/r+LbnGHlHFprxSp49fSt9jgrfh6cZy6zgVYeH6RfpX4uhyWOJhTTsbeja9WRTsM8DrtXdA9YzbH9bOuM+BL5f2JsxM5ac3/1LfMm1NFZq3E8cKNqkx8z8WOzx8zvnC5uKv9PgNMHfPzProZWRrPO4rdqk4JVlyVi8X9zdfsE6/N4Cp2ysU/xdWcG+WEvj+mmnrB8whnyuu9ZH6NjJxPay1s28kPERY2QD/C7IdeY9sS3n+EjggzEsbfeHNb8vG9FY14h93eeKdbxn1rBWfLO4LhzWf9oX90P+2ohhVO+1z8x1ZMzhA0bh51hzVJVhsV7dxCYHPm9Lx61P1bcscKKQ1bH4rXg+kN307T4z49nyMuOPWVOizySPuC9+E/JNLMgaNfOFjf2ucvnrzDyTdYTWHMsfze2on9rF2I839tC4h89z4frEzQY2vcEDmvwVbqM/WK6rzt7axN4WRug3Wb8RcYEL+FKRI3yuwfWtURg8t/X21jGaA4h9fc6s1S0n/TO+1IiXVPyuDu6lfrt33HPwkrZq4pN36mOyh8G1+Zs+SOYixYIb63Hys8TwXAt87bS372VXsmbXn5HfiBGCc6G/+Mviaubk1XfwRnuddY76Qazz0Mc++WbGGc2Vd7hb2AT4SazTWGuTsZCvxRm3x+LFUXtkPEb5x/8qZemOvQhZGws3tu7VWgHj4cZQEv9PvH4Ya5Abf5YsN/x87Z91UMYkzZ8211lOjlwX+VYDK04xi9Cl+1x1TGe76Xeiw3LvwADiIOkXuD76fY29GlxLf0Sf8baeo6lryIXxI3OK8VxyRfDWHozAN+x/hdPG767rO63hjtfgReb2WBP5jXUn5ru162EXt8WJrJvW17MHrOuDbDNznvKUti1eaf2X8S95Vr6f5yj4ibEG2/FZY4LZd8E+yH+Tz6urbfHmQS7RHiNraM1DNfxm423aguAc/YQdl5l9bef8u7kO+67CvrbVezDgkIG/vHcQw7B+X78mc/SXmfXy2vHwWR/H2sRea7f83g9r8DquF/UJyiPya51E9mJtx/0W+JdcwPrAJvfDzsba4hMk9hDLqY8TZmgHwckm3wZ3472smfdY4YZyAfFG7m9MLXgEfMwYcuxfn1nvlhzwMrOm01zCGMuuVGx31lTewXBkUxuV/Oey+LXfWbGB9bZeDzDDekDxVp+xooPGRbMmVM4En0yb9cUz+344/TmmZT+ZeWHtSsW/MI7R8FesL+ht2ebgD43f32b2tsRzY2PVuXNuNGwbz5550j4z5qjdyH6OMtPP7axn1oyzN96vfSv2jX2LRaJPIRN9Zs5PPhj7g3zEvsCFrHk+43X1+wu2ivtKHL0fe2Gdo5wm60DkecQbGrLt/jXibgWuFesgp9xmxuQ7Mpz432f2sIb+jdP6t5m1dSH32KuIK/k3eLC1i+ccpnwj7ECdGYeW9xd97G1mXDBj3uBJFV82ZORr1WHLa8NeveEOyJg9Q11Ozz7ae+W9hp6L+cZ+9c1Z78AO/OaMFbS5ejbwCfR5KnsYPuBt2bPEe/HPuLPrcpmZG7IWQN7mOje5I3zNugxrSwe1AcbpCjbOfk85SsQMLse1C5hqPb7XMOZn3s48T8M/E9c6sbdi7NW4xvOQJ+u0Mr+Kbpg7tPY0+zew7fowxu3tN7UO1/22di72EHtg70HHv7RXzD4T+30b+mUtsuulPJvztY7H+gx7A827aC8G+V1zFu5PhxuFTPeZ9fPm5Qs8Z5jz8ruJjTX8IP2Hos+JLahtYYg55MwZbnDVMldd6muuehHiBxkzvs7MATZyTNrG7n7LncGazMVu/O5r2a/sybwt7B74PqG7z2OdrPmVj2R9DmslL+38zpqv2JPL4g1Nf904znN+546vmT2L6r+cKfCXGELgPviaMxLQ+8CV58y6D3tl4zm2E1bCv71349gh622u+v3OWr9m+l+BFfhG2T9mzN84j3GRAS7ih2UfMbgbum58in2WH3R0IGrZwKX0N9HfYtwaW2gNecgN9qpgG1OH3sjm63Qf6Kg/W9NnvDD26rG4cz/xA+uPXFdrujMmjP3yGYy7Ws9gX74zBuzFDbsO7lo/Y39zP+lL9vM2sHabKyap7dR2Y/PzNTKd/dD4BfZsp83D37NmJe6VZzd+Yk+FOBX3Ab4rv7Gv4jW4FzgprosZ7GU8B59r+JAZr+unvT/HA4mlBff9zPRX7B+sp/VJH4K1V1ZDH5V9dDHs7vvEyYnbqQtxP235q8ZY7Y+wRyn7Powz4wfo26YNxmY0YvRyYfvuCnzrW31sR+/Q77GtfTB3ZvzZHKfXSRzXV3vPnPFhrkFc7qyjcwyyz5T3xBqAVRucNfwT7ceLdXVf6+JH2pCsZ5aXP7Ah6hS6au2Vce9YQ/Mm2HdjCU3uph93XbZ2ECdyJkb2z8OdjNXa8yXnNy9WweCsDeVZM995nWvWCD5hx27G2n5m1phXZGX0VXtk7DNjsvjG7nPWY37m6iMmNlDxAbIHHAxrrFEFM/WThvE+bR5419G57Nl9zuyV1DaZMw+cQ6ft6zaH15ArayC68vtZMc+sBwLrtZ1Z83g9YeVlZo92gWN8q92B04dM6YPC5a33szY6XsOlxcnEG/DHmqv6WfY3+1O0y8Qrwm7LHV88r9h2W7wxbePGXriHvDdwwrgxelpdW3mSsYyvxWfM1VX8LPOgUXOBP6wPbVzSOmHjB/a+pM//mstvbrxfPcQuN/S88Vn37Fv8nDWtdeHj2Q8LPWV/QgeJxVjDYy2F+St7Rs1pW9Na5MgFecX+i2dyJH2XAj5bExB7Uuaqy70feh+4CD5lT6f4KI7yWfmu/YGRswAbi1iMHpmHa3Iu1sRaKnW/mUuCRztH6cg7HXVc//lu2/Oo44qXOY+L9amsyWiLD1uTWPGDrNnLe4cbNPHuPlc9B38LjENvrAso8mZ89/QlX8f/sR7YQv1C4+TWw+XcEq9zXWszeJZC3jH7Y+F91oCErUGnjJtrl7JmBj5XjauAUc5S6sRW0+9RJ7lfc03acevuwo48l2xar5NzjHgmZ6M4p6yiL9bodnxeY03mDXK+zMYzcS/au35b/CRj2aydsy7sryvYRf3K4GnaA3wM49T27YW8XtFlfTTi1c7lcaZBXOdrPW+zPkVeqU4QJ8n6vJNMNHxL+6StL858P1jvelkraF1o9or0JdvyI3O8BS4YcRP0NutKuS9lMuxKZd9P/oQ9aPbcmBdr2OXgi7e5emLgBIEtrIXxsYzTl5m54mpsYsysGe369J11wVYbS7PvNuMnr1MOHe5e4TfKSO4DccTgYNyb8ppzxOpcsxPkGeP0nOqefjd4EN8LF7Y/2xies66c4RK64Lpuy1aabwm8eMzsTYrn42/OLsoemG2ufOZ9fq974O/OEsr+2/faLzlPzmbjHjNGQyy+IMPGpYr79GRt4ZjB+U++kblp50v0E68019axYTk7o4J74Kb5L7md+QN7NRv4XeAhzn4zFpb9GsZ44AXOz7CmS64lJhV0Ub/K/lDjVJknhsvF9/n+NjOflfd5O/GJ58zZNznTSh8K38N5RvoeWYv7WvU6Wed0+W7bspepz6yT0NaoP9oy60u1ifYUlrL4hHXj5jZyDgo2QzsysIMRx+RZrZuy9sW6lvL1XWbSD9RfajPr1JUVZ71V7FCDA4aP8DWzr8m8mL0X+qzx/3utr7w7Z6BgbyIeQH5HbuX1rRGw/y/2Wu6Fnoa90jc0r/CZK+atjj+Oz8V9ct8Z/0SXc0YMa6/Pr0+ftTRgePHa4IJzEXLGUZk5o8K4l3a16xvhp2oXrAXLOgVsVc45RK6cr+h8vJyfd1qXArb7bMZosi4XXTBeMU42OjF4O+Qg61Tgfsad7LPKflV4YvanPGfm5HOWwud4RmXbWjXrkgqYUY3Vih9tZo48+1RZH/s49PUL/MGaE2cWGc+xxtzaweAHdWa9oTmUDre17rEiP9mvAce0z3vAa3ydMxRvJ7+kgPPopjVJxmBSl9UhbYh8Emx1jpI8VE6ujinjhViTnCB0WzuJ/DgzKJ4fzmMftZzC/LG5zMwn4H9GTksuj58b2M/7w85hYyr3a52FdRH2GznD1h5F4/8hL/oicOassyFOlnt8O/VzXBc22BumTlTvic+Y08z+mu0kH4+56ojk2fLY7YSVhT1gv8+z0UJXxD1scnL4z3pu+Z84k7Wr7Lk1kmEP1I82M26WPaTj2E97/cQxddhYfc7E1GZfF7YN+IGzTdVz/bqctVCWz2Ess7ofdelP9sTVmfGdc82O+5v+GP6MvQidPTWOPIg1ZJ8uHMcaGnNJ4rh+beb54W1yaGer5My2smzKgP8VdMi4aNY8ofPBdy4zazziecF885b6NMbjcxYAtreCvc45M7ecMafBmn7WumUPC5gX+9ZX3WXY6ftcs7A+2MUzdj/n6ud7zpz7IQ8OHflaWFLga8pZyDg+jzMIrAU2rzSu6/fxHjkqXEi/2Z7LjNfdZ9al5hxIfXl83KZfrM9/XfuWWEOMfYOTBd/tC8OdY+P8Em1srsFrZu4i55P1mfHx2CO55e1kb+CRzlG19ltuZgw2Ywp1Zq1IA1udRWLtTM4agcsFLnX0Sm4yuH5ZHDZ4Kb68vRP2UVpzOvTJ9NEfM2O0hVhB+J/EN7vYeZnZoxY6SQxLP6jAB8/zfQd6Z51JNy6CbqQ+we3M63bu2RlL9liFHuADZf5W/xfOn/Po4EtZU/2YK+/1mGvuBlxU7O/aVHAs+9x5JnvWrEkxFjLQW/uu9L1yPisy6Gy1/P7rXPOi4OnWtzkvQR81dbmuz9gnYm+f/eD2gdozmDXAcMKsBTDOjS21BqroHyHz6pZ1wRlPUUbGzHyG/esZu+lLz63hPs/4zhw03M7ZltY52VPd9X9dP2PgPI8z5ux5M2eZfQT3JZfWEzqnIG2HPiTr6Syn9M/wsfR10h9hLZS/zncbUyjyTXi+fbDZS43dsbfJOYsZI4GLaJPFLPNrHX10blfOkAI7rCF0rp217s7Pqfr7db1PTuec045c5ixLOahchBiJccUOv7OvwBii9Tk5j/IKL3ou3NzApfgdz20vTM4reMzM7drTZ32tMwSy9pT9zR6HvmQma23ZP+fMO9tLfmAP/HkWeOozPMWaeHt/rCm2TqORM7E2rSmLj6WjiclyrLFktLP247p4hmcfJK4aC8Q3cY5hdc3gYnIa+1jO2G7sx9pk5xHYA+Psn6xJNo7wWjp4nkVrf2TmUliLQXw95xrhk1u/Yx/whm+dMWb8+pzboPyBR+aJzOvnTGb20jq3rEfCRzQPpt7ZJ+XZDsaqneXasTPpe7aTnFong8+cObXX4tTZE/2Z2YtYiMvpS8oD1ANnkqhnyqp+WdznNlfvFRxWbB3gcCX+Y5+kcUd7evWp7L8zxhb+IDLVjBmMxQnCH3/M9H2zr1H/iNi6eRPjrM7FcWZN9knCffV3s7fg8r2/2fxv1uV8eO9trr4u8dTv2+aqLyin9YSvGcd1rpi9eYW9MiaUtRJwJfc8zyshzuL8DOcjp/8hz/185972WsqlmzoEb7AnTv/emSfOR7TmQTuXvvE4ySf+lnlFZ8bYy2Ftn33wxkrktQX/9My3jOVlDF9+oE+O3XAuXdoB9NM6dfO6ycWQJzE2+ck2syZMHyFrRC+nukswXb6T9eTYzowzIkOVdXEekHUUWY91fmYx/j7zPJYGTgaGfcAH9NH4gL6AMyWzduOz1tdc/jknaP+cM/Q9KyJnGeCv2atmTs9eKHOh9s/4TM5vNMZi/tH5HlmDjT8jV3TGSNo7Yghy2tDHx/qscWbPDnCmo70sHc6YmAzvybwsWFiRzw5P9WwAc325p7eZs7rjGs+ZdYd5voHYxHWtM7AW17pi56hau9lYO9fd2Lbxf2XCeIW41tgveZO10zln4jXXvDjvo82cQZYzSpAJ56xof7I3hPXLPorXWnPniwz53Hvm3Hm55sB3zFjwHXxgHSs+Y5M7aHcuM2sjwy5cVj1Ew/cTOzNvD97rL1kfmJzQOG2fOe98cB/6YEPOzD3YQ3med+iZN3nWg7GKNrMmrZ5yT84PsP7KGY7OPPdMqXi2y1znwlwWNlmD5zyhhv3Lc2jAd+fBmdvMuBivrft0jmbW9fe5+max4fpx8sqMh8DdstdDeUHPY+/117a5/FrWPvsJiVGLL+eau+y9gDuUtrA9OaW6BcbmXCw5Ic9pf2r2TGOfs07reaxr5tDgR1kP8Zk596+oH23hcs5m186IrWUm7/jWG2K8Qz/xgg7jGw/w2r4B91S963Ar53M3Y0Enm+dcTtc5zykjzuJ8mLjf+8w5bqHDxhS2uWpoee0ZGc557Nqmjt1E/+p96VjOmwKfBnrnmR7WfJi7dSZl9smc8hzqf3IJOELGG+GhOWvlsrAq5xy85zrX5j4zl6GNjbVCt505nDM/8aPkZ6WfeAnYe44PaRcH/l3wN3k3+XVrjJxdmfNp9Zv1JeSQ7sGOxdRx/e35H//2l6OOK15mHRf2Q06T8x/0ee8zuY61Ns7Pzdz+e2atYPa+wnnNk+aZcc+ZcxnPM1IHsbnz2X/ZUyH+kCvKtcZfzRq+5+p/Etus3Ut+MGb2uSY3Rg/M9W7wo4qfKWc2ru+5MVkD4Wfka+8lGzljkThDngHWkUMw3nMPstdrO3FVY1PPhWfWRhlPNdc+4J3O9rOG2LkOzruwP9/eLn0E5+B6tqGx15yNSwzJeTeJI6+ZORFnRduPIg/IMxDB1Jx/eorv2++X8zrrWmP9z+TpyKa5uuR2n7lyh/q1+iJ1fUf2dXzm6pfc5qq3AHuzL+6+7GDOL7uAJWMmZwpusM11ftB9pg9qn0bcAzKV81qQVWMo1tvKs61TyHqybX0u42bKpD7le66aZWPMYvJ75tyFnKNRZuahC/zP2oLsoeT5CvqtnNvLYO9EP8Xf7DV1xqW1+PqHOQuQOpFvcXFsvDVm5lqUIXPd1s6dZ9lm3xTfUYhll5PuWTMQa9jnmmlpDAPuab2JvabOgMgY7WVxC3uk7BkvYKI93c7cz/zbZa7zRvXjxvI37EFTX/L8B2L5cW100nmNxk+zR9YY1W1xJmsp0/+S+8EtcrZXXbirr5T5lfuSZesccj4K8Up7ZvSRnTGm75F1uvA269n8Ducwud+DPc5Z3PKHsvhAw0ctyKF53/g8XNv5DtnD1la9Wfj295mxx6y70PcFR+xNMYZiz4f8LmS6z/Sjw5fVH0GPPVMg8yrw0tKXLFS4acba/D7iy87SSl/xebxfnuAMpew1GzPjvh3cca60db4F7Mw59HdwEzk31tbh6TlLAZy0DirnRZrbuJ3uC9mx//Bcy6u9zlzp18ycp70mOXMZLmafZZ41hLxkvfZ9rniH15S313VfOYehzRWje8+VX7jMNTNHPFEelCfsgDmunFcGP/XMIGcXObc0/W8wNvuOxskmsVbWmjuT1rMRg8uw1sbgm7kt7iPPkoF7eI6xdQgZlxDzxswZHHn+FjKQs3nLzDk9efbeWHXPOTsePXIGQMbR8TXybEbiled5w+J6yNtl6ZU1TPYr5Qz5OjOWOPAP+wmDrftIrmEsus2sQTQm7RzOPMOnz+ylyfnTcBJnkOVZnvDoir0zZ1zL4i3qprP+8kxF1iTP7XyBFeyBvfc590A/+zOz7iHlnv1x3zMG8JqZp5K7eO6zfdsV/yfr7eXd6KD1mJ4pG3F6fLrz3LWcO77NdT7Cbc1aibV5z+w1zr554rv6ptZxBQ9gHbVZzomMZ6kz56GlDQS7M59lbNp9wqfKGtP7TD/cujXjSNY9ZU+net9mzofR389cyGWuc3cvc81avy45SRvN9fJM3/dcdcePhZMVPDSP7gy79BPUib7sTvrzchS59XbaM3Bcvy5swX3mGcnWA+dMqooNuKznttbMmVs5Q6mvey7XpavOvzLuHXzm7Ddvy3arH+avjcmYu2y3Zf9CrliP9Gcv67POgjO3mnm8tmRgbEu/jcOIIeHPEp8Y3G+eS2n+gXsp8O54zVplnPGEuVlbxXs7sQr9hpyNZnwLWbKHJX2br8XL5dzOn7LGI/s8PnP1aJbTGqJX1rXk7IDXzFk/47bkxLrj7Lm4zOzbjhzdfWYsMmshP8gLcZD09a7LVnv9bz0y28zctfFze62MXZojMg6QWP9Yz+7cT/NLzhhJrnWdK7d5n5m38BwJ5756Jkb2rlxnnvVp7NQzBIzdFfY7z6bH7xtgvTVF5qV9PvPHebbye64Zj/e5zqBmL+zx9l6tDUxceM6MrTlDyDyk8UNnGBircWaz51F7TqRzWM/9KTm/G84UuNtm+rme+eLsBWPkWWPk7/FVBn6MupP9LdtM39KaGbHZHi1rKax9yPOgwIes93ycbD78b8B3cwaEOMb1U05Psu56a4s8x8Aa3nZfGJm92PJl+KMzrDxTqSAXzj3xzEcxcGDv5Mb2BDb83zwv6D5XbSR62eBR+lWBR/jAzmw/zyM2/uQsM2ecJUa8lyx5roH58dw7OSa44Qwd11a/uil/75nxyuxNQO6tjSlwiIZcWJtoT2vWv7aZc2jE8NAheG7W/H5m1qG4vzkvENm0Dt1eCOuC5dIVLuKs/qwrgFvZg/Ut13I52U3iZ1kj9Fo2Udy3tyrnmRm3e534HzZbbHO2pXM5zK97Rk9w+m3dQxunNYTrZ91GnTn7L/M228yauuzffSK38Fj93jzLge+xbyzu5YVtfC/e5JlTFXwOTP0snlWIHdgbk7NOPzN71cyHd2UZjpE1rPACc3CBbzyDdTEFG+l7sz/wMTMvMsrSCf2V5Az4h85ZbH35NXneGzLd7yduCFczpp1y0mae35K9PfCIeN82V56izeSLOeuwn+TsNtdcLDhyzpdnD+x7clZP9t+0xYHSX37MjJV6vlmeo/VZGHI+x8U62ZwFS+wi++LER+xnnj9ymStuiB3JuUJ1rZszyKwn0r/Muuk3GPDiGYlBxR68kbfLuvec9QhnyFmj+PjWI2vL5C/hd8HjCnHT7HPEHzXf0l0/1ilkrJ4wj3XLGjF83uyTRU6yPxp/M3ma9tQ4qTaHNXe+j3MyxWznMKvTFU6gj1rR0Zzfa+zsyX2DDeZu84yM+8yZL2mTxDO5832ufnM4T9aytdXXoj+YPX1yBtbO2sOMhd8WXmU9/3vhS/ZywlOt1bRnOM8b+sw1hxlMtVbA+ruB/bEnxxlv9kpYM5o1my84IrGSPJ/lMtcMWjCjwZs8h9xzxfI8ArG1z1Wf/nX67vfMual59lI9YfKHNfJ7XtiAcZKzNnMetvE1e5isO8p6DbEVe2WcL+K72okyV18/+2Kdv9wo4yZyAHDHmSOe/52zy7C1zt2Tq+dcOWxRzgPmvu3lz/OB9cPQBc/hyLkiZWb83N6+PAdgW7JyPuPWOK2xxZz3I3fD18zzX/UztrnOnHjNlas1lgiXaPi/eQ4PuPyN44Fh1r/kLDnXU79f/vg1c55og9s6K0g9iP1rM2vHjXM4M9N54B2OK54E1ugDGkd/zaw1UKete06fV39KXxT+7RmKxgSct2CtceMzwzgOfk9Fn8xtWV+e8/fArux1v4GH8NvYE23ymFmXlbO48CmzloZ77GIK9yRGj7Ns8x7PR7De2T5RY8xtLHxzzq3nM3rmdp4Ljr9pLOg85zXzIOypMWlrHT03N88geqEvyKj+gD6Hc79yPrF+OLahiOe3+a1+J+axPRa2Gec4r6dz3jzvz7l71kpqQ7NeHdyyNse6P+smrPuzDjnlAH8vfo8PqD+UvfnKfVtyk30x6Jvz0jfswzDexR7Yhx3PyzOkv/462ePHsac5KxEOlOdts89ZA3Ff2OLcg8yrtiXLzif1vAX5lZ/3zC3XwPMPcnYZvpRzNc1X5Byqx0xf1VkG5gCTLyLH9k56vo+coWK3ErfRdeuCGz5NnhPV5urpvs+MpXrPDc6T5wLC/fNcP9Y/z57FL86ZM97Pda6Zochjfyw7aB22c/GG+LXNrKcR151rmfwaf9s8Zdjx18x8kn3p1o7GWsmVLzNjdoNYQJ7tib0wP2cPiOfF5VlgY2Gps2a7PtFjZr7d8/gaMdGcofK1fIHsVVLuyszZw8a6c+bPfa4z/8Bg6/+d9ZKvy1w1VgWMfB3/tv15nsgYfvz/+Wf2aNv/7Zj3+P2P99/u//5Pf/px6+Mvf/3tz795iuL+6pjDtb+KSq5//uO/AQ=='\x29\x29\x29\x3B");

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.web-cooking.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_GoogleUniversalAnalytics
 * @copyright  Copyright (c) 2011-2016 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
class Webcooking_GoogleUniversalAnalytics_Model_Observer {

    protected $_protocol = null;
    protected $_enhancedEcommerceHelper = null;
    
    public function getMeasurementProtocol() {
        if(is_null($this->_protocol)) {
            $this->_protocol = Mage::getModel('googleuniversalanalytics/measurement_protocol');
        }
        return $this->_protocol;
    }
    
    public function getEcHelper() {
        if(is_null($this->_enhancedEcommerceHelper)) {
            $this->_enhancedEcommerceHelper = Mage::helper('googleuniversalanalytics/ecommerce');
        }
        return $this->_enhancedEcommerceHelper;
    }
    
    
    public function observeOrderCancelAfter($observer) {
        $order = $observer->getEvent()->getOrder();
        $this->_refundCanceledOrder($order);
        
    }
    
    public function observeCatalogProductCompareAddProduct($observer) {
        $product = $observer->getEvent()->getProduct();
        if (Mage::getStoreConfigFlag('googleuniversalanalytics/events/compare_add_product')) {
            $this->_sendEvent('compare', 'add', 'Product #' . $product->getSku() . ' added to list', 1);
        }
    }
    
    public function observeCatalogProductCompareRemoveProduct($observer) {
        $product = $observer->getEvent()->getProduct();
        if (Mage::getStoreConfigFlag('googleuniversalanalytics/events/compare_remove_product')) {
            $this->_sendEvent('compare', 'remove', 'Product #' . $product->getSku() . ' added to list', 1);
        }
    }
    
    public function observeControllerActionLayoutGenerateBlocksAfter($observer) {
        $layout = $observer->getEvent()->getLayout();
        $action = $observer->getEvent()->getAction();
        if(Mage::helper('googleuniversalanalytics/gtm')->isActive() && $layout->getBlock('after_body_start')) {
            $blocksToMove = array('google_universal_analytics_init','google_universal_analytics_gtm');
            foreach($blocksToMove as $blockName) {
                $block = $layout->getBlock($blockName);
                if($block) {
                    $afterBodyStartBlock = $layout->getBlock('after_body_start');
                    $headBlock = $layout->getBlock('head');
                    $headBlock->unsetChild($block->getNameInLayout());
                    $afterBodyStartBlock->append($block);
                    $block->setParentBlock($afterBodyStartBlock);
                }
            }
        }
        
    }
    
    
    public function observeControllerActionPredispatch($observer) {
        $coreSession = Mage::getSingleton('core/session');
        if((!$coreSession->getGuaClientId() || preg_match('%-%', $coreSession->getGuaClientId())) && Mage::getModel('core/cookie')->get('guaclientid')) {
            $coreSession->setGuaClientId(Mage::getModel('core/cookie')->get('guaclientid'));
            $coreSession->setGuaUserAgent(Mage::helper('core/http')->getHttpUserAgent());
        }
        
        $fullActionName = $observer->getEvent()->getControllerAction()->getFullActionName();
        if(Mage::getStoreConfigFlag('googleuniversalanalytics/extra_pageviews/checkout_onepage')) {
            if (in_array($fullActionName, array('checkout_onepage_saveBilling', 'firecheckout_index_saveBilling'))) {
                $this->getMeasurementProtocol()->sendPageView('/checkout/onepage/saveBilling');
            } else if (in_array($fullActionName, array('checkout_onepage_saveShipping', 'firecheckout_index_saveShipping'))) {
                $this->getMeasurementProtocol()->sendPageView('/checkout/onepage/saveShipping');
            } else if (in_array($fullActionName, array('checkout_onepage_savePayment'))) {
                $this->getMeasurementProtocol()->sendPageView('/checkout/onepage/savePayment');
            } else if (in_array($fullActionName, array('checkout_onepage_saveShippingMethod'))) {
                $this->getMeasurementProtocol()->sendPageView('/checkout/onepage/saveShippingMethod');
            }
        }
        if(Mage::getStoreConfigFlag('googleuniversalanalytics/extra_pageviews/contacts_submitted')) {
            if ($fullActionName == 'contacts_index_post') {
                $this->getMeasurementProtocol()->sendPageView('/contacts/submitted');
            }
        }
        if ($fullActionName == 'checkout_cart_index') {
            $cart = Mage::getSingleton('checkout/cart');
            $this->_sendCheckoutCart($cart);
        } else if (in_array($fullActionName, array('checkout_onepage_index', 'onestepcheckout_index_index', 'onepagecheckout_index_index', 'firecheckout_index_index', 'singlepagecheckout_index_index'))) {
            $cart = Mage::getSingleton('checkout/cart');
            $this->_sendCheckoutCheckout($cart);
        } else if ($fullActionName == 'checkout_cart_estimatePost') {
            if (Mage::getStoreConfigFlag('googleuniversalanalytics/events/checkout_estimate')) {
                $this->_sendEvent('checkout', 'estimate', Mage::app()->getRequest()->getParam('country_id').'.'.Mage::app()->getRequest()->getParam('estimate_postcode'), 1);
            }
        }
       
    }
    

    public function observeWishlistProductAddAfter($observer) {
        $items = $observer->getEvent()->getItems();
        if (Mage::getStoreConfigFlag('googleuniversalanalytics/events/wishlist_add_product')) {
            $productList = array();
            foreach ($items as $item) {
                $productList[] = $item->getProductId();
            }
            $this->_sendEvent('wishlist', 'add_product', 'Products : ' . implode(', ', $productList) . ' added to wishlist', 1);
        }
    }

    public function observeWishlistShare($observer) {
        $wishlist = $observer->getEvent()->getWishlist();
        if (Mage::getStoreConfigFlag('googleuniversalanalytics/events/wishlist_share')) {
            $this->_sendEvent('wishlist', 'share', 'Customer #' . $wishlist->getCustomerId() . ' shared wishlist ', 1);
        }
    }

    public function observeSalesruleValidatorProcess($observer) {
        $rule = $observer->getEvent()->getRule();
        $item = $observer->getEvent()->getItem();
        $address = $observer->getEvent()->getAddress();
        $quote = $observer->getEvent()->getQuote();
        $qty = $observer->getEvent()->getQty();
        $result = $observer->getEvent()->getResult();
        if ($rule->getCouponCode() && Mage::getStoreConfig('googleuniversalanalytics/events/checkout_add_coupon')) {
            $this->_sendEvent('checkout', 'add_coupon', 'Add coupon code : ' . $rule->getCouponCode(), 1);
        }
    }

    public function observeNewsletterSubscriberSaveCommitAfter($observer) {
        $subscriber = $observer->getEvent()->getDataObject();
        $statusChange = $subscriber->getIsStatusChanged();
        if ($subscriber->getSubscriberStatus() == "1" && $statusChange) {
            if(Mage::getStoreConfigFlag('googleuniversalanalytics/events/newsletter_subscribe')) {
                $this->_sendEvent('newsletter', 'subscribe', 'New newsletter subscriber : ' . $subscriber->getId(), 1);
            }
            if(Mage::getStoreConfigFlag('googleuniversalanalytics/extra_pageviews/newsletter_subscribe')) {
                $this->getMeasurementProtocol()->sendPageView('/customer/newsletter/subscribe');
            }
        }
        if ($subscriber->getSubscriberStatus() == "0" && $statusChange) {
            if(Mage::getStoreConfigFlag('googleuniversalanalytics/events/newsletter_unsubscribe')) {
                $this->_sendEvent('newsletter', 'unsubscribe', 'Newsletter unsubscribed : ' . $subscriber->getId(), 1);
            }
            if(Mage::getStoreConfigFlag('googleuniversalanalytics/extra_pageviews/newsletter_unsubscribe')) {
                $this->getMeasurementProtocol()->sendPageView('/customer/newsletter/unsubscribe');
            }
        }
    }

    public function observeSalesQuoteRemoveItem($observer) {
        $quoteItem = $observer->getEvent()->getQuoteItem();
        $product = $quoteItem->getProduct();
        if (Mage::app()->getStore()->isAdmin()) {
            return;
        }

        if (Mage::getStoreConfig('googleuniversalanalytics/events/checkout_remove_product')) {
            $params = array();
            if ($idx = Mage::getStoreConfig('googleuniversalanalytics/dimensions/product_name')) {
                $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CUSTOM_DIMENSION . $idx] = Mage::helper('googleuniversalanalytics')->formatData($quoteItem->getName());
            }
            if ($idx = Mage::getStoreConfig('googleuniversalanalytics/dimensions/product_sku')) {
                $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CUSTOM_DIMENSION . $idx] = $quoteItem->getSku();
            }
            if ($idx = Mage::getStoreConfig('googleuniversalanalytics/dimensions/product_id')) {
                $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CUSTOM_DIMENSION . $idx] = $quoteItem->getProductId();
            }
            if ($idx = Mage::getStoreConfig('googleuniversalanalytics/dimensions/product_cost')) {
                $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CUSTOM_DIMENSION . $idx] = $quoteItem->getBaseCost();
            }
            if ($idx = Mage::getStoreConfig('googleuniversalanalytics/dimensions/product_profit')) {
                $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CUSTOM_DIMENSION . $idx] = $quoteItem->getPriceInclTax() - $quoteItem->getBaseCost();
            }
            if (Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated()) {
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ID, 1)] = $this->getEcHelper()->getProductSkuValue($product);
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_NAME, 1)] = $this->getEcHelper()->getProductNameValue($product);
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_CATEGORY, 1)] = $this->getEcHelper()->getProductCategoryValue($product);
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_BRAND, 1)] = $this->getEcHelper()->getProductBrandValue($product);
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_VARIANT, 1)] = $this->getEcHelper()->getProductVariantValue($product);
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_PRICE, 1)] = Mage::helper('googleuniversalanalytics/ecommerce')->getItemPriceForQuote($quoteItem);
                $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION] = 'remove';
                if(Mage::getSingleton('core/session')->getLastListName()) {
                    $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION_LIST] = Mage::getSingleton('core/session')->getLastListName();
                }
            }
            $this->_sendEvent('checkout', 'remove_product', $quoteItem->getSku(), $quoteItem->getPrice(), false, false, null, $params);
        }
    }

    public function observeCheckoutCartProductAddAfter($observer) {
        $quoteItem = $observer->getEvent()->getQuoteItem();
        $product = $observer->getEvent()->getProduct();
        if (Mage::app()->getStore()->isAdmin()) {
            return;
        }
        if (Mage::getStoreConfig('googleuniversalanalytics/events/checkout_add_product')) {
            $params = array();
            $customDimensions = Mage::helper('googleuniversalanalytics')->getCustomDimensionsForProduct($product);
            foreach($customDimensions as $idx=>$value) {
                $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CUSTOM_DIMENSION . $idx] = $value;
            }
            if (Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated()) {
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ID, 1)] = $this->getEcHelper()->getProductSkuValue($product);
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_NAME, 1)] = $this->getEcHelper()->getProductNameValue($product);
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_CATEGORY, 1)] = $this->getEcHelper()->getProductCategoryValue($product);
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_BRAND, 1)] = $this->getEcHelper()->getProductBrandValue($product);
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_VARIANT, 1)] = $this->getEcHelper()->getProductVariantValue($product);
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_PRICE, 1)] = Mage::helper('googleuniversalanalytics/ecommerce')->getItemPriceForQuote($quoteItem);
                $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION] = 'add';
                if(Mage::getSingleton('core/session')->getLastListName()) {
                    $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION_LIST] = Mage::getSingleton('core/session')->getLastListName();
                }
            }
            $this->_sendEvent('checkout', 'add_product', $quoteItem->getSku(), $quoteItem->getPrice(), false, false, null, $params);
        }
    }

    public function observeCustomerLogin($observer) {
        $customer = $observer->getEvent()->getCustomer();
        if (Mage::getStoreConfig('googleuniversalanalytics/events/customer_login')) {
            $this->_sendEvent('customer', 'login', 'Customer login #' . $customer->getId(), 1);
        }
        if(Mage::getStoreConfigFlag('googleuniversalanalytics/extra_pageviews/customer_login_success')) {
            $this->getMeasurementProtocol()->sendPageView('/customer/login/success');
        }
        $this->_sendCheckoutLogin($customer);
    }

    public function observeCustomerLogout($observer) {
        $customer = $observer->getEvent()->getCustomer();
        Mage::getSingleton('core/session')->setGuaCustomerLogout(1);
        if (Mage::getStoreConfig('googleuniversalanalytics/events/customer_logout')) {
            $this->_sendEvent('customer', 'logout', 'Customer logout #' . $customer->getId(), 1);
        }
    }

    public function /*observeCustomerRegisterSuccess*/observeCustomerSaveAfter($observer) {
        $customer = $observer->getEvent()->getCustomer();
        if(!$customer->getOrigData('entity_id') && !$customer->getData('gua_registration_sent')) { // registration
            $customer->setData('gua_registration_sent', 1);/*prevent double event treatment */
            if (Mage::getStoreConfig('googleuniversalanalytics/events/customer_register')) {
                $this->_sendEvent('customer', 'registration', 'Customer registration #' . $customer->getId(), 1);
            }
            if (Mage::getStoreConfig('googleuniversalanalytics/extra_pageviews/customer_registration')) {
                $this->getMeasurementProtocol()->sendPageView('/customer/register/success');
            }
        }
    }

    public function observeCheckoutMultishippingControllerSuccessAction($observer) {
        $orderIds = $observer->getEvent()->getOrderIds();
        $this->_setGuaOnSuccessPageView($orderIds);
    }

    public function observeCheckoutOnepageControllerSuccessAction($observer) {
        $orderIds = $observer->getEvent()->getOrderIds();
        $this->_setGuaOnSuccessPageView($orderIds);
    }

    public function observeSalesOrderPlaceBefore($observer) {
        $order = $observer->getEvent()->getOrder();
        $this->_saveGuaClientId($order);
    }
    
    public function observeSalesQuoteSaveBefore($observer) {
        $quote = $observer->getEvent()->getQuote();
        $this->_saveGuaDataToQuote($quote);
    }
    
    public function observeSalesOrderSaveAfter($observer) {
        $order = $observer->getEvent()->getOrder();
        if($order->getOrigData('status') == $order->getData('status')) {
            return;
        }
        $shouldSendTransaction = Mage::getStoreConfig('googleuniversalanalytics/transactions/transaction_event', $order->getStoreId()) == Webcooking_GoogleUniversalAnalytics_Helper_Data::TRANSACTION_EVENT_CHANGE_ORDER_STATUS && in_array($order->getData('status'), explode(',', Mage::getStoreConfig('googleuniversalanalytics/transactions/order_status_changed', $order->getStoreId())));
        $shouldSendTransaction = $shouldSendTransaction || Mage::getStoreConfig('googleuniversalanalytics/transactions/transaction_event', $order->getStoreId())  == Webcooking_GoogleUniversalAnalytics_Helper_Data::TRANSACTION_EVENT_CREATE_ORDER;
        $shouldSendTransaction = $shouldSendTransaction && !$order->getGuaSentFlag();
        try {
            if ($shouldSendTransaction) {
                $this->_sendTransactionRequestFromOrder($order);
            }
            if (Mage::getStoreConfigFlag('googleuniversalanalytics/events/order_status')) {
                $this->_sendEvent('sales', 'order_status', 'Order : ' . $order->getIncrementId() . ' has new status : ' . $order->getStatus(), 1, Mage::app()->getStore()->isAdmin(), $order->getGuaClientId(), $order->getStoreId());
            }
        } catch (Exception $e) {
            Mage::helper('googleuniversalanalytics')->log('Error :' . $e->getMessage());
        }
    }

    public function observeSalesOrderPlaceAfter($observer) {
       $order = $observer->getEvent()->getOrder();
       $shouldSendTransaction = Mage::getStoreConfig('googleuniversalanalytics/transactions/transaction_event', $order->getStoreId()) == Webcooking_GoogleUniversalAnalytics_Helper_Data::TRANSACTION_EVENT_PLACE_ORDER;
        
        try {
            if(Mage::getStoreConfigFlag('googleuniversalanalytics/extra_pageviews/order_created')) {
                $this->getMeasurementProtocol()->sendPageView('/gua/order/created', Mage::app()->getStore()->isAdmin(), $order->getGuaClientId());
            }
            if ($shouldSendTransaction) {
                $this->_sendTransactionRequestFromOrder($order);
            }
            if (!$shouldSendTransaction || !Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated($order->getStoreId())) { //If EE, we will send order as an event...
                $this->_sendEvent('sales', 'order_placed', 'Order : ' . $order->getIncrementId() . ' has been placed', $order->getBaseGrandTotal(), Mage::app()->getStore()->isAdmin(), $order->getGuaClientId(),$order->getStoreId());
            }
            Mage::getSingleton('checkout/session')->setGuaCheckoutStep(0);
        } catch (Exception $e) {
            Mage::helper('googleuniversalanalytics')->log('Error :' . $e->getMessage());
        }
    }

    public function observeSalesOrderCreditmemoRefund($observer) {
        $creditmemo = $observer->getEvent()->getCreditmemo();
        $order = $creditmemo->getOrder();
        try {
            $this->_sendRefundRequestFromCreditmemo($creditmemo, $order);
            if(Mage::getStoreConfigFlag('googleuniversalanalytics/extra_pageviews/order_refunded')) {
                $this->getMeasurementProtocol()->sendPageView('/gua/order/refunded', true, $order->getGuaClientId());
            }
            if (!Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated($order->getStoreId())) {//If EE, we will send order as an event...
                $this->_sendEvent('sales', 'order_refunded', 'Order : ' . $order->getIncrementId() . ' has been refunded', 1, true, $order->getGuaClientId(), $order->getStoreId());
            }
        } catch (Exception $e) {
            Mage::helper('googleuniversalanalytics')->log('Error :' . $e->getMessage());
        }
    }
    
    public function observeSalesOrderInvoicePay($observer) {
        $invoice = $observer->getEvent()->getInvoice();
        $order = $invoice->getOrder();
        $shouldSendTransaction = Mage::getStoreConfig('googleuniversalanalytics/transactions/transaction_event', $order->getStoreId()) == Webcooking_GoogleUniversalAnalytics_Helper_Data::TRANSACTION_EVENT_PAY_INVOICE;
        //Mage::helper('googleuniversalanalytics')->log(Mage::getStoreConfig('googleuniversalanalytics/transactions/transaction_event', $order->getStoreId()), null, 'gua.debug.log');
        try {
            if ($shouldSendTransaction) {
               $this->_sendTransactionRequestFromInvoice($invoice, $order);
            }
            //$this->getMeasurementProtocol()->sendPageView('/gua/order/invoiced', true, $order->getGuaClientId());
            if (!$shouldSendTransaction || !Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated($order->getStoreId())) {//If EE, we will send order as an event...
                $this->_sendEvent('sales', 'order_invoiced', 'Order : ' . $order->getIncrementId() . ' has been paid', 1, true, $order->getGuaClientId(), $order->getStoreId());
            }
        } catch (Exception $e) {
            Mage::helper('googleuniversalanalytics')->log('Error :' . $e->getMessage());
        }
    }
    
    public function observeSalesOrderInvoiceRegister($observer) {
        $invoice = $observer->getEvent()->getInvoice();
        $order = $observer->getEvent()->getOrder();
        $shouldSendTransaction = Mage::getStoreConfig('googleuniversalanalytics/transactions/transaction_event', $order->getStoreId()) == Webcooking_GoogleUniversalAnalytics_Helper_Data::TRANSACTION_EVENT_CREATE_INVOICE;
        //Mage::helper('googleuniversalanalytics')->log(Mage::getStoreConfig('googleuniversalanalytics/transactions/transaction_event', $order->getStoreId()), null, 'gua.debug.log');
        try {
            if ($shouldSendTransaction) {
               $this->_sendTransactionRequestFromInvoice($invoice, $order);
            }
            if(Mage::getStoreConfigFlag('googleuniversalanalytics/extra_pageviews/order_invoiced')) {
                $this->getMeasurementProtocol()->sendPageView('/gua/order/invoiced', true, $order->getGuaClientId());
            }
            if (!$shouldSendTransaction || !Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated($order->getStoreId())) {//If EE, we will send order as an event...
                $this->_sendEvent('sales', 'order_invoiced', 'Order : ' . $order->getIncrementId() . ' has been paid', 1, true, $order->getGuaClientId(), $order->getStoreId());
            }
        } catch (Exception $e) {
            Mage::helper('googleuniversalanalytics')->log('Error :' . $e->getMessage());
        }
    }

    public function observeSalesOrderPaymentPay($observer) {
        $payment = $observer->getEvent()->getPayment();
        $invoice = $observer->getEvent()->getInvoice();
        $order = $invoice->getOrder();
        $shouldSendTransaction = Mage::getStoreConfig('googleuniversalanalytics/transactions/transaction_event', $order->getStoreId()) == Webcooking_GoogleUniversalAnalytics_Helper_Data::TRANSACTION_EVENT_CAPTURE_INVOICE;
        try {
            if ($shouldSendTransaction) {
               $this->_sendTransactionRequestFromInvoice($invoice, $order);
            }
            //$this->getMeasurementProtocol()->sendPageView('/gua/order/paid', true, $order->getGuaClientId());
            if (!$shouldSendTransaction || !Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated($order->getStoreId())) {//If EE, we will send order as an event...
                $this->_sendEvent('sales', 'order_paid', 'Order : ' . $order->getIncrementId() . ' has been paid', 1, true, $order->getGuaClientId(), $order->getStoreId());
            }
        } catch (Exception $e) {
            Mage::helper('googleuniversalanalytics')->log('Error :' . $e->getMessage());
        }
    }

    protected function _setGuaOnSuccessPageView($orderIds) {
        if (empty($orderIds) || !is_array($orderIds)) {
            return;
        }
        $block = Mage::app()->getFrontController()->getAction()->getLayout()->getBlock('google_universal_analytics_init');
        if ($block) {
            $block->setOrderIds($orderIds);
        }
    }
    
    protected function _saveGuaDataToQuote($quote, $saveNeeded = false) {
        if($quote->getGuaClientId()) {
            return;
        }
        $clientId = Mage::helper('googleuniversalanalytics')->getClientId(false);
        if ($clientId) {
            $quote->setGuaClientId($clientId);
            if(Mage::getSingleton('core/session')->getGuaUserAgent()) {
                $quote->setGuaUa(Mage::getSingleton('core/session')->getGuaUserAgent());
            }
        }
        if($saveNeeded) {
            $quote->save();
        }
    }

    protected function _saveGuaClientId($order, $createIfNotFound=true) {
        $clientId = Mage::helper('googleuniversalanalytics')->getClientId($createIfNotFound);
        if ($clientId && !$order->getGuaClientId()) {
            $order->setGuaClientId($clientId);
            if(Mage::getSingleton('core/session')->getGuaUserAgent()) {
                $order->setGuaUa(Mage::getSingleton('core/session')->getGuaUserAgent());
            }
            $order->save();
        }
    }

    protected function _getAdditionalParamsForOrder($order) {
        $params = array();
        
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::USER_ID] = $order->getCustomerId();
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::DOCUMENT_LOCATION_URL] = Mage::app()->getStore($order->getStoreId())->getUrl('checkout/onepage/success');
        if($order->getGuaUa()) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::USER_AGENT_OVERRIDE] = $order->getGuaUa();
        }
        //ShoppingFlux Compatibility
        if ($order->getMarketplaceShoppingflux()) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CAMPAIGN_NAME] = 'Marketplaces';
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CAMPAIGN_SOURCE] = $order->getMarketplaceShoppingflux();
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CAMPAIGN_MEDIUM] = 'Shopping Flux';
        }
        if($order->getRemoteIp()) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::IP_OVERRIDE] = $order->getRemoteIp();
        }
        
        
        if (Mage::helper('wcooall')->isModuleEnabled('Ess_M2ePro')) {
            if(class_exists('Ess_M2ePro_Helper_Component_Amazon')) {
                try {
                    $collection = Mage::helper('M2ePro/Component_Amazon')->getCollection('Order');
                    $collection->getSelect()
                            ->joinLeft(
                                    array('so' => Mage::getSingleton('core/resource')->getTableName('sales/order')), '(so.entity_id = `main_table`.magento_order_id)', array('magento_order_num' => 'increment_id'))
                            ->where("`main_table`.`magento_order_id` = '" . $order->getId() . "'");
                    if ($collection->count() >= 1) {
                        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CAMPAIGN_NAME] = 'Marketplaces';
                        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CAMPAIGN_SOURCE] = 'Amazon';
                        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CAMPAIGN_MEDIUM] = 'M2ePro';
                    }
                } catch (Exception $e) {
                    Mage::helper('googleuniversalanalytics')->log($e->getMessage(), 'gua.m2epro.log');
                }
            }
            if(class_exists('Ess_M2ePro_Helper_Component_Ebay')) {
                try {
                    $collection = Mage::helper('M2ePro/Component_Ebay')->getCollection('Order');
                    $collection->getSelect()
                            ->joinLeft(
                                    array('so' => Mage::getSingleton('core/resource')->getTableName('sales/order')), '(so.entity_id = `main_table`.magento_order_id)', array('magento_order_num' => 'increment_id'))
                            ->where("`main_table`.`magento_order_id` = '" . $order->getId() . "'");
                    if ($collection->count() >= 1) {
                        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CAMPAIGN_NAME] = 'Marketplaces';
                        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CAMPAIGN_SOURCE] = 'eBay';
                        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CAMPAIGN_MEDIUM] = 'M2ePro';
                    }
                } catch (Exception $e) {
                    Mage::helper('googleuniversalanalytics')->log($e->getMessage(), 'gua.m2epro.log');
                }
            }
        }
        return $params;
    }
    
    public function observeSalesQuotePaymentSaveAfter($observer) {
        $payment = $observer->getEvent()->getPayment();
        if($payment->getData('method')) {
            $this->_sendCheckoutPaymentMethod($payment);
        }
    }
    
    public function observeSalesQuoteAddressSaveAfter($observer) {
        $address = $observer->getEvent()->getQuoteAddress();
        if($address->getAddressType() == 'shipping' && $address->getData('shipping_method')) {
            $this->_sendCheckoutShippingMethod($address);
        }
        if($address->getData('city')) {
            $this->_sendCheckoutAddress($address);
        }
    }
    
    protected function _canSendCheckoutStep($stepIndex, $storeId) {
        if(!$stepIndex) {
            return false;
        }
        if($stepIndex > 0 && Mage::getSingleton('checkout/session')->getGuaCheckoutStep() != ($stepIndex-1)) {
            return false;
        }
        if (!Mage::getStoreConfigFlag('googleuniversalanalytics/enhanced_ecommerce/manage_checkout', $storeId)) {
            return false ;
        }
        if (!Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated($storeId)) {
            return false;
        }
        return true;
    }
    
    protected function _sendCheckoutPaymentMethod($payment) {
        $storeId = $payment->getQuote()->getStoreId();
        $stepIndex = Mage::getStoreConfig('googleuniversalanalytics/enhanced_ecommerce/checkout_step_payment_method', $storeId);
        
        if(!$this->_canSendCheckoutStep($stepIndex, $storeId)) {
            return;
        }
        $params = array();
        $items = $payment->getQuote()->getAllVisibleItems();
        $itemIncrement = 1;
        foreach($items as $item) {
            $itemParams = $this->_getItemParams($item, $itemIncrement++);
            $params = array_merge($params, $itemParams);
        }
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION] = 'checkout';
        if(Mage::getSingleton('core/session')->getLastListName()) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION_LIST] = Mage::getSingleton('core/session')->getLastListName();
        }
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CHECKOUT_STEP] = $stepIndex;
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CHECKOUT_STEP_OPTION] = $payment->getMethod();
        $this->_sendEvent('checkout', 'payment', $payment->getMethod(), 1, false, false, $storeId, $params);
        Mage::getSingleton('checkout/session')->setGuaCheckoutStep($stepIndex);
    }
    
    protected function _sendCheckoutAddress($quoteShippingAddress) {
        $storeId = $quoteShippingAddress->getQuote()->getStoreId();
        $stepIndex = $quoteShippingAddress->getAddressType()=='billing' ? 
                Mage::getStoreConfig('googleuniversalanalytics/enhanced_ecommerce/checkout_step_billing_address', $storeId) : 
                Mage::getStoreConfig('googleuniversalanalytics/enhanced_ecommerce/checkout_step_shipping_address', $storeId);
        
        if(!$this->_canSendCheckoutStep($stepIndex, $storeId)) {
            return;
        }
       
        $params = array();
        $items = $quoteShippingAddress->getQuote()->getAllVisibleItems();
        $itemIncrement = 1;
        foreach($items as $item) {
            $itemParams = $this->_getItemParams($item, $itemIncrement++);
            $params = array_merge($params, $itemParams);
        }
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION] = 'checkout';
        if(Mage::getSingleton('core/session')->getLastListName()) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION_LIST] = Mage::getSingleton('core/session')->getLastListName();
        }
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CHECKOUT_STEP] = $stepIndex;
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CHECKOUT_STEP_OPTION] = $quoteShippingAddress->getPostcode() . ' ' .$quoteShippingAddress->getCity() . ' ' . $quoteShippingAddress->getCountryId();
        $this->_sendEvent('checkout', 'address', $quoteShippingAddress->getAddressType(), 1, false, false, $storeId, $params);
        Mage::getSingleton('checkout/session')->setGuaCheckoutStep($stepIndex);
    }
    
    protected function _sendCheckoutShippingMethod($quoteShippingAddress) {
        $storeId = $quoteShippingAddress->getQuote()->getStoreId();
        $stepIndex = Mage::getStoreConfig('googleuniversalanalytics/enhanced_ecommerce/checkout_step_shipment_method', $storeId);
        
        if(!$this->_canSendCheckoutStep($stepIndex, $storeId)) {
            return;
        }
        $params = array();
        $items = $quoteShippingAddress->getQuote()->getAllVisibleItems();
        $itemIncrement = 1;
        foreach($items as $item) {
            $itemParams = $this->_getItemParams($item, $itemIncrement++);
            $params = array_merge($params, $itemParams);
        }
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION] = 'checkout';
        if(Mage::getSingleton('core/session')->getLastListName()) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION_LIST] = Mage::getSingleton('core/session')->getLastListName();
        }
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CHECKOUT_STEP] = $stepIndex;
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CHECKOUT_STEP_OPTION] = $quoteShippingAddress->getShippingDescription()?$quoteShippingAddress->getShippingDescription():$quoteShippingAddress->getShippingMethod();
        $this->_sendEvent('checkout', 'shipping_method', $quoteShippingAddress->getShippingMethod(), 1, false, false, $storeId, $params);
        Mage::getSingleton('checkout/session')->setGuaCheckoutStep($stepIndex);
    }
    
    protected function _sendCheckoutLogin($customer) {
        $storeId = Mage::app()->getStore()->getId(); // customer login is always on frontend.
        $stepIndex = Mage::getStoreConfig('googleuniversalanalytics/enhanced_ecommerce/checkout_step_login', $storeId);
        
        if(!$this->_canSendCheckoutStep($stepIndex, $storeId)) {
            return;
        }
        $params = array();
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION] = 'checkout';
        if(Mage::getSingleton('core/session')->getLastListName()) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION_LIST] = Mage::getSingleton('core/session')->getLastListName();
        }
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CHECKOUT_STEP] = $stepIndex;
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CHECKOUT_STEP_OPTION] = 'Group '.$customer->getGroupId();
        $this->_sendEvent('checkout', 'login', 'Customer ' . $customer->getId(), 1, false, false, $storeId, $params);
        Mage::getSingleton('checkout/session')->setGuaCheckoutStep($stepIndex);
    }
    
    protected function _sendCheckoutCart($cart) {
        if(!$cart || !is_object($cart) || !$cart->getQuote()) {
            return;
        }
        $quote = $cart->getQuote();
        $storeId = $quote->getStoreId(); 
        $stepIndex = Mage::getStoreConfig('googleuniversalanalytics/enhanced_ecommerce/checkout_step_cart', $storeId);
        
        if(!$this->_canSendCheckoutStep($stepIndex, $storeId)) {
            return;
        }
        $params = array();
        $items = $quote->getAllVisibleItems();
        $itemIncrement = 1;
        foreach($items as $item) {
            $itemParams = $this->_getItemParams($item, $itemIncrement++);
            $params = array_merge($params, $itemParams);
        }
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION] = 'checkout';
        if(Mage::getSingleton('core/session')->getLastListName()) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION_LIST] = Mage::getSingleton('core/session')->getLastListName();
        }
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CHECKOUT_STEP] = $stepIndex;
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CHECKOUT_STEP_OPTION] = 'Cart';
        $this->_sendEvent('checkout', 'cart', 'view', 1, false, false, $storeId, $params);
        Mage::getSingleton('checkout/session')->setGuaCheckoutStep($stepIndex);
    }
    
    protected function _sendCheckoutCheckout($cart) {
        if(!$cart || !is_object($cart) || !$cart->getQuote()) {
            return;
        }
        $quote = $cart->getQuote();
        $storeId = $quote->getStoreId(); 
        $stepIndex = Mage::getStoreConfig('googleuniversalanalytics/enhanced_ecommerce/checkout_step_checkout', $storeId);
        
        if(!$this->_canSendCheckoutStep($stepIndex, $storeId)) {
            return;
        }
        $params = array();
        $items = $quote->getAllVisibleItems();
        $itemIncrement = 1;
        foreach($items as $item) {
            $itemParams = $this->_getItemParams($item, $itemIncrement++);
            $params = array_merge($params, $itemParams);
        }
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION] = 'checkout';
        if(Mage::getSingleton('core/session')->getLastListName()) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION_LIST] = Mage::getSingleton('core/session')->getLastListName();
        }
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CHECKOUT_STEP] = $stepIndex;
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CHECKOUT_STEP_OPTION] = 'Checkout';
        $this->_sendEvent('checkout', 'onepage', 'view', 1, false, false, $storeId, $params);
        Mage::getSingleton('checkout/session')->setGuaCheckoutStep($stepIndex);
    }

    protected function _sendRefundRequestFromCreditmemo($creditmemo, $order) {
        $storeId = $order->getStoreId();
        if (!Mage::getStoreConfigFlag('googleuniversalanalytics/enhanced_ecommerce/manage_refund', $storeId)) {
            return;
        }
        if (!$order->getGuaClientId()) {
            return;
        }
        if (!Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated($storeId)) {
            return;
        }

        $params = array();
        $guaClientId = $order->getGuaClientId();
        $transactionId = $order->getIncrementId();
        $items = $creditmemo->getAllItems();
        //$refundTotal = $creditmemo->getGrandTotal();
        
        /* Partial refunds are not working at the moment... Still in beta, so...
        $itemIncrement = 1;
        foreach($items as $item) {
            $hasParentItem = (bool) $item->getOrderItem()->getParentItemId();
            if(!$hasParentItem && $item->getQty() > 0) {
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ID, $itemIncrement)] = $item->getSku();
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_QUANTITY, $itemIncrement)] = $item->getQty();
                $itemIncrement++;
            }
        }
        */
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::TRANSACTION_ID] = $transactionId;
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION] = 'refund';
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::USER_ID] = $order->getCustomerId();
        $this->_sendEvent('transaction', 'creditmemo', $transactionId, false, true, $guaClientId, $order->getStoreId(), $params);
        
    }

    protected function _sendTransactionRequestFromInvoice($invoice, $order) {
        $storeId = $order->getStoreId();
        if (!$order->getGuaClientId()) {
            //Fix for Sage Pay extension
            $this->_saveGuaClientId($order, true);
        }
        if (!Mage::getStoreConfig('googleuniversalanalytics/transactions/use_mp', $storeId) || !$order->getGuaClientId()) {
            return;
        }
        $useStoreCurrency = Mage::helper('googleuniversalanalytics/ecommerce')->useStoreCurrency($storeId);
                
        $guaClientId = $order->getGuaClientId();
        $transactionId = $order->getIncrementId();
        $revenue = Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionRevenueForInvoice($invoice);
        $shipping = Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionShippingForInvoice($invoice);
        $tax = Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionTaxForInvoice($invoice);
        $affiliation = Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionAffiliation($order);
        $currency = $useStoreCurrency ? $invoice->getOrderCurrencyCode() : $invoice->getBaseCurrencyCode();
        $coupon = $order->getCouponCode();
        $items = $invoice->getAllItems();
        $params = $this->_getAdditionalParamsForOrder($order);
        
        if (Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated($storeId)) {
            $itemIncrement = 1;
            foreach($items as $item) {
                $hasParentItem = (bool) $item->getOrderItem()->getParentItemId();
                if(Mage::helper('googleuniversalanalytics/ecommerce')->sendChildrenItems($storeId) || !$hasParentItem) {
                    $itemParams = $this->_getItemParams($item, $itemIncrement++);
                    $params = array_merge($params, $itemParams);
                }
            }
            $transactionParams = $this->getMeasurementProtocol()->getTransactionParams($storeId, $transactionId, $affiliation, $revenue, $shipping, $tax, $currency, $coupon);
            $params = array_merge($params, $transactionParams);
            $this->_sendEvent('transaction', 'invoice', $transactionId, $revenue, true, $guaClientId, $order->getStoreId(), $params);
        } else {
            $this->getMeasurementProtocol()
                    ->sendTransaction($transactionId, $affiliation, $revenue, $shipping, $tax, $currency, $guaClientId, true, $order->getStoreId(), $params);
            $itemIncrement = 1;
            foreach($items as $item) {
                $hasParentItem = (bool) $item->getOrderItem()->getParentItemId();
                if(Mage::helper('googleuniversalanalytics/ecommerce')->sendChildrenItems($storeId) || !$hasParentItem) {
                    $additionalParams = array(
                        Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::USER_ID => $order->getCustomerId()
                    );
                    $this->getMeasurementProtocol()
                            ->sendItem($transactionId, $itemIncrement++, $item->getName(), Mage::helper('googleuniversalanalytics/ecommerce')->getItemPriceForInvoice($item), $item->getQty() ? $item->getQty() : $item->getQtyOrdered(), $item->getSku(), '', '', '', $currency, $guaClientId, 1, $order->getStoreId(), $additionalParams);
                }
            }
        }
        $order->setGuaSentFlag(1)->save();
    }
    
    protected function _getItemParams($item, $itemIncrement) {
        $orderItem = $item->getOrderItem()?$item->getOrderItem():$item;
        $order = $orderItem->getOrder();
        $product = $orderItem->getProduct();
        $storeId = $order?$order->getStoreId():$orderItem->getStoreId();
        if(!$product && $orderItem->getProductId()) {
            //In magento < 1.7, there is no getProduct() on order item model
            $product = Mage::getModel('catalog/product')->setStoreId($storeId)->load($orderItem->getProductId());
        }
        $category = $this->getEcHelper()->getProductCategoryValue($product, true, $storeId);
        $brand = $this->getEcHelper()->getProductBrandValue($product, $storeId);
        $variant = $this->getEcHelper()->getProductVariantValue($product, $orderItem);
        $itemParams = $this->getMeasurementProtocol()->getItemParams($storeId, $itemIncrement, $item->getName(), Mage::helper('googleuniversalanalytics/ecommerce')->getItemPriceForOrder($orderItem), $item->getQty() ? $item->getQty() : $item->getQtyOrdered(), $item->getSku(), $variant, $brand, $category, $product);
        return $itemParams;
    }

    protected function _sendTransactionRequestFromOrder($order) {
        $storeId = $order->getStoreId();
        if (!Mage::getStoreConfig('googleuniversalanalytics/transactions/use_mp', $storeId) || !$order->getGuaClientId()) {
            return;
        }
        $useStoreCurrency = Mage::helper('googleuniversalanalytics/ecommerce')->useStoreCurrency($storeId);
        $guaClientId = $order->getGuaClientId();
        $transactionId = $order->getIncrementId();
        
        $revenue = Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionRevenueForOrder($order);
        $shipping = Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionShippingForOrder($order);
        $tax = Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionTaxForOrder($order);
        $currency = $useStoreCurrency ? $order->getOrderCurrencyCode() : $order->getBaseCurrencyCode();
        
        $affiliation = Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionAffiliation($order);
        $items = $order->getAllItems();
        $coupon = $order->getCouponCode();
        $params = $this->_getAdditionalParamsForOrder($order);
        
        if (Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated($storeId)) {
            $itemIncrement = 1;
            foreach($items as $item) {
                $hasParentItem = (bool) $item->getParentItemId();
                if(Mage::helper('googleuniversalanalytics/ecommerce')->sendChildrenItems($storeId) || !$hasParentItem) {
                    $itemParams = $this->_getItemParams($item, $itemIncrement++);
                    $params = array_merge($params, $itemParams);
                }
            }
            $transactionParams = $this->getMeasurementProtocol()->getTransactionParams($storeId, $transactionId, $affiliation, $revenue, $shipping, $tax, $currency, $coupon);
            $params = array_merge($params, $transactionParams);
            $this->_sendEvent('transaction', 'order', $transactionId, false, true, $guaClientId, $order->getStoreId(), $params);
        } else {
            $this->getMeasurementProtocol()
                    ->sendTransaction($transactionId, $affiliation, $revenue, $shipping, $tax, $currency, $guaClientId, true, $storeId, $params);
            $itemIncrement = 1;
            foreach($items as $item) {
                $hasParentItem = (bool) $item->getParentItemId();
                if(Mage::helper('googleuniversalanalytics/ecommerce')->sendChildrenItems($storeId) || !$hasParentItem) {
                    $additionalParams = array(
                        Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::USER_ID => $order->getCustomerId()
                    );
                    $this->getMeasurementProtocol()
                            ->sendItem($transactionId, $itemIncrement++, $item->getName(), Mage::helper('googleuniversalanalytics/ecommerce')->getItemPriceForOrder($item), $item->getQty() ? $item->getQty() : $item->getQtyOrdered(), $item->getSku(), '', '', '', $currency, $guaClientId, 1, $order->getStoreId(), $additionalParams);
                }
            }
        }
        $order->setGuaSentFlag(1)->save();
    }

    

    protected function _refundCanceledOrder($order) {
        $storeId = $order->getStoreId();
      
        if (!Mage::getStoreConfig('googleuniversalanalytics/transactions/use_mp', $storeId) || !$order->getGuaClientId()) {
            return;
        }
        if (!Mage::getStoreConfigFlag('googleuniversalanalytics/enhanced_ecommerce/manage_refund', $storeId)) {
            return;
        }
        if (!Mage::getStoreConfigFlag('googleuniversalanalytics/enhanced_ecommerce/refund_canceled_order', $storeId)) {
            return;
        }
      
        $params = array();
        $guaClientId = $order->getGuaClientId();
        $transactionId = $order->getIncrementId();
        $items = $order->getAllItems();
        //$refundTotal = $order->getGrandTotal();
        
        
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::TRANSACTION_ID] = $transactionId;
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION] = 'refund';
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::USER_ID] = $order->getCustomerId();
        
        /* Partial refunds are not working at the moment... Not in beta anymore, but...
        $itemIncrement = 1;
        foreach($items as $item) {
            $hasParentItem = (bool) $item->getParentItemId();
            if(!$hasParentItem && $item->getQtyCanceled() > 0) {
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ID, $itemIncrement)] = $item->getSku();
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_QUANTITY, $itemIncrement)] = $item->getQtyCanceled();
                $itemIncrement++;
            }
        }
        */
        
        $this->_sendEvent('sales', 'order_canceled', $transactionId, false, true, $guaClientId, $order->getStoreId(), $params);
       
    }


    protected function _sendEvent($eventCategory, $eventAction, $eventLabel, $eventValue, $nonInteractiveMode = false, $guaClientId = false, $storeId = null, $additionalParameters=array()) {
        /*if (!Mage::getStoreConfig('googleuniversalanalytics/events/use_mp', $storeId)) {
            $event = new Varien_Object();
            $event->setCategory($eventCategory);
            $event->setAction($eventAction);
            $event->setLabel($eventLabel);
            $event->setValue($eventValue);
            $event->setNonInteractiveMode(intval((bool)$nonInteractiveMode));
            $event->setAdditionalParameters($additionalParameters);
            $guaEventsToDisplay = Mage::getSingleton('core/session')->getGuaEventsToDisplay();
            if(!is_array($guaEventsToDisplay)) {
                $guaEventsToDisplay = array();
            }
            $guaEventsToDisplay[] = $event;
            Mage::getSingleton('core/session')->setGuaEventsToDisplay($guaEventsToDisplay);
            return;
        }*/
        return $this->getMeasurementProtocol()
                ->sendEvent($eventCategory, $eventAction, $eventLabel, $eventValue, $nonInteractiveMode, $guaClientId, $storeId, $additionalParameters);
    }
    
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Log
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Log data helper
 */
class Mage_Log_Helper_Data extends Mage_Core_Helper_Abstract
{
    const XML_PATH_LOG_ENABLED = 'system/log/enable_log';

    /**
     * @var Mage_Log_Helper_Data
     */
    protected $_logLevel;

    /**
     * Allowed extensions that can be used to create a log file
     */
    private $_allowedFileExtensions = array('log', 'txt', 'html', 'csv');

    public function __construct(array $data = array())
    {
        $this->_logLevel = isset($data['log_level']) ? $data['log_level']
            : intval(Mage::getStoreConfig(self::XML_PATH_LOG_ENABLED));
    }

    /**
     * Are visitor should be logged
     *
     * @return bool
     */
    public function isVisitorLogEnabled()
    {
        return $this->_logLevel == Mage_Log_Model_Adminhtml_System_Config_Source_Loglevel::LOG_LEVEL_VISITORS
        || $this->isLogEnabled();
    }

    /**
     * Are all events should be logged
     *
     * @return bool
     */
    public function isLogEnabled()
    {
        return $this->_logLevel == Mage_Log_Model_Adminhtml_System_Config_Source_Loglevel::LOG_LEVEL_ALL;
    }

    /**
     * Are all events should be disabled
     *
     * @return bool
     */
    public function isLogDisabled()
    {
        return $this->_logLevel == Mage_Log_Model_Adminhtml_System_Config_Source_Loglevel::LOG_LEVEL_NONE;
    }

    /**
     * Checking if file extensions is allowed. If passed then return true.
     *
     * @param $file
     * @return bool
     */
    public function isLogFileExtensionValid($file)
    {
        $result = false;
        $validatedFileExtension = pathinfo($file, PATHINFO_EXTENSION);
        if ($validatedFileExtension && in_array($validatedFileExtension, $this->_allowedFileExtensions)) {
            $result = true;
        }

        return $result;
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Log
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Logging level backend source model
 *
 * @category    Mage
 * @package     Mage_Log
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Log_Model_Adminhtml_System_Config_Source_Loglevel
{
    /**
     * Don't log anything
     */
    const LOG_LEVEL_NONE = 0;

    /**
     * All possible logs enabled
     */
    const LOG_LEVEL_ALL = 1;

    /**
     * Logs only visitors, needs for working compare products and customer segment's related functionality
     * (eg. shopping cart discount for segments with not logged in customers)
     */
    const LOG_LEVEL_VISITORS = 2;

    /**
     * @var Mage_Log_Helper_Data
     */
    protected $_helper;

    public function __construct(array $data = array())
    {
        $this->_helper = !empty($data['helper']) ? $data['helper'] : Mage::helper('log');
    }

    public function toOptionArray()
    {
        $options = array(
            array(
                'label' => $this->_helper->__('Yes'),
                'value' => self::LOG_LEVEL_ALL,
            ),
            array(
                'label' => $this->_helper->__('No'),
                'value' => self::LOG_LEVEL_NONE,
            ),
            array(
                'label' => $this->_helper->__('Visitors only'),
                'value' => self::LOG_LEVEL_VISITORS,
            ),
        );

        return $options;
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_PageCache
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Page cache observer model
 *
 * @category    Mage
 * @package     Mage_PageCache
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_PageCache_Model_Observer
{
    const XML_NODE_ALLOWED_CACHE = 'frontend/cache/allowed_requests';

    /**
     * Check if full page cache is enabled
     *
     * @return bool
     */
    public function isCacheEnabled()
    {
        return Mage::helper('pagecache')->isEnabled();
    }

    /**
     * Check when cache should be disabled
     *
     * @param Varien_Event_Observer $observer
     * @return Mage_PageCache_Model_Observer
     */
    public function processPreDispatch(Varien_Event_Observer $observer)
    {
        if (!$this->isCacheEnabled()) {
            return $this;
        }
        $action = $observer->getEvent()->getControllerAction();
        $request = $action->getRequest();
        $needCaching = true;

        if ($request->isPost()) {
            $needCaching = false;
        }

        $configuration = Mage::getConfig()->getNode(self::XML_NODE_ALLOWED_CACHE);

        if (!$configuration) {
            $needCaching = false;
        }

        $configuration = $configuration->asArray();
        $module = $request->getModuleName();
        $controller = $request->getControllerName();
        $action = $request->getActionName();

        if (!isset($configuration[$module])) {
            $needCaching = false;
        }

        if (isset($configuration[$module]['controller']) && $configuration[$module]['controller'] != $controller) {
            $needCaching = false;
        }

        if (isset($configuration[$module]['action']) && $configuration[$module]['action'] != $action) {
            $needCaching = false;
        }

        if (!$needCaching) {
            Mage::helper('pagecache')->setNoCacheCookie();
        }

        return $this;
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_PageCache
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Page cache data helper
 *
 * @category    Mage
 * @package     Mage_PageCache
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_PageCache_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Pathes to external cache config options
     */
    const XML_PATH_EXTERNAL_CACHE_ENABLED  = 'system/external_page_cache/enabled';
    const XML_PATH_EXTERNAL_CACHE_LIFETIME = 'system/external_page_cache/cookie_lifetime';
    const XML_PATH_EXTERNAL_CACHE_CONTROL  = 'system/external_page_cache/control';

    /**
     * Path to external cache controls
     */
    const XML_PATH_EXTERNAL_CACHE_CONTROLS = 'global/external_cache/controls';

    /**
     * Cookie name for disabling external caching
     *
     * @var string
     */
    const NO_CACHE_COOKIE = 'external_no_cache';

    /**
     * Check whether external cache is enabled
     *
     * @return bool
     */
    public function isEnabled()
    {
        return (bool)Mage::getStoreConfig(self::XML_PATH_EXTERNAL_CACHE_ENABLED);
    }

    /**
     * Return all available external cache controls
     *
     * @return array
     */
    public function getCacheControls()
    {
        $controls = Mage::app()->getConfig()->getNode(self::XML_PATH_EXTERNAL_CACHE_CONTROLS);
        return $controls->asCanonicalArray();
    }

    /**
     * Initialize proper external cache control model
     *
     * @throws Mage_Core_Exception
     * @return Mage_PageCache_Model_Control_Interface
     */
    public function getCacheControlInstance()
    {
        $usedControl = Mage::getStoreConfig(self::XML_PATH_EXTERNAL_CACHE_CONTROL);
        if ($usedControl) {
            foreach ($this->getCacheControls() as $control => $info) {
                if ($control == $usedControl && !empty($info['class'])) {
                    return Mage::getSingleton($info['class']);
                }
            }
        }
        Mage::throwException($this->__('Failed to load external cache control'));
    }

    /**
     * Disable caching on external storage side by setting special cookie
     *
     * @return void
     */
    public function setNoCacheCookie()
    {
        $cookie   = Mage::getSingleton('core/cookie');
        $lifetime = Mage::getStoreConfig(self::XML_PATH_EXTERNAL_CACHE_LIFETIME);
        $noCache  = $cookie->get(self::NO_CACHE_COOKIE);

        if ($noCache) {
            $cookie->renew(self::NO_CACHE_COOKIE, $lifetime);
        } else {
            $cookie->set(self::NO_CACHE_COOKIE, 1, $lifetime);
        }
    }

    /**
     * Returns a lifetime of cookie for external cache
     *
     * @return string Time in seconds
     */
    public function getNoCacheCookieLifetime()
    {
        return Mage::getStoreConfig(self::XML_PATH_EXTERNAL_CACHE_LIFETIME);
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Persistent
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Persistent Observer
 *
 * @category   Mage
 * @package    Mage_Persistent
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Mage_Persistent_Model_Observer
{
    /**
     * Whether set quote to be persistent in workflow
     *
     * @var bool
     */
    protected $_setQuotePersistent = true;

    /**
     * Apply persistent data
     *
     * @param Varien_Event_Observer $observer
     * @return Mage_Persistent_Model_Observer
     */
    public function applyPersistentData($observer)
    {
        if (!Mage::helper('persistent')->canProcess($observer)
            || !$this->_getPersistentHelper()->isPersistent() || Mage::getSingleton('customer/session')->isLoggedIn()) {
            return $this;
        }
        Mage::getModel('persistent/persistent_config')
            ->setConfigFilePath(Mage::helper('persistent')->getPersistentConfigFilePath())
            ->fire();
        return $this;
    }

    /**
     * Apply persistent data to specific block
     *
     * @param Varien_Event_Observer $observer
     * @return Mage_Persistent_Model_Observer
     */
    public function applyBlockPersistentData($observer)
    {
        if (!$this->_getPersistentHelper()->isPersistent() || Mage::getSingleton('customer/session')->isLoggedIn()) {
            return $this;
        }

        /** @var $block Mage_Core_Block_Abstract */
        $block = $observer->getEvent()->getBlock();

        if (!$block) {
            return $this;
        }

        $xPath = '//instances/blocks/*[block_type="' . get_class($block) . '"]';
        $configFilePath = $observer->getEvent()->getConfigFilePath();

        /** @var $persistentConfig Mage_Persistent_Model_Persistent_Config */
        $persistentConfig = Mage::getModel('persistent/persistent_config')
            ->setConfigFilePath(
                $configFilePath ? $configFilePath : Mage::helper('persistent')->getPersistentConfigFilePath()
            );

        foreach ($persistentConfig->getXmlConfig()->xpath($xPath) as $persistentConfigInfo) {
            $persistentConfig->fireOne($persistentConfigInfo->asArray(), $block);
        }

        return $this;
    }
    /**
     * Emulate welcome message with persistent data
     *
     * @param Mage_Core_Block_Abstract $block
     * @return Mage_Persistent_Model_Observer
     */
    public function emulateWelcomeMessageBlock($block)
    {
        $block->setWelcome(
            Mage::helper('persistent')->__('Welcome, %s!', Mage::helper('core')->escapeHtml($this->_getPersistentCustomer()->getName(), null))
        );
        return $this;
    }
    /**
     * Emulate 'welcome' block with persistent data
     *
     * @param Mage_Core_Block_Abstract $block
     * @return Mage_Persistent_Model_Observer
     */
    public function emulateWelcomeBlock($block)
    {
        $this->_applyAccountLinksPersistentData();
        $block->setAdditionalHtml(Mage::app()->getLayout()->getBlock('header.additional')->toHtml());

        return $this;
    }

    /**
     * Emulate 'account links' block with persistent data
     */
    protected function _applyAccountLinksPersistentData()
    {
        if (!Mage::app()->getLayout()->getBlock('header.additional')) {
            Mage::app()->getLayout()->addBlock('persistent/header_additional', 'header.additional');
        }
    }

    /**
     * Emulate 'account links' block with persistent data
     *
     * @param Mage_Core_Block_Abstract $block
     */
    public function emulateAccountLinks($block)
    {
        $this->_applyAccountLinksPersistentData();
        $block->getCacheKeyInfo();
        $block->addLink(
            Mage::helper('persistent')->getPersistentName(),
            Mage::helper('persistent')->getUnsetCookieUrl(),
            Mage::helper('persistent')->getPersistentName(),
            false,
            array(),
            110
        );
        $block->removeLinkByUrl(Mage::helper('customer')->getRegisterUrl());
        $block->removeLinkByUrl(Mage::helper('customer')->getLoginUrl());
    }

    /**
     * Emulate 'top links' block with persistent data
     *
     * @param Mage_Core_Block_Abstract $block
     */
    public function emulateTopLinks($block)
    {
        $this->_applyAccountLinksPersistentData();
    }

    /**
     * Emulate quote by persistent data
     *
     * @param Varien_Event_Observer $observer
     */
    public function emulateQuote($observer)
    {
        $stopActions = array(
            'persistent_index_saveMethod',
            'customer_account_createpost'
        );

        if (!Mage::helper('persistent')->canProcess($observer)
            || !$this->_getPersistentHelper()->isPersistent() || Mage::getSingleton('customer/session')->isLoggedIn()) {
            return;
        }

        /** @var $action Mage_Checkout_OnepageController */
        $action = $observer->getEvent()->getControllerAction();
        $actionName = $action->getFullActionName();

        if (in_array($actionName, $stopActions)) {
            return;
        }

        /** @var $checkoutSession Mage_Checkout_Model_Session */
        $checkoutSession = Mage::getSingleton('checkout/session');
        if ($this->_isShoppingCartPersist()) {
            $checkoutSession->setCustomer($this->_getPersistentCustomer());
            if (!$checkoutSession->hasQuote()) {
                $checkoutSession->getQuote();
            }
        }
    }

    /**
     * Set persistent data into quote
     *
     * @param Varien_Event_Observer $observer
     */
    public function setQuotePersistentData($observer)
    {
        if (!$this->_isPersistent()) {
            return;
        }

        /** @var $quote Mage_Sales_Model_Quote */
        $quote = $observer->getEvent()->getQuote();
        if (!$quote) {
            return;
        }

        if ($this->_isGuestShoppingCart() && $this->_setQuotePersistent) {
            //Quote is not actual customer's quote, just persistent
            $quote->setIsActive(false)->setIsPersistent(true);
        }
    }

    /**
     * Set quote to be loaded even if not active
     *
     * @param Varien_Event_Observer $observer
     */
    public function setLoadPersistentQuote($observer)
    {
        if (!$this->_isGuestShoppingCart()) {
            return;
        }

        /** @var $checkoutSession Mage_Checkout_Model_Session */
        $checkoutSession = $observer->getEvent()->getCheckoutSession();
        if ($checkoutSession) {
            $checkoutSession->setLoadInactive();
        }
    }

    /**
     * Prevent clear checkout session
     *
     * @param Varien_Event_Observer $observer
     */
    public function preventClearCheckoutSession($observer)
    {
        $action = $this->_checkClearCheckoutSessionNecessity($observer);

        if ($action) {
            $action->setClearCheckoutSession(false);
        }
    }

    /**
     * Make persistent quote to be guest
     *
     * @param Varien_Event_Observer $observer
     */
    public function makePersistentQuoteGuest($observer)
    {
        if (!$this->_checkClearCheckoutSessionNecessity($observer)) {
            return;
        }

        $this->setQuoteGuest(true);
    }

    /**
     * Check if checkout session should NOT be cleared
     *
     * @param Varien_Event_Observer $observer
     * @return bool|Mage_Persistent_IndexController
     */
    protected function _checkClearCheckoutSessionNecessity($observer)
    {
        if (!$this->_isGuestShoppingCart()) {
            return false;
        }

        /** @var $action Mage_Persistent_IndexController */
        $action = $observer->getEvent()->getControllerAction();
        if ($action instanceof Mage_Persistent_IndexController) {
            return $action;
        }

        return false;
    }

    /**
     * Reset session data when customer re-authenticates
     *
     * @param Varien_Event_Observer $observer
     */
    public function customerAuthenticatedEvent($observer)
    {
        /** @var $customerSession Mage_Customer_Model_Session */
        $customerSession = Mage::getSingleton('customer/session');
        $customerSession->setCustomerId(null)->setCustomerGroupId(null);

        if (Mage::app()->getRequest()->getParam('context') != 'checkout') {
            $this->_expirePersistentSession();
            return;
        }

        $this->setQuoteGuest();
    }

    /**
     * Unset persistent cookie and make customer's quote as a guest
     *
     * @param Varien_Event_Observer $observer
     */
    public function removePersistentCookie($observer)
    {
        if (!Mage::helper('persistent')->canProcess($observer) || !$this->_isPersistent()) {
            return;
        }

        $this->_getPersistentHelper()->getSession()->removePersistentCookie();
        /** @var $customerSession Mage_Customer_Model_Session */
        $customerSession = Mage::getSingleton('customer/session');
        if (!$customerSession->isLoggedIn()) {
            $customerSession->setCustomerId(null)->setCustomerGroupId(null);
        }

        $this->setQuoteGuest();
    }

    /**
     * Disable guest checkout if we are in persistent mode
     *
     * @param Varien_Event_Observer $observer
     */
    public function disableGuestCheckout($observer)
    {
        if ($this->_getPersistentHelper()->isPersistent()) {
            $observer->getEvent()->getResult()->setIsAllowed(false);
        }
    }

    /**
     * Prevent express checkout with PayPal Express checkout
     *
     * @param Varien_Event_Observer $observer
     */
    public function preventExpressCheckout($observer)
    {
        if (!$this->_isLoggedOut()) {
            return;
        }

        /** @var $controllerAction Mage_Core_Controller_Front_Action */
        $controllerAction = $observer->getEvent()->getControllerAction();
        if (method_exists($controllerAction, 'redirectLogin')) {
            Mage::getSingleton('core/session')->addNotice(
                Mage::helper('persistent')->__('To proceed to Checkout, please log in using your email address.')
            );
            $controllerAction->redirectLogin();
            if ($controllerAction instanceof Mage_Paypal_Controller_Express_Abstract) {
                Mage::getSingleton('customer/session')
                    ->setBeforeAuthUrl(Mage::getUrl('persistent/index/expressCheckout'));
            }
        }
    }

    /**
     * Retrieve persistent customer instance
     *
     * @return Mage_Customer_Model_Customer
     */
    protected function _getPersistentCustomer()
    {
        return Mage::getModel('customer/customer')->load(
            $this->_getPersistentHelper()->getSession()->getCustomerId()
        );
    }

    /**
     * Retrieve persistent helper
     *
     * @return Mage_Persistent_Helper_Session
     */
    protected function _getPersistentHelper()
    {
        return Mage::helper('persistent/session');
    }

    /**
     * Return current active quote for persistent customer
     *
     * @return Mage_Sales_Model_Quote
     */
    protected function _getQuote()
    {
        $quote = Mage::getModel('sales/quote');
        $quote->loadByCustomer($this->_getPersistentCustomer());
        return $quote;
    }

    /**
     * Check whether shopping cart is persistent
     *
     * @return bool
     */
    protected function _isShoppingCartPersist()
    {
        return Mage::helper('persistent')->isShoppingCartPersist();
    }

    /**
     * Check whether persistent mode is running
     *
     * @return bool
     */
    protected function _isPersistent()
    {
        return $this->_getPersistentHelper()->isPersistent();
    }

    /**
     * Check if persistent mode is running and customer is logged out
     *
     * @return bool
     */
    protected function _isLoggedOut()
    {
        return $this->_isPersistent() && !Mage::getSingleton('customer/session')->isLoggedIn();
    }

    /**
     * Check if shopping cart is guest while persistent session and user is logged out
     *
     * @return bool
     */
    protected function _isGuestShoppingCart()
    {
        return $this->_isLoggedOut() && !Mage::helper('persistent')->isShoppingCartPersist();
    }

    /**
     * Make quote to be guest
     *
     * @param bool $checkQuote Check quote to be persistent (not stolen)
     */
    public function setQuoteGuest($checkQuote = false)
    {
        /** @var $quote Mage_Sales_Model_Quote */
        $quote = Mage::getSingleton('checkout/session')->getQuote();
        if ($quote && $quote->getId()) {
            if ($checkQuote && !Mage::helper('persistent')->isShoppingCartPersist() && !$quote->getIsPersistent()) {
                Mage::getSingleton('checkout/session')->unsetAll();
                return;
            }

            $quote->getPaymentsCollection()->walk('delete');
            $quote->getAddressesCollection()->walk('delete');
            $this->_setQuotePersistent = false;
            $quote
                ->setIsActive(true)
                ->setCustomerId(null)
                ->setCustomerEmail(null)
                ->setCustomerFirstname(null)
                ->setCustomerMiddlename(null)
                ->setCustomerLastname(null)
                ->setCustomerGroupId(Mage_Customer_Model_Group::NOT_LOGGED_IN_ID)
                ->setIsPersistent(false)
                ->removeAllAddresses();
            //Create guest addresses
            $quote->getShippingAddress();
            $quote->getBillingAddress();
            $quote->collectTotals()->save();
        }

        $this->_getPersistentHelper()->getSession()->removePersistentCookie();
    }

    /**
     * Check and clear session data if persistent session expired
     *
     * @param Varien_Event_Observer $observer
     */
    public function checkExpirePersistentQuote(Varien_Event_Observer $observer)
    {
        if (!Mage::helper('persistent')->canProcess($observer)) {
            return;
        }

        /** @var $customerSession Mage_Customer_Model_Session */
        $customerSession = Mage::getSingleton('customer/session');

        if (Mage::helper('persistent')->isEnabled()
            && !$this->_isPersistent()
            && !$customerSession->isLoggedIn()
            && Mage::getSingleton('checkout/session')->getQuoteId()
            && !($observer->getControllerAction() instanceof Mage_Checkout_OnepageController)
            // persistent session does not expire on onepage checkout page to not spoil customer group id
        ) {
            Mage::dispatchEvent('persistent_session_expired');
            $this->_expirePersistentSession();
            $customerSession->setCustomerId(null)->setCustomerGroupId(null);
        }
    }
    /**
     * Active Persistent Sessions
     */
    protected function _expirePersistentSession()
    {
        /** @var $checkoutSession Mage_Checkout_Model_Session */
        $checkoutSession = Mage::getSingleton('checkout/session');

        $quote = $checkoutSession->setLoadInactive()->getQuote();
        if ($quote->getIsActive() && $quote->getCustomerId()) {
            $checkoutSession->setCustomer(null)->unsetAll();
        } else {
            $quote
                ->setIsActive(true)
                ->setIsPersistent(false)
                ->setCustomerId(null)
                ->setCustomerGroupId(Mage_Customer_Model_Group::NOT_LOGGED_IN_ID);
        }
    }

    /**
     * Clear expired persistent sessions
     *
     * @param Mage_Cron_Model_Schedule $schedule
     * @return Mage_Persistent_Model_Observer_Cron
     */
    public function clearExpiredCronJob(Mage_Cron_Model_Schedule $schedule)
    {
        $websiteIds = Mage::getResourceModel('core/website_collection')->getAllIds();
        if (!is_array($websiteIds)) {
            return $this;
        }

        foreach ($websiteIds as $websiteId) {
            Mage::getModel('persistent/session')->deleteExpired($websiteId);
        }

        return $this;
    }

    /**
     * Create handle for persistent session if persistent cookie and customer not logged in
     *
     * @param Varien_Event_Observer $observer
     */
    public function createPersistentHandleLayout(Varien_Event_Observer $observer)
    {
        /** @var $layout Mage_Core_Model_Layout */
        $layout = $observer->getEvent()->getLayout();
        if (Mage::helper('persistent')->canProcess($observer) && $layout && Mage::helper('persistent')->isEnabled()
            && Mage::helper('persistent/session')->isPersistent()
        ) {
            $handle = (Mage::getSingleton('customer/session')->isLoggedIn())
                ? Mage_Persistent_Helper_Data::LOGGED_IN_LAYOUT_HANDLE
                : Mage_Persistent_Helper_Data::LOGGED_OUT_LAYOUT_HANDLE;
            $layout->getUpdate()->addHandle($handle);
        }
    }

    /**
     * Update customer id and customer group id if user is in persistent session
     *
     * @param Varien_Event_Observer $observer
     */
    public function updateCustomerCookies(Varien_Event_Observer $observer)
    {
        if (!$this->_isPersistent()) {
            return;
        }

        $customerCookies = $observer->getEvent()->getCustomerCookies();
        if ($customerCookies instanceof Varien_Object) {
            $persistentCustomer = $this->_getPersistentCustomer();
            $customerCookies->setCustomerId($persistentCustomer->getId());
            $customerCookies->setCustomerGroupId($persistentCustomer->getGroupId());
        }
    }

    /**
     * Set persistent data to customer session
     *
     * @param Varien_Event_Observer $observer
     * @return Mage_Persistent_Model_Observer
     */
    public function emulateCustomer($observer)
    {
        if (!Mage::helper('persistent')->canProcess($observer)
            || !$this->_isShoppingCartPersist()
        ) {
            return $this;
        }

        if ($this->_isLoggedOut()) {
            /** @var $customer Mage_Customer_Model_Customer */
            $customer = Mage::getModel('customer/customer')->load(
                $this->_getPersistentHelper()->getSession()->getCustomerId()
            );
            Mage::getSingleton('customer/session')
                ->setCustomerId($customer->getId())
                ->setCustomerGroupId($customer->getGroupId());
        }
        return $this;
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Persistent
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Persistent Shopping Cart Data Helper
 *
 * @category   Mage
 * @package    Mage_Persistent
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Mage_Persistent_Helper_Data extends Mage_Core_Helper_Data
{
    const XML_PATH_ENABLED = 'persistent/options/enabled';
    const XML_PATH_LIFE_TIME = 'persistent/options/lifetime';
    const XML_PATH_LOGOUT_CLEAR = 'persistent/options/logout_clear';
    const XML_PATH_REMEMBER_ME_ENABLED = 'persistent/options/remember_enabled';
    const XML_PATH_REMEMBER_ME_DEFAULT = 'persistent/options/remember_default';
    const XML_PATH_PERSIST_SHOPPING_CART = 'persistent/options/shopping_cart';

    const LOGGED_IN_LAYOUT_HANDLE = 'customer_logged_in_psc_handle';
    const LOGGED_OUT_LAYOUT_HANDLE = 'customer_logged_out_psc_handle';

    /**
     * Name of config file
     *
     * @var string
     */
    protected $_configFileName = 'persistent.xml';

    /**
     * Checks whether Persistence Functionality is enabled
     *
     * @param int|string|Mage_Core_Model_Store $store
     * @return bool
     */
    public function isEnabled($store = null)
    {
        return Mage::getStoreConfigFlag(self::XML_PATH_ENABLED, $store);
    }

    /**
     * Checks whether "Remember Me" enabled
     *
     * @param int|string|Mage_Core_Model_Store $store
     * @return bool
     */
    public function isRememberMeEnabled($store = null)
    {
        return Mage::getStoreConfigFlag(self::XML_PATH_REMEMBER_ME_ENABLED, $store);
    }

    /**
     * Is "Remember Me" checked by default
     *
     * @param int|string|Mage_Core_Model_Store $store
     * @return bool
     */
    public function isRememberMeCheckedDefault($store = null)
    {
        return Mage::getStoreConfigFlag(self::XML_PATH_REMEMBER_ME_DEFAULT, $store);
    }

    /**
     * Is shopping cart persist
     *
     * @param int|string|Mage_Core_Model_Store $store
     * @return bool
     */
    public function isShoppingCartPersist($store = null)
    {
        return Mage::getStoreConfigFlag(self::XML_PATH_PERSIST_SHOPPING_CART, $store);
    }

    /**
     * Get Persistence Lifetime
     *
     * @param int|string|Mage_Core_Model_Store $store
     * @return int
     */
    public function getLifeTime($store = null)
    {
        $lifeTime = intval(Mage::getStoreConfig(self::XML_PATH_LIFE_TIME, $store));
        return ($lifeTime < 0) ? 0 : $lifeTime;
    }

    /**
     * Check if set `Clear on Logout` in config settings
     *
     * @return bool
     */
    public function getClearOnLogout()
    {
        return Mage::getStoreConfigFlag(self::XML_PATH_LOGOUT_CLEAR);
    }

    /**
     * Retrieve url for unset long-term cookie
     *
     * @return string
     */
    public function getUnsetCookieUrl()
    {
        return $this->_getUrl('persistent/index/unsetCookie');
    }

    /**
     * Retrieve name of persistent customer
     *
     * @return string
     */
    public function getPersistentName()
    {
        return $this->__('(Not %s?)', $this->escapeHtml(Mage::helper('persistent/session')->getCustomer()->getName()));
    }

    /**
     * Retrieve path for config file
     *
     * @return string
     */
    public function getPersistentConfigFilePath()
    {
        return Mage::getConfig()->getModuleDir('etc', $this->_getModuleName()) . DS . $this->_configFileName;
    }

    /**
     * Check whether specified action should be processed
     *
     * @param Varien_Event_Observer $observer
     * @return bool
     */
    public function canProcess($observer)
    {
        $action = $observer->getEvent()->getAction();
        $controllerAction = $observer->getEvent()->getControllerAction();

        if ($action instanceof Mage_Core_Controller_Varien_Action) {
            return !$action->getFlag('', Mage_Core_Controller_Varien_Action::FLAG_NO_START_SESSION);
        }
        if ($controllerAction instanceof Mage_Core_Controller_Varien_Action) {
            return !$controllerAction->getFlag('', Mage_Core_Controller_Varien_Action::FLAG_NO_START_SESSION);
        }
        return true;
    }

    /**
     * Get create account url depends on checkout
     *
     * @param  $url string
     * @return string
     */
    public function getCreateAccountUrl($url)
    {
        if (Mage::helper('checkout')->isContextCheckout()) {
            $url = Mage::helper('core/url')->addRequestParam($url, array('context' => 'checkout'));
        }
        return $url;
    }

}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Persistent
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Persistent Shopping Cart Data Helper
 *
 * @category   Mage
 * @package    Mage_Persistent
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Mage_Persistent_Helper_Session extends Mage_Core_Helper_Data
{
    /**
     * Instance of Session Model
     *
     * @var null|Mage_Persistent_Model_Session
     */
    protected $_sessionModel;

    /**
     * Persistent customer
     *
     * @var null|Mage_Customer_Model_Customer
     */
    protected $_customer;

    /**
     * Is "Remember Me" checked
     *
     * @var null|bool
     */
    protected $_isRememberMeChecked;

    /**
     * Get Session model
     *
     * @return Mage_Persistent_Model_Session
     */
    public function getSession()
    {
        if (is_null($this->_sessionModel)) {
            $this->_sessionModel = Mage::getModel('persistent/session');
            $this->_sessionModel->loadByCookieKey();
        }
        return $this->_sessionModel;
    }

    /**
     * Force setting session model
     *
     * @param Mage_Persistent_Model_Session $sessionModel
     * @return Mage_Persistent_Model_Session
     */
    public function setSession($sessionModel)
    {
        $this->_sessionModel = $sessionModel;
        return $this->_sessionModel;
    }

    /**
     * Check whether persistent mode is running
     *
     * @return bool
     */
    public function isPersistent()
    {
        return $this->getSession()->getId() && Mage::helper('persistent')->isEnabled();
    }

    /**
     * Check if "Remember Me" checked
     *
     * @return bool
     */
    public function isRememberMeChecked()
    {
        if (is_null($this->_isRememberMeChecked)) {
            //Try to get from checkout session
            $isRememberMeChecked = Mage::getSingleton('checkout/session')->getRememberMeChecked();
            if (!is_null($isRememberMeChecked)) {
                $this->_isRememberMeChecked = $isRememberMeChecked;
                Mage::getSingleton('checkout/session')->unsRememberMeChecked();
                return $isRememberMeChecked;
            }

            /** @var $helper Mage_Persistent_Helper_Data */
            $helper = Mage::helper('persistent');
            return $helper->isEnabled() && $helper->isRememberMeEnabled() && $helper->isRememberMeCheckedDefault();
        }

        return (bool)$this->_isRememberMeChecked;
    }

    /**
     * Set "Remember Me" checked or not
     *
     * @param bool $checked
     */
    public function setRememberMeChecked($checked = true)
    {
        $this->_isRememberMeChecked = $checked;
    }

    /**
     * Return persistent customer
     *
     * @return Mage_Customer_Model_Customer|bool
     */
    public function getCustomer()
    {
        if (is_null($this->_customer)) {
            $customerId = $this->getSession()->getCustomerId();
            $this->_customer = Mage::getModel('customer/customer')->load($customerId);
        }
        return $this->_customer;
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Persistent
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Persistent Session Model
 *
 * @category   Mage
 * @package    Mage_Persistent
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Mage_Persistent_Model_Session extends Mage_Core_Model_Abstract
{
    const KEY_LENGTH = 50;
    const COOKIE_NAME = 'persistent_shopping_cart';

    /**
     * Fields which model does not save into `info` db field
     *
     * @var array
     */
    protected $_unserializableFields = array('persistent_id', 'key', 'customer_id', 'website_id', 'info', 'updated_at');

    /**
     * If model loads expired sessions
     *
     * @var bool
     */
    protected $_loadExpired = false;

    /**
     * Define resource model
     */
    protected function _construct()
    {
        $this->_init('persistent/session');
    }

    /**
     * Set if load expired persistent session
     *
     * @param bool $loadExpired
     * @return Mage_Persistent_Model_Session
     */
    public function setLoadExpired($loadExpired = true)
    {
        $this->_loadExpired = $loadExpired;
        return $this;
    }

    /**
     * Get if model loads expired sessions
     *
     * @return bool
     */
    public function getLoadExpired()
    {
        return $this->_loadExpired;
    }

    /**
     * Get date-time before which persistent session is expired
     *
     * @param int|string|Mage_Core_Model_Store $store
     * @return string
     */
    public function getExpiredBefore($store = null)
    {
        return gmdate('Y-m-d H:i:s', time() - Mage::helper('persistent')->getLifeTime($store));
    }

    /**
     * Serialize info for Resource Model to save
     * For new model check and set available cookie key
     *
     * @return Mage_Persistent_Model_Session
     */
    protected function _beforeSave()
    {
        parent::_beforeSave();

        // Setting info
        $info = array();
        foreach ($this->getData() as $index => $value) {
            if (!in_array($index, $this->_unserializableFields)) {
                $info[$index] = $value;
            }
        }
        $this->setInfo(Mage::helper('core')->jsonEncode($info));

        if ($this->isObjectNew()) {
            $this->setWebsiteId(Mage::app()->getStore()->getWebsiteId());
            // Setting cookie key
            do {
                $this->setKey(Mage::helper('core')->getRandomString(self::KEY_LENGTH));
            } while (!$this->getResource()->isKeyAllowed($this->getKey()));
        }

        return $this;
    }

    /**
     * Set model data from info field
     *
     * @return Mage_Persistent_Model_Session
     */
    protected function _afterLoad()
    {
        parent::_afterLoad();
        $info = Mage::helper('core')->jsonDecode($this->getInfo());
        if (is_array($info)) {
            foreach ($info as $key => $value) {
                $this->setData($key, $value);
            }
        }
        return $this;
    }

    /**
     * Get persistent session by cookie key
     *
     * @param string $key
     * @return Mage_Persistent_Model_Session
     */
    public function loadByCookieKey($key = null)
    {
        if (is_null($key)) {
            $key = Mage::getSingleton('core/cookie')->get(Mage_Persistent_Model_Session::COOKIE_NAME);
        }
        if ($key) {
            $this->load($key, 'key');
        }

        return $this;
    }

    /**
     * Load session model by specified customer id
     *
     * @param int $id
     * @return Mage_Core_Model_Abstract
     */
    public function loadByCustomerId($id)
    {
        return $this->load($id, 'customer_id');
    }

    /**
     * Delete customer persistent session by customer id
     *
     * @param int $customerId
     * @param bool $clearCookie
     * @return Mage_Persistent_Model_Session
     */
    public function deleteByCustomerId($customerId, $clearCookie = true)
    {
        if ($clearCookie) {
            $this->removePersistentCookie();
        }
        $this->getResource()->deleteByCustomerId($customerId);
        return $this;
    }

    /**
     * Remove persistent cookie
     *
     * @return Mage_Persistent_Model_Session
     */
    public function removePersistentCookie()
    {
        Mage::getSingleton('core/cookie')->delete(Mage_Persistent_Model_Session::COOKIE_NAME);
        return $this;
    }

    /**
     * Delete expired persistent sessions for the website
     *
     * @param null|int $websiteId
     * @return Mage_Persistent_Model_Session
     */
    public function deleteExpired($websiteId = null)
    {
        if (is_null($websiteId)) {
            $websiteId = Mage::app()->getStore()->getWebsiteId();
        }

        $lifetime = Mage::getConfig()->getNode(
            Mage_Persistent_Helper_Data::XML_PATH_LIFE_TIME,
            'website',
            intval($websiteId)
        );

        if ($lifetime) {
            $this->getResource()->deleteExpired(
                $websiteId,
                gmdate('Y-m-d H:i:s', time() - $lifetime)
            );
        }

        return $this;
    }

    /**
     * Delete 'persistent' cookie
     *
     * @return Mage_Core_Model_Abstract
     */
    protected function _afterDeleteCommit() {
        Mage::getSingleton('core/cookie')->delete(Mage_Persistent_Model_Session::COOKIE_NAME);
        return parent::_afterDeleteCommit();
    }

    /**
     * Set `updated_at` to be always changed
     *
     * @return Mage_Persistent_Model_Session
     */
    public function save()
    {
        $this->setUpdatedAt(gmdate('Y-m-d H:i:s'));
        return parent::save();
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Persistent
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Persistent Session Resource Model
 *
 * @category    Mage
 * @package     Mage_Persistent
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Persistent_Model_Resource_Session extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Use is object new method for object saving
     *
     * @var boolean
     */
    protected $_useIsObjectNew = true;

    /**
     * Initialize connection and define main table and primary key
     */
    protected function _construct()
    {
        $this->_init('persistent/session', 'persistent_id');
    }

    /**
     * Add expiration date filter to select
     *
     * @param string $field
     * @param mixed $value
     * @param Mage_Persistent_Model_Session $object
     * @return Zend_Db_Select
     */
    protected function _getLoadSelect($field, $value, $object)
    {
        $select = parent::_getLoadSelect($field, $value, $object);
        if (!$object->getLoadExpired()) {
            $tableName = $this->getMainTable();
            $select->join(array('customer' => $this->getTable('customer/entity')),
                'customer.entity_id = ' . $tableName . '.customer_id'
            )->where($tableName . '.updated_at >= ?', $object->getExpiredBefore());
        }

        return $select;
    }

    /**
     * Delete customer persistent session by customer id
     *
     * @param int $customerId
     * @return Mage_Persistent_Model_Resource_Session
     */
    public function deleteByCustomerId($customerId)
    {
        $this->_getWriteAdapter()->delete($this->getMainTable(), array('customer_id = ?' => $customerId));
        return $this;
    }

    /**
     * Check if such session key allowed (not exists)
     *
     * @param string $key
     * @return bool
     */
    public function isKeyAllowed($key)
    {
        $sameSession = Mage::getModel('persistent/session')->setLoadExpired();
        $sameSession->loadByCookieKey($key);
        return !$sameSession->getId();
    }

    /**
     * Delete expired persistent sessions
     *
     * @param  $websiteId
     * @param  $expiredBefore
     * @return Mage_Persistent_Model_Resource_Session
     */
    public function deleteExpired($websiteId, $expiredBefore)
    {
        $this->_getWriteAdapter()->delete(
            $this->getMainTable(),
            array(
                'website_id = ?' => $websiteId,
                'updated_at < ?' => $expiredBefore,
            )
        );
        return $this;
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Persistent
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Persistent Session Observer
 *
 * @category   Mage
 * @package    Mage_Persistent
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Mage_Persistent_Model_Observer_Session
{
    /**
     * Create/Update and Load session when customer log in
     *
     * @param Varien_Event_Observer $observer
     */
    public function synchronizePersistentOnLogin(Varien_Event_Observer $observer)
    {
        /** @var $customer Mage_Customer_Model_Customer */
        $customer = $observer->getEvent()->getCustomer();
        // Check if customer is valid (remove persistent cookie for invalid customer)
        if (!$customer || !$customer->getId() || !Mage::helper('persistent/session')->isRememberMeChecked()) {
            Mage::getModel('persistent/session')->removePersistentCookie();
            return;
        }

        $persistentLifeTime = Mage::helper('persistent')->getLifeTime();
        // Delete persistent session, if persistent could not be applied
        if (Mage::helper('persistent')->isEnabled() && ($persistentLifeTime <= 0)) {
            // Remove current customer persistent session
            Mage::getModel('persistent/session')->deleteByCustomerId($customer->getId());
            return;
        }

        /** @var $sessionModel Mage_Persistent_Model_Session */
        $sessionModel = Mage::helper('persistent/session')->getSession();

        // Check if session is wrong or not exists, so create new session
        if (!$sessionModel->getId() || ($sessionModel->getCustomerId() != $customer->getId())) {
            $sessionModel = Mage::getModel('persistent/session')
                ->setLoadExpired()
                ->loadByCustomerId($customer->getId());
            if (!$sessionModel->getId()) {
                $sessionModel = Mage::getModel('persistent/session')
                    ->setCustomerId($customer->getId())
                    ->save();
            }

            Mage::helper('persistent/session')->setSession($sessionModel);
        }

        // Set new cookie
        if ($sessionModel->getId()) {
            Mage::getSingleton('core/cookie')->set(
                Mage_Persistent_Model_Session::COOKIE_NAME,
                $sessionModel->getKey(),
                $persistentLifeTime
            );
        }
    }

    /**
     * Unload persistent session (if set in config)
     *
     * @param Varien_Event_Observer $observer
     */
    public function synchronizePersistentOnLogout(Varien_Event_Observer $observer)
    {
        if (!Mage::helper('persistent')->isEnabled() || !Mage::helper('persistent')->getClearOnLogout()) {
            return;
        }

        /** @var $customer Mage_Customer_Model_Customer */
        $customer = $observer->getEvent()->getCustomer();
        // Check if customer is valid
        if (!$customer || !$customer->getId()) {
            return;
        }

        Mage::getModel('persistent/session')->removePersistentCookie();

        // Unset persistent session
        Mage::helper('persistent/session')->setSession(null);
    }

    /**
     * Synchronize persistent session info
     *
     * @param Varien_Event_Observer $observer
     */
    public function synchronizePersistentInfo(Varien_Event_Observer $observer)
    {
        if (!Mage::helper('persistent')->isEnabled() || !Mage::helper('persistent/session')->isPersistent()) {
            return;
        }

        /** @var $sessionModel Mage_Persistent_Model_Session */
        $sessionModel = Mage::helper('persistent/session')->getSession();

        /** @var $request Mage_Core_Controller_Request_Http */
        $request = $observer->getEvent()->getFront()->getRequest();

        // Quote Id could be changed only by logged in customer
        if (Mage::getSingleton('customer/session')->isLoggedIn()
            || ($request && $request->getActionName() == 'logout' && $request->getControllerName() == 'account')
        ) {
            $sessionModel->save();
        }
    }

    /**
     * Set Checked status of "Remember Me"
     *
     * @param Varien_Event_Observer $observer
     */
    public function setRememberMeCheckedStatus(Varien_Event_Observer $observer)
    {
        if (!Mage::helper('persistent')->canProcess($observer)
            || !Mage::helper('persistent')->isEnabled() || !Mage::helper('persistent')->isRememberMeEnabled()
        ) {
            return;
        }

        /** @var $controllerAction Mage_Core_Controller_Varien_Action */
        $controllerAction = $observer->getEvent()->getControllerAction();
        if ($controllerAction) {
            $rememberMeCheckbox = $controllerAction->getRequest()->getPost('persistent_remember_me');
            Mage::helper('persistent/session')->setRememberMeChecked((bool)$rememberMeCheckbox);
            if (
                $controllerAction->getFullActionName() == 'checkout_onepage_saveBilling'
                    || $controllerAction->getFullActionName() == 'customer_account_createpost'
            ) {
                Mage::getSingleton('checkout/session')->setRememberMeChecked((bool)$rememberMeCheckbox);
            }
        }
    }

    /**
     * Renew persistent cookie
     *
     * @param Varien_Event_Observer $observer
     */
    public function renewCookie(Varien_Event_Observer $observer)
    {
        if (!Mage::helper('persistent')->canProcess($observer)
            || !Mage::helper('persistent')->isEnabled() || !Mage::helper('persistent/session')->isPersistent()
        ) {
            return;
        }

        /** @var $controllerAction Mage_Core_Controller_Front_Action */
        $controllerAction = $observer->getEvent()->getControllerAction();

        if (Mage::getSingleton('customer/session')->isLoggedIn()
            || $controllerAction->getFullActionName() == 'customer_account_logout'
        ) {
            Mage::getSingleton('core/cookie')->renew(
                Mage_Persistent_Model_Session::COOKIE_NAME,
                Mage::helper('persistent')->getLifeTime()
            );
        }
    }
}

/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Fpc
 */
class Amasty_Fpc_Model_Observer
{
    protected $_showBlockNames = false;
    protected $_showBlockTemplates = false;
    protected $_blockHtmlCache = array();
    protected $_ajaxBlocks = null;

    public function __construct()
    {
        try {
            if (
                Mage::app()->useCache('amfpc')
                &&
                Mage::getStoreConfig('amfpc/debug/block_info')
                &&
                Mage::getSingleton('amfpc/fpc_front')->allowedDebugInfo()
                &&
                !Mage::app()->getStore()->isAdmin()
            ) {
                $this->_showBlockNames = true;
                Mage::app()->getCacheInstance()->banUse('block_html');
            }

            $this->_showBlockTemplates = Mage::getStoreConfig('amfpc/debug/block_templates');
        } catch (Mage_Core_Model_Store_Exception $e) // Stores aren't initialized
        {
            $this->_showBlockNames = $this->_showBlockTemplates = false;
        }
    }

    public function actionPredispatch($observer)
    {
        if (Mage::app()->getStore()->isAdmin())
            return;

        if ($page = Mage::registry('amfpc_page')) {
            $page = Mage::helper('amfpc')->replaceFormKey($page);

            /** @var Amasty_Fpc_Model_Fpc_Front $front */
            $front = Mage::getSingleton('amfpc/fpc_front');
            $front->addBlockInfo($page, 'Session Initialized');

            $response = Mage::app()->getResponse();
            $request = Mage::app()->getRequest();

            $this->setResponse(
                $response,
                $page,
                Amasty_Fpc_Model_Fpc_Front::PAGE_LOAD_HIT_SESSION
            );

            if (0 === strpos($request->getPathInfo(), 'catalog/product/view')) {
                Mage::getModel('reports/product_index_viewed')
                    ->setProductId($request->getParam('id'))
                    ->save()
                    ->calculate();
            }

            Mage::app()->dispatchEvent(
                'controller_action_postdispatch',
                array('controller_action' => $observer->getData('controller_action'))
            );

            $response->sendHeaders();
            $response->outputBody();

            Mage::helper('ambase/utils')->_exit();
        }

        $request = $observer->getData('controller_action')->getRequest();
        Mage::getSingleton('amfpc/fpc')->validateBlocks($request);
    }

    public function afterToHtml($observer)
    {
        if (!Mage::app()->useCache('amfpc'))
            return;

        if (Mage::app()->getRequest()->isAjax() && $this->_ajaxBlocks === null)
            return;

        $block = $observer->getBlock();

        $fpc = Mage::getSingleton('amfpc/fpc');

        $discardedBlocks = $fpc->getDiscardedBlocks();
        $discardedAgents = false;

        foreach ($discardedBlocks as $class => $info) {
            if ($block instanceof $class) {
                if ($info['matched']) {
                    $fpc->setReadonly(true);
                    return;
                }
                $discardedAgents = $info['agents'];
            }
        }

        if ($this->_showBlockNames == false) {
            if (Mage::getStoreConfig('amfpc/general/dynamic_blocks')) {
                /** @var Amasty_Fpc_Model_Config $config */
                $config = Mage::getSingleton('amfpc/config');

                if ($discardedAgents) {
                    $transport = $observer->getTransport();

                    $html = $transport->getHtml();
                    $html = "<!--AMFPC_DISCARD[$discardedAgents]-->$html<!--AMFPC_DISCARD-->";
                    $transport->setHtml($html);
                }

                if ($config->blockIsDynamic($block, $isAjax, $tags, $children)) {
                    $name = $block->getNameInLayout();

                    if (in_array($name, array('global_messages', 'messages')) && $block->getData('amfpc_wrapped'))
                        return;

                    if ($name == 'google_analytics' && $block->getOrderIds())
                        return;

                    $transport = $observer->getTransport();

                    $html = $transport->getHtml();

                    $fpc->saveBlockCache($name, $html, $tags);

                    if (($this->_ajaxBlocks !== null) && array_key_exists($name, $this->_ajaxBlocks)) {
                        $this->_ajaxBlocks[$name] = $html;
                    }

                    if (!$block->getData('amfpc_wrapped')) {
                        $tag = ($isAjax ? 'amfpc_ajax' : 'amfpc');

                        $html = "<$tag name=\"$name\">$html</$tag>";

                        $block->setData('amfpc_wrapped', true);
                    }

                    $transport->setHtml($html);
                } else if (!empty($children)) {
                    $transport = $observer->getTransport();

                    $html = $transport->getHtml();

                    foreach ($children as $childName => $tags) {
                        if (preg_match(
                            '#<amfpc\s*name="' . preg_quote($childName) . '"\s*>(.*?)</amfpc>#s',
                            $html,
                            $matches
                        )
                        ) {
                            $fpc->saveBlockCache($childName, $matches[1], $tags);
                        }
                    }
                }
            }
        } else {
            if ($block instanceof Mage_Core_Block_Template || $block instanceof Mage_Cms_Block_Block) {
                $transport = $observer->getTransport();

                $html = $transport->getHtml();

                if ($this->_showBlockTemplates) {
                    if ($block instanceof Mage_Core_Block_Template) {
                        $template = $block->getTemplateFile();
                    } else {
                        $template = get_class($block);
                    }
                    $templateHint
                        = "<div class=\"amfpc-template-info\">$template</div>";
                } else
                    $templateHint = '';

                $html = <<<HTML
<div class="amfpc-block-info">
    <div class="amfpc-block-handle"
        onmouseover="$(this).parentNode.addClassName('active')"
        onmouseout="$(this).parentNode.removeClassName('active')"
    >{$block->getNameInLayout()}</div>
    $templateHint
    $html
</div>
HTML;

                $transport->setHtml($html);
            }
        }

    }

    public function layoutRenderBefore()
    {
        $request = Mage::app()->getRequest();

        if ($dynamicBlocks = Mage::registry('amfpc_blocks')) {
            $layout = Mage::app()->getLayout();
            $page = $dynamicBlocks['page'];

            Mage::app()->setUseSessionVar(false);

            /** @var Amasty_Fpc_Model_Fpc_Front $front */
            $front = Mage::getSingleton('amfpc/fpc_front');

            foreach ($dynamicBlocks['blocks'] as $name) {
                $blockConfig = Mage::app()->getConfig()->getNode('global/amfpc/blocks/' . $name);
                $parent = (string)$blockConfig['parent'];

                $realName = $parent ? $parent : $name;

                if (!isset($this->_blockHtmlCache[$realName])) {
                    $block = $layout->getBlock($realName);
                    if ($block) {
                        $this->_blockHtmlCache[$realName] = $block->toHtml();
                    }
                }

                if ($parent && isset($this->_blockHtmlCache[$realName])) {
                    if (preg_match(
                        '#<amfpc\s*name="' . preg_quote($name) . '"\s*>(.*?)</amfpc>#s',
                        $this->_blockHtmlCache[$realName],
                        $matches
                    )
                    ) {
                        $this->_blockHtmlCache[$name] = $matches[1];
                    }
                }

                if (isset($this->_blockHtmlCache[$name])) {
                    $blockHtml = $this->_blockHtmlCache[$name];
                    $blockHtml = preg_replace('/<amfpc[^>]*>/', '', $blockHtml);
                    $blockHtml = str_replace('</amfpc>', '', $blockHtml);
                    $blockHtml = str_replace('</amfpc_ajax>', '', $blockHtml);

                    $front->addBlockInfo($blockHtml, $name . ($parent ? "[$parent]" : '') . ' (refresh)');

                    if (preg_match_all(
                        '#<amfpc(_ajax)? name="' . preg_quote($name) . '" />#',
                        $page,
                        $matches,
                        PREG_OFFSET_CAPTURE)
                    ) {
                        for ($i = sizeof($matches[0]) - 1; $i >= 0; $i--) {
                            $page = substr_replace($page, $blockHtml, $matches[0][$i][1], strlen($matches[0][$i][0]));
                        }
                    }
                }
            }

            $front->addBlockInfo($page, 'Late page load');

            if (Mage::registry('amfpc_new_session')) {
                $page = Mage::helper('amfpc')->replaceFormKey($page);
            }

            $response = Mage::app()->getResponse();

            $this->setResponse($response, $page, Amasty_Fpc_Model_Fpc_Front::PAGE_LOAD_HIT_UPDATE);

            Mage::app()->dispatchEvent(
                'controller_action_postdispatch',
                array('controller_action' => Mage::app()->getFrontController()->getAction())
            );

            $response->sendHeaders();
            $response->outputBody();

            Mage::helper('ambase/utils')->_exit();
        } else if ($request->isAjax()) {
            $blocks = $request->getParam('amfpc_ajax_blocks');
            if ($blocks) {
                $blocks = explode(',', $blocks);

                $cmsAjaxBlocks = (bool)(string)Mage::app()->getConfig()->getNode('global/amfpc/cms_ajax_blocks');

                if ($cmsAjaxBlocks) {
                    $this->_ajaxBlocks = array_fill_keys($blocks, null);
                } else {

                    /** @var Amasty_Fpc_Model_Fpc_Front $front */
                    $front = Mage::getSingleton('amfpc/fpc_front');

                    Mage::app()->setUseSessionVar(false);

                    $result = array();
                    $layout = Mage::app()->getLayout();
                    foreach ($blocks as $name) {
                        $block = $layout->getBlock($name);
                        if ($block) {
                            $content = Mage::getSingleton('core/url')->sessionUrlVar($block->toHtml());

                            $front->addBlockInfo($content, $name . ' (ajax)');
                            $result[$name] = $content;
                        }
                    }

                    $blocksJson = Mage::helper('core')->jsonEncode($result);

                    Mage::app()->getResponse()->setBody($blocksJson)->sendResponse();
                    Mage::helper('ambase/utils')->_exit();
                }
            }
        }
    }

    protected function _canPreserve()
    {
        if (!Mage::registry('amfpc_preserve'))
            return false;

        if (Mage::app()->getResponse()->getHttpResponseCode() != 200)
            return false;

        foreach (Mage::app()->getResponse()->getHeaders() as $header) {
            if ($header['name'] == 'Status') {
                if (substr($header['value'], 0, 3) !== '200')
                    return false;
                else
                    break;
            }
        }

        if ($this->_showBlockNames)
            return false;

        if ($layout = Mage::app()->getLayout()) {
            if ($block = $layout->getBlock('messages'))
                if ($block->getMessageCollection()->count() > 0)
                    return false;
            if ($block = $layout->getBlock('global_messages'))
                if ($block->getMessageCollection()->count() > 0)
                    return false;
        } else
            return false;

        return true;
    }

    public function setResponse($response, $html, $status)
    {
        /** @var Amasty_Fpc_Model_Fpc_Front $front */
        $front = Mage::getSingleton('amfpc/fpc_front');

        $html = preg_replace(
            '#(<amfpc[^>]*?>|</amfpc(_ajax)?>)#s',
            '',
            $html
        );

        $front->addLoadTimeInfo($html, $status);

        $response->setBody($html);
    }

    public function onHttpResponseSendBefore($observer)
    {
        if (Mage::app()->getRequest()->getModuleName() == 'api')
            return;

        if (!Mage::app()->useCache('amfpc'))
            return;

        if (Mage::app()->getStore()->isAdmin())
            return;

        if (Mage::app()->getRequest()->isAjax()
            && $this->_ajaxBlocks === null
            && !Mage::getSingleton('amfpc/config')->canSaveAjax()
        )
            return;

        // No modifications in response till here

        Mage::getSingleton('core/session')->getFormKey(); // Init form key

        $page = $observer->getResponse()->getBody();

        if ($ignoreStatus = Mage::registry('amfpc_ignored')) {
            $this->setResponse($observer->getResponse(), $page, Amasty_Fpc_Model_Fpc_Front::PAGE_LOAD_IGNORE_PARAM);

            return;
        }

        $tags = Mage::getSingleton('amfpc/config')->matchRoute(Mage::app()->getRequest());

        if (!$tags && !Mage::getStoreConfig('amfpc/pages/all')) {
            $this->setResponse($observer->getResponse(), $page, Amasty_Fpc_Model_Fpc_Front::PAGE_LOAD_NEVER_CACHE);

            return;
        }

        if (Mage::helper('amfpc')->inIgnoreList() || Mage::registry('amfpc_ignorelist')) {
            $this->setResponse($observer->getResponse(), $page, Amasty_Fpc_Model_Fpc_Front::PAGE_LOAD_IGNORE);
            return;
        }


        if ($this->_canPreserve()) {
            $fpc = Mage::getSingleton('amfpc/fpc');

            if ($fpc->matchRoute(Mage::app()->getRequest(), 'amshopby/index/index')) {
                $rootCategory = Mage::helper('amshopby')->getCurrentCategory();
                $tags [] = 'catalog_category_' . $rootCategory->getId();
            }

            $tags[] = Amasty_Fpc_Model_Fpc::CACHE_TAG;
            $tags[] = Mage_Core_Block_Abstract::CACHE_GROUP;
            $tags[] = Mage::getSingleton('amfpc/fpc_front')->getUrlTag();

            $lifetime = +Mage::getStoreConfig('amfpc/general/page_lifetime');
            $lifetime *= 3600;

            $tags = array_filter($tags);
            Mage::getSingleton('amfpc/fpc')->savePage($page, $tags, $lifetime);
            Mage::getSingleton('amfpc/fpc_front')->incrementHits();
        }

        if (Mage::registry('amfpc_cms_blocks')) {
            $this->setResponse($observer->getResponse(), $page, Amasty_Fpc_Model_Fpc_Front::PAGE_LOAD_CMS_UPDATE);
            return;
        }

        if (is_array($this->_ajaxBlocks)) {
            $result = array();
            $front = Mage::getSingleton('amfpc/fpc_front');
            foreach ($this->_ajaxBlocks as $name => $content) {
                /** @var Amasty_Fpc_Model_Fpc_Front $front */
                $front->addBlockInfo($content, $name . ' (ajax)');
                $result[$name] = $content;
            }

            $blocksJson = Mage::helper('core')->jsonEncode($result);

            Mage::app()->getResponse()->setBody($blocksJson);
            return;
        }

        $this->setResponse($observer->getResponse(), $page, Amasty_Fpc_Model_Fpc_Front::PAGE_LOAD_MISS);
    }

    public function cleanCache(Mage_Cron_Model_Schedule $schedule)
    {
        Mage::getSingleton('amfpc/fpc')->getFrontend()->clean(Zend_Cache::CLEANING_MODE_OLD);
    }

    public function flushOutOfStockCache($observer)
    {
        $item = $observer->getItem();

        if ($item->getStockStatusChangedAutomatically()) {
            $tags = array('catalog_product_' . $item->getProductId(), 'catalog_product');
            Mage::dispatchEvent('application_clean_cache', array('tags' => $tags));
        }

        return $item;
    }

    public function onQuoteSubmitSuccess($observer)
    {
        if (Mage::getStoreConfig('amfpc/product/flush_on_purchase')) {
            $this->_cleanItemsCache(
                $observer->getQuote()->getAllItems()
            );
        }
    }

    public function onOrderCancelAfter($observer)
    {
        if (Mage::getStoreConfig('amfpc/product/flush_on_purchase')) {
            $this->_cleanItemsCache(
                $observer->getOrder()->getAllItems()
            );
        }
    }

    protected function _cleanItemsCache($items)
    {
        $tags = array();

        foreach ($items as $item) {
            $tags [] = 'catalog_product_' . $item->getProductId();
            $children = $item->getChildrenItems();
            if ($children) {
                foreach ($children as $childItem) {
                    $tags [] = 'catalog_product_' . $childItem->getProductId();
                }
            }
        }

        if (!empty($tags))
            Mage::dispatchEvent('application_clean_cache', array('tags' => $tags));
    }

    public function onApplicationCleanCache($observer)
    {
        if (!Mage::helper('amfpc')->storeInitialized())
            return;

        $tags = $observer->getTags();

        /**
         * @var Amasty_Fpc_Model_Fpc $fpc
         */
        $fpc = Mage::getSingleton('amfpc/fpc');

        if (!empty($tags)) {
            if (!is_array($tags)) {
                $tags = array($tags);
            }

            $productIds = array();
            foreach ($tags as $tag) {
                if (preg_match('/^catalog_product_(?P<id>\d+)$/i', $tag, $matches)) {
                    $productIds[] = +$matches['id'];
                }
            }

            $flushType = Mage::getStoreConfig('amfpc/product/flush_type');

            if ($flushType == Amasty_Fpc_Model_Config_Source_FlushType::FLUSH_ASSOCIATED) {
                $additionalTags = $fpc->getProductsAdditionalTags($productIds);

                if (!empty($additionalTags)) {
                    $tags = array_merge($tags, $additionalTags);
                }
            } else if ($flushType == Amasty_Fpc_Model_Config_Source_FlushType::FLUSH_PRODUCT_ONLY) {
                if (in_array(Mage_Catalog_Model_Product::CACHE_TAG, $tags)) {
                    // Keep category cache
                    $catTagPrefix = Mage_Catalog_Model_Category::CACHE_TAG . '_';
                    foreach ($tags as $tagKey => $tag) {
                        if (strpos($tag, $catTagPrefix) === 0) {
                            unset($tags[$tagKey]);
                        }
                    }
                }
            }
        }

        $fpc->clean($tags);
    }

    public function onModelSaveBefore($observer)
    {
        $object = $observer->getObject();

        if (class_exists('Mirasvit_AsyncCache_Model_Asynccache', false)
            && $object instanceof Mirasvit_AsyncCache_Model_Asynccache
        ) {
            if ($object->getData('status') == Mirasvit_AsyncCache_Model_Asynccache::STATUS_SUCCESS &&
                $object->getOrigData('status') != Mirasvit_AsyncCache_Model_Asynccache::STATUS_SUCCESS
            ) {
                Mage::getSingleton('amfpc/fpc')->getFrontend()->clean($object->getMode(), $object->getTagArray(), true);
            }
        }
    }

    public function onCustomerLogin($observer)
    {
        $customer = $observer->getCustomer();

        Mage::getSingleton('customer/session')
            ->setCustomerGroupId($customer->getGroupId());
    }

    public function onReviewSaveAfter($observer)
    {
        $review = $observer->getObject();

        $productEntityId = $review->getEntityIdByCode(Mage_Review_Model_Review::ENTITY_PRODUCT_CODE);

        if ($review->getEntityId() == $productEntityId) {
            if (Mage::app()->getStore()->isAdmin()
                || $review->getStatusId() == Mage_Review_Model_Review::STATUS_APPROVED
            ) {

                Mage::getSingleton('amfpc/fpc')->clean(
                    'catalog_product_' . $review->getEntityPkValue()
                );
            }
        }
    }

    public function onQuoteSaveAfter($observer)
    {
        Mage::helper('amfpc')->invalidateBlocksWithAttribute('cart');
    }

    public function onCustomerLoginLogout($observer)
    {
        Mage::helper('amfpc')->invalidateBlocksWithAttribute(array('customer', 'cart'));
    }

    public function onCategorySaveAfter($observer)
    {
        if (Mage::getStoreConfig('amfpc/category/flush_all'))
            Mage::getSingleton('amfpc/fpc')->flush();
    }

    public function onAdminhtmlInitSystemConfig($observer)
    {
        $backend = Mage::getSingleton('amfpc/fpc')->getBackendType();

        if (FALSE === stripos($backend, 'database')) {
            $observer->getConfig()->setNode(
                'sections/amfpc/groups/compression/fields/max_size', false, true
            );
        }
    }

    public function onCrawlerProcessLink($observer)
    {
        /**
         * @var Amasty_Fpc_Model_Fpc $fpc
         */
        $fpc = Mage::getSingleton('amfpc/fpc');
        $data = $observer->getData('data');
        $key = $fpc->getCacheKey($data);
        $meta = $fpc->getFrontend()->getMetadatas($key);

        if ($meta) {
            $timeRemains = $meta['expire'] - time();

            if ($timeRemains > 0) {
                $action = Mage::getStoreConfig('amfpc/regen/crawler_action');

                $data->setData('hasCache', true);

                if (Amasty_Fpc_Model_Config_Source_CrawlerAction::ACTION_REGENERATE == $action) {
                    $lifetime = Mage::getStoreConfig('amfpc/general/page_lifetime');
                    $lifetime *= 3600;

                    $fpc->getFrontend()->touch($key, $lifetime - $timeRemains);
                } else if (Amasty_Fpc_Model_Config_Source_CrawlerAction::ACTION_REFRESH == $action) {
                    $fpc->getFrontend()->remove($key);
                    $data->setData('hasCache', false);
                }
            }
        }
    }

    public function onRefreshType($observer)
    {
        if ($observer->getType() == 'amfpc') {
            Mage::getSingleton('amfpc/fpc')->flush();
        }
    }

    public function onMassRefreshAction($observer)
    {
        $types = Mage::app()->getRequest()->getParam('types');

        if (in_array('amfpc', $types)) {
            Mage::getSingleton('amfpc/fpc')->flush();
        }
    }

    public function onEndProcessCatalogProductSave($observer)
    {
        if ($product = Mage::registry('current_product')) {
            $product->cleanCache();
        }
    }

    public function updateAttributesOnMassAction($observer)
    {
        $productIds = $observer->getProductIds();
        Mage::helper('amfpc')->collectTags($productIds);
    }

    public function updateAttributesOnMassStockUpdate($observer)
    {
        $changedProductIds = $observer->getProducts();
        Mage::helper('amfpc')->collectTags($changedProductIds);

    }

}
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Fpc
 */

class Amasty_Fpc_Model_Config
{
    protected $_config = null;

    public function getConfig()
    {
        if (!$this->_config)
            $this->_config = Mage::app()->getConfig()->getNode('global/amfpc')->asArray();

        if (Mage::getSingleton('amfpc/fpc_front')->getDbConfig('web/cookie/cookie_restriction'))
            $this->_config['blocks'][self::getCookieNoticeBlockName()] = array();

        return $this->_config;
    }

    public function matchRoute(Mage_Core_Controller_Request_Http $request)
    {
        $fpc = Mage::getSingleton('amfpc/fpc');

        $config = $this->getConfig();
        $config = $config['routes'];

        foreach ($config as $route)
        {
            if ($fpc->matchRoute($request, $route['path']))
            {
                $tags = explode(',', $route['tags']);

                foreach ($tags as &$tag)
                {
                    if (preg_match('/\{(\w+)\}/', $tag, $matches))
                    {
                        $paramId = $matches[1];
                        if ($param = Mage::app()->getRequest()->getParam($paramId))
                        {
                            $tag = str_replace($matches[0], $param, $tag);
                        }
                    }
                }

                return $tags;
            }
        }
    }

    public function blockIsDynamic($block, &$isAjax, &$tags, &$children)
    {
        $children = array();

        $config = $this->getConfig();

        $name = $block->getNameInLayout();

        if (isset($config['ajax_blocks'][$name]))
        {
            $isAjax = true;
            return true;
        }

        if (isset($config['blocks'][$name]))
        {
            if (isset($config['blocks'][$name]['tags']))
                $tags = explode(',', $config['blocks'][$name]['tags']);

            return true;
        }

        foreach ($config['blocks'] as $id => $block)
        {
            if (isset($block['@']['parent']) && $block['@']['parent'] == $name)
            {
                $tags = isset($block['tags']) ? explode(',', $block['tags']) : array();

                $children[$id] = $tags;
            }
        }

        return false;
    }

    public static function getCookieNoticeBlockName()
    {
        $name = Mage::app()
            ->getConfig()
            ->getNode('global/amfpc/cookie_notice_block');

        return (string)$name;
    }

    public function canSaveAjax()
    {
        $pattern = (string)Mage::app()
            ->getConfig()
            ->getNode('global/amfpc/allowed_ajax_pattern');

        if (!$pattern)
            return false;

        $request = Mage::app()->getRequest();

        $internalUri = implode('/', array(
            $request->getModuleName(),
            $request->getControllerName(),
            $request->getActionName()
        ));

        $canSave = preg_match("#$pattern#", $internalUri);

        return $canSave;
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Catalog flat abstract helper
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
abstract class Mage_Catalog_Helper_Flat_Abstract extends Mage_Core_Helper_Abstract
{
    /**
     * Catalog Flat index process code
     *
     * @var null|string
     */
    protected $_indexerCode = null;

    /**
     * Store catalog Flat index process instance
     *
     * @var Mage_Index_Model_Process|null
     */
    protected $_process = null;

    /**
     * Flag for accessibility
     *
     * @var bool
     */
    protected $_isAccessible = null;

    /**
     * Flag for availability
     *
     * @var bool
     */
    protected $_isAvailable = null;

    /**
     * Check if Catalog Flat Data has been initialized
     *
     * @param null|bool|int|Mage_Core_Model_Store $store Store(id) for which the value is checked
     * @return bool
     */
    abstract public function isBuilt($store = null);

    /**
     * Check if Catalog Category Flat Data is enabled
     *
     * @param mixed $deprecatedParam this parameter is deprecated and no longer in use
     *
     * @return bool
     */
    abstract public function isEnabled($deprecatedParam = false);

    /**
     * Check if Catalog Category Flat Data is available
     * without lock check
     *
     * @return bool
     */
    public function isAccessible()
    {
        if (is_null($this->_isAccessible)) {
            $this->_isAccessible = $this->isEnabled()
                && $this->getProcess()->getStatus() != Mage_Index_Model_Process::STATUS_RUNNING;
        }
        return $this->_isAccessible;
    }

    /**
     * Check if Catalog Category Flat Data is available for use
     *
     * @return bool
     */
    public function isAvailable()
    {
        if (is_null($this->_isAvailable)) {
            $this->_isAvailable = $this->isAccessible() && !$this->getProcess()->isLocked();
        }
        return $this->_isAvailable;
    }

    /**
     * Retrieve Catalog Flat index process
     *
     * @return Mage_Index_Model_Process
     */
    public function getProcess()
    {
        if (is_null($this->_process)) {
            $this->_process = Mage::getModel('index/process')
                ->load($this->_indexerCode, 'indexer_code');
        }
        return $this->_process;
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Catalog Product Flat Helper
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Mage_Catalog_Helper_Product_Flat extends Mage_Catalog_Helper_Flat_Abstract
{
    /**
     * Catalog Product Flat Config
     */
    const XML_PATH_USE_PRODUCT_FLAT          = 'catalog/frontend/flat_catalog_product';
    const XML_NODE_ADD_FILTERABLE_ATTRIBUTES = 'global/catalog/product/flat/add_filterable_attributes';
    const XML_NODE_ADD_CHILD_DATA            = 'global/catalog/product/flat/add_child_data';

    /**
     * Path for flat flag model
     */
    const XML_PATH_FLAT_FLAG                 = 'global/catalog/product/flat/flag/model';

    /**
     * Catalog Flat Product index process code
     */
    const CATALOG_FLAT_PROCESS_CODE = 'catalog_product_flat';

    /**
     * Catalog Product Flat index process code
     *
     * @var string
     */
    protected $_indexerCode = self::CATALOG_FLAT_PROCESS_CODE;

    /**
     * Catalog Product Flat index process instance
     *
     * @var Mage_Index_Model_Process|null
     */
    protected $_process = null;

    /**
     * Store flags which defines if Catalog Product Flat functionality is enabled
     *
     * @deprecated after 1.7.0.0
     *
     * @var array
     */
    protected $_isEnabled = array();

    /**
     * Catalog Product Flat Flag object
     *
     * @var Mage_Catalog_Model_Product_Flat_Flag
     */
    protected $_flagObject;

    /**
     * Catalog Product Flat force status enable/disable
     * to force EAV for products in quote
     * store settings will be used by default
     *
     * @var boolean
     */
    protected $_forceFlatStatus = false;

    /**
     * Old Catalog Product Flat forced status
     *
     * @var boolean
     */
    protected $_forceFlatStatusOld;

    /**
     * Retrieve Catalog Product Flat Flag object
     *
     * @return Mage_Catalog_Model_Product_Flat_Flag
     */
    public function getFlag()
    {
        if (is_null($this->_flagObject)) {
            $className = (string)Mage::getConfig()->getNode(self::XML_PATH_FLAT_FLAG);
            $this->_flagObject = Mage::getSingleton($className)
                ->loadSelf();
        }
        return $this->_flagObject;
    }

    /**
     * Check Catalog Product Flat functionality is enabled
     *
     * @param int|string|null|Mage_Core_Model_Store $store this parameter is deprecated and no longer in use
     *
     * @return bool
     */
    public function isEnabled($store = null)
    {
        return Mage::getStoreConfigFlag(self::XML_PATH_USE_PRODUCT_FLAT);
    }

    /**
     * Check if Catalog Product Flat Data has been initialized
     *
     * @param null|bool|int|Mage_Core_Model_Store $store Store(id) for which the value is checked
     * @return bool
     */
    public function isBuilt($store = null)
    {
        if ($store !== null) {
            return $this->getFlag()->isStoreBuilt(Mage::app()->getStore($store)->getId());
        }
        return $this->getFlag()->getIsBuilt();
    }

    /**
     * Check if Catalog Product Flat Data has been initialized for all stores
     *
     * @return bool
     */
    public function isBuiltAllStores()
    {
        $isBuildAll = true;
        foreach(Mage::app()->getStores(false) as $store) {
            /** @var $store Mage_Core_Model_Store */
            $isBuildAll = $isBuildAll && $this->isBuilt($store->getId());
        }
        return $isBuildAll;
    }

    /**
     * Is add filterable attributes to Flat table
     *
     * @return int
     */
    public function isAddFilterableAttributes()
    {
        return intval(Mage::getConfig()->getNode(self::XML_NODE_ADD_FILTERABLE_ATTRIBUTES));
    }

    /**
     * Is add child data to Flat
     *
     * @return int
     */
    public function isAddChildData()
    {
        return intval(Mage::getConfig()->getNode(self::XML_NODE_ADD_CHILD_DATA));
    }

    /**
     * Disable Catalog Product Flat
     *
     * @param $save bool
     */
    public function disableFlatCollection($save = false)
    {
        $this->_forceFlatStatus = true;

        if ($save) {
            $this->_forceFlatStatusOld = $this->_forceFlatStatus;
        }
    }

    /**
     * Reset Catalog Product Flat
     */
    public function resetFlatCollection()
    {
        if (isset($this->_forceFlatStatusOld)) {
            $this->_forceFlatStatus = $this->_forceFlatStatusOld;
        } else {
            $this->_forceFlatStatus = false;
        }
    }

    /**
     * Checks if Catalog Product Flat was forced disabled
     *
     * @return bool
     */
    public function isFlatCollectionDisabled()
    {
        return $this->_forceFlatStatus;
    }
}
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Scheckout
 */

class Amasty_Scheckout_Model_Cart extends Mage_Checkout_Model_Cart {
    public function init()
    {
        $initCheckout = $this->getCheckoutSession()->getCheckoutState() !== Mage_Checkout_Model_Session::CHECKOUT_STATE_BEGIN;
        $ret = parent::init();

        if ($initCheckout){
            $this->initAmscheckout();
        }


        return $ret;
    }

    function initAmscheckout()
    {
        if ((string)Mage::getConfig()->getNode('modules/Amasty_Autoshipping/active') != 'true') {
            $this->_initBilling();
            $this->_initShipping();
        } else {
            $this->getQuote()->getShippingAddress()->setCollectShippingRates(true);
        }


        Mage::helper("amscheckout")->initPaymentMethod($this->getQuote());

        if (Mage::getStoreConfig("amscheckout/default/ship_same_address")){
            $this->getQuote()->getShippingAddress()->setSameAsBilling(true);
        } else {
            $this->getQuote()->getShippingAddress()->setSameAsBilling(false);
        }
    }

    protected function _initBilling(){
        $quote = $this->getQuote();

        $address = $quote->getBillingAddress();

        if ($address) {
            $this->_initAddress($address);
        }
    }

    protected function _initShipping(){
        $quote = $this->getQuote();

        $address = $quote->getShippingAddress();

        if ($address && $quote->getItemsCount() > 0) {
            $hlr = Mage::helper("amscheckout");

            $this->_initAddress($address);

            $address->setCollectShippingRates(true);

            //$address->setShippingMethod($hlr->getDefaultShippingMethod($quote));
            $this->_initShippingMethod($address);

            $quote->setTotalsCollectedFlag(false);
            $quote->collectTotals();
            $quote->save();
        }
    }

    protected function _initAddress($address){
        $hlr = Mage::helper("amscheckout");

        $countryId = $hlr->getDefaultCountry();

        $city = $hlr->getDefaultCity();

        $postcode = $hlr->getDefaultPostcode();

        if ($countryId) {
            $address->setCountryId($countryId);
        }

        if ($city){
            $address->setCity($city);
        }

        if ($postcode){
            $address->setPostcode($postcode);
        }
    }

    protected function _initShippingMethod($address)
    {
        $hlr = Mage::helper("amscheckout");
        $quote = $this->getQuote();

        if ($address && $quote->getItemsCount() > 0) {
            $curShippingMethod = $address->getShippingMethod();

            if ($curShippingMethod == "") {
                $curShippingMethod = $hlr->getDefaultShippingMethod($quote);
            }

            $address->setShippingMethod($curShippingMethod);
        }
    }

}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Sales
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Quote model
 *
 * Supported events:
 *  sales_quote_load_after
 *  sales_quote_save_before
 *  sales_quote_save_after
 *  sales_quote_delete_before
 *  sales_quote_delete_after
 *
 * @method Mage_Sales_Model_Resource_Quote _getResource()
 * @method Mage_Sales_Model_Resource_Quote getResource()
 * @method Mage_Sales_Model_Quote setStoreId(int $value)
 * @method string getCreatedAt()
 * @method Mage_Sales_Model_Quote setCreatedAt(string $value)
 * @method string getUpdatedAt()
 * @method Mage_Sales_Model_Quote setUpdatedAt(string $value)
 * @method string getConvertedAt()
 * @method Mage_Sales_Model_Quote setConvertedAt(string $value)
 * @method int getIsActive()
 * @method Mage_Sales_Model_Quote setIsActive(int $value)
 * @method Mage_Sales_Model_Quote setIsVirtual(int $value)
 * @method int getIsMultiShipping()
 * @method Mage_Sales_Model_Quote setIsMultiShipping(int $value)
 * @method int getItemsCount()
 * @method Mage_Sales_Model_Quote setItemsCount(int $value)
 * @method float getItemsQty()
 * @method Mage_Sales_Model_Quote setItemsQty(float $value)
 * @method int getOrigOrderId()
 * @method Mage_Sales_Model_Quote setOrigOrderId(int $value)
 * @method float getStoreToBaseRate()
 * @method Mage_Sales_Model_Quote setStoreToBaseRate(float $value)
 * @method float getStoreToQuoteRate()
 * @method Mage_Sales_Model_Quote setStoreToQuoteRate(float $value)
 * @method string getBaseCurrencyCode()
 * @method Mage_Sales_Model_Quote setBaseCurrencyCode(string $value)
 * @method string getStoreCurrencyCode()
 * @method Mage_Sales_Model_Quote setStoreCurrencyCode(string $value)
 * @method string getQuoteCurrencyCode()
 * @method Mage_Sales_Model_Quote setQuoteCurrencyCode(string $value)
 * @method float getGrandTotal()
 * @method Mage_Sales_Model_Quote setGrandTotal(float $value)
 * @method float getBaseGrandTotal()
 * @method Mage_Sales_Model_Quote setBaseGrandTotal(float $value)
 * @method Mage_Sales_Model_Quote setCheckoutMethod(string $value)
 * @method int getCustomerId()
 * @method Mage_Sales_Model_Quote setCustomerId(int $value)
 * @method Mage_Sales_Model_Quote setCustomerTaxClassId(int $value)
 * @method Mage_Sales_Model_Quote setCustomerGroupId(int $value)
 * @method string getCustomerEmail()
 * @method Mage_Sales_Model_Quote setCustomerEmail(string $value)
 * @method string getCustomerPrefix()
 * @method Mage_Sales_Model_Quote setCustomerPrefix(string $value)
 * @method string getCustomerFirstname()
 * @method Mage_Sales_Model_Quote setCustomerFirstname(string $value)
 * @method string getCustomerMiddlename()
 * @method Mage_Sales_Model_Quote setCustomerMiddlename(string $value)
 * @method string getCustomerLastname()
 * @method Mage_Sales_Model_Quote setCustomerLastname(string $value)
 * @method string getCustomerSuffix()
 * @method Mage_Sales_Model_Quote setCustomerSuffix(string $value)
 * @method string getCustomerDob()
 * @method Mage_Sales_Model_Quote setCustomerDob(string $value)
 * @method string getCustomerNote()
 * @method Mage_Sales_Model_Quote setCustomerNote(string $value)
 * @method int getCustomerNoteNotify()
 * @method Mage_Sales_Model_Quote setCustomerNoteNotify(int $value)
 * @method int getCustomerIsGuest()
 * @method Mage_Sales_Model_Quote setCustomerIsGuest(int $value)
 * @method string getRemoteIp()
 * @method Mage_Sales_Model_Quote setRemoteIp(string $value)
 * @method string getAppliedRuleIds()
 * @method Mage_Sales_Model_Quote setAppliedRuleIds(string $value)
 * @method string getReservedOrderId()
 * @method Mage_Sales_Model_Quote setReservedOrderId(string $value)
 * @method string getPasswordHash()
 * @method Mage_Sales_Model_Quote setPasswordHash(string $value)
 * @method string getCouponCode()
 * @method Mage_Sales_Model_Quote setCouponCode(string $value)
 * @method string getGlobalCurrencyCode()
 * @method Mage_Sales_Model_Quote setGlobalCurrencyCode(string $value)
 * @method float getBaseToGlobalRate()
 * @method Mage_Sales_Model_Quote setBaseToGlobalRate(float $value)
 * @method float getBaseToQuoteRate()
 * @method Mage_Sales_Model_Quote setBaseToQuoteRate(float $value)
 * @method string getCustomerTaxvat()
 * @method Mage_Sales_Model_Quote setCustomerTaxvat(string $value)
 * @method string getCustomerGender()
 * @method Mage_Sales_Model_Quote setCustomerGender(string $value)
 * @method float getSubtotal()
 * @method Mage_Sales_Model_Quote setSubtotal(float $value)
 * @method float getBaseSubtotal()
 * @method Mage_Sales_Model_Quote setBaseSubtotal(float $value)
 * @method float getSubtotalWithDiscount()
 * @method Mage_Sales_Model_Quote setSubtotalWithDiscount(float $value)
 * @method float getBaseSubtotalWithDiscount()
 * @method Mage_Sales_Model_Quote setBaseSubtotalWithDiscount(float $value)
 * @method int getIsChanged()
 * @method Mage_Sales_Model_Quote setIsChanged(int $value)
 * @method int getTriggerRecollect()
 * @method Mage_Sales_Model_Quote setTriggerRecollect(int $value)
 * @method string getExtShippingInfo()
 * @method Mage_Sales_Model_Quote setExtShippingInfo(string $value)
 * @method int getGiftMessageId()
 * @method Mage_Sales_Model_Quote setGiftMessageId(int $value)
 * @method bool|null getIsPersistent()
 * @method Mage_Sales_Model_Quote setIsPersistent(bool $value)
 *
 * @category    Mage
 * @package     Mage_Sales
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Sales_Model_Quote extends Mage_Core_Model_Abstract
{
    protected $_eventPrefix = 'sales_quote';
    protected $_eventObject = 'quote';

    /**
     * Model cache tag for clear cache in after save and after delete
     *
     * When you use true - all cache will be clean
     *
     * @var string || true
     */
    protected $_cacheTag = 'quote';

    /**
     * Quote customer model object
     *
     * @var Mage_Customer_Model_Customer
     */
    protected $_customer;

    /**
     * Quote addresses collection
     *
     * @var Mage_Eav_Model_Entity_Collection_Abstract
     */
    protected $_addresses = null;

    /**
     * Quote items collection
     *
     * @var Mage_Eav_Model_Entity_Collection_Abstract
     */
    protected $_items = null;

    /**
     * Quote payments
     *
     * @var Mage_Eav_Model_Entity_Collection_Abstract
     */
    protected $_payments = null;

    /**
     * Different groups of error infos
     *
     * @var array
     */
    protected $_errorInfoGroups = array();

    /**
     * Whether quote should not be saved
     *
     * @var bool
     */
    protected $_preventSaving = false;

    /**
     * Init resource model
     */
    protected function _construct()
    {
        $this->_init('sales/quote');
    }

    /**
     * Init mapping array of short fields to
     * its full names
     *
     * @return Varien_Object
     */
    protected function _initOldFieldsMap()
    {
        $this->_oldFieldsMap = Mage::helper('sales')->getOldFieldMap('quote');
        return $this;
    }

    /**
     * Get quote store identifier
     *
     * @return int
     */
    public function getStoreId()
    {
        if (!$this->hasStoreId()) {
            return Mage::app()->getStore()->getId();
        }
        return $this->_getData('store_id');
    }

    /**
     * Get quote store model object
     *
     * @return  Mage_Core_Model_Store
     */
    public function getStore()
    {
        return Mage::app()->getStore($this->getStoreId());
    }

    /**
     * Declare quote store model
     *
     * @param   Mage_Core_Model_Store $store
     * @return  Mage_Sales_Model_Quote
     */
    public function setStore(Mage_Core_Model_Store $store)
    {
        $this->setStoreId($store->getId());
        return $this;
    }

    /**
     * Get all available store ids for quote
     *
     * @return array
     */
    public function getSharedStoreIds()
    {
        $ids = $this->_getData('shared_store_ids');
        if (is_null($ids) || !is_array($ids)) {
            if ($website = $this->getWebsite()) {
                return $website->getStoreIds();
            }
            return $this->getStore()->getWebsite()->getStoreIds();
        }
        return $ids;
    }

    /**
     * Prepare data before save
     *
     * @return Mage_Sales_Model_Quote
     */
    protected function _beforeSave()
    {
        /**
         * Currency logic
         *
         * global - currency which is set for default in backend
         * base - currency which is set for current website. all attributes that
         *      have 'base_' prefix saved in this currency
         * store - all the time it was currency of website and all attributes
         *      with 'base_' were saved in this currency. From now on it is
         *      deprecated and will be duplication of base currency code.
         * quote/order - currency which was selected by customer or configured by
         *      admin for current store. currency in which customer sees
         *      price thought all checkout.
         *
         * Rates:
         *      store_to_base & store_to_quote/store_to_order - are deprecated
         *      base_to_global & base_to_quote/base_to_order - must be used instead
         */

        $globalCurrencyCode  = Mage::app()->getBaseCurrencyCode();
        $baseCurrency = $this->getStore()->getBaseCurrency();

        if ($this->hasForcedCurrency()){
            $quoteCurrency = $this->getForcedCurrency();
        } else {
            $quoteCurrency = $this->getStore()->getCurrentCurrency();
        }

        $this->setGlobalCurrencyCode($globalCurrencyCode);
        $this->setBaseCurrencyCode($baseCurrency->getCode());
        $this->setStoreCurrencyCode($baseCurrency->getCode());
        $this->setQuoteCurrencyCode($quoteCurrency->getCode());

        //deprecated, read above
        $this->setStoreToBaseRate($baseCurrency->getRate($globalCurrencyCode));
        $this->setStoreToQuoteRate($baseCurrency->getRate($quoteCurrency));

        $this->setBaseToGlobalRate($baseCurrency->getRate($globalCurrencyCode));
        $this->setBaseToQuoteRate($baseCurrency->getRate($quoteCurrency));

        if (!$this->hasChangedFlag() || $this->getChangedFlag() == true) {
            $this->setIsChanged(1);
        } else {
            $this->setIsChanged(0);
        }

        if ($this->_customer) {
            $this->setCustomerId($this->_customer->getId());
        }

        parent::_beforeSave();
    }

    /**
     * Save related items
     *
     * @return Mage_Sales_Model_Quote
     */
    protected function _afterSave()
    {
        parent::_afterSave();

        if (null !== $this->_addresses) {
            $this->getAddressesCollection()->save();
        }

        if (null !== $this->_items) {
            $this->getItemsCollection()->save();
        }

        if (null !== $this->_payments) {
            $this->getPaymentsCollection()->save();
        }
        return $this;
    }

    /**
     * Loading quote data by customer
     *
     * @return Mage_Sales_Model_Quote
     */
    public function loadByCustomer($customer)
    {
        if ($customer instanceof Mage_Customer_Model_Customer) {
            $customerId = $customer->getId();
        }
        else {
            $customerId = (int) $customer;
        }
        $this->_getResource()->loadByCustomerId($this, $customerId);
        $this->_afterLoad();
        return $this;
    }

    /**
     * Loading only active quote
     *
     * @param int $quoteId
     * @return Mage_Sales_Model_Quote
     */
    public function loadActive($quoteId)
    {
        $this->_getResource()->loadActive($this, $quoteId);
        $this->_afterLoad();
        return $this;
    }

    /**
     * Loading quote by identifier
     *
     * @param int $quoteId
     * @return Mage_Sales_Model_Quote
     */
    public function loadByIdWithoutStore($quoteId)
    {
        $this->_getResource()->loadByIdWithoutStore($this, $quoteId);
        $this->_afterLoad();
        return $this;
    }

    /**
     * Assign customer model object data to quote
     *
     * @param   Mage_Customer_Model_Customer $customer
     * @return  Mage_Sales_Model_Quote
     */
    public function assignCustomer(Mage_Customer_Model_Customer $customer)
    {
        return $this->assignCustomerWithAddressChange($customer);
    }

    /**
     * Assign customer model to quote with billing and shipping address change
     *
     * @param  Mage_Customer_Model_Customer    $customer
     * @param  Mage_Sales_Model_Quote_Address  $billingAddress
     * @param  Mage_Sales_Model_Quote_Address  $shippingAddress
     * @return Mage_Sales_Model_Quote
     */
    public function assignCustomerWithAddressChange(
        Mage_Customer_Model_Customer    $customer,
        Mage_Sales_Model_Quote_Address  $billingAddress  = null,
        Mage_Sales_Model_Quote_Address  $shippingAddress = null
    )
    {
        if ($customer->getId()) {
            $this->setCustomer($customer);

            if (!is_null($billingAddress)) {
                $this->setBillingAddress($billingAddress);
            } else {
                $defaultBillingAddress = $customer->getDefaultBillingAddress();
                if ($defaultBillingAddress && $defaultBillingAddress->getId()) {
                    $billingAddress = Mage::getModel('sales/quote_address')
                        ->importCustomerAddress($defaultBillingAddress);
                    $this->setBillingAddress($billingAddress);
                }
            }

            if (is_null($shippingAddress)) {
                $defaultShippingAddress = $customer->getDefaultShippingAddress();
                if ($defaultShippingAddress && $defaultShippingAddress->getId()) {
                    $shippingAddress = Mage::getModel('sales/quote_address')
                        ->importCustomerAddress($defaultShippingAddress);
                } else {
                    $shippingAddress = Mage::getModel('sales/quote_address');
                }
            }
            $this->setShippingAddress($shippingAddress);
        }

        return $this;
    }

    /**
     * Define customer object
     *
     * @param   Mage_Customer_Model_Customer $customer
     * @return  Mage_Sales_Model_Quote
     */
    public function setCustomer(Mage_Customer_Model_Customer $customer)
    {
        $this->_customer = $customer;
        $this->setCustomerId($customer->getId());
        Mage::helper('core')->copyFieldset('customer_account', 'to_quote', $customer, $this);
        return $this;
    }

    /**
     * Retrieve customer model object
     *
     * @return Mage_Customer_Model_Customer
     */
    public function getCustomer()
    {
        if (is_null($this->_customer)) {
            $this->_customer = Mage::getModel('customer/customer');
            if ($customerId = $this->getCustomerId()) {
                $this->_customer->load($customerId);
                if (!$this->_customer->getId()) {
                    $this->_customer->setCustomerId(null);
                }
            }
        }
        return $this->_customer;
    }

    /**
     * Retrieve customer group id
     *
     * @return int
     */
    public function getCustomerGroupId()
    {
        if ($this->hasData('customer_group_id')) {
            return $this->getData('customer_group_id');
        } else if ($this->getCustomerId()) {
            return $this->getCustomer()->getGroupId();
        } else {
            return Mage_Customer_Model_Group::NOT_LOGGED_IN_ID;
        }
    }

    public function getCustomerTaxClassId()
    {
        /*
        * tax class can vary at any time. so instead of using the value from session,
        * we need to retrieve from db every time to get the correct tax class
        */
        //if (!$this->getData('customer_group_id') && !$this->getData('customer_tax_class_id')) {
        $classId = Mage::getModel('customer/group')->getTaxClassId($this->getCustomerGroupId());
        $this->setCustomerTaxClassId($classId);
        //}

        return $this->getData('customer_tax_class_id');
    }

    /**
     * Retrieve quote address collection
     *
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     */
    public function getAddressesCollection()
    {
        if (is_null($this->_addresses)) {
            $this->_addresses = Mage::getModel('sales/quote_address')->getCollection()
                ->setQuoteFilter($this->getId());

            if ($this->getId()) {
                foreach ($this->_addresses as $address) {
                    $address->setQuote($this);
                }
            }
        }
        return $this->_addresses;
    }

    /**
     * Retrieve quote address by type
     *
     * @param   string $type
     * @return  Mage_Sales_Model_Quote_Address
     */
    protected function _getAddressByType($type)
    {
        foreach ($this->getAddressesCollection() as $address) {
            if ($address->getAddressType() == $type && !$address->isDeleted()) {
                return $address;
            }
        }

        $address = Mage::getModel('sales/quote_address')->setAddressType($type);
        $this->addAddress($address);
        return $address;
    }

    /**
     * Retrieve quote billing address
     *
     * @return Mage_Sales_Model_Quote_Address
     */
    public function getBillingAddress()
    {
        return $this->_getAddressByType(Mage_Sales_Model_Quote_Address::TYPE_BILLING);
    }

    /**
     * Retrieve quote shipping address
     *
     * @return Mage_Sales_Model_Quote_Address
     */
    public function getShippingAddress()
    {
        return $this->_getAddressByType(Mage_Sales_Model_Quote_Address::TYPE_SHIPPING);
    }

    public function getAllShippingAddresses()
    {
        $addresses = array();
        foreach ($this->getAddressesCollection() as $address) {
            if ($address->getAddressType()==Mage_Sales_Model_Quote_Address::TYPE_SHIPPING
                && !$address->isDeleted()) {
                $addresses[] = $address;
            }
        }
        return $addresses;
    }

    public function getAllAddresses()
    {
        $addresses = array();
        foreach ($this->getAddressesCollection() as $address) {
            if (!$address->isDeleted()) {
                $addresses[] = $address;
            }
        }
        return $addresses;
    }

    /**
     *
     * @param int $addressId
     * @return Mage_Sales_Model_Quote_Address
     */
    public function getAddressById($addressId)
    {
        foreach ($this->getAddressesCollection() as $address) {
            if ($address->getId()==$addressId) {
                return $address;
            }
        }
        return false;
    }

    public function getAddressByCustomerAddressId($addressId)
    {
        foreach ($this->getAddressesCollection() as $address) {
            if (!$address->isDeleted() && $address->getCustomerAddressId()==$addressId) {
                return $address;
            }
        }
        return false;
    }

    public function getShippingAddressByCustomerAddressId($addressId)
    {
        foreach ($this->getAddressesCollection() as $address) {
            if (!$address->isDeleted() && $address->getAddressType()==Mage_Sales_Model_Quote_Address::TYPE_SHIPPING
                && $address->getCustomerAddressId()==$addressId) {
                return $address;
            }
        }
        return false;
    }

    public function removeAddress($addressId)
    {
        foreach ($this->getAddressesCollection() as $address) {
            if ($address->getId()==$addressId) {
                $address->isDeleted(true);
                break;
            }
        }
        return $this;
    }

    /**
     * Leave no more than one billing and one shipping address, fill them with default data
     *
     * @return Mage_Sales_Model_Quote
     */
    public function removeAllAddresses()
    {
        $addressByType = array();
        $addressesCollection = $this->getAddressesCollection();

        // mark all addresses as deleted
        foreach ($addressesCollection as $address) {
            $type = $address->getAddressType();
            if (!isset($addressByType[$type]) || $addressByType[$type]->getId() > $address->getId()) {
                $addressByType[$type] = $address;
            }
            $address->isDeleted(true);
        }

        // create new billing and shipping addresses filled with default values, set this data to existing records
        foreach ($addressByType as $type => $address) {
            $id = $address->getId();
            $emptyAddress = $this->_getAddressByType($type);
            $address->setData($emptyAddress->getData())->setId($id)->isDeleted(false);
            $emptyAddress->setDeleteImmediately(true);
        }

        // remove newly created billing and shipping addresses from collection to avoid senseless delete queries
        foreach ($addressesCollection as $key => $item) {
            if ($item->getDeleteImmediately()) {
                $addressesCollection->removeItemByKey($key);
            }
        }

        return $this;
    }

    public function addAddress(Mage_Sales_Model_Quote_Address $address)
    {
        $address->setQuote($this);
        if (!$address->getId()) {
            $this->getAddressesCollection()->addItem($address);
        }
        return $this;
    }

    /**
     * Enter description here...
     *
     * @param Mage_Sales_Model_Quote_Address $address
     * @return Mage_Sales_Model_Quote
     */
    public function setBillingAddress(Mage_Sales_Model_Quote_Address $address)
    {
        $old = $this->getBillingAddress();

        if (!empty($old)) {
            $old->addData($address->getData());
        } else {
            $this->addAddress($address->setAddressType(Mage_Sales_Model_Quote_Address::TYPE_BILLING));
        }
        return $this;
    }

    /**
     * Enter description here...
     *
     * @param Mage_Sales_Model_Quote_Address $address
     * @return Mage_Sales_Model_Quote
     */
    public function setShippingAddress(Mage_Sales_Model_Quote_Address $address)
    {
        if ($this->getIsMultiShipping()) {
            $this->addAddress($address->setAddressType(Mage_Sales_Model_Quote_Address::TYPE_SHIPPING));
        }
        else {
            $old = $this->getShippingAddress();

            if (!empty($old)) {
                $old->addData($address->getData());
            } else {
                $this->addAddress($address->setAddressType(Mage_Sales_Model_Quote_Address::TYPE_SHIPPING));
            }
        }
        return $this;
    }

    public function addShippingAddress(Mage_Sales_Model_Quote_Address $address)
    {
        $this->setShippingAddress($address);
        return $this;
    }

    /**
     * Retrieve quote items collection
     *
     * @param   bool $loaded
     * @return  Mage_Eav_Model_Entity_Collection_Abstract
     */
    public function getItemsCollection($useCache = true)
    {
        if ($this->hasItemsCollection()) {
            return $this->getData('items_collection');
        }
        if (is_null($this->_items)) {
            $this->_items = Mage::getModel('sales/quote_item')->getCollection();
            $this->_items->setQuote($this);
        }
        return $this->_items;
    }

    /**
     * Retrieve quote items array
     *
     * @return array
     */
    public function getAllItems()
    {
        $items = array();
        foreach ($this->getItemsCollection() as $item) {
            if (!$item->isDeleted()) {
                $items[] =  $item;
            }
        }
        return $items;
    }

    /**
     * Get array of all items what can be display directly
     *
     * @return array
     */
    public function getAllVisibleItems()
    {
        $items = array();
        foreach ($this->getItemsCollection() as $item) {
            if (!$item->isDeleted() && !$item->getParentItemId()) {
                $items[] =  $item;
            }
        }
        return $items;
    }

    /**
     * Checking items availability
     *
     * @return bool
     */
    public function hasItems()
    {
        return sizeof($this->getAllItems())>0;
    }

    /**
     * Checking availability of items with decimal qty
     *
     * @return bool
     */
    public function hasItemsWithDecimalQty()
    {
        foreach ($this->getAllItems() as $item) {
            if ($item->getProduct()->getStockItem()
                && $item->getProduct()->getStockItem()->getIsQtyDecimal()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checking product exist in Quote
     *
     * @param int $productId
     * @return bool
     */
    public function hasProductId($productId)
    {
        foreach ($this->getAllItems() as $item) {
            if ($item->getProductId() == $productId) {
                return true;
            }
        }

        return false;
    }

    /**
     * Retrieve item model object by item identifier
     *
     * @param   int $itemId
     * @return  Mage_Sales_Model_Quote_Item
     */
    public function getItemById($itemId)
    {
        return $this->getItemsCollection()->getItemById($itemId);
    }

    /**
     * Delete quote item. If it does not have identifier then it will be only removed from collection
     *
     * @param   Mage_Sales_Model_Quote_Item $item
     * @return  Mage_Sales_Model_Quote
     */
    public function deleteItem(Mage_Sales_Model_Quote_Item $item)
    {
        if ($item->getId()) {
            $this->removeItem($item->getId());
        } else {
            $quoteItems = $this->getItemsCollection();
            $items = array($item);
            if ($item->getHasChildren()) {
                foreach ($item->getChildren() as $child) {
                    $items[] = $child;
                }
            }
            foreach ($quoteItems as $key => $quoteItem) {
                foreach ($items as $item) {
                    if ($quoteItem->compare($item)) {
                        $quoteItems->removeItemByKey($key);
                    }
                }
            }
        }

        return $this;
    }

    /**
     * Remove quote item by item identifier
     *
     * @param   int $itemId
     * @return  Mage_Sales_Model_Quote
     */
    public function removeItem($itemId)
    {
        $item = $this->getItemById($itemId);

        if ($item) {
            $item->setQuote($this);
            /**
             * If we remove item from quote - we can't use multishipping mode
             */
            $this->setIsMultiShipping(false);
            $item->isDeleted(true);
            if ($item->getHasChildren()) {
                foreach ($item->getChildren() as $child) {
                    $child->isDeleted(true);
                }
            }

            $parent = $item->getParentItem();
            if ($parent) {
                $parent->isDeleted(true);
            }

            Mage::dispatchEvent('sales_quote_remove_item', array('quote_item' => $item));
        }

        return $this;
    }

    /**
     * Mark all quote items as deleted (empty quote)
     *
     * @return Mage_Sales_Model_Quote
     */
    public function removeAllItems()
    {
        foreach ($this->getItemsCollection() as $itemId => $item) {
            if (is_null($item->getId())) {
                $this->getItemsCollection()->removeItemByKey($itemId);
            } else {
                $item->isDeleted(true);
            }
        }
        return $this;
    }

    /**
     * Adding new item to quote
     *
     * @param   Mage_Sales_Model_Quote_Item $item
     * @return  Mage_Sales_Model_Quote
     */
    public function addItem(Mage_Sales_Model_Quote_Item $item)
    {
        /**
         * Temporary workaround for purchase process: it is too dangerous to purchase more than one nominal item
         * or a mixture of nominal and non-nominal items, although technically possible.
         *
         * The problem is that currently it is implemented as sequential submission of nominal items and order, by one click.
         * It makes logically impossible to make the process of the purchase failsafe.
         * Proper solution is to submit items one by one with customer confirmation each time.
         */
        if ($item->isNominal() && $this->hasItems() || $this->hasNominalItems()) {
            Mage::throwException(
                Mage::helper('sales')->__('Nominal item can be purchased standalone only. To proceed please remove other items from the quote.')
            );
        }

        $item->setQuote($this);
        if (!$item->getId()) {
            $this->getItemsCollection()->addItem($item);
            Mage::dispatchEvent('sales_quote_add_item', array('quote_item' => $item));
        }
        return $this;
    }

    /**
     * Advanced func to add product to quote - processing mode can be specified there.
     * Returns error message if product type instance can't prepare product.
     *
     * @param mixed $product
     * @param null|float|Varien_Object $request
     * @param null|string $processMode
     * @return Mage_Sales_Model_Quote_Item|string
     */
    public function addProductAdvanced(Mage_Catalog_Model_Product $product, $request = null, $processMode = null)
    {
        if ($request === null) {
            $request = 1;
        }
        if (is_numeric($request)) {
            $request = new Varien_Object(array('qty'=>$request));
        }
        if (!($request instanceof Varien_Object)) {
            Mage::throwException(Mage::helper('sales')->__('Invalid request for adding product to quote.'));
        }

        $cartCandidates = $product->getTypeInstance(true)
            ->prepareForCartAdvanced($request, $product, $processMode);

        /**
         * Error message
         */
        if (is_string($cartCandidates)) {
            return $cartCandidates;
        }

        /**
         * If prepare process return one object
         */
        if (!is_array($cartCandidates)) {
            $cartCandidates = array($cartCandidates);
        }

        $parentItem = null;
        $errors = array();
        $items = array();
        foreach ($cartCandidates as $candidate) {
            // Child items can be sticked together only within their parent
            $stickWithinParent = $candidate->getParentProductId() ? $parentItem : null;
            $candidate->setStickWithinParent($stickWithinParent);
            $item = $this->_addCatalogProduct($candidate, $candidate->getCartQty());
            if($request->getResetCount() && !$stickWithinParent && $item->getId() === $request->getId()) {
                $item->setData('qty', 0);
            }
            $items[] = $item;

            /**
             * As parent item we should always use the item of first added product
             */
            if (!$parentItem) {
                $parentItem = $item;
            }
            if ($parentItem && $candidate->getParentProductId()) {
                $item->setParentItem($parentItem);
            }

            /**
             * We specify qty after we know about parent (for stock)
             */
            $item->addQty($candidate->getCartQty());

            // collect errors instead of throwing first one
            if ($item->getHasError()) {
                $message = $item->getMessage();
                if (!in_array($message, $errors)) { // filter duplicate messages
                    $errors[] = $message;
                }
            }
        }
        if (!empty($errors)) {
            Mage::throwException(implode("\n", $errors));
        }

        Mage::dispatchEvent('sales_quote_product_add_after', array('items' => $items));

        return $item;
    }


    /**
     * Add product to quote
     *
     * return error message if product type instance can't prepare product
     *
     * @param mixed $product
     * @param null|float|Varien_Object $request
     * @return Mage_Sales_Model_Quote_Item|string
     */
    public function addProduct(Mage_Catalog_Model_Product $product, $request = null)
    {
        return $this->addProductAdvanced(
            $product,
            $request,
            Mage_Catalog_Model_Product_Type_Abstract::PROCESS_MODE_FULL
        );
    }

    /**
     * Adding catalog product object data to quote
     *
     * @param   Mage_Catalog_Model_Product $product
     * @return  Mage_Sales_Model_Quote_Item
     */
    protected function _addCatalogProduct(Mage_Catalog_Model_Product $product, $qty = 1)
    {
        $newItem = false;
        $item = $this->getItemByProduct($product);
        if (!$item) {
            $item = Mage::getModel('sales/quote_item');
            $item->setQuote($this);
            if (Mage::app()->getStore()->isAdmin()) {
                $item->setStoreId($this->getStore()->getId());
            }
            else {
                $item->setStoreId(Mage::app()->getStore()->getId());
            }
            $newItem = true;
        }

        /**
         * We can't modify existing child items
         */
        if ($item->getId() && $product->getParentProductId()) {
            return $item;
        }

        $item->setOptions($product->getCustomOptions())
            ->setProduct($product);

        // Add only item that is not in quote already (there can be other new or already saved item
        if ($newItem) {
            $this->addItem($item);
        }

        return $item;
    }

    /**
     * Updates quote item with new configuration
     *
     * $params sets how current item configuration must be taken into account and additional options.
     * It's passed to Mage_Catalog_Helper_Product->addParamsToBuyRequest() to compose resulting buyRequest.
     *
     * Basically it can hold
     * - 'current_config', Varien_Object or array - current buyRequest that configures product in this item,
     *   used to restore currently attached files
     * - 'files_prefix': string[a-z0-9_] - prefix that was added at frontend to names of file options (file inputs), so they won't
     *   intersect with other submitted options
     *
     * For more options see Mage_Catalog_Helper_Product->addParamsToBuyRequest()
     *
     * @param int $itemId
     * @param Varien_Object $buyRequest
     * @param null|array|Varien_Object $params
     * @return Mage_Sales_Model_Quote_Item
     *
     * @see Mage_Catalog_Helper_Product::addParamsToBuyRequest()
     */
    public function updateItem($itemId, $buyRequest, $params = null)
    {
        $item = $this->getItemById($itemId);
        if (!$item) {
            Mage::throwException(Mage::helper('sales')->__('Wrong quote item id to update configuration.'));
        }
        $productId = $item->getProduct()->getId();

        //We need to create new clear product instance with same $productId
        //to set new option values from $buyRequest
        $product = Mage::getModel('catalog/product')
            ->setStoreId($this->getStore()->getId())
            ->load($productId);

        if (!$params) {
            $params = new Varien_Object();
        } else if (is_array($params)) {
            $params = new Varien_Object($params);
        }
        $params->setCurrentConfig($item->getBuyRequest());
        $buyRequest = Mage::helper('catalog/product')->addParamsToBuyRequest($buyRequest, $params);

        $buyRequest->setResetCount(true);
        $resultItem = $this->addProduct($product, $buyRequest);

        if (is_string($resultItem)) {
            Mage::throwException($resultItem);
        }

        if ($resultItem->getParentItem()) {
            $resultItem = $resultItem->getParentItem();
        }

        if ($resultItem->getId() != $itemId) {
            /*
             * Product configuration didn't stick to original quote item
             * It either has same configuration as some other quote item's product or completely new configuration
             */
            $this->removeItem($itemId);

            $items = $this->getAllItems();
            foreach ($items as $item) {
                if (($item->getProductId() == $productId) && ($item->getId() != $resultItem->getId())) {
                    if ($resultItem->compare($item)) {
                        // Product configuration is same as in other quote item
                        $resultItem->setQty($resultItem->getQty() + $item->getQty());
                        $this->removeItem($item->getId());
                        break;
                    }
                }
            }
        } else {
            $resultItem->setQty($buyRequest->getQty());
        }

        return $resultItem;
    }

    /**
     * Retrieve quote item by product id
     *
     * @param   Mage_Catalog_Model_Product $product
     * @return  Mage_Sales_Model_Quote_Item || false
     */
    public function getItemByProduct($product)
    {
        foreach ($this->getAllItems() as $item) {
            if ($item->representProduct($product)) {
                return $item;
            }
        }
        return false;
    }

    public function getItemsSummaryQty()
    {
        $qty = $this->getData('all_items_qty');
        if (is_null($qty)) {
            $qty = 0;
            foreach ($this->getAllItems() as $item) {
                if ($item->getParentItem()) {
                    continue;
                }

                if (($children = $item->getChildren()) && $item->isShipSeparately()) {
                    foreach ($children as $child) {
                        $qty+= $child->getQty()*$item->getQty();
                    }
                } else {
                    $qty+= $item->getQty();
                }
            }
            $this->setData('all_items_qty', $qty);
        }
        return $qty;
    }

    public function getItemVirtualQty()
    {
        $qty = $this->getData('virtual_items_qty');
        if (is_null($qty)) {
            $qty = 0;
            foreach ($this->getAllItems() as $item) {
                if ($item->getParentItem()) {
                    continue;
                }

                if (($children = $item->getChildren()) && $item->isShipSeparately()) {
                    foreach ($children as $child) {
                        if ($child->getProduct()->getIsVirtual()) {
                            $qty+= $child->getQty();
                        }
                    }
                } else {
                    if ($item->getProduct()->getIsVirtual()) {
                        $qty+= $item->getQty();
                    }
                }
            }
            $this->setData('virtual_items_qty', $qty);
        }
        return $qty;
    }

    /*********************** PAYMENTS ***************************/
    public function getPaymentsCollection()
    {
        if (is_null($this->_payments)) {
            $this->_payments = Mage::getModel('sales/quote_payment')->getCollection()
                ->setQuoteFilter($this->getId());

            if ($this->getId()) {
                foreach ($this->_payments as $payment) {
                    $payment->setQuote($this);
                }
            }
        }
        return $this->_payments;
    }

    /**
     * @return Mage_Sales_Model_Quote_Payment
     */
    public function getPayment()
    {
        foreach ($this->getPaymentsCollection() as $payment) {
            if (!$payment->isDeleted()) {
                return $payment;
            }
        }
        $payment = Mage::getModel('sales/quote_payment');
        $this->addPayment($payment);
        return $payment;
    }

    public function getPaymentById($paymentId)
    {
        foreach ($this->getPaymentsCollection() as $payment) {
            if ($payment->getId()==$paymentId) {
                return $payment;
            }
        }
        return false;
    }

    public function addPayment(Mage_Sales_Model_Quote_Payment $payment)
    {
        $payment->setQuote($this);
        if (!$payment->getId()) {
            $this->getPaymentsCollection()->addItem($payment);
        }
        return $this;
    }

    public function setPayment(Mage_Sales_Model_Quote_Payment $payment)
    {
        if (!$this->getIsMultiPayment() && ($old = $this->getPayment())) {
            $payment->setId($old->getId());
        }
        $this->addPayment($payment);

        return $payment;
    }

    public function removePayment()
    {
        $this->getPayment()->isDeleted(true);
        return $this;
    }

    /**
     * Collect totals
     *
     * @return Mage_Sales_Model_Quote
     */
    public function collectTotals()
    {
        /**
         * Protect double totals collection
         */
        if ($this->getTotalsCollectedFlag()) {
            return $this;
        }
        Mage::dispatchEvent($this->_eventPrefix . '_collect_totals_before', array($this->_eventObject => $this));

        $this->setSubtotal(0);
        $this->setBaseSubtotal(0);

        $this->setSubtotalWithDiscount(0);
        $this->setBaseSubtotalWithDiscount(0);

        $this->setGrandTotal(0);
        $this->setBaseGrandTotal(0);

        foreach ($this->getAllAddresses() as $address) {
            $address->setSubtotal(0);
            $address->setBaseSubtotal(0);

            $address->setGrandTotal(0);
            $address->setBaseGrandTotal(0);

            $address->collectTotals();

            $this->setSubtotal((float) $this->getSubtotal() + $address->getSubtotal());
            $this->setBaseSubtotal((float) $this->getBaseSubtotal() + $address->getBaseSubtotal());

            $this->setSubtotalWithDiscount(
                (float) $this->getSubtotalWithDiscount() + $address->getSubtotalWithDiscount()
            );
            $this->setBaseSubtotalWithDiscount(
                (float) $this->getBaseSubtotalWithDiscount() + $address->getBaseSubtotalWithDiscount()
            );

            $this->setGrandTotal((float) $this->getGrandTotal() + $address->getGrandTotal());
            $this->setBaseGrandTotal((float) $this->getBaseGrandTotal() + $address->getBaseGrandTotal());
        }

        Mage::helper('sales')->checkQuoteAmount($this, $this->getGrandTotal());
        Mage::helper('sales')->checkQuoteAmount($this, $this->getBaseGrandTotal());

        $this->setItemsCount(0);
        $this->setItemsQty(0);
        $this->setVirtualItemsQty(0);

        foreach ($this->getAllVisibleItems() as $item) {
            if ($item->getParentItem()) {
                continue;
            }

            $children = $item->getChildren();
            if ($children && $item->isShipSeparately()) {
                foreach ($children as $child) {
                    if ($child->getProduct()->getIsVirtual()) {
                        $this->setVirtualItemsQty($this->getVirtualItemsQty() + $child->getQty()*$item->getQty());
                    }
                }
            }

            if ($item->getProduct()->getIsVirtual()) {
                $this->setVirtualItemsQty($this->getVirtualItemsQty() + $item->getQty());
            }
            $this->setItemsCount($this->getItemsCount()+1);
            $this->setItemsQty((float) $this->getItemsQty()+$item->getQty());
        }

        $this->setData('trigger_recollect', 0);
        $this->_validateCouponCode();

        Mage::dispatchEvent($this->_eventPrefix . '_collect_totals_after', array($this->_eventObject => $this));

        $this->setTotalsCollectedFlag(true);
        return $this;
    }

    /**
     * Get all quote totals (sorted by priority)
     * Method process quote states isVirtual and isMultiShipping
     *
     * @return array
     */
    public function getTotals()
    {
        /**
         * If quote is virtual we are using totals of billing address because
         * all items assigned to it
         */
        if ($this->isVirtual()) {
            return $this->getBillingAddress()->getTotals();
        }

        $shippingAddress = $this->getShippingAddress();
        $totals = $shippingAddress->getTotals();
        // Going through all quote addresses and merge their totals
        foreach ($this->getAddressesCollection() as $address) {
            if ($address->isDeleted() || $address === $shippingAddress) {
                continue;
            }
            foreach ($address->getTotals() as $code => $total) {
                if (isset($totals[$code])) {
                    $totals[$code]->merge($total);
                } else {
                    $totals[$code] = $total;
                }
            }
        }

        $sortedTotals = array();
        foreach ($this->getBillingAddress()->getTotalModels() as $total) {
            /* @var $total Mage_Sales_Model_Quote_Address_Total_Abstract */
            if (isset($totals[$total->getCode()])) {
                $sortedTotals[$total->getCode()] = $totals[$total->getCode()];
            }
        }
        return $sortedTotals;
    }

    public function addMessage($message, $index = 'error')
    {
        $messages = $this->getData('messages');
        if (is_null($messages)) {
            $messages = array();
        }

        if (isset($messages[$index])) {
            return $this;
        }

        if (is_string($message)) {
            $message = Mage::getSingleton('core/message')->error($message);
        }

        $messages[$index] = $message;
        $this->setData('messages', $messages);
        return $this;
    }

    /**
     * Retrieve current quote messages
     *
     * @return array
     */
    public function getMessages()
    {
        $messages = $this->getData('messages');
        if (is_null($messages)) {
            $messages = array();
            $this->setData('messages', $messages);
        }
        return $messages;
    }

    /**
     * Retrieve current quote errors
     *
     * @return array
     */
    public function getErrors()
    {
        $errors = array();
        foreach ($this->getMessages() as $message) {
            /* @var $error Mage_Core_Model_Message_Abstract */
            if ($message->getType() == Mage_Core_Model_Message::ERROR) {
                array_push($errors, $message);
            }
        }
        return $errors;
    }

    /**
     * Sets flag, whether this quote has some error associated with it.
     *
     * @param bool $flag
     * @return Mage_Sales_Model_Quote
     */
    protected function _setHasError($flag)
    {
        return $this->setData('has_error', $flag);
    }

    /**
     * Sets flag, whether this quote has some error associated with it.
     * When TRUE - also adds 'unknown' error information to list of quote errors.
     * When FALSE - clears whole list of quote errors.
     * It's recommended to use addErrorInfo() instead - to be able to remove error statuses later.
     *
     * @param bool $flag
     * @return Mage_Sales_Model_Quote
     * @see addErrorInfo()
     */
    public function setHasError($flag)
    {
        if ($flag) {
            $this->addErrorInfo();
        } else {
            $this->_clearErrorInfo();
        }
        return $this;
    }

    /**
     * Clears list of errors, associated with this quote.
     * Also automatically removes error-flag from oneself.
     *
     * @return Mage_Sales_Model_Quote
     */
    protected function _clearErrorInfo()
    {
        $this->_errorInfoGroups = array();
        $this->_setHasError(false);
        return $this;
    }

    /**
     * Adds error information to the quote.
     * Automatically sets error flag.
     *
     * @param string $type An internal error type ('error', 'qty', etc.), passed then to adding messages routine
     * @param string|null $origin Usually a name of module, that embeds error
     * @param int|null $code Error code, unique for origin, that sets it
     * @param string|null $message Error message
     * @param Varien_Object|null $additionalData Any additional data, that caller would like to store
     * @return Mage_Sales_Model_Quote
     */
    public function addErrorInfo($type = 'error', $origin = null, $code = null, $message = null, $additionalData = null)
    {
        if (!isset($this->_errorInfoGroups[$type])) {
            $this->_errorInfoGroups[$type] = Mage::getModel('sales/status_list');
        }

        $this->_errorInfoGroups[$type]->addItem($origin, $code, $message, $additionalData);

        if ($message !== null) {
            $this->addMessage($message, $type);
        }
        $this->_setHasError(true);

        return $this;
    }

    /**
     * Removes error infos, that have parameters equal to passed in $params.
     * $params can have following keys (if not set - then any item is good for this key):
     *   'origin', 'code', 'message'
     *
     * @param string $type An internal error type ('error', 'qty', etc.), passed then to adding messages routine
     * @param array $params
     * @return Mage_Sales_Model_Quote
     */
    public function removeErrorInfosByParams($type = 'error', $params)
    {
        if ($type && !isset($this->_errorInfoGroups[$type])) {
            return $this;
        }

        $errorLists = array();
        if ($type) {
            $errorLists[] = $this->_errorInfoGroups[$type];
        } else {
            $errorLists = $this->_errorInfoGroups;
        }

        foreach ($errorLists as $type => $errorList) {
            $removedItems = $errorList->removeItemsByParams($params);
            foreach ($removedItems as $item) {
                if ($item['message'] !== null) {
                    $this->removeMessageByText($type, $item['message']);
                }
            }
        }

        $errorsExist = false;
        foreach ($this->_errorInfoGroups as $errorListCheck) {
            if ($errorListCheck->getItems()) {
                $errorsExist = true;
                break;
            }
        }
        if (!$errorsExist) {
            $this->_setHasError(false);
        }

        return $this;
    }

    /**
     * Removes message by text
     *
     * @param string $type
     * @param string $text
     * @return Mage_Sales_Model_Quote
     */
    public function removeMessageByText($type = 'error', $text)
    {
        $messages = $this->getData('messages');
        if (is_null($messages)) {
            $messages = array();
        }

        if (!isset($messages[$type])) {
            return $this;
        }

        $message = $messages[$type];
        if ($message instanceof Mage_Core_Model_Message_Abstract) {
            $message = $message->getText();
        } else if (!is_string($message)) {
            return $this;
        }
        if ($message == $text) {
            unset($messages[$type]);
            $this->setData('messages', $messages);
        }
        return $this;
    }

    /**
     * Generate new increment order id and associate it with current quote
     *
     * @return Mage_Sales_Model_Quote
     */
    public function reserveOrderId()
    {
        if (!$this->getReservedOrderId()) {
            $this->setReservedOrderId($this->_getResource()->getReservedOrderId($this));
        } else {
            //checking if reserved order id was already used for some order
            //if yes reserving new one if not using old one
            if ($this->_getResource()->isOrderIncrementIdUsed($this->getReservedOrderId())) {
                $this->setReservedOrderId($this->_getResource()->getReservedOrderId($this));
            }
        }
        return $this;
    }

    public function validateMinimumAmount($multishipping = false)
    {
        $storeId = $this->getStoreId();
        $minOrderActive = Mage::getStoreConfigFlag('sales/minimum_order/active', $storeId);
        $minOrderMulti  = Mage::getStoreConfigFlag('sales/minimum_order/multi_address', $storeId);
        $minAmount      = Mage::getStoreConfig('sales/minimum_order/amount', $storeId);

        if (!$minOrderActive) {
            return true;
        }

        $addresses = $this->getAllAddresses();

        if ($multishipping) {
            if ($minOrderMulti) {
                foreach ($addresses as $address) {
                    foreach ($address->getQuote()->getItemsCollection() as $item) {
                        $amount = $item->getBaseRowTotal() - $item->getBaseDiscountAmount();
                        if ($amount < $minAmount) {
                            return false;
                        }
                    }
                }
            } else {
                $baseTotal = 0;
                foreach ($addresses as $address) {
                    /* @var $address Mage_Sales_Model_Quote_Address */
                    $baseTotal += $address->getBaseSubtotalWithDiscount();
                }
                if ($baseTotal < $minAmount) {
                    return false;
                }
            }
        } else {
            foreach ($addresses as $address) {
                /* @var $address Mage_Sales_Model_Quote_Address */
                if (!$address->validateMinimumAmount()) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Check quote for virtual product only
     *
     * @return bool
     */
    public function isVirtual()
    {
        $isVirtual = true;
        $countItems = 0;
        foreach ($this->getItemsCollection() as $_item) {
            /* @var $_item Mage_Sales_Model_Quote_Item */
            if ($_item->isDeleted() || $_item->getParentItemId()) {
                continue;
            }
            $countItems ++;
            if (!$_item->getProduct()->getIsVirtual()) {
                $isVirtual = false;
                break;
            }
        }
        return $countItems == 0 ? false : $isVirtual;
    }

    /**
     * Check quote for virtual product only
     *
     * @return bool
     */
    public function getIsVirtual()
    {
        return intval($this->isVirtual());
    }

    /**
     * Has a virtual products on quote
     *
     * @return bool
     */
    public function hasVirtualItems()
    {
        $hasVirtual = false;
        foreach ($this->getItemsCollection() as $_item) {
            if ($_item->getParentItemId()) {
                continue;
            }
            if ($_item->getProduct()->isVirtual()) {
                $hasVirtual = true;
            }
        }
        return $hasVirtual;
    }

    /**
     * Merge quotes
     *
     * @param   Mage_Sales_Model_Quote $quote
     * @return  Mage_Sales_Model_Quote
     */
    public function merge(Mage_Sales_Model_Quote $quote)
    {
        Mage::dispatchEvent(
            $this->_eventPrefix . '_merge_before',
            array(
                 $this->_eventObject=>$this,
                 'source'=>$quote
            )
        );

        foreach ($quote->getAllVisibleItems() as $item) {
            $found = false;
            foreach ($this->getAllItems() as $quoteItem) {
                if ($quoteItem->compare($item)) {
                    $quoteItem->setQty($quoteItem->getQty() + $item->getQty());
                    $found = true;
                    break;
                }
            }

            if (!$found) {
                $newItem = clone $item;
                $this->addItem($newItem);
                if ($item->getHasChildren()) {
                    foreach ($item->getChildren() as $child) {
                        $newChild = clone $child;
                        $newChild->setParentItem($newItem);
                        $this->addItem($newChild);
                    }
                }
            }
        }

        /**
         * Init shipping and billing address if quote is new
         */
        if (!$this->getId()) {
            $this->getShippingAddress();
            $this->getBillingAddress();
        }

        if ($quote->getCouponCode()) {
            $this->setCouponCode($quote->getCouponCode());
        }

        Mage::dispatchEvent(
            $this->_eventPrefix . '_merge_after',
            array(
                 $this->_eventObject=>$this,
                 'source'=>$quote
            )
        );

        return $this;
    }

    /**
     * Whether there are recurring items
     *
     * @return bool
     */
    public function hasRecurringItems()
    {
        foreach ($this->getAllVisibleItems() as $item) {
            if ($item->getProduct() && $item->getProduct()->isRecurring()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Getter whether quote has nominal items
     * Can bypass treating virtual items as nominal
     *
     * @param bool $countVirtual
     * @return bool
     */
    public function hasNominalItems($countVirtual = true)
    {
        foreach ($this->getAllVisibleItems() as $item) {
            if ($item->isNominal()) {
                if ((!$countVirtual) && $item->getProduct()->isVirtual()) {
                    continue;
                }
                return true;
            }
        }
        return false;
    }

    /**
     * Whether quote has nominal items only
     *
     * @return bool
     */
    public function isNominal()
    {
        foreach ($this->getAllVisibleItems() as $item) {
            if (!$item->isNominal()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Create recurring payment profiles basing on the current items
     *
     * @return array
     */
    public function prepareRecurringPaymentProfiles()
    {
        if (!$this->getTotalsCollectedFlag()) {
            // Whoops! Make sure nominal totals must be calculated here.
            throw new Exception('Quote totals must be collected before this operation.');
        }

        $result = array();
        foreach ($this->getAllVisibleItems() as $item) {
            $product = $item->getProduct();
            if (is_object($product) && ($product->isRecurring())
                && $profile = Mage::getModel('sales/recurring_profile')->importProduct($product)
            ) {
                $profile->importQuote($this);
                $profile->importQuoteItem($item);
                $result[] = $profile;
            }
        }
        return $result;
    }

    protected function _validateCouponCode()
    {
        $code = $this->_getData('coupon_code');
        if (strlen($code)) {
            $addressHasCoupon = false;
            $addresses = $this->getAllAddresses();
            if (count($addresses)>0) {
                foreach ($addresses as $address) {
                    if ($address->hasCouponCode()) {
                        $addressHasCoupon = true;
                    }
                }
                if (!$addressHasCoupon) {
                    $this->setCouponCode('');
                }
            }
        }
        return $this;
    }

    /**
     * Trigger collect totals after loading, if required
     *
     * @return Mage_Sales_Model_Quote
     */
    protected function _afterLoad()
    {
        // collect totals and save me, if required
        if (1 == $this->getData('trigger_recollect')) {
            $this->collectTotals()->save();
        }
        return parent::_afterLoad();
    }

    /**
     * @deprecated after 1.4 beta1 - one page checkout responsibility
     */
    const CHECKOUT_METHOD_REGISTER  = 'register';
    const CHECKOUT_METHOD_GUEST     = 'guest';
    const CHECKOUT_METHOD_LOGIN_IN  = 'login_in';

    /**
     * Return quote checkout method code
     *
     * @deprecated after 1.4 beta1 it is checkout module responsibility
     * @param boolean $originalMethod if true return defined method from begining
     * @return string
     */
    public function getCheckoutMethod($originalMethod = false)
    {
        if ($this->getCustomerId() && !$originalMethod) {
            return self::CHECKOUT_METHOD_LOGIN_IN;
        }
        return $this->_getData('checkout_method');
    }

    /**
     * Check is allow Guest Checkout
     *
     * @deprecated after 1.4 beta1 it is checkout module responsibility
     * @return bool
     */
    public function isAllowedGuestCheckout()
    {
        return Mage::helper('checkout')->isAllowedGuestCheckout($this, $this->getStoreId());
    }

    /**
     * Prevent quote from saving
     *
     * @return Mage_Sales_Model_Quote
     */
    public function preventSaving()
    {
        $this->_preventSaving = true;
        return $this;
    }

    /**
     * Save quote with prevention checking
     *
     * @return Mage_Sales_Model_Quote
     */
    public function save()
    {
        if ($this->_preventSaving) {
            return $this;
        }
        return parent::save();
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Sales
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Sales module base helper
 *
 * @category    Mage
 * @package     Mage_Sales
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Sales_Helper_Data extends Mage_Core_Helper_Data
{
    /**
     * Maximum available number
     */
    const MAXIMUM_AVAILABLE_NUMBER = 99999999;

    /**
     * Default precision for price calculations
     */
    const PRECISION_VALUE = 0.0001;

    /**
     * Check quote amount
     *
     * @param Mage_Sales_Model_Quote $quote
     * @param decimal $amount
     * @return Mage_Sales_Helper_Data
     */
    public function checkQuoteAmount(Mage_Sales_Model_Quote $quote, $amount)
    {
        if (!$quote->getHasError() && ($amount>=self::MAXIMUM_AVAILABLE_NUMBER)) {
            $quote->setHasError(true);
            $quote->addMessage(
                $this->__('Items maximum quantity or price do not allow checkout.')
            );
        }
        return $this;
    }

    /**
     * Check allow to send new order confirmation email
     *
     * @param mixed $store
     * @return bool
     */
    public function canSendNewOrderConfirmationEmail($store = null)
    {
        return Mage::getStoreConfigFlag(Mage_Sales_Model_Order::XML_PATH_EMAIL_ENABLED, $store);
    }

    /**
     * Check allow to send new order email
     *
     * @param mixed $store
     * @return bool
     */
    public function canSendNewOrderEmail($store = null)
    {
        return $this->canSendNewOrderConfirmationEmail($store);
    }

    /**
     * Check allow to send order comment email
     *
     * @param mixed $store
     * @return bool
     */
    public function canSendOrderCommentEmail($store = null)
    {
        return Mage::getStoreConfigFlag(Mage_Sales_Model_Order::XML_PATH_UPDATE_EMAIL_ENABLED, $store);
    }

    /**
     * Check allow to send new shipment email
     *
     * @param mixed $store
     * @return bool
     */
    public function canSendNewShipmentEmail($store = null)
    {
        return Mage::getStoreConfigFlag(Mage_Sales_Model_Order_Shipment::XML_PATH_EMAIL_ENABLED, $store);
    }

    /**
     * Check allow to send shipment comment email
     *
     * @param mixed $store
     * @return bool
     */
    public function canSendShipmentCommentEmail($store = null)
    {
        return Mage::getStoreConfigFlag(Mage_Sales_Model_Order_Shipment::XML_PATH_UPDATE_EMAIL_ENABLED, $store);
    }

    /**
     * Check allow to send new invoice email
     *
     * @param mixed $store
     * @return bool
     */
    public function canSendNewInvoiceEmail($store = null)
    {
        return Mage::getStoreConfigFlag(Mage_Sales_Model_Order_Invoice::XML_PATH_EMAIL_ENABLED, $store);
    }

    /**
     * Check allow to send invoice comment email
     *
     * @param mixed $store
     * @return bool
     */
    public function canSendInvoiceCommentEmail($store = null)
    {
        return Mage::getStoreConfigFlag(Mage_Sales_Model_Order_Invoice::XML_PATH_UPDATE_EMAIL_ENABLED, $store);
    }

    /**
     * Check allow to send new creditmemo email
     *
     * @param mixed $store
     * @return bool
     */
    public function canSendNewCreditmemoEmail($store = null)
    {
        return Mage::getStoreConfigFlag(Mage_Sales_Model_Order_Creditmemo::XML_PATH_EMAIL_ENABLED, $store);
    }

    /**
     * Check allow to send creditmemo comment email
     *
     * @param mixed $store
     * @return bool
     */
    public function canSendCreditmemoCommentEmail($store = null)
    {
        return Mage::getStoreConfigFlag(Mage_Sales_Model_Order_Creditmemo::XML_PATH_UPDATE_EMAIL_ENABLED, $store);
    }

    /**
     * Get old field map
     *
     * @param string $entityId
     * @return array
     */
    public function getOldFieldMap($entityId)
    {
        $node = Mage::getConfig()->getNode('global/sales/old_fields_map/' . $entityId);
        if ($node === false) {
            return array();
        }
        return (array) $node;
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Core
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Core layout update resource model
 *
 * @category    Mage
 * @package     Mage_Core
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Core_Model_Resource_Layout extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Define main table
     *
     */
    protected function _construct()
    {
        $this->_init('core/layout_update', 'layout_update_id');
    }

    /**
     * Retrieve layout updates by handle
     *
     * @param string $handle
     * @param array $params
     * @return string
     */
    public function fetchUpdatesByHandle($handle, $params = array())
    {
        $bind = array(
            'store_id'  => Mage::app()->getStore()->getId(),
            'area'      => Mage::getSingleton('core/design_package')->getArea(),
            'package'   => Mage::getSingleton('core/design_package')->getPackageName(),
            'theme'     => Mage::getSingleton('core/design_package')->getTheme('layout')
        );

        foreach ($params as $key => $value) {
            if (isset($bind[$key])) {
                $bind[$key] = $value;
            }
        }
        $bind['layout_update_handle'] = $handle;
        $result = '';

        $readAdapter = $this->_getReadAdapter();
        if ($readAdapter) {
            $select = $readAdapter->select()
                ->from(array('layout_update' => $this->getMainTable()), array('xml'))
                ->join(array('link'=>$this->getTable('core/layout_link')), 
                        'link.layout_update_id=layout_update.layout_update_id',
                        '')
                ->where('link.store_id IN (0, :store_id)')
                ->where('link.area = :area')
                ->where('link.package = :package')
                ->where('link.theme = :theme')
                ->where('layout_update.handle = :layout_update_handle')
                ->order('layout_update.sort_order ' . Varien_Db_Select::SQL_ASC);

            $result = join('', $readAdapter->fetchCol($select, $bind));
        }
        return $result;
    }
}
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Afptc
 * @version    1.1.12
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Afptc_Block_Popup extends Mage_Catalog_Block_Product_Abstract
{
    protected $_products = null;

    public function canShow()
    {
        return (!$this->helper('awafptc')->extensionDisabled()
            && count($this->getProducts()) != 0
            && !$this->_cookieDisallowed())
        ;
    }

    protected function _getPreparedProduct($productId, $ruleModel)
    {
        $product = Mage::getModel('catalog/product')->load($productId);
        if (null !== $product->getId() && $product->isSaleable()) {
            $this->_prepareProductPriceForRule($ruleModel, $product);
            return $product;
        }
        return null;
    }

    protected function _prepareProductsByRule($ruleModel)
    {
        if ($ruleModel->getSimpleAction() == AW_Afptc_Model_Rule::BUY_X_GET_Y_ACTION) {
            foreach ($ruleModel->getBuyXProductIds() as $itemId => $productId) {
                $_product = $this->_getPreparedProduct($productId, $ruleModel);
                if (null === $_product) {
                    continue;
                }
                $_product->addData(array(
                    'afptc_item_id' => $itemId,
                    'afptc_rule_id' => $ruleModel->getId()
                ));
                array_push($this->_products, $_product);
            }
        }

        if ($ruleModel->getSimpleAction() == AW_Afptc_Model_Rule::BY_PERCENT_ACTION) {
            $_product = $this->_getPreparedProduct($ruleModel->getProductId(), $ruleModel);
            if (null !== $_product) {
                $_product->addData(array(
                    'afptc_item_id' => null,
                    'afptc_rule_id' => $ruleModel->getId()
                ));
                array_push($this->_products, $_product);
            }
        }
        return $this;
    }

    public function getProducts()
    {
        $cartModel = Mage::getSingleton('checkout/cart');
        $cartModel->getQuote()->collectTotals();
        if (null === $this->_products) {
            $store = Mage::app()->getStore();
            $rulesCollection = Mage::getResourceModel('awafptc/rule')->getActiveRulesCollection($store);
            $this->_products = array();
            foreach ($rulesCollection as $ruleModel) {
                if (isset($_stopFlag)) {
                    break;
                }

                /** @var AW_Afptc_Model_Rule $ruleModel */
                $ruleModel->load($ruleModel->getId());
                if (!$ruleModel->validate($cartModel)) {
                    continue;
                }

                if ($ruleModel->getStopRulesProcessing()) {
                    $_stopFlag = true;
                }

                if (!$ruleModel->getShowPopup()) {
                    continue;
                }

                if (Mage::helper('awafptc')->getDeclineRuleCookie($ruleModel->getId())) {
                    continue;
                }

                if ($ruleModel->getSimpleAction() == AW_Afptc_Model_Rule::BUY_X_GET_Y_ACTION
                    && count($ruleModel->getBuyXProductIds()) == 0
                ) {
                    continue;
                }

                if (true === $ruleModel->getAlreadyApplied()) {
                    continue;
                }

                $this->_prepareProductsByRule($ruleModel);
            }
        }
        return $this->_products;
    }

    public function getPostUrl()
    {
        return $this->getUrl('awafptc/cart/addProduct');
    }

    public function getDeclinePopupCookieName()
    {
        return $this->helper('awafptc')->getDeclinePopupCookieName();
    }

    public function getDoNotShowAllowed()
    {
        return $this->helper('awafptc')->isDoNotShowOptionAllowed();
    }

    protected function _prepareProductPriceForRule(AW_Afptc_Model_Rule $ruleModel, &$productModel)
    {
        $finalPrice = $productModel->getFinalPrice();
        $productModel->setFinalPrice(max(0, $finalPrice - ($finalPrice * $ruleModel->getDiscount() / 100)));
        return $this;
    }

    protected function _cookieDisallowed()
    {
        return Mage::getSingleton('core/cookie')->get($this->getDeclinePopupCookieName());
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Sales
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Sales order view block
 *
 * @method int|null getCustomerId()
 *
 * @category   Mage
 * @package    Mage_Sales
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Sales_Block_Reorder_Sidebar extends Mage_Core_Block_Template
{
    /**
     * Init orders and templates
     */
    public function __construct()
    {
        parent::__construct();

        if ($this->_getCustomerSession()->isLoggedIn()) {
            $this->setTemplate('sales/order/history.phtml');
            $this->initOrders();
        }
    }

    /**
     * Init customer order for display on front
     */
    public function initOrders()
    {
        $customerId = $this->getCustomerId() ? $this->getCustomerId()
            : $this->_getCustomerSession()->getCustomer()->getId();

        $orders = Mage::getResourceModel('sales/order_collection')
            ->addAttributeToFilter('customer_id', $customerId)
            ->addAttributeToFilter('state',
                array('in' => Mage::getSingleton('sales/order_config')->getVisibleOnFrontStates())
            )
            ->addAttributeToSort('created_at', 'desc')
            ->setPage(1,1);
        //TODO: add filter by current website

        $this->setOrders($orders);
    }

    /**
     * Get list of last ordered products
     *
     * @return array
     */
    public function getItems()
    {
        $items = array();
        $order = $this->getLastOrder();
        $limit = 5;

        if ($order) {
            $website = Mage::app()->getStore()->getWebsiteId();
            foreach ($order->getParentItemsRandomCollection($limit) as $item) {
                if ($item->getProduct() && in_array($website, $item->getProduct()->getWebsiteIds())) {
                    $items[] = $item;
                }
            }
        }

        return $items;
    }

    /**
     * Check item product availability for reorder
     *
     * @param  Mage_Sales_Model_Order_Item $orderItem
     * @return boolean
     */
    public function isItemAvailableForReorder(Mage_Sales_Model_Order_Item $orderItem)
    {
        if ($orderItem->getProduct()) {
            return $orderItem->getProduct()->getStockItem()->getIsInStock();
        }
        return false;
    }

    /**
     * Retrieve form action url and set "secure" param to avoid confirm
     * message when we submit form from secure page to unsecure
     *
     * @return string
     */
    public function getFormActionUrl()
    {
        return $this->getUrl('checkout/cart/addgroup', array('_secure' => true));
    }

    /**
     * Last order getter
     *
     * @return Mage_Sales_Model_Order|bool
     */
    public function getLastOrder()
    {
        if (!$this->getOrders()) {
            return false;
        }

        foreach ($this->getOrders() as $order) {
            return $order;
        }
        return false;
    }

    /**
     * Render "My Orders" sidebar block
     *
     * @return string
     */
    protected function _toHtml()
    {
        return $this->_getCustomerSession()->isLoggedIn() || $this->getCustomerId() ? parent::_toHtml() : '';
    }

    /**
     * Retrieve customer session instance
     *
     * @return Mage_Customer_Model_Session
     */
    protected function _getCustomerSession()
    {
        return Mage::getSingleton('customer/session');
    }

    /**
     * Retrieve block cache tags
     *
     * @return array
     */
    public function getCacheTags()
    {
        return array_merge(
            parent::getCacheTags(),
            $this->getItemsTags($this->_getItemProducts())
        );
    }

    /**
     * Retrieve products list from items
     *
     * @return array
     */
    protected function _getItemProducts()
    {
        $products =  array();
        foreach ($this->getItems() as $item) {
            $products[] = $item->getProduct();
        }
        return $products;
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_ConfigurableSwatches
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Mage_ConfigurableSwatches_Model_Observer extends Mage_Core_Model_Abstract
{
    /**
     * Attach children products after product list load
     * Observes: catalog_block_product_list_collection
     *
     * @param Varien_Event_Observer $observer
     */
    public function productListCollectionLoadAfter(Varien_Event_Observer $observer)
    {
        if (!Mage::helper('configurableswatches')->isEnabled()) { // check if functionality disabled
            return; // exit without loading swatch functionality
        }

        /* @var $mediaHelper Mage_ConfigurableSwatches_Helper_Mediafallback */
        $mediaHelper = Mage::helper('configurableswatches/mediafallback');

        /** @var $priceHelper Mage_ConfigurableSwatches_Helper_List_Price */
        $priceHelper = Mage::helper('configurableswatches/list_price');

        /* @var $collection Mage_Catalog_Model_Resource_Product_Collection */
        $collection = $observer->getCollection();

        if ($collection
            instanceof Mage_ConfigurableSwatches_Model_Resource_Catalog_Product_Type_Configurable_Product_Collection) {
            // avoid recursion
            return;
        }

        $products = $collection->getItems();

        $mediaHelper->attachChildrenProducts($products, $collection->getStoreId());

        $mediaHelper->attachProductChildrenAttributeMapping($products, $collection->getStoreId());

        if ($priceHelper->isEnabled()) {
            $priceHelper->attachConfigurableProductChildrenPricesMapping($products, $collection->getStoreId());
        }

        $mediaHelper->attachGallerySetToCollection($products, $collection->getStoreId());

        /* @var $product Mage_Catalog_Model_Product */
        foreach ($products as $product) {
            $mediaHelper->groupMediaGalleryImages($product);
            Mage::helper('configurableswatches/productimg')
                ->indexProductImages($product, $product->getListSwatchAttrValues());
        }

    }

    /**
     * Attach children products after product load
     * Observes: catalog_product_load_after
     *
     * @param Varien_Event_Observer $observer
     */
    public function productLoadAfter(Varien_Event_Observer $observer) {

        if (!Mage::helper('configurableswatches')->isEnabled()) { // functionality disabled
            return; // exit without loading swatch functionality
        }

        /* @var $helper Mage_ConfigurableSwatches_Helper_Mediafallback */
        $helper = Mage::helper('configurableswatches/mediafallback');

        /* @var $product Mage_Catalog_Model_Product */
        $product = $observer->getDataObject();

        if ($product->getTypeId() != Mage_Catalog_Model_Product_Type_Configurable::TYPE_CODE) {
            return;
        }

        $helper->groupMediaGalleryImages($product);

        $helper->attachProductChildrenAttributeMapping(array($product), $product->getStoreId(), false);
    }

    /**
     * Instruct media attribute to load images for product's children
     * if config swatches enabled.
     * Observes: catalog_product_attribute_backend_media_load_gallery_before
     *
     * @param Varien_Event_Observer $observer
     */
    public function loadChildProductImagesOnMediaLoad(Varien_Event_Observer $observer) {

        if (!Mage::helper('configurableswatches')->isEnabled()) { // functionality disabled
            return; // exit without loading swatch functionality
        }

        /* @var $eventWrapper Varien_Object */
        $eventWrapper = $observer->getEventObjectWrapper();
        /* @var $product Mage_Catalog_Model_Product */
        $product = $eventWrapper->getProduct();

        if ($product->getTypeId() != Mage_Catalog_Model_Product_Type_Configurable::TYPE_CODE) {
            return;
        }

        /* @var $productType Mage_Catalog_Model_Product_Type_Configurable */
        $productType = Mage::getModel('catalog/product_type_configurable');

        $childrenProducts = $productType->getUsedProducts(null, $product);
        $product->setChildrenProducts($childrenProducts);

        $mediaProductIds = array();
        foreach ($childrenProducts as $childProduct) {
            $mediaProductIds[] = $childProduct->getId();
        }

        if (empty($mediaProductIds)) { // no children product IDs found
            return; // stop execution of method
        }

        $mediaProductIds[] = $product->getId(); // ensure original product's media images are still loaded

        $eventWrapper->setProductIdsOverride($mediaProductIds);
    }

    /**
     * Convert a catalog layer block with the right templates
     * Observes: controller_action_layout_generate_blocks_after
     *
     * @param Varien_Event_Observer $observer
     */
    public function convertLayerBlock(Varien_Event_Observer $observer)
    {
        $front = Mage::app()->getRequest()->getRouteName();
        $controller = Mage::app()->getRequest()->getControllerName();
        $action = Mage::app()->getRequest()->getActionName();

        // Perform this operation if we're on a category view page or search results page
        if (($front == 'catalog' && $controller == 'category' && $action == 'view')
            || ($front == 'catalogsearch' && $controller == 'result' && $action == 'index')) {

            // Block name for layered navigation differs depending on which Magento edition we're in
            $blockName = 'catalog.leftnav';
            if (Mage::getEdition() == Mage::EDITION_ENTERPRISE) {
                $blockName = ($front == 'catalogsearch') ? 'enterprisesearch.leftnav' : 'enterprisecatalog.leftnav';
            } elseif ($front == 'catalogsearch') {
                $blockName = 'catalogsearch.leftnav';
            }
            Mage::helper('configurableswatches/productlist')->convertLayerBlock($blockName);
        }
    }
}

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.web-cooking.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_All
 * @copyright  Copyright (c) 2011-2016 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
class Webcooking_All_Helper_Data extends Mage_Core_Helper_Abstract {

    public function isModuleInstalled($module) {
        $modules = Mage::getConfig()->getNode('modules')->children();
        $modulesArray = (array) $modules;

        return isset($modulesArray[$module]);
    }
    
    public function isModuleEnable($module) {
        return $this->isModuleEnabled($module);
    }

    public function applyReplaceAccent($data) {
        $data = @iconv('UTF-8', 'UTF-8//IGNORE', $data);
        $data = htmlentities($data, ENT_COMPAT, 'UTF-8');
        $data = preg_replace('#\&([A-za-z])(?:acute|cedil|circ|grave|ring|tilde|uml)\;#', '\1', $data);
        $data = preg_replace('#\&([A-za-z]{2})(?:lig)\;#', '\1', $data); // pour les ligatures e.g. '&oelig;'
        $data = preg_replace('#\&[^;]+\;#', '', $data); // supprime les autres caractères
        return $data;
    }
    
    public function sanitizeUrlKey($urlKey) {
        $urlKey = $this->applyReplaceAccent($urlKey);
        $urlKey = preg_replace('%[^a-zA-Z0-9-]%i', '-', strtolower($urlKey));
        $urlKey = preg_replace('%--+%i', '-', $urlKey);
        $urlKey = preg_replace('%^-|-$%i', '', $urlKey);
        return $urlKey;
    }
    
    
    /**
     * 
     * @param Mage_Catalog_Model_Product $product : Product to check
     * @param integer $decimals
     * @param boolean $withSymbol
     * @param boolean $allowEmptyEndDate
     * @return boolean|string promo percentage or false if no promo
     */
    public function isInPromo($product, $decimals = 0, $withSymbol = true, $allowEmptyEndDate = true) {
        if (!$product)
            return false;

        if (!$product->getSpecialPrice())
            return false;

        if ($product->getTypeId() == 'bundle') {
            $percentage = (100 - $product->getSpecialPrice());
        } else {
            $percentage = (100 - round((100 * $product->getSpecialPrice()) / $product->getPrice(), $decimals));

            if ($product->getSpecialPrice() >= $product->getPrice())
                return false;
        }
        $now = time();
        $aDay = 24 * 60 * 60;
        if (!$allowEmptyEndDate && !trim($product->getSpecialToDate()))
            return false;
        if (trim($product->getSpecialToDate()) && $now >= strtotime($product->getSpecialToDate()) + $aDay)
            return false;
        if (trim($product->getSpecialFromDate()) && $now < strtotime($product->getSpecialFromDate()))
            return false;
        if ($product->getTypeId() != 'bundle' && $product->getPrice() <= 0)
            return true;
        if ($withSymbol)
            $percentage .= '%';

        return $percentage;
    }


}

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.web-cooking.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_GoogleUniversalAnalytics
 * @copyright  Copyright (c) 2011-2016 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
class Webcooking_GoogleUniversalAnalytics_Helper_Ecommerce extends Webcooking_All_Helper_Data {
 
    
   protected $_brandAttributeCode = null;


   public function getBrandAttributeCode($store = null) {
       if(is_null($this->_brandAttributeCode)) {
           $this->_brandAttributeCode = Mage::getStoreConfig('googleuniversalanalytics/enhanced_ecommerce/brand', $store);
       }
       return $this->_brandAttributeCode;
   }

   
    public function useStoreCurrency($store = null) {
        return Mage::getStoreConfigFlag('googleuniversalanalytics/transactions/use_store_currency', $store);
    }
    
    public function sendChildrenItems($store = null) {
        return Mage::getStoreConfigFlag('googleuniversalanalytics/transactions/send_children_items', $store);
    }
   
   public function getProductAttributeValue($product, $attributeCode, $storeId = false) {
       if(!$product || !$product->getId()) {
           return '';
       }
       if(!$storeId) {
           $storeId = Mage::app()->getStore()->getId();
       }
       $attribute = Mage::helper('wcooall/attribute')->getAttribute($attributeCode);
       if(!$attribute->getId()) {
           return '';
       }
       if($product->getData($attributeCode)) {
           $data = $product->getData($attributeCode);
       } else {
            $data = Mage::getSingleton('catalog/product')->getResource()->getAttributeRawValue($product->getId(), $attributeCode, $storeId);
       }
       if(!$data) {
           return '';
       }
       if (preg_match('%[0-9,]+%i', $data) && $attribute->usesSource() && $attribute->getSource()->getOptionText($data)) {
            $data = $attribute->getSource()->getOptionText($data);
            if(is_array($data)) {
                $data = implode(', ', $data);
            }
       }
       return $this->formatData($data);
   }

   public function getProductPriceValue($product) {
       if($product->getTypeId() == 'grouped') {
           if($product->getMinimalPrice()) {
               return floatval($product->getMinimalPrice());
           }
            $_associatedProducts = $product->getTypeInstance(true)->getAssociatedProducts($product);
            $minPrice = PHP_INT_MAX;
            foreach($_associatedProducts as $_associatedProduct) {
                if($_associatedProduct->getPrice()) {
                    $minPrice = min($_associatedProduct->getPrice(),$minPrice);
                }
            }
            if($minPrice == PHP_INT_MAX) {
                $minPrice = 0;
            }
            return floatval($minPrice);
       }
       return floatval($product->getFinalPrice());
   }
           
   protected $_categoryName = array();
   public function getProductCategoryValue($product, $useCurrentCategory = true, $storeId = null) {
       if(!$product || !$product->getId()) {
           return '';
       }
       if(!$storeId) {
           $storeId = Mage::app()->getStore()->getId();
       }
       $categoryId = false;
       $sessionCache = Mage::getSingleton('core/session')->getGuaProductCategoryValue();
       if(!is_array($sessionCache)) {
            $sessionCache = array();
       }
       if(!isset($sessionCache[$product->getId().'-'.$storeId])) {
           $sessionCache[$product->getId().'-'.$storeId] = false;
            $lastCategoryViewed = Mage::getSingleton('catalog/session')->getLastViewedCategoryId();
            if($lastCategoryViewed && in_array($lastCategoryViewed, $product->getCategoryIds())) {
                $sessionCache[$product->getId().'-'.$storeId] = $lastCategoryViewed;
            } else if(Mage::helper('wcooall')->isModuleEnabled('Webcooking_MainCategory')) {
                $sessionCache[$product->getId().'-'.$storeId] = $product->getMainCategory();
            }
            if(!$sessionCache[$product->getId().'-'.$storeId]) {
                $category = Mage::registry('current_category');
                if($useCurrentCategory && $category && $category->getId()) {
                    $sessionCache[$product->getId().'-'.$storeId] = $category->getId();
                } else {
                    $rootCategoryId = Mage::app()->getStore($storeId)->getRootCategoryId();
                    $sessionCache[$product->getId().'-'.$storeId] = false;
                    /*$categoryIds = $product->getCategoryIds();
                    $categoryId = array_pop($categoryIds);*/
                    $categoryCollection = $product->getCategoryCollection();
                    $categoryCollection->setStoreId($storeId);
                    $categoryCollection->addAttributeToSelect('is_active', 'left');
                    $categoryCollection->addAttributeToSelect('path', 'left');
                    $categoryCollection->setOrder('entity_id', 'ASC');
                    foreach($categoryCollection as $category) {
                        $isActive = Mage::getResourceModel('catalog/category')->getAttributeRawValue($category->getId(), 'is_active', $storeId);
                       if($isActive && $category->getLevel() > 1 && preg_match('%^1/' . $rootCategoryId . '/%', $category->getPath())) {
                            $sessionCache[$product->getId().'-'.$storeId] = $category->getId();
                        }
                    }

                }
            }
       }
       $categoryId = $sessionCache[$product->getId().'-'.$storeId];
       Mage::getSingleton('core/session')->setGuaProductCategoryValue($sessionCache);
       
       
       if(!$categoryId) {
           return '';
       }
       
       // too long if many categories 
       // return $this->formatData(Mage::helper('wcooall/category')->getCategoryNameById($categoryId, '/', 5));
       if(!isset($this->_categoryName[$categoryId])) {
           $category = Mage::getModel('catalog/category')->load($categoryId);
           $this->_categoryName[$categoryId] = $category->getName();
           while($category->getLevel() > 2) {
               $category = $category->getParentCategory();
               $this->_categoryName[$categoryId] = $category->getName() . '/' . $this->_categoryName[$categoryId];
           }
       }
       
       return $this->formatData($this->_categoryName[$categoryId]);
   }

   public function formatData($data) {
       return Mage::helper('googleuniversalanalytics')->formatData($data);
   }

   public function getDefaultListName($useCategoryName = false) {
       $listName =  Mage::app()->getFrontController()->getAction()->getFullActionName();
       switch($listName) {
           case 'cms_index_index':
               $listName = $this->__('Homepage');
               break;
           case 'catalog_category_view':
               $category = Mage::registry('current_category');
               if($category && $category->getId() && $useCategoryName) {
                   $listName = $this->__('Category %s', $category->getName());
               } else {
                   $listName = $this->__('Category view');
               }
               break;
           case 'promotions_promotions_index':
               $listName = $this->__('Promotions');
               break;
           case 'solrsearch_result_index':
           case 'catalogsearch_result_index':
               $listName = $this->__('Search results');
               break;
           case 'advancedcms_page_view':
           case 'cms_page_view':
               $listName = $this->__('CMS page view');
               break;
           case 'nouveautes-new-index':
               $listName = $this->__('Recent products');
               break;
       }
       return $listName;
   }
   
   

   public function getAddPromoImpressionTag($id, $name='', $creative='', $position=false, $withScriptTags = true, $globalAccount = false) {
       if(!Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated()) {
           return '';
       }
       
       /*if(Mage::helper('googleuniversalanalytics/gtm')->isActive()) {
           return Mage::helper('googleuniversalanalytics/gtm')->getAddPromoImpressionTag($id, $name, $creative, $position, $withScriptTags, false);
       }*/
       
       if(!$position) {
           $position = $this->getPromoPosition();
       } else {
           $this->incrementPromoPosition();
       }
       /*if($globalAccount) {
            $trackerName = Mage::helper('googleuniversalanalytics')->getGlobalTrackerName();
        } else {
            $trackerName = Mage::helper('googleuniversalanalytics')->getTrackerName();
        }*/
       
       $html = '';
       if($withScriptTags) {
           $html .= '<script type="text/javascript">';
       }

       /*$html .= sprintf("
                    ga('{$trackerName}ec:addPromo', {
                        'id': '%s',
                        'name': '%s',
                        'creative': '%s',
                        'position': '%s'
                    });
                    ",
                    $this->formatData($id),
                    $this->formatData($name),
                    $this->formatData($creative),
                    $this->formatData($position)
                );*/
       
       $html .= sprintf("
                    promoImpressions.push({
                        'id': '%s',
                        'name': '%s',
                        'creative': '%s',
                        'position': '%s'
                    });
                    ",
                    $this->formatData($id),
                    $this->formatData($name),
                    $this->formatData($creative),
                    $this->formatData($position)
                );


       if($withScriptTags) {
           $html .= '</script>';
       }
       /*if(!$globalAccount && Mage::helper('googleuniversalanalytics')->isGoogleAnalyticsGlobalAccountAvailable()) {
            $html .= $this->getAddPromoImpressionTag($id, $name, $creative, $position, $withScriptTags, true);
        }*/
       return $html;

   }
   
   public function getImpressionEventTag($globalAccount = false) {
       if(Mage::helper('googleuniversalanalytics/gtm')->isActive()) {
           return '';
       }
       Mage::getSingleton('core/session')->setShouldSendImpressionEventTag(false);
       if($globalAccount) {
            $trackerName = Mage::helper('googleuniversalanalytics')->getGlobalTrackerName();
        } else {
            $trackerName = Mage::helper('googleuniversalanalytics')->getTrackerName();
        }
       $guaCode = "\nga('{$trackerName}send', 'event', { 'eventCategory': 'impression', 'eventAction': 'sent', 'useBeacon': true, 'nonInteraction': 1});\n";
       if(!$globalAccount && Mage::helper('googleuniversalanalytics')->isGoogleAnalyticsGlobalAccountAvailable()) {
            $guaCode .= $this->getImpressionEventTag(true);
       }
       return $guaCode;
   }
   
   public function getGlobalImpressionCount() {
        return Mage::registry('gua_ec_impression_count')?Mage::registry('gua_ec_impression_count'):0;
   }
   public function getProductPosition($listName, $incrementFirst = true) {
       if($incrementFirst) {
           $this->incrementProductPosition($listName);
       }
       $count = Mage::registry('gua_ec_product_impression_count');
       if($count && isset($count[$listName])) {
           return $count[$listName];
       }
       return 0;
   }
   public function getPromoPosition($incrementFirst = true) {
       if($incrementFirst) {
           $this->incrementPromoPosition();
       }
       return Mage::registry('gua_ec_promo_impression_count')?Mage::registry('gua_ec_promo_impression_count'):0;
   }
   
   public function incrementProductPosition($listName) {
       Mage::getSingleton('core/session')->setShouldSendImpressionEventTag(true);
       $globalCount = (int)Mage::registry('gua_ec_impression_count');
       $count = Mage::registry('gua_ec_product_impression_count');
       if(!is_array($count)) {
           $count = array();
       }
       if(!isset($count[$listName])) {
           $count[$listName] = 0;
       }
       $count[$listName]++;
       $globalCount++;
       Mage::unregister('gua_ec_impression_count');
       Mage::unregister('gua_ec_product_impression_count');
       Mage::register('gua_ec_impression_count', $globalCount);
       Mage::register('gua_ec_product_impression_count', $count);
   }
   public function incrementPromoPosition() {
       Mage::getSingleton('core/session')->setShouldSendImpressionEventTag(true);
       $globalCount = (int)Mage::registry('gua_ec_impression_count');
       $count = (int)Mage::registry('gua_ec_promo_impression_count');
       Mage::unregister('gua_ec_impression_count');
       Mage::unregister('gua_ec_promo_impression_count');
       Mage::register('gua_ec_impression_count', $globalCount+1);
       Mage::register('gua_ec_promo_impression_count', $count+1);
   }

   public function getAddProductImpressionTag($product, $listName=false, $position = false, $withScriptTags = true, $globalAccount = false) {
       if(!Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated() || !$product || !$product->getId()) {
           return '';
       }
       
       if(!$position) {
           $position = $this->getProductPosition($listName);
       } else {
           $this->incrementProductPosition($listName);
       }
     
       
       $html = '';
       
       
       if($withScriptTags) {
           $html .= '<script type="text/javascript">';
       }
       
       

       //$currency = Mage::app()->getStore()->getCurrentCurrencyCode();
       $brandValue = $this->getProductBrandValue($product);
       $nameValue = $this->getProductNameValue($product);
       $variantValue = $this->getProductVariantValue($product);
       $skuValue = $this->getProductSkuValue($product);
       $categoryValue = $this->getProductCategoryValue($product);
       $priceValue = $this->getProductPriceValue($product);

       if(!$listName) {
           $listName = $this->getDefaultListName();
       }

       
        /*if(Mage::getStoreConfigFlag('googleuniversalanalytics/enhanced_ecommerce/export_products_via_api')) {
            $html .= sprintf("
                       ga('{$trackerName}ec:addImpression', {
                           'id': '%s',
                           'list': '%s',
                           'position': '%s'
                       });
                       ",
                       $skuValue,
                       $this->formatData($listName),
                       intval($position)
                   );
        } else {
             $html .= sprintf("
                       ga('{$trackerName}ec:addImpression', {
                           'id': '%s',
                           'name': '%s',
                           'category': '%s',
                           'brand': '%s',
                           'variant': '%s',
                           'price': '%s',
                           'list': '%s',
                           'position': '%s'
                       });
                       ",
                       $skuValue,
                       $nameValue,
                       $categoryValue,
                       $brandValue,
                       $variantValue,
                       $product->getFinalPrice(),
                       $this->formatData($listName),
                       intval($position)
                   );
        }*/
       $html .= sprintf("
           productImpressions.push({
                'id': '%s',
                'name': '%s',
                'category': '%s',
                'brand': '%s',
                'variant': '%s',
                'price': '%s',
                'list': '%s',
                'position': %s
           });
            ",
            $skuValue,
            $nameValue,
            $categoryValue,
            $brandValue,
            $variantValue,
            $priceValue,
            $this->formatData($listName),
            intval($position)
        );


       if($withScriptTags) {
           $html .= '</script>';
       }
       
       $this->saveLastListName($listName);
       
       return $html;
   }
   
   public function saveLastListName($listName) {
       Mage::getSingleton('core/session')->setLastListName($listName);
   }
   
   public function getAddProductDetailsTag($product, $withScriptTags = false, $globalAccount=false) {
       if(!Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated()) {
            return '';
        }
        if(!$product) {
            return '';
        }
        if(Mage::helper('googleuniversalanalytics/gtm')->isActive()) {
            return Mage::helper('googleuniversalanalytics/gtm')->getAddProductDetailsTag($product, $withScriptTags, false);
        }
        if($globalAccount) {
            $trackerName = Mage::helper('googleuniversalanalytics')->getGlobalTrackerName();
        } else {
            $trackerName = Mage::helper('googleuniversalanalytics')->getTrackerName();
        }
        
        $html = '';
        
        if($withScriptTags) {
            $html = '<script>';
        } 
        
       
        if($product->getTypeId() == 'configurable' && Mage::getStoreConfigFlag('googleuniversalanalytics/enhanced_ecommerce/track_variants')) {
             $html .= 'Product.Config.prototype.origConfigureGUA = Product.Config.prototype.configure;
                    Product.Config.prototype.configure = function(event){               
                        this.origConfigureGUA(event);
                        sendGuaProductVariantDetails(this);
                    };';
         }
         $html .= sprintf("
             var productDetail = {
                 'id': '%s',
                 'name': '%s',
                 'category': '%s',
                 'brand': '%s',
                 'price': '%s',
                 'variant': '%s'
             };
             ga('{$trackerName}ec:addProduct', productDetail);
             ga('{$trackerName}ec:setAction', 'detail');
             ",
             $this->getProductSkuValue($product),
             $this->getProductNameValue($product),
             $this->getProductCategoryValue($product), 
             $this->getProductBrandValue($product), 
             $this->getProductPriceValue($product),
             ''
         );

        
        if($withScriptTags) {
           $html .= '</script>'; 
        }
        
        if(!$globalAccount && Mage::helper('googleuniversalanalytics')->isGoogleAnalyticsGlobalAccountAvailable()) {
            $html .= $this->getAddProductDetailsTag($product, $withScriptTags, true);
        }
        
        return $html;
   }



   public function getProductBrandValue($product, $storeId = null) {
       $brandAttributeCode = $this->getBrandAttributeCode();
       $brandValue = $this->formatData($this->getProductAttributeValue($product, $brandAttributeCode, $storeId));
        
       $brandObj = new Varien_Object(array('value' => $brandValue));
       Mage::dispatchEvent('gua_get_product_brand_value', array('brand' => $brandObj, 'product'=>$product));
       $brandValue = $brandObj->getValue();
       
       return $brandValue;
   }

   public function getProductNameValue($product, $storeId = null) {
       $nameValue = $this->getProductAttributeValue($product, 'name', $storeId);
       return $nameValue;
   }

   public function getProductVariantValue($product, $orderItem = null) {
       if (!$product) {
            return '';
        }
        try {
            if ($orderItem && get_class($orderItem) == 'Mage_Sales_Model_Order_Item') {
                $options = array();
                if ($productOptions = $orderItem->getProductOptions()) {
                    if (isset($productOptions['options'])) {
                        $options = array_merge($options, $productOptions['options']);
                    }
                    if (isset($productOptions['additional_options'])) {
                        $options = array_merge($options, $productOptions['additional_options']);
                    }
                    if (!empty($productOptions['attributes_info'])) {
                        $options = array_merge($productOptions['attributes_info'], $options);
                    }
                }
            } else {
                $options = array();
                if( $product->getTypeId() == 'configurable' ) {
                    $options = $product->getTypeInstance(true)->getSelectedAttributesInfo($product);
                }
                //$productOptions = $product->getTypeInstance(true)->getOrderOptions($product);
                $typeInstance = new Webcooking_GoogleUniversalAnalytics_Model_Catalog_Product_Type_Configurable();
                $productOptions = $typeInstance->setProduct($product)->getOrderOptions($product);
                if (isset($productOptions['options'])) {
                    $options = array_merge($options, $productOptions['options']);
                }
            }
            
            
            
        } catch (Exception $e) {
            return '';
        }
        $variant = array();
        foreach ($options as $option) {
            $variant[] = $option['value'];
        }
        return $this->formatData(implode(' - ', $variant));
    }

   public function getProductSkuValue($product) {
       $skuValue = $this->getProductAttributeValue($product, 'sku');
       return $skuValue;
   }

   public function getProductLinkData($product, $listName=false, $position = false, $noredirect=false) {
       if(!Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated() || !$product || !$product->getId()) {
           return '';
       }
       
       $type = 'gua';
       if(Mage::helper('googleuniversalanalytics/gtm')->isActive()) {
           $type = 'gtm';
       }
       
       $html = ' ';

       $brandValue = $this->getProductBrandValue($product);
       $nameValue = $this->getProductNameValue($product);
       $variantValue = $this->getProductVariantValue($product);
       $skuValue = $this->getProductSkuValue($product);
       $categoryValue = $this->getProductCategoryValue($product);
       $priceValue = $this->getProductPriceValue($product);

       if(!$listName) {
           $listName = $this->getDefaultListName();
       }

       $html .= 'data-'.$type.'-ec-id="' . $skuValue . '" ';
       $html .= 'data-'.$type.'-ec-name="' . $nameValue . '" ';
       $html .= 'data-'.$type.'-ec-category="' . $categoryValue . '" ';
       $html .= 'data-'.$type.'-ec-brand="' . $brandValue . '" ';
       $html .= 'data-'.$type.'-ec-variant="' . $variantValue . '" ';
       $html .= 'data-'.$type.'-ec-list="' . $this->formatData($listName) . '" ';
       $html .= 'data-'.$type.'-ec-price="' . $priceValue . '" ';
       $html .= 'data-'.$type.'-ec-position="' . intval($position) . '" ';
       if($noredirect) {
            $html .= 'data-'.$type.'-ec-noredirect="1" ';
       }

       $this->saveLastListName($listName);
       return $html;
   }



   public function getPromoLinkData($id, $name='', $creative='', $position=false, $noredirect=false) {
       if(!Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated() ) {
           return '';
       }
       $html = ' ';

       $type = 'gua';
       if(Mage::helper('googleuniversalanalytics/gtm')->isActive()) {
           $type = 'gtm';
       }
       
       $html .= 'data-'.$type.'-ec-promo-id="' . $this->formatData($id) . '" ';
       $html .= 'data-'.$type.'-ec-promo-name="' . $this->formatData($name) . '" ';
       $html .= 'data-'.$type.'-ec-promo-creative="' . $this->formatData($creative) . '" ';
       $html .= 'data-'.$type.'-ec-promo-position="' . $this->formatData($position) . '" ';
       if($noredirect) {
            $html .= 'data-'.$type.'-ec-promo-noredirect="1" ';
       }


       return $html;
   }

   public function getEnhancedOrdersTrackingCode($orderIds, $globalAccount = false) {
        if(Mage::getStoreConfig('googleuniversalanalytics/transactions/use_mp')) {
            return '';
        }
        if (empty($orderIds) || !is_array($orderIds)) {
            return '';
        }
        if(Mage::helper('googleuniversalanalytics/gtm')->isActive()) {
            return Mage::helper('googleuniversalanalytics/gtm')->getEnhancedOrdersTrackingCode($orderIds, false);
        }
        if($globalAccount) {
            $trackerName = Mage::helper('googleuniversalanalytics')->getGlobalTrackerName();
        } else {
            $trackerName = Mage::helper('googleuniversalanalytics')->getTrackerName();
        }
        $collection = Mage::getResourceModel('sales/order_collection')
            ->addFieldToFilter('entity_id', array('in' => $orderIds))
        ;
        $result = array("ga('{$trackerName}require', 'ecommerce', 'ecommerce.js');");
        foreach ($collection as $order) {
            if ($order->getIsVirtual()) {
                $address = $order->getBillingAddress();
            } else {
                $address = $order->getShippingAddress();
            }


            foreach ($order->getAllVisibleItems() as $item) {
                $result[] = sprintf("
                    ga('{$trackerName}ec:addProduct', {
                        'id': '%s',
                        'name': '%s',
                        'category': '%s',
                        'brand': '%s',
                        'variant': '%s',
                        'price': '%s',
                        'quantity': %s
                    });
                    ",
                    $this->jsQuoteEscape($item->getSku()),
                    $this->jsQuoteEscape($item->getName()),
                    $this->jsQuoteEscape($this->getProductCategoryValue($item->getProduct())),
                    $this->jsQuoteEscape($this->getProductBrandValue($item->getProduct())),
                    $this->jsQuoteEscape($this->getProductVariantValue($item->getProduct(), $item)),
                    Mage::helper('googleuniversalanalytics/ecommerce')->getItemPriceForOrder($item),
                    round($item->getQtyOrdered())
                );
            }


               $result[] = sprintf("
                 ga('{$trackerName}ec:setAction', 'purchase', {
                    'id': '%s',
                    'affiliation': '%s',
                    'revenue': '%s',
                    'tax': '%s',
                    'shipping': '%s',
                    'coupon': '%s'
                  });
                 ",
                $order->getIncrementId(),
                null,
                Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionRevenueForOrder($order),
                Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionTaxForOrder($order),
                Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionShippingForOrder($order),
                $order->getCouponCode()
            );


        }
        $guaCode =  implode("\n", $result);
        if(!$globalAccount && Mage::helper('googleuniversalanalytics')->isGoogleAnalyticsGlobalAccountAvailable()) {
            $guaCode .= $this->getEnhancedOrdersTrackingCode($orderIds, true);
        }
        return $guaCode;
    }
    
    
    public function getTransactionRevenueForOrder($order) {
        $storeId = $order->getStoreId();
        $useStoreCurrency = $this->useStoreCurrency($storeId);
        $grandTotal = $useStoreCurrency ? $order->getGrandTotal() : $order->getBaseGrandTotal();
        $taxAmount = $useStoreCurrency ? $order->getTaxAmount() : $order->getBaseTaxAmount();
        $shippingAmount = $useStoreCurrency ? $order->getShippingAmount() : $order->getBaseShippingAmount();
        if(Mage::getStoreConfigFlag('googleuniversalanalytics/transactions/transaction_revenue_inc_tax', $storeId) && Mage::getStoreConfigFlag('googleuniversalanalytics/transactions/transaction_revenue_inc_shipping', $storeId)) {
            return $grandTotal;
        } else if(Mage::getStoreConfigFlag('googleuniversalanalytics/transactions/transaction_revenue_inc_shipping', $storeId)) {
            return $grandTotal - $taxAmount;
        } else if(Mage::getStoreConfigFlag('googleuniversalanalytics/transactions/transaction_revenue_inc_tax', $storeId)) {
            return $grandTotal - $shippingAmount;
        } 
        return $grandTotal - $taxAmount - $shippingAmount;
    }
    
    
    public function getTransactionRevenueForInvoice($invoice) {
        $storeId = $invoice->getOrder()->getStoreId();
        $useStoreCurrency = $this->useStoreCurrency($storeId);
        $grandTotal = $useStoreCurrency ? $invoice->getGrandTotal() : $invoice->getBaseGrandTotal();
        $taxAmount = $useStoreCurrency ? $invoice->getTaxAmount() : $invoice->getBaseTaxAmount();
        $shippingAmount = $useStoreCurrency ? $invoice->getShippingAmount() : $invoice->getBaseShippingAmount();
        if(Mage::getStoreConfigFlag('googleuniversalanalytics/transactions/transaction_revenue_inc_tax', $storeId) && Mage::getStoreConfigFlag('googleuniversalanalytics/transactions/transaction_revenue_inc_shipping', $storeId)) {
            return $grandTotal;
        } else if(Mage::getStoreConfigFlag('googleuniversalanalytics/transactions/transaction_revenue_inc_shipping', $storeId)) {
            return $grandTotal - $taxAmount;
        } else if(Mage::getStoreConfigFlag('googleuniversalanalytics/transactions/transaction_revenue_inc_tax', $storeId)) {
            return $grandTotal - $shippingAmount;
        } 
        return $grandTotal - $taxAmount - $shippingAmount;
    }
    
    public function getTransactionShippingForOrder($order) {
        $storeId = $order->getStoreId();
        $useStoreCurrency = $this->useStoreCurrency($storeId);
        $shippingAmount = $useStoreCurrency ? $order->getShippingAmount() : $order->getBaseShippingAmount();
        $shippingTaxAmount = $useStoreCurrency ? $order->getShippingTaxAmount() : $order->getBaseShippingTaxAmount();
        if(Mage::getStoreConfigFlag('googleuniversalanalytics/transactions/transaction_shipping_inc_tax', $storeId)) {
           return $shippingAmount + $shippingTaxAmount;
        } 
        return $shippingAmount;
    }
    
    public function getTransactionShippingForInvoice($invoice) {
        $storeId = $invoice->getOrder()->getStoreId();
        $useStoreCurrency = $this->useStoreCurrency($storeId);
        $shippingAmount = $useStoreCurrency ? $invoice->getShippingAmount() : $invoice->getBaseShippingAmount();
        $shippingTaxAmount = $useStoreCurrency ? $invoice->getShippingTaxAmount() : $invoice->getBaseShippingTaxAmount();
        if(Mage::getStoreConfigFlag('googleuniversalanalytics/transactions/transaction_shipping_inc_tax', $storeId)) {
           return $shippingAmount + $shippingTaxAmount;
        } 
        return $shippingAmount;
    }
    
    public function getTransactionTaxForOrder($order) {
        $storeId = $order->getStoreId();
        $useStoreCurrency = $this->useStoreCurrency($storeId);
        $taxAmount = $useStoreCurrency ? $order->getTaxAmount() : $order->getBaseTaxAmount();
        $shippingTaxAmount = $useStoreCurrency ? $order->getShippingTaxAmount() : $order->getBaseShippingTaxAmount();
        if(Mage::getStoreConfigFlag('googleuniversalanalytics/transactions/transaction_tax_inc_shipping_tax', $storeId)) {
            return $taxAmount;
        } 
        return $taxAmount - $shippingTaxAmount;
    }
    
    public function getTransactionTaxForInvoice($invoice) {
        $storeId = $invoice->getOrder()->getStoreId();
        $useStoreCurrency = $this->useStoreCurrency($storeId);
        $taxAmount = $useStoreCurrency ? $invoice->getTaxAmount() : $invoice->getBaseTaxAmount();
        $shippingTaxAmount = $useStoreCurrency ? $invoice->getShippingTaxAmount() : $invoice->getBaseShippingTaxAmount();
        if(Mage::getStoreConfigFlag('googleuniversalanalytics/transactions/transaction_tax_inc_shipping_tax', $storeId)) {
            return $taxAmount;
        } 
        return $taxAmount - $shippingTaxAmount;
    }
    
    public function getItemPriceForQuote($quoteItem) {
        $quote = $quoteItem->getQuote();
        $storeId = null;
        if($quote) {
            $storeId = $quote->getStoreId();
        }
        $useStoreCurrency = $this->useStoreCurrency($storeId);
        $priceInclTax = $useStoreCurrency ? $quoteItem->getPriceInclTax() : $quoteItem->getBasePriceInclTax();
        $price = $useStoreCurrency ? $quoteItem->getPrice() : $quoteItem->getBasePrice();
        if(Mage::getStoreConfigFlag('googleuniversalanalytics/transactions/transaction_item_price_inc_tax', $storeId)) {
            return $priceInclTax?$priceInclTax:$price;
        } 
        return $price;
    }
    
    public function getItemPriceForOrder($orderItem) {
        $order = $orderItem->getOrder();
        $storeId = null;
        if($order) {
            $storeId = $order->getStoreId();
        }
        $useStoreCurrency = $this->useStoreCurrency($storeId);
        $priceInclTax = $useStoreCurrency ? $orderItem->getPriceInclTax() - $orderItem->getDiscountAmount() : $orderItem->getBasePriceInclTax() - $orderItem->getBaseDiscountAmount();
        $price = $useStoreCurrency ? $priceInclTax - round($orderItem->getTaxAmount()/$orderItem->getQtyOrdered(), 2) : $priceInclTax - round($orderItem->getBaseTaxAmount()/$orderItem->getQtyOrdered(), 2);
        if(Mage::getStoreConfigFlag('googleuniversalanalytics/transactions/transaction_item_price_inc_tax', $storeId)) {
            return $priceInclTax > 0?$priceInclTax:$price;
        } 
        return $price;
    }
    
    public function getItemPriceForInvoice($invoiceItem) {
        $storeId = $invoiceItem->getInvoice()->getOrder()->getStoreId();
        $useStoreCurrency = $this->useStoreCurrency($storeId);
        $priceInclTax = $useStoreCurrency ? $invoiceItem->getPriceInclTax() - $invoiceItem->getDiscountAmount() : $invoiceItem->getBasePriceInclTax()- $invoiceItem->getBaseDiscountAmount();
        $price = $useStoreCurrency ? $priceInclTax - round($invoiceItem->getTaxAmount()/$invoiceItem->getQty(), 2) : $priceInclTax - round($invoiceItem->getBaseTaxAmount()/$invoiceItem->getQty(), 2);
        if(Mage::getStoreConfigFlag('googleuniversalanalytics/transactions/transaction_item_price_inc_tax', $storeId)) {
            return $priceInclTax > 0?$priceInclTax:$price;
        } 
        return $price;
    }

    public function getTransactionAffiliation($order) {
        if($order->getRemoteIp()) {
            return $order->getStore()->getName();
        }
        //admin order
        return Mage::helper('googleuniversalanalytics')->__('Admin order (%s)', $order->getStore()->getName());
        
    }
    
}

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.web-cooking.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_GoogleUniversalAnalytics
 * @copyright  Copyright (c) 2011-2016 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
class Webcooking_GoogleUniversalAnalytics_Helper_Gtm extends Webcooking_GoogleUniversalAnalytics_Helper_Ecommerce {
 
    public function isActive($store = null) {
        return Mage::getStoreConfig('googleuniversalanalytics/gtm/active', $store);
    }
    
    public function shouldAddContainer($store = null) {
        return Mage::getStoreConfig('googleuniversalanalytics/gtm/add_tag', $store) && $this->getAccountId($store);
    }
    
     public function getAccountId($store = null) {
        return Mage::getStoreConfig('googleuniversalanalytics/gtm/account', $store);
    }
    
    public function getAddProductDetailsTag($product, $withScriptTags = false, $globalAccount=false) {
        $html = '';

        if ($withScriptTags) {
            $html = '<script>';
        }

        $html .= sprintf("
                        var productDetail = {
                             'id': '%s',
                             'name': '%s',
                             'category': '%s',
                             'brand': '%s',
                             'price': '%s',
                             'variant': '%s'
                         };
                         dataLayer.push({
                            'ecommerce': {
                                'currencyCode': '%s',
                                'detail': {
                                  'products': [productDetail]
                                 }
                                }
                          });
                         ", 
                $this->getProductSkuValue($product), 
                $this->getProductNameValue($product), 
                $this->getProductCategoryValue($product), 
                $this->getProductBrandValue($product), 
                $this->getProductPriceValue($product), 
                $this->getProductVariantValue($product),
                Mage::app()->getStore()->getCurrentCurrencyCode()
        );


        if ($withScriptTags) {
            $html .= '</script>';
        }
        
        return $html;
   }
   
   public function getTimeOnPageEventCode() {
        $guaCode = '';
        
        $timeOnPageDelays =  Mage::helper('googleuniversalanalytics')->getTimeOnPageDelays();
        foreach($timeOnPageDelays as $delay) {
            $delayMs = $delay * 1000;
            $guaCode .= "setTimeout(function(){   
                            dataLayer.push({
                                'event':'GAevent',
                                'eventCategory':'timeOnPage',
                                'eventAction':'{$delay} seconds',
                                'eventNoInteraction':true 
                            });       
                        }, {$delayMs});    " . "\n";
        }
        
        return $guaCode;
   }
           
   
   public function getCustomDimensionsCode() {
       $customDimensions = Mage::helper('googleuniversalanalytics')->getCustomDimensions();
        
        if(empty($customDimensions)) {
            return '';
        }

        $guaCode = 'dataLayer.push({' . "\n";

        foreach($customDimensions as $customDimensionIndex => $customDimensionValue) {
            $customDimensionValue = str_replace("'", "\'", $customDimensionValue);
            $guaCode .= "'{$customDimensionIndex}': '{$customDimensionValue}'," . "\n";
        }
        $guaCode .= '});';
        return $guaCode;
   }
   
   public function getOrdersTrackingCode($orderIds, $globalAccount = false)
    {
        if (empty($orderIds) || !is_array($orderIds)) {
            return '';
        }
        $collection = Mage::getResourceModel('sales/order_collection')
            ->addFieldToFilter('entity_id', array('in' => $orderIds))
        ;
        foreach ($collection as $order) {
            if ($order->getIsVirtual()) {
                $address = $order->getBillingAddress();
            } else {
                $address = $order->getShippingAddress();
            }
            $items = array();
            foreach ($order->getAllVisibleItems() as $item) {
                $items[] = sprintf("
                    { 
                    'id': '%S', 
                    'name': '%S', 
                    'sku': '%s', 
                    'category': '%s', 
                    'price': '%s', 
                    'quantity': '%s' 
                    }
                    ",
                    $order->getIncrementId(),
                    $this->jsQuoteEscape($item->getName()),
                    $this->jsQuoteEscape($item->getSku()), 
                    null, 
                    Mage::helper('googleuniversalanalytics/ecommerce')->getItemPriceForOrder($item), 
                    round($item->getQtyOrdered())
                );
            }
            
            
               $result[] = sprintf("
                   dataLayer = [{ 
                        'transactionId': '%s',  
                        'transactionAffiliation': '%s', 
                        'transactionTotal': '%s' 
                        'transactionShipping': '%s', 
                        'transactionTax': '%s', 
                        'transactionProducts': [%s] 
                      }]; 
                 ",
                $order->getIncrementId(),
                null,
                Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionRevenueForOrder($order),
                Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionShippingForOrder($order),
                Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionTaxForOrder($order),
                implode(',',$items)
            );
            
        }
        $guaCode = implode("\n", $result);
        
        return $guaCode;
    }
    
    
    public function getEnhancedOrdersTrackingCode($orderIds, $globalAccount = false)
    {
        $collection = Mage::getResourceModel('sales/order_collection')
            ->addFieldToFilter('entity_id', array('in' => $orderIds))
        ;
       
        foreach ($collection as $order) {
            if ($order->getIsVirtual()) {
                $address = $order->getBillingAddress();
            } else {
                $address = $order->getShippingAddress();
            }
            
        
            $items = array();
            foreach ($order->getAllVisibleItems() as $item) {
                $items[] = sprintf("
                    {
                    'id': '%s',
                    'name': '%s',
                    'category': '%s',
                    'brand': '%s',
                    'variant': '%s',
                    'price': '%s',
                    'quantity': '%s'
                   }
                    ",
                    $this->jsQuoteEscape($item->getSku()),
                    $this->jsQuoteEscape($item->getName()),
                    $this->jsQuoteEscape($this->getProductCategoryValue($item->getProduct())),
                    $this->jsQuoteEscape($this->getProductBrandValue($item->getProduct())),
                    $this->jsQuoteEscape($this->getProductVariantValue($item->getProduct(), $item)),
                    Mage::helper('googleuniversalanalytics/ecommerce')->getItemPriceForOrder($item),
                    round($item->getQtyOrdered())
                );
            }


               $result[] = sprintf("
                    dataLayer.push({
                        'ecommerce': {
                          'purchase': {
                            'actionField': {
                              'id': '%s',                         
                              'affiliation': '%s',
                              'revenue': '%s',                
                              'tax':'%s',
                              'shipping': '%s',
                              'coupon': '%s'
                            },
                            'products': [%s]
                          }
                        }
                    });
                 ",
                $order->getIncrementId(),
                null,
                Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionRevenueForOrder($order),
                Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionTaxForOrder($order),
                Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionShippingForOrder($order),
                $order->getCouponCode(),
                implode(',', $items)
            );


        }
        $guaCode =  implode("\n", $result);
        
        return $guaCode;
    }
    
    
    /**
     * 
     * @deprecated
     */
    public function getAddProductImpressionTag($product, $listName=false, $position = false, $withScriptTags = true, $globalAccount = false) {
       if(!$position) {
           $position = $this->getProductPosition($listName);
       } else {
           $this->incrementProductPosition($listName);
       }
       
       $html = '';
       
       
       if($withScriptTags) {
           $html .= '<script type="text/javascript">';
       }
       
       

       $currency = Mage::app()->getStore()->getCurrentCurrencyCode();
       $brandValue = $this->getProductBrandValue($product);
       $nameValue = $this->getProductNameValue($product);
       $variantValue = $this->getProductVariantValue($product);
       $skuValue = $this->getProductSkuValue($product);
       $categoryValue = $this->getProductCategoryValue($product);
       $priceValue = $this->getProductPriceValue($product);

       if(!$listName) {
           $listName = $this->getDefaultListName();
       }

       
        $html .= sprintf("
                     dataLayer.push({
                        'ecommerce': {
                          'currencyCode': '%s',                       
                          'impressions': [
                           {    
                             'id': '%s',
                             'name': '%s', 
                             'category': '%s',
                             'brand': '%s',
                             'variant': '%s',
                             'price': '%s',
                             'list': '%s',
                             'position': %s
                           }
                           ]
                        }
                      });
                     ",
                     $currency,
                     $skuValue,
                     $nameValue,
                     $categoryValue,
                     $brandValue,
                     $variantValue,
                     $priceValue,
                     $this->formatData($listName),
                     intval($position)
                 );
       


       if($withScriptTags) {
           $html .= '</script>';
       }
       
       
       return $html;
   }
   
   
   /**
     * 
     * @deprecated
     */
   public function getAddPromoImpressionTag($id, $name='', $creative='', $position=false, $withScriptTags = true, $globalAccount = false) {
     
       if(!$position) {
           $position = $this->getPromoPosition();
       } else {
           $this->incrementPromoPosition();
       }
       if($globalAccount) {
            $trackerName = Mage::helper('googleuniversalanalytics')->getGlobalTrackerName();
        } else {
            $trackerName = Mage::helper('googleuniversalanalytics')->getTrackerName();
        }
       
       $html = '';
       if($withScriptTags) {
           $html .= '<script type="text/javascript">';
       }

       $html .= sprintf("
                    dataLayer.push({
                        'ecommerce': {
                          'promoView' : {
                            'promotions': [
                             {    
                               'id': '%s',
                               'name': '%s', 
                               'creative': '%s',
                               'position': '%s',
                             }
                             ]
                           }
                        }
                      });
                    ",
                    $this->formatData($id),
                    $this->formatData($name),
                    $this->formatData($creative),
                    $this->formatData($position)
                );


       if($withScriptTags) {
           $html .= '</script>';
       }
       return $html;

   }
    
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Checkout
 * @copyright   Copyright (c) 2010 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Checkout observer model
 *
 * @category   Mage
 * @package    Mage_Checkout
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Devinc_Dailydeal_Model_Observer
{		
    /**
     * Product qty's checked
     * data is valid if you check quote item qty and use singleton instance
     *
     * @var array
     */
    protected $_checkedQuoteItems = array();
    protected $_dealProductIds;
    
	//delete deal if product is deleted from catalog
	public function deleteDeal($observer)
    {
		$productId = $observer->getEvent()->getProduct()->getId();		
		$dealCollection = Mage::getModel('dailydeal/dailydeal')->getCollection()->addFieldToFilter('product_id', $productId);		
		
		if (count($dealCollection)>0) {
			foreach ($dealCollection as $deal) {
				$deal->delete();
			}
		}
	}
	
	//refresh deals and redirect the homepage to the deal page
	public function refreshDealsHomepageRedirect($observer)
    {		
		$helper = Mage::helper('dailydeal');
    
		//refresh deals
		if ($helper->isEnabled() && Mage::getStoreConfig('dailydeal/configuration/refresh_rate')!=0 && $this->_requiresRefresh()) {
			Mage::getModel('dailydeal/dailydeal')->refreshDeals(); 
		}
    
		//redirect to main deal if option enabled and if on homepage
		$url = Mage::helper('core/url')->getCurrentUrl();
		$baseUrl = Mage::getBaseUrl();
		$baseUrlNoIndex = str_replace('index.php/', '', Mage::getBaseUrl());
		$path = str_replace($baseUrlNoIndex, '', str_replace($baseUrl, '', $url));	
		if ($path == '' || $path == '/' || $path == '/index.php' || $path == '/index.php/' || $path == '/home' || $path == '/home/') {	
	        $storeId = Mage::app()->getStore()->getId();
			$redirectEnabled = Mage::getStoreConfig('dailydeal/configuration/redirect', $storeId);
			
			if ($helper->isEnabled() && $redirectEnabled) {   
				$mainDeal = Mage::helper('dailydeal')->getDeal();				
				if ($mainDeal) {
					$response = $observer->getEvent()->getResponse();
					$product = Mage::getModel('catalog/product')->load($mainDeal->getProductId());				
					$product->setDoNotUseCategoryId(true);
					$response->setRedirect($product->getProductUrl());
				}
			}
		}
	}
	
	protected function _requiresRefresh() {
		$resource = Mage::getSingleton('core/resource');
	    $connection = $resource->getConnection('core_read');

	    $select = $connection->select()
	    ->from($resource->getTableName('core_config_data'))
	    ->where('scope = ?', 'default')
	    ->where('scope_id = ?', 0)
	    ->where('path = ?', 'dailydeal/refresh');

		$rows = $connection->fetchAll($select); 
		
		if (count($rows)>0) {
			$refreshDateTime = trim($rows[0]['value']);
			if ($this->_getCurrentDateTime()>$refreshDateTime) {
			    $this->_saveRefresh();
			    return true;
			} else {
			    return false;
			}
		} else {
			$this->_saveRefresh();
			return true;
		}
	}
	
	protected function _getCurrentDateTime($_storeId = null, $_format = 'Y-m-d H:i:s') {
		if (is_null($_storeId)) {
			$_storeId = Mage::app()->getStore()->getId();
		}
		$storeDatetime = new DateTime();
		$storeDatetime->setTimezone(new DateTimeZone(Mage::getStoreConfig('general/locale/timezone', $_storeId)));	
		
		return $storeDatetime->format($_format);
	}
	
	protected function _saveRefresh() {
		$nextVerificationDate = date('Y-m-d H:i:s', strtotime($this->_getCurrentDateTime().'+'.Mage::getStoreConfig('dailydeal/configuration/refresh_rate').' minutes'));
		Mage::getModel('core/config')->saveConfig('dailydeal/refresh', $nextVerificationDate, 'default', 0);	
	}
    
	//runs at checkout pages. checks to see if the deals have reached their maximum qty
	public function reviewCartItem($observer) {	
		if (Mage::helper('dailydeal')->isEnabled()) {	
			$item = $observer->getEvent()->getItem();	
			$quote = $item->getQuote();
			$product = Mage::getModel('catalog/product')->load($item->getProductId());	
			$helper = Mage::helper('dailydeal');
			$deal = $helper->getDealByProduct($product);
			
			if ($deal) {	
				$checkQtyForType = array('simple', 'virtual', 'downloadable');	
				$totalQty = $this->_getQuoteItemQtyForCheck($product->getId(), $item->getId(), $item->getQty());     
			    $maxQty = $deal->getDealQty();
			    
			    if (in_array($product->getTypeId(), $checkQtyForType) && $maxQty<$totalQty) {
			    	$message = $helper->__('The maximum order qty available for the "%s" DEAL is %s.', $product->getName(), $maxQty);
                	$item->setHasError(true);
                	$item->setMessage($message);
			    	$quote->setHasError(true);
			    	$quote->addMessage($message);
			    }     			   						
			} 	
		}
	}
	
	//retrieves the total product qty from the cart; also taking into account the qty of associated products or custom options from the cart
    protected function _getQuoteItemQtyForCheck($productId, $quoteItemId, $itemQty)
    {
        $qty = $itemQty;
        if (isset($this->_checkedQuoteItems[$productId]['qty']) &&
            !in_array($quoteItemId, $this->_checkedQuoteItems[$productId]['items'])) {
                $qty += $this->_checkedQuoteItems[$productId]['qty'];
        }
		
        $this->_checkedQuoteItems[$productId]['qty'] = $qty;
        $this->_checkedQuoteItems[$productId]['items'][] = $quoteItemId;

        return $qty;
    }
	
	//updates the deals available qty and it's sold qty
	public function updateDealQty($observer)
    {
		$items = $observer->getEvent()->getOrder()->getItemsCollection();		
		foreach ($items as $item) {			
			$helper = Mage::helper('dailydeal');	
			$product = Mage::getModel('catalog/product')->load($item->getProductId());
			$deal = $helper->getDealByProduct($product);
			$checkQtyForType = array('simple', 'virtual', 'downloadable', 'configurable');	
			
			if ($helper->isEnabled() && $deal && in_array($product->getTypeId(), $checkQtyForType)) {	
				$newQty = $deal->getDealQty()-$item->getQtyOrdered();
				$newSoldQty = $deal->getQtySold()+$item->getQtyOrdered();				
				
				$deal->setDealQty($newQty)->setQtySold($newSoldQty)->save();	
				Mage::getModel('dailydeal/dailydeal')->refreshDeal($deal);
			}
		}
	}
	
	//sets the deal price to the product
	public function getFinalPrice($observer)
    {
    	$product = $observer->getEvent()->getProduct();
    	$qty = $observer->getEvent()->getQty();
		$helper = Mage::helper('dailydeal');
		$deal = $helper->getDealByProduct($product);
		$currentDateTime = Mage::helper('dailydeal')->getCurrentDateTime(0);
		
		if ($helper->isEnabled() && $deal) {	
			$setPriceForType = array('simple', 'virtual', 'downloadable', 'configurable');	
			if ($currentDateTime>=$deal->getDatetimeFrom() && $currentDateTime<=$deal->getDatetimeTo()) {
				if (in_array($product->getTypeId(), $setPriceForType)) {
					$price = $this->_applyTierPrice($product, $qty, $deal->getDealPrice());
					$product->setFinalPrice($price);	
				} 
			} else {
				Mage::getModel('dailydeal/dailydeal')->refreshDeal($deal);				
			}
		}
	}
	
	public function setCollectionFinalPrice($observer)
    {
		$helper = Mage::helper('dailydeal');
		if(!$helper->isEnabled()){
			return $this;
		}

    	$products = $observer->getEvent()->getCollection();
		$currentDateTime = Mage::helper('dailydeal')->getCurrentDateTime(0);
		
        if (!$this->_dealProductIds) {
			$this->_dealProductIds = Mage::getModel('dailydeal/dailydeal')->getCollection()->addFieldToFilter('status', array('eq'=>Devinc_Dailydeal_Model_Source_Status::STATUS_RUNNING))->getColumnValues('product_id');
		}

    	foreach ($products as $product) {
			if (in_array($product->getId(), $this->_dealProductIds)) {
				$deal = $helper->getDealByProduct($product);
				
				if ($deal) {	
					$setPriceForType = array('simple', 'virtual', 'downloadable', 'configurable');	
					if ($currentDateTime>=$deal->getDatetimeFrom() && $currentDateTime<=$deal->getDatetimeTo()) {
						if (in_array($product->getTypeId(), $setPriceForType)) {
							$price = $this->_applyTierPrice($product, null, $deal->getDealPrice());
							$product->setFinalPrice($price);	
						}
					} else {
						Mage::getModel('dailydeal/dailydeal')->refreshDeal($deal);				
					}
				}
			}
		}
	}
	
	protected function _applyTierPrice($product, $qty, $finalPrice)
    {
        if (is_null($qty)) {
            return $finalPrice;
        }

        $tierPrice  = $product->getTierPrice($qty);
        if (is_numeric($tierPrice)) {
            $finalPrice = min($finalPrice, $tierPrice);
        }
        return $finalPrice;
    }

    //saves the loaded product ids in a session
	public function collectLoadedProductIds($observer)
    {
    	$helper = Mage::helper('dailydeal');
    	if (Mage::getSingleton('customer/session')->getProductIds()) {
    		$productIds = unserialize(Mage::getSingleton('customer/session')->getProductIds());
    	} else {
			$productIds = array();
		}
    	if (Mage::getSingleton('customer/session')->getProductStock()) {
    		$productStock = unserialize(Mage::getSingleton('customer/session')->getProductStock());
    	} else {
			$productStock = array();
		}
		
    	$products = $observer->getEvent()->getCollection();	
        
    	foreach ($products as $product) {
			$productIds[] = $product->getId();
			$productStock[$product->getId()] = $product->isSaleable();
		}
		
		Mage::getSingleton('customer/session')->setProductIds(serialize($productIds));
		Mage::getSingleton('customer/session')->setProductStock(serialize($productStock));
	}

	//enable parallax for desktop pcs
	public function updateBlocksBefore($observer)
	{		
        $block = $observer->getEvent()->getBlock();   
        if ($block->getNameInLayout() == 'head' && Mage::getStoreConfig('dailydeal/configuration/enabled') && !Mage::getModel('license/module')->isMobile() && !Mage::getModel('license/module')->isTablet()) { 
        	$block->setIsDesktop(true);
        }  

		$countdownType = Mage::getStoreConfig('dailydeal/configuration/countdown_type');
        if ($block->getNameInLayout() == 'head' && Mage::getStoreConfig('dailydeal/configuration/enabled')) {
        	if ($countdownType==0) {
        		$block->setCircleCountdown(true);        		
        	} elseif ($countdownType==1) {
        		$block->setFlipCountdown(true);        		
        	} elseif ($countdownType==2) {
        		$block->setSimpleCountdown(true);        		
        	}
        }  
	}

}
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Afptc
 * @version    1.1.12
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Afptc_Helper_Data extends Mage_Core_Helper_Abstract
{     
    const AW_AFPTC_RULE_DECLINE       = 'aw-afptc-rule-decline';
    const AW_AFPTC_POPUP_DECLINE      = 'aw-afptc-popup-decline';

    const GENERAL_ENABLED             = 'awafptc/general/enable';
    const GENERAL_ALLOW_READD_TO_CART = 'awafptc/general/allow_readd_to_cart';
    const POPUP_DO_NOT_SHOW_ALLOWED   = 'awafptc/popup/do_not_show_allowed';
    const POPUP_DISPLAY_ON_CHECKOUT_OR_CART_ONLY   = 'awafptc/popup/display_on_checkout_or_cart_only';
    const POPUP_COOKIE_LIFETIME       = 'awafptc/popup/cookie_lifetime';

    public function isAllowReAddToCart($store = null)
    {
        return Mage::getStoreConfig(self::GENERAL_ALLOW_READD_TO_CART, $store);
    }

    public function getCustomerGroup()
    {        
        return $this->_session()->isLoggedIn() ? $this->_session()->getCustomer()->getGroupId() : 0;
    }
    
    public function getCustomerId()
    {
        return $this->_session()->getCustomer()->getId();
    }

    public function getDeclineRuleCookieName($ruleId)
    {
        return self::AW_AFPTC_RULE_DECLINE . '-' . $ruleId;
    }

    public function getDeclinePopupCookieName()
    {
        return self::AW_AFPTC_POPUP_DECLINE;
    }

    public function setDeclineRuleCookie($ruleId)
    {
        $cookie = Mage::getSingleton('core/cookie');
        $cookie->set($this->getDeclineRuleCookieName($ruleId), '1', time() + $this->getCookieLifetime(), '/');
        return $this;
    }

    public function getDeclineRuleCookie($ruleId)
    {
        $cookie = Mage::getSingleton('core/cookie');
        return $cookie->get($this->getDeclineRuleCookieName($ruleId));
    }

    public function extensionDisabled($store = null)
    {        
        return !$this->isModuleOutputEnabled()
            || !Mage::getStoreConfig(self::GENERAL_ENABLED, $store)
        ;
    }

    public function isDoNotShowOptionAllowed($store = null)
    {
        return Mage::getStoreConfig(self::POPUP_DO_NOT_SHOW_ALLOWED, $store);
    }

    public function isDisplayOnCheckoutOrCartOnly($store = null)
    {
        return Mage::getStoreConfig(self::POPUP_DISPLAY_ON_CHECKOUT_OR_CART_ONLY, $store);
    }

    public function getCookieLifetime($store = null)
    {
        return (int)Mage::getStoreConfig(self::POPUP_COOKIE_LIFETIME, $store);
    }

    protected function _session()
    {
        return Mage::getSingleton('customer/session');
    }

    public function removeDeclineCookies()
    {
        $cookie = Mage::getSingleton('core/cookie');
        $store = Mage::app()->getStore();
        $rulesCollection = Mage::getResourceModel('awafptc/rule')->getActiveRulesCollection($store);
        foreach ($rulesCollection as $rule) {
            $cookie->set($this->getDeclineRuleCookieName($rule->getId()), '0', time() + $this->getCookieLifetime(),'/');
        }
        return $this;
    }

    public function isSubscriptionItem($item)
    {
        return Mage::helper('core')->isModuleEnabled('AW_Sarp2') && Mage::helper('aw_sarp2/quote')->isQuoteItemIsSubscriptionProduct($item);
    }

    public function getIsRuleExpired($rule)
    {
        if (is_integer($rule)) {
            $ruleModel = Mage::getModel('awafptc/rule')->load($rule);
        } elseif ($rule instanceof AW_Afptc_Model_Rule) {
            $ruleModel = $rule;
        } else {
            return true;
        }
        return 1 === Zend_Date::now()->compare(
            new Zend_Date($ruleModel->getEndDate())
        );
    }

    public function isPopupAllowed() {
        if ($this->isAcpEnabled()) {
            $store = Mage::app()->getStore();
            if (Mage::app()->getRequest()->getModuleName() != 'checkout'
                && Mage::app()->getRequest()->getRouteName() != 'aw_onestepcheckout'
                && $this->isDisplayOnCheckoutOrCartOnly($store)
            ) {
                return false;
            }
        }

        return (!$this->extensionDisabled());
    }

    public function isAcpEnabled() {
        return $this->isModuleEnabled('AW_Ajaxcartpro');
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Tax
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Tax Event Observer
 *
 * @author Magento Core Team <core@magentocommerce.com>
 */
class Mage_Tax_Model_Observer
{
    /**
     * Put quote address tax information into order
     *
     * @param Varien_Event_Observer $observer
     */
    public function salesEventConvertQuoteAddressToOrder(Varien_Event_Observer $observer)
    {
        $address = $observer->getEvent()->getAddress();
        $order = $observer->getEvent()->getOrder();

        $taxes = $address->getAppliedTaxes();
        if (is_array($taxes)) {
            if (is_array($order->getAppliedTaxes())) {
                $taxes = array_merge($order->getAppliedTaxes(), $taxes);
            }
            $order->setAppliedTaxes($taxes);
            $order->setConvertingFromQuote(true);
        }
    }

    /**
     * Save order tax information
     *
     * @param Varien_Event_Observer $observer
     */
    public function salesEventOrderAfterSave(Varien_Event_Observer $observer)
    {
        $order = $observer->getEvent()->getOrder();

        if (!$order->getConvertingFromQuote() || $order->getAppliedTaxIsSaved()) {
            return;
        }

        $getTaxesForItems   = $order->getQuote()->getTaxesForItems();
        $taxes              = $order->getAppliedTaxes();

        $ratesIdQuoteItemId = array();
        if (!is_array($getTaxesForItems)) {
            $getTaxesForItems = array();
        }
        foreach ($getTaxesForItems as $quoteItemId => $taxesArray) {
            foreach ($taxesArray as $rates) {
                if (count($rates['rates']) == 1) {
                    $ratesIdQuoteItemId[$rates['id']][] = array(
                        'id'        => $quoteItemId,
                        'percent'   => $rates['percent'],
                        'code'      => $rates['rates'][0]['code']
                    );
                } else {
                    $percentDelta   = $rates['percent'];
                    $percentSum     = 0;
                    foreach ($rates['rates'] as $rate) {
                        $ratesIdQuoteItemId[$rates['id']][] = array(
                            'id'        => $quoteItemId,
                            'percent'   => $rate['percent'],
                            'code'      => $rate['code']
                        );
                        $percentSum += $rate['percent'];
                    }

                    if ($percentDelta != $percentSum) {
                        $delta = $percentDelta - $percentSum;
                        foreach ($ratesIdQuoteItemId[$rates['id']] as &$rateTax) {
                            if ($rateTax['id'] == $quoteItemId) {
                                $rateTax['percent'] = (($rateTax['percent'] / $percentSum) * $delta)
                                        + $rateTax['percent'];
                            }
                        }
                    }
                }
            }
        }

        foreach ($taxes as $id => $row) {
            foreach ($row['rates'] as $tax) {
                if (is_null($row['percent'])) {
                    $baseRealAmount = $row['base_amount'];
                } else {
                    if ($row['percent'] == 0 || $tax['percent'] == 0) {
                        continue;
                    }
                    $baseRealAmount = $row['base_amount'] / $row['percent'] * $tax['percent'];
                }
                $hidden = (isset($row['hidden']) ? $row['hidden'] : 0);
                $data = array(
                    'order_id'          => $order->getId(),
                    'code'              => $tax['code'],
                    'title'             => $tax['title'],
                    'hidden'            => $hidden,
                    'percent'           => $tax['percent'],
                    'priority'          => $tax['priority'],
                    'position'          => $tax['position'],
                    'amount'            => $row['amount'],
                    'base_amount'       => $row['base_amount'],
                    'process'           => $row['process'],
                    'base_real_amount'  => $baseRealAmount,
                );

                $result = Mage::getModel('tax/sales_order_tax')->setData($data)->save();

                if (isset($ratesIdQuoteItemId[$id])) {
                    foreach ($ratesIdQuoteItemId[$id] as $quoteItemId) {
                        if ($quoteItemId['code'] == $tax['code']) {
                            $item = $order->getItemByQuoteItemId($quoteItemId['id']);
                            if ($item) {
                                $data = array(
                                    'item_id'       => $item->getId(),
                                    'tax_id'        => $result->getTaxId(),
                                    'tax_percent'   => $quoteItemId['percent']
                                );
                                Mage::getModel('tax/sales_order_tax_item')->setData($data)->save();
                            }
                        }
                    }
                }
            }
        }

        $order->setAppliedTaxIsSaved(true);
    }

    /**
     * Prepare select which is using to select index data for layered navigation
     *
     * @param   Varien_Event_Observer $observer
     * @return  Mage_Tax_Model_Observer
     */
    public function prepareCatalogIndexPriceSelect(Varien_Event_Observer $observer)
    {
        $table = $observer->getEvent()->getTable();
        $response = $observer->getEvent()->getResponseObject();
        $select = $observer->getEvent()->getSelect();
        $storeId = $observer->getEvent()->getStoreId();

        $additionalCalculations = $response->getAdditionalCalculations();
        $calculation = Mage::helper('tax')->getPriceTaxSql(
            $table . '.min_price', $table.'.tax_class_id'
        );

        if (!empty($calculation)) {
            $additionalCalculations[] = $calculation;
            $response->setAdditionalCalculations($additionalCalculations);
            /**
             * Tax class presented in price index table
             */
            //Mage::helper('tax')->joinTaxClass($select, $storeId, $table);
        }

        return $this;
    }

    /**
     * Add tax percent values to product collection items
     *
     * @param   Varien_Event_Observer $observer
     * @return  Mage_Tax_Model_Observer
     */
    public function addTaxPercentToProductCollection($observer)
    {
        $helper = Mage::helper('tax');
        $collection = $observer->getEvent()->getCollection();
        $store = $collection->getStoreId();
        if (!$helper->needPriceConversion($store)) {
            return $this;
        }

        if ($collection->requireTaxPercent()) {
            $request = Mage::getSingleton('tax/calculation')->getRateRequest();
            foreach ($collection as $item) {
                if (null === $item->getTaxClassId()) {
                    $item->setTaxClassId($item->getMinimalTaxClassId());
                }
                if (!isset($classToRate[$item->getTaxClassId()])) {
                    $request->setProductClassId($item->getTaxClassId());
                    $classToRate[$item->getTaxClassId()] = Mage::getSingleton('tax/calculation')->getRate($request);
                }
                $item->setTaxPercent($classToRate[$item->getTaxClassId()]);
            }

        }
        return $this;
    }

    /**
     * Refresh sales tax report statistics for last day
     *
     * @param Mage_Cron_Model_Schedule $schedule
     * @return Mage_Tax_Model_Observer
     */
    public function aggregateSalesReportTaxData($schedule)
    {
        Mage::app()->getLocale()->emulate(0);
        $currentDate = Mage::app()->getLocale()->date();
        $date = $currentDate->subHour(25);
        Mage::getResourceModel('tax/report_tax')->aggregate($date);
        Mage::app()->getLocale()->revert();
        return $this;
    }

    /**
     * Reset extra tax amounts on quote addresses before recollecting totals
     *
     * @param Varien_Event_Observer $observer
     * @return Mage_Tax_Model_Observer
     */
    public function quoteCollectTotalsBefore(Varien_Event_Observer $observer)
    {
        /* @var $quote Mage_Sales_Model_Quote */
        $quote = $observer->getEvent()->getQuote();
        foreach ($quote->getAllAddresses() as $address) {
            $address->setExtraTaxAmount(0);
            $address->setBaseExtraTaxAmount(0);
        }
        return $this;
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Customer
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Address abstract model
 *
 * @category   Mage
 * @package    Mage_Customer
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Mage_Customer_Model_Address_Abstract extends Mage_Core_Model_Abstract
{
    /**
     * Possible customer address types
     */
    const TYPE_BILLING  = 'billing';
    const TYPE_SHIPPING = 'shipping';

    /**
     * Prefix of model events
     *
     * @var string
     */
    protected $_eventPrefix = 'customer_address';

    /**
     * Name of event object
     *
     * @var string
     */
    protected $_eventObject = 'customer_address';

    /**
     * List of errors
     *
     * @var array
     */
    protected $_errors = array();

    /**
     * Directory country models
     *
     * @var array
     */
    static protected $_countryModels = array();

    /**
     * Directory region models
     *
     * @var array
     */
    static protected $_regionModels = array();

    /**
     * Get full customer name
     *
     * @return string
     */
    public function getName()
    {
        $name = '';
        $config = Mage::getSingleton('eav/config');
        if ($config->getAttribute('customer_address', 'prefix')->getIsVisible() && $this->getPrefix()) {
            $name .= $this->getPrefix() . ' ';
        }
        $name .= $this->getFirstname();
        if ($config->getAttribute('customer_address', 'middlename')->getIsVisible() && $this->getMiddlename()) {
            $name .= ' ' . $this->getMiddlename();
        }
        $name .=  ' ' . $this->getLastname();
        if ($config->getAttribute('customer_address', 'suffix')->getIsVisible() && $this->getSuffix()) {
            $name .= ' ' . $this->getSuffix();
        }
        return $name;
    }

    /**
     * get address street
     *
     * @param   int $line address line index
     * @return  string
     */
    public function getStreet($line=0)
    {
        $street = parent::getData('street');
        if (-1 === $line) {
            return $street;
        } else {
            $arr = is_array($street) ? $street : explode("\n", $street);
            if (0 === $line || $line === null) {
                return $arr;
            } elseif (isset($arr[$line-1])) {
                return $arr[$line-1];
            } else {
                return '';
            }
        }
    }

    public function getStreet1()
    {
        return $this->getStreet(1);
    }

    public function getStreet2()
    {
        return $this->getStreet(2);
    }

    public function getStreet3()
    {
        return $this->getStreet(3);
    }

    public function getStreet4()
    {
        return $this->getStreet(4);
    }

    public function getStreetFull()
    {
        return $this->getData('street');
    }

    public function setStreetFull($street)
    {
        return $this->setStreet($street);
    }

    /**
     * set address street informa
     *
     * @param unknown_type $street
     * @return unknown
     */
    public function setStreet($street)
    {
        if (is_array($street)) {
            $street = trim(implode("\n", $street));
        }
        $this->setData('street', $street);
        return $this;
    }

    /**
     * Create fields street1, street2, etc.
     *
     * To be used in controllers for views data
     *
     */
    public function explodeStreetAddress()
    {
        $streetLines = $this->getStreet();
        foreach ($streetLines as $i=>$line) {
            $this->setData('street'.($i+1), $line);
        }
        return $this;
    }

    /**
     * To be used when processing _POST
     */
    public function implodeStreetAddress()
    {
        $this->setStreet($this->getData('street'));
        return $this;
    }

    /**
     * Retrieve region name
     *
     * @return string
     */
    public function getRegion()
    {
        $regionId = $this->getData('region_id');
        $region   = $this->getData('region');

        if ($regionId) {
               if ($this->getRegionModel($regionId)->getCountryId() == $this->getCountryId()) {
                   $region = $this->getRegionModel($regionId)->getName();
                $this->setData('region', $region);
            }
        }

        if (!empty($region) && is_string($region)) {
            $this->setData('region', $region);
        }
        elseif (!$regionId && is_numeric($region)) {
            if ($this->getRegionModel($region)->getCountryId() == $this->getCountryId()) {
                $this->setData('region', $this->getRegionModel($region)->getName());
                $this->setData('region_id', $region);
            }
        }
        elseif ($regionId && !$region) {
               if ($this->getRegionModel($regionId)->getCountryId() == $this->getCountryId()) {
                $this->setData('region', $this->getRegionModel($regionId)->getName());
            }
        }

        return $this->getData('region');
    }

    /**
     * Return 2 letter state code if available, otherwise full region name
     *
     */
    public function getRegionCode()
    {
        $regionId = $this->getData('region_id');
        $region   = $this->getData('region');

        if (!$regionId && is_numeric($region)) {
            if ($this->getRegionModel($region)->getCountryId() == $this->getCountryId()) {
                $this->setData('region_code', $this->getRegionModel($region)->getCode());
            }
        }
        elseif ($regionId) {
            if ($this->getRegionModel($regionId)->getCountryId() == $this->getCountryId()) {
                $this->setData('region_code', $this->getRegionModel($regionId)->getCode());
            }
        }
        elseif (is_string($region)) {
            $this->setData('region_code', $region);
        }
        return $this->getData('region_code');
    }

    public function getRegionId()
    {
        $regionId = $this->getData('region_id');
        $region   = $this->getData('region');
        if (!$regionId) {
            if (is_numeric($region)) {
                $this->setData('region_id', $region);
                $this->unsRegion();
            } else {
                $regionModel = Mage::getModel('directory/region')
                    ->loadByCode($this->getRegionCode(), $this->getCountryId());
                $this->setData('region_id', $regionModel->getId());
            }
        }
        return $this->getData('region_id');
    }

    public function getCountry()
    {
        /*if ($this->getData('country_id') && !$this->getData('country')) {
            $this->setData('country', Mage::getModel('directory/country')
                ->load($this->getData('country_id'))->getIso2Code());
        }
        return $this->getData('country');*/
        $country = $this->getCountryId();
        return $country ? $country : $this->getData('country');
    }

    /**
     * Retrive country model
     *
     * @return Mage_Directory_Model_Country
     */
    public function getCountryModel()
    {
        if(!isset(self::$_countryModels[$this->getCountryId()])) {
            self::$_countryModels[$this->getCountryId()] = Mage::getModel('directory/country')
                ->load($this->getCountryId());
        }

        return self::$_countryModels[$this->getCountryId()];
    }

    /**
     * Retrive country model
     *
     * @return Mage_Directory_Model_Country
     */
    public function getRegionModel($region=null)
    {
        if(is_null($region)) {
            $region = $this->getRegionId();
        }

        if(!isset(self::$_regionModels[$region])) {
            self::$_regionModels[$region] = Mage::getModel('directory/region')->load($region);
        }

        return self::$_regionModels[$region];
    }

    /**
     * @deprecated for public function format
     */
    public function getHtmlFormat()
    {
        return $this->getConfig()->getFormatByCode('html');
    }

    /**
     * @deprecated for public function format
     */
    public function getFormated($html=false)
    {
        return $this->format($html ? 'html' : 'text');
        //Mage::getModel('directory/country')->load($this->getCountryId())->formatAddress($this, $html);
    }

    public function format($type)
    {
        if(!($formatType = $this->getConfig()->getFormatByCode($type))
            || !$formatType->getRenderer()) {
            return null;
        }
        Mage::dispatchEvent('customer_address_format', array('type' => $formatType, 'address' => $this));
        return $formatType->getRenderer()->render($this);
    }

    /**
     * Retrive address config object
     *
     * @return Mage_Customer_Model_Address_Config
     */
    public function getConfig()
    {
        return Mage::getSingleton('customer/address_config');
    }

    protected function _beforeSave()
    {
        parent::_beforeSave();
        $this->getRegion();
        return $this;
    }

    /**
     * Validate address attribute values
     *
     * @return array | bool
     */
    public function validate()
    {
        $this->_resetErrors();

        $this->implodeStreetAddress();

        $this->_basicCheck();

        Mage::dispatchEvent('customer_address_validation_after', array('address' => $this));

        $errors = $this->_getErrors();

        $this->_resetErrors();

        if (empty($errors) || $this->getShouldIgnoreValidation()) {
            return true;
        }
        return $errors;
    }

    /**
     * Perform basic validation
     *
     * @return void
     */
    protected function _basicCheck()
    {
        if (!Zend_Validate::is($this->getFirstname(), 'NotEmpty')) {
            $this->addError(Mage::helper('customer')->__('Please enter the first name.'));
        }

        if (!Zend_Validate::is($this->getLastname(), 'NotEmpty')) {
            $this->addError(Mage::helper('customer')->__('Please enter the last name.'));
        }

        if (!Zend_Validate::is($this->getStreet(1), 'NotEmpty')) {
            $this->addError(Mage::helper('customer')->__('Please enter the street.'));
        }

        if (!Zend_Validate::is($this->getCity(), 'NotEmpty')) {
            $this->addError(Mage::helper('customer')->__('Please enter the city.'));
        }

        if (!Zend_Validate::is($this->getTelephone(), 'NotEmpty')) {
            $this->addError(Mage::helper('customer')->__('Please enter the telephone number.'));
        }

        $_havingOptionalZip = Mage::helper('directory')->getCountriesWithOptionalZip();
        if (!in_array($this->getCountryId(), $_havingOptionalZip)
            && !Zend_Validate::is($this->getPostcode(), 'NotEmpty')
        ) {
            $this->addError(Mage::helper('customer')->__('Please enter the zip/postal code.'));
        }

        if (!Zend_Validate::is($this->getCountryId(), 'NotEmpty')) {
            $this->addError(Mage::helper('customer')->__('Please enter the country.'));
        }

        if ($this->getCountryModel()->getRegionCollection()->getSize()
            && !Zend_Validate::is($this->getRegionId(), 'NotEmpty')
            && Mage::helper('directory')->isRegionRequired($this->getCountryId())
        ) {
            $this->addError(Mage::helper('customer')->__('Please enter the state/province.'));
        }
    }

    /**
     * Add error
     *
     * @param $error
     * @return Mage_Customer_Model_Address_Abstract
     */
    public function addError($error)
    {
        $this->_errors[] = $error;
        return $this;
    }

    /**
     * Retreive errors
     *
     * @return array
     */
    protected function _getErrors()
    {
        return $this->_errors;
    }

    /**
     * Reset errors array
     *
     * @return Mage_Customer_Model_Address_Abstract
     */
    protected function _resetErrors()
    {
        $this->_errors = array();
        return $this;
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Sales
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Sales Quote address model
 *
 * @method Mage_Sales_Model_Resource_Quote_Address _getResource()
 * @method Mage_Sales_Model_Resource_Quote_Address getResource()
 * @method int getQuoteId()
 * @method Mage_Sales_Model_Quote_Address setQuoteId(int $value)
 * @method string getCreatedAt()
 * @method Mage_Sales_Model_Quote_Address setCreatedAt(string $value)
 * @method string getUpdatedAt()
 * @method Mage_Sales_Model_Quote_Address setUpdatedAt(string $value)
 * @method int getCustomerId()
 * @method Mage_Sales_Model_Quote_Address setCustomerId(int $value)
 * @method int getSaveInAddressBook()
 * @method Mage_Sales_Model_Quote_Address setSaveInAddressBook(int $value)
 * @method int getCustomerAddressId()
 * @method Mage_Sales_Model_Quote_Address setCustomerAddressId(int $value)
 * @method string getAddressType()
 * @method Mage_Sales_Model_Quote_Address setAddressType(string $value)
 * @method string getEmail()
 * @method Mage_Sales_Model_Quote_Address setEmail(string $value)
 * @method string getPrefix()
 * @method Mage_Sales_Model_Quote_Address setPrefix(string $value)
 * @method string getFirstname()
 * @method Mage_Sales_Model_Quote_Address setFirstname(string $value)
 * @method string getMiddlename()
 * @method Mage_Sales_Model_Quote_Address setMiddlename(string $value)
 * @method string getLastname()
 * @method Mage_Sales_Model_Quote_Address setLastname(string $value)
 * @method string getSuffix()
 * @method Mage_Sales_Model_Quote_Address setSuffix(string $value)
 * @method string getCompany()
 * @method Mage_Sales_Model_Quote_Address setCompany(string $value)
 * @method string getCity()
 * @method Mage_Sales_Model_Quote_Address setCity(string $value)
 * @method Mage_Sales_Model_Quote_Address setRegion(string $value)
 * @method Mage_Sales_Model_Quote_Address setRegionId(int $value)
 * @method string getPostcode()
 * @method Mage_Sales_Model_Quote_Address setPostcode(string $value)
 * @method string getCountryId()
 * @method Mage_Sales_Model_Quote_Address setCountryId(string $value)
 * @method string getTelephone()
 * @method Mage_Sales_Model_Quote_Address setTelephone(string $value)
 * @method string getFax()
 * @method Mage_Sales_Model_Quote_Address setFax(string $value)
 * @method int getSameAsBilling()
 * @method Mage_Sales_Model_Quote_Address setSameAsBilling(int $value)
 * @method int getFreeShipping()
 * @method Mage_Sales_Model_Quote_Address setFreeShipping(int $value)
 * @method int getCollectShippingRates()
 * @method Mage_Sales_Model_Quote_Address setCollectShippingRates(int $value)
 * @method string getShippingMethod()
 * @method Mage_Sales_Model_Quote_Address setShippingMethod(string $value)
 * @method string getShippingDescription()
 * @method Mage_Sales_Model_Quote_Address setShippingDescription(string $value)
 * @method float getWeight()
 * @method Mage_Sales_Model_Quote_Address setWeight(float $value)
 * @method float getSubtotal()
 * @method Mage_Sales_Model_Quote_Address setSubtotal(float $value)
 * @method float getBaseSubtotal()
 * @method Mage_Sales_Model_Quote_Address setBaseSubtotal(float $value)
 * @method Mage_Sales_Model_Quote_Address setSubtotalWithDiscount(float $value)
 * @method Mage_Sales_Model_Quote_Address setBaseSubtotalWithDiscount(float $value)
 * @method float getTaxAmount()
 * @method Mage_Sales_Model_Quote_Address setTaxAmount(float $value)
 * @method float getBaseTaxAmount()
 * @method Mage_Sales_Model_Quote_Address setBaseTaxAmount(float $value)
 * @method float getShippingAmount()
 * @method float getBaseShippingAmount()
 * @method float getShippingTaxAmount()
 * @method Mage_Sales_Model_Quote_Address setShippingTaxAmount(float $value)
 * @method float getBaseShippingTaxAmount()
 * @method Mage_Sales_Model_Quote_Address setBaseShippingTaxAmount(float $value)
 * @method float getDiscountAmount()
 * @method Mage_Sales_Model_Quote_Address setDiscountAmount(float $value)
 * @method float getBaseDiscountAmount()
 * @method Mage_Sales_Model_Quote_Address setBaseDiscountAmount(float $value)
 * @method float getGrandTotal()
 * @method Mage_Sales_Model_Quote_Address setGrandTotal(float $value)
 * @method float getBaseGrandTotal()
 * @method Mage_Sales_Model_Quote_Address setBaseGrandTotal(float $value)
 * @method string getCustomerNotes()
 * @method Mage_Sales_Model_Quote_Address setCustomerNotes(string $value)
 * @method string getDiscountDescription()
 * @method Mage_Sales_Model_Quote_Address setDiscountDescription(string $value)
 * @method null|array getDiscountDescriptionArray()
 * @method Mage_Sales_Model_Quote_Address setDiscountDescriptionArray(array $value)
 * @method float getShippingDiscountAmount()
 * @method Mage_Sales_Model_Quote_Address setShippingDiscountAmount(float $value)
 * @method float getBaseShippingDiscountAmount()
 * @method Mage_Sales_Model_Quote_Address setBaseShippingDiscountAmount(float $value)
 * @method float getSubtotalInclTax()
 * @method Mage_Sales_Model_Quote_Address setSubtotalInclTax(float $value)
 * @method float getBaseSubtotalTotalInclTax()
 * @method Mage_Sales_Model_Quote_Address setBaseSubtotalTotalInclTax(float $value)
 * @method int getGiftMessageId()
 * @method Mage_Sales_Model_Quote_Address setGiftMessageId(int $value)
 * @method float getHiddenTaxAmount()
 * @method Mage_Sales_Model_Quote_Address setHiddenTaxAmount(float $value)
 * @method float getBaseHiddenTaxAmount()
 * @method Mage_Sales_Model_Quote_Address setBaseHiddenTaxAmount(float $value)
 * @method float getShippingHiddenTaxAmount()
 * @method Mage_Sales_Model_Quote_Address setShippingHiddenTaxAmount(float $value)
 * @method float getBaseShippingHiddenTaxAmount()
 * @method Mage_Sales_Model_Quote_Address setBaseShippingHiddenTaxAmount(float $value)
 * @method float getShippingInclTax()
 * @method Mage_Sales_Model_Quote_Address setShippingInclTax(float $value)
 * @method float getBaseShippingInclTax()
 * @method Mage_Sales_Model_Quote_Address setBaseShippingInclTax(float $value)
 *
 * @category    Mage
 * @package     Mage_Sales
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Sales_Model_Quote_Address extends Mage_Customer_Model_Address_Abstract
{
    /**
     * Default value for Destination street
     */
    const DEFAULT_DEST_STREET = -1;

    /**
     * Prefix of model events
     *
     * @var string
     */
    protected $_eventPrefix = 'sales_quote_address';

    /**
     * Name of event object
     *
     * @var string
     */
    protected $_eventObject = 'quote_address';

    /**
     * Quote object
     *
     * @var Mage_Sales_Model_Quote
     */
    protected $_items = null;

    /**
     * Quote object
     *
     * @var Mage_Sales_Model_Quote
     */
    protected $_quote = null;

    /**
     * Sales Quote address rates
     *
     * @var Mage_Sales_Model_Quote_Address_Rate
     */
    protected $_rates = null;

    /**
     * Total models collector
     *
     * @var Mage_Sales_Model_Quote_Address_Total_Collector
     */
    protected $_totalCollector = null;

    /**
     * Total data as array
     *
     * @var array
     */
    protected $_totals = array();

    /**
     * Total amounts
     *
     * @var array
     */
    protected $_totalAmounts = array();

    /**
     * Total base amounts
     *
     * @var array
     */
    protected $_baseTotalAmounts = array();

    /**
     * Whether to segregate by nominal items only
     *
     * @var bool
     */
    protected $_nominalOnly = null;

    /**
     * Initialize resource
     */
    protected function _construct()
    {
        $this->_init('sales/quote_address');
    }

    /**
     * Init mapping array of short fields to its full names
     *
     * @return Mage_Sales_Model_Quote_Address
     */
    protected function _initOldFieldsMap()
    {
        $this->_oldFieldsMap = Mage::helper('sales')->getOldFieldMap('quote_address');
        return $this;
    }

    /**
     * Initialize Quote identifier before save
     *
     * @return Mage_Sales_Model_Quote_Address
     */
    protected function _beforeSave()
    {
        parent::_beforeSave();
        $this->_populateBeforeSaveData();
        return $this;
    }

    /**
     * Set the required fields
     */
    protected function _populateBeforeSaveData()
    {
        if ($this->getQuote()) {
            $this->_dataSaveAllowed = (bool)$this->getQuote()->getId();

            if ($this->getQuote()->getId()) {
                $this->setQuoteId($this->getQuote()->getId());
            }
            $this->setCustomerId($this->getQuote()->getCustomerId());

            /**
             * Init customer address id if customer address is assigned
             */
            if ($this->getCustomerAddress()) {
                $this->setCustomerAddressId($this->getCustomerAddress()->getId());
            }

            /**
             * Set same_as_billing to "1" when default shipping address is set as default
             * and it is not equal billing address
             */
            if (!$this->getId()) {
                $this->setSameAsBilling((int)$this->_isSameAsBilling());
            }
        }
    }

    /**
     * Returns true if the billing address is same as the shipping
     *
     * @return bool
     */
    protected function _isSameAsBilling()
    {
        return ($this->getAddressType() == Mage_Sales_Model_Quote_Address::TYPE_SHIPPING
            && ($this->_isNotRegisteredCustomer() || $this->_isDefaultShippingNullOrSameAsBillingAddress()));
    }

    /**
     * Checks if the user is a registered customer
     *
     * @return bool
     */
    protected function _isNotRegisteredCustomer()
    {
        return !$this->getQuote()->getCustomerId() || $this->getCustomerAddressId() === null;
    }

    /**
     * Returns true if the def billing address is same as customer address
     *
     * @return bool
     */
    protected function _isDefaultShippingNullOrSameAsBillingAddress()
    {
        $customer = $this->getQuote()->getCustomer();
        return !$customer->getDefaultShippingAddress()
            || $customer->getDefaultBillingAddress() && $customer->getDefaultShippingAddress()
                && $customer->getDefaultBillingAddress()->getId() == $customer->getDefaultShippingAddress()->getId();
    }

    /**
     * Save child collections
     *
     * @return Mage_Sales_Model_Quote_Address
     */
    protected function _afterSave()
    {
        parent::_afterSave();
        if (null !== $this->_items) {
            $this->getItemsCollection()->save();
        }
        if (null !== $this->_rates) {
            $this->getShippingRatesCollection()->save();
        }
        return $this;
    }

    /**
     * Declare adress quote model object
     *
     * @param   Mage_Sales_Model_Quote $quote
     * @return  Mage_Sales_Model_Quote_Address
     */
    public function setQuote(Mage_Sales_Model_Quote $quote)
    {
        $this->_quote = $quote;
        $this->setQuoteId($quote->getId());
        return $this;
    }

    /**
     * Retrieve quote object
     *
     * @return Mage_Sales_Model_Quote
     */
    public function getQuote()
    {
        return $this->_quote;
    }

    /**
     * Import quote address data from customer address object
     *
     * @param   Mage_Customer_Model_Address $address
     * @return  Mage_Sales_Model_Quote_Address
     */
    public function importCustomerAddress(Mage_Customer_Model_Address $address)
    {
        Mage::helper('core')->copyFieldset('customer_address', 'to_quote_address', $address, $this);
        $email = null;
        if ($address->hasEmail()) {
            $email = $address->getEmail();
        } elseif ($address->getCustomer()) {
            $email = $address->getCustomer()->getEmail();
        }
        if ($email) {
            $this->setEmail($email);
        }
        return $this;
    }

    /**
     * Export data to customer address object
     *
     * @return Mage_Customer_Model_Address
     */
    public function exportCustomerAddress()
    {
        $address = Mage::getModel('customer/address');
        Mage::helper('core')->copyFieldset('sales_convert_quote_address', 'to_customer_address', $this, $address);
        return $address;
    }

    /**
     * Import address data from order address
     *
     * @param   Mage_Sales_Model_Order_Address $address
     * @return  Mage_Sales_Model_Quote_Address
     */
    public function importOrderAddress(Mage_Sales_Model_Order_Address $address)
    {
        $this->setAddressType($address->getAddressType())
            ->setCustomerId($address->getCustomerId())
            ->setCustomerAddressId($address->getCustomerAddressId())
            ->setEmail($address->getEmail());

        Mage::helper('core')->copyFieldset('sales_convert_order_address', 'to_quote_address', $address, $this);

        return $this;
    }

    /**
     * Convert object to array
     *
     * @param   array $arrAttributes
     * @return  array
     */
    public function toArray(array $arrAttributes = array())
    {
        $arr = parent::toArray($arrAttributes);
        $arr['rates'] = $this->getShippingRatesCollection()->toArray($arrAttributes);
        $arr['items'] = $this->getItemsCollection()->toArray($arrAttributes);
        foreach ($this->getTotals() as $k => $total) {
            $arr['totals'][$k] = $total->toArray();
        }
        return $arr;
    }

    /**
     * Retrieve address items collection
     *
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     */
    public function getItemsCollection()
    {
        if (is_null($this->_items)) {
            $this->_items = Mage::getModel('sales/quote_address_item')->getCollection()
                ->setAddressFilter($this->getId());

            if ($this->getId()) {
                foreach ($this->_items as $item) {
                    $item->setAddress($this);
                }
            }
        }
        return $this->_items;
    }

    /**
     * Get all available address items
     *
     * @return array
     */
    public function getAllItems()
    {
        // We calculate item list once and cache it in three arrays - all items, nominal, non-nominal
        $cachedItems = $this->_nominalOnly ? 'nominal' : ($this->_nominalOnly === false ? 'nonnominal' : 'all');
        $key = 'cached_items_' . $cachedItems;
        if (!$this->hasData($key)) {
            // For compatibility  we will use $this->_filterNominal to divide nominal items from non-nominal
            // (because it can be overloaded)
            // So keep current flag $this->_nominalOnly and restore it after cycle
            $wasNominal = $this->_nominalOnly;
            $this->_nominalOnly = true; // Now $this->_filterNominal() will return positive values for nominal items

            $quoteItems = $this->getQuote()->getItemsCollection();
            $addressItems = $this->getItemsCollection();

            $items = array();
            $nominalItems = array();
            $nonNominalItems = array();
            if ($this->getQuote()->getIsMultiShipping() && $addressItems->count() > 0) {
                foreach ($addressItems as $aItem) {
                    if ($aItem->isDeleted()) {
                        continue;
                    }

                    if (!$aItem->getQuoteItemImported()) {
                        $qItem = $this->getQuote()->getItemById($aItem->getQuoteItemId());
                        if ($qItem) {
                            $aItem->importQuoteItem($qItem);
                        }
                    }
                    $items[] = $aItem;
                    if ($this->_filterNominal($aItem)) {
                        $nominalItems[] = $aItem;
                    } else {
                        $nonNominalItems[] = $aItem;
                    }
                }
            } else {
                /*
                * For virtual quote we assign items only to billing address, otherwise - only to shipping address
                */
                $addressType = $this->getAddressType();
                $canAddItems = $this->getQuote()->isVirtual()
                    ? ($addressType == self::TYPE_BILLING)
                    : ($addressType == self::TYPE_SHIPPING);

                if ($canAddItems) {
                    foreach ($quoteItems as $qItem) {
                        if ($qItem->isDeleted()) {
                            continue;
                        }
                        $items[] = $qItem;
                        if ($this->_filterNominal($qItem)) {
                            $nominalItems[] = $qItem;
                        } else {
                            $nonNominalItems[] = $qItem;
                        }
                    }
                }
            }

            // Cache calculated lists
            $this->setData('cached_items_all', $items);
            $this->setData('cached_items_nominal', $nominalItems);
            $this->setData('cached_items_nonnominal', $nonNominalItems);

            $this->_nominalOnly = $wasNominal; // Restore original value before we changed it
        }

        $items = $this->getData($key);
        return $items;
    }

    /**
     * Getter for all non-nominal items
     *
     * @return array
     */
    public function getAllNonNominalItems()
    {
        $this->_nominalOnly = false;
        $result = $this->getAllItems();
        $this->_nominalOnly = null;
        return $result;
    }

    /**
     * Getter for all nominal items
     *
     * @return array
     */
    public function getAllNominalItems()
    {
        $this->_nominalOnly = true;
        $result = $this->getAllItems();
        $this->_nominalOnly = null;
        return $result;
    }

    /**
     * Segregate by nominal criteria
     *
     * true: get nominals only
     * false: get non-nominals only
     * null: get all
     *
     * @param Mage_Sales_Model_Quote_Item_Abstract
     * @return Mage_Sales_Model_Quote_Item_Abstract|false
     */
    protected function _filterNominal($item)
    {
        return (null === $this->_nominalOnly)
            || ((false === $this->_nominalOnly) && !$item->isNominal())
            || ((true === $this->_nominalOnly) && $item->isNominal())
            ? $item : false;
    }

    /**
     * Retrieve all visible items
     *
     * @return array
     */
    public function getAllVisibleItems()
    {
        $items = array();
        foreach ($this->getAllItems() as $item) {
            if (!$item->getParentItemId()) {
                $items[] = $item;
            }
        }
        return $items;
    }

    /**
     * Retrieve item quantity by id
     *
     * @param int $itemId
     * @return float|int
     */
    public function getItemQty($itemId = 0)
    {
        if ($this->hasData('item_qty')) {
            return $this->getData('item_qty');
        }

        $qty = 0;
        if ($itemId == 0) {
            foreach ($this->getAllItems() as $item) {
                $qty += $item->getQty();
            }
        } else {
            $item = $this->getItemById($itemId);
            if ($item) {
                $qty = $item->getQty();
            }
        }
        return $qty;
    }

    /**
     * Check Quote address has Items
     *
     * @return bool
     */
    public function hasItems()
    {
        return sizeof($this->getAllItems())>0;
    }

    /**
     * Get address item object by id without
     *
     * @param int $itemId
     * @return Mage_Sales_Model_Quote_Address_Item
     */
    public function getItemById($itemId)
    {
        foreach ($this->getItemsCollection() as $item) {
            if ($item->getId() == $itemId) {
                return $item;
            }
        }
        return false;
    }

    /**
     * Get prepared not deleted item
     *
     * @param $itemId
     * @return Mage_Sales_Model_Quote_Address_Item
     */
    public function getValidItemById($itemId)
    {
        foreach ($this->getAllItems() as $item) {
            if ($item->getId() == $itemId) {
                return $item;
            }
        }
        return false;
    }

    /**
     * Retrieve item object by quote item Id
     *
     * @param int $itemId
     * @return Mage_Sales_Model_Quote_Address_Item
     */
    public function getItemByQuoteItemId($itemId)
    {
        foreach ($this->getItemsCollection() as $item) {
            if ($item->getQuoteItemId() == $itemId) {
                return $item;
            }
        }
        return false;
    }

    /**
     * Remove item from collection
     *
     * @param int $itemId
     * @return Mage_Sales_Model_Quote_Address
     */
    public function removeItem($itemId)
    {
        $item = $this->getItemById($itemId);
        if ($item) {
            $item->isDeleted(true);
        }
        return $this;
    }

    /**
     * Add item to address
     *
     * @param   Mage_Sales_Model_Quote_Item_Abstract $item
     * @param   int $qty
     * @return  Mage_Sales_Model_Quote_Address
     */
    public function addItem(Mage_Sales_Model_Quote_Item_Abstract $item, $qty = null)
    {
        if ($item instanceof Mage_Sales_Model_Quote_Item) {
            if ($item->getParentItemId()) {
                return $this;
            }
            $addressItem = Mage::getModel('sales/quote_address_item')
                ->setAddress($this)
                ->importQuoteItem($item);
            $this->getItemsCollection()->addItem($addressItem);

            if ($item->getHasChildren()) {
                foreach ($item->getChildren() as $child) {
                    $addressChildItem = Mage::getModel('sales/quote_address_item')
                        ->setAddress($this)
                        ->importQuoteItem($child)
                        ->setParentItem($addressItem);
                    $this->getItemsCollection()->addItem($addressChildItem);
                }
            }
        } else {
            $addressItem = $item;
            $addressItem->setAddress($this);
            if (!$addressItem->getId()) {
                $this->getItemsCollection()->addItem($addressItem);
            }
        }

        if ($qty) {
            $addressItem->setQty($qty);
        }
        return $this;
    }

    /**
     * Retrieve collection of quote shipping rates
     *
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     */
    public function getShippingRatesCollection()
    {
        if (is_null($this->_rates)) {
            $this->_rates = Mage::getModel('sales/quote_address_rate')->getCollection()
                ->setAddressFilter($this->getId());
            if ($this->getQuote()->hasNominalItems(false)) {
                $this->_rates->setFixedOnlyFilter(true);
            }
            if ($this->getId()) {
                foreach ($this->_rates as $rate) {
                    $rate->setAddress($this);
                }
            }
        }
        return $this->_rates;
    }

    /**
     * Retrieve all address shipping rates
     *
     * @return array
     */
    public function getAllShippingRates()
    {
        $rates = array();
        foreach ($this->getShippingRatesCollection() as $rate) {
            if (!$rate->isDeleted()) {
                $rates[] = $rate;
            }
        }
        return $rates;
    }

    /**
     * Retrieve all grouped shipping rates
     *
     * @return array
     */
    public function getGroupedAllShippingRates()
    {
        $rates = array();
        foreach ($this->getShippingRatesCollection() as $rate) {
            if (!$rate->isDeleted() && $rate->getCarrierInstance()) {
                if (!isset($rates[$rate->getCarrier()])) {
                    $rates[$rate->getCarrier()] = array();
                }

                $rates[$rate->getCarrier()][] = $rate;
                $rates[$rate->getCarrier()][0]->carrier_sort_order = $rate->getCarrierInstance()->getSortOrder();
            }
        }
        uasort($rates, array($this, '_sortRates'));
        return $rates;
    }

    /**
     * Sort rates recursive callback
     *
     * @param array $a
     * @param array $b
     * @return int
     */
    protected function _sortRates($a, $b)
    {
        if ((int)$a[0]->carrier_sort_order < (int)$b[0]->carrier_sort_order) {
            return -1;
        } elseif ((int)$a[0]->carrier_sort_order > (int)$b[0]->carrier_sort_order) {
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * Retrieve shipping rate by identifier
     *
     * @param   int $rateId
     * @return  Mage_Sales_Model_Quote_Address_Rate | false
     */
    public function getShippingRateById($rateId)
    {
        foreach ($this->getShippingRatesCollection() as $rate) {
            if ($rate->getId() == $rateId) {
                return $rate;
            }
        }
        return false;
    }

    /**
     * Retrieve shipping rate by code
     *
     * @param   string $code
     * @return  Mage_Sales_Model_Quote_Address_Rate
     */
    public function getShippingRateByCode($code)
    {
        foreach ($this->getShippingRatesCollection() as $rate) {
            if ($rate->getCode() == $code) {
                return $rate;
            }
        }
        return false;
    }

    /**
     * Mark all shipping rates as deleted
     *
     * @return Mage_Sales_Model_Quote_Address
     */
    public function removeAllShippingRates()
    {
        foreach ($this->getShippingRatesCollection() as $rate) {
            $rate->isDeleted(true);
        }
        return $this;
    }

    /**
     * Add shipping rate
     *
     * @param Mage_Sales_Model_Quote_Address_Rate $rate
     * @return Mage_Sales_Model_Quote_Address
     */
    public function addShippingRate(Mage_Sales_Model_Quote_Address_Rate $rate)
    {
        $rate->setAddress($this);
        $this->getShippingRatesCollection()->addItem($rate);
        return $this;
    }

    /**
     * Collecting shipping rates by address
     *
     * @return Mage_Sales_Model_Quote_Address
     */
    public function collectShippingRates()
    {
        if (!$this->getCollectShippingRates()) {
            return $this;
        }

        $this->setCollectShippingRates(false);

        $this->removeAllShippingRates();

        if (!$this->getCountryId()) {
            return $this;
        }

        $found = $this->requestShippingRates();
        if (!$found) {
            $this->setShippingAmount(0)
                ->setBaseShippingAmount(0)
                ->setShippingMethod('')
                ->setShippingDescription('');
        }

        return $this;
    }

    /**
     * Request shipping rates for entire address or specified address item
     * Returns true if current selected shipping method code corresponds to one of the found rates
     *
     * @param Mage_Sales_Model_Quote_Item_Abstract $item
     * @return bool
     */
    public function requestShippingRates(Mage_Sales_Model_Quote_Item_Abstract $item = null)
    {
        /** @var $request Mage_Shipping_Model_Rate_Request */
        $request = Mage::getModel('shipping/rate_request');
        $request->setAllItems($item ? array($item) : $this->getAllItems());
        $request->setDestCountryId($this->getCountryId());
        $request->setDestRegionId($this->getRegionId());
        $request->setDestRegionCode($this->getRegionCode());
        /**
         * need to call getStreet with -1
         * to get data in string instead of array
         */
        $request->setDestStreet($this->getStreet(self::DEFAULT_DEST_STREET));
        $request->setDestCity($this->getCity());
        $request->setDestPostcode($this->getPostcode());
        $request->setPackageValue($item ? $item->getBaseRowTotal() : $this->getBaseSubtotal());
        $packageValueWithDiscount = $item
            ? $item->getBaseRowTotal() - $item->getBaseDiscountAmount()
            : $this->getBaseSubtotalWithDiscount();
        $request->setPackageValueWithDiscount($packageValueWithDiscount);
        $request->setPackageWeight($item ? $item->getRowWeight() : $this->getWeight());
        $request->setPackageQty($item ? $item->getQty() : $this->getItemQty());

        /**
         * Need for shipping methods that use insurance based on price of physical products
         */
        $packagePhysicalValue = $item
            ? $item->getBaseRowTotal()
            : $this->getBaseSubtotal() - $this->getBaseVirtualAmount();
        $request->setPackagePhysicalValue($packagePhysicalValue);

        $request->setFreeMethodWeight($item ? 0 : $this->getFreeMethodWeight());

        /**
         * Store and website identifiers need specify from quote
         */
        /*$request->setStoreId(Mage::app()->getStore()->getId());
        $request->setWebsiteId(Mage::app()->getStore()->getWebsiteId());*/

        $request->setStoreId($this->getQuote()->getStore()->getId());
        $request->setWebsiteId($this->getQuote()->getStore()->getWebsiteId());
        $request->setFreeShipping($this->getFreeShipping());
        /**
         * Currencies need to convert in free shipping
         */
        $request->setBaseCurrency($this->getQuote()->getStore()->getBaseCurrency());
        $request->setPackageCurrency($this->getQuote()->getStore()->getCurrentCurrency());
        $request->setLimitCarrier($this->getLimitCarrier());

        $request->setBaseSubtotalInclTax($this->getBaseSubtotalInclTax() + $this->getBaseExtraTaxAmount());

        $result = Mage::getModel('shipping/shipping')->collectRates($request)->getResult();

        $found = false;
        if ($result) {
            $shippingRates = $result->getAllRates();

            foreach ($shippingRates as $shippingRate) {
                $rate = Mage::getModel('sales/quote_address_rate')
                    ->importShippingRate($shippingRate);
                if (!$item) {
                    $this->addShippingRate($rate);
                }

                if ($this->getShippingMethod() == $rate->getCode()) {
                    if ($item) {
                        $item->setBaseShippingAmount($rate->getPrice());
                    } else {
                        /**
                         * possible bug: this should be setBaseShippingAmount(),
                         * see Mage_Sales_Model_Quote_Address_Total_Shipping::collect()
                         * where this value is set again from the current specified rate price
                         * (looks like a workaround for this bug)
                         */
                        $this->setShippingAmount($rate->getPrice());
                    }

                    $found = true;
                }
            }
        }
        return $found;
    }

    /**
     * Get totals collector model
     *
     * @return Mage_Sales_Model_Quote_Address_Total_Collector
     */
    public function getTotalCollector()
    {
        if ($this->_totalCollector === null) {
            $this->_totalCollector = Mage::getSingleton(
                'sales/quote_address_total_collector',
                array('store' => $this->getQuote()->getStore())
            );
        }
        return $this->_totalCollector;
    }

    /**
     * Retrieve total models
     *
     * @deprecated
     * @return array
     */
    public function getTotalModels()
    {
        return $this->getTotalCollector()->getRetrievers();
    }

    /**
     * Collect address totals
     *
     * @return Mage_Sales_Model_Quote_Address
     */
    public function collectTotals()
    {
        Mage::dispatchEvent($this->_eventPrefix . '_collect_totals_before', array($this->_eventObject => $this));
        foreach ($this->getTotalCollector()->getCollectors() as $model) {
            $model->collect($this);
        }
        Mage::dispatchEvent($this->_eventPrefix . '_collect_totals_after', array($this->_eventObject => $this));
        return $this;
    }

    /**
     * Get address totals as array
     *
     * @return array
     */
    public function getTotals()
    {
        foreach ($this->getTotalCollector()->getRetrievers() as $model) {
            $model->fetch($this);
        }
        return $this->_totals;
    }

    /**
     * Add total data or model
     *
     * @param Mage_Sales_Model_Quote_Total|array $total
     * @return Mage_Sales_Model_Quote_Address
     */
    public function addTotal($total)
    {
        if (is_array($total)) {
            $totalInstance = Mage::getModel('sales/quote_address_total')
                ->setData($total);
        } elseif ($total instanceof Mage_Sales_Model_Quote_Total) {
            $totalInstance = $total;
        }
        $totalInstance->setAddress($this);
        $this->_totals[$totalInstance->getCode()] = $totalInstance;
        return $this;
    }

    /**
     * Rewrite clone method
     *
     * @return Mage_Sales_Model_Quote_Address
     */
    public function __clone()
    {
        $this->setId(null);
    }

    /**
     * Validate minimum amount
     *
     * @return bool
     */
    public function validateMinimumAmount()
    {
        $storeId = $this->getQuote()->getStoreId();
        if (!Mage::getStoreConfigFlag('sales/minimum_order/active', $storeId)) {
            return true;
        }

        if ($this->getQuote()->getIsVirtual() && $this->getAddressType() == self::TYPE_SHIPPING) {
            return true;
        } elseif (!$this->getQuote()->getIsVirtual() && $this->getAddressType() != self::TYPE_SHIPPING) {
            return true;
        }

        $amount = Mage::getStoreConfig('sales/minimum_order/amount', $storeId);
        if ($this->getBaseSubtotalWithDiscount() < $amount) {
            return false;
        }
        return true;
    }

    /**
     * Retrieve applied taxes
     *
     * @return array
     */
    public function getAppliedTaxes()
    {
        try {
            $return = Mage::helper('core/unserializeArray')->unserialize($this->getData('applied_taxes'));
        } catch (Exception $e) {
            $return = array();
        }
        return $return;
    }

    /**
     * Set applied taxes
     *
     * @param array $data
     * @return Mage_Sales_Model_Quote_Address
     */
    public function setAppliedTaxes($data)
    {
        return $this->setData('applied_taxes', serialize($data));
    }

    /**
     * Set shipping amount
     *
     * @param float $value
     * @param bool $alreadyExclTax
     * @return Mage_Sales_Model_Quote_Address
     */
    public function setShippingAmount($value, $alreadyExclTax = false)
    {
        return $this->setData('shipping_amount', $value);
    }

    /**
     * Set base shipping amount
     *
     * @param float $value
     * @param bool $alreadyExclTax
     * @return Mage_Sales_Model_Quote_Address
     */
    public function setBaseShippingAmount($value, $alreadyExclTax = false)
    {
        return $this->setData('base_shipping_amount', $value);
    }

    /**
     * Set total amount value
     *
     * @param   string $code
     * @param   float $amount
     * @return  Mage_Sales_Model_Quote_Address
     */
    public function setTotalAmount($code, $amount)
    {
        $this->_totalAmounts[$code] = $amount;
        if ($code != 'subtotal') {
            $code = $code.'_amount';
        }
        $this->setData($code, $amount);
        return $this;
    }

    /**
     * Set total amount value in base store currency
     *
     * @param   string $code
     * @param   float $amount
     * @return  Mage_Sales_Model_Quote_Address
     */
    public function setBaseTotalAmount($code, $amount)
    {
        $this->_baseTotalAmounts[$code] = $amount;
        if ($code != 'subtotal') {
            $code = $code.'_amount';
        }
        $this->setData('base_'.$code, $amount);
        return $this;
    }

    /**
     * Add amount total amount value
     *
     * @param   string $code
     * @param   float $amount
     * @return  Mage_Sales_Model_Quote_Address
     */
    public function addTotalAmount($code, $amount)
    {
        $amount = $this->getTotalAmount($code)+$amount;
        $this->setTotalAmount($code, $amount);
        return $this;
    }

    /**
     * Add amount total amount value in base store currency
     *
     * @param   string $code
     * @param   float $amount
     * @return  Mage_Sales_Model_Quote_Address
     */
    public function addBaseTotalAmount($code, $amount)
    {
        $amount = $this->getBaseTotalAmount($code)+$amount;
        $this->setBaseTotalAmount($code, $amount);
        return $this;
    }

    /**
     * Get total amount value by code
     *
     * @param   string $code
     * @return  float
     */
    public function getTotalAmount($code)
    {
        if (isset($this->_totalAmounts[$code])) {
            return  $this->_totalAmounts[$code];
        }
        return 0;
    }

    /**
     * Get total amount value by code in base store curncy
     *
     * @param   string $code
     * @return  float
     */
    public function getBaseTotalAmount($code)
    {
        if (isset($this->_baseTotalAmounts[$code])) {
            return  $this->_baseTotalAmounts[$code];
        }
        return 0;
    }

    /**
     * Get all total amount values
     *
     * @return array
     */
    public function getAllTotalAmounts()
    {
        return $this->_totalAmounts;
    }

    /**
     * Get all total amount values in base currency
     *
     * @return array
     */
    public function getAllBaseTotalAmounts()
    {
        return $this->_baseTotalAmounts;
    }

    /**
     * Get subtotal amount with applied discount in base currency
     *
     * @return float
     */
    public function getBaseSubtotalWithDiscount()
    {
        return $this->getBaseSubtotal()+$this->getBaseDiscountAmount();
    }

    /**
     * Get subtotal amount with applied discount
     *
     * @return float
     */
    public function getSubtotalWithDiscount()
    {
        return $this->getSubtotal()+$this->getDiscountAmount();
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Sales
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Sales abstract resource model
 *
 * @category    Mage
 * @package     Mage_Sales
 * @author      Magento Core Team <core@magentocommerce.com>
 */
abstract class Mage_Sales_Model_Resource_Abstract extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Prepare data for save
     *
     * @param Mage_Core_Model_Abstract $object
     * @return array
     */
    protected function _prepareDataForSave(Mage_Core_Model_Abstract $object)
    {
        $currentTime = Varien_Date::now();
        if ((!$object->getId() || $object->isObjectNew()) && !$object->getCreatedAt()) {
            $object->setCreatedAt($currentTime);
        }
        $object->setUpdatedAt($currentTime);
        $data = parent::_prepareDataForSave($object);
        return $data;
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Sales
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Quote address resource model
 *
 * @category    Mage
 * @package     Mage_Sales
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Sales_Model_Resource_Quote_Address extends Mage_Sales_Model_Resource_Abstract
{
    /**
     * Main table and field initialization
     *
     */
    protected function _construct()
    {
        $this->_init('sales/quote_address', 'address_id');
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Sales
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Quote addresses collection
 *
 * @category    Mage
 * @package     Mage_Sales
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Sales_Model_Resource_Quote_Address_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    /**
     * Event prefix
     *
     * @var string
     */
    protected $_eventPrefix    = 'sales_quote_address_collection';

    /**
     * Event object name
     *
     * @var string
     */
    protected $_eventObject    = 'quote_address_collection';

    /**
     * Resource initialization
     *
     */
    protected function _construct()
    {
        $this->_init('sales/quote_address');
    }

    /**
     * Setting filter on quote_id field but if quote_id is 0
     * we should exclude loading junk data from DB
     *
     * @param int $quoteId
     * @return Mage_Sales_Model_Resource_Quote_Address_Collection
     */
    public function setQuoteFilter($quoteId)
    {
        $this->addFieldToFilter('quote_id', $quoteId ? $quoteId : array('null' => 1));
        return $this;
    }

    /**
     * Redeclare after load method for dispatch event
     *
     * @return Mage_Sales_Model_Resource_Quote_Address_Collection
     */
    protected function _afterLoad()
    {
        parent::_afterLoad();

        Mage::dispatchEvent($this->_eventPrefix.'_load_after', array(
            $this->_eventObject => $this
        ));

        return $this;
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Sales
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Quote resource model
 *
 * @category    Mage
 * @package     Mage_Sales
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Sales_Model_Resource_Quote extends Mage_Sales_Model_Resource_Abstract
{
    /**
     * Initialize table nad PK name
     *
     */
    protected function _construct()
    {
        $this->_init('sales/quote', 'entity_id');
    }

    /**
     * Retrieve select object for load object data
     *
     * @param string $field
     * @param mixed $value
     * @param Mage_Core_Model_Abstract $object
     * @return Varien_Db_Select
     */
    protected function _getLoadSelect($field, $value, $object)
    {
        $select   = parent::_getLoadSelect($field, $value, $object);
        $storeIds = $object->getSharedStoreIds();
        if ($storeIds) {
            $select->where('store_id IN (?)', $storeIds);
        } else {
            /**
             * For empty result
             */
            $select->where('store_id < ?', 0);
        }

        return $select;
    }

    /**
     * Load quote data by customer identifier
     *
     * @param Mage_Sales_Model_Quote $quote
     * @param int $customerId
     * @return Mage_Sales_Model_Resource_Quote
     */
    public function loadByCustomerId($quote, $customerId)
    {
        $adapter = $this->_getReadAdapter();
        $select  = $this->_getLoadSelect('customer_id', $customerId, $quote)
            ->where('is_active = ?', 1)
            ->order('updated_at ' . Varien_Db_Select::SQL_DESC)
            ->limit(1);

        $data    = $adapter->fetchRow($select);

        if ($data) {
            $quote->setData($data);
        }

        $this->_afterLoad($quote);

        return $this;
    }

    /**
     * Load only active quote
     *
     * @param Mage_Sales_Model_Quote $quote
     * @param int $quoteId
     * @return Mage_Sales_Model_Resource_Quote
     */
    public function loadActive($quote, $quoteId)
    {
        $adapter = $this->_getReadAdapter();
        $select  = $this->_getLoadSelect('entity_id', $quoteId, $quote)
            ->where('is_active = ?', 1);

        $data    = $adapter->fetchRow($select);
        if ($data) {
            $quote->setData($data);
        }

        $this->_afterLoad($quote);

        return $this;
    }

    /**
     * Load quote data by identifier without store
     *
     * @param Mage_Sales_Model_Quote $quote
     * @param int $quoteId
     * @return Mage_Sales_Model_Resource_Quote
     */
    public function loadByIdWithoutStore($quote, $quoteId)
    {
        $read = $this->_getReadAdapter();
        if ($read) {
            $select = parent::_getLoadSelect('entity_id', $quoteId, $quote);

            $data = $read->fetchRow($select);

            if ($data) {
                $quote->setData($data);
            }
        }

        $this->_afterLoad($quote);
        return $this;
    }

    /**
     * Get reserved order id
     *
     * @param Mage_Sales_Model_Quote $quote
     * @return string
     */
    public function getReservedOrderId($quote)
    {
        $storeId = (int)$quote->getStoreId();
        return Mage::getSingleton('eav/config')->getEntityType(Mage_Sales_Model_Order::ENTITY)
            ->fetchNewIncrementId($storeId);
    }

    /**
     * Check is order increment id use in sales/order table
     *
     * @param string $orderIncrementId
     * @return boolean
     */
    public function isOrderIncrementIdUsed($orderIncrementId)
    {
        $adapter   = $this->_getReadAdapter();
        $bind      = array(':increment_id' => $orderIncrementId);
        $select    = $adapter->select();
        $select->from($this->getTable('sales/order'), 'entity_id')
            ->where('increment_id = :increment_id');
        $entity_id = $adapter->fetchOne($select, $bind);
        if ($entity_id > 0) {
            return true;
        }

        return false;
    }

    /**
     * Mark quotes - that depend on catalog price rules - to be recollected on demand
     *
     *  @param  array|null $productIdList
     *
     * @return Mage_Sales_Model_Resource_Quote
     */
    public function markQuotesRecollectByAffectedProduct($productIdList = null)
    {
        $writeAdapter = $this->_getWriteAdapter();
        $select = $writeAdapter->select();
        $subSelect = clone $select;

        $subSelect
            ->distinct()
            ->from(
                   array('qi' => $this->getTable('sales/quote_item')),
                   array('entity_id' => 'quote_id'))
            ->join(
                   array('pp' => $this->getTable('catalogrule/rule_product_price')),
                   'qi.product_id = pp.product_id',
                   array());
        if ($productIdList !== null) {
           $subSelect->where('qi.product_id IN (?)', $productIdList);
        }

        $select
             ->join(
                    array('tmp' => $subSelect),
                    'q.entity_id = tmp.entity_id',
                    array('trigger_recollect' => new Zend_Db_Expr(1)))
             ->where('q.is_active = ?', 1);
        $sql = $writeAdapter->updateFromSelect($select, array('q' => $this->getTable('sales/quote')));
        $writeAdapter->query($sql);

        return $this;
    }

    /**
     * Mark quotes - that depend on catalog price rules - to be recollected on demand
     *
     * @return Mage_Sales_Model_Resource_Quote
     */
    public function markQuotesRecollectOnCatalogRules()
    {
        return $this->markQuotesRecollectByAffectedProduct();
    }

    /**
     * Subtract product from all quotes quantities
     *
     * @param Mage_Catalog_Model_Product $product
     * @return Mage_Sales_Model_Resource_Quote
     */
    public function substractProductFromQuotes($product)
    {
        $productId = (int)$product->getId();
        if (!$productId) {
            return $this;
        }
        $adapter   = $this->_getWriteAdapter();
        $subSelect = $adapter->select();

        $subSelect->from(false, array(
            'items_qty'   => new Zend_Db_Expr(
                $adapter->quoteIdentifier('q.items_qty') . ' - ' . $adapter->quoteIdentifier('qi.qty')),
            'items_count' => new Zend_Db_Expr($adapter->quoteIdentifier('q.items_count') . ' - 1')
        ))
        ->where('q.items_count > 0')
        ->join(
            array('qi' => $this->getTable('sales/quote_item')),
            implode(' AND ', array(
                'q.entity_id = qi.quote_id',
                'qi.parent_item_id IS NULL',
                $adapter->quoteInto('qi.product_id = ?', $productId)
            )),
            array()
        );

        $updateQuery = $adapter->updateFromSelect($subSelect, array('q' => $this->getTable('sales/quote')));

        $adapter->query($updateQuery);

        return $this;
    }

    /**
     * Mark recollect contain product(s) quotes
     *
     * @param array|int|Zend_Db_Expr $productIds
     * @return Mage_Sales_Model_Resource_Quote
     */
    public function markQuotesRecollect($productIds)
    {
        $tableQuote = $this->getTable('sales/quote');
        $tableItem = $this->getTable('sales/quote_item');
        $subSelect = $this->_getReadAdapter()
            ->select()
            ->from($tableItem, array('entity_id' => 'quote_id'))
            ->where('product_id IN (?)', $productIds)
            ->group('quote_id');

        $select = $this->_getReadAdapter()->select()->join(
            array('t2' => $subSelect),
            't1.entity_id = t2.entity_id',
            array('trigger_recollect' => new Zend_Db_Expr('1'))
        );
        $updateQuery = $select->crossUpdateFromSelect(array('t1' => $tableQuote));
        $this->_getWriteAdapter()->query($updateQuery);

        return $this;
    }
}

/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Autoshipping
 */
class Amasty_Autoshipping_Model_Observer
{
    public function handleCollect($observer)
    {
        if (Mage::getStoreConfig('amautoshipping/general/enable')
            && $this->_needAutoshipping()
        )
        {
            $quote = $observer->getEvent()->getQuote();
            $shippingAddress = $quote->getShippingAddress();
            $notAutoFields = explode(',', Mage::getStoreConfig('amautoshipping/general/not_auto_fill'));
            //for compatibility with region select and region text field
            if (in_array('region', $notAutoFields)) {
                $notAutoFields[] = 'region_id';
            }

            if (!$shippingAddress->getCountryId()
                || $this->_isClassFunction('Amasty_Scheckout_Model_Cart', '_initShipping')) {
                $customerShippingAddress = Mage::getSingleton('customer/session')->getCustomer()->getDefaultShippingAddress();
                if ($customerShippingAddress) {
                    $settings['country_id'] = $customerShippingAddress->getCountryId();
                    $settings['region'] = $customerShippingAddress->getRegion();
                    $settings['region_id'] = $customerShippingAddress->getRegionId();
                    $settings['postcode'] = $customerShippingAddress->getPostcode();
                    $settings['city'] = $customerShippingAddress->getCity();
                } else {
                    $settings = Mage::getStoreConfig('amautoshipping/address');
                }

                foreach ($settings as $k => $v) {
                    if (!in_array($k, $notAutoFields)) {
                        $shippingAddress->setData($k, $v);
                    }
                }
                if (Mage::helper('core')->isModuleEnabled('Amasty_Geoip')
                    && (Mage::getStoreConfig('amautoshipping/geoip/use') == 1)
                    && !$shippingAddress->getShippingMethod()
                    && !$customerShippingAddress
                ) {
                    if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                    } else {
                        $ip = $_SERVER['REMOTE_ADDR'];
                    }
                    $geoIpModel = Mage::getModel('amgeoip/geolocation');
//                $ip = '72.229.28.185';//NY
//                $ip = '50.46.132.0';//Lynnwood

                    $location = $geoIpModel->locate($ip);
                    $country = $location->getCountry();
                    if (!empty($country)) {
                        if ($country != $shippingAddress['county_id']) {
                            foreach ($settings as $k => $v) {
                                $shippingAddress->setData($k, '');
                            }
                        }
                        $shippingAddress->setCountryId($country);

                        if (!in_array('city', $notAutoFields)) {
                            $city = $location->getCity();
                            if (!empty($city)) {
                                $shippingAddress->setCity($city);
                            }
                        }

                        if (!in_array('region', $notAutoFields)) {
                            $region = $location->getRegion();
                            if (!empty($region)) {
                                $shippingAddress->setRegion($region);
                            }
                        }

                        if (!in_array('postcode', $notAutoFields)) {
                            $postcode = $location->getPostalCode();
                            if (!empty($postcode)) {
                                $shippingAddress->setPostcode($postcode);
                            }
                        }
                    }
                }
            }

            if (!$shippingAddress->getCountryId()) {
                $defaultCountryId = Mage::getStoreConfig('general/country/default');
                $shippingAddress->setCountryId($defaultCountryId);
            }

            $isNoAutoMethod = !Mage::getStoreConfig('amautoshipping/general/shipping_method')
                && Mage::getStoreConfig('amautoshipping/general/select_method') == 'not_autoselect'
                && Mage::getStoreConfig('amautoshipping/address/country_id') === ''
            ;

            if (!$isNoAutoMethod) {
                $this->_addShippingMethod($quote);
            }
        }

        return $this;
    }

    public function beforeSaveSettings($observer)
    {
        $countryId = Mage::getStoreConfig('amautoshipping/address/country_id');
        if ($countryId) {
            $regionName = Mage::getStoreConfig('amautoshipping/address/region');
            $collectionRegions = Mage::getModel('directory/region_api')->items($countryId);
            if (is_array($collectionRegions) && !empty($collectionRegions)) {
                foreach ($collectionRegions as $region) {
                    if ($regionName == $region['name']) {
                        $regionId = $region['region_id'];
                        break;
                    }
                }
                if (isset($regionId)) {
                    Mage::getModel('core/config')->saveConfig('amautoshipping/address/region_id', $regionId);
                }
            }
        }

    }

    protected function _addShippingMethod($quote)
    {
        if ($this->_isClassFunction('Mage_Customer_Model_Customer', 'authenticate')) {
            return;
        }
        $shippingAddress = $quote->getShippingAddress();
        $shippingAddress->collectTotals();
        if (!$shippingAddress->getShippingMethod()) {
            $allShippingRates = $shippingAddress->getAllShippingRates();
            $method = $this->_applyIfOneMethod($allShippingRates);
            if (!$method) {
                $method = Mage::getStoreConfig('amautoshipping/general/shipping_method');
            }
            $shippingAddress->setShippingMethod($method)
                ->setCollectShippingRates(true)
            ;

            $shippingAddress->collectTotals();

            if (!$shippingAddress->getShippingMethod()) {
                $method = Mage::getModel('amautoshipping/selectMethods')->applyAutoShipping($allShippingRates);
            }

            $shippingAddress
                ->setShippingMethod($method)
            ;
            $shippingAddress->save();
            $quote->save();
        }
    }

    protected function _isClassFunction($class, $function)
    {
        $isClassFunction = false;
        $backtrace = debug_backtrace();
        foreach ($backtrace as $step) {
            if (isset($step['object'])
                && is_a($step['object'], $class)
                && isset($step['function'])
                && $step['function'] == $function) {
                $isClassFunction = true;
                break;
            }
        }
        $backtrace = NULL;
        return $isClassFunction;
    }

    protected function _needAutoshipping()
    {
        $needAutoshipping = false;
        $backtrace = debug_backtrace();
        foreach ($backtrace as $step) {
            if ((isset($step['object']) && isset($step['function']))) {
                if ($step['object'] instanceof Mage_Checkout_CartController && ($step['function'] == 'indexAction' || $step['function'] == 'addAction')) {
                    $needAutoshipping = true;
                    break;
                }
                if (Mage::helper('core')->isModuleEnabled('Amasty_Scheckout')) {
                    if ($step['object'] instanceof Mage_Checkout_OnepageController &&
                        ($step['function'] == 'updateAction'
                            || $step['function'] == 'initAmscheckout'
                            || $step['function'] == 'indexAction'
                            || $step['function'] == 'cartAction'
                        )) {
                        $needAutoshipping = true;
                        break;
                    }
                }
                if (Mage::helper('core')->isModuleEnabled('Amasty_Cart')) {
                    if ($step['object'] instanceof Amasty_Cart_AjaxController && $step['function'] == 'indexAction') {
                        $needAutoshipping = true;
                        break;
                    }
                }
            }
        }
        $backtrace = NULL;

        return $needAutoshipping;
    }

    protected function _applyIfOneMethod($allShippingRates)
    {
        $method = '';

        if (count($allShippingRates) == 1) {
            $method = $allShippingRates[0]->getCode();
        }

        return $method;
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_CatalogRule
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Catalog Price rules observer model
 */
class Mage_CatalogRule_Model_Observer
{
    /**
     * Preload price rules for all items in quote
     *
     * @var array
     */
    protected $_preloadedPrices = array();

    /**
     * Store calculated catalog rules prices for products
     * Prices collected per website, customer group, date and product
     *
     * @var array
     */
    protected $_rulePrices = array();

    /**
     * Apply all catalog price rules for specific product
     *
     * @param   Varien_Event_Observer $observer
     * @return  Mage_CatalogRule_Model_Observer
     */
    public function applyAllRulesOnProduct($observer)
    {
        $product = $observer->getEvent()->getProduct();
        if ($product->getIsMassupdate()) {
            return;
        }

        Mage::getModel('catalogrule/rule')->applyAllRulesToProduct($product);

        return $this;
    }

    /**
     * Load matched catalog price rules for specific product.
     * Is used for comparison in Mage_CatalogRule_Model_Resource_Rule::applyToProduct method
     *
     * @param   Varien_Event_Observer $observer
     * @return  Mage_CatalogRule_Model_Observer
     */
    public function loadProductRules($observer)
    {
        /** @var Mage_Catalog_Model_Product $product */
        $product = $observer->getEvent()->getProduct();
        if (!$product instanceof Mage_Catalog_Model_Product) {
            return $this;
        }
        Mage::getModel('catalogrule/rule')->loadProductRules($product);
        return $this;
    }

    /**
     * Apply all price rules for current date.
     * Handle catalog_product_import_after event
     *
     * @param   Varien_Event_Observer $observer
     *
     * @return  Mage_CatalogRule_Model_Observer
     */
    public function applyAllRules($observer)
    {
        /** @var $resource Mage_CatalogRule_Model_Resource_Rule */
        $resource = Mage::getResourceSingleton('catalogrule/rule');
        $resource->applyAllRules();
        Mage::getModel('catalogrule/flag')->loadSelf()
            ->setState(0)
            ->save();

        return $this;
    }

    /**
     * Preload all price rules for all items in quote
     *
     * @param   Varien_Event_Observer $observer
     *
     * @return  Mage_CatalogRule_Model_Observer
     */
    public function preloadPriceRules(Varien_Event_Observer $observer)
    {
        $quote = $observer->getQuote();
        $date = Mage::app()->getLocale()->storeTimeStamp($quote->getStoreId());
        $wId = $quote->getStore()->getWebsiteId();
        $gId = $quote->getCustomerGroupId();

        $productIds = array();
        foreach ($quote->getAllItems() as $item) {
            $productIds[] = $item->getProductId();
        }

        $cacheKey = spl_object_hash($quote);

        if (!isset($this->_preloadedPrices[$cacheKey])) {
            $this->_preloadedPrices[$cacheKey] = Mage::getResourceSingleton('catalogrule/rule')
                 ->getRulePrices($date, $wId, $gId, $productIds);
        }

        foreach ($this->_preloadedPrices[$cacheKey] as $pId => $price) {
            $key = $this->_getRulePricesKey(array($date, $wId, $gId, $pId));
            $this->_rulePrices[$key] = $price;
        }

        return $this;
    }

    /**
     * Apply catalog price rules to product on frontend
     *
     * @param   Varien_Event_Observer $observer
     *
     * @return  Mage_CatalogRule_Model_Observer
     */
    public function processFrontFinalPrice($observer)
    {
        $product    = $observer->getEvent()->getProduct();
        $pId        = $product->getId();
        $storeId    = $product->getStoreId();

        if ($observer->hasDate()) {
            $date = $observer->getEvent()->getDate();
        } else {
            $date = Mage::app()->getLocale()->storeTimeStamp($storeId);
        }

        if ($observer->hasWebsiteId()) {
            $wId = $observer->getEvent()->getWebsiteId();
        } else {
            $wId = Mage::app()->getStore($storeId)->getWebsiteId();
        }

        if ($observer->hasCustomerGroupId()) {
            $gId = $observer->getEvent()->getCustomerGroupId();
        } elseif ($product->hasCustomerGroupId()) {
            $gId = $product->getCustomerGroupId();
        } else {
            $gId = Mage::getSingleton('customer/session')->getCustomerGroupId();
        }

        $key = $this->_getRulePricesKey(array($date, $wId, $gId, $pId));
        if (!isset($this->_rulePrices[$key])) {
            $rulePrice = Mage::getResourceModel('catalogrule/rule')
                ->getRulePrice($date, $wId, $gId, $pId);
            $this->_rulePrices[$key] = $rulePrice;
        }
        if ($this->_rulePrices[$key]!==false) {
            $finalPrice = min($product->getData('final_price'), $this->_rulePrices[$key]);
            $product->setFinalPrice($finalPrice);
        }
        return $this;
    }

    /**
     * Apply catalog price rules to product in admin
     *
     * @param   Varien_Event_Observer $observer
     *
     * @return  Mage_CatalogRule_Model_Observer
     */
    public function processAdminFinalPrice($observer)
    {
        $product = $observer->getEvent()->getProduct();
        $storeId = $product->getStoreId();
        $date = Mage::app()->getLocale()->storeDate($storeId);
        $key = false;

        if ($ruleData = Mage::registry('rule_data')) {
            $wId = $ruleData->getWebsiteId();
            $gId = $ruleData->getCustomerGroupId();
            $pId = $product->getId();

            $key = $this->_getRulePricesKey(array($date, $wId, $gId, $pId));
        }
        elseif (!is_null($storeId) && !is_null($product->getCustomerGroupId())) {
            $wId = Mage::app()->getStore($storeId)->getWebsiteId();
            $gId = $product->getCustomerGroupId();
            $pId = $product->getId();
            $key = $this->_getRulePricesKey(array($date, $wId, $gId, $pId));
        }

        if ($key) {
            if (!isset($this->_rulePrices[$key])) {
                $rulePrice = Mage::getResourceModel('catalogrule/rule')
                    ->getRulePrice($date, $wId, $gId, $pId);
                $this->_rulePrices[$key] = $rulePrice;
            }
            if ($this->_rulePrices[$key]!==false) {
                $finalPrice = min($product->getData('final_price'), $this->_rulePrices[$key]);
                $product->setFinalPrice($finalPrice);
            }
        }

        return $this;
    }

    /**
     * Calculate price using catalog price rules of configurable product
     *
     * @param Varien_Event_Observer $observer
     *
     * @return Mage_CatalogRule_Model_Observer
     */
    public function catalogProductTypeConfigurablePrice(Varien_Event_Observer $observer)
    {
        $product = $observer->getEvent()->getProduct();
        if ($product instanceof Mage_Catalog_Model_Product
            && $product->getConfigurablePrice() !== null
        ) {
            $configurablePrice = $product->getConfigurablePrice();
            $productPriceRule = Mage::getModel('catalogrule/rule')->calcProductPriceRule($product, $configurablePrice);
            if ($productPriceRule !== null) {
                $product->setConfigurablePrice($productPriceRule);
            }
        }

        return $this;
    }

    /**
     * Daily update catalog price rule by cron
     * Update include interval 3 days - current day - 1 days before + 1 days after
     * This method is called from cron process, cron is working in UTC time and
     * we should generate data for interval -1 day ... +1 day
     *
     * @param   Varien_Event_Observer $observer
     *
     * @return  Mage_CatalogRule_Model_Observer
     */
    public function dailyCatalogUpdate($observer)
    {
        /** @var $model Mage_CatalogRule_Model_Rule */
        $model = Mage::getSingleton('catalogrule/rule');
        $model->applyAll();

        return $this;
    }

    /**
     * Clean out calculated catalog rule prices for products
     */
    public function flushPriceCache()
    {
        $this->_rulePrices = array();
    }

    /**
     * Calculate minimal final price with catalog rule price
     *
     * @param Varien_Event_Observer $observer
     * @return Mage_CatalogRule_Model_Observer
     */
    public function prepareCatalogProductPriceIndexTable(Varien_Event_Observer $observer)
    {
        $select             = $observer->getEvent()->getSelect();

        $indexTable         = $observer->getEvent()->getIndexTable();
        $entityId           = $observer->getEvent()->getEntityId();
        $customerGroupId    = $observer->getEvent()->getCustomerGroupId();
        $websiteId          = $observer->getEvent()->getWebsiteId();
        $websiteDate        = $observer->getEvent()->getWebsiteDate();
        $updateFields       = $observer->getEvent()->getUpdateFields();

        Mage::getSingleton('catalogrule/rule_product_price')
            ->applyPriceRuleToIndexTable($select, $indexTable, $entityId, $customerGroupId, $websiteId,
                $updateFields, $websiteDate);

        return $this;
    }

    /**
     * Check rules that contains affected attribute
     * If rules were found they will be set to inactive and notice will be add to admin session
     *
     * @param string $attributeCode
     *
     * @return Mage_CatalogRule_Model_Observer
     */
    protected function _checkCatalogRulesAvailability($attributeCode)
    {
        /* @var $collection Mage_CatalogRule_Model_Mysql4_Rule_Collection */
        $collection = Mage::getResourceModel('catalogrule/rule_collection')
            ->addAttributeInConditionFilter($attributeCode);

        $disabledRulesCount = 0;
        foreach ($collection as $rule) {
            /* @var $rule Mage_CatalogRule_Model_Rule */
            $rule->setIsActive(0);
            /* @var $rule->getConditions() Mage_CatalogRule_Model_Rule_Condition_Combine */
            $this->_removeAttributeFromConditions($rule->getConditions(), $attributeCode);
            $rule->save();

            $disabledRulesCount++;
        }

        if ($disabledRulesCount) {
            Mage::getModel('catalogrule/rule')->applyAll();
            Mage::getSingleton('adminhtml/session')->addWarning(
                Mage::helper('catalogrule')->__('%d Catalog Price Rules based on "%s" attribute have been disabled.', $disabledRulesCount, $attributeCode));
        }

        return $this;
    }

    /**
     * Remove catalog attribute condition by attribute code from rule conditions
     *
     * @param Mage_CatalogRule_Model_Rule_Condition_Combine $combine
     *
     * @param string $attributeCode
     */
    protected function _removeAttributeFromConditions($combine, $attributeCode)
    {
        $conditions = $combine->getConditions();
        foreach ($conditions as $conditionId => $condition) {
            if ($condition instanceof Mage_CatalogRule_Model_Rule_Condition_Combine) {
                $this->_removeAttributeFromConditions($condition, $attributeCode);
            }
            if ($condition instanceof Mage_Rule_Model_Condition_Product_Abstract) {
                if ($condition->getAttribute() == $attributeCode) {
                    unset($conditions[$conditionId]);
                }
            }
        }
        $combine->setConditions($conditions);
    }

    /**
     * After save attribute if it is not used for promo rules already check rules for containing this attribute
     *
     * @param Varien_Event_Observer $observer
     *
     * @return Mage_CatalogRule_Model_Observer
     */
    public function catalogAttributeSaveAfter(Varien_Event_Observer $observer)
    {
        $attribute = $observer->getEvent()->getAttribute();
        if ($attribute->dataHasChangedFor('is_used_for_promo_rules') && !$attribute->getIsUsedForPromoRules()) {
            $this->_checkCatalogRulesAvailability($attribute->getAttributeCode());
        }

        return $this;
    }

    /**
     * After delete attribute check rules that contains deleted attribute
     *
     * @param Varien_Event_Observer $observer
     * @return Mage_CatalogRule_Model_Observer
     */
    public function catalogAttributeDeleteAfter(Varien_Event_Observer $observer)
    {
        $attribute = $observer->getEvent()->getAttribute();
        if ($attribute->getIsUsedForPromoRules()) {
            $this->_checkCatalogRulesAvailability($attribute->getAttributeCode());
        }

        return $this;
    }

    public function prepareCatalogProductCollectionPrices(Varien_Event_Observer $observer)
    {
        /* @var $collection Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection */
        $collection = $observer->getEvent()->getCollection();
        $store      = Mage::app()->getStore($observer->getEvent()->getStoreId());
        $websiteId  = $store->getWebsiteId();
        if ($observer->getEvent()->hasCustomerGroupId()) {
            $groupId = $observer->getEvent()->getCustomerGroupId();
        } else {
            /* @var $session Mage_Customer_Model_Session */
            $session = Mage::getSingleton('customer/session');
            if ($session->isLoggedIn()) {
                $groupId = Mage::getSingleton('customer/session')->getCustomerGroupId();
            } else {
                $groupId = Mage_Customer_Model_Group::NOT_LOGGED_IN_ID;
            }
        }
        if ($observer->getEvent()->hasDate()) {
            $date = $observer->getEvent()->getDate();
        } else {
            $date = Mage::app()->getLocale()->storeTimeStamp($store);
        }

        $productIds = array();
        /* @var $product Mage_Core_Model_Product */
        foreach ($collection as $product) {
            $key = $this->_getRulePricesKey(array($date, $websiteId, $groupId, $product->getId()));
            if (!isset($this->_rulePrices[$key])) {
                $productIds[] = $product->getId();
            }
        }

        if ($productIds) {
            $rulePrices = Mage::getResourceModel('catalogrule/rule')
                ->getRulePrices($date, $websiteId, $groupId, $productIds);
            foreach ($productIds as $productId) {
                $key = $this->_getRulePricesKey(array($date, $websiteId, $groupId, $productId));
                $this->_rulePrices[$key] = isset($rulePrices[$productId]) ? $rulePrices[$productId] : false;
            }
        }

        return $this;
    }

    /**
     * Create catalog rule relations for imported products
     *
     * @param Varien_Event_Observer $observer
     */
    public function createCatalogRulesRelations(Varien_Event_Observer $observer)
    {
        $adapter = $observer->getEvent()->getAdapter();
        $affectedEntityIds = $adapter->getAffectedEntityIds();

        if (empty($affectedEntityIds)) {
            return;
        }

        $rules = Mage::getModel('catalogrule/rule')->getCollection()
            ->addFieldToFilter('is_active', 1);

        foreach ($rules as $rule) {
            $rule->setProductsFilter($affectedEntityIds);
            Mage::getResourceSingleton('catalogrule/rule')->updateRuleProductData($rule);
        }
    }

    /**
     * Runs Catalog Product Price Reindex
     *
     * @param Varien_Event_Observer $observer
     */
    public function runCatalogProductPriceReindex(Varien_Event_Observer $observer)
    {
        $indexProcess = Mage::getSingleton('index/indexer')->getProcessByCode('catalog_product_price');
        if ($indexProcess) {
            $indexProcess->reindexAll();
        }
    }

    /**
     * Generate key for rule prices
     *
     * @param array
     */
    protected function _getRulePricesKey($keyInfo)
    {
        return implode('|', $keyInfo);
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Product configurational item interface
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
interface Mage_Catalog_Model_Product_Configuration_Item_Interface
{
    /**
     * Retrieve associated product
     *
     * @return Mage_Catalog_Model_Product
     */
    function getProduct();

    /**
     * Get item option by code
     *
     * @param   string $code
     * @return  Mage_Catalog_Model_Product_Configuration_Item_Option_Interface
     */
    public function getOptionByCode($code);

    /**
     * Returns special download params (if needed) for custom option with type = 'file''
     * Return null, if not special params needed'
     * Or return Varien_Object with any of the following indexes:
     *  - 'url' - url of controller to give the file
     *  - 'urlParams' - additional parameters for url (custom option id, or item id, for example)
     *
     * @return null|Varien_Object
     */
    public function getFileDownloadParams();
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Sales
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Quote item abstract model
 *
 * Price attributes:
 *  - price - initial item price, declared during product association
 *  - original_price - product price before any calculations
 *  - calculation_price - prices for item totals calculation
 *  - custom_price - new price that can be declared by user and recalculated during calculation process
 *  - original_custom_price - original defined value of custom price without any convertion
 *
 * @category   Mage
 * @package    Mage_Sales
 * @author      Magento Core Team <core@magentocommerce.com>
 */
abstract class Mage_Sales_Model_Quote_Item_Abstract extends Mage_Core_Model_Abstract
    implements Mage_Catalog_Model_Product_Configuration_Item_Interface
{
    /**
     * Parent item for sub items for bundle product, configurable product, etc.
     *
     * @var Mage_Sales_Model_Quote_Item_Abstract
     */
    protected $_parentItem  = null;

    /**
     * Children items in bundle product, configurable product, etc.
     *
     * @var array
     */
    protected $_children    = array();

    /**
     *
     * @var array
     */
    protected $_messages    = array();

    /**
     * Retrieve Quote instance
     *
     * @return Mage_Sales_Model_Quote
     */
    abstract function getQuote();

    /**
     * Retrieve product model object associated with item
     *
     * @return Mage_Catalog_Model_Product
     */
    public function getProduct()
    {
        $product = $this->_getData('product');
        if ($product === null && $this->getProductId()) {
            $product = Mage::getModel('catalog/product')
                ->setStoreId($this->getQuote()->getStoreId())
                ->load($this->getProductId());
            $this->setProduct($product);
        }

        /**
         * Reset product final price because it related to custom options
         */
        $product->setFinalPrice(null);
        if (is_array($this->_optionsByCode)) {
            $product->setCustomOptions($this->_optionsByCode);
        }
        return $product;
    }

    /**
     * Returns special download params (if needed) for custom option with type = 'file'
     * Needed to implement Mage_Catalog_Model_Product_Configuration_Item_Interface.
     * Return null, as quote item needs no additional configuration.
     *
     * @return null|Varien_Object
     */
    public function getFileDownloadParams()
    {
        return null;
    }

    /**
     * Specify parent item id before saving data
     *
     * @return  Mage_Sales_Model_Quote_Item_Abstract
     */
    protected function _beforeSave()
    {
        parent::_beforeSave();
        if ($this->getParentItem()) {
            $this->setParentItemId($this->getParentItem()->getId());
        }
        return $this;
    }


    /**
     * Set parent item
     *
     * @param  Mage_Sales_Model_Quote_Item $parentItem
     * @return Mage_Sales_Model_Quote_Item
     */
    public function setParentItem($parentItem)
    {
        if ($parentItem) {
            $this->_parentItem = $parentItem;
            // Prevent duplication of children in those are already set
            if (!in_array($this, $parentItem->getChildren())) {
                $parentItem->addChild($this);
            }
        }
        return $this;
    }

    /**
     * Get parent item
     *
     * @return Mage_Sales_Model_Quote_Item
     */
    public function getParentItem()
    {
        return $this->_parentItem;
    }

    /**
     * Get chil items
     *
     * @return array
     */
    public function getChildren()
    {
        return $this->_children;
    }

    /**
     * Add child item
     *
     * @param  Mage_Sales_Model_Quote_Item_Abstract $child
     * @return Mage_Sales_Model_Quote_Item_Abstract
     */
    public function addChild($child)
    {
        $this->setHasChildren(true);
        $this->_children[] = $child;
        return $this;
    }

    /**
     * Adds message(s) for quote item. Duplicated messages are not added.
     *
     * @param  mixed $messages
     * @return Mage_Sales_Model_Quote_Item_Abstract
     */
    public function setMessage($messages)
    {
        $messagesExists = $this->getMessage(false);
        if (!is_array($messages)) {
            $messages = array($messages);
        }
        foreach ($messages as $message) {
            if (!in_array($message, $messagesExists)) {
                $this->addMessage($message);
            }
        }
        return $this;
    }

    /**
     * Add message of quote item to array of messages
     *
     * @param   string $message
     * @return  Mage_Sales_Model_Quote_Item_Abstract
     */
    public function addMessage($message)
    {
        $this->_messages[] = $message;
        return $this;
    }

    /**
     * Get messages array of quote item
     *
     * @param   bool $string flag for converting messages to string
     * @return  array|string
     */
    public function getMessage($string = true)
    {
        if ($string) {
            return join("\n", $this->_messages);
        }
        return $this->_messages;
    }

    /**
     * Removes message by text
     *
     * @param string $text
     * @return Mage_Sales_Model_Quote_Item_Abstract
     */
    public function removeMessageByText($text)
    {
        foreach ($this->_messages as $key => $message) {
            if ($message == $text) {
                unset($this->_messages[$key]);
            }
        }
        return $this;
    }

    /**
     * Clears all messages
     *
     * @return Mage_Sales_Model_Quote_Item_Abstract
     */
    public function clearMessage()
    {
        $this->unsMessage(); // For older compatibility, when we kept message inside data array
        $this->_messages = array();
        return $this;
    }

    /**
     * Retrieve store model object
     *
     * @return Mage_Core_Model_Store
     */
    public function getStore()
    {
        return $this->getQuote()->getStore();
    }

    /**
     * Checking item data
     *
     * @return Mage_Sales_Model_Quote_Item_Abstract
     */
    public function checkData()
    {
        $this->setHasError(false);
        $this->clearMessage();

        $qty = $this->_getData('qty');

        try {
            $this->setQty($qty);
        } catch (Mage_Core_Exception $e) {
            $this->setHasError(true);
            $this->setMessage($e->getMessage());
        } catch (Exception $e) {
            $this->setHasError(true);
            $this->setMessage(Mage::helper('sales')->__('Item qty declaration error.'));
        }

        try {
            $this->getProduct()->getTypeInstance(true)->checkProductBuyState($this->getProduct());
        } catch (Mage_Core_Exception $e) {
            $this->setHasError(true)
                ->setMessage($e->getMessage());
            $this->getQuote()->setHasError(true)
                ->addMessage(Mage::helper('sales')->__('Some of the products below do not have all the required options.'));
        } catch (Exception $e) {
            $this->setHasError(true)
                ->setMessage(Mage::helper('sales')->__('Item options declaration error.'));
            $this->getQuote()->setHasError(true)
                ->addMessage(Mage::helper('sales')->__('Items options declaration error.'));
        }

        if ($this->getProduct()->getHasError()) {
            $this->setHasError(true)
                ->setMessage(Mage::helper('sales')->__('Some of the selected options are not currently available.'));
            $this->getQuote()->setHasError(true)
                ->addMessage($this->getProduct()->getMessage(), 'options');
        }

        if ($this->getHasConfigurationUnavailableError()) {
            $this->setHasError(true)
                ->setMessage(Mage::helper('sales')->__('Selected option(s) or their combination is not currently available.'));
            $this->getQuote()->setHasError(true)
                ->addMessage(Mage::helper('sales')->__('Some item options or their combination are not currently available.'), 'unavailable-configuration');
            $this->unsHasConfigurationUnavailableError();
        }

        return $this;
    }

    /**
     * Get original (not related with parent item) item quantity
     *
     * @return  int|float
     */
    public function getQty()
    {
        return $this->_getData('qty');
    }

    /**
     * Get total item quantity (include parent item relation)
     *
     * @return  int|float
     */
    public function getTotalQty()
    {
        if ($this->getParentItem()) {
            return $this->getQty()*$this->getParentItem()->getQty();
        }
        return $this->getQty();
    }

    /**
     * Calculate item row total price
     *
     * @return Mage_Sales_Model_Quote_Item
     */
    public function calcRowTotal()
    {
        $qty        = $this->getTotalQty();
        // Round unit price before multiplying to prevent losing 1 cent on subtotal
        $total      = $this->getStore()->roundPrice($this->getCalculationPriceOriginal()) * $qty;
        $baseTotal  = $this->getStore()->roundPrice($this->getBaseCalculationPriceOriginal()) * $qty;

        $this->setRowTotal($this->getStore()->roundPrice($total));
        $this->setBaseRowTotal($this->getStore()->roundPrice($baseTotal));
        return $this;
    }

    /**
     * Get item price used for quote calculation process.
     * This method get custom price (if it is defined) or original product final price
     *
     * @return float
     */
    public function getCalculationPrice()
    {
        $price = $this->_getData('calculation_price');
        if (is_null($price)) {
            if ($this->hasCustomPrice()) {
                $price = $this->getCustomPrice();
            } else {
                $price = $this->getConvertedPrice();
            }
            $this->setData('calculation_price', $price);
        }
        return $price;
    }

    /**
     * Get item price used for quote calculation process.
     * This method get original custom price applied before tax calculation
     *
     * @return float
     */
    public function getCalculationPriceOriginal()
    {
        $price = $this->_getData('calculation_price');
        if (is_null($price)) {
            if ($this->hasOriginalCustomPrice()) {
                $price = $this->getOriginalCustomPrice();
            } else {
                $price = $this->getConvertedPrice();
            }
            $this->setData('calculation_price', $price);
        }
        return $price;
    }

    /**
     * Get calculation price used for quote calculation in base currency.
     *
     * @return float
     */
    public function getBaseCalculationPrice()
    {
        if (!$this->hasBaseCalculationPrice()) {
            if ($this->hasCustomPrice()) {
                $price = (float) $this->getCustomPrice();
                if ($price) {
                    $rate = $this->getStore()->convertPrice($price) / $price;
                    $price = $price / $rate;
                }
            } else {
                $price = $this->getPrice();
            }
            $this->setBaseCalculationPrice($price);
        }
        return $this->_getData('base_calculation_price');
    }

    /**
     * Get original calculation price used for quote calculation in base currency.
     *
     * @return float
     */
    public function getBaseCalculationPriceOriginal()
    {
        if (!$this->hasBaseCalculationPrice()) {
            if ($this->hasOriginalCustomPrice()) {
                $price = (float) $this->getOriginalCustomPrice();
                if ($price) {
                    $rate = $this->getStore()->convertPrice($price) / $price;
                    $price = $price / $rate;
                }
            } else {
                $price = $this->getPrice();
            }
            $this->setBaseCalculationPrice($price);
        }
        return $this->_getData('base_calculation_price');
    }

    /**
     * Get whether the item is nominal
     * TODO: fix for multishipping checkout
     *
     * @return bool
     */
    public function isNominal()
    {
        if (!$this->hasData('is_nominal')) {
            $this->setData('is_nominal', $this->getProduct() ? '1' == $this->getProduct()->getIsRecurring() : false);
        }
        return $this->_getData('is_nominal');
    }

    /**
     * Data getter for 'is_nominal'
     * Used for converting item to order item
     *
     * @return int
     */
    public function getIsNominal()
    {
        return (int)$this->isNominal();
    }

    /**
     * Get original price (retrieved from product) for item.
     * Original price value is in quote selected currency
     *
     * @return float
     */
    public function getOriginalPrice()
    {
        $price = $this->_getData('original_price');
        if (is_null($price)) {
            $price = $this->getStore()->convertPrice($this->getBaseOriginalPrice());
            $this->setData('original_price', $price);
        }
        return $price;
    }

    /**
     * Set original price to item (calculation price will be refreshed too)
     *
     * @param   float $price
     * @return  Mage_Sales_Model_Quote_Item_Abstract
     */
    public function setOriginalPrice($price)
    {
        return $this->setData('original_price', $price);
    }

    /**
     * Get Original item price (got from product) in base website currency
     *
     * @return float
     */
    public function getBaseOriginalPrice()
    {
        return $this->_getData('base_original_price');
    }

    /**
     * Specify custom item price (used in case whe we have apply not product price to item)
     *
     * @param   float $value
     * @return  Mage_Sales_Model_Quote_Item_Abstract
     */
    public function setCustomPrice($value)
    {
        $this->setCalculationPrice($value);
        $this->setBaseCalculationPrice(null);
        return $this->setData('custom_price', $value);
    }

    /**
     * Get item price. Item price currency is website base currency.
     *
     * @return decimal
     */
    public function getPrice()
    {
        return $this->_getData('price');
    }

    /**
     * Specify item price (base calculation price and converted price will be refreshed too)
     *
     * @param   float $value
     * @return  Mage_Sales_Model_Quote_Item_Abstract
     */
    public function setPrice($value)
    {
        $this->setBaseCalculationPrice(null);
        $this->setConvertedPrice(null);
        return $this->setData('price', $value);
    }

    /**
     * Get item price converted to quote currency
     * @return float
     */
    public function getConvertedPrice()
    {
        $price = $this->_getData('converted_price');
        if (is_null($price)) {
            $price = $this->getStore()->convertPrice($this->getPrice());
            $this->setData('converted_price', $price);
        }
        return $price;
    }

    /**
     * Set new value for converted price
     * @param float $value
     * @return Mage_Sales_Model_Quote_Item_Abstract
     */
    public function setConvertedPrice($value)
    {
        $this->setCalculationPrice(null);
        $this->setData('converted_price', $value);
        return $this;
    }

    /**
     * Clone quote item
     *
     * @return Mage_Sales_Model_Quote_Item
     */
    public function __clone()
    {
        $this->setId(null);
        $this->_parentItem  = null;
        $this->_children    = array();
        $this->_messages    = array();
        return $this;
    }

    /**
     * Checking if there children calculated or parent item
     * when we have parent quote item and its children
     *
     * @return bool
     */
    public function isChildrenCalculated()
    {
        if ($this->getParentItem()) {
            $calculate = $this->getParentItem()->getProduct()->getPriceType();
        } else {
            $calculate = $this->getProduct()->getPriceType();
        }

        if ((null !== $calculate) && (int)$calculate === Mage_Catalog_Model_Product_Type_Abstract::CALCULATE_CHILD) {
            return true;
        }
        return false;
    }

    /**
     * Checking can we ship product separatelly (each child separately)
     * or each parent product item can be shipped only like one item
     *
     * @return bool
     */
    public function isShipSeparately()
    {
        if ($this->getParentItem()) {
            $shipmentType = $this->getParentItem()->getProduct()->getShipmentType();
        } else {
            $shipmentType = $this->getProduct()->getShipmentType();
        }

        if ((null !== $shipmentType) &&
            (int)$shipmentType === Mage_Catalog_Model_Product_Type_Abstract::SHIPMENT_SEPARATELY) {
            return true;
        }
        return false;
    }





















    /**
     * Calculate item tax amount
     *
     * @deprecated logic moved to tax totals calculation model
     * @return  Mage_Sales_Model_Quote_Item
     */
    public function calcTaxAmount()
    {
        $store = $this->getStore();

        if (!Mage::helper('tax')->priceIncludesTax($store)) {
            if (Mage::helper('tax')->applyTaxAfterDiscount($store)) {
                $rowTotal       = $this->getRowTotalWithDiscount();
                $rowBaseTotal   = $this->getBaseRowTotalWithDiscount();
            } else {
                $rowTotal       = $this->getRowTotal();
                $rowBaseTotal   = $this->getBaseRowTotal();
            }

            $taxPercent = $this->getTaxPercent()/100;

            $this->setTaxAmount($store->roundPrice($rowTotal * $taxPercent));
            $this->setBaseTaxAmount($store->roundPrice($rowBaseTotal * $taxPercent));

            $rowTotal       = $this->getRowTotal();
            $rowBaseTotal   = $this->getBaseRowTotal();
            $this->setTaxBeforeDiscount($store->roundPrice($rowTotal * $taxPercent));
            $this->setBaseTaxBeforeDiscount($store->roundPrice($rowBaseTotal * $taxPercent));
        } else {
            if (Mage::helper('tax')->applyTaxAfterDiscount($store)) {
                $totalBaseTax = $this->getBaseTaxAmount();
                $totalTax = $this->getTaxAmount();

                if ($totalTax && $totalBaseTax) {
                    $totalTax -= $this->getDiscountAmount() * ($this->getTaxPercent() / 100);
                    $totalBaseTax -= $this->getBaseDiscountAmount() * ($this->getTaxPercent() / 100);

                    $this->setBaseTaxAmount($store->roundPrice($totalBaseTax));
                    $this->setTaxAmount($store->roundPrice($totalTax));
                }
            }
        }

        if (Mage::helper('tax')->discountTax($store) && !Mage::helper('tax')->applyTaxAfterDiscount($store)) {
            if ($this->getDiscountPercent()) {
                $baseTaxAmount =  $this->getBaseTaxBeforeDiscount();
                $taxAmount = $this->getTaxBeforeDiscount();

                $baseDiscountDisposition = $baseTaxAmount/100*$this->getDiscountPercent();
                $discountDisposition = $taxAmount/100*$this->getDiscountPercent();

                $this->setDiscountAmount($this->getDiscountAmount()+$discountDisposition);
                $this->setBaseDiscountAmount($this->getBaseDiscountAmount()+$baseDiscountDisposition);
            }
        }

        return $this;
    }

    /**
     * Get item tax amount
     *
     * @deprecated
     * @return  decimal
     */
    public function getTaxAmount()
    {
        return $this->_getData('tax_amount');
    }


    /**
     * Get item base tax amount
     *
     * @deprecated
     * @return decimal
     */
    public function getBaseTaxAmount()
    {
        return $this->_getData('base_tax_amount');
    }

    /**
     * Get item price (item price always exclude price)
     *
     * @deprecated
     * @return decimal
     */
    protected function _calculatePrice($value, $saveTaxes = true)
    {
        $store = $this->getQuote()->getStore();

        if (Mage::helper('tax')->priceIncludesTax($store)) {
            $bAddress = $this->getQuote()->getBillingAddress();
            $sAddress = $this->getQuote()->getShippingAddress();

            $address = $this->getAddress();

            if ($address) {
                switch ($address->getAddressType()) {
                    case Mage_Sales_Model_Quote_Address::TYPE_BILLING:
                        $bAddress = $address;
                        break;
                    case Mage_Sales_Model_Quote_Address::TYPE_SHIPPING:
                        $sAddress = $address;
                        break;
                }
            }

            if ($this->getProduct()->getIsVirtual()) {
                $sAddress = $bAddress;
            }

            $priceExcludingTax = Mage::helper('tax')->getPrice(
                $this->getProduct()->setTaxPercent(null),
                $value,
                false,
                $sAddress,
                $bAddress,
                $this->getQuote()->getCustomerTaxClassId(),
                $store
            );

            $priceIncludingTax = Mage::helper('tax')->getPrice(
                $this->getProduct()->setTaxPercent(null),
                $value,
                true,
                $sAddress,
                $bAddress,
                $this->getQuote()->getCustomerTaxClassId(),
                $store
            );

            if ($saveTaxes) {
                $qty = $this->getQty();
                if ($this->getParentItem()) {
                    $qty = $qty*$this->getParentItem()->getQty();
                }

                if (Mage::helper('tax')->displayCartPriceInclTax($store)) {
                    $rowTotal = $value*$qty;
                    $rowTotalExcTax = Mage::helper('tax')->getPrice(
                        $this->getProduct()->setTaxPercent(null),
                        $rowTotal,
                        false,
                        $sAddress,
                        $bAddress,
                        $this->getQuote()->getCustomerTaxClassId(),
                        $store
                    );
                    $rowTotalIncTax = Mage::helper('tax')->getPrice(
                        $this->getProduct()->setTaxPercent(null),
                        $rowTotal,
                        true,
                        $sAddress,
                        $bAddress,
                        $this->getQuote()->getCustomerTaxClassId(),
                        $store
                    );
                    $totalBaseTax = $rowTotalIncTax-$rowTotalExcTax;
                    $this->setRowTotalExcTax($rowTotalExcTax);
                }
                else {
                    $taxAmount = $priceIncludingTax - $priceExcludingTax;
                    $this->setTaxPercent($this->getProduct()->getTaxPercent());
                    $totalBaseTax = $taxAmount*$qty;
                }

                $totalTax = $this->getStore()->convertPrice($totalBaseTax);
                $this->setTaxBeforeDiscount($totalTax);
                $this->setBaseTaxBeforeDiscount($totalBaseTax);

                $this->setTaxAmount($totalTax);
                $this->setBaseTaxAmount($totalBaseTax);
            }

            $value = $priceExcludingTax;
        }

        return $value;
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Sales
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Sales Quote Item Model
 *
 * @method Mage_Sales_Model_Resource_Quote_Item _getResource()
 * @method Mage_Sales_Model_Resource_Quote_Item getResource()
 * @method int getQuoteId()
 * @method Mage_Sales_Model_Quote_Item setQuoteId(int $value)
 * @method string getCreatedAt()
 * @method Mage_Sales_Model_Quote_Item setCreatedAt(string $value)
 * @method string getUpdatedAt()
 * @method Mage_Sales_Model_Quote_Item setUpdatedAt(string $value)
 * @method int getProductId()
 * @method Mage_Sales_Model_Quote_Item setProductId(int $value)
 * @method int getStoreId()
 * @method Mage_Sales_Model_Quote_Item setStoreId(int $value)
 * @method int getParentItemId()
 * @method Mage_Sales_Model_Quote_Item setParentItemId(int $value)
 * @method int getIsVirtual()
 * @method Mage_Sales_Model_Quote_Item setIsVirtual(int $value)
 * @method string getSku()
 * @method Mage_Sales_Model_Quote_Item setSku(string $value)
 * @method string getName()
 * @method Mage_Sales_Model_Quote_Item setName(string $value)
 * @method string getDescription()
 * @method Mage_Sales_Model_Quote_Item setDescription(string $value)
 * @method string getAppliedRuleIds()
 * @method Mage_Sales_Model_Quote_Item setAppliedRuleIds(string $value)
 * @method string getAdditionalData()
 * @method Mage_Sales_Model_Quote_Item setAdditionalData(string $value)
 * @method int getFreeShipping()
 * @method Mage_Sales_Model_Quote_Item setFreeShipping(int $value)
 * @method int getIsQtyDecimal()
 * @method Mage_Sales_Model_Quote_Item setIsQtyDecimal(int $value)
 * @method int getNoDiscount()
 * @method Mage_Sales_Model_Quote_Item setNoDiscount(int $value)
 * @method float getWeight()
 * @method Mage_Sales_Model_Quote_Item setWeight(float $value)
 * @method float getBasePrice()
 * @method Mage_Sales_Model_Quote_Item setBasePrice(float $value)
 * @method float getCustomPrice()
 * @method float getDiscountPercent()
 * @method Mage_Sales_Model_Quote_Item setDiscountPercent(float $value)
 * @method float getDiscountAmount()
 * @method Mage_Sales_Model_Quote_Item setDiscountAmount(float $value)
 * @method float getBaseDiscountAmount()
 * @method Mage_Sales_Model_Quote_Item setBaseDiscountAmount(float $value)
 * @method float getTaxPercent()
 * @method Mage_Sales_Model_Quote_Item setTaxPercent(float $value)
 * @method Mage_Sales_Model_Quote_Item setTaxAmount(float $value)
 * @method Mage_Sales_Model_Quote_Item setBaseTaxAmount(float $value)
 * @method float getRowTotal()
 * @method Mage_Sales_Model_Quote_Item setRowTotal(float $value)
 * @method float getBaseRowTotal()
 * @method Mage_Sales_Model_Quote_Item setBaseRowTotal(float $value)
 * @method float getRowTotalWithDiscount()
 * @method Mage_Sales_Model_Quote_Item setRowTotalWithDiscount(float $value)
 * @method float getRowWeight()
 * @method Mage_Sales_Model_Quote_Item setRowWeight(float $value)
 * @method Mage_Sales_Model_Quote_Item setProductType(string $value)
 * @method float getBaseTaxBeforeDiscount()
 * @method Mage_Sales_Model_Quote_Item setBaseTaxBeforeDiscount(float $value)
 * @method float getTaxBeforeDiscount()
 * @method Mage_Sales_Model_Quote_Item setTaxBeforeDiscount(float $value)
 * @method float getOriginalCustomPrice()
 * @method Mage_Sales_Model_Quote_Item setOriginalCustomPrice(float $value)
 * @method string getRedirectUrl()
 * @method Mage_Sales_Model_Quote_Item setRedirectUrl(string $value)
 * @method float getBaseCost()
 * @method Mage_Sales_Model_Quote_Item setBaseCost(float $value)
 * @method float getPriceInclTax()
 * @method Mage_Sales_Model_Quote_Item setPriceInclTax(float $value)
 * @method float getBasePriceInclTax()
 * @method Mage_Sales_Model_Quote_Item setBasePriceInclTax(float $value)
 * @method float getRowTotalInclTax()
 * @method Mage_Sales_Model_Quote_Item setRowTotalInclTax(float $value)
 * @method float getBaseRowTotalInclTax()
 * @method Mage_Sales_Model_Quote_Item setBaseRowTotalInclTax(float $value)
 * @method int getGiftMessageId()
 * @method Mage_Sales_Model_Quote_Item setGiftMessageId(int $value)
 * @method string getWeeeTaxApplied()
 * @method Mage_Sales_Model_Quote_Item setWeeeTaxApplied(string $value)
 * @method float getWeeeTaxAppliedAmount()
 * @method Mage_Sales_Model_Quote_Item setWeeeTaxAppliedAmount(float $value)
 * @method float getWeeeTaxAppliedRowAmount()
 * @method Mage_Sales_Model_Quote_Item setWeeeTaxAppliedRowAmount(float $value)
 * @method float getBaseWeeeTaxAppliedAmount()
 * @method Mage_Sales_Model_Quote_Item setBaseWeeeTaxAppliedAmount(float $value)
 * @method float getBaseWeeeTaxAppliedRowAmount()
 * @method Mage_Sales_Model_Quote_Item setBaseWeeeTaxAppliedRowAmount(float $value)
 * @method float getWeeeTaxDisposition()
 * @method Mage_Sales_Model_Quote_Item setWeeeTaxDisposition(float $value)
 * @method float getWeeeTaxRowDisposition()
 * @method Mage_Sales_Model_Quote_Item setWeeeTaxRowDisposition(float $value)
 * @method float getBaseWeeeTaxDisposition()
 * @method Mage_Sales_Model_Quote_Item setBaseWeeeTaxDisposition(float $value)
 * @method float getBaseWeeeTaxRowDisposition()
 * @method Mage_Sales_Model_Quote_Item setBaseWeeeTaxRowDisposition(float $value)
 * @method float getHiddenTaxAmount()
 * @method Mage_Sales_Model_Quote_Item setHiddenTaxAmount(float $value)
 * @method float getBaseHiddenTaxAmount()
 * @method Mage_Sales_Model_Quote_Item setBaseHiddenTaxAmount(float $value)
 * @method null|bool getHasConfigurationUnavailableError()
 * @method Mage_Sales_Model_Quote_Item setHasConfigurationUnavailableError(bool $value)
 * @method Mage_Sales_Model_Quote_Item unsHasConfigurationUnavailableError()
 *
 * @category    Mage
 * @package     Mage_Sales
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Sales_Model_Quote_Item extends Mage_Sales_Model_Quote_Item_Abstract
{
    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'sales_quote_item';

    /**
     * Parameter name in event
     *
     * In observe method you can use $observer->getEvent()->getObject() in this case
     *
     * @var string
     */
    protected $_eventObject = 'item';

    /**
     * Quote model object
     *
     * @var Mage_Sales_Model_Quote
     */
    protected $_quote;

    /**
     * Item options array
     *
     * @var array
     */
    protected $_options = array();

    /**
     * Item options by code cache
     *
     * @var array
     */
    protected $_optionsByCode = array();

    /**
     * Not Represent options
     *
     * @var array
     */
    protected $_notRepresentOptions = array('info_buyRequest');

    /**
     * Flag stating that options were successfully saved
     *
     */
    protected $_flagOptionsSaved = null;

    /**
     * Array of errors associated with this quote item
     *
     * @var Mage_Sales_Model_Status_List
     */
    protected $_errorInfos = null;

    /**
     * Initialize resource model
     *
     */
    protected function _construct()
    {
        $this->_init('sales/quote_item');
        $this->_errorInfos = Mage::getModel('sales/status_list');
    }

    /**
     * Init mapping array of short fields to
     * its full names
     *
     * @return Varien_Object
     */
    protected function _initOldFieldsMap()
    {
        $this->_oldFieldsMap = Mage::helper('sales')->getOldFieldMap('quote_item');
        return $this;
    }

    /**
     * Quote Item Before Save prepare data process
     *
     * @return Mage_Sales_Model_Quote_Item
     */
    protected function _beforeSave()
    {
        parent::_beforeSave();
        $this->setIsVirtual($this->getProduct()->getIsVirtual());
        if ($this->getQuote()) {
            $this->setQuoteId($this->getQuote()->getId());
        }
        return $this;
    }

    /**
     * Declare quote model object
     *
     * @param   Mage_Sales_Model_Quote $quote
     * @return  Mage_Sales_Model_Quote_Item
     */
    public function setQuote(Mage_Sales_Model_Quote $quote)
    {
        $this->_quote = $quote;
        $this->setQuoteId($quote->getId());
        return $this;
    }

    /**
     * Retrieve quote model object
     *
     * @return Mage_Sales_Model_Quote
     */
    public function getQuote()
    {
        return $this->_quote;
    }

    /**
     * Prepare quantity
     *
     * @param float|int $qty
     * @return int|float
     */
    protected function _prepareQty($qty)
    {
        $qty = Mage::app()->getLocale()->getNumber($qty);
        $qty = ($qty > 0) ? $qty : 1;
        return $qty;
    }

    /**
     * Get Magento App instance
     *
     * @return Mage_Core_Model_App
     */
    protected function _getApp()
    {
        return Mage::app();
    }

    /**
     * Adding quantity to quote item
     *
     * @param float $qty
     * @return Mage_Sales_Model_Quote_Item
     */
    public function addQty($qty)
    {
        $oldQty = $this->getQty();
        $qty = $this->_prepareQty($qty);

        /**
         * We can't modify quontity of existing items which have parent
         * This qty declared just once duering add process and is not editable
         */
        if (!$this->getParentItem() || !$this->getId()) {
            $this->setQtyToAdd($qty);
            $this->setQty($oldQty + $qty);
        }
        return $this;
    }

    /**
     * Declare quote item quantity
     *
     * @param float $qty
     * @return Mage_Sales_Model_Quote_Item
     */
    public function setQty($qty)
    {
        $qty = $this->_prepareQty($qty);
        $oldQty = $this->_getData('qty');
        $this->setData('qty', $qty);

        Mage::dispatchEvent('sales_quote_item_qty_set_after', array('item' => $this));

        if ($this->getQuote() && $this->getQuote()->getIgnoreOldQty()) {
            return $this;
        }
        if ($this->getUseOldQty()) {
            $this->setData('qty', $oldQty);
        }

        return $this;
    }

    /**
     * Retrieve option product with Qty
     *
     * Return array
     * 'qty'        => the qty
     * 'product'    => the product model
     *
     * @return array
     */
    public function getQtyOptions()
    {
        $qtyOptions = $this->getData('qty_options');
        if (is_null($qtyOptions)) {
            $productIds = array();
            $qtyOptions = array();
            foreach ($this->getOptions() as $option) {
                /** @var $option Mage_Sales_Model_Quote_Item_Option */
                if (is_object($option->getProduct())
                    && $option->getProduct()->getId() != $this->getProduct()->getId()
                ) {
                    $productIds[$option->getProduct()->getId()] = $option->getProduct()->getId();
                }
            }

            foreach ($productIds as $productId) {
                $option = $this->getOptionByCode('product_qty_' . $productId);
                if ($option) {
                    $qtyOptions[$productId] = $option;
                }
            }

            $this->setData('qty_options', $qtyOptions);
        }

        return $qtyOptions;
    }

    /**
     * Set option product with Qty
     *
     * @param  $qtyOptions
     * @return Mage_Sales_Model_Quote_Item
     */
    public function setQtyOptions($qtyOptions)
    {
        return $this->setData('qty_options', $qtyOptions);
    }

    /**
     * Setup product for quote item
     *
     * @param   Mage_Catalog_Model_Product $product
     * @return  Mage_Sales_Model_Quote_Item
     */
    public function setProduct($product)
    {
        if ($this->getQuote()) {
            $product->setStoreId($this->getQuote()->getStoreId());
            $product->setCustomerGroupId($this->getQuote()->getCustomerGroupId());
        }
        $this->setData('product', $product)
            ->setProductId($product->getId())
            ->setProductType($product->getTypeId())
            ->setSku($this->getProduct()->getSku())
            ->setName($product->getName())
            ->setWeight($this->getProduct()->getWeight())
            ->setTaxClassId($product->getTaxClassId())
            ->setBaseCost($product->getCost())
            ->setIsRecurring($product->getIsRecurring());

        if ($product->getStockItem()) {
            $this->setIsQtyDecimal($product->getStockItem()->getIsQtyDecimal());
        }

        Mage::dispatchEvent('sales_quote_item_set_product', array(
            'product' => $product,
            'quote_item' => $this
        ));


//        if ($options = $product->getCustomOptions()) {
//            foreach ($options as $option) {
//                $this->addOption($option);
//            }
//        }
        return $this;
    }

    /**
     * Check product representation in item
     *
     * @param   Mage_Catalog_Model_Product $product
     * @return  bool
     */
    public function representProduct($product)
    {
        $itemProduct = $this->getProduct();
        if (!$product || $itemProduct->getId() != $product->getId()) {
            return false;
        }

        /**
         * Check maybe product is planned to be a child of some quote item - in this case we limit search
         * only within same parent item
         */
        $stickWithinParent = $product->getStickWithinParent();
        if ($stickWithinParent) {
            if ($this->getParentItem() !== $stickWithinParent) {
                return false;
            }
        }

        // Check options
        $itemOptions = $this->getOptionsByCode();
        $productOptions = $product->getCustomOptions();

        if (!$this->compareOptions($itemOptions, $productOptions)) {
            return false;
        }
        if (!$this->compareOptions($productOptions, $itemOptions)) {
            return false;
        }
        return true;
    }

    /**
     * Check if two options array are identical
     * First options array is prerogative
     * Second options array checked against first one
     *
     * @param array $options1
     * @param array $options2
     * @return bool
     */
    public function compareOptions($options1, $options2)
    {
        foreach ($options1 as $option) {
            $code = $option->getCode();
            if (in_array($code, $this->_notRepresentOptions)) {
                continue;
            }
            if (!isset($options2[$code])
                || ($options2[$code]->getValue() === null)
                || $options2[$code]->getValue() != $option->getValue()
            ) {
                return false;
            }
        }
        return true;
    }

    /**
     * Compare item
     *
     * @param   Mage_Sales_Model_Quote_Item $item
     * @return  bool
     */
    public function compare($item)
    {
        if ($this->getProductId() != $item->getProductId()) {
            return false;
        }
        foreach ($this->getOptions() as $option) {
            if (in_array($option->getCode(), $this->_notRepresentOptions)
                && !$item->getProduct()->hasCustomOptions()
            ) {
                continue;
            }
            if ($itemOption = $item->getOptionByCode($option->getCode())) {
                $itemOptionValue = $itemOption->getValue();
                $optionValue = $option->getValue();

                // dispose of some options params, that can cramp comparing of arrays
                if (is_string($itemOptionValue) && is_string($optionValue)) {
                    try {
                        /** @var Unserialize_Parser $parser */
                        $parser = Mage::helper('core/unserializeArray');

                        $_itemOptionValue =
                            is_numeric($itemOptionValue) ? $itemOptionValue : $parser->unserialize($itemOptionValue);
                        $_optionValue = is_numeric($optionValue) ? $optionValue : $parser->unserialize($optionValue);

                        if (is_array($_itemOptionValue) && is_array($_optionValue)) {
                            $itemOptionValue = $_itemOptionValue;
                            $optionValue = $_optionValue;
                            // looks like it does not break bundle selection qty
                            foreach (array('qty', 'uenc', 'form_key', 'item', 'original_qty') as $key) {
                                unset($itemOptionValue[$key], $optionValue[$key]);
                            }
                        }

                    } catch (Exception $e) {
                        Mage::logException($e);
                    }
                }

                if ($itemOptionValue != $optionValue) {
                    return false;
                }
            } else {
                return false;
            }
        }
        return true;
    }

    /**
     * Get item product type
     *
     * @return string
     */
    public function getProductType()
    {
        if ($option = $this->getOptionByCode('product_type')) {
            return $option->getValue();
        }
        if ($product = $this->getProduct()) {
            return $product->getTypeId();
        }
        return $this->_getData('product_type');
    }

    /**
     * Return real product type of item
     *
     * @return unknown
     */
    public function getRealProductType()
    {
        return $this->_getData('product_type');
    }

    /**
     * Convert Quote Item to array
     *
     * @param array $arrAttributes
     * @return array
     */
    public function toArray(array $arrAttributes = array())
    {
        $data = parent::toArray($arrAttributes);

        if ($product = $this->getProduct()) {
            $data['product'] = $product->toArray();
        }
        return $data;
    }

    /**
     * Initialize quote item options
     *
     * @param   array $options
     * @return  Mage_Sales_Model_Quote_Item
     */
    public function setOptions($options)
    {
        foreach ($options as $option) {
            $this->addOption($option);
        }
        return $this;
    }

    /**
     * Get all item options
     *
     * @return array
     */
    public function getOptions()
    {
        return $this->_options;
    }

    /**
     * Get all item options as array with codes in array key
     *
     * @return array
     */
    public function getOptionsByCode()
    {
        return $this->_optionsByCode;
    }

    /**
     * Add option to item
     *
     * @param   Mage_Sales_Model_Quote_Item_Option|Varien_Object $option
     * @return  Mage_Sales_Model_Quote_Item
     */
    public function addOption($option)
    {
        if (is_array($option)) {
            $option = Mage::getModel('sales/quote_item_option')->setData($option)
                ->setItem($this);
        } elseif (($option instanceof Varien_Object) && !($option instanceof Mage_Sales_Model_Quote_Item_Option)) {
            $option = Mage::getModel('sales/quote_item_option')->setData($option->getData())
                ->setProduct($option->getProduct())
                ->setItem($this);
        } elseif ($option instanceof Mage_Sales_Model_Quote_Item_Option) {
            $option->setItem($this);
        } else {
            Mage::throwException(Mage::helper('sales')->__('Invalid item option format.'));
        }

        if ($exOption = $this->getOptionByCode($option->getCode())) {
            $exOption->addData($option->getData());
        } else {
            $this->_addOptionCode($option);
            $this->_options[] = $option;
        }
        return $this;
    }

    /**
     * Can specify specific actions for ability to change given quote options values
     * Exemple: cataloginventory decimal qty validation may change qty to int,
     * so need to change quote item qty option value.
     *
     * @param Varien_Object $option
     * @param int|float|null $value
     * @return Mage_Sales_Model_Quote_Item
     */
    public function updateQtyOption(Varien_Object $option, $value)
    {
        $optionProduct = $option->getProduct();
        $options = $this->getQtyOptions();

        if (isset($options[$optionProduct->getId()])) {
            $options[$optionProduct->getId()]->setValue($value);
        }

        $this->getProduct()->getTypeInstance(true)
            ->updateQtyOption($this->getOptions(), $option, $value, $this->getProduct());

        return $this;
    }

    /**
     *Remove option from item options
     *
     * @param string $code
     * @return Mage_Sales_Model_Quote_Item
     */
    public function removeOption($code)
    {
        $option = $this->getOptionByCode($code);
        if ($option) {
            $option->isDeleted(true);
        }
        return $this;
    }

    /**
     * Register option code
     *
     * @param   Mage_Sales_Model_Quote_Item_Option $option
     * @return  Mage_Sales_Model_Quote_Item
     */
    protected function _addOptionCode($option)
    {
        if (!isset($this->_optionsByCode[$option->getCode()])) {
            $this->_optionsByCode[$option->getCode()] = $option;
        } else {
            Mage::throwException(Mage::helper('sales')->__('An item option with code %s already exists.', $option->getCode()));
        }
        return $this;
    }

    /**
     * Get item option by code
     *
     * @param   string $code
     * @return  Mage_Sales_Model_Quote_Item_Option || null
     */
    public function getOptionByCode($code)
    {
        if (isset($this->_optionsByCode[$code]) && !$this->_optionsByCode[$code]->isDeleted()) {
            return $this->_optionsByCode[$code];
        }
        return null;
    }

    /**
     * Checks that item model has data changes.
     * Call save item options if model isn't need to save in DB
     *
     * @return boolean
     */
    protected function _hasModelChanged()
    {
        if (!$this->hasDataChanges()) {
            return false;
        }

        return $this->_getResource()->hasDataChanged($this);
    }

    /**
     * Save item options
     *
     * @return Mage_Sales_Model_Quote_Item
     */
    protected function _saveItemOptions()
    {
        foreach ($this->_options as $index => $option) {
            if ($option->isDeleted()) {
                $option->delete();
                unset($this->_options[$index]);
                unset($this->_optionsByCode[$option->getCode()]);
            } else {
                $option->save();
            }
        }

        $this->_flagOptionsSaved = true; // Report to watchers that options were saved

        return $this;
    }

    /**
     * Save model plus its options
     * Ensures saving options in case when resource model was not changed
     */
    public function save()
    {
        $hasDataChanges = $this->hasDataChanges();
        $this->_flagOptionsSaved = false;

        parent::save();

        if ($hasDataChanges && !$this->_flagOptionsSaved) {
            $this->_saveItemOptions();
        }
    }

    /**
     * Save item options after item saved
     *
     * @return Mage_Sales_Model_Quote_Item
     */
    protected function _afterSave()
    {
        $this->_saveItemOptions();
        return parent::_afterSave();
    }

    /**
     * Clone quote item
     *
     * @return Mage_Sales_Model_Quote_Item
     */
    public function __clone()
    {
        parent::__clone();
        $options = $this->getOptions();
        $this->_quote = null;
        $this->_options = array();
        $this->_optionsByCode = array();
        foreach ($options as $option) {
            $this->addOption(clone $option);
        }
        return $this;
    }

    /**
     * Returns formatted buy request - object, holding request received from
     * product view page with keys and options for configured product
     *
     * @return Varien_Object
     */
    public function getBuyRequest()
    {
        $option = $this->getOptionByCode('info_buyRequest');
        $buyRequest = new Varien_Object($option ? unserialize($option->getValue()) : null);

        // Overwrite standard buy request qty, because item qty could have changed since adding to quote
        $buyRequest->setOriginalQty($buyRequest->getQty())
            ->setQty($this->getQty() * 1);

        return $buyRequest;
    }

    /**
     * Sets flag, whether this quote item has some error associated with it.
     *
     * @param bool $flag
     * @return Mage_Sales_Model_Quote_Item
     */
    protected function _setHasError($flag)
    {
        return $this->setData('has_error', $flag);
    }

    /**
     * Sets flag, whether this quote item has some error associated with it.
     * When TRUE - also adds 'unknown' error information to list of quote item errors.
     * When FALSE - clears whole list of quote item errors.
     * It's recommended to use addErrorInfo() instead - to be able to remove error statuses later.
     *
     * @param bool $flag
     * @return Mage_Sales_Model_Quote_Item
     * @see addErrorInfo()
     */
    public function setHasError($flag)
    {
        if ($flag) {
            $this->addErrorInfo();
        } else {
            $this->_clearErrorInfo();
        }
        return $this;
    }

    /**
     * Clears list of errors, associated with this quote item.
     * Also automatically removes error-flag from oneself.
     *
     * @return Mage_Sales_Model_Quote_Item
     */
    protected function _clearErrorInfo()
    {
        $this->_errorInfos->clear();
        $this->_setHasError(false);
        return $this;
    }

    /**
     * Adds error information to the quote item.
     * Automatically sets error flag.
     *
     * @param string|null $origin Usually a name of module, that embeds error
     * @param int|null $code Error code, unique for origin, that sets it
     * @param string|null $message Error message
     * @param Varien_Object|null $additionalData Any additional data, that caller would like to store
     * @return Mage_Sales_Model_Quote_Item
     */
    public function addErrorInfo($origin = null, $code = null, $message = null, $additionalData = null)
    {
        $this->_errorInfos->addItem($origin, $code, $message, $additionalData);
        if ($message !== null) {
            $this->setMessage($message);
        }
        $this->_setHasError(true);

        return $this;
    }

    /**
     * Retrieves all error infos, associated with this item
     *
     * @return array
     */
    public function getErrorInfos()
    {
        return $this->_errorInfos->getItems();
    }

    /**
     * Removes error infos, that have parameters equal to passed in $params.
     * $params can have following keys (if not set - then any item is good for this key):
     *   'origin', 'code', 'message'
     *
     * @param array $params
     * @return Mage_Sales_Model_Quote_Item
     */
    public function removeErrorInfosByParams($params)
    {
        $removedItems = $this->_errorInfos->removeItemsByParams($params);
        foreach ($removedItems as $item) {
            if ($item['message'] !== null) {
                $this->removeMessageByText($item['message']);
            }
        }

        if (!$this->_errorInfos->getItems()) {
            $this->_setHasError(false);
        }

        return $this;
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Sales
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Service model for managing statuses information. Statuses are just records with code, message and any
 * additional data. The model helps to keep track and manipulate statuses, that different modules want to set
 * to owner object of this model.
 *
 * @category    Mage
 * @package     Mage_Sales
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Sales_Model_Status_List
{
    /**
     * Status information entities
     *
     * @var array
     */
    protected $_items = array();

    /**
     * Adds status information to the list of items.
     *
     * @param string|null $origin Usually a name of module, that adds this status
     * @param int|null $code Code of status, unique for origin, that sets it
     * @param string|null $message Status message
     * @param Varien_Object|null $additionalData Any additional data, that caller would like to store
     * @return Mage_Sales_Model_Status_List
     */
    public function addItem($origin = null, $code = null, $message = null, $additionalData = null)
    {
        $this->_items[] = array(
            'origin' => $origin,
            'code' => $code,
            'message' => $message,
            'additionalData' => $additionalData
        );
        return $this;
    }

    /**
     * Retrieves all items
     *
     * @return array
     */
    public function getItems()
    {
        return $this->_items;
    }

    /**
     * Removes items, that have parameters equal to passed in $params.
     * Returns items removed.
     * $params can have following keys (if not set - then any item is good for this key):
     *   'origin', 'code', 'message'
     *
     * @param array $params
     * @return array
     */
    public function removeItemsByParams($params)
    {
        $items = $this->getItems();
        if (!$items) {
            return array();
        }

        $indexes = array();
        $paramKeys = array('origin', 'code', 'message');
        foreach ($items as $index => $item) {
            $remove = true;
            foreach ($paramKeys as $key) {
                if (!isset($params[$key])) {
                    continue;
                }
                if ($params[$key] != $item[$key]) {
                    $remove = false;
                    break;
                }
            }
            if ($remove) {
                $indexes[] = $index;
            }
        }

        return $this->removeItems($indexes);
    }

    /**
     * Removes items at mentioned index/indexes.
     * Returns items removed.
     *
     * @param int|array $indexes
     * @return array
     */
    public function removeItems($indexes)
    {
        if (!array($indexes)) {
            $indexes = array($indexes);
        }
        if (!$indexes) {
            return array();
        }

        $items = $this->getItems();
        if (!$items) {
            return array();
        }

        $newItems = array();
        $removedItems = array();
        foreach ($items as $indexNow => $item) {
            if (in_array($indexNow, $indexes)) {
                $removedItems[] = $item;
            } else {
                $newItems[] = $item;
            }
        }

        $this->_items = $newItems;
        return $removedItems;
    }

    /**
     * Clears list from all items
     *
     * @return Mage_Sales_Model_Status_List
     */
    public function clear()
    {
        $this->_items = array();
        return $this;
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Sales
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Quote resource model
 *
 * @category    Mage
 * @package     Mage_Sales
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Sales_Model_Resource_Quote_Item extends Mage_Sales_Model_Resource_Abstract
{
    /**
     * Main table and field initialization
     *
     */
    protected function _construct()
    {
        $this->_init('sales/quote_item', 'item_id');
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Sales
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Quote item resource collection
 *
 * @category    Mage
 * @package     Mage_Sales
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Sales_Model_Resource_Quote_Item_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    /**
     * Collection quote instance
     *
     * @var Mage_Sales_Model_Quote
     */
    protected $_quote;

    /**
     * Product Ids array
     *
     * @var array
     */
    protected $_productIds   = array();

    /**
     * Initialize resource model
     *
     */
    protected function _construct()
    {
        $this->_init('sales/quote_item');
    }

    /**
     * Retrieve store Id (From Quote)
     *
     * @return int
     */
    public function getStoreId()
    {
        return (int)$this->_quote->getStoreId();
    }

    /**
     * Set Quote object to Collection
     *
     * @param Mage_Sales_Model_Quote $quote
     * @return Mage_Sales_Model_Resource_Quote_Item_Collection
     */
    public function setQuote($quote)
    {
        $this->_quote = $quote;
        $quoteId      = $quote->getId();
        if ($quoteId) {
            $this->addFieldToFilter('quote_id', $quote->getId());
        } else {
            $this->_totalRecords = 0;
            $this->_setIsLoaded(true);
        }
        return $this;
    }

    /**
     * Reset the collection and inner join it to quotes table
     * Optionally can select items with specified product id only
     *
     * @param string $quotesTableName
     * @param int $productId
     * @return Mage_Sales_Model_Resource_Quote_Item_Collection
     */
    public function resetJoinQuotes($quotesTableName, $productId = null)
    {
        $this->getSelect()->reset()
            ->from(
                array('qi' => $this->getResource()->getMainTable()),
                array('item_id', 'qty', 'quote_id'))
            ->joinInner(
                array('q' => $quotesTableName),
               'qi.quote_id = q.entity_id',
                array('store_id', 'items_qty', 'items_count')
            );
        if ($productId) {
            $this->getSelect()->where('qi.product_id = ?', (int)$productId);
        }
        return $this;
    }

    /**
     * After load processing
     *
     * @return Mage_Sales_Model_Resource_Quote_Item_Collection
     */
    protected function _afterLoad()
    {
        parent::_afterLoad();

        /**
         * Assign parent items
         */
        foreach ($this as $item) {
            if ($item->getParentItemId()) {
                $item->setParentItem($this->getItemById($item->getParentItemId()));
            }
            if ($this->_quote) {
                $item->setQuote($this->_quote);
            }
        }

        /**
         * Assign options and products
         */
        $this->_assignOptions();
        $this->_assignProducts();
        $this->resetItemsDataChanged();

        return $this;
    }

    /**
     * Add options to items
     *
     * @return Mage_Sales_Model_Resource_Quote_Item_Collection
     */
    protected function _assignOptions()
    {
        $itemIds          = array_keys($this->_items);
        $optionCollection = Mage::getModel('sales/quote_item_option')->getCollection()
            ->addItemFilter($itemIds);
        foreach ($this as $item) {
            $item->setOptions($optionCollection->getOptionsByItem($item));
        }
        $productIds        = $optionCollection->getProductIds();
        $this->_productIds = array_merge($this->_productIds, $productIds);

        return $this;
    }

    /**
     * Add products to items and item options
     *
     * @return Mage_Sales_Model_Resource_Quote_Item_Collection
     */
    protected function _assignProducts()
    {
        Varien_Profiler::start('QUOTE:'.__METHOD__);
        $productFlatHelper = Mage::helper('catalog/product_flat');
        $productFlatHelper->disableFlatCollection();

        $productIds = array();
        foreach ($this as $item) {
            $productIds[] = (int)$item->getProductId();
        }
        $this->_productIds = array_merge($this->_productIds, $productIds);

        $productCollection = Mage::getModel('catalog/product')->getCollection()
            ->setStoreId($this->getStoreId())
            ->addIdFilter($this->_productIds)
            ->addAttributeToSelect(Mage::getSingleton('sales/quote_config')->getProductAttributes())
            ->addOptionsToResult()
            ->addStoreFilter()
            ->addUrlRewrite()
            ->addTierPriceData();

        Mage::dispatchEvent('prepare_catalog_product_collection_prices', array(
            'collection'            => $productCollection,
            'store_id'              => $this->getStoreId(),
        ));
        Mage::dispatchEvent('sales_quote_item_collection_products_after_load', array(
            'product_collection'    => $productCollection
        ));

        $recollectQuote = false;
        foreach ($this as $item) {
            $product = $productCollection->getItemById($item->getProductId());
            if ($product) {
                $product->setCustomOptions(array());
                $qtyOptions         = array();
                $optionProductIds   = array();
                foreach ($item->getOptions() as $option) {
                    /**
                     * Call type-specific logic for product associated with quote item
                     */
                    $product->getTypeInstance(true)->assignProductToOption(
                        $productCollection->getItemById($option->getProductId()),
                        $option,
                        $product
                    );

                    if (is_object($option->getProduct()) && $option->getProduct()->getId() != $product->getId()) {
                        $optionProductIds[$option->getProduct()->getId()] = $option->getProduct()->getId();
                    }
                }

                if ($optionProductIds) {
                    foreach ($optionProductIds as $optionProductId) {
                        $qtyOption = $item->getOptionByCode('product_qty_' . $optionProductId);
                        if ($qtyOption) {
                            $qtyOptions[$optionProductId] = $qtyOption;
                        }
                    }
                }

                $item->setQtyOptions($qtyOptions)->setProduct($product);
            } else {
                $item->isDeleted(true);
                $recollectQuote = true;
            }
            $item->checkData();
        }

        if ($recollectQuote && $this->_quote) {
            $this->_quote->collectTotals();
        }

        $productFlatHelper->resetFlatCollection();
        Varien_Profiler::stop('QUOTE:'.__METHOD__);
        return $this;
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Rule
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Abstract Rule entity resource model
 *
 * @category Mage
 * @package Mage_Rule
 * @author Magento Core Team <core@magentocommerce.com>
 */
abstract class Mage_Rule_Model_Resource_Abstract extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Store associated with rule entities information map
     *
     * Example:
     * array(
     *    'entity_type1' => array(
     *        'associations_table' => 'table_name',
     *        'rule_id_field'      => 'rule_id',
     *        'entity_id_field'    => 'entity_id'
     *    ),
     *    'entity_type2' => array(
     *        'associations_table' => 'table_name',
     *        'rule_id_field'      => 'rule_id',
     *        'entity_id_field'    => 'entity_id'
     *    )
     *    ....
     * )
     *
     * @var array
     */
    protected $_associatedEntitiesMap = array();

    /**
     * Prepare rule's active "from" and "to" dates
     *
     * @param Mage_Core_Model_Abstract $object
     *
     * @return Mage_Rule_Model_Resource_Abstract
     */
    public function _beforeSave(Mage_Core_Model_Abstract $object)
    {
        $fromDate = $object->getFromDate();
        if ($fromDate instanceof Zend_Date) {
            $object->setFromDate($fromDate->toString(Varien_Date::DATETIME_INTERNAL_FORMAT));
        } elseif (!is_string($fromDate) || empty($fromDate)) {
            $object->setFromDate(null);
        }

        $toDate = $object->getToDate();
        if ($toDate instanceof Zend_Date) {
            $object->setToDate($toDate->toString(Varien_Date::DATETIME_INTERNAL_FORMAT));
        } elseif (!is_string($toDate) || empty($toDate)) {
            $object->setToDate(null);
        }

        parent::_beforeSave($object);
        return $this;
    }

    /**
     * Prepare select for condition
     *
     * @param int $storeId
     * @param Mage_Rule_Model_Condition_Abstract $condition
     * @return Varien_Db_Select
     */
    public function getProductFlatSelect($storeId, $condition)
    {
        $select = $this->_getReadAdapter()->select();
        $select->from(
                array('p' => $this->getTable('catalog/product')),
                array(new Zend_Db_Expr('DISTINCT p.entity_id'))
            )
            ->joinInner(
                array('cpf' => $this->getTable('catalog/product_flat') . '_' . $storeId),
                'cpf.entity_id = p.entity_id',
                array()
            )->joinLeft(
                array('ccp' => $this->getTable('catalog/category_product')),
                'ccp.product_id = p.entity_id',
                array()
            );

        $where = $condition->prepareConditionSql();
        if (!empty($where)) {
            $select->where($where);
        }

        return $select;
    }

    /**
     * Bind specified rules to entities
     *
     * @param array|int|string $ruleIds
     * @param array|int|string $entityIds
     * @param string $entityType
     * @param bool $deleteOldResults
     *
     * @throws Exception
     * @return Mage_Rule_Model_Resource_Abstract
     */
    public function bindRuleToEntity($ruleIds, $entityIds, $entityType, $deleteOldResults = true)
    {
        if (empty($ruleIds) || empty($entityIds)) {
            return $this;
        }
        $adapter    = $this->_getWriteAdapter();
        $entityInfo = $this->_getAssociatedEntityInfo($entityType);

        if (!is_array($ruleIds)) {
            $ruleIds = array((int) $ruleIds);
        }
        if (!is_array($entityIds)) {
            $entityIds = array((int) $entityIds);
        }

        $data  = array();
        $count = 0;

        $adapter->beginTransaction();

        try {
            foreach ($ruleIds as $ruleId) {
                foreach ($entityIds as $entityId) {
                    $data[] = array(
                        $entityInfo['entity_id_field'] => $entityId,
                        $entityInfo['rule_id_field'] => $ruleId
                    );
                    $count++;
                    if (($count % 1000) == 0) {
                        $adapter->insertOnDuplicate(
                            $this->getTable($entityInfo['associations_table']),
                            $data,
                            array($entityInfo['rule_id_field'])
                        );
                        $data = array();
                    }
                }
            }
            if (!empty($data)) {
                $adapter->insertOnDuplicate(
                    $this->getTable($entityInfo['associations_table']),
                    $data,
                    array($entityInfo['rule_id_field'])
                );
            }

            if ($deleteOldResults) {
                $adapter->delete($this->getTable($entityInfo['associations_table']),
                    $adapter->quoteInto($entityInfo['rule_id_field']   . ' IN (?) AND ', $ruleIds) .
                    $adapter->quoteInto($entityInfo['entity_id_field'] . ' NOT IN (?)',  $entityIds)
                );
            }
        } catch (Exception $e) {
            $adapter->rollback();
            throw $e;

        }

        $adapter->commit();

        return $this;
    }

    /**
     * Unbind specified rules from entities
     *
     * @param array|int|string $ruleIds
     * @param array|int|string $entityIds
     * @param string $entityType
     *
     * @return Mage_Rule_Model_Resource_Abstract
     */
    public function unbindRuleFromEntity($ruleIds = array(), $entityIds = array(), $entityType)
    {
        $writeAdapter = $this->_getWriteAdapter();
        $entityInfo   = $this->_getAssociatedEntityInfo($entityType);

        if (!is_array($entityIds)) {
            $entityIds = array((int) $entityIds);
        }
        if (!is_array($ruleIds)) {
            $ruleIds = array((int) $ruleIds);
        }

        $where = array();
        if (!empty($ruleIds)) {
            $where[] = $writeAdapter->quoteInto($entityInfo['rule_id_field'] . ' IN (?)', $ruleIds);
        }
        if (!empty($entityIds)) {
            $where[] = $writeAdapter->quoteInto($entityInfo['entity_id_field'] . ' IN (?)', $entityIds);
        }

        $writeAdapter->delete($this->getTable($entityInfo['associations_table']), implode(' AND ', $where));

        return $this;
    }

    /**
     * Retrieve rule's associated entity Ids by entity type
     *
     * @param int $ruleId
     * @param string $entityType
     *
     * @return array
     */
    public function getAssociatedEntityIds($ruleId, $entityType)
    {
        $entityInfo = $this->_getAssociatedEntityInfo($entityType);

        $select = $this->_getReadAdapter()->select()
            ->from($this->getTable($entityInfo['associations_table']), array($entityInfo['entity_id_field']))
            ->where($entityInfo['rule_id_field'] . ' = ?', $ruleId);

        return $this->_getReadAdapter()->fetchCol($select);
    }

    /**
     * Retrieve website ids of specified rule
     *
     * @param int $ruleId
     * @return array
     */
    public function getWebsiteIds($ruleId)
    {
        return $this->getAssociatedEntityIds($ruleId, 'website');
    }

    /**
     * Retrieve customer group ids of specified rule
     *
     * @param int $ruleId
     * @return array
     */
    public function getCustomerGroupIds($ruleId)
    {
        return $this->getAssociatedEntityIds($ruleId, 'customer_group');
    }

    /**
     * Retrieve correspondent entity information (associations table name, columns names)
     * of rule's associated entity by specified entity type
     *
     * @param string $entityType
     *
     * @return array
     */
    protected function _getAssociatedEntityInfo($entityType)
    {
        if (isset($this->_associatedEntitiesMap[$entityType])) {
            return $this->_associatedEntitiesMap[$entityType];
        }

        $e = Mage::exception(
            'Mage_Core',
            Mage::helper('rule')->__('There is no information about associated entity type "%s".', $entityType)
        );
        throw $e;
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_CatalogRule
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Catalog rules resource model
 *
 * @category    Mage
 * @package     Mage_CatalogRule
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_CatalogRule_Model_Resource_Rule extends Mage_Rule_Model_Resource_Abstract
{
    /**
     * Store number of seconds in a day
     */
    const SECONDS_IN_DAY = 86400;

    /**
     * Number of products in range for insert
     */
    const RANGE_PRODUCT_STEP = 1000000;

    /**
     * Store associated with rule entities information map
     *
     * @var array
     */
    protected $_associatedEntitiesMap = array(
        'website' => array(
            'associations_table' => 'catalogrule/website',
            'rule_id_field'      => 'rule_id',
            'entity_id_field'    => 'website_id'
        ),
        'customer_group' => array(
            'associations_table' => 'catalogrule/customer_group',
            'rule_id_field'      => 'rule_id',
            'entity_id_field'    => 'customer_group_id'
        )
    );

    /**
     * Factory instance
     *
     * @var Mage_Core_Model_Factory
     */
    protected $_factory;

    /**
     * App instance
     *
     * @var Mage_Core_Model_App
     */
    protected $_app;

    /**
     * Constructor with parameters
     * Array of arguments with keys
     *  - 'factory' Mage_Core_Model_Factory
     *
     * @param array $args
     */
    public function __construct(array $args = array())
    {
        $this->_factory = !empty($args['factory']) ? $args['factory'] : Mage::getSingleton('core/factory');
        $this->_app     = !empty($args['app']) ? $args['app'] : Mage::app();

        parent::__construct();
    }

    /**
     * Initialize main table and table id field
     */
    protected function _construct()
    {
        $this->_init('catalogrule/rule', 'rule_id');
    }

    /**
     * Add customer group ids and website ids to rule data after load
     *
     * @param Mage_Core_Model_Abstract $object
     *
     * @return Mage_CatalogRule_Model_Resource_Rule
     */
    protected function _afterLoad(Mage_Core_Model_Abstract $object)
    {
        $object->setData('customer_group_ids', (array)$this->getCustomerGroupIds($object->getId()));
        $object->setData('website_ids', (array)$this->getWebsiteIds($object->getId()));

        return parent::_afterLoad($object);
    }

    /**
     * Bind catalog rule to customer group(s) and website(s).
     * Update products which are matched for rule.
     *
     * @param Mage_Core_Model_Abstract $object
     *
     * @return Mage_CatalogRule_Model_Resource_Rule
     */
    protected function _afterSave(Mage_Core_Model_Abstract $object)
    {
        if ($object->hasWebsiteIds()) {
            $websiteIds = $object->getWebsiteIds();
            if (!is_array($websiteIds)) {
                $websiteIds = explode(',', (string)$websiteIds);
            }
            $this->bindRuleToEntity($object->getId(), $websiteIds, 'website');
        }

        if ($object->hasCustomerGroupIds()) {
            $customerGroupIds = $object->getCustomerGroupIds();
            if (!is_array($customerGroupIds)) {
                $customerGroupIds = explode(',', (string)$customerGroupIds);
            }
            $this->bindRuleToEntity($object->getId(), $customerGroupIds, 'customer_group');
        }

        parent::_afterSave($object);
        return $this;
    }

    /**
     * Deletes records in catalogrule/product_data by rule ID and product IDs
     *
     * @param int $ruleId
     * @param array $productIds
     */
    public function cleanProductData($ruleId, array $productIds = array())
    {
        /** @var $write Varien_Db_Adapter_Interface */
        $write = $this->_getWriteAdapter();

        $conditions = array('rule_id = ?' => $ruleId);

        if (count($productIds) > 0) {
            $conditions['product_id IN (?)'] = $productIds;
        }

        $write->delete($this->getTable('catalogrule/rule_product'), $conditions);
    }

    /**
     * Return whether the product fits the rule
     *
     * @param Mage_CatalogRule_Model_Rule $rule
     * @param Varien_Object $product
     * @param array $websiteIds
     * @return bool
     */
    public function validateProduct(Mage_CatalogRule_Model_Rule $rule, Varien_Object $product, $websiteIds = array())
    {
        /** @var $helper Mage_Catalog_Helper_Product_Flat */
        $helper = $this->_factory->getHelper('catalog/product_flat');
        if ($helper->isEnabled() && $helper->isBuiltAllStores()) {
            /** @var $store Mage_Core_Model_Store */
            foreach ($this->_app->getStores(false) as $store) {
                if (count($websiteIds) == 0 || in_array($store->getWebsiteId(), $websiteIds)) {
                    /** @var $selectByStore Varien_Db_Select */
                    $selectByStore = $rule->getProductFlatSelect($store->getId());
                    $selectByStore->where('p.entity_id = ?', $product->getId());
                    $selectByStore->limit(1);
                    if ($this->_getReadAdapter()->fetchOne($selectByStore)) {
                        return true;
                    }
                }
            }
            return false;
        } else {
            return $rule->getConditions()->validate($product);
        }
    }

    /**
     * Inserts rule data into catalogrule/rule_product table
     *
     * @param Mage_CatalogRule_Model_Rule $rule
     * @param array $websiteIds
     * @param array $productIds
     */
    public function insertRuleData(Mage_CatalogRule_Model_Rule $rule, array $websiteIds, array $productIds = array())
    {
        /** @var $write Varien_Db_Adapter_Interface */
        $write = $this->_getWriteAdapter();

        $customerGroupIds = $rule->getCustomerGroupIds();

        $fromTime = (int) strtotime($rule->getFromDate());
        $toTime = (int) strtotime($rule->getToDate());
        $toTime = $toTime ? ($toTime + self::SECONDS_IN_DAY - 1) : 0;

        /** @var Mage_Core_Model_Date $coreDate */
        $coreDate  = $this->_factory->getModel('core/date');
        $timestamp = $coreDate->gmtTimestamp('Today');
        if ($fromTime > $timestamp
            || ($toTime && $toTime < $timestamp)
        ) {
            return;
        }
        $sortOrder = (int) $rule->getSortOrder();
        $actionOperator = $rule->getSimpleAction();
        $actionAmount = (float) $rule->getDiscountAmount();
        $subActionOperator = $rule->getSubIsEnable() ? $rule->getSubSimpleAction() : '';
        $subActionAmount = (float) $rule->getSubDiscountAmount();
        $actionStop = (int) $rule->getStopRulesProcessing();
        /** @var $helper Mage_Catalog_Helper_Product_Flat */
        $helper = $this->_factory->getHelper('catalog/product_flat');

        if ($helper->isEnabled() && $helper->isBuiltAllStores()) {
            /** @var $store Mage_Core_Model_Store */
            foreach ($this->_app->getStores(false) as $store) {
                if (in_array($store->getWebsiteId(), $websiteIds)) {
                    /** @var $selectByStore Varien_Db_Select */
                    $selectByStore = $rule->getProductFlatSelect($store->getId())
                        ->joinLeft(array('cg' => $this->getTable('customer/customer_group')),
                            $write->quoteInto('cg.customer_group_id IN (?)', $customerGroupIds),
                            array('cg.customer_group_id'))
                        ->reset(Varien_Db_Select::COLUMNS)
                        ->columns(array(
                            new Zend_Db_Expr($store->getWebsiteId()),
                            'cg.customer_group_id',
                            'p.entity_id',
                            new Zend_Db_Expr($rule->getId()),
                            new Zend_Db_Expr($fromTime),
                            new Zend_Db_Expr($toTime),
                            new Zend_Db_Expr("'" . $actionOperator . "'"),
                            new Zend_Db_Expr($actionAmount),
                            new Zend_Db_Expr($actionStop),
                            new Zend_Db_Expr($sortOrder),
                            new Zend_Db_Expr("'" . $subActionOperator . "'"),
                            new Zend_Db_Expr($subActionAmount),
                        ));

                    if (count($productIds) > 0) {
                        $selectByStore->where('p.entity_id IN (?)', array_keys($productIds));
                    }

                    $selects = $write->selectsByRange('entity_id', $selectByStore, self::RANGE_PRODUCT_STEP);
                    foreach ($selects as $select) {
                        $write->query(
                            $write->insertFromSelect(
                                $select, $this->getTable('catalogrule/rule_product'), array(
                                    'website_id',
                                    'customer_group_id',
                                    'product_id',
                                    'rule_id',
                                    'from_time',
                                    'to_time',
                                    'action_operator',
                                    'action_amount',
                                    'action_stop',
                                    'sort_order',
                                    'sub_simple_action',
                                    'sub_discount_amount',
                                ), Varien_Db_Adapter_Interface::INSERT_IGNORE
                            )
                        );
                    }
                }
            }
        } else {
            if (count($productIds) == 0) {
                Varien_Profiler::start('__MATCH_PRODUCTS__');
                $productIds = $rule->getMatchingProductIds();
                Varien_Profiler::stop('__MATCH_PRODUCTS__');
            }

            $rows = array();
            foreach ($productIds as $productId => $validationByWebsite) {
                foreach ($websiteIds as $websiteId) {
                    foreach ($customerGroupIds as $customerGroupId) {
                        if (empty($validationByWebsite[$websiteId])) {
                            continue;
                        }
                        $rows[] = array(
                            'rule_id'             => $rule->getId(),
                            'from_time'           => $fromTime,
                            'to_time'             => $toTime,
                            'website_id'          => $websiteId,
                            'customer_group_id'   => $customerGroupId,
                            'product_id'          => $productId,
                            'action_operator'     => $actionOperator,
                            'action_amount'       => $actionAmount,
                            'action_stop'         => $actionStop,
                            'sort_order'          => $sortOrder,
                            'sub_simple_action'   => $subActionOperator,
                            'sub_discount_amount' => $subActionAmount,
                        );

                        if (count($rows) == 1000) {
                            $write->insertMultiple($this->getTable('catalogrule/rule_product'), $rows);
                            $rows = array();
                        }
                    }
                }
            }

            if (!empty($rows)) {
                $write->insertMultiple($this->getTable('catalogrule/rule_product'), $rows);
            }
        }
    }

    /**
     * Update products which are matched for rule
     *
     * @param Mage_CatalogRule_Model_Rule $rule
     *
     * @throws Exception
     * @return Mage_CatalogRule_Model_Resource_Rule
     */
    public function updateRuleProductData(Mage_CatalogRule_Model_Rule $rule)
    {
        $ruleId = $rule->getId();
        $write  = $this->_getWriteAdapter();
        $write->beginTransaction();
        if ($rule->getProductsFilter()) {
            $this->cleanProductData($ruleId, $rule->getProductsFilter());
        } else {
            $this->cleanProductData($ruleId);
        }

        if (!$rule->getIsActive()) {
            $write->commit();
            return $this;
        }

        $websiteIds = $rule->getWebsiteIds();
        if (!is_array($websiteIds)) {
            $websiteIds = explode(',', $websiteIds);
        }
        if (empty($websiteIds)) {
            return $this;
        }

        try {
            $this->insertRuleData($rule, $websiteIds);
            $write->commit();
        } catch (Exception $e) {
            $write->rollback();
            throw $e;
        }

        return $this;
    }

    /**
     * Get all product ids matched for rule
     *
     * @param int $ruleId
     *
     * @return array
     */
    public function getRuleProductIds($ruleId)
    {
        $read = $this->_getReadAdapter();
        $select = $read->select()->from($this->getTable('catalogrule/rule_product'), 'product_id')
            ->where('rule_id=?', $ruleId);

        return $read->fetchCol($select);
    }

    /**
     * Remove catalog rules product prices for specified date range and product
     *
     * @param int|string $fromDate
     * @param int|string $toDate
     * @param int|null $productId
     *
     * @return Mage_CatalogRule_Model_Resource_Rule
     */
    public function removeCatalogPricesForDateRange($fromDate, $toDate, $productId = null)
    {
        $write = $this->_getWriteAdapter();
        $conds = array();
        $cond = $write->quoteInto('rule_date between ?', $this->formatDate($fromDate));
        $cond = $write->quoteInto($cond.' and ?', $this->formatDate($toDate));
        $conds[] = $cond;
        if (!is_null($productId)) {
            $conds[] = $write->quoteInto('product_id=?', $productId);
        }

        /**
         * Add information about affected products
         * It can be used in processes which related with product price (like catalog index)
         */
        $select = $this->_getWriteAdapter()->select()
            ->from($this->getTable('catalogrule/rule_product_price'), 'product_id')
            ->where(implode(' AND ', $conds))
            ->group('product_id');

        $replace = $write->insertFromSelect(
            $select,
            $this->getTable('catalogrule/affected_product'),
            array('product_id'),
            true
        );
        $write->query($replace);
        $write->delete($this->getTable('catalogrule/rule_product_price'), $conds);
        return $this;
    }

    /**
     * Delete old price rules data
     *
     * @param string $date
     * @param int|null $productId
     *
     * @return Mage_CatalogRule_Model_Resource_Rule
     */
    public function deleteOldData($date, $productId = null)
    {
        $write = $this->_getWriteAdapter();
        $conds = array();
        $conds[] = $write->quoteInto('rule_date<?', $this->formatDate($date));
        if (!is_null($productId)) {
            $conds[] = $write->quoteInto('product_id=?', $productId);
        }
        $write->delete($this->getTable('catalogrule/rule_product_price'), $conds);
        return $this;
    }

    /**
     * Get DB resource statement for processing query result
     *
     * @param int $fromDate
     * @param int $toDate
     * @param int|null $productId
     * @param int|null $websiteId
     *
     * @return Zend_Db_Statement_Interface
     */
    protected function _getRuleProductsStmt($fromDate, $toDate, $productId = null, $websiteId = null)
    {
        $read = $this->_getReadAdapter();
        /**
         * Sort order is important
         * It used for check stop price rule condition.
         * website_id   customer_group_id   product_id  sort_order
         *  1           1                   1           0
         *  1           1                   1           1
         *  1           1                   1           2
         * if row with sort order 1 will have stop flag we should exclude
         * all next rows for same product id from price calculation
         */
        $select = $read->select()
            ->from(array('rp' => $this->getTable('catalogrule/rule_product')))
            ->where($read->quoteInto('rp.from_time = 0 or rp.from_time <= ?', $toDate)
            . ' OR ' . $read->quoteInto('rp.to_time = 0 or rp.to_time >= ?', $fromDate))
            ->order(array('rp.website_id', 'rp.customer_group_id', 'rp.product_id', 'rp.sort_order', 'rp.rule_id'));

        if (!is_null($productId)) {
            $select->where('rp.product_id=?', $productId);
        }

        /**
         * Join default price and websites prices to result
         */
        $priceAttr  = Mage::getSingleton('eav/config')->getAttribute(Mage_Catalog_Model_Product::ENTITY, 'price');
        $priceTable = $priceAttr->getBackend()->getTable();
        $attributeId= $priceAttr->getId();

        $joinCondition = '%1$s.entity_id=rp.product_id AND (%1$s.attribute_id=' . $attributeId
            . ') and %1$s.store_id=%2$s';

        $select->join(
            array('pp_default'=>$priceTable),
            sprintf($joinCondition, 'pp_default', Mage_Core_Model_App::ADMIN_STORE_ID),
            array('default_price'=>'pp_default.value')
        );

        if ($websiteId !== null) {
            $website  = Mage::app()->getWebsite($websiteId);
            $defaultGroup = $website->getDefaultGroup();
            if ($defaultGroup instanceof Mage_Core_Model_Store_Group) {
                $storeId = $defaultGroup->getDefaultStoreId();
            } else {
                $storeId = Mage_Core_Model_App::ADMIN_STORE_ID;
            }

            $select->joinInner(
                array('product_website' => $this->getTable('catalog/product_website')),
                'product_website.product_id=rp.product_id ' .
                'AND rp.website_id=product_website.website_id ' .
                'AND product_website.website_id='.$websiteId,
                array()
            );

            $tableAlias = 'pp'.$websiteId;
            $fieldAlias = 'website_'.$websiteId.'_price';
            $select->joinLeft(
                array($tableAlias=>$priceTable),
                sprintf($joinCondition, $tableAlias, $storeId),
                array($fieldAlias=>$tableAlias.'.value')
            );
        } else {
            foreach (Mage::app()->getWebsites() as $website) {
                $websiteId  = $website->getId();
                $defaultGroup = $website->getDefaultGroup();
                if ($defaultGroup instanceof Mage_Core_Model_Store_Group) {
                    $storeId = $defaultGroup->getDefaultStoreId();
                } else {
                    $storeId = Mage_Core_Model_App::ADMIN_STORE_ID;
                }

                $tableAlias = 'pp' . $websiteId;
                $fieldAlias = 'website_' . $websiteId . '_price';
                $select->joinLeft(
                    array($tableAlias => $priceTable),
                    sprintf($joinCondition, $tableAlias, $storeId),
                    array($fieldAlias => $tableAlias.'.value')
                );
            }
        }

        return $read->query($select);
    }

    /**
     * Generate catalog price rules prices for specified date range
     * If from date is not defined - will be used previous day by UTC
     * If to date is not defined - will be used next day by UTC
     *
     * @param int|Mage_Catalog_Model_Product $product
     *
     * @throws Exception
     * @return Mage_CatalogRule_Model_Resource_Rule
     */
    public function applyAllRules($product = null)
    {
        $this->_reindexCatalogRule($product);
        return $this;
    }

    /**
     * Generate catalog price rules prices for specified date range
     * If from date is not defined - will be used previous day by UTC
     * If to date is not defined - will be used next day by UTC
     *
     * @param int|string|null $fromDate
     * @param int|string|null $toDate
     * @param int $productId
     *
     * @deprecated after 1.7.0.2 use method applyAllRules
     *
     * @return Mage_CatalogRule_Model_Resource_Rule
     */
    public function applyAllRulesForDateRange($fromDate = null, $toDate = null, $productId = null)
    {
        return $this->applyAllRules($productId);
    }

    /**
     * Run reindex
     *
     * @param int|Mage_Catalog_Model_Product $product
     */
    protected function _reindexCatalogRule($product = null)
    {
        $indexerCode = 'catalogrule/action_index_refresh';
        $value = null;
        if ($product) {
            $value = $product instanceof Mage_Catalog_Model_Product ? $product->getId() : $product;
            $indexerCode = 'catalogrule/action_index_refresh_row';
        }

        /** @var $indexer Mage_CatalogRule_Model_Action_Index_Refresh */
        $indexer = Mage::getModel(
            $indexerCode,
            array(
                'connection' => $this->_getWriteAdapter(),
                'factory'    => Mage::getModel('core/factory'),
                'resource'   => $this,
                'app'        => Mage::app(),
                'value'      => $value
            )
        );
        $indexer->execute();
    }

    /**
     * Calculate product price based on price rule data and previous information
     *
     * @param array $ruleData
     * @param null|array $productData
     *
     * @return float
     */
    protected function _calcRuleProductPrice($ruleData, $productData = null)
    {
        if ($productData !== null && isset($productData['rule_price'])) {
            $productPrice = $productData['rule_price'];
        } else {
            $websiteId = $ruleData['website_id'];
            if (isset($ruleData['website_'.$websiteId.'_price'])) {
                $productPrice = $ruleData['website_'.$websiteId.'_price'];
            } else {
                $productPrice = $ruleData['default_price'];
            }
        }

        $productPrice = Mage::helper('catalogrule')->calcPriceRule(
            $ruleData['action_operator'],
            $ruleData['action_amount'],
            $productPrice);

        return Mage::app()->getStore()->roundPrice($productPrice);
    }

    /**
     * Save rule prices for products to DB
     *
     * @param array $arrData
     *
     * @return Mage_CatalogRule_Model_Resource_Rule
     */
    protected function _saveRuleProductPrices($arrData)
    {
        if (empty($arrData)) {
            return $this;
        }

        $adapter    = $this->_getWriteAdapter();
        $productIds = array();

        $adapter->beginTransaction();
        try {
            foreach ($arrData as $key => $data) {
                $productIds['product_id'] = $data['product_id'];
                $arrData[$key]['rule_date'] = $this->formatDate($data['rule_date'], false);
                $arrData[$key]['latest_start_date'] = $this->formatDate($data['latest_start_date'], false);
                $arrData[$key]['earliest_end_date'] = $this->formatDate($data['earliest_end_date'], false);
            }
            $adapter->insertOnDuplicate($this->getTable('catalogrule/affected_product'), array_unique($productIds));
            $adapter->insertOnDuplicate($this->getTable('catalogrule/rule_product_price'), $arrData);

        } catch (Exception $e) {
            $adapter->rollback();
            throw $e;

        }
        $adapter->commit();

        return $this;
    }

    /**
     * Get catalog rules product price for specific date, website and
     * customer group
     *
     * @param int|string $date
     * @param int $wId
     * @param int $gId
     * @param int $pId
     *
     * @return float|bool
     */
    public function getRulePrice($date, $wId, $gId, $pId)
    {
        $data = $this->getRulePrices($date, $wId, $gId, array($pId));
        if (isset($data[$pId])) {
            return $data[$pId];
        }

        return false;
    }

    /**
     * Retrieve product prices by catalog rule for specific date, website and customer group
     * Collect data with  product Id => price pairs
     *
     * @param int|string $date
     * @param int $websiteId
     * @param int $customerGroupId
     * @param array $productIds
     *
     * @return array
     */
    public function getRulePrices($date, $websiteId, $customerGroupId, $productIds)
    {
        $adapter = $this->_getReadAdapter();
        $select  = $adapter->select()
            ->from($this->getTable('catalogrule/rule_product_price'), array('product_id', 'rule_price'))
            ->where('rule_date = ?', $this->formatDate($date, false))
            ->where('website_id = ?', $websiteId)
            ->where('customer_group_id = ?', $customerGroupId)
            ->where('product_id IN(?)', $productIds);
        return $adapter->fetchPairs($select);
    }

    /**
     * Get active rule data based on few filters
     *
     * @param int|string $date
     * @param int $websiteId
     * @param int $customerGroupId
     * @param int $productId
     * @return array
     */
    public function getRulesFromProduct($date, $websiteId, $customerGroupId, $productId)
    {
        $adapter = $this->_getReadAdapter();
        if (is_string($date)) {
            $date = strtotime($date);
        }
        $select = $adapter->select()
            ->from($this->getTable('catalogrule/rule_product'))
            ->where('website_id = ?', $websiteId)
            ->where('customer_group_id = ?', $customerGroupId)
            ->where('product_id = ?', $productId)
            ->where('from_time = 0 or from_time < ?', $date)
            ->where('to_time = 0 or to_time > ?', $date)
            ->order('sort_order');

        return $adapter->fetchAll($select);
    }

    /**
     * Retrieve product price data for all customer groups
     *
     * @param int|string $date
     * @param int $wId
     * @param int $pId
     *
     * @return array
     */
    public function getRulesForProduct($date, $wId, $pId)
    {
        $read = $this->_getReadAdapter();
        $select = $read->select()
            ->from($this->getTable('catalogrule/rule_product_price'), '*')
            ->where('rule_date=?', $this->formatDate($date, false))
            ->where('website_id=?', $wId)
            ->where('product_id=?', $pId);

        return $read->fetchAll($select);
    }

    /**
     * Apply catalog rule to product
     *
     * @param Mage_CatalogRule_Model_Rule $rule
     * @param Mage_Catalog_Model_Product $product
     * @param array $websiteIds
     *
     * @throws Exception
     * @return Mage_CatalogRule_Model_Resource_Rule
     */
    public function applyToProduct($rule, $product, $websiteIds)
    {
        if (!$rule->getIsActive()) {
            return $this;
        }

        $ruleId    = $rule->getId();
        $productId = $product->getId();

        $write = $this->_getWriteAdapter();
        $write->beginTransaction();

        if ($this->_isProductMatchedRule($ruleId, $product)) {
            $this->cleanProductData($ruleId, array($productId));
        }
        if ($this->validateProduct($rule, $product, $websiteIds)) {
            try {
                $this->insertRuleData($rule, $websiteIds, array(
                    $productId => array_combine(array_values($websiteIds), array_values($websiteIds)))
                );
            } catch (Exception $e) {
                $write->rollback();
                throw $e;
            }
        } else {
            $write->delete($this->getTable('catalogrule/rule_product_price'), array(
                $write->quoteInto('product_id = ?', $productId),
            ));
        }

        $write->commit();
        return $this;
    }

    /**
     * Get ids of matched rules for specific product
     *
     * @param int $productId
     * @return array
     */
    public function getProductRuleIds($productId)
    {
        $read = $this->_getReadAdapter();
        $select = $read->select()->from($this->getTable('catalogrule/rule_product'), 'rule_id');
        $select->where('product_id = ?', $productId);
        return array_flip($read->fetchCol($select));
    }

    /**
     * Is product has been matched the rule
     *
     * @param int $ruleId
     * @param Mage_Catalog_Model_Product $product
     * @return bool
     */
    protected function _isProductMatchedRule($ruleId, $product)
    {
        $rules = $product->getMatchedRules();
        return isset($rules[$ruleId]);
    }
}
/**
 * MageWorx
 * Admin Order Editor extension
 *
 * @category   MageWorx
 * @package    MageWorx_OrdersEdit
 * @copyright  Copyright (c) 2016 MageWorx (http://www.mageworx.com/)
 */

class MageWorx_OrdersEdit_Model_Observer
{

    /** Before edit order set old price
     *
     * @param $observer
     * @return $this
     */
    public function convertOrderItemToQuoteItem($observer)
    {
        $helper = $this->getMwHelper();
        if (!$helper->isEnabled()) {
            return $this;
        }

        /** @var Mage_Sales_Model_Quote_Item $quoteItem */
        $quoteItem = $observer->getEvent()->getQuoteItem();

        // fix for magento 1620-1700:
        $shippingAddress = $quoteItem->getQuote()->getShippingAddress();
        if ($shippingAddress) {
            $shippingAddress->setSameAsBilling(0);
        }

        // KeepPurchasePrice
        /** @var Mage_Sales_Model_Order_Item $orderItem */
        $orderItem = $observer->getEvent()->getOrderItem();
        $storeId = $orderItem->getOrder()->getStoreId();
        $store = Mage::app()->getStore($storeId);


        $oldQuoteItemId = $orderItem->getQuoteItemId();

        $oldPrice = $orderItem->getPrice();
        if (Mage::helper('tax')->priceIncludesTax($store)) {
            $oldPrice = $orderItem->getOriginalPrice();
        }

        /** @var Mage_Core_Model_Resource $coreResource */
        $coreResource = Mage::getSingleton('core/resource');
        $read = $coreResource->getConnection('core_read');

        if ($orderItem->getProductType() != 'bundle' && $oldQuoteItemId > 0) {
            $select = $read->select()
                ->from($coreResource->getTableName('sales_flat_quote_item'), 'original_custom_price')
                ->where('item_id = ?', $oldQuoteItemId);
            $originalCustomPrice = $read->fetchOne($select);
            if ($originalCustomPrice) {
                $oldPrice = $originalCustomPrice;
            }
        }

        if ($orderItem->getProductType() == 'configurable') {
            $productId = $orderItem->getProductId();
            $itemPrice = $quoteItem->getParentItem()->getProduct()->getPriceModel()->getFinalPrice(1, $quoteItem->getParentItem()->getProduct());
            $items = $quoteItem->getQuote()->getItemsCollection();
            foreach ($items as $item) {
                if ($item->getProduct()->getId() == $productId && !$item->getApplyPriceFlag()) {
                    if ($oldPrice != $itemPrice) {
                        $item->setCustomPrice($oldPrice)->setOriginalCustomPrice($oldPrice);
                    }

                    $item->setApplyPriceFlag(true); // mark item
                }
            }

            return $this;
        } elseif ($orderItem->getProductType() == 'bundle') {
            // prepare bundle old price
            if (!$oldQuoteItemId) {
                return $this;
            }

            if ($quoteItem->getParentItem()) $quoteItem = $quoteItem->getParentItem();
            $select = $read->select()
                ->from($coreResource->getTableName('sales_flat_quote_item'), array('product_id', 'price', 'original_custom_price', 'price_incl_tax'))
                ->where('parent_item_id = ?', $oldQuoteItemId);
            $children = $read->fetchAll($select);
            if (!$children) {
                return $this;
            }

            $orderChildren = array();
            foreach ($children as $child) {
                $orderChildren[$child['product_id']] = $child;
            }

            // foreach all children and apply old price
            $children = $quoteItem->getChildren();
            if (!$children) {
                return $this;
            }

            foreach ($children as $child) {
                if (isset($orderChildren[$child->getProductId()])) {
                    $orderChild = $orderChildren[$child->getProductId()];
                    if (Mage::helper('tax')->priceIncludesTax($store)) {
                        $oldPrice = $orderChild['price_incl_tax'];
                    } else {
                        $oldPrice = $orderChild['price'];
                    }

                    $oldPrice = $orderChild['original_custom_price'] ? $orderChild['original_custom_price'] : $oldPrice;
                    if ($oldPrice != $child->getProduct()->getFinalPrice()) {
                        $child->setCustomPrice($oldPrice)->setOriginalCustomPrice($oldPrice);
                    }
                }
            }

            return $this;
        }

        // simple
        if ($oldPrice != $quoteItem->getProduct()->getFinalPrice()) {
            $quoteItem->setCustomPrice($oldPrice)->setOriginalCustomPrice($oldPrice);
        }

    }

    /** Before edit order collectShippingRates
     *
     * @param $observer
     * @return $this
     */
    public function convertOrderToQuote($observer)
    {
        $helper = $this->getMwHelper();
        if (!$helper->isEnabled()) {
            return $this;
        }

        /** @var Mage_Sales_Model_Order $order */
        $order = $observer->getEvent()->getOrder();
        /** @var Mage_Sales_Model_Quote $quote */
        $quote = $observer->getEvent()->getQuote();

        $billing = $order->getBillingAddress();
        $shipping = $order->getShippingAddress();

        // set same_as_billing = yes/no
        if ($shipping) {
            if ($billing->getFirstname() == $shipping->getFirstname()
                && $billing->getMiddlename() == $shipping->getMiddlename()
                && $billing->getSuffix() == $shipping->getSuffix()
                && $billing->getCompany() == $shipping->getCompany()
                && $billing->getStreet() == $shipping->getStreet()
                && $billing->getCity() == $shipping->getCity()
                && $billing->getRegion() == $shipping->getRegion()
                && $billing->getRegionId() == $shipping->getRegionId()
                && $billing->getPostcode() == $shipping->getPostcode()
                && $billing->getCountryId() == $shipping->getCountryId()
                && $billing->getTelephone() == $shipping->getTelephone()
                && $billing->getFax() == $shipping->getFax()
            ) {
                $shipping->setSameAsBilling(1);
                Mage::getSingleton('adminhtml/sales_order_create')->getShippingAddress()->setSameAsBilling(1);
            } else {
                Mage::getSingleton('adminhtml/sales_order_create')->setShippingAsBilling(0);
            }
        }

        $store = Mage::getSingleton('adminhtml/session_quote')->getStore();
        if (Mage::helper('tax')->shippingPriceIncludesTax($store)) {
            $baseShippingAmount = $order->getBaseShippingInclTax();
        } else {
            $baseShippingAmount = $order->getBaseShippingAmount();
        }

        if (!Mage::getSingleton('adminhtml/session_quote')->getBaseShippingCustomPrice()) {
            Mage::getSingleton('adminhtml/session_quote')->setBaseShippingCustomPrice($baseShippingAmount);
        }

        // for collectShippingRates
        $quote->setTotalsCollectedFlag(false);
    }


    /**
     * @param $observer
     */
    public function orderCreateProcessData($observer)
    {
        $request = $observer->getEvent()->getRequest();
        if (isset($request['order']['shipping_price'])) {
            $shippingPrice = $request['order']['shipping_price'];
            if ($shippingPrice == 'null') {
                $shippingPrice = null;
            } else {
                $shippingPrice = floatval($shippingPrice);
            }

            Mage::getSingleton('adminhtml/session_quote')->setBaseShippingCustomPrice($shippingPrice);
        }

        // if no cancel reset_shipping - recollectShippingRates
        Mage::getSingleton('adminhtml/sales_order_create')->collectShippingRates();
    }

    /** Edit order set old coupone
     *
     * @param $observer
     * @return $this
     */
    public function quoteCollectTotalsAfter($observer)
    {
        if (!$this->getMwHelper()->isEnabled()) {
            return $this;
        }

        /** @var Mage_Sales_Model_Quote $quote */
        $quote = $observer->getEvent()->getQuote();

        // apply custom shipping price
        if ($this->getMwHelper()->isShippingPriceEditEnabled() && Mage::app()->getStore()->isAdmin()) {
            $address = $quote->getShippingAddress();
            $baseShippingCustomPrice = Mage::getSingleton('adminhtml/session_quote')->getBaseShippingCustomPrice();
            if ($address && !is_null($baseShippingCustomPrice)) {
                if ($address->getShippingMethod()) {
                    $origBaseShippingInclTax = $address->getBaseShippingInclTax();
                    $origShippingInclTax = $address->getShippingInclTax();

                    $address->setBaseTotalAmount('shipping', $baseShippingCustomPrice);
                    $shippingCustomPrice = $quote->getStore()->convertPrice($baseShippingCustomPrice);
                    $address->setTotalAmount('shipping', $shippingCustomPrice);

                    $creditModel = null;
                    $address->setAppliedTaxesReset(false);

                    foreach ($address->getTotalCollector()->getCollectors() as $code => $model) {
                        // for calculate shipping tax
                        if ($code == 'tax_shipping' || $code == 'tax') {
                            $model->collect($address);
                        }

                        if ($code == 'customercredit') {
                            $creditModel = $model;
                        }
                    }

                    $address->setGrandTotal((float)$address->getGrandTotal() + ($address->getShippingInclTax() - $origShippingInclTax));
                    $address->setBaseGrandTotal((float)$address->getBaseGrandTotal() + ($address->getBaseShippingInclTax() - $origBaseShippingInclTax));

                    // for recollect customer credit and authorizenet in admin
                    if ($creditModel && $address->getBaseCustomerCreditAmount() > 0) {
                        $baseCreditLeft = $address->getBaseCustomerCreditAmount();
                        $creditLeft = $address->getCustomerCreditAmount();
                        $address->setBaseGrandTotal($address->getBaseGrandTotal() + $baseCreditLeft);
                        $address->setGrandTotal($address->getGrandTotal() + $creditLeft);
                        $creditModel->collect($address);
                    }
                }
            }
        }

        if (Mage::helper('mageworx_ordersedit')->isFreeShippingCouponCalculationEnabled()) {
            $quote = $this->applyFreeShippingCartRule($quote);
        }

        // apply old coupon_code
        $orderId = Mage::getSingleton('adminhtml/session_quote')->getOrderId();
        if (!$orderId) {
            return $this;
        }

        /** @var Mage_Sales_Model_Order $order */
        $order = Mage::getModel('sales/order')->load($orderId);
        if (!$order->getId()) {
            return $this;
        }

        if (!$order->getAppliedRuleIds()) {
            return $this;
        }

        if (!$quote->getCouponCode() && !Mage::getSingleton('adminhtml/session_quote')->getCouponCodeIsDeleted() && $order->getCouponCode()) {
            $quote->setCouponCode($order->getCouponCode());

            /** @var Mage_Sales_Model_Quote_Address $address */
            foreach ($quote->getAllAddresses() as $address) {
                $amount = $address->getDiscountAmount();
                if ($amount != 0) {
                    $description = $order->getDiscountDescription();
                    $address->setCouponCode($order->getCouponCode())->setDiscountDescription($description);
                }
            }
        }

        return $this;
    }

    /** Add coupon block after order items block
     *  (for order view page)
     *
     * @param Varien_Event_Observer $observer
     */
    public function insertCouponBlock($observer)
    {

        /** @var Varien_Object $transport */
        $transport = $observer->getTransport();
        /** @var Mage_Core_Block_Abstract $block */
        $block = $observer->getBlock();
        if($block->getType() == 'adminhtml/sales_order_view_items' && $block->getNameInLayout() == 'order_items')
        {
            /** @var string $oldHtml */
            $oldHtml = $transport->getHtml();

            /** @var string $couponsBlockHtml */
            $couponsBlockHtml = Mage::getSingleton('core/layout')
                ->createBlock('mageworx_ordersedit/adminhtml_sales_order_coupons', 'coupons')
                ->toHtml();

            /** @var string $newHtml */
            $newHtml = $oldHtml . $couponsBlockHtml; // append coupon block html
            $transport->setHtml($newHtml);
        }

        return;
    }

    /** Reset all changes of the order from session on load of the order page
     *
     * @param Varien_Event_Observer $observer
     */
    public function resetSessionEditChanges($observer)
    {
        $request = $observer->getEvent()->getControllerAction()->getRequest()->getParams();
        if (isset($request['order_id']) && Mage::getModel('sales/order')->load($request['order_id'])->getId()) {
            $orderId = $request['order_id'];
            Mage::helper('mageworx_ordersedit/edit')->resetPendingChanges($orderId);
            $order = Mage::getModel('sales/order')->load($orderId);
            Mage::helper('mageworx_ordersedit/edit')->removeTempQuoteItems($order);
        }

        return;
    }

    /**
     * Save modified order tax information
     *
     * @param Varien_Event_Observer $observer
     */
    public function salesOrderAfterSave($observer)
    {
        //modified version of Mage_Tax_Model_Observer::salesEventOrderAfterSave()
        $order = $observer->getEvent()->getOrder();

        if ($order->getAppliedTaxIsSaved()) {
            return;
        }

        $quote = Mage::getModel('sales/quote')->loadByIdWithoutStore($order->getQuoteId());
        $address = $quote->getShippingAddress();
        $orderTaxes = Mage::getModel('tax/sales_order_tax')->getCollection()->loadByOrder($order);
        foreach ($orderTaxes->getItems() as $orderTax) {
            $orderTax->delete();
        }

        $getTaxesForItems   = $address->getQuote()->getTaxesForItems();
        $taxes              = $address->getAppliedTaxes();

        $ratesIdQuoteItemId = array();
        if (!is_array($getTaxesForItems)) {
            $getTaxesForItems = array();
        }

        foreach ($getTaxesForItems as $quoteItemId => $taxesArray) {
            foreach ($taxesArray as $rates) {
                if (count($rates['rates']) == 1) {
                    $ratesIdQuoteItemId[$rates['id']][] = array(
                        'id'        => $quoteItemId,
                        'percent'   => $rates['percent'],
                        'code'      => $rates['rates'][0]['code']
                    );
                } else {
                    $percentDelta   = $rates['percent'];
                    $percentSum     = 0;
                    foreach ($rates['rates'] as $rate) {
                        $ratesIdQuoteItemId[$rates['id']][] = array(
                            'id'        => $quoteItemId,
                            'percent'   => $rate['percent'],
                            'code'      => $rate['code']
                        );
                        $percentSum += $rate['percent'];
                    }

                    if ($percentDelta != $percentSum) {
                        $delta = $percentDelta - $percentSum;
                        foreach ($ratesIdQuoteItemId[$rates['id']] as &$rateTax) {
                            if ($rateTax['id'] == $quoteItemId) {
                                $rateTax['percent'] = (($rateTax['percent'] / $percentSum) * $delta)
                                    + $rateTax['percent'];
                            }
                        }
                    }
                }
            }
        }

        if (!is_array($taxes)) {
            $taxes = array();
        }

        foreach ($taxes as $id => $row) {
            foreach ($row['rates'] as $tax) {
                if (is_null($row['percent'])) {
                    $baseRealAmount = $row['base_amount'];
                } else {
                    if ($row['percent'] == 0 || $tax['percent'] == 0) {
                        continue;
                    }

                    $baseRealAmount = $row['base_amount'] / $row['percent'] * $tax['percent'];
                }

                $hidden = (isset($row['hidden']) ? $row['hidden'] : 0);
                $data = array(
                    'order_id'          => $order->getId(),
                    'code'              => $tax['code'],
                    'title'             => $tax['title'],
                    'hidden'            => $hidden,
                    'percent'           => $tax['percent'],
                    'priority'          => $tax['priority'],
                    'position'          => $tax['position'],
                    'amount'            => $row['amount'],
                    'base_amount'       => $row['base_amount'],
                    'process'           => $row['process'],
                    'base_real_amount'  => $baseRealAmount,
                );

                $data['process'] = !empty($row['process']) ? $row['process'] : 0;

                $result = Mage::getModel('tax/sales_order_tax')->setData($data)->save();

                if (isset($ratesIdQuoteItemId[$id])) {
                    foreach ($ratesIdQuoteItemId[$id] as $quoteItemId) {
                        if ($quoteItemId['code'] == $tax['code']) {
                            $item = $order->getItemByQuoteItemId($quoteItemId['id']);
                            if ($item) {
                                $data = array(
                                    'item_id'       => $item->getId(),
                                    'tax_id'        => $result->getTaxId(),
                                    'tax_percent'   => $quoteItemId['percent']
                                );
                                Mage::getModel('tax/sales_order_tax_item')->setData($data)->save();
                            }
                        }
                    }
                }
            }
        }

        $order->setAppliedTaxIsSaved(true);
    }

    /**
     * Save mageworx order status history
     *
     * @param Varien_Event_Observer $observer
     */
    public function salesOrderStatusHistoryAfterSave($observer)
    {
        /** @var Mage_Sales_Model_Order_Status_History $history */
        $history = $observer->getEvent()->getStatusHistory();
        Mage::getModel('mageworx_ordersedit/order_status_history')->updateHistory($history);
    }

    /**
     * @return MageWorx_OrdersEdit_Helper_Data
     */
    protected function getMwHelper()
    {
        return Mage::helper('mageworx_ordersedit');
    }

    /** Check and apply free shipping shopping cart rule if needed
     * @return Mage_Sales_Model_Quote
     */
    protected function applyFreeShippingCartRule($quote)
    {
        //check if shipping block was edited
        if (!Mage::getSingleton('adminhtml/session')->getShippingEdited()) {
            $appliedRuleIds = explode(',', $quote->getAppliedRuleIds());
            $rules =  Mage::getModel('salesrule/rule')->getCollection()->addFieldToFilter('rule_id', array('in' => $appliedRuleIds));
            foreach ($rules as $rule) {
                //check and apply free shipping shopping cart rule
                if($rule->getSimpleFreeShipping() == Mage_SalesRule_Model_Rule::FREE_SHIPPING_ADDRESS) {
                    $address = $quote->getShippingAddress();
                    $address->setGrandTotal((float)$address->getGrandTotal() - ($address->getTotalAmount('shipping') + $address->getShippingTaxAmount()));
                    $address->setBaseGrandTotal((float)$address->getBaseGrandTotal() - ($address->getBaseTotalAmount('shipping') + $address->getBaseShippingTaxAmount()));

                    $address->setBaseTotalAmount('shipping', 0);
                    $address->setTotalAmount('shipping', 0);
                    $address->setBaseShippingInclTax(0);
                    $address->setShippingInclTax(0);
                    $address->setBaseShippingTaxAmount(0);
                    $address->setShippingTaxAmount(0);
                    break;
                }
            }
        }

        Mage::getSingleton('adminhtml/session')->setShippingEdited(false);
        return $quote;
    }
}

/**
 * MageWorx
 * Admin Order Editor extension
 *
 * @category   MageWorx
 * @package    MageWorx_OrdersEdit
 * @copyright  Copyright (c) 2016 MageWorx (http://www.mageworx.com/)
 */
class MageWorx_OrdersEdit_Helper_Data extends Mage_Core_Helper_Abstract
{

    const XML_ENABLED = 'mageworx_ordersmanagement/ordersedit/enabled';

    const XML_HIDE_EDIT_BUTTON = 'mageworx_ordersmanagement/ordersedit/hide_edit_button';
    const XML_SEND_INVOICE_EMAIL = 'mageworx_ordersmanagement/ordersedit/send_invoice_email';

    const XML_HIDE_DELETED_ORDERS_FOR_CUSTOMERS = 'mageworx_ordersmanagement/ordersedit/hide_deleted_orders_for_customers';
    const XML_ENABLE_DELETE_ORDERS_COMPLETELY = 'mageworx_ordersmanagement/ordersedit/enable_delete_orders_completely';

    const XML_GRID_COLUMNS = 'mageworx_ordersmanagement/ordersedit/grid_columns';
    const XML_CUSTOMER_GRID_COLUMNS = 'mageworx_ordersmanagement/ordersedit/customer_grid_columns';

    const XML_SEND_UPDATE_EMAIL = 'mageworx_ordersmanagement/ordersedit/send_update_email';
    const XML_ENABLE_SHIPPING_PRICE_EDITION = 'mageworx_ordersmanagement/ordersedit/enable_shipping_price_edition';
    const XML_ENABLE_SHIPPING_COST_RECALCULATION = 'mageworx_ordersmanagement/ordersedit/enable_shipping_cost_recalculation';
    const XML_SHOW_ALL_STATES_IN_HISTORY = 'mageworx_ordersmanagement/ordersedit/show_all_states_in_history';
    const XML_ENABLE_AUTO_INVOICE = 'mageworx_ordersmanagement/ordersedit/enable_auto_invoice';
    const XML_FREE_SHIPPING_COUPON_DISABLED = 'mageworx_ordersmanagement/ordersedit/disable_free_shipping';

    protected $_contentType = 'application/octet-stream';
    protected $_resourceFile = null;
    protected $_handle = null;


    public function isEnabled()
    {
        return Mage::getStoreConfigFlag(self::XML_ENABLED);
    }

    public function isHideEditButton()
    {
        return Mage::getStoreConfigFlag(self::XML_HIDE_EDIT_BUTTON);
    }

    public function isShippingCostRecalculationEnabled()
    {
        return Mage::getStoreConfigFlag(self::XML_ENABLE_SHIPPING_COST_RECALCULATION);
    }

    public function isShippingPriceEditEnabled()
    {
        return Mage::getStoreConfigFlag(self::XML_ENABLE_SHIPPING_PRICE_EDITION);
    }

    public function isSendInvoiceEmail()
    {
        return Mage::getStoreConfigFlag(self::XML_SEND_INVOICE_EMAIL);
    }

    public function isHideDeletedOrdersForCustomers()
    {
        return Mage::getStoreConfigFlag(self::XML_HIDE_DELETED_ORDERS_FOR_CUSTOMERS);
    }

    public function isEnableDeleteOrdersCompletely()
    {
        return Mage::getStoreConfigFlag(self::XML_ENABLE_DELETE_ORDERS_COMPLETELY);
    }

    public function isSendUpdateEmail()
    {
        return Mage::getStoreConfig(self::XML_SEND_UPDATE_EMAIL);
    }

    public function isFreeShippingCouponCalculationEnabled()
    {
        return !Mage::getStoreConfigFlag(self::XML_FREE_SHIPPING_COUPON_DISABLED);
    }

    /**
     * Check if all states' statuses need to be shown in history
     * @return int
     */
    public function isNeedToShowAllStates()
    {
        return Mage::getStoreConfigFlag(self::XML_SHOW_ALL_STATES_IN_HISTORY);
    }

    /**
     * @return array|mixed
     */
    public function getGridColumns()
    {
        $listColumns = Mage::getStoreConfig(self::XML_GRID_COLUMNS);
        $listColumns = explode(',', $listColumns);
        return $listColumns;
    }

    /**
     * @return array|mixed
     */
    public function getCustomerGridColumns()
    {
        $listColumns = Mage::getStoreConfig(self::XML_CUSTOMER_GRID_COLUMNS);
        $listColumns = explode(',', $listColumns);
        return $listColumns;
    }

    /**
     * @return int
     */
    public function getNumberComments()
    {
        return intval(Mage::getStoreConfig('mageworx_ordersmanagement/ordersedit/number_comments'));
    }

    public function isShowThumbnails()
    {
        return Mage::getStoreConfig('mageworx_ordersmanagement/ordersedit/show_thumbnails');
    }

    public function getThumbnailHeight()
    {
        return Mage::getStoreConfig('mageworx_ordersmanagement/ordersedit/thumbnail_height');
    }

    /**
     * Is auto invoice and auto refund enabled
     *
     * @return bool
     */
    public function isAutoInvoice()
    {
        return Mage::getStoreConfigFlag(self::XML_ENABLE_AUTO_INVOICE);
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @return bool
     */
    public function isOrderEditable(Mage_Sales_Model_Order $order)
    {
        /** @see MageWorx_OrdersSurcharge_Model_Observer_Order::checkIsOrderEditable() */
        if (Mage::getSingleton('adminhtml/session')->getBlockEditOrder()) {
            return false;
        }

        return true;
    }

    /** Return count of deleted orders
     *
     * @param $orderIds
     * @return int
     */
    public function deleteOrderCompletely($orderIds)
    {
        foreach ($orderIds as $orderId) {
            $this->deleteOrderCompletelyById($orderId);
        }

        return count($orderIds);
    }

    /**
     * @param Mage_Sales_Model_Order|int $order
     * @throws Exception
     */
    public function deleteOrderCompletelyById($order)
    {
        /** @var Mage_Core_Model_Resource $coreResource */
        $coreResource = Mage::getSingleton('core/resource');
        $write = $coreResource->getConnection('core_write');
        if (is_object($order)) {
            $orderId = $order->getId();
        } else {
            /** @var Mage_Sales_Model_Order $order */
            $order = Mage::getModel('sales/order')->load(intval($order), 'entity_id');
            $orderId = $order->getId();
        }

        if ($orderId) {
            // cancel
            try {
                $order->cancel()->save();
            } catch (Exception $e) {
            }

            // delete            
            if ($order->getQuoteId()) {
                $write->query("DELETE FROM `" . $coreResource->getTableName('sales_flat_quote') . "` WHERE `entity_id`=" . $order->getQuoteId());
                $write->query("DELETE FROM `" . $coreResource->getTableName('sales_flat_quote_address') . "` WHERE `quote_id`=" . $order->getQuoteId());
                $write->query("DELETE FROM `" . $coreResource->getTableName('sales_flat_quote_item') . "` WHERE `quote_id`=" . $order->getQuoteId());
                $write->query("DELETE FROM `" . $coreResource->getTableName('sales_flat_quote_payment') . "` WHERE `quote_id`=" . $order->getQuoteId());
            }

            $order->delete();
            $write->query("DELETE FROM `" . $coreResource->getTableName('sales_flat_order_grid') . "` WHERE `entity_id`=" . $orderId);
            $write->query("DELETE FROM `" . $coreResource->getTableName('sales_flat_order_address') . "` WHERE `parent_id`=" . $orderId);
            $write->query("DELETE FROM `" . $coreResource->getTableName('sales_flat_order_item') . "` WHERE `order_id`=" . $orderId);
            $write->query("DELETE FROM `" . $coreResource->getTableName('sales_flat_order_payment') . "` WHERE `parent_id`=" . $orderId);
            $write->query("DELETE FROM `" . $coreResource->getTableName('sales_payment_transaction') . "` WHERE `order_id`=" . $orderId);
            $write->query("DELETE FROM `" . $coreResource->getTableName('sales_flat_order_status_history') . "` WHERE `parent_id`=" . $orderId);

            $write->query("DELETE FROM `" . $coreResource->getTableName('sales_flat_invoice') . "` WHERE `order_id`=" . $orderId);
            $write->query("DELETE FROM `" . $coreResource->getTableName('sales_flat_creditmemo') . "` WHERE `order_id`=" . $orderId);
            $write->query("DELETE FROM `" . $coreResource->getTableName('sales_flat_shipment') . "` WHERE `order_id`=" . $orderId);
            $write->query("DELETE FROM `" . $coreResource->getTableName('sales_order_tax') . "` WHERE `order_id`=" . $orderId);


            if (Mage::getConfig()->getModuleConfig('AW_Booking')->is('active', true)) {
                $write->query("DELETE FROM `" . $coreResource->getTableName('aw_booking_orders') . "` WHERE `order_id`=" . $orderId);
            }
        }
    }

    /**
     * @param $fileId
     * @param bool|false $createFolder
     * @return string
     */
    public function getUploadFilesPath($fileId, $createFolder = false)
    {
        // 3 byte -> 8 chars
        $fileId = '00000000' . $fileId;
        $fileId = substr($fileId, strlen($fileId) - 8, 8);
        $dir = substr($fileId, 0, 5);
        $file = substr($fileId, 5);

        $catalog = Mage::getBaseDir('media') . DS . 'ordersedit' . DS;

        if ($createFolder && !file_exists($catalog)) {
            mkdir($catalog);
        }

        if ($createFolder && !file_exists($catalog . $dir . DS)) {
            mkdir($catalog . $dir . DS);
        }

        return $catalog . $dir . DS . $file;
    }

    /**
     * @param $fileId
     * @return null|string
     */
    public function isUploadFile($fileId)
    {
        $file = $this->getUploadFilesPath($fileId, false);
        if (file_exists($file)) {
            return $file;
        } else {
            return null;
        }
    }

    /**
     * For frontend
     *
     * @param $fileId
     * @param $fileName
     * @return string
     */
    public function getUploadFilesUrl($fileId, $fileName)
    {
        return $this->_getUrl('mageworx_ordersedit/dl/file', array('id' => $fileId, 'filename' => $fileName));
    }

    /**
     * For admin
     *
     * @param $fileId
     * @param $fileName
     * @return string
     */
    public function getAdminUploadFilesUrl($fileId, $fileName)
    {
        return Mage::getModel('adminhtml/url')->getUrl('adminhtml/mageworx_ordersedit_dl/file', array('id' => $fileId, 'filename' => $fileName));
    }

    /**
     * @param $size
     * @return string
     */
    public function prepareFileSize($size)
    {

        if ($size >= 1048576) {
            return round($size / 1048576, 2) . ' ' . $this->__('MB');
        } elseif ($size >= 1024) {
            return round($size / 1024, 2) . ' ' . $this->__('KB');
        } else {
            return $size . ' ' . $this->__('B');
        }
    }

    /**
     * @return null|Varien_Io_File
     * @throws Exception
     * @throws Mage_Core_Exception
     */
    protected function _getHandle()
    {
        if (!$this->_resourceFile) {
            Mage::throwException($this->__('Please set resource file and link type'));
        }

        if (is_null($this->_handle)) {
            $this->_handle = new Varien_Io_File();
            $this->_handle->open(array('path' => Mage::getBaseDir('var')));
            if (!$this->_handle->fileExists($this->_resourceFile, true)) {
                Mage::throwException($this->__('File does not exist'));
            }

            $this->_handle->streamOpen($this->_resourceFile, 'r');
        }

        return $this->_handle;
    }

    /**
     * @return Mage_Core_Model_Config_Element|string
     */
    public function getContentType()
    {
        $this->_getHandle();
        if (function_exists('mime_content_type')) {
            return mime_content_type($this->_resourceFile);
        } else {
            return $this->getFileType($this->_resourceFile);
        }
    }

    /**
     * @param $fileName
     * @return Mage_Core_Model_Config_Element|string
     */
    public function getFileType($fileName)
    {
        $ext = substr($fileName, strrpos($fileName, '.') + 1);
        $type = Mage::getConfig()->getNode('global/mime/types/x' . $ext);
        if ($type) {
            return $type;
        }

        return $this->_contentType;
    }

    /**
     * @param Mage_Sales_Model_Order $orders
     * @param bool|true $notifyCustomer
     * @param string $comment
     * @param null $filePath
     * @param null $fileName
     * @return $this
     */
    public function sendOrderUpdateEmail($orders, $notifyCustomer = true, $comment = '', $filePath = null, $fileName = null)
    {
        $storeId = $orders->getStore()->getId();

        if (!Mage::helper('sales')->canSendOrderCommentEmail($storeId)) {
            return $this;
        }

        // Get the destination email addresses to send copies to
        $copyTo = $this->_getEmails('sales_email/order_comment/copy_to', $storeId);
        $copyMethod = Mage::getStoreConfig('sales_email/order_comment/copy_method', $storeId);
        // Check if at least one recepient is found
        if (!$notifyCustomer && !$copyTo) {
            return $this;
        }

        // Retrieve corresponding email template id and customer name
        if ($orders->getCustomerIsGuest()) {
            $templateId = Mage::getStoreConfig('sales_email/order_comment/guest_template', $storeId);
            $customerName = $orders->getBillingAddress()->getName();
        } else {
            $templateId = Mage::getStoreConfig('sales_email/order_comment/template', $storeId);
            $customerName = $orders->getCustomerName();
        }

        /** @var MageWorx_OrdersEdit_Model_Core_Email_Template_Mailer $mailer */
        $mailer = Mage::getModel('mageworx_ordersedit/core_email_template_mailer');

        if ($notifyCustomer) {
            /** @var Mage_Core_Model_Email_Info $emailInfo */
            $emailInfo = Mage::getModel('core/email_info');
            $emailInfo->addTo($orders->getCustomerEmail(), $customerName);
            if ($copyTo && $copyMethod == 'bcc') {
                // Add bcc to customer email
                foreach ($copyTo as $email) {
                    $emailInfo->addBcc($email);
                }
            }

            $mailer->addEmailInfo($emailInfo);
        }

        // Email copies are sent as separated emails if their copy method is 'copy' or a customer should not be notified
        if ($copyTo && ($copyMethod == 'copy' || !$notifyCustomer)) {
            foreach ($copyTo as $email) {
                $emailInfo = Mage::getModel('core/email_info');
                $emailInfo->addTo($email);
                $mailer->addEmailInfo($emailInfo);
            }
        }

        // Set all required params and send emails
        $mailer->setSender(Mage::getStoreConfig('sales_email/order_comment/identity', $storeId));
        $mailer->setStoreId($storeId);
        $mailer->setTemplateId($templateId);
        $mailer->setTemplateParams(
            array(
                'order' => $orders,
                'comment' => $comment,
                'billing' => $orders->getBillingAddress()
            )
        );
        $mailer->send($filePath, $fileName);

        return $this;
    }

    /**
     * @param $configPath
     * @param $storeId
     * @return array|bool
     */
    protected function _getEmails($configPath, $storeId)
    {
        $data = Mage::getStoreConfig($configPath, $storeId);
        if (!empty($data)) {
            return explode(',', $data);
        }

        return false;
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @return bool
     */
    public function invoiceOrder($order)
    {
        $savedQtys = array();
        /** @var Mage_Sales_Model_Order_Item $orderItem */
        foreach ($order->getAllItems() as $orderItem) {
            if ($orderItem->getQtyToInvoice() > 0) {
                $savedQtys[$orderItem->getId()] = $orderItem->getQtyToInvoice();
            }
        }

        /** @var Mage_Sales_Model_Order_Invoice $invoice */
        $invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice($savedQtys);
        if (!$invoice->getTotalQty()) {
            return false;
        };

        $invoice->setRequestedCaptureCase('online');

        $invoice->register();

        // if send email
        $sendEmailFlag = $this->isSendInvoiceEmail();
        if ($sendEmailFlag) {
            $invoice->setEmailSent(true);
        }

        $invoice->getOrder()->setCustomerNoteNotify($sendEmailFlag);
        $invoice->getOrder()->setIsInProcess(true);

        $transactionSave = Mage::getModel('core/resource_transaction')
            ->addObject($invoice)
            ->addObject($invoice->getOrder());
        $transactionSave->save();

        // if send email
        $invoice->sendEmail($sendEmailFlag, '');

        return $invoice;
    }

    /**
     * @param $orderIds
     * @return int
     */
    public function invoiceOrderMass($orderIds)
    {
        $count = 0;
        foreach ($orderIds as $orderId) {
            $orderId = intval($orderId);
            if ($orderId > 0) {

                /** @var Mage_Sales_Model_Order $order */
                $order = Mage::getModel('sales/order')->load($orderId);
                if (!$order->getId()) {
                    continue;
                }

                if (!$order->canInvoice()) {
                    continue;
                }

                $invoice = $this->invoiceOrder($order);
                if ($invoice) {
                    $count++;
                }
            }
        }

        return $count;
    }

    /**
     * @param $orderIds
     * @param $status
     * @param string $comment
     * @param int $isVisibleOnFront
     * @param bool|false $isCustomerNotified
     * @return int
     */
    public function changeStatusOrder($orderIds, $status, $comment = '', $isVisibleOnFront = 1, $isCustomerNotified = false)
    {
        $count = 0;
        foreach ($orderIds as $orderId) {
            $orderId = intval($orderId);
            if ($orderId > 0) {
                try {
                    /** @var Mage_Sales_Model_Order $order */
                    $order = Mage::getModel('sales/order')->load($orderId);
                    if (!$order->getId()) {
                        continue;
                    }

                    $order->addStatusHistoryComment($comment, $status)
                        ->setIsVisibleOnFront($isVisibleOnFront)
                        ->setIsCustomerNotified($isCustomerNotified);

                    if ($isCustomerNotified) {
                        $comment = trim(strip_tags($comment));
                        $order->sendOrderUpdateEmail($isCustomerNotified, $comment);
                    }

                    $order->save();
                    $count++;
                } catch (Exception $e) {
                }
            }
        }

        return $count;
    }

    /** translate and QuoteEscape
     * @param $str
     * @return mixed
     */
    public function __js($str)
    {
        return $this->jsQuoteEscape(str_replace("\'", "'", $this->__($str)));
    }

    /**
     * @return array|mixed
     */
    public function getAllPaymentMethods()
    {
        if (Mage::registry('payment_methods')) {
            return Mage::registry('payment_methods');
        }

        $payments = Mage::getSingleton('payment/config')->getAllMethods();
        $methods = array();
        foreach ($payments as $paymentCode => $paymentModel) {
            $methods[$paymentCode] = Mage::getStoreConfig('payment/' . $paymentCode . '/title');
        }

        Mage::register('payment_methods', $methods);
        return $methods;
    }

    /**
     * @return array|mixed
     */
    public function getAllShippingMethods()
    {
        if (Mage::registry('shipping_methods')) {
            return Mage::registry('shipping_methods');
        }

        $carriers = Mage::getSingleton('shipping/config')->getAllCarriers();
        $methods = array();
        foreach ($carriers as $code => $carriersModel) {
            $title = Mage::getStoreConfig('carriers/' . $code . '/title');
            if ($title) {
                $methods[$code . '_' . $code] = $title;
            }
        }

        Mage::register('shipping_methods', $methods);
        return $methods;
    }

    /**
     * @return array|mixed
     */
    public function getCustomerGroups()
    {
        if (Mage::registry('customer_groups')) {
            return Mage::registry('customer_groups');
        }

        $customerGroups = Mage::getResourceModel('customer/group_collection')->load()->toOptionArray();
        $groups = array();
        foreach ($customerGroups as $data) {
            $groups[$data['value']] = $data['label'];
        }

        Mage::register('customer_groups', $groups);
        return $groups;
    }

    /**
     * @return mixed
     */
    public function getOrderGroups()
    {
        if (Mage::registry('order_groups')) {
            return Mage::registry('order_groups');
        }

        $orderGroups = Mage::getResourceModel('mageworx_ordersedit/order_group_collection')->load()->toOptionArray();
        Mage::register('order_groups', $orderGroups);
        return $orderGroups;
    }

    /**
     * @return array|mixed
     */
    public function getShippedStatuses()
    {
        if (Mage::registry('shipped_statuses')) {
            return Mage::registry('shipped_statuses');
        }

        $statuses = array('1' => $this->__('Yes'), '0' => $this->__('No'));
        Mage::register('shipped_statuses', $statuses);
        return $statuses;
    }

    /**
     * @return array|mixed
     */
    public function getEditedStatuses()
    {
        if (Mage::registry('edited_statuses')) {
            return Mage::registry('edited_statuses');
        }

        $statuses = array('1' => $this->__('Yes'), '0' => $this->__('No'));
        Mage::register('edited_statuses', $statuses);
        return $statuses;
    }

    /**
     * @param $item
     * @return Mage_Catalog_Helper_Image|null
     */
    public function getImgByItem($item)
    {
        if ((string)Mage::getConfig()->getModuleConfig('MageWorx_CustomOptions')->active == 'true') {
            $image = $this->getCustomOptionImage($item);
            if ($image) {
                return $image;
            }
        }

        $productId = $item->getProductId();
        $product = Mage::getModel('catalog/product')->setStoreId($item->getStoreId())->load($productId);

        switch ($product->getTypeId()) {
            case 'configurable':
                return $this->getImgByItemForConfigurableProduct($item, $product);
                break;

            default:
                if ($product->getThumbnail() && $product->getThumbnail() != 'no_selection') {
                    try {
                        return Mage::helper('catalog/image')->init($product, 'thumbnail');
                    } catch (Exception $e) {
                        return null;
                    }
                }
        }

        return null;
    }

    /**
     * Get option image if the image mode of the option was set as replace
     * APO comp.
     *
     * @param $item
     * @return $this|null
     */
    public function getCustomOptionImage($item)
    {
        $product = $item->getProduct();

        if (!$product) {
            return null;
        }

        if (is_null($product->getHasOptions())) {
            $product->load($product->getId());
        }

        $availableProductTypes = array(
            'simple',
            'virtual',
            'downloadable'
        );
        $productTypeAvailable = in_array($product->getTypeId(), $availableProductTypes);
        if ($productTypeAvailable && $product->getHasOptions()) {
            $post = $item->getProductOptions();
            if (!empty($post['options'])) {
                $options = $post['options'];
            } else {
                return null;
            }

            foreach ($options as $optionId => $value) {
                if (!isset($value['option_id'])) {
                    continue;
                }

                $optionModel = $product->getOptionById($value['option_id']);
                if (!$optionModel) {
                    continue;
                }

                $optionModel->setProduct($product);

                /* Process an image only if the image mode set as "replace" */
                if ($optionModel->getImageMode() != 2) {
                    continue;
                }

                switch ($optionModel->getType()) {
                    case 'drop_down':
                    case 'radio':
                    case 'checkbox':
                    case 'multiple':
                    case 'swatch':
                    case 'multiswatch':
                        if (is_array($value['option_value'])) {
                            $optionTypeIds = $value['option_value'];
                        } else {
                            $optionTypeIds = explode(',', $value['option_value']);
                        }

                        foreach ($optionTypeIds as $optionTypeId) {
                            if (!$optionTypeId) {
                                continue;
                            }

                            $images = $optionModel->getOptionValueImages($optionTypeId);
                            if ($images) {
                                foreach ($images as $index => $image) {
                                    // file
                                    if ($image['source'] == 1 && (!$optionModel->getExcludeFirstImage() || ($optionModel->getExcludeFirstImage() && $index > 0))) {
                                        // replace main image
                                        $thumb = Mage::getModel('mageworx_customoptions/catalog_product_option_image')->init($image['image_file']);
                                        return $thumb;
                                    }
                                }
                            }
                        }
                }
            }
        }

        return null;
    }

    /**
     * @param $item
     * @param $product
     * @return Mage_Catalog_Helper_Image|null
     */
    protected function getImgByItemForConfigurableProduct($item, $product)
    {
        $childrens = $item->getChildrenItems();
        $child = current($childrens);
        if ($child === false) {
            return null;
        }

        $childProductId = $child->getProductId();
        if (!$childProductId) {
            return null;
        }

        $childProduct = Mage::getModel('catalog/product')->setStoreId($item->getStoreId())->load($childProductId);
        if ($childProduct->getThumbnail() && $childProduct->getThumbnail() != 'no_selection') {
            try {
                return Mage::helper('catalog/image')->init($childProduct, 'thumbnail');
            } catch (Exception $e) {
                return null;
            }
        } elseif ($product->getThumbnail() && $product->getThumbnail() != 'no_selection') {
            try {
                return Mage::helper('catalog/image')->init($product, 'thumbnail');
            } catch (Exception $e) {
                return null;
            }
        }

        return null;
    }

    /**
     * @return bool
     */
    public function isMagentoEnterprise()
    {
        $isEnterprise = false;
        $i = Mage::getVersionInfo();
        if ($i['major'] == 1) {
            if (method_exists('Mage', 'getEdition')) {
                if (Mage::getEdition() == Mage::EDITION_ENTERPRISE) {
                    $isEnterprise = true;
                }
            } elseif ($i['minor'] > 7) {
                $isEnterprise = true;
            }
        }

        return $isEnterprise;
    }

    /**
     * @return string
     */
    public function getMagentoVersion()
    {
        $i = Mage::getVersionInfo();
        if ($i['major'] == 1 && $this->isMagentoEnterprise()) {
            $i['minor'] -= 5;
        }

        return trim("{$i['major']}.{$i['minor']}.{$i['revision']}" . ($i['patch'] != '' ? ".{$i['patch']}" : "") . "-{$i['stability']}{$i['number']}", '.-');
    }

    /**
     * Check module and class (optional)
     *
     * @param  string $module
     * @param  null|string $class
     * @return bool
     */
    public static function foeModuleCheck($module, $class = null, $rewriteClass = null)
    {
        $module = (string)$module;
        if ($module && (string)Mage::getConfig()->getModuleConfig($module)->active == 'true') {
            if ($class && $rewriteClass) {
                return is_subclass_of($class, $rewriteClass);
            } elseif ($class && !$rewriteClass) {
                return class_exists($class);
            }

            return true;
        }

        return false;
    }

    /**
     * Get admin user name
     *
     * @param MageWorx_OrdersEdit_Model_Order_Status_History $item
     *
     * @return string
     */
    public function getAdminUserName($item)
    {
        //if admin user exists, use current firstname and lastname
        if ($item->getAdminFirstname() && $item->getAdminLastname()) {
            return $item->getAdminFirstname() . ' ' . $item->getAdminLastname();
        }

        //if admin user doesn't exist, use saved data
        if ($item->getCreatorFirstname() || $item->getCreatorLastname()) {
            return $item->getCreatorFirstname() . ' ' . $item->getCreatorLastname();
        } else {
            return $item->getCreatorUsername();
        }
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Adminhtml quote session
 *
 * @category   Mage
 * @package    Mage_Adminhtml
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Adminhtml_Model_Session_Quote extends Mage_Core_Model_Session_Abstract
{
    const XML_PATH_DEFAULT_CREATEACCOUNT_GROUP = 'customer/create_account/default_group';

    /**
     * Quote model object
     *
     * @var Mage_Sales_Model_Quote
     */
    protected $_quote   = null;

    /**
     * Customer mofrl object
     *
     * @var Mage_Customer_Model_Customer
     */
    protected $_customer= null;

    /**
     * Store model object
     *
     * @var Mage_Core_Model_Store
     */
    protected $_store   = null;

    /**
     * Order model object
     *
     * @var Mage_Sales_Model_Order
     */
    protected $_order   = null;

    public function __construct()
    {
        $this->init('adminhtml_quote');
        if (Mage::app()->isSingleStoreMode()) {
            $this->setStoreId(Mage::app()->getStore(true)->getId());
        }
    }

    /**
     * Retrieve quote model object
     *
     * @return Mage_Sales_Model_Quote
     */
    public function getQuote()
    {
        if (is_null($this->_quote)) {
            $this->_quote = Mage::getModel('sales/quote');
            if ($this->getStoreId() && $this->getQuoteId()) {
                $this->_quote->setStoreId($this->getStoreId())
                    ->load($this->getQuoteId());
            }
            elseif($this->getStoreId() && $this->hasCustomerId()) {
                $this->_quote->setStoreId($this->getStoreId())
                    ->setCustomerGroupId(Mage::getStoreConfig(self::XML_PATH_DEFAULT_CREATEACCOUNT_GROUP))
                    ->assignCustomer($this->getCustomer())
                    ->setIsActive(false)
                    ->save();
                $this->setQuoteId($this->_quote->getId());
            }
            $this->_quote->setIgnoreOldQty(true);
            $this->_quote->setIsSuperMode(true);
        }
        return $this->_quote;
    }

    /**
     * Set customer model object
     * To enable quick switch of preconfigured customer
     * @param Mage_Customer_Model_Customer $customer
     * @return Mage_Adminhtml_Model_Session_Quote
     */
    public function setCustomer(Mage_Customer_Model_Customer $customer)
    {
        $this->_customer = $customer;
        return $this;
    }

/**
     * Retrieve customer model object
     * @param bool $forceReload
     * @param bool $useSetStore
     * @return Mage_Customer_Model_Customer
     */
    public function getCustomer($forceReload=false, $useSetStore=false)
    {
        if (is_null($this->_customer) || $forceReload) {
            $this->_customer = Mage::getModel('customer/customer');
            if ($useSetStore && $this->getStore()->getId()) {
                $this->_customer->setStore($this->getStore());
            }
            if ($customerId = $this->getCustomerId()) {
                $this->_customer->load($customerId);
            }
        }
        return $this->_customer;
    }

    /**
     * Retrieve store model object
     *
     * @return Mage_Core_Model_Store
     */
    public function getStore()
    {
        if (is_null($this->_store)) {
            $this->_store = Mage::app()->getStore($this->getStoreId());
            if ($currencyId = $this->getCurrencyId()) {
                $this->_store->setCurrentCurrencyCode($currencyId);
            }
        }
        return $this->_store;
    }

    /**
     * Retrieve order model object
     *
     * @return Mage_Sales_Model_Order
     */
    public function getOrder()
    {
        if (is_null($this->_order)) {
            $this->_order = Mage::getModel('sales/order');
            if ($this->getOrderId()) {
                $this->_order->load($this->getOrderId());
            }
        }
        return $this->_order;
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Sales
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Sales observer
 *
 * @category   Mage
 * @package    Mage_Sales
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Mage_Sales_Model_Observer
{
    /**
     * Expire quotes additional fields to filter
     *
     * @var array
     */
    protected $_expireQuotesFilterFields = array();

    /**
     * Clean expired quotes (cron process)
     *
     * @param Mage_Cron_Model_Schedule $schedule
     * @return Mage_Sales_Model_Observer
     */
    public function cleanExpiredQuotes($schedule)
    {
        Mage::dispatchEvent('clear_expired_quotes_before', array('sales_observer' => $this));

        $lifetimes = Mage::getConfig()->getStoresConfigByPath('checkout/cart/delete_quote_after');
        foreach ($lifetimes as $storeId=>$lifetime) {
            $lifetime *= 86400;

            /** @var $quotes Mage_Sales_Model_Mysql4_Quote_Collection */
            $quotes = Mage::getModel('sales/quote')->getCollection();

            $quotes->addFieldToFilter('store_id', $storeId);
            $quotes->addFieldToFilter('updated_at', array('to'=>date("Y-m-d", time()-$lifetime)));
            $quotes->addFieldToFilter('is_active', 0);

            foreach ($this->getExpireQuotesAdditionalFilterFields() as $field => $condition) {
                $quotes->addFieldToFilter($field, $condition);
            }

            $quotes->walk('delete');
        }
        return $this;
    }

    /**
     * Retrieve expire quotes additional fields to filter
     *
     * @return array
     */
    public function getExpireQuotesAdditionalFilterFields()
    {
        return $this->_expireQuotesFilterFields;
    }

    /**
     * Set expire quotes additional fields to filter
     *
     * @param array $fields
     * @return Mage_Sales_Model_Observer
     */
    public function setExpireQuotesAdditionalFilterFields(array $fields)
    {
        $this->_expireQuotesFilterFields = $fields;
        return $this;
    }

    /**
     * When deleting product, substract it from all quotes quantities
     *
     * @throws Exception
     * @param Varien_Event_Observer
     * @return Mage_Sales_Model_Observer
     */
    public function substractQtyFromQuotes($observer)
    {
        $product = $observer->getEvent()->getProduct();
        Mage::getResourceSingleton('sales/quote')->substractProductFromQuotes($product);
        return $this;
    }

    /**
     * When applying a catalog price rule, make related quotes recollect on demand
     *
     * @param Varien_Event_Observer $observer
     * @return Mage_Sales_Model_Observer
     */
    public function markQuotesRecollectOnCatalogRules($observer)
    {
        $product = $observer->getEvent()->getProduct();

        if (is_numeric($product)) {
            $product = Mage::getModel("catalog/product")->load($product);
        }
        if ($product instanceof Mage_Catalog_Model_Product) {
            $childrenProductList = Mage::getSingleton('catalog/product_type')->factory($product)
                ->getChildrenIds($product->getId(), false);

            $productIdList = array($product->getId());
            foreach ($childrenProductList as $groupData) {
                $productIdList = array_merge($productIdList, $groupData);
            }
        } else {
            $productIdList = null;
        }

        Mage::getResourceSingleton('sales/quote')->markQuotesRecollectByAffectedProduct($productIdList);
        return $this;
    }

    /**
     * Catalog Product After Save (change status process)
     *
     * @param Varien_Event_Observer $observer
     * @return Mage_Sales_Model_Observer
     */
    public function catalogProductSaveAfter(Varien_Event_Observer $observer)
    {
        $product = $observer->getEvent()->getProduct();
        if ($product->getStatus() == Mage_Catalog_Model_Product_Status::STATUS_ENABLED) {
            return $this;
        }

        Mage::getResourceSingleton('sales/quote')->markQuotesRecollect($product->getId());

        return $this;
    }

    /**
     * Catalog Mass Status update process
     *
     * @param Varien_Event_Observer $observer
     * @return Mage_Sales_Model_Observer
     */
    public function catalogProductStatusUpdate(Varien_Event_Observer $observer)
    {
        $status     = $observer->getEvent()->getStatus();
        if ($status == Mage_Catalog_Model_Product_Status::STATUS_ENABLED) {
            return $this;
        }
        $productId  = $observer->getEvent()->getProductId();
        Mage::getResourceSingleton('sales/quote')->markQuotesRecollect($productId);

        return $this;
    }

    /**
     * Refresh sales order report statistics for last day
     *
     * @param Mage_Cron_Model_Schedule $schedule
     * @return Mage_Sales_Model_Observer
     */
    public function aggregateSalesReportOrderData($schedule)
    {
        Mage::app()->getLocale()->emulate(0);
        $currentDate = Mage::app()->getLocale()->date();
        $date = $currentDate->subHour(25);
        Mage::getResourceModel('sales/report_order')->aggregate($date);
        Mage::app()->getLocale()->revert();
        return $this;
    }

    /**
     * Refresh sales shipment report statistics for last day
     *
     * @param Mage_Cron_Model_Schedule $schedule
     * @return Mage_Sales_Model_Observer
     */
    public function aggregateSalesReportShipmentData($schedule)
    {
        Mage::app()->getLocale()->emulate(0);
        $currentDate = Mage::app()->getLocale()->date();
        $date = $currentDate->subHour(25);
        Mage::getResourceModel('sales/report_shipping')->aggregate($date);
        Mage::app()->getLocale()->revert();
        return $this;
    }

    /**
     * Refresh sales invoiced report statistics for last day
     *
     * @param Mage_Cron_Model_Schedule $schedule
     * @return Mage_Sales_Model_Observer
     */
    public function aggregateSalesReportInvoicedData($schedule)
    {
        Mage::app()->getLocale()->emulate(0);
        $currentDate = Mage::app()->getLocale()->date();
        $date = $currentDate->subHour(25);
        Mage::getResourceModel('sales/report_invoiced')->aggregate($date);
        Mage::app()->getLocale()->revert();
        return $this;
    }

    /**
     * Refresh sales refunded report statistics for last day
     *
     * @param Mage_Cron_Model_Schedule $schedule
     * @return Mage_Sales_Model_Observer
     */
    public function aggregateSalesReportRefundedData($schedule)
    {
        Mage::app()->getLocale()->emulate(0);
        $currentDate = Mage::app()->getLocale()->date();
        $date = $currentDate->subHour(25);
        Mage::getResourceModel('sales/report_refunded')->aggregate($date);
        Mage::app()->getLocale()->revert();
        return $this;
    }

    /**
     * Refresh bestsellers report statistics for last day
     *
     * @param Mage_Cron_Model_Schedule $schedule
     * @return Mage_Sales_Model_Observer
     */
    public function aggregateSalesReportBestsellersData($schedule)
    {
        Mage::app()->getLocale()->emulate(0);
        $currentDate = Mage::app()->getLocale()->date();
        $date = $currentDate->subHour(25);
        Mage::getResourceModel('sales/report_bestsellers')->aggregate($date);
        Mage::app()->getLocale()->revert();
        return $this;
    }

    /**
     * Add the recurring profile form when editing a product
     *
     * @param Varien_Event_Observer $observer
     */
    public function prepareProductEditFormRecurringProfile($observer)
    {
        // replace the element of recurring payment profile field with a form
        $profileElement = $observer->getEvent()->getProductElement();
        $block = Mage::app()->getLayout()->createBlock('sales/adminhtml_recurring_profile_edit_form',
            'adminhtml_recurring_profile_edit_form')->setParentElement($profileElement)
            ->setProductEntity($observer->getEvent()->getProduct());
        $observer->getEvent()->getResult()->output = $block->toHtml();

        // make the profile element dependent on is_recurring
        $dependencies = Mage::app()->getLayout()->createBlock('adminhtml/widget_form_element_dependence',
            'adminhtml_recurring_profile_edit_form_dependence')->addFieldMap('is_recurring', 'product[is_recurring]')
            ->addFieldMap($profileElement->getHtmlId(), $profileElement->getName())
            ->addFieldDependence($profileElement->getName(), 'product[is_recurring]', '1')
            ->addConfigOptions(array('levels_up' => 2));
        $observer->getEvent()->getResult()->output .= $dependencies->toHtml();
    }

    /**
     * Block admin ability to use customer billing agreements
     *
     * @param Varien_Event_Observer $observer
     */
    public function restrictAdminBillingAgreementUsage($observer)
    {
        $methodInstance = $observer->getEvent()->getMethodInstance();
        if (!($methodInstance instanceof Mage_Sales_Model_Payment_Method_Billing_AgreementAbstract)) {
            return;
        }
        if (!Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/use')) {
            $observer->getEvent()->getResult()->isAvailable = false;
        }
    }

    /**
     * Set new customer group to all his quotes
     *
     * @param  Varien_Event_Observer $observer
     * @return Mage_Sales_Model_Observer
     */
    public function customerSaveAfter(Varien_Event_Observer $observer)
    {
        /** @var $customer Mage_Customer_Model_Customer */
        $customer = $observer->getEvent()->getCustomer();

        if ($customer->getGroupId() !== $customer->getOrigData('group_id')) {
            /**
             * It is needed to process customer's quotes for all websites
             * if customer accounts are shared between all of them
             */
            $websites = (Mage::getSingleton('customer/config_share')->isWebsiteScope())
                ? array(Mage::app()->getWebsite($customer->getWebsiteId()))
                : Mage::app()->getWebsites();

            /** @var $quote Mage_Sales_Model_Quote */
            $quote = Mage::getSingleton('sales/quote');

            foreach ($websites as $website) {
                $quote->setWebsite($website);
                $quote->loadByCustomer($customer);

                if ($quote->getId()) {
                    $quote->setCustomerGroupId($customer->getGroupId());
                    $quote->collectTotals();
                    $quote->save();
                }
            }
        }

        return $this;
    }

    /**
     * Set Quote information about MSRP price enabled
     *
     * @param Varien_Event_Observer $observer
     */
    public function setQuoteCanApplyMsrp(Varien_Event_Observer $observer)
    {
        /** @var $quote Mage_Sales_Model_Quote */
        $quote = $observer->getEvent()->getQuote();

        $canApplyMsrp = false;
        if (Mage::helper('catalog')->isMsrpEnabled()) {
            foreach ($quote->getAllAddresses() as $adddress) {
                if ($adddress->getCanApplyMsrp()) {
                    $canApplyMsrp = true;
                    break;
                }
            }
        }

        $quote->setCanApplyMsrp($canApplyMsrp);
    }

    /**
     * Add VAT validation request date and identifier to order comments
     *
     * @param Varien_Event_Observer $observer
     * @return null
     */
    public function addVatRequestParamsOrderComment(Varien_Event_Observer $observer)
    {
        /** @var $orderInstance Mage_Sales_Model_Order */
        $orderInstance = $observer->getOrder();
        /** @var $orderAddress Mage_Sales_Model_Order_Address */
        $orderAddress = $this->_getVatRequiredSalesAddress($orderInstance);
        if (!($orderAddress instanceof Mage_Sales_Model_Order_Address)) {
            return;
        }

        $vatRequestId = $orderAddress->getVatRequestId();
        $vatRequestDate = $orderAddress->getVatRequestDate();
        if (is_string($vatRequestId) && !empty($vatRequestId) && is_string($vatRequestDate)
            && !empty($vatRequestDate)
        ) {
            $orderHistoryComment = Mage::helper('customer')->__('VAT Request Identifier')
                . ': ' . $vatRequestId . '<br />' . Mage::helper('customer')->__('VAT Request Date')
                . ': ' . $vatRequestDate;
            $orderInstance->addStatusHistoryComment($orderHistoryComment, false);
        }
    }

    /**
     * Retrieve sales address (order or quote) on which tax calculation must be based
     *
     * @param Mage_Core_Model_Abstract $salesModel
     * @param Mage_Core_Model_Store|string|int|null $store
     * @return Mage_Customer_Model_Address_Abstract|null
     */
    protected function _getVatRequiredSalesAddress($salesModel, $store = null)
    {
        $configAddressType = Mage::helper('customer/address')->getTaxCalculationAddressType($store);
        $requiredAddress = null;
        switch ($configAddressType) {
            case Mage_Customer_Model_Address_Abstract::TYPE_SHIPPING:
                $requiredAddress = $salesModel->getShippingAddress();
                break;
            default:
                $requiredAddress = $salesModel->getBillingAddress();
        }
        return $requiredAddress;
    }

    /**
     * Retrieve customer address (default billing or default shipping) ID on which tax calculation must be based
     *
     * @param Mage_Customer_Model_Customer $customer
     * @param Mage_Core_Model_Store|string|int|null $store
     * @return int|string
     */
    protected function _getVatRequiredCustomerAddress(Mage_Customer_Model_Customer $customer, $store = null)
    {
        $configAddressType = Mage::helper('customer/address')->getTaxCalculationAddressType($store);
        $requiredAddress = null;
        switch ($configAddressType) {
            case Mage_Customer_Model_Address_Abstract::TYPE_SHIPPING:
                $requiredAddress = $customer->getDefaultShipping();
                break;
            default:
                $requiredAddress = $customer->getDefaultBilling();
        }
        return $requiredAddress;
    }

    /**
     * Handle customer VAT number if needed on collect_totals_before event of quote address
     *
     * @param Varien_Event_Observer $observer
     */
    public function changeQuoteCustomerGroupId(Varien_Event_Observer $observer)
    {
        /** @var $addressHelper Mage_Customer_Helper_Address */
        $addressHelper = Mage::helper('customer/address');

        $quoteAddress = $observer->getQuoteAddress();
        $quoteInstance = $quoteAddress->getQuote();
        $customerInstance = $quoteInstance->getCustomer();
        $isDisableAutoGroupChange = $customerInstance->getDisableAutoGroupChange();

        $storeId = $customerInstance->getStore();

        $configAddressType = Mage::helper('customer/address')->getTaxCalculationAddressType($storeId);

        // When VAT is based on billing address then Magento have to handle only billing addresses
        $additionalBillingAddressCondition = ($configAddressType == Mage_Customer_Model_Address_Abstract::TYPE_BILLING)
            ? $configAddressType != $quoteAddress->getAddressType() : false;
        // Handle only addresses that corresponds to VAT configuration
        if (!$addressHelper->isVatValidationEnabled($storeId) || $additionalBillingAddressCondition) {
            return;
        }

        /** @var $customerHelper Mage_Customer_Helper_Data */
        $customerHelper = Mage::helper('customer');

        $customerCountryCode = $quoteAddress->getCountryId();
        $customerVatNumber = $quoteAddress->getVatId();

        if ((empty($customerVatNumber) || !Mage::helper('core')->isCountryInEU($customerCountryCode))
            && !$isDisableAutoGroupChange
        ) {
            $groupId = ($customerInstance->getId()) ? $customerHelper->getDefaultCustomerGroupId($storeId)
                : Mage_Customer_Model_Group::NOT_LOGGED_IN_ID;

            $quoteAddress->setPrevQuoteCustomerGroupId($quoteInstance->getCustomerGroupId());
            $customerInstance->setGroupId($groupId);
            $quoteInstance->setCustomerGroupId($groupId);

            return;
        }

        /** @var $coreHelper Mage_Core_Helper_Data */
        $coreHelper = Mage::helper('core');
        $merchantCountryCode = $coreHelper->getMerchantCountryCode();
        $merchantVatNumber = $coreHelper->getMerchantVatNumber();

        $gatewayResponse = null;
        if ($addressHelper->getValidateOnEachTransaction($storeId)
            || $customerCountryCode != $quoteAddress->getValidatedCountryCode()
            || $customerVatNumber != $quoteAddress->getValidatedVatNumber()
        ) {
            // Send request to gateway
            $gatewayResponse = $customerHelper->checkVatNumber(
                $customerCountryCode,
                $customerVatNumber,
                ($merchantVatNumber !== '') ? $merchantCountryCode : '',
                $merchantVatNumber
            );

            // Store validation results in corresponding quote address
            $quoteAddress->setVatIsValid((int)$gatewayResponse->getIsValid())
                ->setVatRequestId($gatewayResponse->getRequestIdentifier())
                ->setVatRequestDate($gatewayResponse->getRequestDate())
                ->setVatRequestSuccess($gatewayResponse->getRequestSuccess())
                ->setValidatedVatNumber($customerVatNumber)
                ->setValidatedCountryCode($customerCountryCode)
                ->save();
        } else {
            // Restore validation results from corresponding quote address
            $gatewayResponse = new Varien_Object(array(
                'is_valid' => (int)$quoteAddress->getVatIsValid(),
                'request_identifier' => (string)$quoteAddress->getVatRequestId(),
                'request_date' => (string)$quoteAddress->getVatRequestDate(),
                'request_success' => (boolean)$quoteAddress->getVatRequestSuccess()
            ));
        }

        // Magento always has to emulate group even if customer uses default billing/shipping address
        if (!$isDisableAutoGroupChange) {
            $groupId = $customerHelper->getCustomerGroupIdBasedOnVatNumber(
                $customerCountryCode, $gatewayResponse, $customerInstance->getStore()
            );
        } else {
            $groupId = $quoteInstance->getCustomerGroupId();
        }

        if ($groupId) {
            $quoteAddress->setPrevQuoteCustomerGroupId($quoteInstance->getCustomerGroupId());
            $customerInstance->setGroupId($groupId);
            $quoteInstance->setCustomerGroupId($groupId);
        }
    }

    /**
     * Restore initial customer group ID in quote if needed on collect_totals_after event of quote address
     *
     * @param Varien_Event_Observer $observer
     */
    public function restoreQuoteCustomerGroupId($observer)
    {
        $quoteAddress = $observer->getQuoteAddress();
        $configAddressType = Mage::helper('customer/address')->getTaxCalculationAddressType();
        // Restore initial customer group ID in quote only if VAT is calculated based on shipping address
        if ($quoteAddress->hasPrevQuoteCustomerGroupId()
            && $configAddressType == Mage_Customer_Model_Address_Abstract::TYPE_SHIPPING
        ) {
            $quoteAddress->getQuote()->setCustomerGroupId($quoteAddress->getPrevQuoteCustomerGroupId());
            $quoteAddress->unsPrevQuoteCustomerGroupId();
        }
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Core
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Abstract resource model class
 *
 * @category    Mage
 * @package     Mage_Core
 * @author      Magento Core Team <core@magentocommerce.com>
 */
abstract class Mage_Core_Model_Mysql4_Abstract extends Mage_Core_Model_Resource_Db_Abstract
{
}
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Afptc
 * @version    1.1.12
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Afptc_Model_Resource_Rule extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init('awafptc/rule', 'rule_id');
    }
    
    protected function _beforeSave(Mage_Core_Model_Abstract $object)
    {
        if (is_array($object->getStoreIds()))
            $object->setStoreIds(implode(',', $object->getStoreIds()));
        
        if (is_array($object->getCustomerGroups()))
            $object->setCustomerGroups(implode(',', $object->getCustomerGroups()));
    }

    public function getActiveRulesCollection(Mage_Core_Model_Store $store)
    {
        $rulesCollection = Mage::getModel('awafptc/rule')->getCollection();
        $rulesCollection
            ->addStatusFilter()
            ->addTimeLimitFilter()
            ->addStoreFilter((int) $store->getId())
            ->addGroupFilter((int) Mage::helper('awafptc')->getCustomerGroup())
            ->addPriorityOrder()
        ;
        return $rulesCollection;
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Rule
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Abstract Rule entity data model
 *
 * @category Mage
 * @package Mage_Rule
 * @author Magento Core Team <core@magentocommerce.com>
 */
abstract class Mage_Rule_Model_Abstract extends Mage_Core_Model_Abstract
{
    /**
     * Store rule combine conditions model
     *
     * @var Mage_Rule_Model_Condition_Combine
     */
    protected $_conditions;

    /**
     * Store rule actions model
     *
     * @var Mage_Rule_Model_Action_Collection
     */
    protected $_actions;

    /**
     * Store rule form instance
     *
     * @var Varien_Data_Form
     */
    protected $_form;

    /**
     * Is model can be deleted flag
     *
     * @var bool
     */
    protected $_isDeleteable = true;

    /**
     * Is model readonly
     *
     * @var bool
     */
    protected $_isReadonly = false;

    /**
     * Getter for rule combine conditions instance
     *
     * @return Mage_Rule_Model_Condition_Combine
     */
    abstract public function getConditionsInstance();

    /**
     * Getter for rule actions collection instance
     *
     * @return Mage_Rule_Model_Action_Collection
     */
    abstract public function getActionsInstance();

    /**
     * Prepare select for condition
     *
     * @param int $storeId
     * @return Varien_Db_Select
     */
    public function getProductFlatSelect($storeId)
    {
        /** @var $resource Mage_Rule_Model_Resource_Abstract */
        $resource = $this->getResource();

        return $resource->getProductFlatSelect($storeId, $this->getConditions());
    }

    /**
     * Prepare data before saving
     *
     * @return Mage_Rule_Model_Abstract
     */
    protected function _beforeSave()
    {
        // Check if discount amount not negative
        if ($this->hasDiscountAmount()) {
            if ((int)$this->getDiscountAmount() < 0) {
                Mage::throwException(Mage::helper('rule')->__('Invalid discount amount.'));
            }
        }

        // Serialize conditions
        if ($this->getConditions()) {
            $this->setConditionsSerialized(serialize($this->getConditions()->asArray()));
            $this->unsConditions();
        }

        // Serialize actions
        if ($this->getActions()) {
            $this->setActionsSerialized(serialize($this->getActions()->asArray()));
            $this->unsActions();
        }

        /**
         * Prepare website Ids if applicable and if they were set as string in comma separated format.
         * Backwards compatibility.
         */
        if ($this->hasWebsiteIds()) {
            $websiteIds = $this->getWebsiteIds();
            if (is_string($websiteIds) && !empty($websiteIds)) {
                $this->setWebsiteIds(explode(',', $websiteIds));
            }
        }

        /**
         * Prepare customer group Ids if applicable and if they were set as string in comma separated format.
         * Backwards compatibility.
         */
        if ($this->hasCustomerGroupIds()) {
            $groupIds = $this->getCustomerGroupIds();
            if (is_string($groupIds) && !empty($groupIds)) {
                $this->setCustomerGroupIds(explode(',', $groupIds));
            }
        }

        parent::_beforeSave();
        return $this;
    }

    /**
     * Set rule combine conditions model
     *
     * @param Mage_Rule_Model_Condition_Combine $conditions
     *
     * @return Mage_Rule_Model_Abstract
     */
    public function setConditions($conditions)
    {
        $this->_conditions = $conditions;
        return $this;
    }

    /**
     * Retrieve rule combine conditions model
     *
     * @return Mage_Rule_Model_Condition_Combine
     */
    public function getConditions()
    {
        if (empty($this->_conditions)) {
            $this->_resetConditions();
        }

        // Load rule conditions if it is applicable
        if ($this->hasConditionsSerialized()) {
            $conditions = $this->getConditionsSerialized();
            if (!empty($conditions)) {
                $conditions = Mage::helper('core/unserializeArray')->unserialize($conditions);
                if (is_array($conditions) && !empty($conditions)) {
                    $this->_conditions->loadArray($conditions);
                }
            }
            $this->unsConditionsSerialized();
        }

        return $this->_conditions;
    }

    /**
     * Set rule actions model
     *
     * @param Mage_Rule_Model_Action_Collection $actions
     *
     * @return Mage_Rule_Model_Abstract
     */
    public function setActions($actions)
    {
        $this->_actions = $actions;
        return $this;
    }

    /**
     * Retrieve rule actions model
     *
     * @return Mage_Rule_Model_Action_Collection
     */
    public function getActions()
    {
        if (!$this->_actions) {
            $this->_resetActions();
        }

        // Load rule actions if it is applicable
        if ($this->hasActionsSerialized()) {
            $actions = $this->getActionsSerialized();
            if (!empty($actions)) {
                $actions = Mage::helper('core/unserializeArray')->unserialize($actions);
                if (is_array($actions) && !empty($actions)) {
                    $this->_actions->loadArray($actions);
                }
            }
            $this->unsActionsSerialized();
        }

        return $this->_actions;
    }

    /**
     * Reset rule combine conditions
     *
     * @param null|Mage_Rule_Model_Condition_Combine $conditions
     *
     * @return Mage_Rule_Model_Abstract
     */
    protected function _resetConditions($conditions = null)
    {
        if (is_null($conditions)) {
            $conditions = $this->getConditionsInstance();
        }
        $conditions->setRule($this)->setId('1')->setPrefix('conditions');
        $this->setConditions($conditions);

        return $this;
    }

    /**
     * Reset rule actions
     *
     * @param null|Mage_Rule_Model_Action_Collection $actions
     *
     * @return Mage_Rule_Model_Abstract
     */
    protected function _resetActions($actions = null)
    {
        if (is_null($actions)) {
            $actions = $this->getActionsInstance();
        }
        $actions->setRule($this)->setId('1')->setPrefix('actions');
        $this->setActions($actions);

        return $this;
    }

    /**
     * Rule form getter
     *
     * @return Varien_Data_Form
     */
    public function getForm()
    {
        if (!$this->_form) {
            $this->_form = new Varien_Data_Form();
        }
        return $this->_form;
    }

    /**
     * Initialize rule model data from array
     *
     * @param array $data
     *
     * @return Mage_Rule_Model_Abstract
     */
    public function loadPost(array $data)
    {
        $arr = $this->_convertFlatToRecursive($data);
        if (isset($arr['conditions'])) {
            $this->getConditions()->setConditions(array())->loadArray($arr['conditions'][1]);
        }
        if (isset($arr['actions'])) {
            $this->getActions()->setActions(array())->loadArray($arr['actions'][1], 'actions');
        }

        return $this;
    }

    /**
     * Set specified data to current rule.
     * Set conditions and actions recursively.
     * Convert dates into Zend_Date.
     *
     * @param array $data
     *
     * @return array
     */
    protected function _convertFlatToRecursive(array $data)
    {
        $arr = array();
        foreach ($data as $key => $value) {
            if (($key === 'conditions' || $key === 'actions') && is_array($value)) {
                foreach ($value as $id=>$data) {
                    $path = explode('--', $id);
                    $node =& $arr;
                    for ($i=0, $l=sizeof($path); $i<$l; $i++) {
                        if (!isset($node[$key][$path[$i]])) {
                            $node[$key][$path[$i]] = array();
                        }
                        $node =& $node[$key][$path[$i]];
                    }
                    foreach ($data as $k => $v) {
                        $node[$k] = $v;
                    }
                }
            } else {
                /**
                 * Convert dates into Zend_Date
                 */
                if (in_array($key, array('from_date', 'to_date')) && $value) {
                    $value = Mage::app()->getLocale()->date(
                        $value,
                        Varien_Date::DATE_INTERNAL_FORMAT,
                        null,
                        false
                    );
                }
                $this->setData($key, $value);
            }
        }

        return $arr;
    }

    /**
     * Validate rule conditions to determine if rule can run
     *
     * @param Varien_Object $object
     *
     * @return bool
     */
    public function validate(Varien_Object $object)
    {
        return $this->getConditions()->validate($object);
    }

    /**
     * Validate rule data
     *
     * @param Varien_Object $object
     *
     * @return bool|array - return true if validation passed successfully. Array with errors description otherwise
     */
    public function validateData(Varien_Object $object)
    {
        $result   = array();
        $fromDate = $toDate = null;

        if ($object->hasFromDate() && $object->hasToDate()) {
            $fromDate = $object->getFromDate();
            $toDate = $object->getToDate();
        }

        if ($fromDate && $toDate) {
            $fromDate = new Zend_Date($fromDate, Varien_Date::DATE_INTERNAL_FORMAT);
            $toDate = new Zend_Date($toDate, Varien_Date::DATE_INTERNAL_FORMAT);

            if ($fromDate->compare($toDate) === 1) {
                $result[] = Mage::helper('rule')->__('End Date must be greater than Start Date.');
            }
        }

        if ($object->hasWebsiteIds()) {
            $websiteIds = $object->getWebsiteIds();
            if (empty($websiteIds)) {
                $result[] = Mage::helper('rule')->__('Websites must be specified.');
            }
        }
        if ($object->hasCustomerGroupIds()) {
            $customerGroupIds = $object->getCustomerGroupIds();
            if (empty($customerGroupIds)) {
                $result[] = Mage::helper('rule')->__('Customer Groups must be specified.');
            }
        }

        return !empty($result) ? $result : true;
    }

    /**
     * Check availability to delete rule
     *
     * @return bool
     */
    public function isDeleteable()
    {
        return $this->_isDeleteable;
    }

    /**
     * Set is rule can be deleted flag
     *
     * @param bool $value
     *
     * @return Mage_Rule_Model_Abstract
     */
    public function setIsDeleteable($value)
    {
        $this->_isDeleteable = (bool) $value;
        return $this;
    }

    /**
     * Check if rule is readonly
     *
     * @return bool
     */
    public function isReadonly()
    {
        return $this->_isReadonly;
    }

    /**
     * Set is readonly flag to rule
     *
     * @param bool $value
     *
     * @return Mage_Rule_Model_Abstract
     */
    public function setIsReadonly($value)
    {
        $this->_isReadonly = (bool) $value;
        return $this;
    }

    /**
     * Get rule associated website Ids
     *
     * @return array
     */
    public function getWebsiteIds()
    {
        if (!$this->hasWebsiteIds()) {
            $websiteIds = $this->_getResource()->getWebsiteIds($this->getId());
            $this->setData('website_ids', (array)$websiteIds);
        }
        return $this->_getData('website_ids');
    }




    /**
     * @deprecated since 1.7.0.0
     *
     * @param string $format
     *
     * @return string
     */
    public function asString($format='')
    {
        return '';
    }

    /**
     * @deprecated since 1.7.0.0
     *
     * @return string
     */
    public function asHtml()
    {
        return '';
    }

    /**
     * Returns rule as an array for admin interface
     *
     * @deprecated since 1.7.0.0
     *
     * @param array $arrAttributes
     *
     * @return array
     */
    public function asArray(array $arrAttributes = array())
    {
        return array();
    }

    /**
     * Combine website ids to string
     *
     * @deprecated since 1.7.0.0
     *
     * @return Mage_Rule_Model_Abstract
     */
    protected function _prepareWebsiteIds()
    {
        return $this;
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Rule
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Abstract Rule entity data model
 *
 * @deprecated since 1.7.0.0 use Mage_Rule_Model_Abstract instead
 *
 * @category Mage
 * @package Mage_Rule
 * @author Magento Core Team <core@magentocommerce.com>
 */
class Mage_Rule_Model_Rule extends Mage_Rule_Model_Abstract
{
    /**
     * Getter for rule combine conditions instance
     *
     * @return Mage_Rule_Model_Condition_Combine
     */
    public function getConditionsInstance()
    {
        return Mage::getModel('rule/condition_combine');
    }

    /**
     * Getter for rule actions collection instance
     *
     * @return Mage_Rule_Model_Action_Collection
     */
    public function getActionsInstance()
    {
        return Mage::getModel('rule/action_collection');
    }
}
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Afptc
 * @version    1.1.12
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Afptc_Model_Rule extends Mage_Rule_Model_Rule
{
    const BY_PERCENT_ACTION  = 1;
    const BUY_X_GET_Y_ACTION = 2;

    public function _construct()
    {
        parent::_construct();
        $this->_init('awafptc/rule');
    }

    public function getConditionsInstance()
    {
        return Mage::getModel('awafptc/rule_condition_combine');
    }

    public function loadPost(array $rule)
    {
        $arr = $this->_convertFlatToRecursive($rule);
        if (isset($arr['conditions'])) {
            $this->getConditions()->setConditions(array())->loadArray($arr['conditions'][1]);
        }
        return $this;
    }

    public function validate(Varien_Object $object)
    {
        $_result = false;
        $needRecalculate = false;
        if ($object instanceof Mage_Checkout_Model_Cart) {
            $this->_prepareValidate($object);

            //check if free product by rule already in cart
            $this->setBuyXProductIds(array());
            $itemsCount = count($object->getQuote()->getAllItems());
            if (!count($object->getQuote()->getAllItems())) {
                return false;
            }
            foreach ($object->getQuote()->getAllItems() as $item) {
                // AW_Sarp2 compatibility
                if (Mage::helper('awafptc')->isSubscriptionItem($item)) {
                    return false;
                }
                $ruleIdOption = $item->getOptionByCode('aw_afptc_rule');

                //Buy X get one free
                if ($this->getSimpleAction() == self::BUY_X_GET_Y_ACTION
                    && $item->getQty() >= $this->getDiscountStep()
                    && null === $ruleIdOption
                ) {
                    $_products = $this->getBuyXProductIds();
                    $_products[$item->getId()] = $item->getProductId();
                    $this->setBuyXProductIds($_products);
                }
                //check already applied rules and remove item if shopping cart not valid for rule
                if (null !== $ruleIdOption && $ruleIdOption->getValue() == $this->getId()) {
                    if ($this->getSimpleAction() == self::BUY_X_GET_Y_ACTION) {
                        $relatedItemIdOption = $item->getOptionByCode('aw_afptc_related_item_id');

                        $_relatedItemId = null;
                        if (null !== $relatedItemIdOption) {
                            $_relatedItemId = $relatedItemIdOption->getValue();
                        }

                        $itemModel = null;
                        if ($_relatedItemId !== null) {
                            $_products = $this->getBuyXProductIds();
                            unset($_products[$_relatedItemId]);
                            $this->setBuyXProductIds($_products);

                            foreach ($object->getQuote()->getItemsCollection() as $_item) {
                                if ($_item->getId() == $_relatedItemId) {
                                    $itemModel = $_item;
                                    break;
                                }
                            }
                        }

                        if (null === $itemModel
                            || null === $itemModel->getId()
                            || $itemModel->getQty() < $this->getDiscountStep()
                            || $itemModel->isDeleted()
                        ) {
                            $needRemoveFlag = true;
                        }
                    }
                    $allItemsOriginal = $object->getAllItems();
                    $allItems = array();
                    foreach ($allItemsOriginal as $cartQuoteItem) {
                        if ($cartQuoteItem->getId() != $item->getId()) {
                            $allItems[] = $cartQuoteItem;
                        }
                    }
                    $validWithout = $this->_validateItems($object, $allItems);
                    $validWith = $this->_validateItems($object, $allItemsOriginal);
                    if (!($validWith && $validWithout) || isset($needRemoveFlag)) {
                        $object->removeItem($item->getId());
                        $needRecalculate = true;
                    } else {
                        $this->setAlreadyApplied(true);
                    }

                    if ($this->getSimpleAction() == self::BUY_X_GET_Y_ACTION && $this->getBuyXProductIds() != 0) {
                        $this->setAlreadyApplied(false);
                    }

                    if ($itemsCount == 1) {
                        $object->truncate();
                    }
                }
            }
            if ($needRecalculate) {
                $object->getQuote()->unsTotalsCollectedFlag()->collectTotals();
            }
            $_result = $this->getConditions()->validate($object);
        }
        return $_result;
    }

    protected function _prepareValidate(Mage_Checkout_Model_Cart $cart)
    {
        $cart->setData('all_items', $cart->getQuote()->getAllItems());

        if ($cart->getQuote()->isVirtual()) {
            $address = $cart->getQuote()->getBillingAddress();
        } else {
            $address = $cart->getQuote()->getShippingAddress();
        }

        $bonusItemsQty = 0;
        $bonusItemsBaseSubtotal = 0;
        $bonusItemsWeight = 0;

        foreach ($cart->getQuote()->getAllItems() as $item) {
            $ruleIdOption = $item->getOptionByCode('aw_afptc_rule');

            if (null === $ruleIdOption) {
                continue;
            }

            $bonusItemsQty += $item->getQty();
            $bonusItemsBaseSubtotal += $item->getBaseRowTotal();
            $bonusItemsWeight += $item->getWeight();
        }
        $address->setTotalQty($cart->getItemsQty() - $bonusItemsQty);
        $address->setBaseSubtotal($address->getBaseSubtotal() - $bonusItemsBaseSubtotal);
        $address->setWeight($address->getWeight() - $bonusItemsWeight);

        if ((AW_All_Helper_Versions::getPlatform() == AW_All_Helper_Versions::CE_PLATFORM
                && version_compare(Mage::getVersion(), '1.7', '>='))
            || (AW_All_Helper_Versions::getPlatform() == AW_All_Helper_Versions::EE_PLATFORM
                && version_compare(Mage::getVersion(), '1.14', '>='))
        ) {
            $quote = $cart->getQuote();
            foreach ($quote->getAllItems() as $item) {
                $itemProduct = $item->getProduct();
                $product = Mage::getModel('catalog/product')->load($itemProduct->getId());
                foreach ($product->getData() as $key => $value) {
                    if (null === $itemProduct->getData($key)) {
                        $itemProduct->setData($key, $value);
                    }
                }
            }
        }
        return $this;
    }

    public function getDiscount()
    {
        $_discount = $this->getData('discount');
        if ($this->getSimpleAction() == self::BUY_X_GET_Y_ACTION) {
            $_discount = 100;
        }
        return $_discount;
    }

    public function apply(Mage_Checkout_Model_Cart $cart, $relatedItemId = null)
    {
        if (!$this->validate($cart) || true === $this->getAlreadyApplied()) {
            return $this;
        }
        if (
            null === $relatedItemId
            && $this->getSimpleAction() == self::BUY_X_GET_Y_ACTION
            && count($this->getBuyXProductIds()) != 0
        ) {
            foreach ($this->getBuyXProductIds() as $itemId => $productId) {
                $this->_apply($cart, $itemId);
            }
            return $this;
        }
        return $this->_apply($cart, $relatedItemId);
    }

    private function _apply(Mage_Checkout_Model_Cart $cart, $relatedItemId) {
        /** @var Mage_Sales_Model_Quote_Item $itemModel */
        $itemModel = Mage::getModel('sales/quote_item');
        if (
            $this->getSimpleAction() == self::BUY_X_GET_Y_ACTION
            && count($this->getBuyXProductIds()) != 0
        ) {
            if (array_key_exists($relatedItemId, $this->getBuyXProductIds())) {
                /** @var Mage_Sales_Model_Quote_Item $relatedModel */
                $relatedModel = null;
                foreach ($cart->getQuote()->getItemsCollection() as $_item) {
                    if ($_item->getId() == $relatedItemId) {
                        $relatedModel = $_item;
                        break;
                    }
                }
                if (null === $relatedModel || null === $relatedModel->getId()) {
                    return $this;
                }
                $relatedProduct = $relatedModel->getProduct();
                $itemModel = clone $relatedModel;
                $_product = clone $relatedProduct;

                $itemModel->setProduct($_product);
                $_product->addCustomOption('aw_afptc_related_item_id', $relatedItemId);

                /*
                 * This go-round is required to make Magento compare the quote items correctly
                 * when it merges quote upon customer login. Otherwise it will found no options
                 * in the first (related) item and will not look further, resulting in positive compare
                 * even though the free item has this option. In short, both X and Y items MUST
                 * have the same option with different value for merge to behave nicely.
                 * This specific option was selected simply because it has the null-check
                 * everywhere in the code and will not affect anything. We could have added
                 * another option exclusively for this purpose, but in my opinion the less options
                 * the better.
                 */
                if (version_compare(Mage::getVersion(), '1.5.1.0') >= 0) {
                    $relatedOption = Mage::getModel('catalog/product_configuration_item_option')
                        ->addData(
                            array(
                                'product_id'=> $relatedProduct->getId(),
                                'product'   => $relatedProduct,
                                'code'      => 'aw_afptc_related_item_id',
                                'value'     => null,
                            )
                        )
                    ;
                }
                else {
                    $relatedOption = new Varien_Object(
                        array(
                            'product_id'=> $relatedProduct->getId(),
                            'product'   => $relatedProduct,
                            'code'      => 'aw_afptc_related_item_id',
                            'value'     => null,
                        )
                    );
                }
                $relatedModel->addOption($relatedOption);
            }
        }

        if (
            $this->getSimpleAction() == AW_Afptc_Model_Rule::BY_PERCENT_ACTION
            && true !== $this->getAlreadyApplied()
        ) {
            $_product = Mage::getModel('catalog/product')->load($this->getProductId());
        }

        if (!isset($_product)) {
            return $this;
        }

        if($_product->getTypeId() == 'downloadable') {
            if (!$_product->getTypeInstance(true)->getProduct($_product)->getLinksPurchasedSeparately()) {
                $links = $_product->getTypeInstance(true)->getLinks($_product);
                foreach($links as $link) {
                    $preparedLinks[] = $link->getId();
                }
                $_product->addCustomOption('downloadable_link_ids', implode(',', $preparedLinks));
            }
        }

        if (!isset($_product) || null === $_product->getId() || !$_product->isSaleable()) {
            return $this;
        }

        $_product->addCustomOption('aw_afptc_discount', min(100, $this->getDiscount()));
        $_product->addCustomOption('aw_afptc_rule', $this->getId());

        $itemModel
            ->setQuote($cart->getQuote())
            ->setStoreId(Mage::app()->getStore()->getId())
            ->setOptions($_product->getCustomOptions())
            ->setProduct($_product)
            ->setQty(1)
        ;

        if ($this->getSimpleAction() == self::BUY_X_GET_Y_ACTION) {
            $itemModel->setCalculationPrice(0);
            $itemModel->setPriceInclTax(0);
            $itemModel->setPrice(0);
            $itemModel->setTaxableAmount(0);
            $itemModel->setRowTotal(0);
            $itemModel->setRowTotalInclTax(0);
            $itemModel->setBaseCalculationPrice(0);
            $itemModel->setBaseTaxableAmount(0);
            $itemModel->setBasePrice(0);
            $itemModel->setBasePriceInclTax(0);
            $itemModel->setBaseRowTotal(0);
            $itemModel->setBaseRowTotalInclTax(0);
        }

        $cart->getQuote()->addItem($itemModel);
        //$cart->save();

        return $this;
    }

    private function _validateItems(Mage_Checkout_Model_Cart $cart, $items)
    {
        $cart->setAllItems($items);
        return $this->getConditions()->validate($cart);
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Core
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Abstract Core Resource Collection
 *
 * @category    Mage
 * @package     Mage_Core
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Core_Model_Mysql4_Collection_Abstract extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
}
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Afptc
 * @version    1.1.12
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Afptc_Model_Resource_Rule_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('awafptc/rule');
    }

    public function addStatusFilter()
    {
        $this->getSelect()->where('main_table.status = ?', 1);
        return $this;
    }

    public function addPriorityOrder()
    {
        $this->getSelect()->order('main_table.priority DESC');
        return $this;
    }
    
    public function addTimeLimitFilter()
    {
        $this->getSelect()
            ->where("if(main_table.end_date is null, true, main_table.end_date > UTC_TIMESTAMP()) AND
                if(main_table.start_date is null, true, main_table.start_date < UTC_TIMESTAMP())");

        return $this;
    }
    
    public function addStoreFilter($store)
    {
        $this->getSelect()->where('find_in_set(0, store_ids) OR find_in_set(?, store_ids)', $store);
        return $this;
    }
    
    public function addGroupFilter($group)
    {
        $this->getSelect()->where('find_in_set(?, customer_groups)', $group);
        return $this;
    }
}
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_Label
 */
class Amasty_Label_Model_Observer
{
    public function applyLabels($observer)
    {
        if (!Mage::app()->getRequest()->getParam('amlabels')
        || !Mage::getSingleton('admin/session')->isAllowed('catalog/products/assign_labels'))
            return $this;

        $product = $observer->getEvent()->getProduct();

        $collection = Mage::getModel('amlabel/label')->getCollection()
            ->addFieldToFilter('include_type', array('neq' => 1));

        foreach ($collection as $label) {
            $skus = trim($label->getIncludeSku(), ', ');
            if ($skus) {
                $skus = explode(',', $skus);
            } else {
                $skus = array();
            }

            $name = 'amlabel_' . $label->getId();
            if (Mage::app()->getRequest()->getParam($name)) { // add
                if (!in_array($product->getSku(), $skus)) {
                    $skus[] = $product->getSku();
                }
            } else { // remove
                $key = array_search($product->getSku(), $skus);
                while (false !== $key) {
                    unset($skus[$key]);
                    $key = array_search($product->getSku(), $skus);
                }
            }
            $label->setIncludeSku(implode(',', $skus));
            $label->save();
        }

        return $this;
    }

    /**
     * @param Varien_Event_Observer $observer
     *
     * @return $this
     */
    public function onCoreBlockAbstractToHtmlBefore(Varien_Event_Observer $observer)
    {
        if (Mage::getSingleton('admin/session')->isAllowed('catalog/products/assign_labels')) {
            $block = $observer->getBlock();
            $catalogProductEditTabsClass = Mage::getConfig()->getBlockClassName('adminhtml/catalog_product_edit_tabs');
            if ($catalogProductEditTabsClass == get_class($block) && $block->getProduct()->getTypeId()) {
                $name = Mage::helper('amlabel')->__('Product Labels');
                $block->addTab('general', array(
                        'label'   => $name,
                        'content' => $block->getLayout()->createBlock('amlabel/adminhtml_catalog_product_edit_labels')
                                ->setTitle($name)->toHtml(),
                    )
                );
            }
        }

        return $this;
    }

    public function onCoreBlockAbstractToHtmlAfter(Varien_Event_Observer $observer)
    {
        if (!Mage::getStoreConfig('amlabel/options/use_js', Mage::app()->getStore()->getId())) {
            return $this;
        }
        $block = $observer->getBlock();
        if ($block instanceof Mage_Catalog_Block_Product_Price) {
            $id = $block->getProduct()->getId();
            if (!Mage::registry('amlabel_product_id_' . $id)) {
                // add product ID info in output
                Mage::register('amlabel_product_id_' . $id, true, true);
                $html = $observer->getTransport()->getHtml();
                $html = '<div class="price" id="amlabel-product-price-' . $id . '" style="display:none"></div>' . $html;

                // add label for product
                $product = $block->getProduct();
                /*
                 * old method was:
                 *  $type    = strpos($block->getModuleName(), 'Catalog') !== false ? 'category' : 'product';
                 *
                 * new method:
                 */
                $controller = Mage::app()->getRequest()->getControllerName();
                $action     = Mage::app()->getRequest()->getActionName();
                $type       = ($controller == 'product' && $action == 'view') ? 'product' : 'category';

                $label   = Mage::helper('amlabel')->getLabels($product, $type, true);
                if ($label) {
                    $this->addScript($id, addslashes($label));
                }

                $observer->getTransport()->setHtml($html);
            }
        }

        return $this;
    }

    private function addScript($id, $label)
    {
        $scripts = Mage::registry('amlabel_scripts');
        if (strpos($scripts, "label_product_ids[$id]") === false) {
            $scripts .= "\r\n amlabel_product_ids[$id] = '" . addslashes($label) . "'; \r\n";
        }
        Mage::unregister('amlabel_scripts');
        Mage::register('amlabel_scripts', $scripts);
    }

    public function addLabelProductCollectionScript(Varien_Event_Observer $observer)
    {
        if (Mage::registry('amlabel_getting_product')
            || !Mage::getStoreConfig('amlabel/options/use_js', Mage::app()->getStore()->getId())) {
            return $this;
        }

        /*
         * register global flag to prevent grouped/configurable/bundle products
         * loading all child products caught by observer
         */
        Mage::register('amlabel_getting_product', true, true);
        $productCollection = $observer->getCollection();
        $blockClass = get_class($productCollection);
        $blockedClasses = array(
            'Mage_Reports_Model_Resource_Product_Index_Viewed_Collection',
        );
        if (in_array($blockClass, $blockedClasses)) {
            return $this;
        }
        if ($productCollection) {
            foreach ($productCollection as $item) {
                $label = Mage::helper('amlabel')->getLabels($item, 'category', true);
                if ($label) {
                    $this->addScript($item->getId(), $label);
                }
            }
        }

        Mage::unregister('amlabel_getting_product');

        return $this;
    }

    public function addLabelProductLoadScript(Varien_Event_Observer $observer)
    {
        if (Mage::registry('amlabel_getting_product')
            || !Mage::getStoreConfig('amlabel/options/use_js', Mage::app()->getStore()->getId())) {
            return $this;
        }

        /*
         * register global flag to prevent grouped/configurable/bundle products
         * loading all child products caught by observer
         */
        $label      = '';
        $controller = Mage::app()->getRequest()->getControllerName();
        Mage::register('amlabel_getting_product', true, true);
        $product = $observer->getProduct();
        if ($product) {
            if (strpos($controller, 'cart') === false) {
                $label = Mage::helper('amlabel')->getLabels($product, 'product', true);
            }
            if ($label) {
                $this->addScript($product->getId(), $label);
            }
        }

        Mage::unregister('amlabel_getting_product');

        return $this;
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Core
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Core Observer model
 *
 * @category   Mage
 * @package    Mage_Core
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Mage_Core_Model_Observer
{
    /**
     * Check if synchronize process is finished and generate notification message
     *
     * @param  Varien_Event_Observer $observer
     * @return Mage_Core_Model_Observer
     */
    public function addSynchronizeNotification(Varien_Event_Observer $observer)
    {
        $adminSession = Mage::getSingleton('admin/session');
        if (!$adminSession->hasSyncProcessStopWatch()) {
            $flag = Mage::getSingleton('core/file_storage')->getSyncFlag();
            $state = $flag->getState();
            if ($state == Mage_Core_Model_File_Storage_Flag::STATE_RUNNING) {
                $syncProcessStopWatch = true;
            } else {
                $syncProcessStopWatch = false;
            }

            $adminSession->setSyncProcessStopWatch($syncProcessStopWatch);
        }
        $adminSession->setSyncProcessStopWatch(false);

        if (!$adminSession->getSyncProcessStopWatch()) {
            if (!isset($flag)) {
                $flag = Mage::getSingleton('core/file_storage')->getSyncFlag();
            }

            $state = $flag->getState();
            if ($state == Mage_Core_Model_File_Storage_Flag::STATE_FINISHED) {
                $flagData = $flag->getFlagData();
                if (isset($flagData['has_errors']) && $flagData['has_errors']) {
                    $severity       = Mage_AdminNotification_Model_Inbox::SEVERITY_MAJOR;
                    $title          = Mage::helper('adminhtml')->__('An error has occured while syncronizing media storages.');
                    $description    = Mage::helper('adminhtml')->__('One or more media files failed to be synchronized during the media storages syncronization process. Refer to the log file for details.');
                } else {
                    $severity       = Mage_AdminNotification_Model_Inbox::SEVERITY_NOTICE;
                    $title          = Mage::helper('adminhtml')->__('Media storages synchronization has completed!');
                    $description    = Mage::helper('adminhtml')->__('Synchronization of media storages has been successfully completed.');
                }

                $date = date('Y-m-d H:i:s');
                Mage::getModel('adminnotification/inbox')->parse(array(
                    array(
                        'severity'      => $severity,
                        'date_added'    => $date,
                        'title'         => $title,
                        'description'   => $description,
                        'url'           => '',
                        'internal'      => true
                    )
                ));

                $flag->setState(Mage_Core_Model_File_Storage_Flag::STATE_NOTIFIED)->save();
            }

            $adminSession->setSyncProcessStopWatch(false);
        }

        return $this;
    }

    /**
     * Cron job method to clean old cache resources
     *
     * @param Mage_Cron_Model_Schedule $schedule
     */
    public function cleanCache(Mage_Cron_Model_Schedule $schedule)
    {
        Mage::app()->getCache()->clean(Zend_Cache::CLEANING_MODE_OLD);
        Mage::dispatchEvent('core_clean_cache');
    }


    /**
     * Cleans cache by tags
     *
     * @param Varien_Event_Observer $observer
     * @return Mage_Core_Model_Observer
     */
    public function cleanCacheByTags(Varien_Event_Observer $observer)
    {
        /** @var $tags array */
        $tags = $observer->getEvent()->getTags();
        if (empty($tags)) {
            Mage::app()->cleanCache();
            return $this;
        }

        Mage::app()->cleanCache($tags);
        return $this;
    }

    /**
     * Checks method availability for processing in variable
     *
     * @param Varien_Event_Observer $observer
     * @throws Exception
     * @return Mage_Core_Model_Observer
     */
    public function secureVarProcessing(Varien_Event_Observer $observer)
    {
        if (Mage::registry('varProcessing')) {
            Mage::throwException(Mage::helper('core')->__('Disallowed template variable method.'));
        }
        return $this;
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Core
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Date conversion model
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Core_Model_Date
{
    /**
     * Current config offset in seconds
     *
     * @var int
     */
    private $_offset = 0;

    /**
     * Current system offset in seconds
     *
     * @var int
     */
    private $_systemOffset = 0;

    /**
     * Init offset
     *
     */
    public function __construct()
    {
        $this->_offset = $this->calculateOffset($this->_getConfigTimezone());
        $this->_systemOffset = $this->calculateOffset();
    }

    /**
     * Gets the store config timezone
     *
     * @return string
     */
    protected function _getConfigTimezone()
    {
        return Mage::app()->getStore()->getConfig('general/locale/timezone');
    }

    /**
     * Calculates timezone offset
     *
     * @param  string $timezone
     * @return int offset between timezone and gmt
     */
    public function calculateOffset($timezone = null)
    {
        $result = true;
        $offset = 0;

        if (!is_null($timezone)){
            $oldzone = @date_default_timezone_get();
            $result = date_default_timezone_set($timezone);
        }

        if ($result === true) {
            $offset = (int)date('Z');
        }

        if (!is_null($timezone)){
            date_default_timezone_set($oldzone);
        }

        return $offset;
    }

    /**
     * Forms GMT date
     *
     * @param  string $format
     * @param  int|string $input date in current timezone
     * @return string
     */
    public function gmtDate($format = null, $input = null)
    {
        if (is_null($format)) {
            $format = 'Y-m-d H:i:s';
        }

        $date = $this->gmtTimestamp($input);

        if ($date === false) {
            return false;
        }

        $result = date($format, $date);
        return $result;
    }

    /**
     * Converts input date into date with timezone offset
     * Input date must be in GMT timezone
     *
     * @param  string $format
     * @param  int|string $input date in GMT timezone
     * @return string
     */
    public function date($format = null, $input = null)
    {
        if (is_null($format)) {
            $format = 'Y-m-d H:i:s';
        }

        $result = date($format, $this->timestamp($input));
        return $result;
    }

    /**
     * Forms GMT timestamp
     *
     * @param  int|string $input date in current timezone
     * @return int
     */
    public function gmtTimestamp($input = null)
    {
        if (is_null($input)) {
            return gmdate('U');
        } else if (is_numeric($input)) {
            $result = $input;
        } else {
            $result = strtotime($input);
        }

        if ($result === false) {
            // strtotime() unable to parse string (it's not a date or has incorrect format)
            return false;
        }

        $date      = Mage::app()->getLocale()->date($result);
        $timestamp = $date->get(Zend_Date::TIMESTAMP) - $date->get(Zend_Date::TIMEZONE_SECS);

        unset($date);
        return $timestamp;

    }

    /**
     * Converts input date into timestamp with timezone offset
     * Input date must be in GMT timezone
     *
     * @param  int|string $input date in GMT timezone
     * @return int
     */
    public function timestamp($input = null)
    {
        if (is_null($input)) {
            $result = $this->gmtTimestamp();
        } else if (is_numeric($input)) {
            $result = $input;
        } else {
            $result = strtotime($input);
        }

        $date      = Mage::app()->getLocale()->date($result);
        $timestamp = $date->get(Zend_Date::TIMESTAMP) + $date->get(Zend_Date::TIMEZONE_SECS);

        unset($date);
        return $timestamp;
    }

    /**
     * Get current timezone offset in seconds/minutes/hours
     *
     * @param  string $type
     * @return int
     */
    public function getGmtOffset($type = 'seconds')
    {
        $result = $this->_offset;
        switch ($type) {
            case 'seconds':
            default:
                break;

            case 'minutes':
                $result = $result / 60;
                break;

            case 'hours':
                $result = $result / 60 / 60;
                break;
        }
        return $result;
    }

    /**
     * Deprecated since 1.1.7
     */
    public function checkDateTime($year, $month, $day, $hour = 0, $minute = 0, $second = 0)
    {
        if (!checkdate($month, $day, $year)) {
            return false;
        }
        foreach (array('hour' => 23, 'minute' => 59, 'second' => 59) as $var => $maxValue) {
            $value = (int)$$var;
            if (($value < 0) || ($value > $maxValue)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Deprecated since 1.1.7
     */
    public function parseDateTime($dateTimeString, $dateTimeFormat)
    {
        // look for supported format
        $isSupportedFormatFound = false;

        $formats = array(
            // priority is important!
            '%m/%d/%y %I:%M' => array(
                '/^([0-9]{1,2})\/([0-9]{1,2})\/([0-9]{1,2}) ([0-9]{1,2}):([0-9]{1,2})/',
                array('y' => 3, 'm' => 1, 'd' => 2, 'h' => 4, 'i' => 5)
            ),
            'm/d/y h:i' => array(
                '/^([0-9]{1,2})\/([0-9]{1,2})\/([0-9]{1,2}) ([0-9]{1,2}):([0-9]{1,2})/',
                array('y' => 3, 'm' => 1, 'd' => 2, 'h' => 4, 'i' => 5)
            ),
            '%m/%d/%y' => array('/^([0-9]{1,2})\/([0-9]{1,2})\/([0-9]{1,2})/', array('y' => 3, 'm' => 1, 'd' => 2)),
            'm/d/y' => array('/^([0-9]{1,2})\/([0-9]{1,2})\/([0-9]{1,2})/', array('y' => 3, 'm' => 1, 'd' => 2)),
        );

        foreach ($formats as $supportedFormat => $regRule) {
            if (false !== strpos($dateTimeFormat, $supportedFormat, 0)) {
                $isSupportedFormatFound = true;
                break;
            }
        }
        if (!$isSupportedFormatFound) {
            Mage::throwException(Mage::helper('core')->__('Date/time format "%s" is not supported.', $dateTimeFormat));
        }

        // apply reg rule to found format
        $regex = array_shift($regRule);
        $mask  = array_shift($regRule);
        if (!preg_match($regex, $dateTimeString, $matches)) {
            Mage::throwException(Mage::helper('core')->__('Specified date/time "%1$s" do not match format "%2$s".', $dateTimeString, $dateTimeFormat));
        }

        // make result
        $result = array();
        foreach (array('y', 'm', 'd', 'h', 'i', 's') as $key) {
            $value = 0;
            if (isset($mask[$key]) && isset($matches[$mask[$key]])) {
                $value = (int)$matches[$mask[$key]];
            }
            $result[] = $value;
        }

        // make sure to return full year
        if ($result[0] < 100) {
            $result[0] = 2000 + $result[0];
        }

        return $result;
    }
}

class Devinc_Dailydeal_Helper_Data extends Mage_Core_Helper_Abstract
{
	const STATUS_RUNNING = Devinc_Dailydeal_Model_Source_Status::STATUS_RUNNING;
	const STATUS_DISABLED = Devinc_Dailydeal_Model_Source_Status::STATUS_DISABLED;
	const STATUS_ENDED = Devinc_Dailydeal_Model_Source_Status::STATUS_ENDED;
	const STATUS_QUEUED = Devinc_Dailydeal_Model_Source_Status::STATUS_QUEUED;
	
	//check if extension is enabled
	public static function isEnabled()
	{
		$storeId = Mage::app()->getStore()->getId();
		$isModuleEnabled = Mage::getStoreConfig('advanced/modules_disable_output/Devinc_Dailydeal', $storeId);
		$isEnabled = Mage::getStoreConfig('dailydeal/configuration/enabled', $storeId);
		return ($isModuleEnabled == 0 && $isEnabled == 1);
	}
	
	//$toDate format(year-month-day hour:minute:second) = 0000-00-00 00:00:00
    public function getCountdown($_product, $finished = false)
    {
    	$toDate = $_product->getDatetimeTo();
    	$countdownId = $_product->getId();
		$randomNr = rand(10e16, 10e20);

    	//from/to date variables
		$fromDate = $this->getCurrentDateTime();
		$jsFromDate = date('F d, Y H:i:s', strtotime($fromDate));
		$jsToDate = date('F d, Y H:i:s', strtotime($toDate));			
		if ($finished) {
			$toDate = $fromDate;
			$jsToDate = $jsFromDate;	
		}	
		
		$countdownType = Mage::getStoreConfig('dailydeal/configuration/countdown_type');

		//cirle countdown configuration		
		if ($countdownType==0) {
			$bgColor = (Mage::getStoreConfig('dailydeal/circle_countdown/bg_color')) ? $this->hex2rgb(Mage::getStoreConfig('dailydeal/circle_countdown/bg_color')) : '192, 202, 202';
			$loadingColor = (Mage::getStoreConfig('dailydeal/circle_countdown/loading_color')) ? $this->hex2rgb(Mage::getStoreConfig('dailydeal/circle_countdown/loading_color')) : '229, 233, 233';
			$daysText = Mage::getStoreConfig('dailydeal/circle_countdown/days_text');
			$hourText = Mage::getStoreConfig('dailydeal/circle_countdown/hour_text');
			$minText = Mage::getStoreConfig('dailydeal/circle_countdown/min_text');
			$secText = Mage::getStoreConfig('dailydeal/circle_countdown/sec_text');
		} elseif ($countdownType==1) {
			$daysText = Mage::getStoreConfig('dailydeal/flip_countdown/days_text');
			$hourText = Mage::getStoreConfig('dailydeal/flip_countdown/hour_text');
			$minText = Mage::getStoreConfig('dailydeal/flip_countdown/min_text');
			$secText = Mage::getStoreConfig('dailydeal/flip_countdown/sec_text');
		} elseif ($countdownType==2) {
			$daysText = Mage::getStoreConfig('dailydeal/simple_countdown/days_text');
			$hourText = Mage::getStoreConfig('dailydeal/simple_countdown/hour_text');
			$minText = Mage::getStoreConfig('dailydeal/simple_countdown/min_text');
			$secText = Mage::getStoreConfig('dailydeal/simple_countdown/sec_text');
		}
		
		$date1 = strtotime($fromDate);
	    $date2 = strtotime($toDate);	   
		$dateDiff = $date2 - $date1;
		$days = floor($dateDiff/(60*60*24));

		if ($countdownType==0) {
			if ($days>0) {
				$size = '50';
				$daysClass = ' countdown-days';
				$layout = '<ul>'.
				'{d<}<li class="days"><em>{dn}</em> '.$daysText.'</li>{d>}'.
				'{h<}<li class="hours"><em>{hn}</em> '.$hourText.'</li>{h>}'.
				'{m<}<li class="minutes"><em>{mn}</em> '.$minText.'</li>{m>}'.
				'{s<}<li class="seconds"><em>{sn}</em> '.$secText.'</li>{s>}'.
				'</ul>';
			} else {
				$size = '60';
				$daysClass = '';				
				$layout = '<ul>'.
				'{h<}<li class="hours"><em>{hn}</em> '.$hourText.'</li>{h>}'.
				'{m<}<li class="minutes"><em>{mn}</em> '.$minText.'</li>{m>}'.
				'{s<}<li class="seconds"><em>{sn}</em> '.$secText.'</li>{s>}'.
				'</ul>';
			}
			$html = '
				<div class="countdown'.$daysClass.'">
					<div id="countdown-'.$countdownId.'-'.$randomNr.'-timer" class="countdown_timer"></div>			                
				    <div id="countdown-'.$countdownId.'-'.$randomNr.'" class="countdown_clock">';
				    if ($days>0) {             
				        $html .= '<canvas class="circular_countdown_element" id="circular_countdown_days-'.$countdownId.'-'.$randomNr.'" width="'.$size.'" height="'.$size.'"></canvas>';
				    }

			  $html .= '<canvas class="circular_countdown_element" id="circular_countdown_hours-'.$countdownId.'-'.$randomNr.'" width="'.$size.'" height="'.$size.'"></canvas>
				        <canvas class="circular_countdown_element" id="circular_countdown_minutes-'.$countdownId.'-'.$randomNr.'" width="'.$size.'" height="'.$size.'"></canvas>
				        <canvas class="circular_countdown_element last" id="circular_countdown_seconds-'.$countdownId.'-'.$randomNr.'" width="'.$size.'" height="'.$size.'"></canvas>
				    </div>
				</div>';

			$html .= '<script type="text/javascript">
						jQueryDD(document).ready(function($){
						    jQueryDD(\'#countdown-'.$countdownId.'-'.$randomNr.'\').circularCountdown({
						        strokeDaysBackgroundColor:\'rgba('.$loadingColor.',1)\',
						        strokeDaysColor:\'rgba('.$bgColor.',1)\',
						        strokeHoursBackgroundColor:\'rgba('.$loadingColor.',1)\',
						        strokeHoursColor:\'rgba('.$bgColor.',1)\',
						        strokeMinutesBackgroundColor:\'rgba('.$loadingColor.',1)\',
						        strokeMinutesColor:\'rgba('.$bgColor.',1)\',
						        strokeSecondsBackgroundColor:\'rgba('.$loadingColor.',1)\',
						        strokeSecondsColor:\'rgba('.$bgColor.',1)\',
						        strokeWidth:6,
						        strokeBackgroundWidth:6,
						        countdownEasing:\'easeOutBounce\',
						        countdownTickSpeed:\'slow\',
						        currentDateTime: \''.$jsFromDate.'\',
						        until: new Date(\''.$jsToDate.'\'),
						        layout: \''.$layout.'\',
						        id: \''.$countdownId.'-'.$randomNr.'\'
						    });
						});
					</script>';
		} elseif ($countdownType==1) {
			$countdownToDateTime = explode(' ', $toDate);
			$countdownToDate = $countdownToDateTime[0];
			$countdownToTime = $countdownToDateTime[1];

			if ($days>0) {
				$html = '<div class="countdown countdown-days" id="countdown-'.$countdownId.'-'.$randomNr.'">
				<div class="unit-wrap">
					<div class="days"></div>
					<span class="ce-days-label"></span>
				</div>';
			} else {
				$html = '<div class="countdown" id="countdown-'.$countdownId.'-'.$randomNr.'">';
			}

			$html .= '<div class="unit-wrap">
					<div class="hours"></div>
					<span class="ce-hours-label"></span>
				</div>
				<div class="unit-wrap">
					<div class="minutes"></div>
					<span class="ce-minutes-label"></span>
				</div>
				<div class="unit-wrap">
					<div class="seconds"></div>
					<span class="ce-seconds-label"></span>
				</div>
			</div>';
			
			$html .= '<script type="text/javascript">
				 			runCountdown(\'countdown-'.$countdownId.'-'.$randomNr.'\',\''.$countdownToDate.'\',\''.$countdownToTime.'\',\''.$jsFromDate.'\',\''.$daysText.'\',\''.$hourText.'\',\''.$minText.'\',\''.$secText.'\');
				 		</script>';
		} else {
			if ($days>0) {
				$html = '<div class="countdown countdown-days" id="countdown-'.$countdownId.'-'.$randomNr.'">
								<div class="days">
				                    <span class="digits">42</span>
				                    <span class="label">'.$daysText.'</span>
				                </div>
				                <span class="sep days-sep">•</span>';
			} else {
				$html = '<div class="countdown" id="countdown-'.$countdownId.'-'.$randomNr.'">';
			}
			
			$html .= '<div class="hours">
		                    <span class="digits">11</span>
		                    <span class="label">'.$hourText.'</span>
		                </div>
		                <span class="sep">•</span>
		                <div class="minutes">
		                    <span class="digits">48</span>
		                    <span class="label">'.$minText.'</span>
		                </div>
		                <span class="sep">•</span>
		                <div class="seconds">
		                    <span class="digits">46</span>
		                    <span class="label">'.$secText.'</span>
		                </div>
			        </div>';

			$html .= '<script type="text/javascript">
				 			var jsCountdown = new JsCountdown("'.$jsFromDate.'", "'.$jsToDate.'", "countdown-'.$countdownId.'-'.$randomNr.'");
				 		</script>';
		}
	
        return $html;
    }
        
    //called on product list pages
    public function getProductCountdown(Varien_Object $_product, $_timeLeftText=false) {
		$deal = $this->getDealByProduct($_product);
		$html = '';		
		if (Mage::helper('dailydeal')->isEnabled() && $deal) {		
			$toDate = $deal->getDatetimeTo();
			$_product->setDatetimeTo($toDate);
			$currentDateTime = Mage::helper('dailydeal')->getCurrentDateTime(0);
			//set it to finished if the deal's time is up or product is out of stock
			if ($currentDateTime>=$deal->getDatetimeFrom() && $currentDateTime<=$deal->getDatetimeTo()) {
    			$finished = ($_product->isSaleable()) ? false : true;
    		} else {
				Mage::getModel('dailydeal/dailydeal')->refreshDeal($deal);
    			$finished = true;
    		}
    		$html .= ($_timeLeftText) ? '<span class="countdown-time-left">'.$this->__('Time left to buy').'</span>' : '';
			$html .= $this->getCountdown($_product, $finished);
		}
				
		return $html;
    }
	
	public function getCurrentDateTime($_storeId = null, $_format = 'Y-m-d H:i:s') {
		if (is_null($_storeId)) {
			$_storeId = Mage::app()->getStore()->getId();
		}
		$storeDatetime = new DateTime();
		$storeDatetime->setTimezone(new DateTimeZone(Mage::getStoreConfig('general/locale/timezone', $_storeId)));	
		
		return $storeDatetime->format($_format);
	}
	
	//returns the page number for the "Select a Product" tab on the deal edit page
    public function getProductPage($productId)
    {
    	$visibility = array(2, 4);
    	$collectionSize = Mage::getModel('catalog/product')->getCollection()->setOrder('entity_id', 'DESC')->addAttributeToFilter('visibility', $visibility)->addAttributeToFilter('entity_id', array('gteq' => $productId))->getSize();
    	
    	return ceil($collectionSize/20);
    }
    
    public function getMagentoVersion() {
		return (int)str_replace(".", "", Mage::getVersion());
    }
    
    //return true if deal should run on store
    public function runOnStore(Varien_Object $_deal, $_storeId = false) {
    	if ($_deal->getStores()!='') {
    		$dealStoreIds = array();
    		if (strpos($_deal->getStores(), ',')) {
				$dealStoreIds = explode(',', $_deal->getStores());
			} else {
				$dealStoreIds[] = $_deal->getStores();
			}
			if (!$_storeId) {
    			$_storeId = Mage::app()->getStore()->getId();
    		}
    		return (in_array($_storeId, $dealStoreIds)) ? true : false;
    	}
    	
    	return false;
    }
    
    //returns the main running deal of the current store
    public function getDeal($_excludeProductIds = array(0)) {  		    	
		$dealCollection = Mage::getModel('dailydeal/dailydeal')->getCollection()->addFieldToFilter('product_id', array('nin'=>$_excludeProductIds))->addFieldToFilter('status', array('eq'=>self::STATUS_RUNNING))->setOrder('position', 'ASC')->setOrder('dailydeal_id', 'DESC');	
			
		if (count($dealCollection)) {
	        foreach ($dealCollection as $deal) {
    	    	if ($this->runOnStore($deal)) {
		    		return $deal;
		    	}
		    }
		}	
		
		return false;	
    }
    
    //if the product is a deal and the deal is running under the current store, returns the deal data
    public function getDealByProduct(Varien_Object $_product) {  		    	
		$dealCollection = Mage::getModel('dailydeal/dailydeal')->getCollection()->addFieldToFilter('product_id', array('eq'=>$_product->getId()))->addFieldToFilter('status', array('eq'=>self::STATUS_RUNNING))->setOrder('position', 'ASC')->setOrder('dailydeal_id', 'DESC');
				
		if (count($dealCollection)) {
	        foreach ($dealCollection as $deal) {
    	    	if ($this->runOnStore($deal)) {
		    		return $deal;
		    	}
		    }
		}	
		
		return false;	
    }
    
    public function getDealsByProductIds($_productIds) {  		    	
		$dealCollection = Mage::getModel('dailydeal/dailydeal')->getCollection()->addFieldToFilter('product_id', array('in'=>$_productIds))->addFieldToFilter('status', array('eq'=>self::STATUS_RUNNING))->setOrder('position', 'ASC')->setOrder('dailydeal_id', 'DESC');
				
		if (count($dealCollection)) {
	        foreach ($dealCollection as $key=>$deal) {
    	    	if (!$this->runOnStore($deal)) {
		    		$dealCollection->removeItemByKey($key);
		    	}
		    }
		}	
		
		return $dealCollection;	
    }
    
    public function hex2rgb($hex) {
        return Mage::getModel('license/module')->hex2rgb($hex);
    }
    
    public function isMobile()
    {           
        if (Mage::getModel('license/module')->isMobile()) {
            return true;
        }
        
        return false;
    }    
    
    public function isTablet()
    {           
        if (Mage::getModel('license/module')->isTablet()) {
            return true;
        }
        
        return false;
    }  
    
}

class Devinc_Dailydeal_Model_Source_Status extends Varien_Object
{
    const STATUS_QUEUED		= -1;
    const STATUS_RUNNING	= 1;
    const STATUS_DISABLED	= 2;
    const STATUS_ENDED  	= 3; 

    static public function getOptionArray()
    {
        return array(
            self::STATUS_RUNNING    => Mage::helper('catalog')->__('Enabled'),
            self::STATUS_DISABLED   => Mage::helper('catalog')->__('Disabled')
        );
    }

}