function saveCredential() {
    $(credentialForm.formId).request({
        onFailure: function(error) {
            console.log('Error: '+error.statusText+' - Error No: '+error.status+' - Request URL: '+error.request.url);
        },
        onSuccess: function(response) {
            response = response.responseJSON;
            if (response.status == 1) {
                showMessage(response.message, 'success');
                window.location.replace(response.redirect_uri);
            } else {
                if (Object.isString(response.error)) {
                    showMessage(response.error, 'error');
                } else {
                    Object.keys(response.error).each(function(key, index) {
                        $(key).removeClassName('validation-passed');
                        $(key).removeClassName('validation-failed');

                        var value = response.error[key];
                        var inputClass = '';

                        if (value === 'blank') {
                            inputClass = 'required-entry';
                        } else if (value === 'not_validated') {
                            if (key === 'app_email') {
                                inputClass = 'validate-email';
                            } else if (key === 'app_api_key') {
                                inputClass = 'validate-alphanum';
                            }
                        } else if (value === 'length_issue') {
                            inputClass = 'validate-length minimum-length-32 maximum-length-32';
                        }

                        $(key).addClassName(inputClass);

                        credentialForm.validate();
                    });
                }
            }
        }
    });
}

function saveSettings() {
    $(settingsForm.formId).request({
        onFailure: function(error) {
            console.log('Error: '+error.statusText+' - Error No: '+error.status+' - Request URL: '+error.request.url);
        },
        onSuccess: function(response) {
            response = response.responseJSON;
            if (response.status == 1) {
                showMessage(response.message, 'success');
            } else {
                showMessage(response.error, 'error');
            }
        }
    });
}

function creataBatchImportProgressTracker() {
    var batchImportProgressTracker = new Ajax.PeriodicalUpdater('hidden-progress', baseUrl + 'getBatchImportProgress?isAjax=true', {
        onSuccess : function (response) {
            response = response.responseJSON.result;

            if (response.status === 'on_progress') {
                $('product-grid-container').addClassName('disabledGrid');

                checkOnGoingProcess = false;

                var processedRowCount = parseInt(response.processed);
                var totalRowCount = parseInt(response.total);

                var currentPercentage = processedRowCount * 100 / totalRowCount;

                setProgressBar(currentPercentage);
                showProgressBar();
            } else if (response.status === 'finished') {
                if (checkOnGoingProcess !== true) {
                    showMessage('Product import is successfully completed. Open my  '
                        + '<a href="http://app.prisync.com/dashboard" target="_blank">Prisync Dashboard</a> ', 'success');

                    $('product-grid-container').removeClassName('disabledGrid');
                    Element.hide('loading-mask');
                    setProgressBar(0);
                }

                hideProgressBar();
                batchImportProgressTracker.stop();
            } else if (response.error) {
                hideProgressBar();
                batchImportProgressTracker.stop();
                showMessage(response.error, 'error');
            }
        },
        onLoading : function () {
            isBatchImportProgressTrackerActive = true;

            if (checkOnGoingProcess) {
                Element.hide('loading-mask');
            }
        },
        onComplete : function () {
            isBatchImportProgressTrackerActive = false;
        },
        frequency: 4
    });

    return batchImportProgressTracker;
}

function showMessage(txt, type) { // Show info/error messages
    var html = '<ul class="messages"><li class="'+type+'-msg"><ul><li>' + txt + '</li></ul></li></ul>';
    $('messages').update(html);
}

function clearMessage() { // Clear messages
    var html = '';
    $('messages').update(html);
}

function setGridView() { // Set product list grid
    $('product_list_massaction-select').select('option[value=""]').invoke('remove'); // Remove blank action option
    product_list_massactionJsObject.useAjax = true; // Set useAjax for action
}

function initProgressBar() {
    return new Progress(0);
}

function setProgressBar(value) {
    progressBar.update(value);
}

function showProgressBar() {
    $('progress-bar-container').show();
}

function hideProgressBar() {
    $('progress-bar-container').hide();
}

function countCheckedProducts() { // Count how many products checked
    $(document).on('click', '#product_list_table td.a-center, #product_list_massaction a', function(event, element) {
        var productCount = $('product_list_massaction-count').innerHTML;
        var companyPackageLimit = $('companyPackageLimit').value;

        if (parseInt(productCount) > parseInt(companyPackageLimit)) {
            alert('Your package supports upto ' + companyPackageLimit + ' product');
            element.select('input')[0].click();
        }

        return productCount;
    });
}

var Progress = (function () {
    function Progress (p) {
        this.bar = document.querySelectorAll('#progress-bar > .progress-bar')[0];
        this.p = p;
        this.update(0);
    };
    Progress.prototype.update = function (value)  {
        if (this.p < 100) { this.p = value }
        this.bar.style.width = this.p + '%';
    };
    return Progress;
}());

