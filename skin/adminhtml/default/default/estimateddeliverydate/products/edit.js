estimatedDeliveryDate = {
    update: function (field, name, type) {
        if (!field.hasClassName("skip")) {
            if (field.multiple) {
                values = eval("data." + type + "_" + name);

                values.split(',').each(function (value) {

                    field.select("OPTION[value=" + value + "]")[0].selected = true;
                })
            } else if (field.hasClassName("time")) {
                values = eval("data." + type + "_" + name + ".split(',')");

                field.value = values[0];
                field.next().value = values[1];
                field.next().value = values[2];

            } else {
                field.value = eval("data." + type + "_" + name);
            }
        }
    },
    useConfig: function (elt, type, name) {
        eval("data=this.data." + type);

        elt.ancestors()[0].select('.input').each(function (field) {
            if (elt.checked) {
                field.disabled = true;
                estimatedDeliveryDate.update(field, name, "config");
                field.addClassName("disabled");
            } else {

                field.removeClassName("disabled");
                field.disabled = false;
            }
        });



    },
    useBase: function (elt, type, name) {
        eval("data=this.data." + type);

        fields_1 = elt.ancestors()[1].select('.input');
        field_2 = elt.ancestors()[1].select('.checkbox')[0];

        if (elt.checked) {

            field_2.disabled = true;
            field_2.addClassName("disabled");
            field_2.checked = eval("Math.round(data.default_use_config_" + name + ")");
            fields_1.each(function (field) {
                field.disabled = true;
                field.addClassName("disabled");
                estimatedDeliveryDate.update(field, name, 'default');
            })
        } else {

            field_2.disabled = false;
            field_2.removeClassName("disabled");
            if (!field_2.checked) {
                fields_1.each(function (field) {
                    field.disabled = false;
                    field.removeClassName("disabled");
                })
            }

        }

    }
};
document.observe("dom:loaded", function () {
    $$(".updateOnChange").each(function (input) {
        input.observe("change", function () {

            if (input.type === "checkbox")
                $(input.id).value = (input.checked) ? 1 : 0;
            else if (input.type === "select-multiple") {
                value = new Array;
                input.select('option[selected]').each(function (o) {
                    value.push(o.value);
                });

                $(input.id).value = value.join(',')
            } else if (input.type === "select-one") {
                value = new Array;
                input.ancestors()[0].select("select").each(function (s) {
                    value.push(s.value);
                });
                $(input.id).value = value.join(',');

            } else
                $(input.id).value = input.value;
        })
    })
});

