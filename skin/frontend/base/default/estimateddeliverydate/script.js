/*
 * Copyright © 2016 Wyomind. All rights reserved.
 * See LICENSE.txt for license details.
 */


var launched = false;
var estimatedDeliveryDate = {
    product: {
        id: null
    },
    translate: {},
    update: function () {
        attr = [];
        selection = true;
        $$(".super-attribute-select").each(function (s) {
            if (s.value === "")
                selection = false;
            attr.push({'id': s.id, 'value': s.value});
        });
        if (selection) {
            if (attr.length) {
                estimatedDeliveryDate.children.each(function (e) {
                    found = true;
                    attr.each(function (a) {
                        if (eval("e." + a.id) !== a.value)
                            found = false;
                    });
                    if (found) {
                        estimatedDeliveryDate.product.id=e.id;
                        $("estimated-delivery-date-container").update(e.message);
                        estimatedDeliveryDate.countdown.launch(false);
                    }
                })
            }
        } else {
            $("estimated-delivery-date-container").update();
        }
    },
    countdown: {
        format: null,
        realtime: null,
        format:'',
                realtime:'',
                time: 0,
        launch: function (decrement) {

            $$('.edd_countdown').each(function (countdown) {


                if (countdown) {
                    var current_countdown = parseInt(countdown.getAttribute('countdown')) - estimatedDeliveryDate.countdown.time;
                    if (decrement) {
                        estimatedDeliveryDate.countdown.time += 1;
                    }
                    if (current_countdown <= 0) {
                        countdown.update("");
                        return;
                    }

                    var d = Math.floor(current_countdown / 86400);
                    var h = Math.floor(current_countdown % 86400 / 3600);
                    var m = Math.floor(current_countdown % 86400 % 3600 / 60);
                    var s = (current_countdown % 86400 % 3600 % 60);
                    var timer = new Array();
                    if (estimatedDeliveryDate.countdown.format >= 1) {
                        var day = (d >= 0) ? ((d > 1) ? (d + " " + estimatedDeliveryDate.translate.days) : (d + " " + estimatedDeliveryDate.translate.day)) : "";
                        if (day != "") {
                            if (estimatedDeliveryDate.countdown.type != 1) {
                                if (d > 0) {
                                    timer.push(day);
                                }
                            } else {
                                timer.push('<div class="knob-container"><input value="' + d + '" type="text" id="countdown-sec" data-width="60" data-height="60" data-max="' + d + '" data-thickness=".2"  data-fgcolor="green"  data-bgcolor="white" data-min="0" class="knob" data-readonly="true" /> <label>' + estimatedDeliveryDate.translate.days + '</label></div>');
                            }
                        }
                    }
                    if (estimatedDeliveryDate.countdown.format >= 2) {
                        var hour = (h >= 0) ? ((h > 1) ? (h + " " + estimatedDeliveryDate.translate.hours) : (h + " " + estimatedDeliveryDate.translate.hour)) : "";
                        if (hour != "") {
                            if (estimatedDeliveryDate.countdown.type != 1) {
                                timer.push(hour);
                            } else {
                                timer.push('<div class="knob-container"><input value="' + h + '" type="text" id="countdown-sec" data-width="60" data-height="60" data-max="24" data-thickness=".2"  data-fgcolor="green"  data-bgcolor="white" data-min="0" class="knob" data-readonly="true" /> <label>' + estimatedDeliveryDate.translate.hours + '</label></div>');
                            }
                        }
                    }
                    if (estimatedDeliveryDate.countdown.format >= 3) {
                        var min = (m >= 0) ? ((m > 1) ? (m + " " + estimatedDeliveryDate.translate.minutes) : (m + " " + estimatedDeliveryDate.translate.minute)) : "";
                        if (min != "") {
                            if (estimatedDeliveryDate.countdown.type != 1) {
                                timer.push(min);
                            } else {

                                timer.push('<div class="knob-container"><input value="' + m + '" type="text" id="countdown-sec" data-width="60" data-height="60" data-max="59" data-thickness=".2"  data-fgcolor="green"  data-bgcolor="white" data-min="0" class="knob" data-readonly="true" /> <label>' + estimatedDeliveryDate.translate.minutes + '</label></div>');
                            }
                        }

                    }
                    if (estimatedDeliveryDate.countdown.format >= 4) {
                        var sec = (s >= 0) ? ((s > 1) ? (s + " " + estimatedDeliveryDate.translate.seconds) : (s + " " + estimatedDeliveryDate.translate.second)) : "";
                        if (sec != "") {
                            if (estimatedDeliveryDate.countdown.type != 1) {
                                timer.push(sec);
                            } else {
                                timer.push('<div class="knob-container"><input value="' + s + '" type="text" id="countdown-sec" data-width="60" data-height="60" data-max="59" data-thickness=".2"  data-fgcolor="green"  data-bgcolor="white" data-min="0" class="knob" data-readonly="true" /> <label>' + estimatedDeliveryDate.translate.seconds + '</label></div>');
                            }
                        }
                    }

                    countdown.update(timer.join().replace(/,/g, ' '));
                    if (estimatedDeliveryDate.countdown.type == 1) {
                        jQuery('.knob').knob();
                    }

                }
            })
        }
    },
    customOptions: {
        url: null,
        update: function () {
            leadtime_from = new Array;
            leadtime_to = new Array;
            edd = estimatedDeliveryDate.customOptions.data;
            $$('.product-custom-option').each(function (s) {

                if (s.hasClassName("multiselect")) {
                    if (s.value > 0) {
                        s.select("OPTION").each(function (opt) {
                            if (opt.selected) {
                                lt = opt.value.split(',')
                                eval("leadtime_from.push(edd[" + s.id.replace('select_', '') + "][" + lt[0] + "])");
                                if (typeof (lt[1]) != "undefined")
                                    eval("leadtime_to.push(edd[" + s.id.replace('select_', '') + "][" + lt[1] + "])");
                            }
                        })
                    }
                } else if (s.hasClassName("radio") || s.hasClassName("checkbox")) {

                    if (s.checked) {
                        lt = s.value.split(',')
                        id = s.id.replace('options_', '');
                        id = id.split("_");
                        id = id[0];
                        ;
                        eval("leadtime_from.push(edd[" + id + "][" + lt[0] + "])");
                        if (typeof (lt[1]) != "undefined")
                            eval("leadtime_to.push(edd[" + id + "][" + lt[1] + "])");
                    }
                } else {
                    if (s.value > 0) {
                        if (typeof edd[s.id.replace('select_', '')] != "undefined") {
                            lt = s.value.split(',')
                            eval("leadtime_from.push(edd[" + s.id.replace('select_', '') + "][" + lt[0] + "])");
                            if (typeof (lt[1]) != "undefined")
                                eval("leadtime_to.push(edd[" + s.id.replace('select_', '') + "][" + lt[1] + "])");
                        }
                    }
                }
            })
            $$(".estimated-delivery-date").each(function (element) {
                data = {id: estimatedDeliveryDate.product.id, elt: element.id, leadtime_from: Object.toJSON(leadtime_from), leadtime_to: Object.toJSON(leadtime_to)}

                element.update('').addClassName("loading");
                new Ajax.Request(estimatedDeliveryDate.customOptions.url, {
                    parameters: data,
                    method: 'post',
                    onSuccess: function (response) {
                        element.replace(response.responseText);
                        element.removeClassName("loading");
                        countdown(false);
                        estimatedDeliveryDate.countdown.launch(false);

                    }

                })
            })
        }
    }
}










document.observe("dom:loaded", function () {
    estimatedDeliveryDate.countdown.launch();
    if (estimatedDeliveryDate.countdown.realtime == 1) {
        setInterval(function () {
            estimatedDeliveryDate.countdown.launch(true);
        }, 1000);
    } else {
        estimatedDeliveryDate.countdown.launch(true);
    }
    $$(".swatch-link").each(function (sa) {

        sa.observe("click", function () {

            setTimeout(function () {
                estimatedDeliveryDate.update()
            }, 100);
        })
    })
    $$(".super-attribute-select").each(function (sa) {

        sa.observe("change", function () {
            estimatedDeliveryDate.update()
        })
    })


    $$('.swatch').each(function (select) {
        select.observe('click', function () {
            setTimeout(function () {
                estimatedDeliveryDate.customOptions.update();
            }, 200);
            
        })
    })

    $$('.product-custom-option').each(function (select) {
        select.observe('change', function () {
            estimatedDeliveryDate.customOptions.update();

        })
    })



});